# vereinsleben.de

## Links

- Production: https://www.vereinsleben.de
- Staging: https://www-stag.vereinsleben.de

## Development

Copy `.env.example` to `.env` for development and set the appropriate values.
To test email in development you can use the "log" email driver which just logs the mails instead of sending them.

### Pulls + Build

You can fire up a `git pull` and all necessary build tasks by executing `bin/pull.sh`.

### Watch and build files in development

... by using `gulp watch`.

## Setup

After checking out the project please execute the following in the project root:

```bash
composer install
npm install
npm install babel-preset-es2015 && npm install babel-preset-react # you probably have to do that in case of errors with gulp
gulp
```

## Deployment and environments

Deploy to staging and production via [Capistrano](http://capistranorb.com/).

A few notes and warnings to consider:

- Never make any manual changes to the application on the server, they will be gone after the next deployment.
- According to the config file `config/deploy.rb` the only file(s) shared between releases are `.env`, the only shared folders are `storage`, `public/uploaded`, `node_modules`. All other files and folders should not contain any user-generated data or content which should be kept, because everything else will be unreachable to the application after the next release and eventually be destroyed after a few (currently 20) releases.
- Capistrano runs commands remotely on the server(s), so you can only deploy to servers (staging/production) you have SSH access to the user `www-stag` and `www-prod` from your local machine.
- Capistrano pulls the to-be-released code from the `master` branch from the GitHub repository. So before deploying changes you need to merge your changes into the appropriate deploy branch and push it to GitHub because Capistrano pulls from there.

### Install Capistrano

Install Capistrano and cofig it for deployment. 

Workflow:
Capistrano (Auth SSH Key) --> Staging or Production Server --> Github (Auth SSH Deploy Key) --> Deploy on Staging or Production
                                                                            
### Run the deployment

Note that you need SSH access to the user `www-stag` on the target machine from your local environment.

#### Deploy to staging

Note: Staging deploys are pulled from the `master` branch.

```
cap staging deploy
```

#### Deploy to production

Note: Production deploys are pulled from the `master` branch.

```
cap production deploy
```
