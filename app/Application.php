<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    public function club(){
        return $this->belongsTo('Vereinsleben\Club');
    }

    public function user(){
        return $this->hasOne('Vereinsleben\User');
    }
}
