<?php

namespace Vereinsleben;

use DB;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Illuminate\Database\Eloquent\Model;
use Codesleeve\Stapler\ORM\EloquentTrait as StaplerEloquentTrait;

class Bandage extends Model implements StaplerableInterface
{
    use StaplerEloquentTrait;
    // fillable via mass assignment
    protected $fillable = [
        'name',
        'email',
        'founded',
        'shorthand',
        'member_count',
        'zip',
        'street',
        'house_number',
        'city',
        'description',
        'about',
        'avatar',
        'header',
        'published',
        'slug',
        'unowned',
        'reviewed',
        'bandage_id',
        'avatar',
        'type',
        'region'
    ];

    /**
     * Constructor
     *
     * @return Bandage
     */
    public function __construct(array $attributes = array())
    {
        $this->hasAttachedFile('avatar', [
            'styles'      => [
                'startPage'  => '140x140#', # resize then crop
                'list'       => '75x75#',   # resize then crop
                'map'        => '30x30#',   # resize then crop
                'singleView' => '293x293#', # resize then crop
            ],
            'url'         => '/uploaded/bandage/:id/:attachment/:style-:secure_hash.:extension',
            'default_url' => '/default_images/bandage/default-:attachment-:style.jpg',
        ]);
        $this->hasAttachedFile('header', [
            'styles'      => [
                'singleView' => '1920x492#', # resize then crop
            ],
            'url'         => '/uploaded/bandage/:id/:attachment/:style-:secure_hash.:extension',
            'default_url' => '/default_images/bandage/default-:attachment-:style.jpg',
        ]);

        parent::__construct($attributes);
    }

    /**
     * Returns the full concatenated address.
     *
     * @return string
     */
    public function fullAddress()
    {
        if ($this->street !== null && trim($this->street) != ''){
            return sprintf('%s %s, %s %s',
                $this->street,
                $this->house_number,
                $this->zip,
                $this->city
            );
        } else if ($this->city !== null && trim($this->city) != ''){
            return sprintf('%s %s',
                $this->zip,
                $this->city
            );
        } else{
            return '';
        }
    }

    /**
     * Checks if the bandage is published.
     *
     * @return boolean
     */
    public function isPublished()
    {
        return $this->published === 1;
    }

    //
    public function clubs()
    {

        return $this->hasMany('Vereinsleben\Club');
    }

    /**
     * The users with roles in this bandage.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('Vereinsleben\User', 'user_bandage_role')
                    ->withPivot('role_id');
    }

    /**
     * the father of bandage
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo('Vereinsleben\Bandage', 'parent_id');
    }

    /**
     * the children of bandage
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany('Vereinsleben\Bandage', 'parent_id');
    }
	
		/**
		 * The sports of this bandage.
		 *
		 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
		 */
		public function sports()
		{
			return $this->belongsToMany('Vereinsleben\Sport');
		}
    
    /**
     * The social links of this bandage
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function socialLinks()
    {
        return $this->morphMany('Vereinsleben\SocialLink', 'linkable');
    }

    /**
     * The posts of this bandage.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany('Vereinsleben\Post', 'bandage_id');
    }

    /**
     * The events of this bandage.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function events()
    {
        return $this->hasMany('Vereinsleben\Event', 'bandage_id');
    }
	
		/**
		 * The events of this bandage.
		 *
		 * @return \Illuminate\Database\Eloquent\Relations\HasMany
		 */
		public function contactPersons()
		{
			return $this->hasMany('Vereinsleben\ContactPerson', 'bandage_id');
		}
    
    /**
     * The contacts of this bandage.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function contacts()
    {
        return $this->morphtomany('Vereinsleben\User', 'contactable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     *
     */
    public function views()
    {
        return $this->morphMany('Vereinsleben\View', 'viewable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function meta()
    {
        return $this->morphOne('Vereinsleben\Meta', 'metaable');
    }

    /**
     * Scope a query to fetch the published bandages
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublished($query)
    {
        return $query->where('published', 1);
    }

    /**
     * Scoped keyword search query
     *
     * @param $query
     * @param $keyword
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearch($query, $keyword)
    {
        $query = $query->where(function($query) use ($keyword){
            $query->where('name', 'like', '%' . $keyword . '%')
                  ->orWhere('city', 'like', '%' . $keyword . '%')
                  ->orWhere('zip', 'like', '%' . $keyword . '%');
        });

        return $query;
    }

    /**
     * Scope a query to fetch the closest clubs
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeClosest($query, $lat, $lon, $distance = 50)
    {
        // TODO: optimize? http://stackoverflow.com/a/29931224/650203

        return $query->select(DB::raw('*, (
          6371 * acos (
            cos ( radians(' . (float)$lat . ') )
            * cos( radians( lat ) )
            * cos( radians( lng ) - radians(' . (float)$lon . ') )
            + sin ( radians(' . (float)$lat . ') )
            * sin( radians( lat ) )
          )
        ) AS distance'))->having('distance', '<=', $distance)->orderBy('distance', 'asc');

        // Using ST_Distance and ST_Within function in MySQL 5.6.1
        #$lat = 50.9469188;
        #$lon = 10.7092884;
        #$maximumDistance = 1000;
        #$lonBound1 = $lon - $maximumDistance / abs(cos(deg2rad($lat)) * 69);
        #$lonBound2 = $lon + $maximumDistance / abs(cos(deg2rad($lat)) * 69);
        #$latBound1 = $lat - ($maximumDistance / 69);
        #$latBound2 = $lat + ($maximumDistance / 69);
        #$clubs = Club::whereRaw("
        #  ST_Within(
        #    geolocation,
        #    envelope(
        #      linestring(
        #        point($lonBound1, $latBound1),
        #        point($lonBound2, $latBound2)
        #      )
        #    )
        #  )")->orderByRaw("
        #  ST_Distance(
        #    geolocation,
        #    GeomFromText('POINT($lat $lon)')
        #  )"
        #)->get();
    }
	
	
	public function administrators()
	{
		$role = new Role();
		$adminRoleId = $role->getByConstant(Role::BANDAGE_ADMIN)->id;
		
		return $this->users()->wherePivot('role_id', $adminRoleId);
	}
	
	public function subcribles()
	{
		$role = new Role();
		return $this->belongsToMany('Vereinsleben\User', 'user_bandage_role')
			->wherePivot('role_id', $role->getByConstant(Role::BANDAGE_SUBSCRIBE)->id);
	}
}
