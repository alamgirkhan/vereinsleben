<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;

/**
 * Vereinsleben\ClubSport
 *
 * @property int $id
 * @property int|null $club_id
 * @property int|null $sport_id
 * @property string|null $m_club_id
 * @property string|null $m_sport_id
 * @property-read \Vereinsleben\Club|null $club
 * @property-read \Vereinsleben\Sport|null $sport
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\ClubSport whereClubId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\ClubSport whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\ClubSport whereMClubId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\ClubSport whereMSportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\ClubSport whereSportId($value)
 * @mixin \Eloquent
 */
class BandageSport extends Model
{
    protected $table = 'bandage_sport';

    protected $fillable = [
        'bandage_id',
        'sport_id'
    ];

    public $timestamps = false;

    /**
     * Associated clubs.
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function club()
    {
        return $this->belongsTo('Vereinsleben\Bandage', 'bandage_id');
    }

    /**
     * Associated sport.
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function sport()
    {
        return $this->belongsTo('Vereinsleben\Sport', 'sport_id');
    }
}
