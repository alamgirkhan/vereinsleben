<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
	const STATE_HIDDEN = 'HIDDEN';
	const STATE_PUBLISHED = 'PUBLISHED';
	const STATE_DRAFT = 'DRAFT';
	
	protected $table = 'campaign';
	
	protected $fillable = [
		'name', 'customers_name', 'link', 'description', 'image_1', 'image_2',
		'image_3', 'user_id', 'published_from', 'published_to'
	];
	
	public function campaignList()
	{
		return Campaign::all();
	}
	
	public function getCampaignById($id_campaign)
	{
		return Campaign::all()->where('id', $id_campaign)->first();
	}
	
	public function states()
	{
		return $this->morphToMany('Vereinsleben\State', 'stateable');
	}
}
