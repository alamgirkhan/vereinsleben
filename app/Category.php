<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $table = 'category';

    public function sportsID()
    {
        $id = $this->select('id')->where('parent_id', '=', 66)->get();

        return $id;
    }
}
