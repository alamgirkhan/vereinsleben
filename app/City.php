<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'city';
}
