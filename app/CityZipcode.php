<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;

class CityZipcode extends Model
{
    protected $table = 'city_zipcode';

    /**
     * The city this zipcode belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('Vereinsleben\City', 'city_id');
    }
}
