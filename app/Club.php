<?php

namespace Vereinsleben;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Hootlex\Friendships\Traits\Friendable;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait as StaplerEloquentTrait;

/**
 * Vereinsleben\Club
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $shorthand
 * @property string|null $founded
 * @property string|null $description
 * @property string|null $street
 * @property string|null $house_number
 * @property string|null $city
 * @property string|null $federal_state
 * @property string|null $zip
 * @property float|null $lat
 * @property float|null $lng
 * @property string|null $email
 * @property int|null $published
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $created_at
 * @property string|null $slug
 * @property string|null $about
 * @property int|null $gallery_id
 * @property string|null $member_count
 * @property string $m_id
 * @property string $m_owner
 * @property string|null $m_sports
 * @property string|null $m_achievements
 * @property string|null $m_social_links
 * @property string|null $m_social_web
 * @property string|null $m_social_facebook
 * @property string|null $m_social_twitter
 * @property string|null $m_social_googleplus
 * @property string|null $m_social_youtube
 * @property string|null $m_social_instagram
 * @property string|null $m_social_flickr
 * @property string|null $m_avatar_original
 * @property string|null $m_avatar_singleview
 * @property string|null $m_avatar_startpage
 * @property string|null $m_avatar_list
 * @property string|null $m_avatar_map
 * @property string|null $m_header_original
 * @property string|null $m_header_singleview
 * @property string|null $avatar_file_name
 * @property int|null $avatar_file_size
 * @property string|null $avatar_content_type
 * @property string|null $avatar_updated_at
 * @property string|null $header_file_name
 * @property int|null $header_file_size
 * @property string|null $header_content_type
 * @property string|null $header_updated_at
 * @property int|null $unowned
 * @property int $reviewed
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Vereinsleben\ClubAchievement[] $achievements
 * @property-read \Illuminate\Database\Eloquent\Collection|\Vereinsleben\Event[] $events
 * @property-read \Illuminate\Database\Eloquent\Collection|\Vereinsleben\Image[] $images
 * @property-read \Illuminate\Database\Eloquent\Collection|\Vereinsleben\Post[] $posts
 * @property-read \Illuminate\Database\Eloquent\Collection|\Vereinsleben\SocialLink[] $socialLinks
 * @property-read \Illuminate\Database\Eloquent\Collection|\Vereinsleben\Sport[] $sports
 * @property-read \Illuminate\Database\Eloquent\Collection|\Vereinsleben\Team[] $teams
 * @property-read \Illuminate\Database\Eloquent\Collection|\Vereinsleben\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club closest($lat, $lon, $distance = 50)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\Club onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club published()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereAbout($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereAvatarContentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereAvatarFileName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereAvatarFileSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereAvatarUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereFederalState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereFounded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereGalleryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereHeaderContentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereHeaderFileName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereHeaderFileSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereHeaderUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereHouseNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereLng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereMAchievements($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereMAvatarList($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereMAvatarMap($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereMAvatarOriginal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereMAvatarSingleview($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereMAvatarStartpage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereMHeaderOriginal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereMHeaderSingleview($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereMId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereMOwner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereMSocialFacebook($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereMSocialFlickr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereMSocialGoogleplus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereMSocialInstagram($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereMSocialLinks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereMSocialTwitter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereMSocialWeb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereMSocialYoutube($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereMSports($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereMemberCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereReviewed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereShorthand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereUnowned($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Club whereZip($value)
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\Club withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\Club withoutTrashed()
 * @mixin \Eloquent
 */
class Club extends Model implements StaplerableInterface
{
    use StaplerEloquentTrait,
        Friendable,
        SoftDeletes;

    const STATUS_REVIEWED = 1;

    protected $table = 'club';
    protected $morphClass = 'club';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public $timestamps = false;

    // fillable via mass assignment
    protected $fillable = [
        'name',
        'street',
        'house_number',
        'zip',
        'city',
        'email',
        'founded',
        'shorthand',
        'member_count',
        'description',
        'about',
        'avatar',
        'header',
        'published',
        'slug',
        'unowned',
        'reviewed',
        'bandage_id'
    ];

    /**
     * Constructor
     *
     * @return Club
     */
    public function __construct(array $attributes = array())
    {
        $this->hasAttachedFile('avatar', [
            'styles'      => [
                'startPage'  => '140x140#', # resize then crop
                'list'       => '75x75#', # resize then crop
                'map'        => '30x30#', # resize then crop
                'singleView' => '293x293#', # resize then crop
            ],
            'url'         => '/uploaded/club/:id/:attachment/:style-:secure_hash.:extension',
            'default_url' => '/default_images/club/default-:attachment-:style.jpg',
        ]);
        $this->hasAttachedFile('header', [
            'styles'      => [
                'singleView' => '1920x492#', # resize then crop
            ],
            'url'         => '/uploaded/club/:id/:attachment/:style-:secure_hash.:extension',
            'default_url' => '/default_images/club/default-:attachment-:style.jpg',
        ]);

        parent::__construct($attributes);
    }

    /**
     * Returns the full concatenated address.
     *
     * @return string
     */
    public function fullAddress()
    {
        if ($this->street !== null && trim($this->street) != ''){
            return sprintf('%s %s, %s %s',
                $this->street,
                $this->house_number,
                $this->zip,
                $this->city
            );
        } else if ($this->city !== null && trim($this->city) != ''){
            return sprintf('%s %s',
                $this->zip,
                $this->city
            );
        } else{
            return '';
        }
    }

    /**
     * Returns the full concatenated address with country, for geocoding.
     *
     * @return string
     */
    public function fullAddressForGeocoding()
    {
        $addressString = $this->fullAddress();

        if ($addressString !== ''){
            return sprintf('%s, %s',
                $addressString,
                'Germany'
            );
        } else{
            return '';
        }
    }

    /**
     * Checks if the club is published.
     *
     * @return boolean
     */
    public function isPublished()
    {
        return $this->published === 1;
    }

    /**
     * Returns all sports as concatenated string.
     *
     * @return string
     */
    public function allSportsStringsAsArray()
    {
        $arrSportarten = $this->sports->pluck('title')->toArray();
        $arrSportarten = array_values(array_unique($arrSportarten));
        return $arrSportarten;
    }

    /**
     * Returns all sports as concatenated string.
     *
     * @return string
     */
    public function allSportsString()
    {
        $strSportarten = join(', ', $this->allSportsStringsAsArray());
        return $strSportarten;
    }

    /**
     * The images of this club
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function images()
    {
        return $this->morphMany('Vereinsleben\Image', 'imageable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     *
     */
    public function views()
    {
        return $this->morphMany('Vereinsleben\View', 'viewable');
    }

    public function bandage()
    {
        return $this->belongsTo('Vereinsleben\Bandage');
    }

    /**
     * The posts of this club.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany('Vereinsleben\Post', 'club_id');
    }

    /**
     * The sports of this club.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function sports()
    {
        return $this->belongsToMany('Vereinsleben\Sport');
    }

    /**
     * Events of this club
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function events()
    {
        return $this->hasMany('Vereinsleben\Event', 'club_id');
    }

    /**
     * Achievements of this club
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function achievements()
    {
        return $this->hasMany('Vereinsleben\ClubAchievement', 'club_id');
    }

    /**
     * Teams of this club
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function teams()
    {
        return $this->hasMany('Vereinsleben\Team', 'club_id');
    }

    /**
     * The social links of this club
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function socialLinks()
    {
        return $this->morphMany('Vereinsleben\SocialLink', 'linkable');
    }

    /**
     * The users with roles in this club.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('Vereinsleben\User', 'user_club_role')
                    ->withPivot('role_id', 'member_id')
                    ->using('Vereinsleben\ClubUser');

    }

    /**
     * Scope a query to fetch the closest clubs
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeClosest($query, $lat, $lon, $distance = 50)
    {
        // TODO: optimize? http://stackoverflow.com/a/29931224/650203

        return $query->select(DB::raw('*, (
          6371 * acos (
            cos ( radians(' . (float)$lat . ') )
            * cos( radians( lat ) )
            * cos( radians( lng ) - radians(' . (float)$lon . ') )
            + sin ( radians(' . (float)$lat . ') )
            * sin( radians( lat ) )
          )
        ) AS distance'))->having('distance', '<=', $distance)->orderBy('distance', 'asc');

        // Using ST_Distance and ST_Within function in MySQL 5.6.1
        #$lat = 50.9469188;
        #$lon = 10.7092884;
        #$maximumDistance = 1000;
        #$lonBound1 = $lon - $maximumDistance / abs(cos(deg2rad($lat)) * 69);
        #$lonBound2 = $lon + $maximumDistance / abs(cos(deg2rad($lat)) * 69);
        #$latBound1 = $lat - ($maximumDistance / 69);
        #$latBound2 = $lat + ($maximumDistance / 69);
        #$clubs = Club::whereRaw("
        #  ST_Within(
        #    geolocation,
        #    envelope(
        #      linestring(
        #        point($lonBound1, $latBound1),
        #        point($lonBound2, $latBound2)
        #      )
        #    )
        #  )")->orderByRaw("
        #  ST_Distance(
        #    geolocation,
        #    GeomFromText('POINT($lat $lon)')
        #  )"
        #)->get();
    }

    /**
     * Scope a query to fetch the published clubs
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublished($query)
    {
        return $query->where('published', 1);
    }

    public function scopeNotPublished($query)
    {
        return $query->where('published', 0);
    }

    /**
     * Calculate club profile completion progress.
     *
     * @return array
     */
    public function getClubProfileCompletionProgress()
    {
        $empty = function($var){
            return !isset($var) || trim($var) == '';
        };

        $progressCriteriaMap = [
            [
                'name'     => 'logo',
                'title'    => 'Logo',
                'weight'   => 20,
                'finished' => (!$empty($this->avatar_file_name))
            ], [
                'name'     => 'header',
                'title'    => 'Headerbild',
                'weight'   => 15,
                'finished' => (!$empty($this->header_file_name))
            ], [
                'name'     => 'infos',
                'title'    => 'Infos',
                'weight'   => 20,
                'finished' => (!$empty($this->shorthand) && !$empty($this->founded) && $this->founded != 0 && !$empty($this->member_count) && !$empty($this->about) && !$empty($this->description))
            ], [
                'name'     => 'teams',
                'title'    => 'Mannschaften',
                'weight'   => 15,
                'finished' => ($this->teams()->count() > 0)
            ], [
                'name'     => 'links',
                'title'    => 'Weblinks',
                'weight'   => 10,
                'finished' => ($this->socialLinks()->count() > 0)
            ], [
                'name'     => 'achievements',
                'title'    => 'Erfolge',
                'weight'   => 10,
                'finished' => ($this->achievements()->count() > 0)
            ], [
                'name'     => 'posts',
                'title'    => 'Status-Post',
                'weight'   => 10,
                'finished' => ($this->posts()->count() > 0)
            ]
        ];

        $result = [
            'progress_percentage' => array_sum(array_map(function($e){
                return ($e['finished'] === true) ? $e['weight'] : 0;
            }, $progressCriteriaMap)),
            'solved_criteria'     => array_filter($progressCriteriaMap, function($e){
                return $e['finished'] === true;
            }),
            'unsolved_criteria'   => array_filter($progressCriteriaMap, function($e){
                return $e['finished'] === false;
            }),
        ];

        return $result;
    }
    
		public function getPercentClubProfileProgressAttribute()
			{
				$empty = function($var){
					return !isset($var) || trim($var) == '';
				};
				
				$progressCriteriaMap = [
					[
						'name'     => 'logo',
						'title'    => 'Logo',
						'weight'   => 20,
						'finished' => (!$empty($this->avatar_file_name))
					], [
						'name'     => 'header',
						'title'    => 'Headerbild',
						'weight'   => 15,
						'finished' => (!$empty($this->header_file_name))
					], [
						'name'     => 'infos',
						'title'    => 'Infos',
						'weight'   => 20,
						'finished' => (!$empty($this->shorthand) && !$empty($this->founded) && $this->founded != 0 && !$empty($this->member_count) && !$empty($this->about) && !$empty($this->description))
					], [
						'name'     => 'teams',
						'title'    => 'Mannschaften',
						'weight'   => 15,
						'finished' => ($this->teams->count() > 0)
					], [
						'name'     => 'links',
						'title'    => 'Weblinks',
						'weight'   => 10,
						'finished' => ($this->socialLinks->count() > 0)
					], [
						'name'     => 'achievements',
						'title'    => 'Erfolge',
						'weight'   => 10,
						'finished' => ($this->achievements->count() > 0)
					], [
						'name'     => 'posts',
						'title'    => 'Status-Post',
						'weight'   => 10,
						'finished' => ($this->posts->count() > 0)
					]
				];
				
				return array_sum(array_map(function($e){
						return ($e['finished'] === true) ? $e['weight'] : 0;
					}, $progressCriteriaMap));
			}
			
			public function getHasAvatarAttribute() {
				$empty = function($var){
					return !isset($var) || trim($var) == '';
				};
				
    	  if (!$empty($this->avatar_file_name)) {
    	  	return true;
	      }
    	  return false;
			}
			
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function members()
    {
        $role = new Role();
        $memberRoleId = $role->getByConstant(Role::CLUB_MEMBER)->id;
        $oldMemberId = Member::where('name', 'old')->firstOrFail()->id;

        return $this->users()->wherePivot('role_id', $memberRoleId)
                    ->wherePivot('member_id', '!=', $oldMemberId);
    }

    public function administrators()
    {
        $role = new Role();
        $adminRoleId = $role->getByConstant(Role::CLUB_ADMIN)->id;

        return $this->users()->wherePivot('role_id', $adminRoleId);
    }

    /**
     * get clubadmin requests
     */
    public function getAdminRequests(){

        $role = new Role();
        $adminreq = $role->getByConstant(Role::CLUB_ADMIN_REQUEST)->id;

        return $this->users()->wherePivot('role_id', $adminreq);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function fans()
    {
        $role = new Role();
        $memberRoleId = $role->getByConstant(Role::CLUB_FAN)->id;

        return $this->users()->wherePivot('role_id', $memberRoleId);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function oldMembers()
    {
        $role = new Role();
        $memberRoleId = $role->getByConstant(Role::CLUB_MEMBER)->id;
        $oldMemberId = Member::where('name', 'old')->firstOrFail()->id;

        return $this->users()->wherePivot('role_id', $memberRoleId)
                    ->wherePivot('member_id', $oldMemberId);
    }

    public function traininsorts()
    {
        return $this->belongsToMany('Vereinsleben\Trainingsort');
    }


    public function contacts()
    {
        return $this->morphtomany('Vereinsleben\User', 'contactable');
    }

    public function scopeSearch($query, $searchString)
    {
        $query = $query->where(function($query) use ($searchString){
            $query->where('name', 'like', '%' . $searchString . '%');
        });

        return $query;
    }

    public function application()
    {
        return $this->hasOne('Vereinsleben\Application');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function meta()
    {
        return $this->morphOne('Vereinsleben\Meta', 'metaable');
    }

    public function states()
    {
        return $this->morphToMany('Vereinsleben\State', 'stateable');
    }

    public function suggestedFrom()
    {
        return $this->belongsTo('Vereinsleben\User', 'suggested_from');
    }

    public function departments()
    {
        return $this->hasMany('Vereinsleben\Department', 'club_id');
    }
		public function reviews()
		{
			return $this->hasMany('Vereinsleben\Review')->where('parent_id', 0);
		}
}
