<?php

namespace Vereinsleben;

use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait as StaplerEloquentTrait;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Vereinsleben\ClubAchievement
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $description
 * @property string|null $date
 * @property string|null $m_club_id
 * @property int|null $club_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $picture_file_name
 * @property int|null $picture_file_size
 * @property string|null $picture_content_type
 * @property string|null $picture_updated_at
 * @property string|null $year
 * @property string|null $month
 * @property string|null $day
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Vereinsleben\Club|null $club
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\ClubAchievement onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\ClubAchievement whereClubId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\ClubAchievement whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\ClubAchievement whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\ClubAchievement whereDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\ClubAchievement whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\ClubAchievement whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\ClubAchievement whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\ClubAchievement whereMClubId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\ClubAchievement whereMonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\ClubAchievement wherePictureContentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\ClubAchievement wherePictureFileName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\ClubAchievement wherePictureFileSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\ClubAchievement wherePictureUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\ClubAchievement whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\ClubAchievement whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\ClubAchievement whereYear($value)
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\ClubAchievement withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\ClubAchievement withoutTrashed()
 * @mixin \Eloquent
 */
class ClubAchievement extends Model implements StaplerableInterface
{
    use StaplerEloquentTrait;
    use SoftDeletes;

    protected $table = 'club_achievement';

    // fillable via mass assignment
    protected $fillable = [
        'title',
        'description',
        'date',
        'year',
        'month',
        'day',
        'picture',
    ];

    protected $dates = ['deleted_at'];

    /**
     * Constructor
     *
     * @return Club
     */
    public function __construct(array $attributes = array())
    {
        $this->hasAttachedFile('picture', [
            'styles' => [
                'singleView' => '840', # resize then crop
            ],
            'url' => '/uploaded/clubachievement/:id/:attachment/:style-:secure_hash.:extension',
            'default_url' => '/default_images/clubachievement/default-:attachment-:style.jpg',
        ]);

        parent::__construct($attributes);
    }

    /**
     * The club this achievement belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function club()
    {
        return $this->belongsTo('Vereinsleben\Club', 'club_id');
    }

    public function getFormattedDateWithOptionalFields()
    {
        if (is_null($this->month) || trim($this->month) === '') {
            return "{$this->year}";
        } else if (is_null($this->day) || trim($this->day) === '') {
            $date = \DateTime::createFromFormat('!m', $this->month);
            $month = $date->format('F');
            return "{$month} {$this->year}";
        } else {
            return "{$this->day}.{$this->month}.{$this->year}";
        }
    }
}
