<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Vereinsleben\ClubMappingHistory
 *
 * @property int $id
 * @property int $legacy_club_id
 * @property int $new_club_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\ClubMappingHistory onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\ClubMappingHistory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\ClubMappingHistory whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\ClubMappingHistory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\ClubMappingHistory whereLegacyClubId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\ClubMappingHistory whereNewClubId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\ClubMappingHistory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\ClubMappingHistory withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\ClubMappingHistory withoutTrashed()
 * @mixin \Eloquent
 */
class ClubMappingHistory extends Model
{
    use SoftDeletes;

    protected $table = 'club_mapping_history';
    
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $fillable = [
        'legacy_club_id',
        'new_club_id'
    ];
}
