<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ClubUser extends Pivot
{
    //
    public function member(){
        return $this->belongsTo('Vereinsleben\Member');
    }

}
