<?php


namespace Vereinsleben\Console\Commands;

use DB;
use Illuminate\Console\Command;
use Vereinsleben\News;
use Vereinsleben\Role;
use Vereinsleben\State;


class ChangeClubOwnerToAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'change the club_owner to club_admin ! there is no club_owner anymore';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $type = $this->argument('type');
        $this->info($type);
        switch ($type){
            case 'changeowner':
                $this->changeOwner();
                break;

            case 'newsfederalstate':
                $this->newsAssignFederalstate();
                break;
        }
    }

    function changeOwner()
    {
        $ownerRoleId = Role::where('constant', 'CLUB_OWNER')->firstOrFail()->id;
        $adminRoleId = Role::where('constant', 'CLUB_ADMIN')->firstOrFail()->id;

        DB::table('user_club_role')->where('role_id', $ownerRoleId)
          ->update(['role_id' => $adminRoleId]);
    }

    public function newsAssignFederalstate()
    {
        $allNews = News::all();
        $states = State::all();
        foreach ($allNews as $news){
            $news->states()->detach();
            foreach ($states as $state){
                $news->states()->save($state);
            }
        }

    }
}
