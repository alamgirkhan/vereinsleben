<?php

namespace Vereinsleben\Console\Commands;

use Illuminate\Console\Command;
use Vereinsleben\UserCheck;

class CheckForFakeMailAddresses extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:fake-mails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->checkDomain('fyii.de');
        /*$users = UserCheck::orderBy('id', 'desc')->get();
        $domain_list = $fake_user_list = array();

        foreach( $users as $user ) {
            $domain = explode('@', $user->email)[1];
            $domain_list[$domain][$user->id] = $user;
        }

        foreach( $domain_list as $domain => $user_list ) {
            if( $this->checkDomain($domain) ) {
                foreach( $user_list as $user_id => $user ) {
                    $fake_user_list[] = $user_id;
                }
            }
        }

        $this->info('Es wurden' . count($domain_list) . 'Fake-Mail-Adressen gefunden.');

        foreach ($domain_list as $user_id => $domain) {
            $user_votes_list[$user_id] = array();
        }*/
    }

    /**
     * Check $domain over mogelmail.de for fake
     *
     * @param  string $domain
     * @return boolean
     */
    protected function checkDomain($domain)
    {
        $url = 'https://www.mogelmail.de/q/' . $domain;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true );

        $isFake = intval(curl_exec($curl));

        $info = curl_getinfo($curl);

        curl_close($curl);

        var_dump($info, CURLINFO_PRIVATE);

        return $isFake;
    }
}
