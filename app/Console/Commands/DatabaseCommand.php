<?php

namespace Vereinsleben\Console\Commands;

use Illuminate\Console\Command;
use Vereinsleben\News;
use Vereinsleben\State;

class DatabaseCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'news';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


        $this->newsAssignFederalstate();
        //
    }

    public function newsAssignFederalstate(){

        $allNews = News::all();
        $states = State::all();
        foreach ($allNews as $news){
            $news->states()->detach();
            foreach ($states as $state){
                    $news->states()->save($state);
            }
        }

    }
}
