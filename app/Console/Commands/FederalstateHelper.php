<?php

namespace Vereinsleben\Console\Commands;

use Dompdf\Exception;
use Vereinsleben\Services\GeocoderHelper;
use Vereinsleben\User;
use Illuminate\Console\Command;
use Geocoder;

class FederalstateHelper extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'correct:federal-state {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'correct the federal state by zip code of the city and it is for user, club and bandage';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $type = $this->argument('type');
        switch ($type){
            case 'users':
                $this->correctUsers();
                break;
            case 'clubs':
                $this->correctClubs();
                break;
            case 'bandages':
                $this->correctBandages();
                break;
            default :
                $this->info('please choose users , clubs or brandages');
        }
    }

    /**
     *
     */
    public function correctUsers()
    {
        User::chunk(100, function($users){
            foreach ($users as $user){
                if ($user->city && $user->city != '' && $user->federal_state == ""){
                    try{
                        $geocodeHelper = new GeocoderHelper('Germany');
                        $geocodeHelper->setCity($user->zip, $user->city);
                        $this->info($user->username);
                        $user->federal_state = $geocodeHelper->getFederalStateInGerman();
                        $user->update();

                    } catch (\Exception $e){
                        $this->error($e->getMessage());
                        $this->info($e->getMessage());
                    }
                }
            }
        });
    }

    public function correctClubs()
    {

        $this->info('I m in clubs');

    }

    public function correctBandages()
    {

        $this->info('I m in bandages');

    }
}
