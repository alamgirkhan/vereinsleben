<?php

namespace Vereinsleben\Console\Commands;

use Illuminate\Console\Command;

use Log;
use Geocoder;
use Vereinsleben\Event;
use Vereinsleben\Helpers\GeocodeHelper;

class GeocodeEvents extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'geocode:event';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks events coordinates, fetch and update geo-coordinates if necessary';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Fetching events without coordinates');
        $events = Event::whereNull('lat')->orWhereNull('lng')->latest()->get();
        $this->info('Look up and store event\'s coordinates');

        $bar = $this->output->createProgressBar($events->count());

        foreach ($events as $event) {
            $coordinates = GeocodeHelper::geocodeEntity($event);

            $event->lat = $coordinates['lat'];
            $event->lng = $coordinates['lng'];

            $event->save();

            $bar->advance();
        }

        $bar->finish();
        $this->info('Geocoding of events has finished!');

    }
}
