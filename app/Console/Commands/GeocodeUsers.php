<?php

namespace Vereinsleben\Console\Commands;

use Illuminate\Console\Command;

use Log;
use Geocoder;
use Vereinsleben\User;
use Vereinsleben\Helpers\GeocodeHelper;

class GeocodeUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'geocode:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks users coordinates, fetch and update geo-coordinates if necessary';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Fetching users without coordinates');
        $users = User::whereNull('lat')->orWhereNull('lng')->latest()->get();
        $this->info('Look up and store user\'s coordinates');

        $bar = $this->output->createProgressBar($users->count());

        foreach ($users as $user) {
            $coordinates = GeocodeHelper::geocodeEntity($user);
            
            $user->lat = $coordinates['lat'];
            $user->lng = $coordinates['lng'];
            
            $user->save();

            $bar->advance();
        }

        $bar->finish();
        $this->info('Geocoding of users has finished!');

    }
}
