<?php

namespace Vereinsleben\Console\Commands;

use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;
use Vereinsleben\KeyWord;

class ImportClubStateCity extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clubStateCity:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Club State/City To Database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
	    // Excel processing
	    $fName = 'Nachfrageanalyse-Vereinsleben.de.xlsx';
	    $rows = Excel::load('storage/app/tmp/' . $fName)->all()->toArray();
	    $sheetDouble1 = [];$sheetDouble2 = [];$sheetDouble3 = [];
	    $sheetDouble4 = [];$sheetDouble5 = [];$sheetDouble6 = [];
	    $sheetDouble7 = [];$sheetDouble8 = [];$sheetDouble9 = [];
	    $sheetDouble10 = [];$sheetDouble11 = [];$sheetDouble12 = [];
	    foreach ($rows as $key => $items) {
		    if ($key === 1) {
			    foreach ($items as $item) {
				    if ($item[1] > 0) {
					    $sheetDouble1[0][] = $item;
				    }
			    }
		    } else if ($key === 2) {
			    foreach ($items as $item) {
				    if ($item[1] > 0) {
					    $sheetDouble1[1][] = $item;
				    }
			    }
		    } else if ($key === 3) {
			    foreach ($items as $item) {
				    if ($item[1] > 0) {
					    $sheetDouble2[0][] = $item;
				    }
			    }
		    } else if ($key === 4) {
			    foreach ($items as $item) {
				    if ($item[1] > 0) {
					    $sheetDouble2[1][] = $item;
				    }
			    }
		    } else if ($key === 5) {
			    foreach ($items as $item) {
				    if ($item[1] > 0) {
					    $sheetDouble3[0][] = $item;
				    }
			    }
		    } else if ($key === 6) {
			    foreach ($items as $item) {
				    if ($item[1] > 0) {
					    $sheetDouble3[1][] = $item;
				    }
			    }
		    } else if ($key === 7) {
			    foreach ($items as $item) {
				    if ($item[1] > 0) {
					    $sheetDouble4[0][] = $item;
				    }
			    }
		    } else if ($key === 8) {
			    foreach ($items as $item) {
				    if ($item[1] > 0) {
					    $sheetDouble4[1][] = $item;
				    }
			    }
		    } else if ($key === 9) {
			    foreach ($items as $item) {
				    if ($item[1] > 0) {
					    $sheetDouble5[0][] = $item;
				    }
			    }
		    } else if ($key === 10) {
			    foreach ($items as $item) {
				    if ($item[1] > 0) {
					    $sheetDouble5[1][] = $item;
				    }
			    }
		    } else if ($key === 11) {
			    foreach ($items as $item) {
				    if ($item[1] > 0) {
					    $sheetDouble6[0][] = $item;
				    }
			    }
		    } else if ($key === 12) {
			    foreach ($items as $item) {
				    if ($item[1] > 0) {
					    $sheetDouble6[1][] = $item;
				    }
			    }
		    } else if ($key === 13) {
			    foreach ($items as $item) {
				    if ($item[1] > 0) {
					    $sheetDouble7[0][] = $item;
				    }
			    }
		    } else if ($key === 14) {
			    foreach ($items as $item) {
				    if ($item[1] > 0) {
					    $sheetDouble7[1][] = $item;
				    }
			    }
		    } else if ($key === 15) {
			    foreach ($items as $item) {
				    if ($item[1] > 0) {
					    $sheetDouble8[0][] = $item;
				    }
			    }
		    } else if ($key === 16) {
			    foreach ($items as $item) {
				    if ($item[1] > 0) {
					    $sheetDouble8[1][] = $item;
				    }
			    }
		    } else if ($key === 17) {
			    foreach ($items as $item) {
				    if ($item[1] > 0) {
					    $sheetDouble9[0][] = $item;
				    }
			    }
		    } else if ($key === 18) {
			    foreach ($items as $item) {
				    if ($item[1] > 0) {
					    $sheetDouble9[1][] = $item;
				    }
			    }
		    } else if ($key === 19) {
			    foreach ($items as $item) {
				    if ($item[1] > 0) {
					    $sheetDouble10[0][] = $item;
				    }
			    }
		    } else if ($key === 20) {
			    foreach ($items as $item) {
				    if ($item[1] > 0) {
					    $sheetDouble10[1][] = $item;
				    }
			    }
		    } else if ($key === 21) {
			    foreach ($items as $item) {
				    if ($item[1] > 0) {
					    $sheetDouble11[0][] = $item;
				    }
			    }
		    } else if ($key === 22) {
			    foreach ($items as $item) {
				    if ($item[1] > 0) {
					    $sheetDouble11[1][] = $item;
				    }
			    }
		    } else if ($key === 23) {
			    foreach ($items as $item) {
				    if ($item[1] > 0) {
					    $sheetDouble12[0][] = $item;
				    }
			    }
		    } else if ($key === 24) {
			    foreach ($items as $item) {
				    if ($item[1] > 0) {
					    $sheetDouble12[1][] = $item;
				    }
			    }
		    }
	    }
	
	    // remove all data in table before insert DB
	    KeyWord::truncate();
	
	    // ----- check math sheetDouble and save DB ----
	    // process "Verein + Bundesland & Bundesland + Verein" tab
	    if (isset($sheetDouble1[0]) && isset($sheetDouble1[1])) {
		    foreach ($sheetDouble1[0] as $row0) {
			    $str1 = explode(" ", $row0[0]);
			    if (count($str1) === 4) {
				    $newStr1 = strtolower($str1[2] . '-' . $str1[3] . '_' . $str1[0] . '-' . $str1[1]);
			    } else if (count($str1) === 3) {
				    if (strpos($str1[2], 'vereine') !== false || strpos($str1[2], 'verein') !== false) {
					    $str1[1] = $str1[1] . '-' . $str1[2];
					    unset($str1[2]);
					    $newStr1 = str_replace(' ', '-', strtolower($str1[0]  . '_' . $str1[1]));
				    } else {
					    if (strpos($str1[0], 'verein') !== false || strpos($str1[0], 'verein') !== false) {
						    $newStr1 = str_replace(' ', '-', strtolower($str1[2] . '-' . $str1[1]  . '_' . $str1[0]));
					    } else {
						    $newStr1 = str_replace(' ', '-', strtolower($str1[2] . '_' . $str1[0] . '-' . $str1[1]));
					    }
				    }
			    } else if (count($str1) === 2) {
				    if(strpos($str1[0], 'vereine') !== false || strpos($str1[0], 'verein') !== false) {
					    $newStr1 = $str1[1] . '_' . $str1[0];
				    } else {
					    $newStr1 = $str1[0] . '_' . $str1[1];
				    }
			    }
			    foreach ($sheetDouble1[1] as $row1) {
				    $str2 = explode(" ", $row1[0]);
				    if (count($str2) === 4) {
					    $newStr2 = strtolower($str2[0] . '-' . $str2[1] . '_' . $str2[2] . '-' . $str2[3]);
				    } else if (count($str2) === 3) {
					    if (strpos($str2[2], 'vereine') !== false || strpos($str2[2], 'verein') !== false) {
						    $str2[1] = $str2[1] . '-' . $str2[2];
						    unset($str2[2]);
						    $newStr2 = str_replace(' ', '-', strtolower($str2[0]  . '_' . $str2[1]));
					    } else {
						    if (strpos($str2[0], 'verein') !== false || strpos($str2[0], 'verein') !== false) {
							    $newStr2 = str_replace(' ', '-', strtolower($str2[2] . '-' . $str2[1]  . '_' . $str2[0]));
						    } else {
							    $newStr2 = str_replace(' ', '-', strtolower($str2[0] . '-' . $str2[1] . '_' . $str2[2]));
						    }
					    }
				    } else if (count($str2) === 2) {
					    if(strpos($str2[0], 'vereine') !== false || strpos($str2[0], 'verein') !== false) {
						    $newStr2 = strtolower($str2[1] . '_' . $str2[0]);
					    } else {
						    $newStr2 = strtolower($str2[0] . '_' . $str2[1]);
					    }
				    }
				    if ($newStr1 === $newStr2) {
					    $this->saveDataSheet($newStr1, $row0, $row1);
				    }
			    }
		    }
	    }
	    
	    // process "Sportverein + Städte & Stätde + Sportverein" tab
	    if (isset($sheetDouble2[0]) && isset($sheetDouble2[1])) {
		    $this->processSheet($sheetDouble2[0], $sheetDouble2[1]);
	    }
	
	    // process "Fußball + Verein + Städte & Städte + Fußball + Verein" tab
	    if (isset($sheetDouble3[0]) && isset($sheetDouble3[1])) {
		    foreach ($sheetDouble3[0] as $row0) {
			    $str1 = explode(" ", $row0[0]);
			    if(count($str1) === 4) {
				    $newStr1 = strtolower($str1[1] . '-' . $str1[2] . '-' . $str1[3] . '_' . $str1[0]);
			    } else if(count($str1) === 3) {
				    $newStr1 = strtolower($str1[1] . '-' . $str1[2] . '_' . $str1[0]);
			    } else if(count($str1) === 2) {
				    $newStr1 = strtolower($str1[1] . '_' . $str1[0]);
			    }
			    foreach ($sheetDouble3[1] as $row1) {
				    $str2 = explode(" ", $row1[0] );
				    if (count($str2) === 5) {
					    $newStr2 = strtolower($str2[0] . '-' . $str2[1] . '-' . $str2[2] . '_' . $str2[3] . '-' . $str2[4]);
				    } else if (count($str2) === 4) {
					    $newStr2 = strtolower($str2[0] . '-' . $str2[1] . '-' . $str2[2] . '_' . $str2[3]);
				    } else if (count($str2) === 3) {
					    $newStr2 = strtolower($str2[0] . '_' . $str2[1] . '-' . $str2[2]);
				    } else if (count($str2) === 2) {
					    $newStr2 = strtolower($str2[0] . '_' . $str2[1]);
				    }
				    if ($newStr1 === $newStr2) {
					    $this->saveDataSheet($newStr1, $row0, $row1);
				    }
			    }
		    }
	    }
	    
	    // process "Schützenverein + Städte & Städte + Schützenverein" tab
	    if (isset($sheetDouble4[0]) && isset($sheetDouble4[1])) {
		    $this->processSheet($sheetDouble4[0], $sheetDouble4[1]);
	    }
	
	    // process "Turnverein + Städte & Städte + Turnverein" tab
	    if (isset($sheetDouble5[0]) && isset($sheetDouble5[1])) {
		    $this->processSheet($sheetDouble5[0], $sheetDouble5[1]);
	    }
	
	    // process "Golfverein + Städte & Städte + Golfverein" tab
	    if (isset($sheetDouble6[0]) && isset($sheetDouble6[1])) {
		    foreach ($sheetDouble6[0] as $row0) {
			    $str1 = explode(" ", $row0[0]);
			    if(count($str1) === 4) {
				    $newStr1 = strtolower($str1[1] . '-' . $str1[2] . '-' . $str1[3] . '_' . $str1[0]);
			    } else if(count($str1) === 3) {
				    $newStr1 = strtolower($str1[1] . '-' . $str1[2] . '_' . $str1[0]);
			    } else if(count($str1) === 2) {
				    $newStr1 = strtolower($str1[1] . '_' . $str1[0]);
			    }
			    foreach ($sheetDouble6[1] as $row1) {
				    $str2 = explode(" ", $row1[0] );
				    if (count($str2) === 5) {
					    $newStr2 = strtolower($str2[0] . '-' . $str2[1] . '-' . $str2[2] . '_' . $str2[3] . '-' . $str2[4]);
				    } else if (count($str2) === 4) {
					    $newStr2 = strtolower($str2[0] . '-' . $str2[1] . '_' . $str2[2] . '-' . $str2[3]);
				    } else if (count($str2) === 3) {
					    $newStr2 = strtolower($str2[0] . '_' . $str2[1] . '-' . $str2[2]);
				    } else if (count($str2) === 2) {
					    $newStr2 = strtolower($str2[0] . '_' . $str2[1]);
				    }
				    if ($newStr1 === $newStr2) {
					    $this->saveDataSheet($newStr1, $row0, $row1);
				    }
			    }
		    }
	    }
	
	    // process "Handballverein + Städte & Städte + Handballverein" tab
	    if (isset($sheetDouble7[0]) && isset($sheetDouble7[1])) {
		    $this->processSheet($sheetDouble7[0], $sheetDouble7[1]);
	    }
	
	    // process "Tischtennisverein + Städte & Städte + Tischtennisverein" tab
	    if (isset($sheetDouble8[0]) && isset($sheetDouble8[1])) {
		    $this->processSheet($sheetDouble8[0], $sheetDouble8[1]);
	    }
	
	    // process "Basketball + Städte & Städte + Basketball" tab
	    if (isset($sheetDouble9[0]) && isset($sheetDouble9[1])) {
		    foreach ($sheetDouble9[0] as $row0) {
			    $str1 = explode(" ", $row0[0]);
			
			    if (count($str1) === 4) {
				    if (strpos($str1[0], 'vereine') !== false || strpos($str1[0], 'verein') !== false) {
					    $newStr1 = $str1[1] . '-' . $str1[2] . '-' . $str1[3] . '_' . $str1[0];
				    } else {
					    $newStr1 = $str1[2] . '-' . $str1[3] . '_' . $str1[0] . '-' . $str1[1];
				    }
			    } else if (count($str1) === 3) {
				    if (strpos($str1[2], 'vereine') !== false || strpos($str1[2], 'verein') !== false) {
					    $str1[1] = $str1[1] . '-' . $str1[2];
					    unset($str1[2]);
					    $newStr1 = str_replace(' ', '-', strtolower($str1[0]  . '_' . $str1[1]));
				    } else {
					    if (strpos($str1[0], 'verein') !== false || strpos($str1[0], 'verein') !== false) {
						    $newStr1 = str_replace(' ', '-', strtolower($str1[2] . '-' . $str1[1]  . '_' . $str1[0]));
					    } else {
						    $newStr1 = str_replace(' ', '-', strtolower($str1[2] . '_' . $str1[0] . '-' . $str1[1]));
					    }
				    }
			    } else if (count($str1) === 2) {
				    if(strpos($str1[0], 'vereine') !== false || strpos($str1[0], 'verein') !== false) {
					    $newStr1 = strtolower($str1[1] . '_' . $str1[0]);
				    } else {
					    $newStr1 = strtolower($str1[0] . '_' . $str1[1]);
				    }
			    }
			    foreach ($sheetDouble9[1] as $row1) {
				    $str2 = explode(" ", $row1[0]);
				    if (count($str2) === 4) {
					    $newStr2 = strtolower($str2[0] . '-' . $str2[1] . '_' . $str2[2] . '-' . $str2[3]);
				    } else if (count($str2) === 3) {
					    if (strpos($str2[2], 'vereine') !== false || strpos($str2[2], 'verein') !== false) {
						    $str2[1] = $str2[1] . '-' . $str2[2];
						    unset($str2[2]);
						    $newStr2 = str_replace(' ', '-', strtolower($str2[0]  . '_' . $str2[1]));
					    } else {
						    if (strpos($str2[0], 'verein') !== false || strpos($str2[0], 'verein') !== false) {
							    $newStr2 = str_replace(' ', '-', strtolower($str2[2] . '-' . $str2[1]  . '_' . $str2[0]));
						    } else {
							    $newStr2 = str_replace(' ', '-', strtolower($str2[0] . '-' . $str2[1] . '_' . $str2[2]));
						    }
					    }
				    } else if (count($str2) === 2) {
					    if(strpos($str2[0], 'vereine') !== false || strpos($str2[0], 'verein') !== false) {
						    $newStr2 = $str2[1] . '_' . $str2[0];
					    } else {
						    $newStr2 = $str2[0] . '_' . $str2[1];
					    }
				    }
				    if ($newStr1 === $newStr2) {
					    $keyword = explode('_', $newStr1);
					    // get fields to save
					    $last = array_pop($keyword);
					    $keyword = array(implode('-', $keyword), $last);
					    $location = $keyword[0];
					    $sport = $keyword[1];
					    $description = $row0[0] . ' ' . $row0[1] . ' & ' . $row1[0] . ' ' . $row1[1];
					
					    // save
					    $newKeyWord = new KeyWord();
					    $newKeyWord->sport = $sport;
					    $newKeyWord->location = $location;
					    $newKeyWord->description = $description;
					    $newKeyWord->save();
				    }
			    }
		    }
	    }
	
	    // process "Schwimmverein + Städte & Städte + Schwimmverein" tab
	    if (isset($sheetDouble10[0]) && isset($sheetDouble10[1])) {
		    $this->processSheet($sheetDouble10[0], $sheetDouble10[1]);
	    }
	
	    // process "Tennisverein + Städte & Städte + Tennisverein" tab
	    if (isset($sheetDouble11[0]) && isset($sheetDouble11[1])) {
		    $this->processSheet($sheetDouble11[0], $sheetDouble11[1]);
	    }
	
	    // process "Reitverein + Städte & Städte + Reitverein" tab
	    if (isset($sheetDouble12[0]) && isset($sheetDouble12[1])) {
	      $this->processSheet($sheetDouble12[0], $sheetDouble12[1]);
	    }
	
	    $this->info('Done.');
    }
    
    public function processSheet($sheet1, $sheet2) {
	    foreach ($sheet1 as $row0) {
		    $str1 = explode(" ", $row0[0]);
		    if(count($str1) === 4) {
			    $newStr1 = strtolower($str1[1] . '-' . $str1[2] . '-' . $str1[3] . '_' . $str1[0]);
		    } else if(count($str1) === 3) {
			    $newStr1 = strtolower($str1[1] . '-' . $str1[2] . '_' . $str1[0]);
		    } else if(count($str1) === 2) {
			    $newStr1 = strtolower($str1[1] . '_' . $str1[0]);
		    }
		    foreach ($sheet2 as $row1) {
			    $str2 = explode(" ", $row1[0] );
			    if (count($str2) === 5) {
				    $newStr2 = strtolower($str2[0] . '-' . $str2[1] . '-' . $str2[2] . '_' . $str2[3] . '-' . $str2[4]);
			    } else if (count($str2) === 4) {
				    $newStr2 = strtolower($str2[0] . '-' . $str2[1] . '-' . $str2[2] . '_' . $str2[3]);
			    } else if (count($str2) === 3) {
				    $newStr2 = strtolower($str2[0] . '-' . $str2[1] . '_' . $str2[2]);
			    } else if (count($str2) === 2) {
				    $newStr2 = strtolower($str2[0] . '_' . $str2[1]);
			    }
			    if ($newStr1 === $newStr2) {
			      $this->saveDataSheet($newStr1, $row0, $row1);
			    }
		    }
	    }
    }
    
    public function saveDataSheet($string, $row0, $row1) {
	    $keyword = explode('_', $string);
	    // get fields to save
	    $last = array_pop($keyword);
	    $keyword = array(implode('-', $keyword), $last);
	    $location = $keyword[0];
	    $sport = $keyword[1];
	    $description = $row0[0] . ' ' . $row0[1] . ' & ' . $row1[0] . ' ' . $row1[1];
	
	    // save
	    $newKeyWord = new KeyWord();
	    $newKeyWord->sport = $sport;
	    $newKeyWord->location = $location;
	    $newKeyWord->description = $description;
	    $newKeyWord->save();
    }
}
