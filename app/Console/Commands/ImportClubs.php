<?php

namespace Vereinsleben\Console\Commands;

use Illuminate\Console\Command;
use Vereinsleben\Club;
use Vereinsleben\SocialLink;
use Vereinsleben\Sport;
use Vereinsleben\State;
use Validator;

class ImportClubs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'club:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->importClubs();

    }

    public function importClubs()
    {
        $filename = 'clubs.csv';
        $file = storage_path('app/tmp/' . $filename);
        $this->info($file);

        $delimiter = '|';
        if (($handle = fopen($file, 'r')) !== false){
            $header = fgetcsv($handle, 1000, $delimiter);
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false){
                $data = array_combine($header, $row);
                $federalState = $this->convert($data['Bundesland']);

                $club = new Club();
                $club->name = $this->convert($data['Name']);
                $club->slug = $this->generateSlug($club, $club->name);
                $club->street = $this->convert($data['Strasse']);
                $club->city = $this->convert($data['Ort']);
                $club->zip = $this->convert($data['PLZ']);
                $club->federal_state = $federalState;
                $club->published = 1;
                $club->save();

                $state = State::where('name', $federalState)->first();

                if ($state != null){
                    $club->states()->save($state);
                }

                $sports = explode(',', $this->convert($data['Sportarten']));
                if (count($sports) > 0){
                    $this->storeSports($club, $sports);
                }


                $this->saveDomainSocial($data, $club);
                unset($date);
            }
            fclose($handle);
        }


    }

    private function generateSlug($club, $clubName){
        $slug = str_slug($clubName, '-');

        $validator = Validator::make(['slug' => $slug], [
            'slug' => 'required|min:3|unique:club',
        ]);

        if ($validator->fails()){
            $slug = str_slug($club->name . '-' . rand(100, 999), '-');
        }

        return $slug;
    }

    /**
     * @param $str
     * @return string
     */
    private function convert($str)
    {
        return iconv("Windows-1252", "UTF-8", $str);
    }

    /**
     * @param Club $club
     * @param array $sports
     */
    private function storeSports(Club $club, array $sports)
    {
        $sportIds = [];
        $sportRules = [
            'title' => ['required', 'min:2', 'max:255'],
        ];
        foreach ($sports as $key){
            $key = trim($key);
            if (is_numeric($key) && Sport::find($key) !== null){ // TODO: check later if the sport is universal or belongs to the club?
                $sportIds[] = $key;
            } else{
                $validator = Validator::make(['title' => $key], $sportRules);

                if (!$validator->fails()){
                    $sport = Sport::where('title', $key)->first();
                    if ($sport === null){
                        $sport = new Sport([
                            'title' => $key
                        ]);
                    }
                    $sport->save();
                    $sportIds[] = $sport->id;
                }
            }
        }
        $club->sports()->sync($sportIds);
    }

    /**
     * @param $data
     * @param $club
     */
    public function saveDomainSocial($data, $club)
    {
        if ($data['Internetadresse'] != ''){
            $link = new SocialLink();
            $link->link_type = 'web';
            $link->url = $data['Internetadresse'];
            if (!preg_match('/^https?:\/\//', $link->url)){
                $link->url = "http://" . $link->url;
            }
            $club->socialLinks()->save($link);
        } else{
            $link = $club->socialLinks()->where('link_type', 'web')->first();
            if (!is_null($link)){
                $link->delete();
            }
        }
    }
}
