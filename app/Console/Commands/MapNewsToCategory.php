<?php

namespace Vereinsleben\Console\Commands;

use Illuminate\Console\Command;

use Vereinsleben\News;

class MapNewsToCategory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'news:map';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Map existing news with categories';

    /**
     * Map the given news by their slug with provided category ids
     *
     * @var array
     */
    protected $mapping = [
        '2016-02-26-baskets-school' => 40,
        '2015-11-23-baskets-school' => 40,
        '2016-04-19-euer-tag-unter-loewen' => 39,
        '2016-04-05-fussballzauber' => 31,
        '2015-09-06-bewegt-durchs-leben-gehen' => 23,
        '2016-02-01-ferien-am-ort' => 30,
        '2015-12-09-silvester-im-schnee' => 30,
        '2015-10-12-gemeinsam-kicken' => 2,
        '2015-07-29-der-tv-urbar' => 2,
        '2015-08-31-unsere-grosse-aktion' => 2,
        '2015-08-18-der-fc-urbar' => 2,
        '2015-08-10-der-tc-oberwerth' => 2,
        '2015-07-22-ringtennis-in-der-schule-e-v' => 2,
        '2015-08-17-der-src-heimbach-weis' => 2,
        '2015-08-13-der-bsv-koblenz' => 2,
        '2015-07-31-drachenboot-beim-psv-koblenz' => 2,
        '2015-07-21-mission-aufstieg' => 2,
        '2015-07-23-die-fighting-farmers-montabaur' => 2,
        '2015-08-11-das-billard-team-fab' => 2,
        '2015-08-07-der-aero-club-koblenz' => 2,
        '2015-08-03-der-tv-urbar' => 2,
        '2015-08-25-tag-1-im-forum' => 2,
        '2015-08-05-der-sv-niederwerth' => 2,
        '2015-11-09-erfolgreich-unterwegs' => 2,
        '2015-08-03-aufloesung-unseres-gewinnspiel' => 16,
        '2015-06-08-heike-henkel' => 16,
        '2015-06-24-sabrina-mockenhaupt' => 16,
        '2015-07-13-jochen-hecht' => 16,
        '2015-08-14-daniel-schueler' => 16,
        '2015-12-06-weihnachten-mit-bibi-tina' => 41,
        '2015-12-03-unsere-wochenendtipps' => 41,
        '2015-07-30-unsere-wochenendtipps' => 41,
        '2016-05-12-unsere-wochenendtipps' => 41,
        '2016-07-047-deutscher-eck-cup' => 41,
        '2016-04-22-unsere-wochenendtipps' => 41,
        '2015-09-10-unsere-wochenendtipps' => 41,
        '2015-11-12-unsere-wochenend-tipps' => 41,
        '2015-12-10-unsere-wochenend-tipps' => 41,
        '2016-06-30-unsere-wochenendtipps' => 41,
        '2016-01-21-unsere-wochenendtipps' => 41,
        '2016-06-02-unsere-wochenendtipps' => 41,
        '2015-08-07-unsere-wochenendtipps' => 41,
        '2015-07-23-unsere-wochenendtipps' => 41,
        '2015-07-02-unsere-wochenendtipps' => 41,
        '2015-10-29-unsere-wochenend-tipps' => 41,
        '2015-11-19-unsere-wochenend-tipps' => 41,
        '2016-04-13-unsere-wochenendtipps' => 41,
        '2015-07-10-unsere-wochenendtipps' => 41,
        '2016-05-24-unsere-wochenendtipps' => 41,
        '2015-07-16-unsere-wochenendtipps' => 41,
        '2015-10-01-unsere-wochenend-tipps' => 41,
        '2016-04-28-unsere-wochenendtipps' => 41,
        '2015-08-13-unsere-wochenendtipps' => 41,
        '2015-10-15-unsere-wochenend-tipps' => 41,
        '2015-11-05-unsere-wochenend-tipps' => 41,
        '2015-11-25-unsere-wochenend-tipps' => 41,
        '2016-05-17-unsere-wochenendtipps' => 41,
        '2016-07-08-unsere-wochenendtipps' => 41,
        '2015-10-08-unsere-wochenend-tipps' => 41,
        '2016-06-23-unsere-wochenendtipps' => 41,
        '2016-06-08-unsere-wochenendtipps' => 41,
        '2016-07-14-unsere-wochenendtipps' => 41,
        '2015-10-22-unsere-wochenend-tipps' => 41,
        '2016-05-04-unsere-wochenendtipps' => 41,
        '2015-07-17-aufpassen-auf-portemonnaie-co' => 8,
        '2015-09-23-sportabzeichen-fuer-alle' => 8,
        '2015-08-05-die-lotto-elf' => 8,
        '2015-11-02-das-problem-im-handball' => 6,
        '2015-08-21-die-entstehung-des-modernen-sports' => 6,
        '2015-09-03-randsportart-basketball' => 6,
        '2015-08-18-sport-im-buergertum-des-mittelalters' => 6,
        '2015-08-10-sport-im-roemischen-reich' => 6,
        '2015-10-20-sport-mit-einem-hocker' => 6,
        '2015-09-01-transferwahnsinn-in-england' => 6,
        '2015-08-05-sport-in-der-antike' => 6,
        '2015-09-18-sonne-strand-und-davis-cup' => 6,
        '2015-07-14-der-hammerwurf' => 6,
        '2015-08-18-der-dfb-und-seine-aufgaben' => 6,
        '2015-11-24-trampolin-wm-in-daenemark' => 6,
        '2015-07-30-sport-gegen-den-antisemitismus' => 6,
        '2015-09-01-sport-in-der-weimarer-republik' => 6,
        '2015-12-28-kein-schnee-kein-wintersport' => 6,
        '2015-10-29-auslaenderanteil-in-eishockeymannschaften' => 6,
        '2015-08-13-das-mittelalterliche-turnier' => 6,
        '2015-12-04-frauen-handball-wm' => 6,
        '2015-09-16-top-facts-zur-europa-league' => 6,
        '2015-12-09-der-heimliche-uefa-boss' => 6,
        '2015-10-23-turn-wm-in-glasgow' => 6,
        '2015-12-22-der-boxing-day' => 6,
        '2015-08-05-brust-kraul-schmetterling' => 6,
        '2015-12-21-ballsportarten-im-fokus' => 6,
        '2015-12-28-die-zeit-der-niederlagen' => 6,
        '2015-12-07-jahresrueckblick-teil-3' => 6,
        '2015-11-28-der-winter-findet-seine-meister' => 6,
        '2015-12-14-jahresrueckblick-teil-3' => 6,
        '2015-11-23-ein-turbulenter-jahresstart' => 6,
        '2015-08-21-der-tus-diedesfeld' => 2,
        '2015-08-19-der-tus-diedesfeld' => 2,
        '2016-02-16-ehre-wem-ehre-gebuehrt' => 22,
        '2016-07-04-auf-geht-s-nach-rio' => 22,
        '2016-06-28-rheinland-pfalz-goes-rio' => 22,
        '2016-06-21-mehr-geld-fuer-den-sport' => 22,
        '2016-05-3011-lsb-partnerlauf' => 22,
        '2016-04-12-sport-guide-fuer-fluechtlinge' => 22,
        '2016-01-22-oeffentliches-wahlhearing-in-mainz' => 22,
        '2015-11-23-die-sportler-des-jahres-2015' => 22,
        '2015-10-12-der-39-landessportball' => 22,
        '2015-09-18-der-landessportball-2015' => 22,
        '2015-08-10-projekt-einfach-gemeinsam' => 22,
        '2016-07-14-vereine-aus-der-region-praemiert' => 22,
        '2016-01-11-rennfahrer-timo-bernhard-zu-gast-im-sportmuseum-hauenstein' => 9,
        '2016-01-13-vereine-auch-im-motorsport-wichtig' => 9,
        '2016-02-17-sport-und-medizin' => 9,
        '2016-03-16-spobau-2016' => 9,
        '2015-11-24-auszeichnung-fuer-greenkeeper-der-vereine' => 9,
        '2016-03-11-wir-machen-s-gemeinsam' => 23,
        '2016-04-21-olympia-fuer-alle' => 23,
        '2015-07-16-ruhmreiche-sportler' => 23,
        '2016-06-13-neue-fuehrung-fuer-den-sbr' => 23,
        '2016-04-25-sport-bei-fritz-walter-wetter' => 23,
        '2016-03-11-fit-in-der-firma' => 15,
        '2015-11-05-lebenslauf-sammelt-rund-30-000' => 15,
        '2016-05-23-die-landeshauptstadt-als-marathon-mekka' => 15,
        '2016-02-29-sport-baut-bruecken' => 15,
        '2016-08-02-die-vereinsleben-de-medallienjagd' => 16,
        '2016-08-02-kampfsportler-unterstuetzen' => 2,
        '2016-08-01-fortuna-sportgeraete-mehr' => 43,
        '2016-07-28-sportverletzungen' => 44,
        '2016-08-02-allergien-und-sport' => 44,
    ];

    protected $oldCategories = [
        13,
        28,
        29,
        35,
        36,
        27,
    ];

    protected $publishNews = [
        '2016-08-02-allergien-und-sport',
        '2016-07-28-sportverletzungen',
        '2016-08-01-fortuna-sportgeraete-mehr'
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Updating news with category ids');
        $bar = $this->output->createProgressBar(count($this->mapping));

        foreach ($this->mapping as $slug => $categoryId) {
            News::where('slug', $slug)->update(['category_id' => $categoryId]);
            $bar->advance();
        }

        $bar->finish();

        $this->info('Updating news within old categories');

        News::whereIn('category_id', $this->oldCategories)->update(['state' => News::STATE_HIDDEN]);

        // publish news
        News::whereIn('slug', $this->publishNews)->update(['state' => News::STATE_PUBLISHED]);

        $this->info('Done.');

    }
}
