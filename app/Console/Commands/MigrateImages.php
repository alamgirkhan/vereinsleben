<?php

namespace Vereinsleben\Console\Commands;

use Illuminate\Console\Command;
use File;

class MigrateImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate-images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrates ... whait, images!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Todo: doku, original files are getting deleted

        // TODO: mode 1 gets the pictures from S3 and stores it in the local folder before, mode 2 skips this and accesses the folder directly.

        $mode = 'local'; # remote or local

        $tmpFolder = sys_get_temp_dir().'/vereinsleben_migrate-images_'.uniqid();
        mkdir($tmpFolder);

        try {
            $newses = \Vereinsleben\News::all();
            foreach ($newses as $news) {
                echo "Starting with news {$news->id}\n";

                if ($news->m_image_original !== null) {
                    $file = $this->getAttachmentSourceFile($mode, $news->m_image_original, $tmpFolder);
                    if ($file === null) {
                        echo "!! file for news {$news->id} NOT FOUND\n";
                    } else {
                        try {
                            $news->update(['main_image' => $file]);
                            echo "updated news {$news->id}\n";
                        } catch (\Imagine\Exception\InvalidArgumentException $e) {
                            echo "ERROR while processing news id {$news->id} / {$news->m_image_original}: {$e->getMessage()}\n";
                        }
                    }
                }
            }

            $clubs = \Vereinsleben\Club::all();
            foreach ($clubs as $club) {
                echo "Starting with club {$club->slug}\n";

                if ($club->m_avatar_original !== null) {
                    $file = $this->getAttachmentSourceFile($mode, $club->m_avatar_original, $tmpFolder);
                    if ($file === null) {
                        echo "!! file for avatar of {$club->slug} NOT FOUND\n";
                    } else {
                        $club->update(['avatar' => $file]);
                        echo "updated avatar of {$club->slug}\n";
                    }
                }
                if ($club->m_header_original !== null) {
                    $file = $this->getAttachmentSourceFile($mode, $club->m_header_original, $tmpFolder);
                    if ($file === null) {
                        echo "!! file for header of {$club->slug} NOT FOUND\n";
                    } else {
                        $club->update(['header' => $file]);
                        echo "updated header of {$club->slug}\n";
                    }
                }
            }

            $images = \Vereinsleben\Image::all();
            foreach ($images as $image) {
                echo "Starting with image {$image->id}\n";

                if ($image->url !== null) {
                    $file = $this->getAttachmentSourceFile($mode, $image->url, $tmpFolder);
                    if ($file === null) {
                        echo "!! file for image {$image->id} NOT FOUND\n";
                    } else {
                        try {
                            $image->update(['picture' => $file]);
                            echo "updated image {$image->id}\n";
                        } catch (\Imagine\Exception\InvalidArgumentException $e) {
                            echo "ERROR while processing image id {$image->id} / {$image->url}: {$e->getMessage()}\n";
                        }
                    }
                }
            }

        } catch (Exception $e) {
            var_dump($e); var_dump($club); var_dump($image);
        }

        # rm -rf $tmpFolder
    }

    protected function getAttachmentSourceFile($mode, $url, $tmpFolder)
    {
        $tmpStorage = $tmpFolder.'/'.uniqid('', true);
        #$isDir = is_dir($tmpStorage) ? 'true' : 'false';
        #echo "creating temp folder {$tmpStorage} - exists: {$isDir}\n";
        mkdir($tmpStorage);

        if ($mode === 'local') {
            $localAttachmentStorage = public_path().'/uploaded/migration/s3/';

            $path = str_replace('https://s3.eu-central-1.amazonaws.com/vereinsleben/uploads/', $localAttachmentStorage, $url);
            $tmpPath = $tmpStorage.File::basename($path);

            if (!File::exists($path)) {
                return null;
            }
            File::copy($path, $tmpPath); // copy to tmp path because the
        } elseif ($mode === 'remote') {
            $file = file_get_contents($url);
            $tmpPath = $tmpStorage.\File::basename($url);
            file_put_contents($tmpPath, $file);
        }
        $file = new \Symfony\Component\HttpFoundation\File\File($tmpPath, basename($tmpPath));
        return $file;
    }
}
