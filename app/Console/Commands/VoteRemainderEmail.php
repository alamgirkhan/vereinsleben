<?php

namespace Vereinsleben\Console\Commands;

use Carbon\Carbon;
use DB;
use Hash;
use Illuminate\Console\Command;
use Mail;
use Vereinsleben\Content;
use Vereinsleben\User;
use Vereinsleben\VoteUser;

class VoteRemainderEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vote:reminder_email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send remainder email for the user who want to vote!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = Carbon::now()->format('Y-m-d');
        $vote = DB::table('vote')
                  ->where('period_start', '<=', $now)
                  ->where('period_end', '>=', $now)
                  ->first();
        if ($vote != null){
            $vote->id;

            $usersToRemaind = VoteUser::where('vote_id', $vote->id)
                                      ->where('reminder', 1)
                                      ->get();
            $imprint = Content::select('content')->where('slug', 'imprint')->first();
            foreach ($usersToRemaind as $userToRemaind){
                try{
                    $user = User::find($userToRemaind->user_id);
                } catch (NoResultException $e){
                    $user = null;
                }
                if ($user != null){
                    $hash = Hash::make(rand(0, time()));
                    $userToRemaind->deleted_token = $hash;
                    $userToRemaind->save();
                    Mail::send('vote.emails.remember', ['user' => $user, 'deleted_token' => $hash, 'imprint' => $imprint], function($message) use ($user, $hash){
                        $message->to($user->email, $user->firstname)->subject('Verein des Monats // Abstimmen nicht vergessen');
                    });
                }

            }
        }
    }
}
