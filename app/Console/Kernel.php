<?php

namespace Vereinsleben\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Psy\Command\Command;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,
        Commands\MigrateImages::class,
        Commands\MapNewsToCategory::class,
        Commands\GeocodeUsers::class,
        Commands\GeocodeEvents::class,
        Commands\CheckForFakeMailAddresses::class,
        Commands\FederalstateHelper::class,
        Commands\ChangeClubOwnerToAdmin::class,
        Commands\DatabaseCommand::class,
        Commands\ImportClubs::class,
        Commands\VoteRemainderEmail::class,
        Commands\ImportClubStateCity::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }
}
