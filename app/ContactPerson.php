<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;


class ContactPerson extends Model
{
    protected $table = 'contact_persion';
    
		/**
		 * The club this event belongs to.
		 *
		 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
		 */
		public function bandage()
		{
			return $this->belongsTo('Vereinsleben\Bandage', 'bandage_id');
		}
}
