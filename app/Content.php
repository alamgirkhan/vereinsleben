<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use \DB;

use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait as StaplerEloquentTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Content extends Model
{
    protected $table = 'content';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function meta()
    {
        return $this->morphOne('Vereinsleben\Meta', 'metaable');
    }

}