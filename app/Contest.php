<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;

class Contest extends Model
{
    protected $table = 'contest';


    protected $fillable = [
        'club_id',
        'clubname',
        'clubslug',
        'user_id',
        'username',
        'user_firstname',
        'user_lastname',
        'created_at',
        'updated_at',
    ];

    protected $dates = ['created_at', 'updated_at'];

}
