<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    const STATE_NEW     = 'NEW';
    const STATE_DELETED = 'DELETED';

    protected $table = 'departments';
    protected $morphClass = 'department';

    protected $fillable = [
        'name',
        'description',
        'club_id',
        'sport_id',
        'user_id',
        'created_at',
        'updated_at',
    ];

    protected $dates = ['created_at', 'updated_at'];


    public function club()
    {
        return $this->belongsTo('Vereinsleben\Club', 'club_id');
    }

    public function sport()
    {
        return $this->belongsTo('Vereinsleben\Club', 'sport_id');
    }

    public function user()
    {
        return $this->belongsTo('Vereinsleben\Club', 'user_id');
    }

    public function contacts()
    {
        return $this->morphtomany('Vereinsleben\User', 'contactable');
    }

    public function teams()
    {
        return $this->hasMany('Vereinsleben\Team', 'department_id');
    }
}