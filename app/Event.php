<?php

namespace Vereinsleben;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait as StaplerEloquentTrait;

/**
 * Vereinsleben\Event
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $content
 * @property string|null $content_raw
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $published_at
 * @property string|null $published_from
 * @property string|null $published_to
 * @property int|null $club_id
 * @property int|null $user_id
 * @property int|null $gallery_id
 * @property string|null $schedule_begin
 * @property string|null $schedule_end
 * @property string|null $street
 * @property string|null $house_number
 * @property string|null $city
 * @property string|null $zip
 * @property string|null $state
 * @property float|null $lat
 * @property float|null $lng
 * @property string|null $m_id
 * @property string|null $m_gallery
 * @property string $m_club
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Vereinsleben\Club|null $club
 * @property-read \Illuminate\Database\Eloquent\Collection|\Vereinsleben\Image[] $images
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Event closest($lat, $lon, $distance = 50)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\Event onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Event public()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Event upcoming()
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Event whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Event whereClubId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Event whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Event whereContentRaw($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Event whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Event whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Event whereGalleryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Event whereHouseNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Event whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Event whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Event whereLng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Event whereMClub($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Event whereMGallery($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Event whereMId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Event wherePublishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Event wherePublishedFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Event wherePublishedTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Event whereScheduleBegin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Event whereScheduleEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Event whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Event whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Event whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Event whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Event whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Event whereZip($value)
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\Event withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\Event withoutTrashed()
 * @mixin \Eloquent
 */
class Event extends Model
{
    use SoftDeletes;

    const STATE_NEW     = 'NEW';
    const STATE_DELETED = 'DELETED';

    protected $table = 'event';
    protected $morphClass = 'event';

    protected $fillable = [
        'title',
        'content',
        'content_raw',
        'published_from',
        'published_to',
        'schedule_begin',
        'schedule_end',
        'schedule_end',
        'street',
        'house_number',
        'city',
        'zip',
        'state',
        'sport',
        'location',
        'price',
        'sender'
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * The club this event belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function club()
    {
        return $this->belongsTo('Vereinsleben\Club', 'club_id');
    }
	
		/**
		 * The club this event belongs to.
		 *
		 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
		 */
		public function bandage()
		{
			return $this->belongsTo('Vereinsleben\Bandage', 'bandage_id');
		}

    /**
     * The images of this post
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function images()
    {
        return $this->morphMany('Vereinsleben\Image', 'imageable');
    }

    /**
     * A scope to retrieve public events.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublic($query)
    {
        return $query
            ->whereNotNull('published_at')
            ->where(function ($query) {
                $query
                    ->where(function ($query) {
                        $query
                            ->where('published_from', '<=', Carbon::now())
                            ->where('published_to', '>=', Carbon::now());
                    })
                    ->orWhere(function ($query) {
                        $query
                            ->where('published_from', '<=', Carbon::now())
                            ->whereNull('published_to');
                    })
                    ->orWhere(function ($query) {
                        $query
                            ->where('published_to', '>=', Carbon::now())
                            ->whereNull('published_from');
                    })
                    ->orWhere(function ($query) {
                        $query
                            ->whereNull('published_to')
                            ->whereNull('published_from');
                    });
            });
    }

    /**
     * A scope to retrieve upcoming events.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUpcoming($query)
    {
        return $query->where('schedule_end', '>=', Carbon::now())->orderBy('schedule_begin', 'asc');
    }

    /**
     * Returns the full concatenated address.
     *
     * @return string
     */
    public function fullAddress()
    {
        if ($this->street !== null && trim($this->street) != '') {
            return sprintf('%s %s, %s %s',
                $this->street,
                $this->house_number,
                $this->zip,
                $this->city
            );
        } else if ($this->city !== null && trim($this->city) != '') {
            return sprintf('%s %s',
                $this->zip,
                $this->city
            );
        } else {
            return '';
        }
    }


    /**
     * Scope a query to fetch the closest events
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeClosest($query, $lat, $lon, $distance = 50)
    {
        // TODO: optimize? http://stackoverflow.com/a/29931224/650203

        return $query->select(DB::raw('*, (
          6371 * acos (
            cos ( radians(' . (float)$lat . ') )
            * cos( radians( lat ) )
            * cos( radians( lng ) - radians(' . (float)$lon . ') )
            + sin ( radians(' . (float)$lat . ') )
            * sin( radians( lat ) )
          )
        ) AS distance'))->having('distance', '<=', $distance)->orderBy('distance', 'asc');
    }


    function scopeByState($query, $state)
    {
        return $query->whereHas('states', function($q) use ($state){
            $q->where('state_id', $state->id);
        });
    }


    public function states()
    {
        return $this->morphToMany('Vereinsleben\State', 'stateable');
    }

    /**
     * Search the events with keyword
     *
     * @return $query
     */
    public function scopeSearchByKeyword($query, $keywords)
    {
//        $query->select('id', 'title', 'sub_title');
        if (empty($keywords)){

            $query = $query->all();

        }else{
            foreach($keywords as $keyword){
                $query->where('schedule_end', '>=', Carbon::today())
                      ->where(function($query) use ($keyword) {
	                      $query->orWhere(DB::raw("REPLACE(title, 'ß', 'ss')"), 'LIKE', DB::raw("concat('%', REPLACE('".$keyword."', 'ß', 'ss'), '%')"));
	                      $query->orWhere(DB::raw("REPLACE(content, 'ß', 'ss')"), 'LIKE', DB::raw("concat('%', REPLACE('".$keyword."', 'ß', 'ss'), '%')"));
	                      $query->orWhere(DB::raw("REPLACE(content_raw, 'ß', 'ss')"), 'LIKE', DB::raw("concat('%', REPLACE('".$keyword."', 'ß', 'ss'), '%')"));
                      });
            }
        }
        return $query;
    }
}
