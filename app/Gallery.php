<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;

/**
 * Vereinsleben\Gallery
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $description
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $m_id
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Gallery whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Gallery whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Gallery whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Gallery whereMId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Gallery whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Gallery whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Gallery extends Model
{
    protected $table = 'gallery';
}
