<?php

namespace Vereinsleben\Helpers;

use DB;
use Vereinsleben\User;
use Illuminate\Pagination\LengthAwarePaginator;

class CustomPaginator
{

    public $data;
    public $page;
    public $size;
    public $extraOffset = 0;

    public function paginate()
    {
        $currentPage = $this->page;
        $offSet = (($currentPage * $this->size) - $this->size) + $this->extraOffset;
        $currentPageSearchResults = array_slice($this->data, $offSet, $this->size, true);
        $result = new LengthAwarePaginator($currentPageSearchResults, (count($this->data) - $this->extraOffset), $this->size, $currentPage);

        return $result;
    }
}