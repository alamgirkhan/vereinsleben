<?php

namespace Vereinsleben\Helpers;

use Geocoder;
use Illuminate\Support\Facades\Log;

class GeocodeHelper
{
    /**
     * Set lat/lng on entity (with fullAdress() model method)
     *
     * @param \Illuminate\Database\Eloquent\Model $entity
     * @return array
     */
    static public function geocodeEntity($entity)
    {
        try {
            $result = [
                'lat' => null,
                'lng' => null
            ];

            $geocoder = Geocoder::geocode($entity->fullAddress());

            if ($geocoder !== null
                && $geocoder->get()->first()->getCoordinates()->getLatitude() !== 0
                &&  $geocoder->get()->first()->getCoordinates()->getLongitude() !== 0) {

                $result = [
                    'lat' => $geocoder->get()->first()->getCoordinates()->getLatitude(),
                    'lng' => $geocoder->get()->first()->getCoordinates()->getLongitude()
                ];
            }

            return $result;
        } catch (\Exception $exception) {
            Log::error($exception->getMessage(), [
                'at' => 'Vereinsleben\Helpers\GeocodeHelper',
                'entity' => $entity
            ]);
        }

    }
}