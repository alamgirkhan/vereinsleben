<?php

namespace Vereinsleben\Helpers;

use DB;
use Vereinsleben\User;
use Illuminate\Pagination\LengthAwarePaginator;

class LocationHelper
{
    public $entity;
    public $distance = 25;

    public function find()
    {
        $user = User::find(\Auth::id());
        $result = null;

        if ($user->lat and $user->lng) {
            $nearBy = DB::table($this->entity)
                ->select(DB::raw('id,( 6371 * acos (cos ( radians(' . (float)$user->lat . '))* cos( radians( lat ))* cos( radians( lng ) - radians(' . (float)$user->lng . '))+ sin ( radians(' . (float)$user->lat . '))* sin( radians( lat )))) AS distance'))
                ->whereNull('deleted_at')
                ->having('distance', '<=', $this->distance)
                ->orderBy('distance', 'asc');
            
            $result = (count($nearBy->get()) > 3) ? $nearBy : null;
        }

        return $result;
    }
}