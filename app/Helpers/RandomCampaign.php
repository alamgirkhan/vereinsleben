<?php
	namespace Vereinsleben\Helpers;
	
	use Carbon\Carbon;
	use Vereinsleben\Campaign;
	use Vereinsleben\Http\Controllers\BaseController;
	use Vereinsleben\Option;
	
	class RandomCampaign extends BaseController
	{
		public function getRandomForStream()
		{
			$state = $this->getFederalState();
			$campaigns = Campaign::where('state', '=', Campaign::STATE_PUBLISHED)
				->where('published_to', '>=', Carbon::today())
				->where('banner_horizontal', '<>' , '')
				->whereHas('states', function($query) use ($state) {
					$query->where('state_id', $state->id);
				})->get();
			
			$option = new Option;
			$bannerSpacingStream = $option->get('banner_spacing_in_the_stream');
			
			if (count($campaigns) > 0) {
				$campaigns->shuffle();
				$shuffled = $campaigns->shuffle();
				$campaignsShuffled = $shuffled->all();
				
				$responsive = [
					"campaigns" => $campaignsShuffled,
					"bannerSpacingStream" => $bannerSpacingStream,
				];
				return $responsive;
			} else {
				return null;
			}
		}
		
		public function getRandomForNews() {
			$state = $this->getFederalState();
			$campaign = Campaign::inRandomOrder()->where('state', '=', Campaign::STATE_PUBLISHED)
																						->where('published_to', '>=', Carbon::today())
																						->whereHas('states', function($query) use ($state) {
																							$query->where('state_id', $state->id);
																						})->first();
			if (isset($campaign)) {
				return $campaign;
			} else {
				return null;
			}
		}
		
		public static function instance()
		{
			return new RandomCampaign();
		}
	}