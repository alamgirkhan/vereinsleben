<?php

namespace Vereinsleben\Helpers;

use Carbon\Carbon;
use Dompdf\Exception;
use Illuminate\Support\Facades\Log;
use Vereinsleben\User;
use \GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;

class VenusNewsletter
{
    /**
     * Config
     *
     * @var array
     */
    protected $config = [];

    /**
     * Constructor
     *
     * @param array $config
     * @return VenusNewsletter
     */
    public function __construct(array $config)
    {
        $requiredKeys = array('auth_uri', 'auth_username', 'auth_client_id', 'auth_password', 'import_uri');

        foreach ($requiredKeys as $key) {
            if (!isset($config[$key])) {
                throw new Exception("Required config key {$key} not provided!");
            }
        }

        $this->config = $config;
    }

    /**
     * Subscribe a user to the newsletter via the VENUS API.
     *
     * @param User $user
     * @return boolean
     */
    public function subscribeUser(User $user)
    {
        $client = new HttpClient();

        // authenticate
        $authPayload = [
            'username'  => $this->config['auth_username'],
            'client_id' => $this->config['auth_client_id'],
            'password'  => $this->config['auth_password'],
        ];

        $subscribeResponse = null;

        // POST to auth uri, get body.access_token
        // can throw GuzzleHttp\Exception\TransferException
        $authResponse = $client->request('POST', $this->config['auth_uri'], ['json' => $authPayload]);

        if (!$authResponse->getStatusCode() === 200) {
            throw new Exception("Error while authenticating against the VENUS API.");
        }

        $authResponseJSON = json_decode($authResponse->getBody(), true);

        if ($authResponseJSON === null || !isset($authResponseJSON['access_token'])) {
            throw new Exception("Error while authenticating against the VENUS API - the response did not contain the token.");
        }

        $sendData = $this->getVenusUserdata($user);
        try {
            $subscribeResponse = $client->request('POST', $this->config['import_uri'], [
                'headers' => [
                    'Authorization' => "Bearer {$authResponseJSON['access_token']}",
                    'Content-Type'  => 'application/json; charset=utf-8',
                    'Accept'        => 'application/json',
                ],
                'json'    => $sendData
            ]);

            if ($subscribeResponse->getStatusCode() !== 201) {
                throw new Exception("Error while subscribing {$user->email}: {$subscribeResponse->getBody()}");
            }
        } catch(ClientException $e) {
            Log::error($e->getCode().': '.$e->getMessage().' ('.$user->email.')');
        }

        return true;
    }

    public function updateUser(User $user)
    {
        $client = new HttpClient();

        // authenticate
        $authPayload = [
            'username'  => $this->config['auth_username'],
            'client_id' => $this->config['auth_client_id'],
            'password'  => $this->config['auth_password'],
        ];

        $subscribeResponse = null;

        // POST to auth uri, get body.access_token
        // can throw GuzzleHttp\Exception\TransferException
        $authResponse = $client->request('POST', $this->config['auth_uri'], ['json' => $authPayload]);

        if (!$authResponse->getStatusCode() === 200) {
            throw new Exception("Error while authenticating against the VENUS API.");
        }

        $authResponseJSON = json_decode($authResponse->getBody(), true);

        if ($authResponseJSON === null || !isset($authResponseJSON['access_token'])) {
            throw new Exception("Error while authenticating against the VENUS API - the response did not contain the token.");
        }


        $sendData = $this->getVenusUserdata($user);
        try {
            $subscribeResponse = $client->request('PUT', $this->config['import_uri'], [
                'headers' => [
                    'Authorization' => "Bearer {$authResponseJSON['access_token']}",
                    'Content-Type'  => 'application/json; charset=utf-8',
                    'Accept'        => 'application/json',
                ],
                'json'    => $sendData
            ]);

            if ($subscribeResponse->getStatusCode() !== 200) {
                throw new Exception("Error while updating {$user->email}: {$subscribeResponse->getBody()}");
            }
        } catch(ClientException $e) {
            Log::error($e->getCode().': '.$e->getMessage().' ('.$user->email.')');
        }

        return true;
    }

    /**
     * Returns VENUS-compatible userdata for the given user.
     *
     * @param User $user
     * @return array
     */
    protected function getVenusUserdata(User $user)
    {
        $data = [];
        $user_optin = Carbon::parse($user->OptinDate)->format('Y-m-d\TH:i:s');
        $optindate = Carbon::parse($user->created_at)->format('Y-m-d\TH:i:s');

        $data['Firstname'] = $user->firstname;
        $data['Lastname'] = $user->lastname;
        $data['Email'] = $user->email;
        $data['Gender'] = ($user->gender === 'male') ? 'männlich' : (($user->gender === 'female') ? 'weiblich' : '');
        $data['Birthdate'] = ($user->birthday !== null) ? date('d.m.Y', strtotime($user->birthday)) : '';
        $data['Phone'] = '';
        $data['MobilePhone'] = '';
        $data['Address'] = ($user->street !== null && $user->street !== '') ? trim($user->street . ' ' . $user->house_number) : '';
        $data['City'] = $user->city;
        $data['ZipCode'] = $user->zip;
        $data['Country'] = 'Deutschland';
        $data['Newsletter'] = ($user->newsletter == 1 || $user->newsletter == '1') ? true : false;
        /*$data['Newsletter'] = ($user->newsletter == 1 || $user->newsletter == '1') ? 1 : 0;*/
        $data['Peoplegroup'] = 'Vereinsleben';
        $data['OptinDate'] = ($user->OptinDate !== null) ? $user_optin : $optindate;

        return $data;
    }
}