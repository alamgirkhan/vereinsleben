<?php

namespace Vereinsleben\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Storage;
use Illuminate\Support\Facades\Log;
use Vereinsleben\Club;
use Vereinsleben\Http\Controllers\Controller;
use Vereinsleben\User;
use Vereinsleben\UserCheck;
use Vereinsleben\UserVoteNominee;
use Vereinsleben\Vote;
use Vereinsleben\VoteNominee;

class CheckingController extends Controller
{
    protected $verified_domain_list = array(
        'gmail.com', 'googlemail.com',
        'freenet.de',
        'gmx.de', 'gmx.net',
        'aol.com',
        'telekom.de', 't-online.de',
        'web.de',
    );

    protected $fake_domain_list = [];

    protected $fake_user_list = array();

    public function fake($getOutput = false)
    {
        $users = UserCheck::withTrashed()->orderBy('id', 'desc')->get();

        $fake_mail_file = 'fake-mail-list.txt';
        if( Storage::exists($fake_mail_file) ) {
            $fake_mail_list = str_replace('""', '', Storage::get($fake_mail_file));
            $this->fake_domain_list = explode("\n", $fake_mail_list);
        }

        $fake_user_file = 'fake-user-list.txt';
        if( Storage::exists($fake_user_file)) {
            $fake_user_list = str_replace('""', '', Storage::get($fake_user_file));
            $this->fake_user_list = explode("\n", $fake_user_list);
        }

        $domain_list = $fake_list = array();

        foreach( $users as $user ) {
            $domain = explode('@', $user->email)[1];
            if(in_array($domain, str_replace("\r", "", $this->fake_domain_list))) {
                $domain_list[$domain][$user->id] = $user;
            }
            if(in_array($user->email, str_replace("\r", "", $this->fake_user_list))) {
                $domain_list['specific'][$user->id] = $user;
            }
        }

        foreach( $domain_list as $domain => $user_list ) {
            foreach ( $user_list as $user_id => $user) {
                if( !$user->has_fake_email ) {
                    $user->has_fake_email = 1;
                    $user->save();
                }
                $fake_list[$user_id] = $user;
                $votes = UserVoteNominee::where('user_id', $user->id)->get();

                $vote_count[$user_id] = count($votes);

                foreach( $votes as $vote ) {
                    $vote->is_fake = 1;
                    $vote->save();
                }
            }
        }

        Log::info('fakeuserlist count: ' . count($fake_list));

        if($getOutput) {
            return $fake_list;
        } else {
            return view('admin.user.fake-check', ['fake_user_list' => $fake_list, 'vote_count' => $vote_count, 'fake_file' => $fake_mail_file, 'file_exists' => Storage::exists($fake_mail_file)]);
        }
    }

    public function deleteUser($data, $deleteRelatedClubs = false)
    {
        $deleteRelatedClubs = (($deleteRelatedClubs === 'true') ? true : false);

        $user = User::where( function($query) use ($data) {
            $query->where('email', 'LIKE', '%'. $data.'%')
                ->orWhere('fullname', 'LIKE', '%'.$data.'%')
                ->orWhere('username', 'LIKE', '%'.$data.'%');
        })
            ->latest()
            ->first();

        $deletedClubs = [];
        if( $deleteRelatedClubs && $user instanceof User ) {
            $clubs = $user->clubs;



            if( count($clubs) > 0 ) {
                foreach( $clubs as $club ) {
                    $deletedClubs[] = $club;
                    $club->delete();
                }
            }
        }

        if( !( $user instanceof User ) ) {
            return view('admin.user.deletion', ['value' => $data])->with('flash-error', 'Der Benutzer konnte nicht gefunden werden.');
        }

        $deletedUser = $user;

        $user->deleted = 1;
        $user->save();
        $user->delete();

        return view('admin.user.deletion', ['user' => $deletedUser, 'value' => $data, 'clubs' => $deletedClubs])->with('flash-success', 'Der Benutzer wurde erfolgreich gelöscht.');
    }

    public function deleteFake()
    {

        $user_list = $this->fake(true);

        foreach($user_list as $user_id => $user) {
            $user->deleted = 1;
            $user->save();
            $user->delete();
        }

    }

    public function verifyUser($email = '')
    {
        $user = User::where('email', 'LIKE', '%'. $email .'%')->latest()->first();

        $user->verified = 1;
        $user->save();

        return view('admin.user.verification', ['user' => $user])->with('flash-success', 'Der Benutzer wurde erfolgreich verifiziert.');
    }

    public function proofDoublet()
    {
        $doublets = DB::table("user")
            ->select(DB::raw("COUNT(id), fullname, LCASE(fullname) AS lc_name"))
            ->whereRaw("fullname != ''")
            ->groupBy("lc_name")
            ->havingRaw("COUNT(id) > 1")
            ->get();

        foreach($doublets as $doublet) {
            User::whereRaw('lower(fullname) = ?', [strtolower($doublet->fullname)])->update(['has_fake_email' => 1]);

            UserVoteNominee::where('user_id', $doublet->id)->update(['is_fake' => 1]);
        }

        dd(count($doublets));
    }

    public function addVotesToIdNominee($vote_id = null, $nominee_id = null, $votes = 200)
    {
        $nominee = Vote::orderBy('id', 'desc')->first()->voteNominees()->where('id', '=', (int)$nominee_id)->first();

        /** @var VoteNominee $pre_count */
        $pre_count = $nominee->users()->count();
        for($i = 0; $i < $votes; $i++) {
            $vote = new UserVoteNominee([
                'user_id' =>  5183,
                'vote_nominee_id' => $nominee->id
            ]);
            $vote->save();
        }

        return \Redirect::route('vote.show', [
            'vote' => Vote::with('voteNominees')->findOrFail($vote_id)
        ]);
    }
}
