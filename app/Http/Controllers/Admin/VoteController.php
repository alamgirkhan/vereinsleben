<?php

namespace Vereinsleben\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Vereinsleben\Http\Controllers\BaseController;
use Vereinsleben\Http\Requests;
use Vereinsleben\Http\Controllers\Controller;
use Vereinsleben\User;
use Vereinsleben\UserVoteNominee;
use Vereinsleben\Vote;
use Vereinsleben\VoteNominee;

class VoteController extends BaseController
{
    public function __construct(Request $request){
        parent::__construct($request);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $votes = Vote::orderBy('id', 'desc')->get();

        return view('admin.vote.index', compact('votes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.vote.detail', ['vote' => new Vote()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $vote = Vote::create($request->except('_token'));

        return redirect()->to('admin/vote/' . $vote->id)->with('flash-success', 'Die Abstimmung wurde erfolgreich angelegt');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
//        $vote = Vote::with('voteNominees')->findOrFail($id);

        $vote = Vote::with(['voteNominees' => function($query){
            $query->orderBy('count_voted', 'desc');
        }])->findOrFail($id);

        foreach($vote->voteNominees as $voteNominee) {
            $count = UserVoteNominee::where('vote_nominee_id', $voteNominee->id)->count();
            $voteNominee->count_voted = $count;

            $count_cleared = UserVoteNominee::where('vote_nominee_id', $voteNominee->id)->where('is_fake', 0)->count();
            $voteNominee->count_cleared = $count_cleared;

            $voteNominee->save();
        }

        return view('admin.vote.detail', [
            'vote' => $vote
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vote = Vote::findOrFail($id);
        $vote->update($request->all());

        return redirect()->to('admin/vote/' . $vote->id)->with('flash-success', 'Die Abstimmung wurde erfolgreich aktualisiert');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function storeWinner(Request $request, $id)
    {
        $vote = Vote::findOrFail($id);
        $vote->create($request->all());
    }
}
