<?php

namespace Vereinsleben\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Vereinsleben\Http\Requests;
use Vereinsleben\Http\Requests\VoteNomineeFormRequest;
use Vereinsleben\Http\Controllers\Controller;
use Vereinsleben\Vote;
use Vereinsleben\VoteNominee;

class VoteNomineeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($voteId)
    {
        return view('admin.vote.nominee', [
            'vote' => Vote::findOrFail($voteId)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $voteId
     * @return \Illuminate\Http\Response
     */
    public function store(VoteNomineeFormRequest $request, $voteId)
    {
        $voteNominee = new VoteNominee();

        $voteNominee->vote_id = $voteId;
        $voteNominee->voteable_type = 'Club';
        $voteNominee->voteable_id = $request['club_id'];
        $voteNominee->project = $request['project'];
        $voteNominee->project_description = $request['project_description'];

        $voteNominee->save();

        return redirect()->to('admin/vote/' . $voteId . '/nominee/' . $voteNominee->id . '/show')->with('flash-success', 'Der Teilnehmer wurde erfolgreich hinzugefügt');;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $voteId
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($voteId, $id)
    {
        return view('admin.vote.nominee', [
            'vote' => Vote::findOrFail($voteId),
            'nominee' => VoteNominee::findOrFail($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $voteId
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $voteId, $id)
    {
        $vote = Vote::findOrFail($voteId);
        $voteNominee = VoteNominee::findOrFail($id);
        $voteNominee->update($request->except('_token', '_method'));

        return redirect()->to('admin/vote/' . $vote->id . '/nominee/' . $voteNominee->id . '/show')->with('flash-success', 'Der Teilnehmer wurde erfolgreich aktualisiert');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $voteId
     * @param  int $nomineeId
     * @return \Illuminate\Http\Response
     */
    public function delete($voteId, $nomineeId)
    {
        $voteNominee = VoteNominee::findOrFail($nomineeId);
        $voteNominee->delete();


        return redirect()->to('admin/vote/' . $voteId)->with('flash-success', 'Der Teilnehmer wurde erfolgreich gelöscht');
    }

    /**
     * Set winner of vote cycle
     *
     * @param  int $voteId
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function setWinner($voteId, $id)
    {
        $vote = Vote::findOrFail($voteId);
        $voteNominees = $vote->voteNominees;

        foreach ($voteNominees as $nominee) {
            if($nominee->id != $id) {
                $nominee->winner = 0;
            } else {
                if($nominee->winner) {
                    $nominee->winner = 0;
                } else {
                    $nominee->winner = 1;
                }
            }
            $nominee->save();
        }

        return redirect()->to('admin/vote/' . $voteId)->with('flash-success', 'Der Gewinner wurde erfolgreich geändert');
    }
}
