<?php

namespace Vereinsleben\Http\Controllers;

use Auth;
use Carbon\Carbon;
use DB;
use Exception;
use File;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Http\Request;
use League\Csv\Writer;
use PhpParser\Node\Stmt\If_;
use Vereinsleben\ClubAchievement;
use Vereinsleben\Event;
use Vereinsleben\Post;
use Vereinsleben\Role;
use Vereinsleben\Sport;
use Vereinsleben\state;
use Vereinsleben\Team;
use Vereinsleben\Trainingsort;
use Vereinsleben\User;
use Vereinsleben\Club;
use Vereinsleben\Member;

class AdminClubController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function clubList(Request $request)
    {
		    session(['clubListQueryString' => $request->all()]);
        $club = new Club;
        $clubs = null;

        $keyword = trim($request->input('keyword', ''));
        $email = trim($request->input('email', ''));
        $city = trim($request->input('city', ''));

        /*new filter functions*/
        $anfangOld = $request->input('anfang');
        $endeOld = $request->input('ende');
        $date_filter = trim($request->input('date_filter'));
        $sporttype = $request->input('sport', '');
        $plz_von = $request->input('von', '');
        $plz_bis = $request->input('bis', '');

        $anfang = '';
        $ende = '';

        $path = storage_path('app' . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'vlde-vereinsliste-gefiltert.csv');
        $exp_club = \File::exists($path);

        if ($anfangOld !== ''){
            $anfang = Carbon::parse($anfangOld)->format('Y-m-d');
//            $year = intval($anfang->format('Y'));
            $year = Carbon::parse($anfang)->format('Y');
        }
        if ($endeOld !== ''){
            $ende = Carbon::parse($endeOld)->format('Y-m-d');
        }


        $role = new Role;
        $adminClub = serialize($role->getByConstant(Role::CLUB_ADMIN)->id);

        if ($keyword){
            $club = $club->search($keyword);
        }
        if ($email){
            $club = $club->where('email', 'LIKE', '%' . $email . '%');
        }
        if ($city){
            $club = $club->where(function($query) use ($city){
                $query->orWhere('city', 'LIKE', '%' . $city . '%')->orWhere('federal_state', $city);
            });
        }

        if ($sporttype !== ''){
            /** Sport IDs */
            $sportids = array();
            $sporty = Sport::where('title', 'LIKE', '%' . $sporttype . '%')->get();
            foreach ($sporty as $sp){
                array_push($sportids, $sp->id);
            }
            $club = $club->whereHas('sports', function($query) use ($sportids){
                $query->whereIn('sport_id', $sportids);
            });
        }


        if ($date_filter == 'created_at'){

            if ($year < 2018){

                $club = $club->whereHas('users', function($query){
                    $query->where('user_club_role.role_id', '=', 4);
                });
            }

            if ($anfang !== '' && $ende !== '' && $year >= 2018){

                $club = $club->whereHas('users', function($query) use ($anfang, $ende, $date_filter, $adminClub){
                    $query->where('user_club_role.role_id', '=', 4)->whereBetween('user_club_role.created_at', [$anfang, $ende]);
                });

            } elseif (($anfang !== '' && $ende == '') || ($anfang == '' && $ende !== '') || ($anfang == '' && $ende == '')){
                return \Redirect::route('admin.clubs.list')->with('flash-info', 'Anfangsdatum und Enddatum müssen gefüllt sein!');
            }
        }

        if ($date_filter == 'updated_at'){
            if ($anfang !== '' && $ende !== ''){

                $club = $club->where($date_filter, '>=', $anfang)->where($date_filter, '<=', $ende);

            } else{
                return \Redirect::route('admin.clubs.list')->with('flash-info', 'Anfangsdatum und Enddatum müssen gefüllt sein!');
            }
        }

        if ($plz_von !== '' && $plz_bis !== ''){
            $club = $club->whereBetween('zip', [$plz_von, $plz_bis]);
        } elseif ($plz_von !== '' && $plz_bis == ''){
            $club = $club->where('zip', '=', $plz_von);
        }


        if ($keyword !== '' || $email !== '' || $city !== '' || $date_filter !== ''
            || $sporttype !== '' || $plz_von !== '' || $plz_bis !== ''){
            $clubs = $club->orderBy('id', 'desc')->get();
        }

        return view('admin.club.list', [
            'clubs'    => $clubs,
            'exp_club' => $exp_club,
            'query'    => [
                'keyword' => $keyword,
                'email'   => $email,
                'city'    => $city,
                'sport'   => $sporttype,
                'von'     => $plz_von,
                'bis'     => $plz_bis,
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($username)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {

        $club = Club::where('slug', $slug)->firstOrFail();

        $admins = $club->administrators()->get();
//        foreach ($admins as $user){
//                array_push($clubAdmins, $user);
//        }

        $k = 0;
        $sports = [];

        foreach ($club->teams as $team){
            if ($team->sport() != null){
                $sports[$k]['id'] = $team->sport()->first()->id;
                $sports[$k]['title'] = $team->sport()->first()->title;
                $k++;
            }
        }

        if (Auth::user()->isAdmin()){ // important - only admins can see the view

            return view('admin.club.edit', [
                'club'   => $club,
                'sports' => $sports,
                'admins' => $admins,
            ]);
        } else{
            abort(403);
        }
    }


    public function exportClubsData(Request $request)
    {

        $club = new Club;
        $role = new Role;

        $ids = json_decode($request->ids, true);
        $clubs = $club->whereIn('id', $ids)->with('teams')->with('posts')->with('events')->get();

        $clubAdmin = array();

        $filename = 'vlde-vereinsliste-gefiltert.csv';
        $filename = storage_path('app' . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . $filename);

        $writer = Writer::createFromPath($filename, "w+");
        $writer->setDelimiter(";");
        $writer->setNewline("\r\n"); //use windows line endings for compatibility with some csv libraries
        $writer->setOutputBOM(Writer::BOM_UTF8); //adding the BOM sequence on output

        // set csv first (header) row
        $writer->insertOne("\xEF\xBB\xBF");
        $writer->insertOne([
            'ID',
            'Vereinsname',
            'Straße',
            'Hausnummer',
            'PLZ',
            'Ort',
            'Bundesland',
            'Web',
            'E-Mail',
            'Administratoren',
            'Mannschaften',
            'Erfolge',
            'Posts',
            'Events',
            'übernommen am',
        ]);

        $clubs->each(function($club) use ($writer, $role, $clubAdmin){

            $club->administrators()->each(function($user) use ($clubAdmin){
                array_push($clubAdmin, $user->id);
            });

            $clubadmins = implode(" - ", $clubAdmin);

            $writer->insertOne([
                $club->id,
                $club->name,
                $club->street,
                $club->house_number,
                $club->zip,
                $club->city,
                $club->federal_state,
                $club->m_social_web,
                $club->email,
                $clubadmins,
                //                $club->departments,
                $club->teams->count(),
                $club->achievements->count(),
                $club->posts->count(),
                $club->events->count(),
                date('d.m.Y', strtotime($club->created_at)),
            ]);
        });

        return back()->withInput()->with('flash-success', "Der Export war erfolgreich!");

    }


    public function downloadClubFile()
    {

        $path = storage_path('app' . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'vlde-vereinsliste-gefiltert.csv');

        if (\File::exists($path)){

            return response()->download($path)->deleteFileAfterSend($path);
        } else{
            abort(403);
        }
    }


    public function addAdmin(Request $request)
    {


        if (Auth::user()->isAdmin() && $request){

            $role = new Role();
            $user = User::where('id', $request['userid'])->firstOrFail();
            $club = Club::where('id', $request['clubid'])->firstOrFail();
            $clubAdminId = $role->getByConstant(Role::CLUB_ADMIN)->id;

            DB::table('user_club_role')
              ->insert(['user_id' => $request['userid'], 'club_id' => $request['clubid'], 'role_id' => $clubAdminId]);

//            $club->users()
//                 ->wherePivot('user_id', $user->id)
//                 ->updateExistingPivot($user->id, ['role_id' => $clubAdminId], false);

            return back()->withInput()->with('flash-success', "Dem Verein $club->name wurde der neue Admin $user->firstname $user->lastname zugeteilt!");

        } else{
            abort(403);
        }
    }


    public function deleteAdmin($clubid, $id)
    {
        if (Auth::user()->isAdmin()){

            $role = new Role();
            $clubAdminId = $role->getByConstant(Role::CLUB_ADMIN)->id;

            DB::table('user_club_role')
              ->where('user_id', '=', $id)
              ->where('club_id', '=', $clubid)
              ->where('role_id', '=', $clubAdminId)
              ->delete();

            return back()->withInput()->with('flash-warning', "Der Admin mit der ID: $id wurde entfernt!");

        } else{
            abort(403);
        }
    }


    public function deactivate($id)
    {

        $club = Club::find($id);
        $name = $club->name;

        if (Auth::user()->isAdmin()){

            if ($club->published == 1){
                $club->published = 0;
                $club->save();

                return back()->withInput()->with('flash-success', "Der Verein $name wurde deaktiviert!");
            } elseif ($club->published == 0){
                $club->published = 1;
                $club->save();

                return back()->withInput()->with('flash-success', "Der Verein $name wurde aktiviert!");
            }
        } else{
            abort(403);
        }
    }

    public function reset($id)
    {

        $club = Club::find($id);
        $name = $club->name;


        if (Auth::user()->isAdmin()){

            $image_path = public_path() . DIRECTORY_SEPARATOR . 'uploaded' . DIRECTORY_SEPARATOR . 'club' . DIRECTORY_SEPARATOR . $club->id;

            if (File::isDirectory($image_path)){
                File::deleteDirectory($image_path, true);
                dd('Order wurde gelöscht!');
            }

            //set columns to null
            $club->update(['about'               => null, 'm_id' => null, 'm_owner' => null, 'm_sports' => null,
                           'm_achievements'      => null, 'm_social_links' => null, 'm_social_facebook' => null,
                           'm_social_twitter'    => null, 'm_social_googleplus' => null, 'm_social_youtube' => null,
                           'm_social_instagram'  => null, 'm_social_flickr' => null, 'm_header_original' => null,
                           'm_header_singleview' => null, 'header_file_name' => null, 'header_file_size' => null,
                           'header_content_type' => null, 'header_updated_at' => null, 'unowned' => null, 'views' => null]);


            //hard-delete all achievements from club
            $achievements = ClubAchievement::whereClubId($id)->get();
            foreach ($achievements as $achievement){

                $achievement->forceDelete();
            }


            //hard-delete all posts from club
            $posts = Post::whereClubId($id)->get();
            foreach ($posts as $post){
                $post->forceDelete();
            }


            //hard-delete all admins from club
            DB::table('user_club_role')
              ->where('club_id', '=', $id)
              ->where('role_id', '=', 4)
              ->delete();


            //hard-delete all events from club
            $events = Event::whereClubId($id)->get();
            foreach ($events as $event){
                $event->forceDelete();
            }


            //hard-delete all teams from club
            $teams = Team::whereClubId($id)->get();
            foreach ($teams as $team){
                $team->forceDelete();
            }


            //hard-delete all trainingsort from club
            $club->traininsorts()->detach();

            return back()->withInput()->with('flash-success', "Der Verein $name wurde zurückgesetzt!");

        } else{
            abort(403);
        }
    }

    public function mappy(Request $request)
    {

        if ($request['newclub'] == ''){
            return back()->withInput()->with('flash-error', "Mapping muss gefüllt sein!");
        }

        if (Auth::user()->isAdmin()){

            $oldclub = Club::whereId($request['oldclubid'])->firstOrFail();
            $club = Club::whereName($request['newclub'])->firstOrFail();
            $clubsports = $club->sports()->get();
            $oldclubmembers = $oldclub->members()->get();
            $oldclubfans = $oldclub->fans()->get();
            $oldclubadmins = $oldclub->administrators()->get();
            $oldclubadminreqs = $oldclub->getAdminRequests()->get();
            $oldclubsports = $oldclub->sports()->get();
            $oldclubposts = $oldclub->posts()->get();
            $sportidold = 0;
            $sportidnew = 0;
            $sportmap = false;
            $today = Carbon::now();


            //map sports from old club to new club
            foreach ($oldclubsports as $sport){

                $sportidold = $sport->id;

                foreach ($clubsports as $newsport){

                    $sportidnew = $newsport->id;

                    if ($sportidold == $sportidnew){
                        $sportmap = true;
                        break;
                    }
                }

                if ($sportmap){
                    $sportmap = false;
                    continue;
                } else{
                    $club->sports()->save($sport);
                }
            }


            //map adminrequests from old club to new club
            $role = new Role;
            $adminRequestId = $role->getByConstant(Role::CLUB_ADMIN_REQUEST)->id;

            if($oldclubadminreqs){
                foreach($oldclubadminreqs as $adminreq){
                    $club->users()->save($adminreq, ['role_id' => $adminRequestId]);
                }
            }


            //map members from old club to new club
            $memberrole = new Role;
            $mem = new Member();
            $memberActiveId = $mem->getByConstant(Member::ACTIVE)->id;
            $memberRoleId = $memberrole->getByConstant(Role::CLUB_MEMBER)->id;

            foreach ($oldclubmembers as $member){
                $club->users()->save($member, ['role_id' => $memberRoleId, 'member_id' => $memberActiveId]);
            }


            //map fans from old club to new club
            $fanrole = new Role;
            $memberfan = new Member();
            $fanRoleId = $fanrole->getByConstant(Role::CLUB_FAN)->id;
            $fanId = $memberfan->getByConstant(Member::FAN)->id;

            foreach ($oldclubfans as $fan){
                $club->users()->save($fan, ['role_id' => $fanRoleId, 'member_id' => $fanId]);
            }


            //map posts from old club to new club
            foreach ($oldclubposts as $post){
                $club->posts()->save($post);
            }


            //delete posts from old club
            if (count($oldclubposts) > 0){
                $posts = new Post;
                try{
                    $posts->where('club_id', '=', $oldclub->id)->delete();
                } catch (Exception $e){
                }
            }


            //delete members, fans and adminrequests of old club
            if ($oldclubmembers || $oldclubfans || $oldclubadminreqs){
                $oldclub->users(function($query) use ($fanRoleId, $memberRoleId, $adminRequestId){
                    $query->where('role_id', '=', $fanRoleId)
                          ->orWhere('role_id', '=', $memberRoleId)
                          ->orWhere('role_id', '=', $adminRequestId);
                })->detach();
            }


            //delete sports from old club
            if (count($oldclubsports) > 0){
                $oldclub->sports()->detach();
            }


            //delete old club
            if ($oldclub){
                try{
                    $oldclub->delete();
                } catch (Exception $e){
                }
            }

            //save mapping history
            DB::table('club_mapping_history')->insert(['legacy_club_id' => $oldclub->id, 'new_club_id' => $club->id, 'created_at' => $today, 'updated_at' => $today]);

            return back()->with('flash-success', "Der Verein $oldclub->name wurde gemappt mit $club->name!");

        } else{
            abort(403);
        }

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $club = Club::find($request->id);
        if ($request->ajax()){
            $club->deleted_at = date("Y-m-d H:i:s");
            $club->users()->detach();

            $club->save();

            return response()->json($club->deleted_at);
        } else{
            abort(403);
        }
    }
}
