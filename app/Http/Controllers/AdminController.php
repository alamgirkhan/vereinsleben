<?php

namespace Vereinsleben\Http\Controllers;

use DB;
use File;
use Geocoder;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Mail;
use Redirect;
use Storage;
use Vereinsleben\Bandage;
use Vereinsleben\Content;
use Vereinsleben\Contest;
use Vereinsleben\Event;
use Vereinsleben\Http\Requests\EventFormRequest;
use Vereinsleben\Meta;
use Vereinsleben\News;
use Vereinsleben\Image;
use Vereinsleben\NewsCategory;
use Vereinsleben\SportCareer;
use Vereinsleben\State;
use Vereinsleben\Tag;
use Vereinsleben\User;
use Vereinsleben\Club;
use Vereinsleben\Role;
use Vereinsleben\Sport;
use Vereinsleben\Http\Requests\NewsFormRequest;
use Vereinsleben\ClubMappingHistory;
use Vereinsleben\SportMappingHistory;
use Auth;
use Validator;

use Yajra\Datatables\Facades\Datatables;
use League\Csv\Writer;

class AdminController extends BaseController
{
    protected static $IMAGE_DIMENSIONS = [
        'singleview' => [
            'width' => '750',
        ],
        'startpage'  => [
            'width' => '360',
        ],
    ];

    protected $_newsUploadPath = 'uploaded/news/';

    public function __construct(Request $request)
    {
        parent::__construct($request);
        if (!Auth::check() || !Auth::user()->isAdmin()){
//            abort(403);
        }
    }

    public function index()
    {
	    $news = News::orderBy('news.created_at', 'desc')->with('user')->get();
    	
        return view('admin.index', ['news' => $news]);
        //return view('admin.index', ['news' => News::orderBy('created_at', 'desc')->get()]);
    }



    public function showExports(){

        return view('admin.export.index');
    }

    public function showSports(){

        return view('admin.sport.index');
    }



    /** Get domains of fakemail
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getFakeDomains()
    {

        try{
            $content = File::get(storage_path('app/fake-mail-list.txt'));
            $file = explode("\n", $content);

        } catch (FileNotFoundException $e){
            die("The file doesn't exist");
        }

        return view('admin.fakedomains', ['file' => $file]);
    }


    /**
     * Get all published events
     */
    public function getEventsList()
    {

        $events = new Event;

        $events = $events->public()->with('images')->orderBy('schedule_begin', 'desc')->get();

        return view('admin.event.list', [
            'events' => $events
        ]);
    }

    /** Edit Event
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editEvent($id)
    {
        $event = Event::find($id);

        $sport = new Sport;

        if (isset($event)){

            $sport = $sport->find($event->sport);

            return view('admin.event.edit', [
                'event' => $event,
                'sport' => $sport
            ]);
        } else{
            $dummy = new Event;

            return view('admin.event.edit', [
                'event' => $dummy
            ]);
        }

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws AuthorizationException
     */
    public function updateEvent(EventFormRequest $request, $id)
    {

        if ($request['sender'] == '0'){

            return \Redirect::route('admin.event.edit', $id)->with('flash-warning', 'Bitte einen Sender angeben!');

        }

        $event = Event::findOrFail($id);

        if ($request['sport'] != '' && $request['sport'] != null){

            $sport = new Sport;
            $sport = $sport->where('id', '=', $request['sport'])->firstOrFail();
            $event->sport = $sport->id;

        }

        if ($request['sender'] == '1'){

            $event->sender = 'vereinsleben.de';

        } elseif ($request['sender'] == '2'){

            if ($request['sender_id'] == '' && $request['sender_id'] == null){

                return \Redirect::route('admin.event.edit', $id)->with('flash-warning', 'Bei Verein oder Verband muss die ID angegeben werden!');

            } else{
                $club = Club::findOrFail($request['sender_id']);

                if (isset($club)){
                    $event->sender = $club->name;
                } else{
                    return \Redirect::route('admin.event.edit', $id)->with('flash-warning', 'Der Verein mit der ID' . $request['sender_id'] . 'existiert nicht!');
                }
            }
        } else{
            if ($request['sender_id'] == '' && $request['sender_id'] == null){

                return \Redirect::route('admin.event.edit', $id)->with('flash-warning', 'Bei Verein oder Verband muss die ID angegeben werden!');

            } else{
                $bandage = Bandage::findOrFail($request['sender_id']);

                if (isset($bandage)){
                    $event->sender = $bandage->name;
                } else{
                    return \Redirect::route('admin.event.edit', $id)->with('flash-warning', 'Der Verein mit der ID' . $request['sender_id'] . 'existiert nicht!');
                }
            }
        }


        $event->title = $request['title'];
        $event->content = $request['content_raw'];
        $event->content_raw = $request['content_raw'];
        $event->street = $request['street'];
        $event->house_number = $request['house_number'];
        $event->zip = $request['zip'];
        $event->city = $request['city'];
        $event->price = $request['price'];
        $event->location = $request['location'];
        $event->sender = $request['sender_id'];
        $event->schedule_begin = date('Y-m-d H:i:s', strtotime($request['schedule_begin']));
        $event->schedule_end = date('Y-m-d H:i:s', strtotime($request['schedule_end']));
        $event->published_from = !empty($request['published_from']) ? date('Y-m-d H:i:s', strtotime($request['published_from'])) : null;
        $event->published_to = !empty($request['published_to']) ? date('Y-m-d H:i:s', strtotime($request['published_to'])) : null;

        try{
            $geocoder = Geocoder::geocode($event->fullAddress());

            if ($geocoder !== null
                && $geocoder->get()->first()->getCoordinates()->getLatitude() !== 0
                && $geocoder->get()->first()->getCoordinates()->getLongitude() !== 0){

                $event->lat = $geocoder->get()->first()->getCoordinates()->getLatitude();
                $event->lng = $geocoder->get()->first()->getCoordinates()->getLongitude();
            }
        } catch (\Exception $e){

            $event->lat = null;
            $event->lng = null;

            Log::warning($e->getMessage(), [
                'during' => 'event@update',
                'event'  => $event->id
            ]);
        }

        if (!is_null($event->published_from)){
            $event->published_at = $event->published_from;
        }

        try{
            $this->authorize($event);
        } catch (AuthorizationException $e){
        }

        $event->save();

        $images = $request->file('images');
        if ($images !== null && count($images) > 0){
            foreach ($images as $imageUploaded){
                if ($imageUploaded !== null){
                    $image = new Image(['picture' => $imageUploaded]);
                    $event->images()->save($image);
                }
            }
        }

        $imagesDelete = $request->input('delete-image');
        if ($imagesDelete !== null && count($imagesDelete) > 0){
            foreach ($imagesDelete as $imageDeleteId){
                if (is_numeric($imageDeleteId)){
                    $image = $event->images()->find($imageDeleteId);
                    if ($image !== null){
                        $image->delete();
                    }
                }
            }
        }

        return \Redirect::route('admin.event.edit', $id)->with('flash-success', 'Die Veranstaltung wurde aktualisiert!');
    }


    public function storeEvent(EventFormRequest $request)
    {
        $event = new Event;

        $event->title = $request['title'];
        $event->content = $request['content_raw'];
        $event->content_raw = $request['content_raw'];
        $event->street = $request['street'];
        $event->house_number = $request['house_number'];
        $event->zip = $request['zip'];
        $event->city = $request['city'];
        $event->schedule_begin = date('Y-m-d H:i:s', strtotime($request['schedule_begin']));
        $event->schedule_end = date('Y-m-d H:i:s', strtotime($request['schedule_end']));
        $event->published_from = !empty($request['published_from']) ? date('Y-m-d H:i:s', strtotime($request['published_from'])) : null;
        $event->published_to = !empty($request['published_to']) ? date('Y-m-d H:i:s', strtotime($request['published_to'])) : null;
        $event->club_id = $request['club_id'];
        $event->state = Event::STATE_NEW;
        $event->price = $request['price'];
        $event->location = $request['location'];
        $event->sender = $request['sender_id'];

        if ($request['sport'] != '' && $request['sport'] != null){

            $sport = new Sport;
            $sport = $sport->where('id', '=', $request['sport'])->firstOrFail();
            $event->sport = $sport->id;

        }

        try{
            $geocoder = Geocoder::geocode($event->fullAddress());

            if ($geocoder !== null
                && $geocoder->get()->first()->getCoordinates()->getLatitude() !== 0
                && $geocoder->get()->first()->getCoordinates()->getLongitude() !== 0){

                $event->lat = $geocoder->get()->first()->getCoordinates()->getLatitude();
                $event->lng = $geocoder->get()->first()->getCoordinates()->getLongitude();
            }
        } catch (\Exception $e){

            $event->lat = null;
            $event->lng = null;

            Log::warning($e->getMessage(), [
                'during' => 'event@store',
                'event'  => $event->id
            ]);
        }

        if (is_null($event->published_from)){
            $event->published_at = date("Y-m-d H:i:s");
        } else{
            $event->published_at = $event->published_from;
        }

        try{
            $this->authorize($event);
        } catch (AuthorizationException $e){
        }

        $event->save();

        $images = $request->file('images');
        if ($images !== null && count($images) > 0){
            foreach ($images as $imageUploaded){
                if ($imageUploaded !== null){
                    $image = new Image(['picture' => $imageUploaded]);
                    $event->images()->save($image);
                }
            }
        }

        return \Redirect::route('admin.event.list')->with('flash-success', 'Deine Veranstaltung wurde erfolgreich gespeichert');
    }


//    public function deleteEvent(Request $request){
//
//        $event = Event::findOrFail($request->id);
//
//        $this->authorize($event);
//
//        $club = Club::whereId($event->club_id);
//
//        $event->delete();
//
////        return json_encode(['success' => true, 'item' => $event]);
//
//        return back()->withInput()->with('flash-success', "Event " . $event->name . " wurde erfolgreich entfernt!");
//
//    }
//
//
//    public function destroyEvent(Request $request)
//    {
//        $event = Event::find($request->id);
//
//        dd($event);
//
//        if ($request->ajax()){
//
//            $event->deleted_at = date("Y-m-d H:i:s");
//            $event->save();
//
////            return response()->json($event->deleted_at);
//
//            return back()->withInput()->with('flash-success', "Event " . $event->name . " wurde erfolgreich entfernt!");
//
//        } else{
//            abort(403);
//        }
//    }



    /** Add domain to fakedomain list
     * @param Request $request
     */
    public function addFakeDomain(Request $request)
    {

        $fakedomain = trim($request->input('domain', ''));

        if (Auth::user()->isAdmin()){

            if ($fakedomain){

                if (Storage::exists('fake-mail-list.txt')){

                    Storage::prepend('fake-mail-list.txt', $fakedomain);

                    return back()->withInput()->with('flash-success', "Domain wurde hinzugefügt!");
                } else{
                    return back()->withInput()->with('flash-error', "Datei wurde nicht gefunden!");
                }

            } else{
                return back()->withInput()->with('flash-error', "Domain muss gefüllt sein!");
            }
        } else{
            return back()->withInput()->with('flash-error', "Sie haben keine Berechtigung!");
        }

    }


    public function addSportArt(Request $request)
    {
        $sportTitle = $request['newsportart'];
        if (Auth::user()->isAdmin()){
            if (Sport::where('title', $sportTitle)->first() == null){
                $sport = new Sport([
                    'title' => $sportTitle
                ]);
                $sport->save();

                return back()->withInput()->with('flash-success', "Sportart wurde erfolgreich hinzugefügt!");
            } else{

                return back()->withInput()->with('flash-error', "Sportart ist schon vorhanden!");
            }
        } else{
            return back()->withInput()->with('flash-error', "Du hast keine Berechtigung!");
        }
    }

    /** Remove domain from fakedomain list
     * @param Request $request
     */
    public function deleteFakeDomain($domain)
    {

        if (Auth::user()->isAdmin()){

            try{
                $content = File::get(storage_path('app/fake-mail-list.txt'));
                $file = explode("\n", $content);


            } catch (FileNotFoundException $e){
                die("The file doesn't exist");
            }

            $newcontent = '';

            foreach ($file as $dom){

                if ($dom == $domain){
                    continue;
                } else{
                    $newcontent = $newcontent . $dom . "\n";
                }
            }


            $newcontent = trim($newcontent);

            Storage::delete('fake-mail-list.txt');
            Storage::put('fake-mail-list.txt', $newcontent);

            return back()->withInput()->with('flash-success', "Domain wurde erfolgreich entfernt!");
        } else{
            return back()->withInput()->with('flash-error', "Sie haben keine Berechtigung!");
        }
    }


    public function getContest()
    {
        $contestlist = Contest::all();

        return view('admin.dashboard.contest.all', ['contest' => $contestlist]);
    }


    public function deleteContest($id)
    {

        if (Auth::user()->isAdmin()){
            $contest = Contest::where('id', '=', $id)->first();
            $contest->delete();

            return back()->withInput()->with('flash-danger', 'Eintrag wurde gelöscht.');
        } else{

            return back()->withInput()->with('flash-error', 'Sie haben keine Berechtigung!');
        }

    }


    /** Get domains of fakemail
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getFakeUsers()
    {

        try{
            $content = File::get(storage_path('app/fake-user-list.txt'));
            $fakeusers = explode("\n", $content);

        } catch (FileNotFoundException $e){
            die("The file doesn't exist");
        }

        return view('admin.fakeusers', ['fakeusers' => $fakeusers]);
    }


    /** Add User to fakeuser list
     * @param Request $request
     */
    public function addFakeUser(Request $request)
    {

        $fakeuser = trim($request->input('email', ''));


        if (Auth::user()->isAdmin()){

            if ($fakeuser){

                if (Storage::exists('fake-user-list.txt')){

                    Storage::prepend('fake-user-list.txt', $fakeuser);

                    return back()->withInput()->with('flash-success', "User wurde hinzugefügt!");
                } else{
                    return back()->withInput()->with('flash-error', "Datei wurde nicht gefunden!");
                }

            } else{
                return back()->withInput()->with('flash-error', "Email muss gefüllt sein!");
            }
        } else{
            return back()->withInput()->with('flash-error', "Sie haben keine Berechtigung!");
        }

    }


    /** Remove user from fakeuser list
     * @param Request $request
     */
    public function deleteFakeUser($user)
    {

        if (Auth::user()->isAdmin()){

            try{
                $content = File::get(storage_path('app/fake-user-list.txt'));
                $file = explode("\n", $content);


            } catch (FileNotFoundException $e){

                die("Die Datei wurde nicht gefunden.");
            }

            $newcontent = '';

            foreach ($file as $mail){

                if ($mail == $user){
                    continue;
                } else{
                    $newcontent = $newcontent . $mail . "\n";
                }
            }

            $newcontent = trim($newcontent);
            Storage::delete('fake-user-list.txt');
            Storage::put('fake-user-list.txt', $newcontent);

            return back()->withInput()->with('flash-success', "User wurde erfolgreich entfernt!");
        } else{
            return back()->withInput()->with('flash-error', "Sie haben keine Berechtigung!");
        }
    }


    /**
     * Get all News with Position between 1 to 8 which are public
     */
    public function posNews()
    {

        if (Auth::user()->isAdmin() || Auth::user()->isContentManager()){
            $news = new News;
//            $newsMerge = collect([]);
            $newsMerge = array();
            $news = $news->whereBetween('position', [1, 8])->orderBy('position', 'asc')->get();

            if (count($news) > 0){
                foreach ($news as $newsItem){
                    $newsMerge = array_add($newsMerge, $newsItem->position, $newsItem->toArray());
                }
            }

            return view('admin.positions', ['news' => $newsMerge]);

        } else{
            abort(403);
        }
    }

    /**
     * Save Positions of News
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function posNewsSave(Request $request)
    {

        if ($request['newsid_pos1'] == '' && $request['newsid_pos2'] == '' && $request['newsid_pos3'] == '' && $request['newsid_pos4'] == '' && $request['newsid_pos5'] == ''
            && $request['newsid_pos6'] == '' && $request['newsid_pos7'] == '' && $request['newsid_pos8'] == ''
            && $request['oldnewsid_pos1'] == '' && $request['oldnewsid_pos2'] == '' && $request['oldnewsid_pos3'] == '' && $request['oldnewsid_pos4'] == ''
            && $request['oldnewsid_pos5'] == '' && $request['oldnewsid_pos6'] == '' && $request['oldnewsid_pos7'] == '' && $request['oldnewsid_pos8'] == ''){
            return back()->withInput()->with('flash-error', "Mindestens eine News-ID für die Position muss gefüllt sein!");
        }

        $pos1 = $request['newsid_pos1'];
        $pos2 = $request['newsid_pos2'];
        $pos3 = $request['newsid_pos3'];
        $pos4 = $request['newsid_pos4'];
        $pos5 = $request['newsid_pos5'];
        $pos6 = $request['newsid_pos6'];
        $pos7 = $request['newsid_pos7'];
        $pos8 = $request['newsid_pos8'];

        $oldpos1 = $request['oldnewsid_pos1'];
        $oldpos2 = $request['oldnewsid_pos2'];
        $oldpos3 = $request['oldnewsid_pos3'];
        $oldpos4 = $request['oldnewsid_pos4'];
        $oldpos5 = $request['oldnewsid_pos5'];
        $oldpos6 = $request['oldnewsid_pos6'];
        $oldpos7 = $request['oldnewsid_pos7'];
        $oldpos8 = $request['oldnewsid_pos8'];


        if (Auth::user()->isAdmin() || Auth::user()->isContentManager()){

            $newsPos1 = new News;
            $newsPos2 = new News;
            $newsPos3 = new News;
            $newsPos4 = new News;
            $newsPos5 = new News;
            $newsPos6 = new News;
            $newsPos7 = new News;
            $newsPos8 = new News;

            $oldNewsPos1 = new News;
            $oldNewsPos2 = new News;
            $oldNewsPos3 = new News;
            $oldNewsPos4 = new News;
            $oldNewsPos5 = new News;
            $oldNewsPos6 = new News;
            $oldNewsPos7 = new News;
            $oldNewsPos8 = new News;


            $newsPos1 = $newsPos1->where('id', '=', $pos1)->where('has_gallery', '=', 0)->first();
            $newsPos2 = $newsPos2->where('id', '=', $pos2)->where('has_gallery', '=', 0)->first();
            $newsPos3 = $newsPos3->where('id', '=', $pos3)->where('has_gallery', '=', 0)->first();
            $newsPos4 = $newsPos4->where('id', '=', $pos4)->where('has_gallery', '=', 0)->first();
            $newsPos5 = $newsPos5->where('id', '=', $pos5)->where('has_gallery', '=', 0)->first();
            $newsPos6 = $newsPos6->where('id', '=', $pos6)->where('has_gallery', '=', 0)->first();
            $newsPos7 = $newsPos7->where('id', '=', $pos7)->where('has_gallery', '=', 0)->first();
            $newsPos8 = $newsPos8->where('id', '=', $pos8)->where('has_gallery', '=', 1)->first();

            $oldNewsPos1 = $oldNewsPos1->where('position', '=', 1)->where('has_gallery', '=', 0)->first();
            $oldNewsPos2 = $oldNewsPos2->where('position', '=', 2)->where('has_gallery', '=', 0)->first();
            $oldNewsPos3 = $oldNewsPos3->where('position', '=', 3)->where('has_gallery', '=', 0)->first();
            $oldNewsPos4 = $oldNewsPos4->where('position', '=', 4)->where('has_gallery', '=', 0)->first();
            $oldNewsPos5 = $oldNewsPos5->where('position', '=', 5)->where('has_gallery', '=', 0)->first();
            $oldNewsPos6 = $oldNewsPos6->where('position', '=', 6)->where('has_gallery', '=', 0)->first();
            $oldNewsPos7 = $oldNewsPos7->where('position', '=', 7)->where('has_gallery', '=', 0)->first();
            $oldNewsPos8 = $oldNewsPos8->where('position', '=', 8)->where('has_gallery', '=', 1)->first();


            //Delete empty position if position was set before and is empty at present request
            if ($pos1 == '' && $oldpos1 != ''){
                $oldNews1 = new News;
                $oldNews1 = $oldNews1->where('id', '=', $oldpos1)->where('has_gallery', '=', 0)->first();
                $oldNews1->update(['position' => 'null']);
            }

            if ($pos2 == '' && $oldpos2 != ''){
                $oldNews2 = new News;
                $oldNews2 = $oldNews2->where('id', '=', $oldpos2)->where('has_gallery', '=', 0)->first();
                $oldNews2->update(['position' => 'null']);
            }

            if ($pos3 == '' && $oldpos3 != ''){
                $oldNews3 = new News;
                $oldNews3 = $oldNews3->where('id', '=', $oldpos3)->where('has_gallery', '=', 0)->first();
                $oldNews3->update(['position' => 'null']);
            }

            if ($pos4 == '' && $oldpos4 != ''){
                $oldNews4 = new News;
                $oldNews4 = $oldNews4->where('id', '=', $oldpos4)->where('has_gallery', '=', 0)->first();
                $oldNews4->update(['position' => 'null']);
            }

            if ($pos5 == '' && $oldpos5 != ''){
                $oldNews5 = new News;
                $oldNews5 = $oldNews5->where('id', '=', $oldpos5)->where('has_gallery', '=', 0)->first();
                $oldNews5->update(['position' => 'null']);
            }

            if ($pos6 == '' && $oldpos6 != ''){
                $oldNews6 = new News;
                $oldNews6 = $oldNews6->where('id', '=', $oldpos6)->where('has_gallery', '=', 0)->first();
                $oldNews6->update(['position' => 'null']);
            }

            if ($pos7 == '' && $oldpos7 != ''){
                $oldNews7 = new News;
                $oldNews7 = $oldNews7->where('id', '=', $oldpos7)->where('has_gallery', '=', 0)->first();
                $oldNews7->update(['position' => 'null']);
            }

            if ($pos8 == '' && $oldpos8 != ''){
                $oldNews8 = new News;
                $oldNews8 = $oldNews8->where('id', '=', $oldpos8)->where('has_gallery', '=', 1)->first();
                $oldNews8->update(['position' => 'null']);
            }


            //positions have been updated which were set before or set new positions
            if ($newsPos1 && $oldNewsPos1 && $oldNewsPos1->id != $newsPos1->id){
                $oldNewsPos1->update(['position' => 'null']);
                $newsPos1->update(['position' => 1]);
            } elseif ($newsPos1){
                $newsPos1->update(['position' => 1]);
            }

            if ($newsPos2 && $oldNewsPos2 && $oldNewsPos2->id != $newsPos2->id){
                $oldNewsPos2->update(['position' => 'null']);
                $newsPos2->update(['position' => 2]);
            } elseif ($newsPos2){
                $newsPos2->update(['position' => 2]);
            }

            if ($newsPos3 && $oldNewsPos3 && $oldNewsPos3->id != $newsPos3->id){
                $oldNewsPos3->update(['position' => 'null']);
                $newsPos3->update(['position' => 3]);
            } elseif ($newsPos3){
                $newsPos3->update(['position' => 3]);
            }

            if ($newsPos4 && $oldNewsPos4 && $oldNewsPos4->id != $newsPos4->id){
                $oldNewsPos4->update(['position' => 'null']);
                $newsPos4->update(['position' => 4]);
            } elseif ($newsPos4){
                $newsPos4->update(['position' => 4]);
            }

            if ($newsPos5 && $oldNewsPos5 && $oldNewsPos5->id != $newsPos5->id){
                $oldNewsPos5->update(['position' => 'null']);
                $newsPos5->update(['position' => 5]);
            } elseif ($newsPos5){
                $newsPos5->update(['position' => 5]);
            }

            if ($newsPos6 && $oldNewsPos6 && $oldNewsPos6->id != $newsPos6->id){
                $oldNewsPos6->update(['position' => 'null']);
                $newsPos6->update(['position' => 6]);
            } elseif ($newsPos6){
                $newsPos6->update(['position' => 6]);
            }

            if ($newsPos7 && $oldNewsPos7 && $oldNewsPos7->id != $newsPos7->id){
                $oldNewsPos7->update(['position' => 'null']);
                $newsPos7->update(['position' => 7]);
            } elseif ($newsPos7){
                $newsPos7->update(['position' => 7]);
            }

            if ($newsPos8 && $oldNewsPos8 && $oldNewsPos8->id != $newsPos8->id){
                $oldNewsPos8->update(['position' => 'null']);
                $newsPos8->update(['position' => 8]);
            } elseif ($newsPos8){
                $newsPos8->update(['position' => 8]);
            }

            return back()->with('flash-success', "Positionen wurden gespeichert!");

        } else{
            return back()->withInput()->with('flash-error', "Sie besitzen keine Berechtigungen!");
        }
    }


    public function create()
    {
		$role = new Role();
        return view('admin.edit', [
            'news'           => new News(['state' => News::STATE_DRAFT]),
            'newsCategories' => NewsCategory::whereNotNull('parent_id')->orderBy('title', 'asc')->pluck('title', 'id')->all(),
		        'usersContentManager' => User::where('role_id', $role->getByConstant(Role::CONTENT_MANAGER)->id)
			        ->orWhere('role_id', $role->getByConstant(Role::ADMIN)->id)->orderBy('role_id', 'asc')->get(),
        ]);
    }

    // @todo improve and move this
    public function store(NewsFormRequest $request)
    {
        if (!isset($request->federal_states)) {
            return \Redirect::to(URL::previous() . '#')->with('flash-error', 'Bitte mindestens ein Bundesland auswählen!');
        }
    	
        $news = new News;

        $news->slug = $request['slug'];

        $validator = Validator::make(['slug' => $news->slug], [
            'slug' => 'required|min:3|unique:news',
        ]);

        if ($validator->fails()){
            $news->slug = str_slug($news->slug . '-' . rand(100, 999), '-');
        }

        $news->published_from = !empty($request['published_from']) ? date('Y-m-d H:i:s', strtotime($request['published_from'])) : null;
        $news->published_until = !empty($request['published_until']) ? date('Y-m-d H:i:s', strtotime($request['published_until'])) : null;
        $news->element_order = !empty($request['element_order']) ? $request['element_order'] : null;

        $news->category_id = $request['category_id'];
        $news->title = $request['title'];
        $news->sub_title = $request['sub_title'];
        $news->content_teaser = $request['content_teaser'];
        $news->content = $request['content'];
        $news->state = $request['state'];

        if ($request['exclusive'] == 1 || $request['exclusive'] == '1'){
            $news->exclusive = 1;
        } else{
            $news->exclusive = 0;
        }

        $news->main_image = $request['main_image'];
        $news->main_image_source = $request['main_image_source'];

        $news->owner_id = Auth::user()->id;
	      $news->user_id = $request->contentManagerId;

        if ($news->state === News::STATE_PUBLISHED){
            $news->published_at = date("Y-m-d H:i:s");
        }

        $news->save();
        
        $this->saveMetaData($request, $news);
        $this->saveFederalstate($request['federal_states'], $news);
        $this->saveTags($request['tag'], $news);

        $images = $request['newsImages'];
        $imgTitle = $request['imgTitle'];
        $imgDesc = $request['imgDesc'];

        if ($images !== null && count($images) > 0){
            $imgOrder = 0;
            foreach ($images as $key => $image){
                $title = $imgTitle[$key];
                $desc = $imgDesc[$key];
                $image = new Image(['picture' => $image, 'title' => $title, 'description' => $desc, 'order' => $imgOrder]);
                $news->images()->save($image);
                $imgOrder++;
            }
        }

        if ($news->images()->count()){
            $news->has_gallery = 1;
            $news->save();
        }

        return Redirect::route('admin.index')->with('flash-success', 'Die News wurde angelegt!');
    }

    public function uploadNewsImage(Request $request)
    {
        $file = $request->file()['upload'];
        $fileName = hash('sha256', $file->getFilename()) . '-' . $file->getClientOriginalName();
        $file->move($this->_newsUploadPath, $fileName);

        // TODO: scale the image, yo!

        return asset($this->_newsUploadPath . $fileName);
    }

    /**
     * @param NewsFormRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(NewsFormRequest $request, $id)
    {
        if (!isset($request->federal_states)) {
            return \Redirect::to(URL::previous() . '#')->with('flash-error', 'Bitte mindestens ein Bundesland auswählen!');
        }
    	
        $news = News::findOrFail($id);

        $slug = $request['slug'];

        $validator = Validator::make(['slug' => $slug], [
            'slug' => 'required|min:3|unique:news',
        ]);

        if ($validator->fails()){
            if ($slug != $news->slug){
                $news->slug = str_slug($news->slug . '-' . rand(100, 999), '-');
            }
        }
        $news->slug = $slug;
        $news->published_from = !empty($request['published_from']) ? date('Y-m-d H:i:s', strtotime($request['published_from'])) : null;
        $news->published_until = !empty($request['published_until']) ? date('Y-m-d H:i:s', strtotime($request['published_until'])) : null;
        $news->element_order = !empty($request['element_order']) ? $request['element_order'] : null;

        $news->category_id = $request['category_id'];
        $news->title = $request['title'];
        $news->sub_title = $request['sub_title'];
        $news->content_teaser = $request['content_teaser'];
        $news->content = $request['content'];
        $news->is_popular = $request->is_popular;
        $news->is_featured = $request->is_featured;
        $news->state = $request['state'];

        if ($request['exclusive'] == 1 || $request['exclusive'] == '1'){
            $news->exclusive = 1;
        } else{
            $news->exclusive = 0;
        }

        $news->main_image = $request['main_image'];
        $news->main_image_source = $request['main_image_source'];

        #$news->owner_id = Auth::user()->id;
        $news->user_id = $request->contentManagerId;

        $news->is_popular = $request['is_popular'];
        $news->is_featured = $request['is_featured'];

        // only update the published_at date if the state changes and no date have been set previously
        if ($news->state === News::STATE_PUBLISHED and $news->getOriginal('published_at') == null){
            $news->published_at = date("Y-m-d H:i:s");
        }

        $news->save();
        
        $this->saveMetaData($request, $news);
        $this->saveFederalstate($request['federal_states'], $news);
        $this->saveTags($request['tag'], $news);
        $images = $request['newsImages'];
        $imgTitle = $request['imgTitle'];
        $imgDesc = $request['imgDesc'];
        $imgUpdateTitle = $request['imgUpdateTitle'];
        $imgUpdateDesc = $request['imgUpdateDesc'];
        $deleteImage = $request['delete-image'];

        if ($images !== null && count($images) > 0){
            $imgMaxOrder = $news->images()->count() == 0 ? 0 : $news->images()->max('order');
            foreach ($images as $key => $image){
                $imgMaxOrder++;
                $title = $imgTitle[$key];
                $desc = $imgDesc[$key];
                $image = new Image(['picture' => $image, 'title' => $title, 'description' => $desc, 'order' => $imgMaxOrder]);
                $news->images()->save($image);
            }
        }
        if ($imgUpdateTitle !== null && count($imgUpdateTitle) > 0){
            $imgOrder = 0;
            foreach ($imgUpdateTitle as $key => $imgUpdate){
                $title = $imgUpdateTitle[$key];
                $desc = $imgUpdateDesc[$key];
                Image::find($key)->update(['title' => $title, 'description' => $desc, 'order' => $imgOrder]);
                $imgOrder++;
            }
        }

        if ($deleteImage !== null && count($deleteImage) > 0){
            foreach ($deleteImage as $key => $val){
                Image::find($val)->delete();
            }
        }

        if ($news->images()->count()){
            $news->has_gallery = 1;
            $news->save();
        } else{
            $news->has_gallery = 0;
            $news->save();
        }

        return Redirect::route('admin.index')->with('flash-success', 'Deine Änderungen wurden gespeichert!');
    }


    public function edit($id)
    {
        $news = News::find($id);
        $role = new Role();
        if (!isset($news->meta)){
            $news->meta()->create([]);
        }

        return view('admin.edit', [
            'news'           => News::find($id),
            'newsImages'     => News::find($id)->images()->orderBy('order', 'ASC')->get(),
            'newsCategories' => NewsCategory::whereNotNull('parent_id')->orderBy('title', 'asc')->pluck('title', 'id')->all(),
            'usersContentManager' => User::where('role_id', $role->getByConstant(Role::CONTENT_MANAGER)->id)
	                                        ->orWhere('role_id', $role->getByConstant(Role::ADMIN)->id)->orderBy('role_id', 'asc')->get(),
            'userContentManager' => $news->userContentManager()->first(),
        ]);
    }


    public function toggleVisibility(Request $request)
    {
        $news = News::find($request->id);
        if ($request->ajax()){
            $news->state = ($news->state !== News::STATE_PUBLISHED) ? News::STATE_PUBLISHED : News::STATE_HIDDEN;
            $news->save();

            return response()->json($news->state);
        } else{
            abort(403);
        }
    }

    public function destroy(Request $request)
    {
        $news = News::find($request->id);
        if ($request->ajax()){
            $news->deleted_at = date("Y-m-d H:i:s");
            $news->save();

            return response()->json($news->deleted_at);
        } else{
            abort(403);
        }
    }

    /**
     * Returns a specific (Datatables) club list handled by request params
     *
     * @param $type
     * @return mixed
     */
    public function clubList($type)
    {
        $club = new Club;

        switch ($type){
            case 'new-unowned':
                $club = $club->where('reviewed', 0)->where('unowned', 1);
                break;
            case 'existing-unowned':
                $club = $club->where('reviewed', 1)->where('unowned', 1);
                break;
            case 'new-owned':
                $club = $club->where('reviewed', 0)->where(function($query){
                    $query
                        ->where('unowned', 0)
                        ->orWhereNull('unowned');
                });
                break;
            case 'existing-owned':
                $club = $club->where('reviewed', 1)->where(function($query){
                    $query
                        ->where('unowned', 0)
                        ->orWhereNull('unowned');
                });
                break;
        }

        $clubs = $club->get();

        return Datatables::of($clubs)
                         ->addColumn('name_mapping', function($club){
                             return "<select type='text' data-club-mapping class='input' value='$club->name'></select>";
                         })
                         ->addColumn('options', function($club){
                             return "<div class='admin__table-option'>
                            <a class='fa fa-check' data-review-club='" . $club->id . "' href='#'></a>
                            
                            <a class='fa fa-eye' target='_blank' href='" . route('club.detail', $club->slug) . "'></a>
                            
                            <a class='fa fa-trash' data-toggle-delete data-club-delete='" . route('admin.club.delete', ['id' => $club->id]) . "' href='#'></a>
                        </div>";
                         })
                         ->make(true);
    }

    /**
     * Returns a specific (Datatables) sport list handled by request params
     *
     * @param $type
     * @return mixed
     */
    public function sportList($type)
    {
        $sport = new Sport;

        switch ($type){
            case 'new':
                $sport = $sport->where('reviewed', 0);
                break;
            case 'existing':
                $sport = $sport->where('reviewed', 1);
                break;
        }

        $sports = $sport->get();

        return Datatables::of($sports)
                         ->addColumn('name_mapping', function($sport){
                             return "<select type='text' data-sport-mapping class='input' value='$sport->title'></select>";
                         })
                         ->addColumn('options', function($sport){
                             return "<div class='admin__table-option'>
                            <a class='fa fa-check' data-sport-review data-handle-url='" . route('admin.sport.review', $sport->id) . "' href='#'></a>
                            
                            <a class='fa fa-trash' data-sport-delete data-handle-url='" . route('admin.sport.delete', ['id' => $sport->id]) . "' href='#'></a>
                        </div>";
                         })
                         ->make(true);
    }


    /**
     * @param $id
     * @param Request $request
     * @return array
     */
    public function reviewSport($id, Request $request)
    {
        $sport = new Sport;

        $mapId = empty($request->input('map_id')) ? null : $request->input('map_id');

        if (!is_null($mapId)){
            // needs mapping
            $sportMappingHistory = new SportMappingHistory;

            $legacySport = $sport->where('id', $id)->firstOrFail();
            $newSport = $sport->where('id', $mapId)->firstOrFail();

            $sportLegacyUserIds = $legacySport->users()->pluck('user.id')->all();
            $sportLegacyClubIds = $legacySport->clubs()->pluck('club.id')->all();

            // detach existing associations
            $legacySport->users()->detach();
            $legacySport->clubs()->detach();

            // move existing associations from legacy club to new club
            $newSport->users()->attach($sportLegacyUserIds, ['interest' => 30]);
            $newSport->clubs()->attach($sportLegacyClubIds);
            $newSport->clubs()->attach($legacySport->id);

            // log club mapping as history entry
            $sportMappingHistory->create([
                'legacy_sport_id' => $legacySport->id,
                'new_sport_id'    => $newSport->id
            ]);

            // flag edited sports as reviewed
            $legacySport->update(['reviewed' => Sport::STATUS_REVIEWED]);
            $newSport->update(['reviewed' => Sport::STATUS_REVIEWED]);

            // flag legacy sport as deleted
            $legacySport->delete();

        } else{
            // alright fine! no mapping needed
            // ...just flag this sport as reviewed
            $sport->where('id', $id)->update(
                ['reviewed' => Sport::STATUS_REVIEWED]
            );
        }

        return [$id, $mapId];

    }

    public function reviewClub(Request $request)
    {
        $club = new Club;

        $clubId = (int)$request->input('club_id');
        $mapClubId = empty($request->input('map_club_id')) ? null : $request->input('map_club_id');

        if (!is_null($mapClubId)){
            // needs mapping
            $role = new Role;
            $sportCareer = SportCareer::where('club_id', $clubId)->first();
            $clubMappingHistory = new ClubMappingHistory;

            $legacyClub = $club->where('id', $clubId)->firstOrFail();
            $newClub = $club->where('id', $mapClubId)->firstOrFail();

//            $legacyClubUserIds = $legacyClub->users()->wherePivot('role_id', '!=', $role->getByConstant(Role::CLUB_OWNER))->pluck('user.id');
            $legacyClubUserIds = $legacyClub->users()->pluck('user.id');
            $newClubUserIds = $newClub->users()->pluck('user.id');
            $userIds = collect([]);

            $legacyClubUserIds->each(function($id) use ($newClubUserIds, $userIds){
                if (!$newClubUserIds->contains($id)){
                    $userIds->push($id);
                }
            });

            // detach existing associations
            $legacyClub->users()->detach();

            if ($sportCareer){
                $sportCareer->update(['club_id' => $newClub->id]);
            }

            // move existing associations from legacy club to new club
            $newClub->users()->attach($userIds->all(), ['role_id' => $role->getByConstant(Role::CLUB_MEMBER)->id]);

            // log club mapping as history entry
            $clubMappingHistory->create([
                'legacy_club_id' => $legacyClub->id,
                'new_club_id'    => $newClub->id
            ]);

            // flag edited clubs as reviewed
            $legacyClub->update(['reviewed' => Club::STATUS_REVIEWED]);
            $newClub->update(['reviewed' => Club::STATUS_REVIEWED]);

            // flag legacy club as deleted
            $legacyClub->delete();

        } else{
            // alright fine! no mapping needed
            // ...just flag this club as reviewed
            $club->where('id', $clubId)->update(
                ['reviewed' => Club::STATUS_REVIEWED]
            );
        }

        return [$clubId, $mapClubId];

    }


    public function exportClubs()
    {
        $club = new Club;
        $role = new Role;
        $clubs = $club->with('teams')->with('posts')->with('events')->with('users')->get();
        $roleMapping = [
            Role::CLUB_OWNER  => $role->getByConstant(Role::CLUB_OWNER)->id,
            Role::CLUB_MEMBER => $role->getByConstant(Role::CLUB_MEMBER)->id,
            Role::CLUB_FAN    => $role->getByConstant(Role::CLUB_FAN)->id,
            Role::CLUB_ADMIN  => $role->getByConstant(Role::CLUB_ADMIN)->id,
        ];

        $writer = Writer::createFromFileObject(new \SplTempFileObject());

        $writer->setNewline("\r\n"); //use windows line endings for compatibility with some csv libraries
        $writer->setOutputBOM(Writer::BOM_UTF8); //adding the BOM sequence on output

        // set csv first (header) row
        $writer->insertOne([
            'Name',
            'Straße',
            'Hausnummer',
            'PLZ',
            'Ort',
            'E-Mail',
            'Gegründet',
            'Kürzel',
            'Mitglieder',
            'Fans',
            'Mitglieder Anzahl',
            'Mannschaften',
            'Sportarten',
            'Posts',
            'Events',
            'Profilstatus',
            'Eigentümer',
            'Veröffentlicht',
            'Erstellt',
        ]);

        $clubs->each(function($club) use ($writer, $role, $roleMapping){
            $clubOwner = null;
            $clubUsers = [
                $roleMapping[Role::CLUB_OWNER]  => 0,
                $roleMapping[Role::CLUB_MEMBER] => 0,
                $roleMapping[Role::CLUB_FAN]    => 0,
                $roleMapping[Role::CLUB_ADMIN]  => 0,
            ];

            $club->users->each(function($user) use (&$clubOwner, &$clubUsers, $roleMapping){
                if ($user->pivot->role_id === $roleMapping[Role::CLUB_OWNER]){
                    $clubOwner = $user->username;
                }

                $clubUsers[$user->pivot->role_id]++;
            });

            $writer->insertOne([
                $club->name,
                $club->street,
                $club->house_number,
                $club->zip,
                $club->city,
                $club->email,
                $club->founded,
                $club->shorthand,
                $clubUsers[$roleMapping[Role::CLUB_MEMBER]],
                $clubUsers[$roleMapping[Role::CLUB_FAN]],
                $club->member_count,
                $club->teams->count(),
                $club->allSportsString(),
                $club->posts->count(),
                $club->events->count(),
                $club->getClubProfileCompletionProgress()['progress_percentage'] . '%',
                ($clubOwner !== null) ? $clubOwner : 'Kein Eigentümer',
                $club->isPublished() ? 'Ja' : 'Nein',
                date('d.m.Y', strtotime($club->created_at)),
            ]);
        });

        $filename = 'vlde-vereinsliste_' . date('d-m-Y') . '.csv';

        $writer->output($filename);
    }

    public function exportUsers()
    {
        $user = new User;
        $role = new Role;
        $users = $user->with('clubs')->with('posts')->with('sport_careers')->with('sports')->get();
        $writer = Writer::createFromFileObject(new \SplTempFileObject());

        $writer->setNewline("\r\n"); //use windows line endings for compatibility with some csv libraries
        $writer->setOutputBOM(Writer::BOM_UTF8); //adding the BOM sequence on output

        // set csv first (header) row
        $writer->insertOne([
            'Name',
            'Benutzername',
            'Straße',
            'Hausnummer',
            'PLZ',
            'Ort',
            'E-Mail',
            'Geburtstag',
            'Sportliche Interessen',
            'Vereine',
            'Posts',
            'Laufbahn',
            'Bestätigt',
            'Newsletter',
            'Datenschutz',
            'Ist Fake',
            'Registriert',
        ]);

        $users->each(function($user) use ($writer, $role){
            $sports = $user->sports->flatMap(function($sport){
                return [$sport->title . ':' . $sport->pivot->interest];
            });

            $writer->insertOne([
                $user->fullname,
                $user->username,
                $user->street,
                $user->house_number,
                $user->zip,
                $user->city,
                $user->email,
                date('d.m.Y', strtotime($user->birthday)),
                $sports->implode(', '),
                $user->clubs->implode('name', ', '),
                !is_null($user->posts) ? $user->posts->count() : 0,
                !is_null($user->sport_careers) ? $user->sport_careers->count() : 0,
                !is_null($user->verified) ? $user->verified : 0,
                $user->newsletter,
                !is_null($user->privacy) ? $user->privacy : 0,
                !is_null($user->has_fake_email) ? $user->has_fake_email : 0,
                date('d.m.Y', strtotime($user->created_at)),
            ]);
        });

        $filename = 'vlde-benutzerliste_' . date('d-m-Y') . '.csv';

        $writer->output($filename);
    }

    /**
     * @param NewsFormRequest $request
     * @param $news
     */
    public function saveMetaData(NewsFormRequest $request, $news)
    {
        $meta = new Meta();

        $meta->meta_bild_alt = $request->meta_bild_alt;
        $meta->meta_description = $request->meta_description;
        $meta->meta_keywords = $request->meta_keywords;
        $meta->meta_author = $request->meta_author;
        $meta->meta_language = $request->meta_language;

        if ($news->meta === null){
            $news->meta()->create($meta->toArray());
        } else{
            $news->meta()->update($meta->toArray());
        }
    }

    public function saveFederalstate($federalstates, $news)
    {
        $news->states()->detach();
        if ($federalstates != null){
            foreach ($federalstates as $stateId){
                if ($news->states()->where('stateable_id', $stateId)->first() === null){
                    $state = State::find($stateId);
                    $news->states()->save($state);
                }
            }
        }
    }

    public function saveTags($tagLine, $news)
    {
        $news->tags()->detach();
        if ($tagLine != ""){
            $tags = explode(" ", $tagLine);
            foreach ($tags as $tagWord){
                if ($tagWord != " " and $tagWord != ""){
                    $tag = Tag::where('name', $tagWord)->first();
                    if ($tag === null){
                        $tag = new Tag(['name' => $tagWord]);
                    }
                    $news->tags()->save($tag);
                }
            }
        }
    }


    public function adminClubs()
    {
        $role = new Role();
        $adminRequestedId = $role->getByConstant(Role::CLUB_ADMIN_REQUEST)->id;
        $adminRequestedClubs = DB::table('user_club_role')->where('role_id', $adminRequestedId)->get();

        return view('admin.club.index', ['adminRequestedClubs' => $adminRequestedClubs]);
    }

    public function adminRequestAccept(Request $request)
    {
        $role = new Role();
        $user = User::where('username', $request['username'])->firstOrFail();
        $club = Club::where('slug', $request['clubSlug'])->firstOrFail();
        $clubAdminId = $role->getByConstant(Role::CLUB_ADMIN)->id;
        $adminRequestId = $role->getByConstant(Role::CLUB_ADMIN_REQUEST)->id;
        $club->users()->wherePivot('role_id', $adminRequestId)
             ->wherePivot('user_id', $user->id)
             ->updateExistingPivot($user->id, ['role_id' => $clubAdminId], false);

        $imprint = Content::select('content')->where('slug', 'imprint')->first();

        Mail::send('admin.emails.club-accept-admin', ['user' => $user, 'imprint' => $imprint], function($message) use ($user){
            $message->to($user->email, $user->firstname)->subject('vereinsleben.de // Dein Antrag zur Profilübernahme wurde angenommen');
        });

        return Redirect::route('admin.clubs')->with('flash-success', $user->firstname . " " . $user->lastname . " ist jetzt Admin des Vereins: " . $club->name);
    }

    public function adminRequestDecline(Request $request)
    {

        $role = new Role();
        $user = User::where('username', $request['username'])->firstOrFail();
        $club = Club::where('slug', $request['clubSlug'])->firstOrFail();
        $adminRequestId = $role->getByConstant(Role::CLUB_ADMIN_REQUEST)->id;

        $club->users()->newPivotStatementForId($user->id)->where('role_id', $adminRequestId)->delete();

        $imprint = Content::select('content')->where('slug', 'imprint')->first();

        Mail::send('admin.emails.club-decline-admin', ['user' => $user, 'imprint' => $imprint], function($message) use ($user){
            $message->to($user->email, $user->firstname)->subject('vereinsleben.de // Dein Antrag zur Profilübernahme wurde abgelehnt');
        });

        return Redirect::route('admin.clubs')->with('flash-warning', $user->firstname . " " . $user->lastname . " ist kein Admin des Vereins: " . $club->name);
    }

    public function adminClubsVita()
    {

        $suggestedClubs = new Club;
        $suggestedClubs = $suggestedClubs
            ->where('published', '=', 0)
            ->orWhereNull('published')
            ->where('suggested_from', '!=', 0)
            ->get();

        return view('admin.club.vita', ['suggestedClubs' => $suggestedClubs]);
    }

    public function adminSuggestedPublish(Request $request)
    {
        $clubToPublish = Club::where('slug', $request['clubSlug'])->firstOrFail();
        $user = $clubToPublish->suggestedFrom();
        $clubToPublish->published = 1;
        $clubToPublish->save();

//        Mail::send('admin.emails.publish-suggested-club', ['user' => $user, 'club' => $clubToPublish], function($message) use ($user, $clubToPublish){
//            $message->to($user->email, $user->firstname)->subject('vereinsleben.de // Dein vorgeschlagener Verein ' . $clubToPublish->name . ' wurde akzeptiert.');
//        });

        return Redirect::route('admin.clubs.vita')->with('flash-success', 'Der Verein  ' . $request['clubSlug'] . ' wurde erfolgreich veröffentlicht!');

    }

    public function adminSuggestedRemove(Request $request)
    {
        $clubExisting = Club::where('name', $request['club'])->first();
        $clubToRemove = Club::where('slug', $request['clubSlug'])->first();
        $user = $clubToRemove->suggestedFrom();
        $clubToRemove->delete();

//        Mail::send('admin.emails.remove-suggested-club', ['user' => $user, 'club' => $clubToRemove, 'existing' => $clubExisting], function($message) use ($user, $clubToRemove, $clubExisting){
//            $message->to($user->email, $user->firstname)->subject('vereinsleben.de // Dein Vereinsvorschlag ' . $clubToRemove->name . ' wurde abgelehnt.');
//        });

        return Redirect::route('admin.clubs.vita')->with('flash-success', 'Der Verein  ' . $request['clubSlug'] . ' wurde erfolgreich entfernt!');
    }

    public function sports(Request $request){
        return view('admin.sport.index');
    }

}
