<?php

namespace Vereinsleben\Http\Controllers;

use Auth;
use Carbon\Carbon;
use DateTime;
use DB;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use League\Csv\Writer;
use Mail;
use Response;
use Storage;
use Vereinsleben\Club;
use Vereinsleben\ClubSport;
use Vereinsleben\Content;
use Vereinsleben\Role;
use Vereinsleben\Sport;
use Vereinsleben\State;
use Vereinsleben\User;
use Illuminate\Support\Facades\Validator;
use Vereinsleben\UserSport;


class AdminUserController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(Request $request)
    {
        parent::__construct($request);
    }



    public function index(Request $request)
    {
        $user = new User;
        $users = null;

        $keyword = trim($request->input('keyword', ''));
        $email = trim($request->input('email', ''));
        $city = trim($request->input('city', ''));
        $anfangOld = $request->input('anfang');
        $endeOld = $request->input('ende');
        $date_filter = trim($request->input('date_filter'));
        $sporttype = $request->input('sport', '');
        $plz_von = $request->input('von', '');
        $plz_bis = $request->input('bis', '');
        $vorname = $request->input('vorname', '');
        $nachname = $request->input('nachname', '');

        $path = storage_path('app' . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'vlde-benutzerliste-gefiltert.csv');
        $exp_user = \File::exists($path);


        if ($keyword){
            $user = $user->search($keyword);
        }
        if ($email){
            $user = $user->where('email', 'LIKE', '%' . $email . '%');
        }
        if ($city){
            $user = $user->where(function($query) use ($city){
                $query->orWhere('city', 'LIKE', '%' . $city . '%')->orWhere('federal_state', $city);
                //$query->orWhere('city', 'LIKE', '%' . $city . '%')->orWhere('federal_state', $city)->orWhere('zip', $city);
            });
        }

        if ($vorname !== '' && $nachname !== ''){
            $user = $user
                ->where('firstname', 'LIKE', '%' . $vorname)
                ->where('lastname', 'LIKE', '%' . $nachname);
        } elseif ($vorname !== '' && $nachname == ''){
            $user = $user->where('firstname', 'LIKE', '%' . $vorname);
        } elseif ($vorname == '' && $nachname !== ''){
            $user = $user->where('lastname', 'LIKE', '%' . $nachname);
        }

        if ($sporttype !== ''){
            /** Sport IDs */
            $sportids = array();
            $sporty = Sport::where('title', 'LIKE', '%' . $sporttype . '%')->get();
            foreach ($sporty as $sp){
                array_push($sportids, $sp->id);
            }
            $user = $user->whereHas('sports', function($query) use ($sportids){
                $query->whereIn('sport_id', $sportids);
            });
        }


        $anfang = Carbon::parse($anfangOld)->format('Y-m-d');
        $ende = Carbon::parse($endeOld)->format('Y-m-d');


        if ($date_filter == 'created_at'){
            if ($anfang !== '' && $ende !== ''){

                $user = $user->where($date_filter, '>=', $anfang)->where($date_filter, '<=', $ende);

            } else{
                return \Redirect::route('admin.users')->with('flash-info', 'Anfangsdatum und Enddatum müssen gefüllt sein!');
            }
        }

        if ($date_filter == 'login_at'){
            if ($anfang !== '' && $ende !== ''){

                $user = $user->where($date_filter, '>=', $anfang)->where($date_filter, '<=', $ende);

            } else{
                return \Redirect::route('admin.users')->with('flash-info', 'Anfangsdatum und Enddatum müssen gefüllt sein!');
            }
        }

        if ($date_filter == 'updated_at'){
            if ($anfang !== '' && $ende !== ''){

                $user = $user->where($date_filter, '>=', $anfang)->where($date_filter, '<=', $ende);

            } else{
                return \Redirect::route('admin.users')->with('flash-info', 'Anfangsdatum und Enddatum müssen gefüllt sein!');
            }
        }


        if ($plz_von !== '' && $plz_bis !== ''){
            $user = $user->whereBetween('zip', [$plz_von, $plz_bis]);
        } elseif ($plz_von !== '' && $plz_bis == ''){
            $user = $user->where('zip', '=', $plz_von);
        }

        if ($keyword !== '' || $email !== '' || $city !== '' || $date_filter !== '' || $vorname !== ''
            || $nachname !== '' || $sporttype !== '' || $plz_von !== '' || $plz_bis !== ''){
            $users = $user->orderBy('id', 'desc')->get();
        }

        return view('admin.user.index', [
            'users'    => $users,
            'exp_user' => $exp_user,
            'query'    => [
                'keyword'     => $keyword,
                'email'       => $email,
                'city'        => $city,
                'date_filter' => $date_filter,
                'sport'       => $sporttype,
                'von'         => $plz_von,
                'bis'         => $plz_bis,
                'vorname'     => $vorname,
                'nachname'    => $nachname
                //'anfang' => $anfang,
                //'ende' => $ende
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($username)
    {
        //
        $user = User::where('username', $username)->firstOrFail();
        $roles = Role::where('constant', 'NOT LIKE', 'CLUB%')
                     ->where('constant', 'NOT LIKE', 'BAND%')
                     ->orderBy('constant')
                     ->get();
        $toAddStates = $user->ToAddStates();
        $toRemoveStates = $user->states;

        return view('admin.user.edit', ['user'           => $user,
                                        'roles'          => $roles,
                                        'toAddStates'    => $toAddStates,
                                        'toRemoveStates' => $toRemoveStates]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::where('constant', 'NOT LIKE', 'CLUB%')
                     ->where('constant', 'NOT LIKE', 'BAND%')
                     ->orderBy('constant')
                     ->get();
        $roles->prepend(['id' => '0', 'name' => 'nicht vergeben', 'constant' => '', 'description' => '', 'created_at' => '', 'updated_at' => '']);

        $clubs = $user->clubsWhereAdmin()->get();

        return view('admin.user.edit', [
            'user'  => $user,
            'roles' => $roles,
            'clubs' => $clubs
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::where('id', $id)->firstOrFail();
        $this->authorize(Auth::user(), $user);

        $birthday = $request->get('birthday-year') . '-' . str_pad($request->get('birthday-month'), 2, '0', STR_PAD_LEFT) . '-' .
            str_pad($request->get('birthday-day'), 2, '0', STR_PAD_LEFT) . ' 12:59:59';

        if($request->get('birthday-year') == '' || $request->get('birthday-month') == '' || $request->get('birthday-day') == ''){
            $birthday = null;
        }

        $newsletter['news'] = ($request->get('newsletter') === '1') ? 1 : 0;
        $newsletter['old'] = $user->newsletter;
        $fullname = $request->get('firstname') . ' ' . $request->get('lastname');

        $new_email = $request->get('new_email');

        switch ($request->input('action')){

            case 'Bestätigungsmail senden':

                $validator = Validator::make(['email' => $new_email], [
                    'email' => 'email',
                ]);
                if ($validator->fails()){
                    return \Redirect::route('admin.user.edit', $user->id)->with('flash-error', 'E-Mail Format ist fehlerhaft!');
                }

                $validator = Validator::make(['email' => $new_email], [
                    'email' => 'unique:user',
                ]);

                if ($validator->fails()){
                    return \Redirect::route('admin.user.edit', $user->id)->with('flash-error', 'E-Mail ist bereits vergeben!');
                }

                if ($new_email !== '' && $new_email !== $user->email){
                    $user->update([
                        'change_email_token'            => str_random(054),
                        'change_email_unconfirmedEmail' => $new_email]);
                    $user->save();

                    $imprint = Content::select('content')->where('slug', 'imprint')->first();

                    Mail::send('auth.emails.update-email', ['user' => $user, 'imprint' => $imprint], function($message) use ($user, $new_email){
                        $message->to($new_email, $user->username)->subject('vereinsleben.de - E-Mail Änderung bestätigen');
                    });

                    return \Redirect::route('admin.user.edit', $user->id)->with('flash-success', 'Bestätigungs-E-Mail wurde versandt!');
                }

            case 'Registrierungsmail senden':

                if ($new_email !== '' && $new_email == $user->email && $user->verified == 0){
                    $imprint = Content::select('content')->where('slug', 'imprint')->first();
                    Mail::send('auth.emails.register-confirmation', ['user' => $user, 'imprint' => $imprint], function($message) use ($user){
                        $message->to($user->email, $user->username)->subject('vereinsleben.de - Registrierung bestätigen');
                    });

                    return \Redirect::route('admin.user.edit', $user->id)->with('flash-success', 'Registrierungs-E-Mail wurde versandt!');
                }

            case 'Speichern':
                $user->update([
                    'role_id'       => $request->get('role_id'),
                    'gender'        => $request->get('gender'),
                    'firstname'     => $request->get('firstname'),
                    'lastname'      => $request->get('lastname'),
                    'fullname'      => $fullname,
                    'birthday'      => $birthday,
                    'street'        => $request->get('street'),
                    'house_number'  => $request->get('house_number'),
                    'zip'           => $request->get('zip'),
                    'city'          => $request->get('city'),
                    'federal_state' => $request->get('federal_state'),
                    'newsletter'    => $newsletter['news'],
                    'messages'      => ($request->get('messages') === '1') ? 1 : 0,
                ]);
                $verified = $request->get('verified') == null ? 0 : 1;
                $user->verified = $verified;
                $user->save();
                
	              return \Redirect::to(URL::previous() . '#')->with('flash-success', 'Die Nutzerdaten wurden erfolgreich aktualisiert!');
        }

    }

    /*08-10-2018: addClub to User where User will be ClubAdmin*/
    public function addClub(Request $request)
    {

        if ($request['clubid'] == '' || $request['clubid'] == null){
            return back()->withInput()->with('flash-error', "Vereins-ID darf nicht leer sein!");
        }

        if (Auth::user()->isAdmin() && $request){

            $role = new Role();
            $user = User::where('id', $request['userid'])->firstOrFail();
            $club = Club::where('id', $request['clubid'])->firstOrFail();
            $clubAdminId = $role->getByConstant(Role::CLUB_ADMIN)->id;
            $id = $request['clubid'];
            $today = Carbon::now();

            $clubad = DB::table('user_club_role')
                        ->where('user_id', '=', $user->id)
                        ->where('club_id', '=', $club->id)
                        ->where('role_id', '=', $clubAdminId)
                        ->exists();


            if ($club && $clubad != true){

                DB::table('user_club_role')
                  ->insert(['user_id' => $user->id, 'club_id' => $club->id, 'role_id' => $clubAdminId, 'created_at' => $today]);

                return back()->withInput()->with('flash-success', "Der Benutzer $user->fullname wurde dem Verein $club->name als Vereinsadmin zugeteilt!");

            } elseif ($clubad){

                return back()->withInput()->with('flash-error', "Benutzer ist bereits Admin des Vereins mit der ID $id!");
            } else{
                return back()->withInput()->with('flash-error', "Der Verein mit der ID $id existiert nicht oder wurde gelöscht!");
            }

        } else{
            abort(403);
        }
    }

    /*08-10-2018: Delete relation of user and club where user is ClubAdmin*/
    public function deleteClub($userid, $id)
    {

        if (Auth::user()->isAdmin()){

            $role = new Role();
            $clubAdminId = $role->getByConstant(Role::CLUB_ADMIN)->id;

            $dataToDelete = DB::table('user_club_role')
                              ->where('user_id', '=', $userid)
                              ->where('club_id', '=', $id)
                              ->where('role_id', '=', $clubAdminId)
                              ->delete();

            return back()->withInput()->with('flash-warning', "Die Admin-Beziehung zum Verein mit der ID: $id wurde entfernt!");

        } else{
            abort(403);
        }
    }


    /**
     * @param Request $request
     * @param $username
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function updateState(Request $request, $username)
    {
        $user = User::where('username', $username)->firstOrFail();
        $this->authorize(Auth::user(), $user);
        $stateId = $request->get('state_id');
        if (State::where('id', $stateId)->count() > 0){
            if ($request->get('add_federalstate')){
                $user->states()->sync($request->get('state_id'), false);
            } elseif ($request->get('remove_federalstate')){
                $user->states()->detach($request->get('state_id'));
            }
        }

        return \Redirect::route('admin.user.detail', $user->username);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $user = User::find($request->id);
		    $user->deleted_at = date("Y-m-d H:i:s");
		    $user->save();
		    
	      return \Redirect::to(URL::previous() . '#')->with('flash-success', 'Dieser Benutzer wurde erfolgreich gelöscht');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function showDelReq()
    {

        $user = new User;
        $users = null;

        $user = $user->onlyTrashed();
        $users = $user->orderBy('id', 'desc')->get();

        return view('admin.user.deletereq', [
            'users' => $users
        ]);
    }

    /**
     * Accept the user account force delete request
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function hardDeleteUser(Request $request)
    {
        User::where('id', $request->id)->forcedelete();

        return \Redirect::route('admin.users.deletereq')->with('flash-info', 'Benutzer wurde erfolgreich gelöscht!');
    }

    /**
     * Deny the user account delete request
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function denyDeleteUser(Request $request)
    {
        User::withTrashed()->find($request->id)->restore();

        return \Redirect::route('admin.users.deletereq')->with('flash-info', 'Konto löschen wurde für den gewählten Benutzer abgelehnt!');
    }


    /**
     * Export User data from datatable
     */

    public function exportUsersData(Request $request)
    {

        $user = new User;
        $role = new Role;
        $ids = json_decode($request->ids, true);
        $users = $user->whereIn('id', $ids)->with('clubs')->with('posts')->with('sport_careers')->with('sports')->get();
        $filename = 'vlde-benutzerliste-gefiltert.csv';
        $filename = storage_path('app' . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . $filename);

        $writer = Writer::createFromPath($filename, "w+");
        $writer->setNewline("\r\n"); //use windows line endings for compatibility with some csv libraries
        $writer->setDelimiter(";");
        $writer->setOutputBOM(Writer::BOM_UTF8); //adding the BOM sequence on output

        // set csv first (header) row
        $writer->insertOne("\xEF\xBB\xBF");
        $writer->insertOne([
            'id',
            'Name',
            'Vorname',
            'Straße',
            'Hausnummer',
            'PLZ',
            'Ort',
            'Bundesland',
            'Geburtstag',
            'E-Mail',
            'Vita Interessen',
            'Vereine',
            'Posts (ja / nein)',
            'Bestätigt',
            'Newsletter',
            'Datum der Registrierung',
        ]);

        $users->each(function($user) use ($writer, $role){
            $sports = $user->sports->flatMap(function($sport){
                return [$sport->title . ':' . $sport->pivot->interest];
            });

            $writer->insertOne([
                $user->id,
                $user->lastname,
                $user->firstname,
                $user->street,
                $user->house_number,
                $user->zip,
                $user->city,
                $user->federal_state,
                date('d.m.Y', strtotime($user->birthday)),
                $user->email,
                $sports->implode(', '),
                $user->clubs->implode('name', ', '),
                !is_null($user->posts) ? $user->posts->count() : 0,
                !is_null($user->verified) ? $user->verified : 0,
                $user->newsletter,
                date('d.m.Y', strtotime($user->created_at)),
            ]);
        });

        return back()->withInput()->with('flash-success', "Der Export war erfolgreich!");

    }

    public function downloadUserFile()
    {

        $path = storage_path('app' . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'vlde-benutzerliste-gefiltert.csv');

        if (\File::exists($path)){

            return response()->download($path)->deleteFileAfterSend($path);
        } else{
            abort(403);
        }

    }




    /**
     * User Statistics for new Admin Dashboard
     */

    public function userStatistics(Request $request)
    {

        $federal_states = State::pluck('name', 'id')->toArray();

        $fed_state = trim($request->input('state', ''));

        $sporttype = $request->input('sport', '');

        $abschnitt = ['User', 'Neuregistrierungen', 'Altersklassen', 'Aktivität', 'Intensität', 'Löschungen'];

        $abschnitt_auswahl = trim($request->input('abschnitt'));

        $today = date('Y-m-d');
        $today_new = date_create($today);
        $past_1 = date('Y-m-d', strtotime("-1 day"));
        $past_7 = date('Y-m-d', strtotime("-7 days"));
        $past_30 = date('Y-m-d', strtotime("-30 days"));
        $past_90 = date('Y-m-d', strtotime("-90 days"));


        $statistics = array();
        $all_users = new User;
        $user_registered = new User;

        if ($fed_state !== '' && $fed_state !== 'all'){

            /** registered users */
            $user_registered = $user_registered
                ->where('federal_state', 'LIKE', $fed_state)
                ->count();

            $statistics['user_registered'] = $user_registered;
        } else{
            /** registered users */
            $user_registered = $user_registered->count();
            $statistics['user_registered'] = $user_registered;
        }


        if ($sporttype !== ''){

            /** Sport IDs */
            $sportids = array();
            $sporty = Sport::where('title', 'LIKE', '%' . $sporttype . '%')->get();
            foreach ($sporty as $sp){
                array_push($sportids, $sp->id);
            }

        }


        if ($sporttype !== ''){

            switch ($abschnitt_auswahl){
                case 0:

                    /** USERS for sport
                     * registered users sport*/
                    $all_users_sp = new User;

                    $user_registered_sp = new User;
                    $user_reg_verified_sp = new User;
                    $user_reg_male_sp = new User;
                    $user_reg_female_sp = new User;

                    /** all users sport*/
                    $all_users_sp = $all_users_sp->whereHas('sports', function($query) use ($sportids){
                        $query->whereIn('sport_id', $sportids);
                    })->count();
                    $statistics['all_users_sp'] = $all_users_sp;

                    /** registered users */
                    $user_registered_sp = $user_registered_sp
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_registered_sp'] = $user_registered_sp;

                    $user_reg_verified_sp = $user_reg_verified_sp
                        ->where('verified', '=', 1)
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_reg_verified_sp'] = $user_reg_verified_sp;

                    $user_reg_male_sp = $user_reg_male_sp
                        ->where('gender', 'LIKE', 'male')
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_reg_male_sp'] = $user_reg_male_sp;

                    $user_reg_female_sp = $user_reg_female_sp
                        ->where('gender', 'LIKE', 'female')
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_reg_female_sp'] = $user_reg_female_sp;

                    break;


                case 1:

                    /** new registered users 24h sport*/
                    $user_new_24_all_sp = new User;
                    $user_new_24_verified_sp = new User;
                    $user_new_24_male_sp = new User;
                    $user_new_24_female_sp = new User;

                    /** new registered users 7 days sport*/
                    $user_new_7d_all_sp = new User;
                    $user_new_7d_verified_sp = new User;
                    $user_new_7d_male_sp = new User;
                    $user_new_7d_female_sp = new User;

                    /** new registered users 30 days sport*/
                    $user_new_30d_all_sp = new User;
                    $user_new_30d_verified_sp = new User;
                    $user_new_30d_male_sp = new User;
                    $user_new_30d_female_sp = new User;

                    /** new registered users 90 days sport*/
                    $user_new_90d_all_sp = new User;
                    $user_new_90d_verified_sp = new User;
                    $user_new_90d_male_sp = new User;
                    $user_new_90d_female_sp = new User;

                    /** new registered users 24h*/
                    $user_new_24_all_sp = $user_new_24_all_sp
                        ->whereBetween('created_at', [$past_1, $today])
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_new_24_all_sp'] = $user_new_24_all_sp;

                    $user_new_24_verified_sp = $user_new_24_verified_sp
                        ->whereBetween('created_at', [$past_1, $today])
                        ->where('verified', '=', 1)
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_new_24_verified_sp'] = $user_new_24_verified_sp;

                    $user_new_24_male_sp = $user_new_24_male_sp
                        ->whereBetween('created_at', [$past_1, $today])
                        ->where('gender', 'LIKE', 'male')
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_new_24_male_sp'] = $user_new_24_male_sp;

                    $user_new_24_female_sp = $user_new_24_female_sp
                        ->whereBetween('created_at', [$past_1, $today])
                        ->where('gender', 'LIKE', 'female')
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_new_24_female_sp'] = $user_new_24_female_sp;

                    /** new registered users 7 days */
                    $user_new_7d_all_sp = $user_new_7d_all_sp
                        ->whereBetween('created_at', [$past_7, $today])
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_new_7d_all_sp'] = $user_new_7d_all_sp;

                    $user_new_7d_verified_sp = $user_new_7d_verified_sp
                        ->whereBetween('created_at', [$past_7, $today])
                        ->where('verified', '=', 1)
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_new_7d_verified_sp'] = $user_new_7d_verified_sp;

                    $user_new_7d_male_sp = $user_new_7d_male_sp
                        ->whereBetween('created_at', [$past_7, $today])
                        ->where('gender', 'LIKE', 'male')
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_new_7d_male_sp'] = $user_new_7d_male_sp;

                    $user_new_7d_female_sp = $user_new_7d_female_sp
                        ->whereBetween('created_at', [$past_7, $today])
                        ->where('gender', 'LIKE', 'female')
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_new_7d_female_sp'] = $user_new_7d_female_sp;

                    /** new registered users 30 days */
                    $user_new_30d_all_sp = $user_new_30d_all_sp
                        ->whereBetween('created_at', [$past_30, $today])
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_new_30d_all_sp'] = $user_new_30d_all_sp;

                    $user_new_30d_verified_sp = $user_new_30d_verified_sp
                        ->whereBetween('created_at', [$past_30, $today])
                        ->where('verified', '=', 1)
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_new_30d_verified_sp'] = $user_new_30d_verified_sp;

                    $user_new_30d_male_sp = $user_new_30d_male_sp
                        ->whereBetween('created_at', [$past_30, $today])
                        ->where('gender', 'LIKE', 'male')
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_new_30d_male_sp'] = $user_new_30d_male_sp;

                    $user_new_30d_female_sp = $user_new_30d_female_sp
                        ->whereBetween('created_at', [$past_30, $today])
                        ->where('gender', 'LIKE', 'female')
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_new_30d_female_sp'] = $user_new_30d_female_sp;

                    /** new registered users 90 days */
                    $user_new_90d_all_sp = $user_new_90d_all_sp
                        ->whereBetween('created_at', [$past_90, $today])
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_new_90d_all_sp'] = $user_new_90d_all_sp;

                    $user_new_90d_verified_sp = $user_new_90d_verified_sp
                        ->whereBetween('created_at', [$past_90, $today])
                        ->where('verified', '=', 1)
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_new_90d_verified_sp'] = $user_new_90d_verified_sp;

                    $user_new_90d_male_sp = $user_new_90d_male_sp
                        ->whereBetween('created_at', [$past_90, $today])
                        ->where('gender', 'LIKE', 'male')
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })
                        ->count();
                    $statistics['user_new_90d_male_sp'] = $user_new_90d_male_sp;

                    $user_new_90d_female_sp = $user_new_90d_female_sp
                        ->whereBetween('created_at', [$past_90, $today])
                        ->where('gender', 'LIKE', 'female')
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_new_90d_female_sp'] = $user_new_90d_female_sp;

                    break;

                case 2:

                    /** all Users for age sport*/
                    $users_all_sp = new User;
                    $users_reg_veri_sp = new User;
                    $users_reg_male_sp = new User;
                    $users_reg_female_sp = new User;
                    $i_12_sp = 0;
                    $i_13_sp = 0;
                    $i_18_sp = 0;
                    $i_25_sp = 0;
                    $i_35_sp = 0;
                    $i_45_sp = 0;
                    $i_55_sp = 0;
                    $i_65_sp = 0;

                    /** verified Users for age sport*/
                    $i_12_veri_sp = 0;
                    $i_13_veri_sp = 0;
                    $i_18_veri_sp = 0;
                    $i_25_veri_sp = 0;
                    $i_35_veri_sp = 0;
                    $i_45_veri_sp = 0;
                    $i_55_veri_sp = 0;
                    $i_65_veri_sp = 0;

                    /** male Users for age sport*/
                    $i_12_male_sp = 0;
                    $i_13_male_sp = 0;
                    $i_18_male_sp = 0;
                    $i_25_male_sp = 0;
                    $i_35_male_sp = 0;
                    $i_45_male_sp = 0;
                    $i_55_male_sp = 0;
                    $i_65_male_sp = 0;

                    /** female Users for age sport*/
                    $i_12_female_sp = 0;
                    $i_13_female_sp = 0;
                    $i_18_female_sp = 0;
                    $i_25_female_sp = 0;
                    $i_35_female_sp = 0;
                    $i_45_female_sp = 0;
                    $i_55_female_sp = 0;
                    $i_65_female_sp = 0;

                    /** User age */
                    $users_all_sp = $users_all_sp
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->pluck('birthday');

                    $users_reg_veri_sp = $users_reg_veri_sp
                        ->where('verified', '=', 1)
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->pluck('birthday');

                    $users_reg_male_sp = $users_reg_male_sp
                        ->where('gender', 'LIKE', 'male')
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->pluck('birthday');

                    $users_reg_female_sp = $users_reg_female_sp
                        ->where('gender', 'LIKE', 'female')
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->pluck('birthday');


                    foreach ($users_all_sp as $user){
                        if (is_null($user) || $user == '') continue;
                        $diff = date_diff($user, $today_new);
                        $age = $diff->format('%y');

                        if ($age < 12){
                            $i_12_sp++;
                        }
                        if ($age >= 13 && $age <= 17){
                            $i_13_sp++;
                        }
                        if ($age >= 18 && $age <= 24){
                            $i_18_sp++;
                        }
                        if ($age >= 25 && $age <= 34){
                            $i_25_sp++;
                        }
                        if ($age >= 35 && $age <= 44){
                            $i_35_sp++;
                        }
                        if ($age >= 45 && $age <= 54){
                            $i_45_sp++;
                        }
                        if ($age >= 55 && $age <= 64){
                            $i_55_sp++;
                        }
                        if ($age >= 65){
                            $i_65_sp++;
                        }

                    }

                    $statistics['12_sp'] = $i_12_sp;
                    $statistics['13_sp'] = $i_13_sp;
                    $statistics['18_sp'] = $i_18_sp;
                    $statistics['25_sp'] = $i_25_sp;
                    $statistics['35_sp'] = $i_35_sp;
                    $statistics['45_sp'] = $i_45_sp;
                    $statistics['55_sp'] = $i_55_sp;
                    $statistics['65_sp'] = $i_65_sp;


                    foreach ($users_reg_veri_sp as $user_veri){
                        if (is_null($user_veri) || $user_veri == '') continue;
                        $diff_veri = date_diff($user_veri, $today_new);
                        $age_veri = $diff_veri->format('%y');

                        if ($age_veri < 12){
                            $i_12_veri_sp++;
                        }
                        if ($age_veri >= 13 && $age_veri <= 17){
                            $i_13_veri_sp++;
                        }
                        if ($age_veri >= 18 && $age_veri <= 24){
                            $i_18_veri_sp++;
                        }
                        if ($age_veri >= 25 && $age_veri <= 34){
                            $i_25_veri_sp++;
                        }
                        if ($age_veri >= 35 && $age_veri <= 44){
                            $i_35_veri_sp++;
                        }
                        if ($age_veri >= 45 && $age_veri <= 54){
                            $i_45_veri_sp++;
                        }
                        if ($age_veri >= 55 && $age_veri <= 64){
                            $i_55_veri_sp++;
                        }
                        if ($age_veri >= 65){
                            $i_65_veri_sp++;
                        }
                    }

                    $statistics['12_veri_sp'] = $i_12_veri_sp;
                    $statistics['13_veri_sp'] = $i_13_veri_sp;
                    $statistics['18_veri_sp'] = $i_18_veri_sp;
                    $statistics['25_veri_sp'] = $i_25_veri_sp;
                    $statistics['35_veri_sp'] = $i_35_veri_sp;
                    $statistics['45_veri_sp'] = $i_45_veri_sp;
                    $statistics['55_veri_sp'] = $i_55_veri_sp;
                    $statistics['65_veri_sp'] = $i_65_veri_sp;


                    foreach ($users_reg_male_sp as $user_male){
                        if (is_null($user_male) || $user_male == '') continue;
                        $diff_male = date_diff($user_male, $today_new);
                        $age_male = $diff_male->format('%y');

                        if ($age_male < 12){
                            $i_12_male_sp++;
                        }
                        if ($age_male >= 13 && $age_male <= 17){
                            $i_13_male_sp++;
                        }
                        if ($age_male >= 18 && $age_male <= 24){
                            $i_18_male_sp++;
                        }
                        if ($age_male >= 25 && $age_male <= 34){
                            $i_25_male_sp++;
                        }
                        if ($age_male >= 35 && $age_male <= 44){
                            $i_35_male_sp++;
                        }
                        if ($age_male >= 45 && $age_male <= 54){
                            $i_45_male_sp++;
                        }
                        if ($age_male >= 55 && $age_male <= 64){
                            $i_55_male_sp++;
                        }
                        if ($age_male >= 65){
                            $i_65_male_sp++;
                        }
                    }

                    $statistics['12_male_sp'] = $i_12_male_sp;
                    $statistics['13_male_sp'] = $i_13_male_sp;
                    $statistics['18_male_sp'] = $i_18_male_sp;
                    $statistics['25_male_sp'] = $i_25_male_sp;
                    $statistics['35_male_sp'] = $i_35_male_sp;
                    $statistics['45_male_sp'] = $i_45_male_sp;
                    $statistics['55_male_sp'] = $i_55_male_sp;
                    $statistics['65_male_sp'] = $i_65_male_sp;


                    foreach ($users_reg_female_sp as $user_female){
                        if (is_null($user_female) || $user_female == '') continue;
                        $diff_female = date_diff($user_female, $today_new);
                        $age_female = $diff_female->format('%y');

                        if ($age_female < 12){
                            $i_12_female_sp++;
                        }
                        if ($age_female >= 13 && $age_female <= 17){
                            $i_13_female_sp++;
                        }
                        if ($age_female >= 18 && $age_female <= 24){
                            $i_18_female_sp++;
                        }
                        if ($age_female >= 25 && $age_female <= 34){
                            $i_25_female_sp++;
                        }
                        if ($age_female >= 35 && $age_female <= 44){
                            $i_35_female_sp++;
                        }
                        if ($age_female >= 45 && $age_female <= 54){
                            $i_45_female_sp++;
                        }
                        if ($age_female >= 55 && $age_female <= 64){
                            $i_55_female_sp++;
                        }
                        if ($age_female >= 65){
                            $i_65_female_sp++;
                        }
                    }

                    $statistics['12_female_sp'] = $i_12_female_sp;
                    $statistics['13_female_sp'] = $i_13_female_sp;
                    $statistics['18_female_sp'] = $i_18_female_sp;
                    $statistics['25_female_sp'] = $i_25_female_sp;
                    $statistics['35_female_sp'] = $i_35_female_sp;
                    $statistics['45_female_sp'] = $i_45_female_sp;
                    $statistics['55_female_sp'] = $i_55_female_sp;
                    $statistics['65_female_sp'] = $i_65_female_sp;

                    break;

                case 3:

                    /** activity 24h sport*/
                    $user_act_24_all_sp = new User;
                    $user_act_24_male_sp = new User;
                    $user_act_24_female_sp = new User;

                    /** activity 7d sport*/
                    $user_act_7d_all_sp = new User;
                    $user_act_7d_male_sp = new User;
                    $user_act_7d_female_sp = new User;

                    /** activity 30d sport*/
                    $user_act_30d_all_sp = new User;
                    $user_act_30d_male_sp = new User;
                    $user_act_30d_female_sp = new User;

                    /** activity 90d sport*/
                    $user_act_90d_all_sp = new User;
                    $user_act_90d_male_sp = new User;
                    $user_act_90d_female_sp = new User;

                    /** User Activity */
                    /** activity 24h */
                    $user_act_24_all_sp = $user_act_24_all_sp
                        ->whereBetween('login_at', [$past_1, $today])
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_act_24_all_sp'] = $user_act_24_all_sp;

                    $user_act_24_male_sp = $user_act_24_male_sp
                        ->whereBetween('login_at', [$past_1, $today])
                        ->where('gender', 'LIKE', 'male')
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_act_24_male_sp'] = $user_act_24_male_sp;

                    $user_act_24_female_sp = $user_act_24_female_sp
                        ->whereBetween('login_at', [$past_1, $today])
                        ->where('gender', 'LIKE', 'female')
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_act_24_female_sp'] = $user_act_24_female_sp;

                    /** activity 7d */
                    $user_act_7d_all_sp = $user_act_7d_all_sp
                        ->whereBetween('login_at', [$past_7, $today])
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_act_7d_all_sp'] = $user_act_7d_all_sp;

                    $user_act_7d_male_sp = $user_act_7d_male_sp
                        ->whereBetween('login_at', [$past_7, $today])
                        ->where('gender', 'LIKE', 'male')
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_act_7d_male_sp'] = $user_act_7d_male_sp;

                    $user_act_7d_female_sp = $user_act_7d_female_sp
                        ->whereBetween('login_at', [$past_7, $today])
                        ->where('gender', 'LIKE', 'female')
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_act_7d_female_sp'] = $user_act_7d_female_sp;

                    /** activity 30d */
                    $user_act_30d_all_sp = $user_act_30d_all_sp
                        ->whereBetween('login_at', [$past_30, $today])
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_act_30d_all_sp'] = $user_act_30d_all_sp;

                    $user_act_30d_male_sp = $user_act_30d_male_sp
                        ->whereBetween('login_at', [$past_30, $today])
                        ->where('gender', 'LIKE', 'male')
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_act_30d_male_sp'] = $user_act_30d_male_sp;

                    $user_act_30d_female_sp = $user_act_30d_female_sp
                        ->whereBetween('login_at', [$past_30, $today])
                        ->where('gender', 'LIKE', 'female')
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_act_30d_female_sp'] = $user_act_30d_female_sp;

                    /** activity 90d */
                    $user_act_90d_all_sp = $user_act_90d_all_sp
                        ->whereBetween('login_at', [$past_90, $today])
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_act_90d_all_sp'] = $user_act_90d_all_sp;

                    $user_act_90d_male_sp = $user_act_90d_male_sp
                        ->whereBetween('login_at', [$past_90, $today])
                        ->where('gender', 'LIKE', 'male')
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_act_90d_male_sp'] = $user_act_90d_male_sp;

                    $user_act_90d_female_sp = $user_act_90d_female_sp
                        ->whereBetween('login_at', [$past_90, $today])
                        ->where('gender', 'LIKE', 'female')
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_act_90d_female_sp'] = $user_act_90d_female_sp;

                    break;

                case 4:

                    /** user mandatory part sport*/
                    $user_mand_part_all_sp = new User;
                    $user_mand_part_male_sp = new User;
                    $user_mand_part_female_sp = new User;

                    /** user mandatory full sport*/
                    $user_mand_all_sp = new User;
                    $user_mand_male_sp = new User;
                    $user_mand_female_sp = new User;

                    /** user sportvita sport*/
                    $user_sportvita_all_sp = new User;
                    $user_sportvita_male_sp = new User;
                    $user_sportvita_female_sp = new User;

                    /** user profile pic sport*/
                    $user_profile_pic_all_sp = new User;
                    $user_profile_pic_male_sp = new User;
                    $user_profile_pic_female_sp = new User;

                    /** User intensity */
                    /** user profile mandatory part */
                    $user_mand_part_all_sp = $user_mand_part_all_sp
                        ->where(function($query){
                            $query->whereNull('firstname')
                                  ->orWhereNull('lastname')
                                  ->orWhereNull('birthday')
                                  ->orWhereNull('city')
                                  ->orWhereNull('zip');
                        })
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_mand_part_all_sp'] = $user_mand_part_all_sp;

                    $user_mand_part_male_sp = $user_mand_part_male_sp
                        ->where('gender', 'LIKE', 'male')
                        ->where(function($query){
                            $query->whereNull('firstname')
                                  ->orWhereNull('lastname')
                                  ->orWhereNull('birthday')
                                  ->orWhereNull('city')
                                  ->orWhereNull('zip');
                        })
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_mand_part_male_sp'] = $user_mand_part_male_sp;

                    $user_mand_part_female_sp = $user_mand_part_female_sp
                        ->where('gender', 'LIKE', 'female')
                        ->where(function($query){
                            $query->whereNull('firstname')
                                  ->orWhereNull('lastname')
                                  ->orWhereNull('birthday')
                                  ->orWhereNull('city')
                                  ->orWhereNull('zip');
                        })
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_mand_part_female_sp'] = $user_mand_part_female_sp;

                    /** user profile mandatory full */
                    $user_mand_all_sp = $user_mand_all_sp
                        ->whereNotNull('firstname')->whereNotNull('lastname')
                        ->whereNotNull('birthday')->whereNotNull('city')
                        ->whereNotNull('gender')->whereNotNull('zip')
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_mand_all_sp'] = $user_mand_all_sp;

                    $user_mand_male_sp = $user_mand_male_sp
                        ->where('gender', 'LIKE', 'male')
                        ->whereNotNull('firstname')
                        ->whereNotNull('lastname')
                        ->whereNotNull('birthday')
                        ->whereNotNull('city')
                        ->whereNotNull('zip')
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_mand_male_sp'] = $user_mand_male_sp;

                    $user_mand_female_sp = $user_mand_female_sp
                        ->where('gender', 'LIKE', 'female')
                        ->whereNotNull('firstname')
                        ->whereNotNull('lastname')
                        ->whereNotNull('birthday')
                        ->whereNotNull('city')
                        ->whereNotNull('zip')
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_mand_female_sp'] = $user_mand_female_sp;

                    /** user sportvita */
                    $user_sportvita_all_sp = $user_sportvita_all_sp
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_sportvita_all_sp'] = $user_sportvita_all_sp;

                    $user_sportvita_male_sp = $user_sportvita_male_sp
                        ->where('gender', 'LIKE', 'male')
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })
                        ->count();
                    $statistics['user_sportvita_male_sp'] = $user_sportvita_male_sp;

                    $user_sportvita_female_sp = $user_sportvita_female_sp
                        ->where('gender', 'LIKE', 'female')
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_sportvita_female_sp'] = $user_sportvita_female_sp;


                    /** user profile pic */
                    $user_profile_pic_all_sp = $user_profile_pic_all_sp
                        ->whereNotNull('avatar_file_name')
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_profile_pic_all_sp'] = $user_profile_pic_all_sp;

                    $user_profile_pic_male_sp = $user_profile_pic_male_sp
                        ->whereNotNull('avatar_file_name')
                        ->where('gender', 'LIKE', 'male')
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_profile_pic_male_sp'] = $user_profile_pic_male_sp;

                    $user_profile_pic_female_sp = $user_profile_pic_female_sp
                        ->whereNotNull('avatar_file_name')
                        ->where('gender', 'LIKE', 'male')
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_profile_pic_female_sp'] = $user_profile_pic_female_sp;

                    break;

                case 5:

                    /** user deletion profile 24h sport*/
                    $user_del_24_all_sp = new User;
                    $user_del_24_male_sp = new User;
                    $user_del_24_female_sp = new User;

                    /** user deletion profile 7d sport*/
                    $user_del_7_all_sp = new User;
                    $user_del_7_male_sp = new User;
                    $user_del_7_female_sp = new User;

                    /** user deletion profile 30d sport*/
                    $user_del_30_all_sp = new User;
                    $user_del_30_male_sp = new User;
                    $user_del_30_female_sp = new User;

                    /** user deletion profile 90d sport*/
                    $user_del_90_all_sp = new User;
                    $user_del_90_male_sp = new User;
                    $user_del_90_female_sp = new User;

                    /** user deletion profile 24h*/
                    $user_del_24_all_sp = $user_del_24_all_sp
                        ->onlyTrashed()
                        ->whereBetween('deleted_at', [$past_1, $today])
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_del_24_all_sp'] = $user_del_24_all_sp;

                    $user_del_24_male_sp = $user_del_24_male_sp
                        ->onlyTrashed()
                        ->where('gender', 'LIKE', 'male')
                        ->whereBetween('deleted_at', [$past_1, $today])
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_del_24_male_sp'] = $user_del_24_male_sp;

                    $user_del_24_female_sp = $user_del_24_female_sp
                        ->onlyTrashed()
                        ->where('gender', 'LIKE', 'female')
                        ->whereBetween('deleted_at', [$past_1, $today])
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_del_24_female_sp'] = $user_del_24_female_sp;

                    /** user deletion profile 7d */
                    $user_del_7_all_sp = $user_del_7_all_sp
                        ->onlyTrashed()
                        ->whereBetween('deleted_at', [$past_7, $today])
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_del_7_all_sp'] = $user_del_7_all_sp;

                    $user_del_7_male_sp = $user_del_7_male_sp
                        ->onlyTrashed()
                        ->where('gender', 'LIKE', 'male')
                        ->whereBetween('deleted_at', [$past_7, $today])
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_del_7_male_sp'] = $user_del_7_male_sp;

                    $user_del_7_female_sp = $user_del_7_female_sp
                        ->onlyTrashed()
                        ->where('gender', 'LIKE', 'female')
                        ->whereBetween('deleted_at', [$past_7, $today])
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_del_7_female_sp'] = $user_del_7_female_sp;

                    /** user deletion profile 30d */
                    $user_del_30_all_sp = $user_del_30_all_sp
                        ->onlyTrashed()
                        ->whereBetween('deleted_at', [$past_30, $today])
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_del_30_all_sp'] = $user_del_30_all_sp;

                    $user_del_30_male_sp = $user_del_30_male_sp
                        ->onlyTrashed()
                        ->where('gender', 'LIKE', 'male')
                        ->whereBetween('deleted_at', [$past_30, $today])
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_del_30_male_sp'] = $user_del_30_male_sp;

                    $user_del_30_female_sp = $user_del_30_female_sp
                        ->onlyTrashed()
                        ->where('gender', 'LIKE', 'female')
                        ->whereBetween('deleted_at', [$past_30, $today])
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_del_30_female_sp'] = $user_del_30_female_sp;

                    /** user deletion profile 90d */
                    $user_del_90_all_sp = $user_del_90_all_sp
                        ->onlyTrashed()
                        ->whereBetween('deleted_at', [$past_90, $today])
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_del_90_all_sp'] = $user_del_90_all_sp;

                    $user_del_90_male_sp = $user_del_90_male_sp
                        ->onlyTrashed()
                        ->where('gender', 'LIKE', 'male')
                        ->whereBetween('deleted_at', [$past_90, $today])
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_del_90_male_sp'] = $user_del_90_male_sp;

                    $user_del_90_female_sp = $user_del_90_female_sp
                        ->onlyTrashed()
                        ->where('gender', 'LIKE', 'female')
                        ->whereBetween('deleted_at', [$past_90, $today])
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })->count();
                    $statistics['user_del_90_female_sp'] = $user_del_90_female_sp;

                    break;

            }

        }

        /** all users */
        $all_users = $all_users->count();
        $statistics['all_users'] = $all_users;


        $statistics['federal'] = 0;
        $statistics['sport'] = 0;


        if ($fed_state !== '' && $fed_state !== 'all'){

            switch ($abschnitt_auswahl){
                case 0:
                    /** USERS for federal
                     * registered users federal*/
                    $all_users_fed = new User;
                    $user_registered_fed = new User;
                    $user_reg_verified_fed = new User;
                    $user_reg_male_fed = new User;
                    $user_reg_female_fed = new User;

                    /** all users federal*/
                    $all_users_fed = $all_users_fed->where('federal_state', 'LIKE', $fed_state)->count();
                    $statistics['all_users_fed'] = $all_users_fed;

                    $user_registered_fed = $user_registered_fed->where('federal_state', 'LIKE', $fed_state)->count();
                    $statistics['user_registered_fed'] = $user_registered_fed;

                    $user_reg_verified_fed = $user_reg_verified_fed->where('federal_state', 'LIKE', $fed_state)
                                                                   ->where('verified', '=', 1)->count();
                    $statistics['user_reg_verified_fed'] = $user_reg_verified_fed;

                    $user_reg_male_fed = $user_reg_male_fed->where('federal_state', 'LIKE', $fed_state)
                                                           ->where('gender', 'LIKE', 'male')->count();
                    $statistics['user_reg_male_fed'] = $user_reg_male_fed;

                    $user_reg_female_fed = $user_reg_female_fed->where('federal_state', 'LIKE', $fed_state)
                                                               ->where('gender', 'LIKE', 'female')->count();
                    $statistics['user_reg_female_fed'] = $user_reg_female_fed;

                    break;

                case 1:
                    /** new registered users 24h federal*/
                    $user_new_24_all_fed = new User;
                    $user_new_24_verified_fed = new User;
                    $user_new_24_male_fed = new User;
                    $user_new_24_female_fed = new User;

                    /** new registered users 7 days federal*/
                    $user_new_7d_all_fed = new User;
                    $user_new_7d_verified_fed = new User;
                    $user_new_7d_male_fed = new User;
                    $user_new_7d_female_fed = new User;

                    /** new registered users 30 days federal*/
                    $user_new_30d_all_fed = new User;
                    $user_new_30d_verified_fed = new User;
                    $user_new_30d_male_fed = new User;
                    $user_new_30d_female_fed = new User;

                    /** new registered users 90 days federal*/
                    $user_new_90d_all_fed = new User;
                    $user_new_90d_verified_fed = new User;
                    $user_new_90d_male_fed = new User;
                    $user_new_90d_female_fed = new User;


                    /** new registered users 24h*/
                    $user_new_24_all_fed = $user_new_24_all_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->whereBetween('created_at', [$past_1, $today])->count();
                    $statistics['user_new_24_all_fed'] = $user_new_24_all_fed;

                    $user_new_24_verified_fed = $user_new_24_verified_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->whereBetween('created_at', [$past_1, $today])->where('verified', '=', 1)->count();
                    $statistics['user_new_24_verified_fed'] = $user_new_24_verified_fed;

                    $user_new_24_male_fed = $user_new_24_male_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->whereBetween('created_at', [$past_1, $today])->where('gender', 'LIKE', 'male')->count();
                    $statistics['user_new_24_male_fed'] = $user_new_24_male_fed;

                    $user_new_24_female_fed = $user_new_24_female_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->whereBetween('created_at', [$past_1, $today])->where('gender', 'LIKE', 'female')->count();
                    $statistics['user_new_24_female_fed'] = $user_new_24_female_fed;

                    /** new registered users 7 days */
                    $user_new_7d_all_fed = $user_new_7d_all_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->whereBetween('created_at', [$past_7, $today])->count();
                    $statistics['user_new_7d_all_fed'] = $user_new_7d_all_fed;

                    $user_new_7d_verified_fed = $user_new_7d_verified_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->whereBetween('created_at', [$past_7, $today])->where('verified', '=', 1)->count();
                    $statistics['user_new_7d_verified_fed'] = $user_new_7d_verified_fed;

                    $user_new_7d_male_fed = $user_new_7d_male_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->whereBetween('created_at', [$past_7, $today])->where('gender', 'LIKE', 'male')->count();
                    $statistics['user_new_7d_male_fed'] = $user_new_7d_male_fed;

                    $user_new_7d_female_fed = $user_new_7d_female_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->whereBetween('created_at', [$past_7, $today])->where('gender', 'LIKE', 'female')->count();
                    $statistics['user_new_7d_female_fed'] = $user_new_7d_female_fed;

                    /** new registered users 30 days */
                    $user_new_30d_all_fed = $user_new_30d_all_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->whereBetween('created_at', [$past_30, $today])->count();
                    $statistics['user_new_30d_all_fed'] = $user_new_30d_all_fed;

                    $user_new_30d_verified_fed = $user_new_30d_verified_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->whereBetween('created_at', [$past_30, $today])->where('verified', '=', 1)->count();
                    $statistics['user_new_30d_verified_fed'] = $user_new_30d_verified_fed;

                    $user_new_30d_male_fed = $user_new_30d_male_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->whereBetween('created_at', [$past_30, $today])->where('gender', 'LIKE', 'male')->count();
                    $statistics['user_new_30d_male_fed'] = $user_new_30d_male_fed;

                    $user_new_30d_female_fed = $user_new_30d_female_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->whereBetween('created_at', [$past_30, $today])->where('gender', 'LIKE', 'female')->count();
                    $statistics['user_new_30d_female_fed'] = $user_new_30d_female_fed;

                    /** new registered users 90 days */
                    $user_new_90d_all_fed = $user_new_90d_all_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->whereBetween('created_at', [$past_90, $today])->count();
                    $statistics['user_new_90d_all_fed'] = $user_new_90d_all_fed;

                    $user_new_90d_verified_fed = $user_new_90d_verified_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->whereBetween('created_at', [$past_90, $today])->where('verified', '=', 1)->count();
                    $statistics['user_new_90d_verified_fed'] = $user_new_90d_verified_fed;

                    $user_new_90d_male_fed = $user_new_90d_male_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->whereBetween('created_at', [$past_90, $today])->where('gender', 'LIKE', 'male')->count();
                    $statistics['user_new_90d_male_fed'] = $user_new_90d_male_fed;

                    $user_new_90d_female_fed = $user_new_90d_female_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->whereBetween('created_at', [$past_90, $today])->where('gender', 'LIKE', 'female')->count();
                    $statistics['user_new_90d_female_fed'] = $user_new_90d_female_fed;

                    break;

                case 2:
                    /** all Users for age federal*/
                    $users_all_fed = new User;
                    $users_reg_veri_fed = new User;
                    $users_reg_male_fed = new User;
                    $users_reg_female_fed = new User;
                    $i_12_fed = 0;
                    $i_13_fed = 0;
                    $i_18_fed = 0;
                    $i_25_fed = 0;
                    $i_35_fed = 0;
                    $i_45_fed = 0;
                    $i_55_fed = 0;
                    $i_65_fed = 0;

                    /** verified Users for age federal*/
                    $i_12_veri_fed = 0;
                    $i_13_veri_fed = 0;
                    $i_18_veri_fed = 0;
                    $i_25_veri_fed = 0;
                    $i_35_veri_fed = 0;
                    $i_45_veri_fed = 0;
                    $i_55_veri_fed = 0;
                    $i_65_veri_fed = 0;

                    /** male Users for age federal*/
                    $i_12_male_fed = 0;
                    $i_13_male_fed = 0;
                    $i_18_male_fed = 0;
                    $i_25_male_fed = 0;
                    $i_35_male_fed = 0;
                    $i_45_male_fed = 0;
                    $i_55_male_fed = 0;
                    $i_65_male_fed = 0;

                    /** female Users for age federal*/
                    $i_12_female_fed = 0;
                    $i_13_female_fed = 0;
                    $i_18_female_fed = 0;
                    $i_25_female_fed = 0;
                    $i_35_female_fed = 0;
                    $i_45_female_fed = 0;
                    $i_55_female_fed = 0;
                    $i_65_female_fed = 0;

                    /** User age */
                    $users_all_fed = $users_all_fed->where('federal_state', 'LIKE', $fed_state)->pluck('birthday');
                    $users_reg_veri_fed = $users_reg_veri_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->where('verified', '=', 1)->pluck('birthday');
                    $users_reg_male_fed = $users_reg_male_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->where('gender', 'LIKE', 'male')->pluck('birthday');
                    $users_reg_female_fed = $users_reg_female_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->where('gender', 'LIKE', 'female')->pluck('birthday');


                    foreach ($users_all_fed as $user){
                        if (is_null($user) || $user == '') continue;
                        $diff = date_diff($user, $today_new);
                        $age = $diff->format('%y');

                        if ($age < 12){
                            $i_12_fed++;
                        }
                        if ($age >= 13 && $age <= 17){
                            $i_13_fed++;
                        }
                        if ($age >= 18 && $age <= 24){
                            $i_18_fed++;
                        }
                        if ($age >= 25 && $age <= 34){
                            $i_25_fed++;
                        }
                        if ($age >= 35 && $age <= 44){
                            $i_35_fed++;
                        }
                        if ($age >= 45 && $age <= 54){
                            $i_45_fed++;
                        }
                        if ($age >= 55 && $age <= 64){
                            $i_55_fed++;
                        }
                        if ($age >= 65){
                            $i_65_fed++;
                        }

                    }

                    $statistics['12_fed'] = $i_12_fed;
                    $statistics['13_fed'] = $i_13_fed;
                    $statistics['18_fed'] = $i_18_fed;
                    $statistics['25_fed'] = $i_25_fed;
                    $statistics['35_fed'] = $i_35_fed;
                    $statistics['45_fed'] = $i_45_fed;
                    $statistics['55_fed'] = $i_55_fed;
                    $statistics['65_fed'] = $i_65_fed;

                    foreach ($users_reg_veri_fed as $user_veri){
                        if (is_null($user_veri) || $user_veri == '') continue;
                        $diff_veri = date_diff($user_veri, $today_new);
                        $age_veri = $diff_veri->format('%y');

                        if ($age_veri < 12){
                            $i_12_veri_fed++;
                        }
                        if ($age_veri >= 13 && $age_veri <= 17){
                            $i_13_veri_fed++;
                        }
                        if ($age_veri >= 18 && $age_veri <= 24){
                            $i_18_veri_fed++;
                        }
                        if ($age_veri >= 25 && $age_veri <= 34){
                            $i_25_veri_fed++;
                        }
                        if ($age_veri >= 35 && $age_veri <= 44){
                            $i_35_veri_fed++;
                        }
                        if ($age_veri >= 45 && $age_veri <= 54){
                            $i_45_veri_fed++;
                        }
                        if ($age_veri >= 55 && $age_veri <= 64){
                            $i_55_veri_fed++;
                        }
                        if ($age_veri >= 65){
                            $i_65_veri_fed++;
                        }
                    }

                    $statistics['12_veri_fed'] = $i_12_veri_fed;
                    $statistics['13_veri_fed'] = $i_13_veri_fed;
                    $statistics['18_veri_fed'] = $i_18_veri_fed;
                    $statistics['25_veri_fed'] = $i_25_veri_fed;
                    $statistics['35_veri_fed'] = $i_35_veri_fed;
                    $statistics['45_veri_fed'] = $i_45_veri_fed;
                    $statistics['55_veri_fed'] = $i_55_veri_fed;
                    $statistics['65_veri_fed'] = $i_65_veri_fed;

                    foreach ($users_reg_male_fed as $user_male){
                        if (is_null($user_male) || $user_male == '') continue;
                        $diff_male = date_diff($user_male, $today_new);
                        $age_male = $diff_male->format('%y');

                        if ($age_male < 12){
                            $i_12_male_fed++;
                        }
                        if ($age_male >= 13 && $age_male <= 17){
                            $i_13_male_fed++;
                        }
                        if ($age_male >= 18 && $age_male <= 24){
                            $i_18_male_fed++;
                        }
                        if ($age_male >= 25 && $age_male <= 34){
                            $i_25_male_fed++;
                        }
                        if ($age_male >= 35 && $age_male <= 44){
                            $i_35_male_fed++;
                        }
                        if ($age_male >= 45 && $age_male <= 54){
                            $i_45_male_fed++;
                        }
                        if ($age_male >= 55 && $age_male <= 64){
                            $i_55_male_fed++;
                        }
                        if ($age_male >= 65){
                            $i_65_male_fed++;
                        }
                    }

                    $statistics['12_male_fed'] = $i_12_male_fed;
                    $statistics['13_male_fed'] = $i_13_male_fed;
                    $statistics['18_male_fed'] = $i_18_male_fed;
                    $statistics['25_male_fed'] = $i_25_male_fed;
                    $statistics['35_male_fed'] = $i_35_male_fed;
                    $statistics['45_male_fed'] = $i_45_male_fed;
                    $statistics['55_male_fed'] = $i_55_male_fed;
                    $statistics['65_male_fed'] = $i_65_male_fed;

                    foreach ($users_reg_female_fed as $user_female){
                        if (is_null($user_female) || $user_female == '') continue;
                        $diff_female = date_diff($user_female, $today_new);
                        $age_female = $diff_female->format('%y');

                        if ($age_female < 12){
                            $i_12_female_fed++;
                        }
                        if ($age_female >= 13 && $age_female <= 17){
                            $i_13_female_fed++;
                        }
                        if ($age_female >= 18 && $age_female <= 24){
                            $i_18_female_fed++;
                        }
                        if ($age_female >= 25 && $age_female <= 34){
                            $i_25_female_fed++;
                        }
                        if ($age_female >= 35 && $age_female <= 44){
                            $i_35_female_fed++;
                        }
                        if ($age_female >= 45 && $age_female <= 54){
                            $i_45_female_fed++;
                        }
                        if ($age_female >= 55 && $age_female <= 64){
                            $i_55_female_fed++;
                        }
                        if ($age_female >= 65){
                            $i_65_female_fed++;
                        }
                    }

                    $statistics['12_female_fed'] = $i_12_female_fed;
                    $statistics['13_female_fed'] = $i_13_female_fed;
                    $statistics['18_female_fed'] = $i_18_female_fed;
                    $statistics['25_female_fed'] = $i_25_female_fed;
                    $statistics['35_female_fed'] = $i_35_female_fed;
                    $statistics['45_female_fed'] = $i_45_female_fed;
                    $statistics['55_female_fed'] = $i_55_female_fed;
                    $statistics['65_female_fed'] = $i_65_female_fed;

                    break;


                case 3:
                    /** activity 24h federal*/
                    $user_act_24_all_fed = new User;
                    $user_act_24_male_fed = new User;
                    $user_act_24_female_fed = new User;

                    /** activity 7d federal*/
                    $user_act_7d_all_fed = new User;
                    $user_act_7d_male_fed = new User;
                    $user_act_7d_female_fed = new User;

                    /** activity 30d federal*/
                    $user_act_30d_all_fed = new User;
                    $user_act_30d_male_fed = new User;
                    $user_act_30d_female_fed = new User;

                    /** activity 90d federal*/
                    $user_act_90d_all_fed = new User;
                    $user_act_90d_male_fed = new User;
                    $user_act_90d_female_fed = new User;

                    /** User Activity */
                    /** activity 24h */
                    $user_act_24_all_fed = $user_act_24_all_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->whereBetween('login_at', [$past_1, $today])->count();
                    $statistics['user_act_24_all_fed'] = $user_act_24_all_fed;

                    $user_act_24_male_fed = $user_act_24_male_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->whereBetween('login_at', [$past_1, $today])->where('gender', 'LIKE', 'male')->count();
                    $statistics['user_act_24_male_fed'] = $user_act_24_male_fed;

                    $user_act_24_female_fed = $user_act_24_female_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->whereBetween('login_at', [$past_1, $today])->where('gender', 'LIKE', 'female')->count();
                    $statistics['user_act_24_female_fed'] = $user_act_24_female_fed;

                    /** activity 7d */
                    $user_act_7d_all_fed = $user_act_7d_all_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->whereBetween('login_at', [$past_7, $today])->count();
                    $statistics['user_act_7d_all_fed'] = $user_act_7d_all_fed;

                    $user_act_7d_male_fed = $user_act_7d_male_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->whereBetween('login_at', [$past_7, $today])->where('gender', 'LIKE', 'male')->count();
                    $statistics['user_act_7d_male_fed'] = $user_act_7d_male_fed;

                    $user_act_7d_female_fed = $user_act_7d_female_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->whereBetween('login_at', [$past_7, $today])->where('gender', 'LIKE', 'female')->count();
                    $statistics['user_act_7d_female_fed'] = $user_act_7d_female_fed;

                    /** activity 30d */
                    $user_act_30d_all_fed = $user_act_30d_all_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->whereBetween('login_at', [$past_30, $today])->count();
                    $statistics['user_act_30d_all_fed'] = $user_act_30d_all_fed;

                    $user_act_30d_male_fed = $user_act_30d_male_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->whereBetween('login_at', [$past_30, $today])->where('gender', 'LIKE', 'male')->count();
                    $statistics['user_act_30d_male_fed'] = $user_act_30d_male_fed;

                    $user_act_30d_female_fed = $user_act_30d_female_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->whereBetween('login_at', [$past_30, $today])->where('gender', 'LIKE', 'female')->count();
                    $statistics['user_act_30d_female_fed'] = $user_act_30d_female_fed;

                    /** activity 90d */
                    $user_act_90d_all_fed = $user_act_90d_all_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->whereBetween('login_at', [$past_90, $today])->count();
                    $statistics['user_act_90d_all_fed'] = $user_act_90d_all_fed;

                    $user_act_90d_male_fed = $user_act_90d_male_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->whereBetween('login_at', [$past_90, $today])->where('gender', 'LIKE', 'male')->count();
                    $statistics['user_act_90d_male_fed'] = $user_act_90d_male_fed;

                    $user_act_90d_female_fed = $user_act_90d_female_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->whereBetween('login_at', [$past_90, $today])->where('gender', 'LIKE', 'female')->count();
                    $statistics['user_act_90d_female_fed'] = $user_act_90d_female_fed;

                    break;

                case 4:

                    /** user mandatory part federal*/
                    $user_mand_part_all_fed = new User;
                    $user_mand_part_male_fed = new User;
                    $user_mand_part_female_fed = new User;

                    /** user mandatory full federal*/
                    $user_mand_all_fed = new User;
                    $user_mand_male_fed = new User;
                    $user_mand_female_fed = new User;

                    /** user sportvita federal*/
                    $user_sportvita_all_fed = new User;
                    $user_sportvita_male_fed = new User;
                    $user_sportvita_female_fed = new User;

                    /** user profile pic federal*/
                    $user_profile_pic_all_fed = new User;
                    $user_profile_pic_male_fed = new User;
                    $user_profile_pic_female_fed = new User;

                    /** User intensity */
                    /** user profile mandatory part */
                    $user_mand_part_all_fed = $user_mand_part_all_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->where(function($query){
                            $query->whereNull('firstname')
                                  ->orWhereNull('lastname')
                                  ->orWhereNull('birthday')
                                  ->orWhereNull('city')
                                  ->orWhereNull('zip');
                        })->count();
                    $statistics['user_mand_part_all_fed'] = $user_mand_part_all_fed;

                    $user_mand_part_male_fed = $user_mand_part_male_fed
                        ->where('federal_state', 'LIKE', $fed_state)->where('gender', 'LIKE', 'male')
                        ->where(function($query){
                            $query->whereNull('firstname')
                                  ->orWhereNull('lastname')
                                  ->orWhereNull('birthday')
                                  ->orWhereNull('city')
                                  ->orWhereNull('zip');
                        })->count();
                    $statistics['user_mand_part_male_fed'] = $user_mand_part_male_fed;

                    $user_mand_part_female_fed = $user_mand_part_female_fed
                        ->where('federal_state', 'LIKE', $fed_state)->where('gender', 'LIKE', 'female')
                        ->where(function($query){
                            $query->whereNull('firstname')
                                  ->orWhereNull('lastname')
                                  ->orWhereNull('birthday')
                                  ->orWhereNull('city')
                                  ->orWhereNull('zip');
                        })->count();
                    $statistics['user_mand_part_female_fed'] = $user_mand_part_female_fed;

                    /** user profile mandatory full */
                    $user_mand_all_fed = $user_mand_all_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->whereNotNull('firstname')->whereNotNull('lastname')->whereNotNull('birthday')->whereNotNull('city')
                        ->whereNotNull('gender')->whereNotNull('zip')->count();
                    $statistics['user_mand_all_fed'] = $user_mand_all_fed;

                    $user_mand_male_fed = $user_mand_male_fed
                        ->where('federal_state', 'LIKE', $fed_state)->where('gender', 'LIKE', 'male')
                        ->whereNotNull('firstname')->whereNotNull('lastname')->whereNotNull('birthday')->whereNotNull('city')
                        ->whereNotNull('zip')->count();
                    $statistics['user_mand_male_fed'] = $user_mand_male_fed;

                    $user_mand_female_fed = $user_mand_female_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->where('gender', 'LIKE', 'female')
                        ->whereNotNull('firstname')
                        ->whereNotNull('lastname')
                        ->whereNotNull('birthday')
                        ->whereNotNull('city')
                        ->whereNotNull('zip')->count();
                    $statistics['user_mand_female_fed'] = $user_mand_female_fed;


                    /** user sportvita */
                    $user_sportvita_all_fed = $user_sportvita_all_fed->where('federal_state', 'LIKE', $fed_state)->has('sports')->count();
                    $statistics['user_sportvita_all_fed'] = $user_sportvita_all_fed;

                    $user_sportvita_male_fed = $user_sportvita_male_fed->where('federal_state', 'LIKE', $fed_state)
                                                                       ->where('gender', 'LIKE', 'male')
                                                                       ->has('sports')->count();
                    $statistics['user_sportvita_male_fed'] = $user_sportvita_male_fed;

                    $user_sportvita_female_fed = $user_sportvita_female_fed->where('federal_state', 'LIKE', $fed_state)
                                                                           ->where('gender', 'LIKE', 'female')
                                                                           ->has('sports')->count();
                    $statistics['user_sportvita_female_fed'] = $user_sportvita_female_fed;


                    /** user profile pic */
                    $user_profile_pic_all_fed = $user_profile_pic_all_fed->where('federal_state', 'LIKE', $fed_state)
                                                                         ->whereNotNull('avatar_file_name')->count();
                    $statistics['user_profile_pic_all_fed'] = $user_profile_pic_all_fed;

                    $user_profile_pic_male_fed = $user_profile_pic_male_fed->where('federal_state', 'LIKE', $fed_state)
                                                                           ->whereNotNull('avatar_file_name')
                                                                           ->where('gender', 'LIKE', 'male')
                                                                           ->count();
                    $statistics['user_profile_pic_male_fed'] = $user_profile_pic_male_fed;

                    $user_profile_pic_female_fed = $user_profile_pic_female_fed->where('federal_state', 'LIKE', $fed_state)
                                                                               ->whereNotNull('avatar_file_name')
                                                                               ->where('gender', 'LIKE', 'male')
                                                                               ->count();
                    $statistics['user_profile_pic_female_fed'] = $user_profile_pic_female_fed;

                    break;

                case 5:
                    /** user deletion profile 24h federal*/
                    $user_del_24_all_fed = new User;
                    $user_del_24_male_fed = new User;
                    $user_del_24_female_fed = new User;

                    /** user deletion profile 7d federal*/
                    $user_del_7_all_fed = new User;
                    $user_del_7_male_fed = new User;
                    $user_del_7_female_fed = new User;

                    /** user deletion profile 30d federal*/
                    $user_del_30_all_fed = new User;
                    $user_del_30_male_fed = new User;
                    $user_del_30_female_fed = new User;

                    /** user deletion profile 90d federal*/
                    $user_del_90_all_fed = new User;
                    $user_del_90_male_fed = new User;
                    $user_del_90_female_fed = new User;

                    /** User deletion */
                    /** user deletion profile 24h*/
                    $user_del_24_all_fed =
                        DB::table('delusers')
                          ->where('federal_state', 'LIKE', $fed_state)
                          ->whereBetween('created_at', [$past_1, $today])->count();
                    $statistics['user_del_24_all_fed'] = $user_del_24_all_fed;

                    $user_del_24_male_fed = $user_del_24_male_fed =
                        DB::table('delusers')
                          ->where('federal_state', 'LIKE', $fed_state)
                          ->whereBetween('created_at', [$past_1, $today])->count();
                    $statistics['user_del_24_male_fed'] = $user_del_24_male_fed;

                    $user_del_24_female_fed =
                        DB::table('delusers')
                          ->where('federal_state', 'LIKE', $fed_state)
                          ->whereBetween('created_at', [$past_1, $today])->count();
                    $statistics['user_del_24_female_fed'] = $user_del_24_female_fed;

                    /** user deletion profile 7d */
                    $user_del_7_all_fed =
                        DB::table('delusers')
                          ->where('federal_state', 'LIKE', $fed_state)
                          ->whereBetween('created_at', [$past_7, $today])->count();
                    $statistics['user_del_7_all_fed'] = $user_del_7_all_fed;

                    $user_del_7_male_fed =
                        DB::table('delusers')
                          ->where('federal_state', 'LIKE', $fed_state)
                          ->whereBetween('created_at', [$past_7, $today])->count();
                    $statistics['user_del_7_male_fed'] = $user_del_7_male_fed;

                    $user_del_7_female_fed =
                        DB::table('delusers')
                          ->where('federal_state', 'LIKE', $fed_state)
                          ->whereBetween('created_at', [$past_7, $today])->count();
                    $statistics['user_del_7_female_fed'] = $user_del_7_female_fed;

                    /** user deletion profile 30d */
                    $user_del_30_all_fed =
                        DB::table('delusers')
                          ->where('federal_state', 'LIKE', $fed_state)
                          ->whereBetween('created_at', [$past_30, $today])->count();
                    $statistics['user_del_30_all_fed'] = $user_del_30_all_fed;

                    $user_del_30_male_fed =
                        DB::table('delusers')
                          ->where('federal_state', 'LIKE', $fed_state)
                          ->whereBetween('created_at', [$past_30, $today])->count();
                    $statistics['user_del_30_male_fed'] = $user_del_30_male_fed;

                    $user_del_30_female_fed =
                        DB::table('delusers')
                          ->where('federal_state', 'LIKE', $fed_state)
                          ->whereBetween('created_at', [$past_30, $today])->count();
                    $statistics['user_del_30_female_fed'] = $user_del_30_female_fed;

                    /** user deletion profile 90d */
                    $user_del_90_all_fed =
                        DB::table('delusers')
                          ->where('federal_state', 'LIKE', $fed_state)
                          ->whereBetween('created_at', [$past_90, $today])->count();
                    $statistics['user_del_90_all_fed'] = $user_del_90_all_fed;

                    $user_del_90_male_fed =
                        DB::table('delusers')
                          ->where('federal_state', 'LIKE', $fed_state)
                          ->whereBetween('created_at', [$past_90, $today])->count();
                    $statistics['user_del_90_male_fed'] = $user_del_90_male_fed;

                    $user_del_90_female_fed =
                        DB::table('delusers')
                          ->where('federal_state', 'LIKE', $fed_state)
                          ->whereBetween('created_at', [$past_90, $today])->count();
                    $statistics['user_del_90_female_fed'] = $user_del_90_female_fed;


                    break;

            }

        }


        /** Bundesland ausgewählt */
        if ($fed_state !== '' && $fed_state !== 'all'){

            $statistics['federal'] = 1;

            switch ($abschnitt_auswahl){

                case 0:
                    /** USERS
                     * registered users */
                    $user_registered = new User;
                    $user_reg_verified = new User;
                    $user_reg_male = new User;
                    $user_reg_female = new User;


                    if ($sporttype !== ''){

                        $statistics['sport'] = 1;

                        /** registered users */
                        $user_registered = $user_registered
                            ->where('federal_state', 'LIKE', $fed_state)
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_registered'] = $user_registered;

                        $user_reg_verified = $user_reg_verified
                            ->where('federal_state', 'LIKE', $fed_state)
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->where('verified', '=', 1)->get()->count('*');
                        $statistics['user_reg_verified'] = $user_reg_verified;

                        $user_reg_male = $user_reg_male->where('federal_state', 'LIKE', $fed_state)
                                                       ->where('gender', 'LIKE', 'male')
                                                       ->whereHas('sports', function($query) use ($sportids){
                                                           $query->whereIn('sport_id', $sportids);
                                                       })
                                                       ->count();
                        $statistics['user_reg_male'] = $user_reg_male;

                        $user_reg_female = $user_reg_female->where('federal_state', 'LIKE', $fed_state)
                                                           ->where('gender', 'LIKE', 'female')
                                                           ->whereHas('sports', function($query) use ($sportids){
                                                               $query->whereIn('sport_id', $sportids);
                                                           })
                                                           ->count();
                        $statistics['user_reg_female'] = $user_reg_female;

                    } else{

                        /** registered users */
                        $user_registered = $user_registered->where('federal_state', 'LIKE', $fed_state)->count();
                        $statistics['user_registered'] = $user_registered;

                        $user_reg_verified = $user_reg_verified->where('federal_state', 'LIKE', $fed_state)
                                                               ->where('verified', '=', 1)->get()->count('*');
                        $statistics['user_reg_verified'] = $user_reg_verified;

                        $user_reg_male = $user_reg_male->where('federal_state', 'LIKE', $fed_state)
                                                       ->where('gender', 'LIKE', 'male')->count();
                        $statistics['user_reg_male'] = $user_reg_male;

                        $user_reg_female = $user_reg_female->where('federal_state', 'LIKE', $fed_state)
                                                           ->where('gender', 'LIKE', 'female')->count();
                        $statistics['user_reg_female'] = $user_reg_female;

                    }

                    break;


                case 1:

                    /** new registered users 24h*/
                    $user_new_24_all = new User;
                    $user_new_24_verified = new User;
                    $user_new_24_male = new User;
                    $user_new_24_female = new User;

                    /** new registered users 7 days */
                    $user_new_7d_all = new User;
                    $user_new_7d_verified = new User;
                    $user_new_7d_male = new User;
                    $user_new_7d_female = new User;

                    /** new registered users 30 days */
                    $user_new_30d_all = new User;
                    $user_new_30d_verified = new User;
                    $user_new_30d_male = new User;
                    $user_new_30d_female = new User;

                    /** new registered users 90 days */
                    $user_new_90d_all = new User;
                    $user_new_90d_verified = new User;
                    $user_new_90d_male = new User;
                    $user_new_90d_female = new User;


                    if ($sporttype !== ''){

                        /** new registered users 24h*/
                        $user_new_24_all = $user_new_24_all->where('federal_state', 'LIKE', $fed_state)
                                                           ->whereHas('sports', function($query) use ($sportids){
                                                               $query->whereIn('sport_id', $sportids);
                                                           })
                                                           ->whereBetween('created_at', [$past_1, $today])->count();
                        $statistics['user_new_24_all'] = $user_new_24_all;

                        $user_new_24_verified = $user_new_24_verified->where('federal_state', 'LIKE', $fed_state)
                                                                     ->whereHas('sports', function($query) use ($sportids){
                                                                         $query->whereIn('sport_id', $sportids);
                                                                     })
                                                                     ->whereBetween('created_at', [$past_1, $today])->where('verified', '=', 1)->count();
                        $statistics['user_new_24_verified'] = $user_new_24_verified;

                        $user_new_24_male = $user_new_24_male->where('federal_state', 'LIKE', $fed_state)
                                                             ->whereHas('sports', function($query) use ($sportids){
                                                                 $query->whereIn('sport_id', $sportids);
                                                             })
                                                             ->whereBetween('created_at', [$past_1, $today])->where('gender', 'LIKE', 'male')->count();
                        $statistics['user_new_24_male'] = $user_new_24_male;

                        $user_new_24_female = $user_new_24_female->where('federal_state', 'LIKE', $fed_state)
                                                                 ->whereHas('sports', function($query) use ($sportids){
                                                                     $query->whereIn('sport_id', $sportids);
                                                                 })
                                                                 ->whereBetween('created_at', [$past_1, $today])->where('gender', 'LIKE', 'female')->count();
                        $statistics['user_new_24_female'] = $user_new_24_female;

                        /** new registered users 7 days */
                        $user_new_7d_all = $user_new_7d_all->where('federal_state', 'LIKE', $fed_state)
                                                           ->whereHas('sports', function($query) use ($sportids){
                                                               $query->whereIn('sport_id', $sportids);
                                                           })
                                                           ->whereBetween('created_at', [$past_7, $today])->count();
                        $statistics['user_new_7d_all'] = $user_new_7d_all;

                        $user_new_7d_verified = $user_new_7d_verified->where('federal_state', 'LIKE', $fed_state)
                                                                     ->whereHas('sports', function($query) use ($sportids){
                                                                         $query->whereIn('sport_id', $sportids);
                                                                     })
                                                                     ->whereBetween('created_at', [$past_7, $today])->where('verified', '=', 1)->count();
                        $statistics['user_new_7d_verified'] = $user_new_7d_verified;

                        $user_new_7d_male = $user_new_7d_male->where('federal_state', 'LIKE', $fed_state)
                                                             ->whereHas('sports', function($query) use ($sportids){
                                                                 $query->whereIn('sport_id', $sportids);
                                                             })
                                                             ->whereBetween('created_at', [$past_7, $today])->where('gender', 'LIKE', 'male')->count();
                        $statistics['user_new_7d_male'] = $user_new_7d_male;

                        $user_new_7d_female = $user_new_7d_female->where('federal_state', 'LIKE', $fed_state)
                                                                 ->whereHas('sports', function($query) use ($sportids){
                                                                     $query->whereIn('sport_id', $sportids);
                                                                 })
                                                                 ->whereBetween('created_at', [$past_7, $today])->where('gender', 'LIKE', 'female')->count();
                        $statistics['user_new_7d_female'] = $user_new_7d_female;

                        /** new registered users 30 days */
                        $user_new_30d_all = $user_new_30d_all->where('federal_state', 'LIKE', $fed_state)
                                                             ->whereHas('sports', function($query) use ($sportids){
                                                                 $query->whereIn('sport_id', $sportids);
                                                             })
                                                             ->whereBetween('created_at', [$past_30, $today])->count();
                        $statistics['user_new_30d_all'] = $user_new_30d_all;

                        $user_new_30d_verified = $user_new_30d_verified->where('federal_state', 'LIKE', $fed_state)
                                                                       ->whereHas('sports', function($query) use ($sportids){
                                                                           $query->whereIn('sport_id', $sportids);
                                                                       })
                                                                       ->whereBetween('created_at', [$past_30, $today])->where('verified', '=', 1)->count();
                        $statistics['user_new_30d_verified'] = $user_new_30d_verified;

                        $user_new_30d_male = $user_new_30d_male->where('federal_state', 'LIKE', $fed_state)
                                                               ->whereHas('sports', function($query) use ($sportids){
                                                                   $query->whereIn('sport_id', $sportids);
                                                               })
                                                               ->whereBetween('created_at', [$past_30, $today])->where('gender', 'LIKE', 'male')->count();
                        $statistics['user_new_30d_male'] = $user_new_30d_male;

                        $user_new_30d_female = $user_new_30d_female->where('federal_state', 'LIKE', $fed_state)
                                                                   ->whereHas('sports', function($query) use ($sportids){
                                                                       $query->whereIn('sport_id', $sportids);
                                                                   })
                                                                   ->whereBetween('created_at', [$past_30, $today])->where('gender', 'LIKE', 'female')->count();
                        $statistics['user_new_30d_female'] = $user_new_30d_female;

                        /** new registered users 90 days */
                        $user_new_90d_all = $user_new_90d_all->where('federal_state', 'LIKE', $fed_state)
                                                             ->whereHas('sports', function($query) use ($sportids){
                                                                 $query->whereIn('sport_id', $sportids);
                                                             })
                                                             ->whereBetween('created_at', [$past_90, $today])->count();
                        $statistics['user_new_90d_all'] = $user_new_90d_all;

                        $user_new_90d_verified = $user_new_90d_verified->where('federal_state', 'LIKE', $fed_state)
                                                                       ->whereHas('sports', function($query) use ($sportids){
                                                                           $query->whereIn('sport_id', $sportids);
                                                                       })
                                                                       ->whereBetween('created_at', [$past_90, $today])->where('verified', '=', 1)->count();
                        $statistics['user_new_90d_verified'] = $user_new_90d_verified;

                        $user_new_90d_male = $user_new_90d_male->where('federal_state', 'LIKE', $fed_state)
                                                               ->whereHas('sports', function($query) use ($sportids){
                                                                   $query->whereIn('sport_id', $sportids);
                                                               })
                                                               ->whereBetween('created_at', [$past_90, $today])->where('gender', 'LIKE', 'male')->count();
                        $statistics['user_new_90d_male'] = $user_new_90d_male;

                        $user_new_90d_female = $user_new_90d_female->where('federal_state', 'LIKE', $fed_state)
                                                                   ->whereHas('sports', function($query) use ($sportids){
                                                                       $query->whereIn('sport_id', $sportids);
                                                                   })
                                                                   ->whereBetween('created_at', [$past_90, $today])->where('gender', 'LIKE', 'female')->count();
                        $statistics['user_new_90d_female'] = $user_new_90d_female;

                    } else{

                        /** new registered users 24h*/
                        $user_new_24_all = $user_new_24_all->where('federal_state', 'LIKE', $fed_state)
                                                           ->whereBetween('created_at', [$past_1, $today])->count();
                        $statistics['user_new_24_all'] = $user_new_24_all;

                        $user_new_24_verified = $user_new_24_verified->where('federal_state', 'LIKE', $fed_state)
                                                                     ->whereBetween('created_at', [$past_1, $today])->where('verified', '=', 1)->count();
                        $statistics['user_new_24_verified'] = $user_new_24_verified;

                        $user_new_24_male = $user_new_24_male->where('federal_state', 'LIKE', $fed_state)
                                                             ->whereBetween('created_at', [$past_1, $today])->where('gender', 'LIKE', 'male')->count();
                        $statistics['user_new_24_male'] = $user_new_24_male;

                        $user_new_24_female = $user_new_24_female->where('federal_state', 'LIKE', $fed_state)
                                                                 ->whereBetween('created_at', [$past_1, $today])->where('gender', 'LIKE', 'female')->count();
                        $statistics['user_new_24_female'] = $user_new_24_female;

                        /** new registered users 7 days */
                        $user_new_7d_all = $user_new_7d_all->where('federal_state', 'LIKE', $fed_state)
                                                           ->whereBetween('created_at', [$past_7, $today])->count();
                        $statistics['user_new_7d_all'] = $user_new_7d_all;

                        $user_new_7d_verified = $user_new_7d_verified->where('federal_state', 'LIKE', $fed_state)
                                                                     ->whereBetween('created_at', [$past_7, $today])->where('verified', '=', 1)->count();
                        $statistics['user_new_7d_verified'] = $user_new_7d_verified;

                        $user_new_7d_male = $user_new_7d_male->where('federal_state', 'LIKE', $fed_state)
                                                             ->whereBetween('created_at', [$past_7, $today])->where('gender', 'LIKE', 'male')->count();
                        $statistics['user_new_7d_male'] = $user_new_7d_male;

                        $user_new_7d_female = $user_new_7d_female->where('federal_state', 'LIKE', $fed_state)
                                                                 ->whereBetween('created_at', [$past_7, $today])->where('gender', 'LIKE', 'female')->count();
                        $statistics['user_new_7d_female'] = $user_new_7d_female;

                        /** new registered users 30 days */
                        $user_new_30d_all = $user_new_30d_all->where('federal_state', 'LIKE', $fed_state)
                                                             ->whereBetween('created_at', [$past_30, $today])->count();
                        $statistics['user_new_30d_all'] = $user_new_30d_all;

                        $user_new_30d_verified = $user_new_30d_verified->where('federal_state', 'LIKE', $fed_state)
                                                                       ->whereBetween('created_at', [$past_30, $today])->where('verified', '=', 1)->count();
                        $statistics['user_new_30d_verified'] = $user_new_30d_verified;

                        $user_new_30d_male = $user_new_30d_male->where('federal_state', 'LIKE', $fed_state)
                                                               ->whereBetween('created_at', [$past_30, $today])->where('gender', 'LIKE', 'male')->count();
                        $statistics['user_new_30d_male'] = $user_new_30d_male;

                        $user_new_30d_female = $user_new_30d_female->where('federal_state', 'LIKE', $fed_state)
                                                                   ->whereBetween('created_at', [$past_30, $today])->where('gender', 'LIKE', 'female')->count();
                        $statistics['user_new_30d_female'] = $user_new_30d_female;

                        /** new registered users 90 days */
                        $user_new_90d_all = $user_new_90d_all->where('federal_state', 'LIKE', $fed_state)
                                                             ->whereBetween('created_at', [$past_90, $today])->count();
                        $statistics['user_new_90d_all'] = $user_new_90d_all;

                        $user_new_90d_verified = $user_new_90d_verified->where('federal_state', 'LIKE', $fed_state)
                                                                       ->whereBetween('created_at', [$past_90, $today])->where('verified', '=', 1)->count();
                        $statistics['user_new_90d_verified'] = $user_new_90d_verified;

                        $user_new_90d_male = $user_new_90d_male->where('federal_state', 'LIKE', $fed_state)
                                                               ->whereBetween('created_at', [$past_90, $today])->where('gender', 'LIKE', 'male')->count();
                        $statistics['user_new_90d_male'] = $user_new_90d_male;

                        $user_new_90d_female = $user_new_90d_female->where('federal_state', 'LIKE', $fed_state)
                                                                   ->whereBetween('created_at', [$past_90, $today])->where('gender', 'LIKE', 'female')->count();
                        $statistics['user_new_90d_female'] = $user_new_90d_female;

                    }

                    break;

                case 2:

                    /** all Users for age */
                    $users_all = new User;
                    $users_reg_veri = new User;
                    $users_reg_male = new User;
                    $users_reg_female = new User;
                    $i_12 = 0;
                    $i_13 = 0;
                    $i_18 = 0;
                    $i_25 = 0;
                    $i_35 = 0;
                    $i_45 = 0;
                    $i_55 = 0;
                    $i_65 = 0;

                    /** verified Users for age*/
                    $i_12_veri = 0;
                    $i_13_veri = 0;
                    $i_18_veri = 0;
                    $i_25_veri = 0;
                    $i_35_veri = 0;
                    $i_45_veri = 0;
                    $i_55_veri = 0;
                    $i_65_veri = 0;

                    /** male Users for age*/
                    $i_12_male = 0;
                    $i_13_male = 0;
                    $i_18_male = 0;
                    $i_25_male = 0;
                    $i_35_male = 0;
                    $i_45_male = 0;
                    $i_55_male = 0;
                    $i_65_male = 0;

                    /** female Users for age*/
                    $i_12_female = 0;
                    $i_13_female = 0;
                    $i_18_female = 0;
                    $i_25_female = 0;
                    $i_35_female = 0;
                    $i_45_female = 0;
                    $i_55_female = 0;
                    $i_65_female = 0;


                    if ($sporttype !== ''){

                        /** User age */
                        $users_all = $users_all->where('federal_state', 'LIKE', $fed_state)
                                               ->whereHas('sports', function($query) use ($sportids){
                                                   $query->whereIn('sport_id', $sportids);
                                               })
                                               ->get();

                        $users_reg_veri = $users_reg_veri->where('federal_state', 'LIKE', $fed_state)->where('verified', '=', 1)
                                                         ->whereHas('sports', function($query) use ($sportids){
                                                             $query->whereIn('sport_id', $sportids);
                                                         })
                                                         ->get();

                        $users_reg_male = $users_reg_male->where('federal_state', 'LIKE', $fed_state)->where('gender', 'LIKE', 'male')
                                                         ->whereHas('sports', function($query) use ($sportids){
                                                             $query->whereIn('sport_id', $sportids);
                                                         })
                                                         ->get();

                        $users_reg_female = $users_reg_female->where('federal_state', 'LIKE', $fed_state)->where('gender', 'LIKE', 'female')
                                                             ->whereHas('sports', function($query) use ($sportids){
                                                                 $query->whereIn('sport_id', $sportids);
                                                             })
                                                             ->get();


                        foreach ($users_all as $user){
                            if (is_null($user->birthday) || $user->birthday == '') continue;
                            $diff = date_diff($user->birthday, $today_new);
                            $age = $diff->format('%y');

                            if ($age < 12){
                                $i_12++;
                            }
                            if ($age >= 13 && $age <= 17){
                                $i_13++;
                            }
                            if ($age >= 18 && $age <= 24){
                                $i_18++;
                            }
                            if ($age >= 25 && $age <= 34){
                                $i_25++;
                            }
                            if ($age >= 35 && $age <= 44){
                                $i_35++;
                            }
                            if ($age >= 45 && $age <= 54){
                                $i_45++;
                            }
                            if ($age >= 55 && $age <= 64){
                                $i_55++;
                            }
                            if ($age >= 65){
                                $i_65++;
                            }

                        }

                        $statistics['12'] = $i_12;
                        $statistics['13'] = $i_13;
                        $statistics['18'] = $i_18;
                        $statistics['25'] = $i_25;
                        $statistics['35'] = $i_35;
                        $statistics['45'] = $i_45;
                        $statistics['55'] = $i_55;
                        $statistics['65'] = $i_65;


                        foreach ($users_reg_veri as $user_veri){
                            if (is_null($user_veri->birthday) || $user_veri->birthday == '') continue;
                            $diff_veri = date_diff($user_veri->birthday, $today_new);
                            $age_veri = $diff_veri->format('%y');

                            if ($age_veri < 12){
                                $i_12_veri++;
                            }
                            if ($age_veri >= 13 && $age_veri <= 17){
                                $i_13_veri++;
                            }
                            if ($age_veri >= 18 && $age_veri <= 24){
                                $i_18_veri++;
                            }
                            if ($age_veri >= 25 && $age_veri <= 34){
                                $i_25_veri++;
                            }
                            if ($age_veri >= 35 && $age_veri <= 44){
                                $i_35_veri++;
                            }
                            if ($age_veri >= 45 && $age_veri <= 54){
                                $i_45_veri++;
                            }
                            if ($age_veri >= 55 && $age_veri <= 64){
                                $i_55_veri++;
                            }
                            if ($age_veri >= 65){
                                $i_65_veri++;
                            }
                        }

                        $statistics['12_veri'] = $i_12_veri;
                        $statistics['13_veri'] = $i_13_veri;
                        $statistics['18_veri'] = $i_18_veri;
                        $statistics['25_veri'] = $i_25_veri;
                        $statistics['35_veri'] = $i_35_veri;
                        $statistics['45_veri'] = $i_45_veri;
                        $statistics['55_veri'] = $i_55_veri;
                        $statistics['65_veri'] = $i_65_veri;


                        foreach ($users_reg_male as $user_male){
                            if (is_null($user_male->birthday) || $user_male->birthday == '') continue;
                            $diff_male = date_diff($user_male->birthday, $today_new);
                            $age_male = $diff_male->format('%y');

                            if ($age_male < 12){
                                $i_12_male++;
                            }
                            if ($age_male >= 13 && $age_male <= 17){
                                $i_13_male++;
                            }
                            if ($age_male >= 18 && $age_male <= 24){
                                $i_18_male++;
                            }
                            if ($age_male >= 25 && $age_male <= 34){
                                $i_25_male++;
                            }
                            if ($age_male >= 35 && $age_male <= 44){
                                $i_35_male++;
                            }
                            if ($age_male >= 45 && $age_male <= 54){
                                $i_45_male++;
                            }
                            if ($age_male >= 55 && $age_male <= 64){
                                $i_55_male++;
                            }
                            if ($age_male >= 65){
                                $i_65_male++;
                            }
                        }

                        $statistics['12_male'] = $i_12_male;
                        $statistics['13_male'] = $i_13_male;
                        $statistics['18_male'] = $i_18_male;
                        $statistics['25_male'] = $i_25_male;
                        $statistics['35_male'] = $i_35_male;
                        $statistics['45_male'] = $i_45_male;
                        $statistics['55_male'] = $i_55_male;
                        $statistics['65_male'] = $i_65_male;


                        foreach ($users_reg_female as $user_female){
                            if (is_null($user_female->birthday) || $user_female->birthday == '') continue;
                            $diff_female = date_diff($user_female->birthday, $today_new);
                            $age_female = $diff_female->format('%y');

                            if ($age_female < 12){
                                $i_12_female++;
                            }
                            if ($age_female >= 13 && $age_female <= 17){
                                $i_13_female++;
                            }
                            if ($age_female >= 18 && $age_female <= 24){
                                $i_18_female++;
                            }
                            if ($age_female >= 25 && $age_female <= 34){
                                $i_25_female++;
                            }
                            if ($age_female >= 35 && $age_female <= 44){
                                $i_35_female++;
                            }
                            if ($age_female >= 45 && $age_female <= 54){
                                $i_45_female++;
                            }
                            if ($age_female >= 55 && $age_female <= 64){
                                $i_55_female++;
                            }
                            if ($age_female >= 65){
                                $i_65_female++;
                            }
                        }

                        $statistics['12_female'] = $i_12_female;
                        $statistics['13_female'] = $i_13_female;
                        $statistics['18_female'] = $i_18_female;
                        $statistics['25_female'] = $i_25_female;
                        $statistics['35_female'] = $i_35_female;
                        $statistics['45_female'] = $i_45_female;
                        $statistics['55_female'] = $i_55_female;
                        $statistics['65_female'] = $i_65_female;

                    } else{

                        /** User age */
                        $users_all = $users_all
                            ->where('federal_state', 'LIKE', $fed_state)->pluck('birthday');

                        $users_reg_veri = $users_reg_veri
                            ->where('federal_state', 'LIKE', $fed_state)
                            ->where('verified', '=', 1)->pluck('birthday');

                        $users_reg_male = $users_reg_male
                            ->where('federal_state', 'LIKE', $fed_state)
                            ->where('gender', 'LIKE', 'male')->pluck('birthday');

                        $users_reg_female = $users_reg_female
                            ->where('federal_state', 'LIKE', $fed_state)
                            ->where('gender', 'LIKE', 'female')->pluck('birthday');


                        foreach ($users_all as $user){
                            if (is_null($user) || $user == '') continue;
                            $diff = date_diff($user, $today_new);
                            $age = $diff->format('%y');


                            if ($age < 12){
                                $i_12++;
                            }
                            if ($age >= 13 && $age <= 17){
                                $i_13++;
                            }
                            if ($age >= 18 && $age <= 24){
                                $i_18++;
                            }
                            if ($age >= 25 && $age <= 34){
                                $i_25++;
                            }
                            if ($age >= 35 && $age <= 44){
                                $i_35++;
                            }
                            if ($age >= 45 && $age <= 54){
                                $i_45++;
                            }
                            if ($age >= 55 && $age <= 64){
                                $i_55++;
                            }
                            if ($age >= 65){
                                $i_65++;
                            }

                        }

                        $statistics['12'] = $i_12;
                        $statistics['13'] = $i_13;
                        $statistics['18'] = $i_18;
                        $statistics['25'] = $i_25;
                        $statistics['35'] = $i_35;
                        $statistics['45'] = $i_45;
                        $statistics['55'] = $i_55;
                        $statistics['65'] = $i_65;


                        foreach ($users_reg_veri as $user_veri){

                            if (is_null($user_veri) || $user_veri == '') continue;
                            $diff_veri = date_diff($user_veri, $today_new);
                            $age_veri = $diff_veri->format('%y');

                            if ($age_veri < 12){
                                $i_12_veri++;
                            }
                            if ($age_veri >= 13 && $age_veri <= 17){
                                $i_13_veri++;
                            }
                            if ($age_veri >= 18 && $age_veri <= 24){
                                $i_18_veri++;
                            }
                            if ($age_veri >= 25 && $age_veri <= 34){
                                $i_25_veri++;
                            }
                            if ($age_veri >= 35 && $age_veri <= 44){
                                $i_35_veri++;
                            }
                            if ($age_veri >= 45 && $age_veri <= 54){
                                $i_45_veri++;
                            }
                            if ($age_veri >= 55 && $age_veri <= 64){
                                $i_55_veri++;
                            }
                            if ($age_veri >= 65){
                                $i_65_veri++;
                            }
                        }

                        $statistics['12_veri'] = $i_12_veri;
                        $statistics['13_veri'] = $i_13_veri;
                        $statistics['18_veri'] = $i_18_veri;
                        $statistics['25_veri'] = $i_25_veri;
                        $statistics['35_veri'] = $i_35_veri;
                        $statistics['45_veri'] = $i_45_veri;
                        $statistics['55_veri'] = $i_55_veri;
                        $statistics['65_veri'] = $i_65_veri;


                        foreach ($users_reg_male as $user_male){

                            if (is_null($user_male) || $user_male == '') continue;
                            $diff_male = date_diff($user_male, $today_new);
                            $age_male = $diff_male->format('%y');


                            if ($age_male < 12){
                                $i_12_male++;
                            }
                            if ($age_male >= 13 && $age_male <= 17){
                                $i_13_male++;
                            }
                            if ($age_male >= 18 && $age_male <= 24){
                                $i_18_male++;
                            }
                            if ($age_male >= 25 && $age_male <= 34){
                                $i_25_male++;
                            }
                            if ($age_male >= 35 && $age_male <= 44){
                                $i_35_male++;
                            }
                            if ($age_male >= 45 && $age_male <= 54){
                                $i_45_male++;
                            }
                            if ($age_male >= 55 && $age_male <= 64){
                                $i_55_male++;
                            }
                            if ($age_male >= 65){
                                $i_65_male++;
                            }
                        }

                        $statistics['12_male'] = $i_12_male;
                        $statistics['13_male'] = $i_13_male;
                        $statistics['18_male'] = $i_18_male;
                        $statistics['25_male'] = $i_25_male;
                        $statistics['35_male'] = $i_35_male;
                        $statistics['45_male'] = $i_45_male;
                        $statistics['55_male'] = $i_55_male;
                        $statistics['65_male'] = $i_65_male;


                        foreach ($users_reg_female as $user_female){

                            if (is_null($user_female) || $user_female == '') continue;
                            $diff_female = date_diff($user_female, $today_new);
                            $age_female = $diff_female->format('%y');

                            if ($age_female < 12){
                                $i_12_female++;
                            }
                            if ($age_female >= 13 && $age_female <= 17){
                                $i_13_female++;
                            }
                            if ($age_female >= 18 && $age_female <= 24){
                                $i_18_female++;
                            }
                            if ($age_female >= 25 && $age_female <= 34){
                                $i_25_female++;
                            }
                            if ($age_female >= 35 && $age_female <= 44){
                                $i_35_female++;
                            }
                            if ($age_female >= 45 && $age_female <= 54){
                                $i_45_female++;
                            }
                            if ($age_female >= 55 && $age_female <= 64){
                                $i_55_female++;
                            }
                            if ($age_female >= 65){
                                $i_65_female++;
                            }
                        }

                        $statistics['12_female'] = $i_12_female;
                        $statistics['13_female'] = $i_13_female;
                        $statistics['18_female'] = $i_18_female;
                        $statistics['25_female'] = $i_25_female;
                        $statistics['35_female'] = $i_35_female;
                        $statistics['45_female'] = $i_45_female;
                        $statistics['55_female'] = $i_55_female;
                        $statistics['65_female'] = $i_65_female;

                    }

                    break;

                case 3:

                    /** activity 24h */
                    $user_act_24_all = new User;
                    $user_act_24_male = new User;
                    $user_act_24_female = new User;

                    /** activity 7d */
                    $user_act_7d_all = new User;
                    $user_act_7d_male = new User;
                    $user_act_7d_female = new User;

                    /** activity 30d */
                    $user_act_30d_all = new User;
                    $user_act_30d_male = new User;
                    $user_act_30d_female = new User;

                    /** activity 90d */
                    $user_act_90d_all = new User;
                    $user_act_90d_male = new User;
                    $user_act_90d_female = new User;


                    if ($sporttype !== ''){

                        /** User Activity */
                        /** activity 24h */
                        $user_act_24_all = $user_act_24_all->where('federal_state', 'LIKE', $fed_state)
                                                           ->whereBetween('login_at', [$past_1, $today])
                                                           ->whereHas('sports', function($query) use ($sportids){
                                                               $query->whereIn('sport_id', $sportids);
                                                           })
                                                           ->count();
                        $statistics['user_act_24_all'] = $user_act_24_all;

                        $user_act_24_male = $user_act_24_male->where('federal_state', 'LIKE', $fed_state)
                                                             ->whereBetween('login_at', [$past_1, $today])->where('gender', 'LIKE', 'male')
                                                             ->whereHas('sports', function($query) use ($sportids){
                                                                 $query->whereIn('sport_id', $sportids);
                                                             })
                                                             ->count();
                        $statistics['user_act_24_male'] = $user_act_24_male;

                        $user_act_24_female = $user_act_24_female->where('federal_state', 'LIKE', $fed_state)
                                                                 ->whereHas('sports', function($query) use ($sportids){
                                                                     $query->whereIn('sport_id', $sportids);
                                                                 })
                                                                 ->whereBetween('login_at', [$past_1, $today])->where('gender', 'LIKE', 'female')->count();
                        $statistics['user_act_24_female'] = $user_act_24_female;

                        /** activity 7d */
                        $user_act_7d_all = $user_act_7d_all->where('federal_state', 'LIKE', $fed_state)
                                                           ->whereHas('sports', function($query) use ($sportids){
                                                               $query->whereIn('sport_id', $sportids);
                                                           })
                                                           ->whereBetween('login_at', [$past_7, $today])->count();
                        $statistics['user_act_7d_all'] = $user_act_7d_all;

                        $user_act_7d_male = $user_act_7d_male->where('federal_state', 'LIKE', $fed_state)
                                                             ->whereHas('sports', function($query) use ($sportids){
                                                                 $query->whereIn('sport_id', $sportids);
                                                             })
                                                             ->whereBetween('login_at', [$past_7, $today])->where('gender', 'LIKE', 'male')->count();
                        $statistics['user_act_7d_male'] = $user_act_7d_male;

                        $user_act_7d_female = $user_act_7d_female->where('federal_state', 'LIKE', $fed_state)
                                                                 ->whereHas('sports', function($query) use ($sportids){
                                                                     $query->whereIn('sport_id', $sportids);
                                                                 })
                                                                 ->whereBetween('login_at', [$past_7, $today])->where('gender', 'LIKE', 'female')->count();
                        $statistics['user_act_7d_female'] = $user_act_7d_female;

                        /** activity 30d */
                        $user_act_30d_all = $user_act_30d_all->where('federal_state', 'LIKE', $fed_state)
                                                             ->whereHas('sports', function($query) use ($sportids){
                                                                 $query->whereIn('sport_id', $sportids);
                                                             })
                                                             ->whereBetween('login_at', [$past_30, $today])->count();
                        $statistics['user_act_30d_all'] = $user_act_30d_all;

                        $user_act_30d_male = $user_act_30d_male->where('federal_state', 'LIKE', $fed_state)
                                                               ->whereHas('sports', function($query) use ($sportids){
                                                                   $query->whereIn('sport_id', $sportids);
                                                               })
                                                               ->whereBetween('login_at', [$past_30, $today])->where('gender', 'LIKE', 'male')->count();
                        $statistics['user_act_30d_male'] = $user_act_30d_male;

                        $user_act_30d_female = $user_act_30d_female->where('federal_state', 'LIKE', $fed_state)
                                                                   ->whereHas('sports', function($query) use ($sportids){
                                                                       $query->whereIn('sport_id', $sportids);
                                                                   })
                                                                   ->whereBetween('login_at', [$past_30, $today])->where('gender', 'LIKE', 'female')->count();
                        $statistics['user_act_30d_female'] = $user_act_30d_female;

                        /** activity 90d */
                        $user_act_90d_all = $user_act_90d_all->where('federal_state', 'LIKE', $fed_state)
                                                             ->whereHas('sports', function($query) use ($sportids){
                                                                 $query->whereIn('sport_id', $sportids);
                                                             })
                                                             ->whereBetween('login_at', [$past_90, $today])->count();
                        $statistics['user_act_90d_all'] = $user_act_90d_all;

                        $user_act_90d_male = $user_act_90d_male->where('federal_state', 'LIKE', $fed_state)
                                                               ->whereHas('sports', function($query) use ($sportids){
                                                                   $query->whereIn('sport_id', $sportids);
                                                               })
                                                               ->whereBetween('login_at', [$past_90, $today])->where('gender', 'LIKE', 'male')->count();
                        $statistics['user_act_90d_male'] = $user_act_90d_male;

                        $user_act_90d_female = $user_act_90d_female->where('federal_state', 'LIKE', $fed_state)
                                                                   ->whereHas('sports', function($query) use ($sportids){
                                                                       $query->whereIn('sport_id', $sportids);
                                                                   })
                                                                   ->whereBetween('login_at', [$past_90, $today])->where('gender', 'LIKE', 'female')->count();
                        $statistics['user_act_90d_female'] = $user_act_90d_female;

                    } else{

                        /** User Activity */
                        /** activity 24h */
                        $user_act_24_all = $user_act_24_all->where('federal_state', 'LIKE', $fed_state)
                                                           ->whereBetween('login_at', [$past_1, $today])->count();
                        $statistics['user_act_24_all'] = $user_act_24_all;

                        $user_act_24_male = $user_act_24_male->where('federal_state', 'LIKE', $fed_state)
                                                             ->whereBetween('login_at', [$past_1, $today])->where('gender', 'LIKE', 'male')->count();
                        $statistics['user_act_24_male'] = $user_act_24_male;

                        $user_act_24_female = $user_act_24_female->where('federal_state', 'LIKE', $fed_state)
                                                                 ->whereBetween('login_at', [$past_1, $today])->where('gender', 'LIKE', 'female')->count();
                        $statistics['user_act_24_female'] = $user_act_24_female;

                        /** activity 7d */
                        $user_act_7d_all = $user_act_7d_all->where('federal_state', 'LIKE', $fed_state)
                                                           ->whereBetween('login_at', [$past_7, $today])->count();
                        $statistics['user_act_7d_all'] = $user_act_7d_all;

                        $user_act_7d_male = $user_act_7d_male->where('federal_state', 'LIKE', $fed_state)
                                                             ->whereBetween('login_at', [$past_7, $today])->where('gender', 'LIKE', 'male')->count();
                        $statistics['user_act_7d_male'] = $user_act_7d_male;

                        $user_act_7d_female = $user_act_7d_female->where('federal_state', 'LIKE', $fed_state)
                                                                 ->whereBetween('login_at', [$past_7, $today])->where('gender', 'LIKE', 'female')->count();
                        $statistics['user_act_7d_female'] = $user_act_7d_female;

                        /** activity 30d */
                        $user_act_30d_all = $user_act_30d_all->where('federal_state', 'LIKE', $fed_state)
                                                             ->whereBetween('login_at', [$past_30, $today])->count();
                        $statistics['user_act_30d_all'] = $user_act_30d_all;

                        $user_act_30d_male = $user_act_30d_male->where('federal_state', 'LIKE', $fed_state)
                                                               ->whereBetween('login_at', [$past_30, $today])->where('gender', 'LIKE', 'male')->count();
                        $statistics['user_act_30d_male'] = $user_act_30d_male;

                        $user_act_30d_female = $user_act_30d_female->where('federal_state', 'LIKE', $fed_state)
                                                                   ->whereBetween('login_at', [$past_30, $today])->where('gender', 'LIKE', 'female')->count();
                        $statistics['user_act_30d_female'] = $user_act_30d_female;

                        /** activity 90d */
                        $user_act_90d_all = $user_act_90d_all->where('federal_state', 'LIKE', $fed_state)
                                                             ->whereBetween('login_at', [$past_90, $today])->count();
                        $statistics['user_act_90d_all'] = $user_act_90d_all;

                        $user_act_90d_male = $user_act_90d_male->where('federal_state', 'LIKE', $fed_state)
                                                               ->whereBetween('login_at', [$past_90, $today])->where('gender', 'LIKE', 'male')->count();
                        $statistics['user_act_90d_male'] = $user_act_90d_male;

                        $user_act_90d_female = $user_act_90d_female->where('federal_state', 'LIKE', $fed_state)
                                                                   ->whereBetween('login_at', [$past_90, $today])->where('gender', 'LIKE', 'female')->count();
                        $statistics['user_act_90d_female'] = $user_act_90d_female;

                    }

                    break;

                case 4:

                    /** user mandatory part */
                    $user_mand_part_all = new User;
                    $user_mand_part_male = new User;
                    $user_mand_part_female = new User;

                    /** user mandatory full */
                    $user_mand_all = new User;
                    $user_mand_male = new User;
                    $user_mand_female = new User;

                    /** user sportvita */
                    $user_sportvita_all = new User;
                    $user_sportvita_male = new User;
                    $user_sportvita_female = new User;

                    /** user profile pic */
                    $user_profile_pic_all = new User;
                    $user_profile_pic_male = new User;
                    $user_profile_pic_female = new User;


                    if ($sporttype !== ''){

                        /** User intensity */
                        /** user profile mandatory part */
                        $user_mand_part_all = $user_mand_part_all->where('federal_state', 'LIKE', $fed_state)
                                                                 ->where(function($query){
                                                                     $query->whereNull('firstname')
                                                                           ->orWhereNull('lastname')
                                                                           ->orWhereNull('birthday')
                                                                           ->orWhereNull('city')
                                                                           ->orWhereNull('zip');
                                                                 })
                                                                 ->whereHas('sports', function($query) use ($sportids){
                                                                     $query->whereIn('sport_id', $sportids);
                                                                 })->count();
                        $statistics['user_mand_part_all'] = $user_mand_part_all;

                        $user_mand_part_male = $user_mand_part_male->where('federal_state', 'LIKE', $fed_state)->where('gender', 'LIKE', 'male')
                                                                   ->where(function($query){
                                                                       $query->whereNull('firstname')
                                                                             ->orWhereNull('lastname')
                                                                             ->orWhereNull('birthday')
                                                                             ->orWhereNull('city')
                                                                             ->orWhereNull('zip');
                                                                   })
                                                                   ->whereHas('sports', function($query) use ($sportids){
                                                                       $query->whereIn('sport_id', $sportids);
                                                                   })->count();
                        $statistics['user_mand_part_male'] = $user_mand_part_male;

                        $user_mand_part_female = $user_mand_part_female->where('federal_state', 'LIKE', $fed_state)->where('gender', 'LIKE', 'female')
                                                                       ->where(function($query){
                                                                           $query->whereNull('firstname')
                                                                                 ->orWhereNull('lastname')
                                                                                 ->orWhereNull('birthday')
                                                                                 ->orWhereNull('city')
                                                                                 ->orWhereNull('zip');
                                                                       })
                                                                       ->whereHas('sports', function($query) use ($sportids){
                                                                           $query->whereIn('sport_id', $sportids);
                                                                       })->count();
                        $statistics['user_mand_part_female'] = $user_mand_part_female;

                        /** user profile mandatory full */
                        $user_mand_all = $user_mand_all->where('federal_state', 'LIKE', $fed_state)
                                                       ->whereNotNull('firstname')->whereNotNull('lastname')->whereNotNull('birthday')->whereNotNull('city')
                                                       ->whereNotNull('gender')->whereNotNull('zip')
                                                       ->whereHas('sports', function($query) use ($sportids){
                                                           $query->whereIn('sport_id', $sportids);
                                                       })
                                                       ->count();
                        $statistics['user_mand_all'] = $user_mand_all;

                        $user_mand_male = $user_mand_male->where('federal_state', 'LIKE', $fed_state)->where('gender', 'LIKE', 'male')
                                                         ->whereNotNull('firstname')->whereNotNull('lastname')->whereNotNull('birthday')->whereNotNull('city')
                                                         ->whereNotNull('zip')
                                                         ->whereHas('sports', function($query) use ($sportids){
                                                             $query->whereIn('sport_id', $sportids);
                                                         })
                                                         ->count();
                        $statistics['user_mand_male'] = $user_mand_male;

                        $user_mand_female = $user_mand_female->where('federal_state', 'LIKE', $fed_state)->where('gender', 'LIKE', 'female')
                                                             ->whereNotNull('firstname')->whereNotNull('lastname')->whereNotNull('birthday')->whereNotNull('city')
                                                             ->whereNotNull('zip')
                                                             ->whereHas('sports', function($query) use ($sportids){
                                                                 $query->whereIn('sport_id', $sportids);
                                                             })
                                                             ->count();
                        $statistics['user_mand_female'] = $user_mand_female;


                        /** user sportvita */
                        $user_sportvita_all = $user_sportvita_all->where('federal_state', 'LIKE', $fed_state)
                                                                 ->whereHas('sports', function($query) use ($sportids){
                                                                     $query->whereIn('sport_id', $sportids);
                                                                 })
                                                                 ->count();
                        $statistics['user_sportvita_all'] = $user_sportvita_all;

                        $user_sportvita_male = $user_sportvita_male->where('federal_state', 'LIKE', $fed_state)
                                                                   ->where('gender', 'LIKE', 'male')
                                                                   ->whereHas('sports', function($query) use ($sportids){
                                                                       $query->whereIn('sport_id', $sportids);
                                                                   })
                                                                   ->count();
                        $statistics['user_sportvita_male'] = $user_sportvita_male;

                        $user_sportvita_female = $user_sportvita_female->where('federal_state', 'LIKE', $fed_state)
                                                                       ->where('gender', 'LIKE', 'female')
                                                                       ->whereHas('sports', function($query) use ($sportids){
                                                                           $query->whereIn('sport_id', $sportids);
                                                                       })
                                                                       ->count();
                        $statistics['user_sportvita_female'] = $user_sportvita_female;


                        /** user profile pic */
                        $user_profile_pic_all = $user_profile_pic_all->where('federal_state', 'LIKE', $fed_state)
                                                                     ->whereNotNull('avatar_file_name')
                                                                     ->whereHas('sports', function($query) use ($sportids){
                                                                         $query->whereIn('sport_id', $sportids);
                                                                     })
                                                                     ->count();
                        $statistics['user_profile_pic_all'] = $user_profile_pic_all;

                        $user_profile_pic_male = $user_profile_pic_male->where('federal_state', 'LIKE', $fed_state)
                                                                       ->whereNotNull('avatar_file_name')
                                                                       ->where('gender', 'LIKE', 'male')
                                                                       ->whereHas('sports', function($query) use ($sportids){
                                                                           $query->whereIn('sport_id', $sportids);
                                                                       })
                                                                       ->count();
                        $statistics['user_profile_pic_male'] = $user_profile_pic_male;

                        $user_profile_pic_female = $user_profile_pic_female->where('federal_state', 'LIKE', $fed_state)
                                                                           ->whereNotNull('avatar_file_name')
                                                                           ->where('gender', 'LIKE', 'male')
                                                                           ->whereHas('sports', function($query) use ($sportids){
                                                                               $query->whereIn('sport_id', $sportids);
                                                                           })
                                                                           ->count();
                        $statistics['user_profile_pic_female'] = $user_profile_pic_female;

                    } else{

                        /** User intensity */
                        /** user profile mandatory part */
                        $user_mand_part_all = $user_mand_part_all->where('federal_state', 'LIKE', $fed_state)
                                                                 ->where(function($query){
                                                                     $query->whereNull('firstname')
                                                                           ->orWhereNull('lastname')
                                                                           ->orWhereNull('birthday')
                                                                           ->orWhereNull('city')
                                                                           ->orWhereNull('zip');
                                                                 })->count();
                        $statistics['user_mand_part_all'] = $user_mand_part_all;

                        $user_mand_part_male = $user_mand_part_male->where('federal_state', 'LIKE', $fed_state)->where('gender', 'LIKE', 'male')
                                                                   ->where(function($query){
                                                                       $query->whereNull('firstname')
                                                                             ->orWhereNull('lastname')
                                                                             ->orWhereNull('birthday')
                                                                             ->orWhereNull('city')
                                                                             ->orWhereNull('zip');
                                                                   })->count();
                        $statistics['user_mand_part_male'] = $user_mand_part_male;

                        $user_mand_part_female = $user_mand_part_female->where('federal_state', 'LIKE', $fed_state)->where('gender', 'LIKE', 'female')
                                                                       ->where(function($query){
                                                                           $query->whereNull('firstname')
                                                                                 ->orWhereNull('lastname')
                                                                                 ->orWhereNull('birthday')
                                                                                 ->orWhereNull('city')
                                                                                 ->orWhereNull('zip');
                                                                       })->count();
                        $statistics['user_mand_part_female'] = $user_mand_part_female;

                        /** user profile mandatory full */
                        $user_mand_all = $user_mand_all->where('federal_state', 'LIKE', $fed_state)
                                                       ->whereNotNull('firstname')->whereNotNull('lastname')->whereNotNull('birthday')->whereNotNull('city')
                                                       ->whereNotNull('gender')->whereNotNull('zip')->count();
                        $statistics['user_mand_all'] = $user_mand_all;

                        $user_mand_male = $user_mand_male->where('federal_state', 'LIKE', $fed_state)->where('gender', 'LIKE', 'male')
                                                         ->whereNotNull('firstname')->whereNotNull('lastname')->whereNotNull('birthday')->whereNotNull('city')
                                                         ->whereNotNull('zip')->count();
                        $statistics['user_mand_male'] = $user_mand_male;

                        $user_mand_female = $user_mand_female->where('federal_state', 'LIKE', $fed_state)->where('gender', 'LIKE', 'female')
                                                             ->whereNotNull('firstname')->whereNotNull('lastname')->whereNotNull('birthday')->whereNotNull('city')
                                                             ->whereNotNull('zip')->count();
                        $statistics['user_mand_female'] = $user_mand_female;


                        /** user sportvita */
                        $user_sportvita_all = $user_sportvita_all->where('federal_state', 'LIKE', $fed_state)->has('sports')->count();
                        $statistics['user_sportvita_all'] = $user_sportvita_all;

                        $user_sportvita_male = $user_sportvita_male->where('federal_state', 'LIKE', $fed_state)
                                                                   ->where('gender', 'LIKE', 'male')
                                                                   ->has('sports')->count();
                        $statistics['user_sportvita_male'] = $user_sportvita_male;

                        $user_sportvita_female = $user_sportvita_female->where('federal_state', 'LIKE', $fed_state)
                                                                       ->where('gender', 'LIKE', 'female')
                                                                       ->has('sports')->count();
                        $statistics['user_sportvita_female'] = $user_sportvita_female;


                        /** user profile pic */
                        $user_profile_pic_all = $user_profile_pic_all->where('federal_state', 'LIKE', $fed_state)
                                                                     ->whereNotNull('avatar_file_name')->count();
                        $statistics['user_profile_pic_all'] = $user_profile_pic_all;

                        $user_profile_pic_male = $user_profile_pic_male->where('federal_state', 'LIKE', $fed_state)
                                                                       ->whereNotNull('avatar_file_name')
                                                                       ->where('gender', 'LIKE', 'male')
                                                                       ->count();
                        $statistics['user_profile_pic_male'] = $user_profile_pic_male;

                        $user_profile_pic_female = $user_profile_pic_female->where('federal_state', 'LIKE', $fed_state)
                                                                           ->whereNotNull('avatar_file_name')
                                                                           ->where('gender', 'LIKE', 'male')
                                                                           ->count();
                        $statistics['user_profile_pic_female'] = $user_profile_pic_female;

                    }

                    break;

                case 5:

                    /** user deletion profile 24h*/
                    $user_del_24_all = new User;
                    $user_del_24_male = new User;
                    $user_del_24_female = new User;

                    /** user deletion profile 7d*/
                    $user_del_7_all = new User;
                    $user_del_7_male = new User;
                    $user_del_7_female = new User;

                    /** user deletion profile 30d*/
                    $user_del_30_all = new User;
                    $user_del_30_male = new User;
                    $user_del_30_female = new User;

                    /** user deletion profile 90d*/
                    $user_del_90_all = new User;
                    $user_del_90_male = new User;
                    $user_del_90_female = new User;


                    if ($sporttype !== ''){

                        /** user deletion profile 24h*/
                        $user_del_24_all = $user_del_24_all->onlyTrashed()->where('federal_state', 'LIKE', $fed_state)
                                                           ->whereBetween('deleted_at', [$past_1, $today])
                                                           ->whereHas('sports', function($query) use ($sportids){
                                                               $query->whereIn('sport_id', $sportids);
                                                           })
                                                           ->count();
                        $statistics['user_del_24_all'] = $user_del_24_all;

                        $user_del_24_male = $user_del_24_male->onlyTrashed()->where('federal_state', 'LIKE', $fed_state)
                                                             ->where('gender', 'LIKE', 'male')
                                                             ->whereBetween('deleted_at', [$past_1, $today])
                                                             ->whereHas('sports', function($query) use ($sportids){
                                                                 $query->whereIn('sport_id', $sportids);
                                                             })
                                                             ->count();
                        $statistics['user_del_24_male'] = $user_del_24_male;

                        $user_del_24_female = $user_del_24_female->onlyTrashed()->where('federal_state', 'LIKE', $fed_state)
                                                                 ->where('gender', 'LIKE', 'female')
                                                                 ->whereBetween('deleted_at', [$past_1, $today])
                                                                 ->whereHas('sports', function($query) use ($sportids){
                                                                     $query->whereIn('sport_id', $sportids);
                                                                 })
                                                                 ->count();
                        $statistics['user_del_24_female'] = $user_del_24_female;

                        /** user deletion profile 7d */
                        $user_del_7_all = $user_del_7_all->onlyTrashed()->where('federal_state', 'LIKE', $fed_state)
                                                         ->whereBetween('deleted_at', [$past_7, $today])
                                                         ->whereHas('sports', function($query) use ($sportids){
                                                             $query->whereIn('sport_id', $sportids);
                                                         })
                                                         ->count();
                        $statistics['user_del_7_all'] = $user_del_7_all;

                        $user_del_7_male = $user_del_7_male->onlyTrashed()->where('federal_state', 'LIKE', $fed_state)
                                                           ->where('gender', 'LIKE', 'male')
                                                           ->whereBetween('deleted_at', [$past_7, $today])
                                                           ->whereHas('sports', function($query) use ($sportids){
                                                               $query->whereIn('sport_id', $sportids);
                                                           })
                                                           ->count();
                        $statistics['user_del_7_male'] = $user_del_7_male;

                        $user_del_7_female = $user_del_7_female->onlyTrashed()->where('federal_state', 'LIKE', $fed_state)
                                                               ->where('gender', 'LIKE', 'female')
                                                               ->whereBetween('deleted_at', [$past_7, $today])
                                                               ->whereHas('sports', function($query) use ($sportids){
                                                                   $query->whereIn('sport_id', $sportids);
                                                               })
                                                               ->count();
                        $statistics['user_del_7_female'] = $user_del_7_female;

                        /** user deletion profile 30d */
                        $user_del_30_all = $user_del_30_all->onlyTrashed()->where('federal_state', 'LIKE', $fed_state)
                                                           ->whereBetween('deleted_at', [$past_30, $today])
                                                           ->whereHas('sports', function($query) use ($sportids){
                                                               $query->whereIn('sport_id', $sportids);
                                                           })
                                                           ->count();
                        $statistics['user_del_30_all'] = $user_del_30_all;

                        $user_del_30_male = $user_del_30_male->onlyTrashed()->where('federal_state', 'LIKE', $fed_state)
                                                             ->where('gender', 'LIKE', 'male')
                                                             ->whereBetween('deleted_at', [$past_30, $today])
                                                             ->whereHas('sports', function($query) use ($sportids){
                                                                 $query->whereIn('sport_id', $sportids);
                                                             })
                                                             ->count();
                        $statistics['user_del_30_male'] = $user_del_30_male;

                        $user_del_30_female = $user_del_30_female->onlyTrashed()->where('federal_state', 'LIKE', $fed_state)
                                                                 ->where('gender', 'LIKE', 'female')
                                                                 ->whereBetween('deleted_at', [$past_30, $today])
                                                                 ->whereHas('sports', function($query) use ($sportids){
                                                                     $query->whereIn('sport_id', $sportids);
                                                                 })
                                                                 ->count();
                        $statistics['user_del_30_female'] = $user_del_30_female;

                        /** user deletion profile 90d */
                        $user_del_90_all = $user_del_90_all->onlyTrashed()->where('federal_state', 'LIKE', $fed_state)
                                                           ->whereBetween('deleted_at', [$past_90, $today])
                                                           ->whereHas('sports', function($query) use ($sportids){
                                                               $query->whereIn('sport_id', $sportids);
                                                           })
                                                           ->count();
                        $statistics['user_del_90_all'] = $user_del_90_all;

                        $user_del_90_male = $user_del_90_male->onlyTrashed()->where('federal_state', 'LIKE', $fed_state)
                                                             ->where('gender', 'LIKE', 'male')
                                                             ->whereBetween('deleted_at', [$past_90, $today])
                                                             ->whereHas('sports', function($query) use ($sportids){
                                                                 $query->whereIn('sport_id', $sportids);
                                                             })
                                                             ->count();
                        $statistics['user_del_90_male'] = $user_del_90_male;

                        $user_del_90_female = $user_del_90_female->onlyTrashed()->where('federal_state', 'LIKE', $fed_state)
                                                                 ->where('gender', 'LIKE', 'female')
                                                                 ->whereBetween('deleted_at', [$past_90, $today])
                                                                 ->whereHas('sports', function($query) use ($sportids){
                                                                     $query->whereIn('sport_id', $sportids);
                                                                 })
                                                                 ->count();
                        $statistics['user_del_90_female'] = $user_del_90_female;

                    } else{

                        /** user deletion profile 24h*/
                        $user_del_24_all =
                            DB::table('delusers')
                              ->where('federal_state', 'LIKE', $fed_state)
                              ->whereBetween('created_at', [$past_1, $today])->count();
                        $statistics['user_del_24_all'] = $user_del_24_all;

                        $user_del_24_male =
                            DB::table('delusers')
                              ->where('federal_state', 'LIKE', $fed_state)
                              ->whereBetween('created_at', [$past_1, $today])->count();
                        $statistics['user_del_24_male'] = $user_del_24_male;

                        $user_del_24_female =
                            DB::table('delusers')
                              ->where('federal_state', 'LIKE', $fed_state)
                              ->whereBetween('created_at', [$past_1, $today])->count();
                        $statistics['user_del_24_female'] = $user_del_24_female;

                        /** user deletion profile 7d */
                        $user_del_7_all =
                            DB::table('delusers')
                              ->where('federal_state', 'LIKE', $fed_state)
                              ->whereBetween('created_at', [$past_7, $today])->count();
                        $statistics['user_del_7_all'] = $user_del_7_all;

                        $user_del_7_male =
                            DB::table('delusers')
                              ->where('federal_state', 'LIKE', $fed_state)
                              ->whereBetween('created_at', [$past_7, $today])->count();
                        $statistics['user_del_7_male'] = $user_del_7_male;

                        $user_del_7_female =
                            DB::table('delusers')
                              ->where('federal_state', 'LIKE', $fed_state)
                              ->whereBetween('created_at', [$past_7, $today])->count();
                        $statistics['user_del_7_female'] = $user_del_7_female;

                        /** user deletion profile 30d */
                        $user_del_30_all =
                            DB::table('delusers')
                              ->where('federal_state', 'LIKE', $fed_state)
                              ->whereBetween('created_at', [$past_30, $today])->count();
                        $statistics['user_del_30_all'] = $user_del_30_all;

                        $user_del_30_male =
                            DB::table('delusers')
                              ->where('federal_state', 'LIKE', $fed_state)
                              ->whereBetween('created_at', [$past_30, $today])->count();
                        $statistics['user_del_30_male'] = $user_del_30_male;

                        $user_del_30_female =
                            DB::table('delusers')
                              ->where('federal_state', 'LIKE', $fed_state)
                              ->whereBetween('created_at', [$past_30, $today])->count();
                        $statistics['user_del_30_female'] = $user_del_30_female;

                        /** user deletion profile 90d */
                        $user_del_90_all =
                            DB::table('delusers')
                              ->where('federal_state', 'LIKE', $fed_state)
                              ->whereBetween('created_at', [$past_90, $today])->count();
                        $statistics['user_del_90_all'] = $user_del_90_all;

                        $user_del_90_male =
                            DB::table('delusers')
                              ->where('federal_state', 'LIKE', $fed_state)
                              ->whereBetween('created_at', [$past_90, $today])->count();
                        $statistics['user_del_90_male'] = $user_del_90_male;

                        $user_del_90_female =
                            DB::table('delusers')
                              ->where('federal_state', 'LIKE', $fed_state)
                              ->whereBetween('created_at', [$past_90, $today])->count();
                        $statistics['user_del_90_female'] = $user_del_90_female;

                    }

                    break;

            }

        }


        /** Bundesland nicht ausgewählt, alle Daten anzeigen */
        if ($fed_state == '' || $fed_state == 'all'){

            switch ($abschnitt_auswahl){

                case 0:

                    /** USERS
                     * registered users */
                    $user_registered = new User;
                    $user_reg_verified = new User;
                    $user_reg_male = new User;
                    $user_reg_female = new User;


                    if ($sporttype !== ''){


                        $statistics['sport'] = 1;

                        /** registered users */
                        $user_registered = $user_registered
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_registered'] = $user_registered;

                        $user_reg_verified = $user_reg_verified
                            ->where('verified', '=', 1)
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_reg_verified'] = $user_reg_verified;

                        $user_reg_male = $user_reg_male
                            ->where('gender', 'LIKE', 'male')
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_reg_male'] = $user_reg_male;

                        $user_reg_female = $user_reg_female
                            ->where('gender', 'LIKE', 'female')
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_reg_female'] = $user_reg_female;

                    } else{

                        /** registered users */
                        $user_registered = $user_registered->count();
                        $statistics['user_registered'] = $user_registered;

                        $user_reg_verified = $user_reg_verified
                            ->where('verified', '=', 1)->count();
                        $statistics['user_reg_verified'] = $user_reg_verified;

                        $user_reg_male = $user_reg_male
                            ->where('gender', 'LIKE', 'male')->count();
                        $statistics['user_reg_male'] = $user_reg_male;

                        $user_reg_female = $user_reg_female
                            ->where('gender', 'LIKE', 'female')->count();
                        $statistics['user_reg_female'] = $user_reg_female;

                    }

                    break;

                case 1:

                    /** new registered users 24h*/
                    $user_new_24_all = new User;
                    $user_new_24_verified = new User;
                    $user_new_24_male = new User;
                    $user_new_24_female = new User;

                    /** new registered users 7 days */
                    $user_new_7d_all = new User;
                    $user_new_7d_verified = new User;
                    $user_new_7d_male = new User;
                    $user_new_7d_female = new User;

                    /** new registered users 30 days */
                    $user_new_30d_all = new User;
                    $user_new_30d_verified = new User;
                    $user_new_30d_male = new User;
                    $user_new_30d_female = new User;

                    /** new registered users 90 days */
                    $user_new_90d_all = new User;
                    $user_new_90d_verified = new User;
                    $user_new_90d_male = new User;
                    $user_new_90d_female = new User;


                    if ($sporttype !== ''){

                        /** new registered users 24h*/
                        $user_new_24_all = $user_new_24_all
                            ->whereBetween('created_at', [$past_1, $today])
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_new_24_all'] = $user_new_24_all;

                        $user_new_24_verified = $user_new_24_verified
                            ->whereBetween('created_at', [$past_1, $today])
                            ->where('verified', '=', 1)
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_new_24_verified'] = $user_new_24_verified;

                        $user_new_24_male = $user_new_24_male
                            ->whereBetween('created_at', [$past_1, $today])
                            ->where('gender', 'LIKE', 'male')
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_new_24_male'] = $user_new_24_male;

                        $user_new_24_female = $user_new_24_female
                            ->whereBetween('created_at', [$past_1, $today])
                            ->where('gender', 'LIKE', 'female')
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_new_24_female'] = $user_new_24_female;

                        /** new registered users 7 days */
                        $user_new_7d_all = $user_new_7d_all
                            ->whereBetween('created_at', [$past_7, $today])
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_new_7d_all'] = $user_new_7d_all;

                        $user_new_7d_verified = $user_new_7d_verified
                            ->whereBetween('created_at', [$past_7, $today])
                            ->where('verified', '=', 1)
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_new_7d_verified'] = $user_new_7d_verified;

                        $user_new_7d_male = $user_new_7d_male
                            ->whereBetween('created_at', [$past_7, $today])
                            ->where('gender', 'LIKE', 'male')
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_new_7d_male'] = $user_new_7d_male;

                        $user_new_7d_female = $user_new_7d_female
                            ->whereBetween('created_at', [$past_7, $today])
                            ->where('gender', 'LIKE', 'female')
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_new_7d_female'] = $user_new_7d_female;

                        /** new registered users 30 days */
                        $user_new_30d_all = $user_new_30d_all
                            ->whereBetween('created_at', [$past_30, $today])
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_new_30d_all'] = $user_new_30d_all;

                        $user_new_30d_verified = $user_new_30d_verified
                            ->whereBetween('created_at', [$past_30, $today])
                            ->where('verified', '=', 1)
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_new_30d_verified'] = $user_new_30d_verified;

                        $user_new_30d_male = $user_new_30d_male
                            ->whereBetween('created_at', [$past_30, $today])
                            ->where('gender', 'LIKE', 'male')
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_new_30d_male'] = $user_new_30d_male;

                        $user_new_30d_female = $user_new_30d_female
                            ->whereBetween('created_at', [$past_30, $today])
                            ->where('gender', 'LIKE', 'female')
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_new_30d_female'] = $user_new_30d_female;

                        /** new registered users 90 days */
                        $user_new_90d_all = $user_new_90d_all
                            ->whereBetween('created_at', [$past_90, $today])
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_new_90d_all'] = $user_new_90d_all;

                        $user_new_90d_verified = $user_new_90d_verified
                            ->whereBetween('created_at', [$past_90, $today])
                            ->where('verified', '=', 1)
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_new_90d_verified'] = $user_new_90d_verified;

                        $user_new_90d_male = $user_new_90d_male
                            ->whereBetween('created_at', [$past_90, $today])
                            ->where('gender', 'LIKE', 'male')
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_new_90d_male'] = $user_new_90d_male;

                        $user_new_90d_female = $user_new_90d_female
                            ->whereBetween('created_at', [$past_90, $today])
                            ->where('gender', 'LIKE', 'female')
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_new_90d_female'] = $user_new_90d_female;

                    } else{

                        /** new registered users 24h*/
                        $user_new_24_all = $user_new_24_all
                            ->whereBetween('created_at', [$past_1, $today])->count();
                        $statistics['user_new_24_all'] = $user_new_24_all;

                        $user_new_24_verified = $user_new_24_verified
                            ->whereBetween('created_at', [$past_1, $today])
                            ->where('verified', '=', 1)->count();
                        $statistics['user_new_24_verified'] = $user_new_24_verified;

                        $user_new_24_male = $user_new_24_male
                            ->whereBetween('created_at', [$past_1, $today])
                            ->where('gender', 'LIKE', 'male')->count();
                        $statistics['user_new_24_male'] = $user_new_24_male;

                        $user_new_24_female = $user_new_24_female
                            ->whereBetween('created_at', [$past_1, $today])
                            ->where('gender', 'LIKE', 'female')->count();
                        $statistics['user_new_24_female'] = $user_new_24_female;

                        /** new registered users 7 days */
                        $user_new_7d_all = $user_new_7d_all
                            ->whereBetween('created_at', [$past_7, $today])->count();
                        $statistics['user_new_7d_all'] = $user_new_7d_all;

                        $user_new_7d_verified = $user_new_7d_verified
                            ->whereBetween('created_at', [$past_7, $today])
                            ->where('verified', '=', 1)->count();
                        $statistics['user_new_7d_verified'] = $user_new_7d_verified;

                        $user_new_7d_male = $user_new_7d_male
                            ->whereBetween('created_at', [$past_7, $today])
                            ->where('gender', 'LIKE', 'male')->count();
                        $statistics['user_new_7d_male'] = $user_new_7d_male;

                        $user_new_7d_female = $user_new_7d_female
                            ->whereBetween('created_at', [$past_7, $today])
                            ->where('gender', 'LIKE', 'female')->count();
                        $statistics['user_new_7d_female'] = $user_new_7d_female;

                        /** new registered users 30 days */
                        $user_new_30d_all = $user_new_30d_all
                            ->whereBetween('created_at', [$past_30, $today])->count();
                        $statistics['user_new_30d_all'] = $user_new_30d_all;

                        $user_new_30d_verified = $user_new_30d_verified
                            ->whereBetween('created_at', [$past_30, $today])
                            ->where('verified', '=', 1)->count();
                        $statistics['user_new_30d_verified'] = $user_new_30d_verified;

                        $user_new_30d_male = $user_new_30d_male
                            ->whereBetween('created_at', [$past_30, $today])
                            ->where('gender', 'LIKE', 'male')->count();
                        $statistics['user_new_30d_male'] = $user_new_30d_male;

                        $user_new_30d_female = $user_new_30d_female
                            ->whereBetween('created_at', [$past_30, $today])
                            ->where('gender', 'LIKE', 'female')->count();
                        $statistics['user_new_30d_female'] = $user_new_30d_female;

                        /** new registered users 90 days */
                        $user_new_90d_all = $user_new_90d_all
                            ->whereBetween('created_at', [$past_90, $today])->count();
                        $statistics['user_new_90d_all'] = $user_new_90d_all;

                        $user_new_90d_verified = $user_new_90d_verified
                            ->whereBetween('created_at', [$past_90, $today])
                            ->where('verified', '=', 1)->count();
                        $statistics['user_new_90d_verified'] = $user_new_90d_verified;

                        $user_new_90d_male = $user_new_90d_male
                            ->whereBetween('created_at', [$past_90, $today])
                            ->where('gender', 'LIKE', 'male')->count();
                        $statistics['user_new_90d_male'] = $user_new_90d_male;

                        $user_new_90d_female = $user_new_90d_female
                            ->whereBetween('created_at', [$past_90, $today])
                            ->where('gender', 'LIKE', 'female')->count();
                        $statistics['user_new_90d_female'] = $user_new_90d_female;

                    }

                    break;

                case 2:

                    /** all Users for age */
                    $users_all = new User;
                    $users_reg_veri = new User;
                    $users_reg_male = new User;
                    $users_reg_female = new User;
                    $i_12 = 0;
                    $i_13 = 0;
                    $i_18 = 0;
                    $i_25 = 0;
                    $i_35 = 0;
                    $i_45 = 0;
                    $i_55 = 0;
                    $i_65 = 0;

                    /** verified Users for age*/
                    $i_12_veri = 0;
                    $i_13_veri = 0;
                    $i_18_veri = 0;
                    $i_25_veri = 0;
                    $i_35_veri = 0;
                    $i_45_veri = 0;
                    $i_55_veri = 0;
                    $i_65_veri = 0;

                    /** male Users for age*/
                    $i_12_male = 0;
                    $i_13_male = 0;
                    $i_18_male = 0;
                    $i_25_male = 0;
                    $i_35_male = 0;
                    $i_45_male = 0;
                    $i_55_male = 0;
                    $i_65_male = 0;

                    /** female Users for age*/
                    $i_12_female = 0;
                    $i_13_female = 0;
                    $i_18_female = 0;
                    $i_25_female = 0;
                    $i_35_female = 0;
                    $i_45_female = 0;
                    $i_55_female = 0;
                    $i_65_female = 0;


                    if ($sporttype !== ''){

                        /** User age */
                        $users_all = $users_all
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->pluck('birthday');

                        $users_reg_veri = $users_reg_veri->where('verified', '=', 1)
                                                         ->whereHas('sports', function($query) use ($sportids){
                                                             $query->whereIn('sport_id', $sportids);
                                                         })
                                                         ->pluck('birthday');

                        $users_reg_male = $users_reg_male->where('gender', 'LIKE', 'male')
                                                         ->whereHas('sports', function($query) use ($sportids){
                                                             $query->whereIn('sport_id', $sportids);
                                                         })
                                                         ->pluck('birthday');

                        $users_reg_female = $users_reg_female->where('gender', 'LIKE', 'female')
                                                             ->whereHas('sports', function($query) use ($sportids){
                                                                 $query->whereIn('sport_id', $sportids);
                                                             })
                                                             ->pluck('birthday');


                        foreach ($users_all as $user){
                            if (is_null($user) || $user == '') continue;
                            $diff = date_diff($user, $today_new);
                            $age = $diff->format('%y');

                            if ($age < 12){
                                $i_12++;
                            }
                            if ($age >= 13 && $age <= 17){
                                $i_13++;
                            }
                            if ($age >= 18 && $age <= 24){
                                $i_18++;
                            }
                            if ($age >= 25 && $age <= 34){
                                $i_25++;
                            }
                            if ($age >= 35 && $age <= 44){
                                $i_35++;
                            }
                            if ($age >= 45 && $age <= 54){
                                $i_45++;
                            }
                            if ($age >= 55 && $age <= 64){
                                $i_55++;
                            }
                            if ($age >= 65){
                                $i_65++;
                            }

                        }

                        $statistics['12'] = $i_12;
                        $statistics['13'] = $i_13;
                        $statistics['18'] = $i_18;
                        $statistics['25'] = $i_25;
                        $statistics['35'] = $i_35;
                        $statistics['45'] = $i_45;
                        $statistics['55'] = $i_55;
                        $statistics['65'] = $i_65;


                        foreach ($users_reg_veri as $user_veri){
                            if (is_null($user_veri) || $user_veri == '') continue;
                            $diff_veri = date_diff($user_veri, $today_new);
                            $age_veri = $diff_veri->format('%y');

                            if ($age_veri < 12){
                                $i_12_veri++;
                            }
                            if ($age_veri >= 13 && $age_veri <= 17){
                                $i_13_veri++;
                            }
                            if ($age_veri >= 18 && $age_veri <= 24){
                                $i_18_veri++;
                            }
                            if ($age_veri >= 25 && $age_veri <= 34){
                                $i_25_veri++;
                            }
                            if ($age_veri >= 35 && $age_veri <= 44){
                                $i_35_veri++;
                            }
                            if ($age_veri >= 45 && $age_veri <= 54){
                                $i_45_veri++;
                            }
                            if ($age_veri >= 55 && $age_veri <= 64){
                                $i_55_veri++;
                            }
                            if ($age_veri >= 65){
                                $i_65_veri++;
                            }
                        }

                        $statistics['12_veri'] = $i_12_veri;
                        $statistics['13_veri'] = $i_13_veri;
                        $statistics['18_veri'] = $i_18_veri;
                        $statistics['25_veri'] = $i_25_veri;
                        $statistics['35_veri'] = $i_35_veri;
                        $statistics['45_veri'] = $i_45_veri;
                        $statistics['55_veri'] = $i_55_veri;
                        $statistics['65_veri'] = $i_65_veri;


                        foreach ($users_reg_male as $user_male){
                            if (is_null($user_male) || $user_male == '') continue;
                            $diff_male = date_diff($user_male, $today_new);
                            $age_male = $diff_male->format('%y');

                            if ($age_male < 12){
                                $i_12_male++;
                            }
                            if ($age_male >= 13 && $age_male <= 17){
                                $i_13_male++;
                            }
                            if ($age_male >= 18 && $age_male <= 24){
                                $i_18_male++;
                            }
                            if ($age_male >= 25 && $age_male <= 34){
                                $i_25_male++;
                            }
                            if ($age_male >= 35 && $age_male <= 44){
                                $i_35_male++;
                            }
                            if ($age_male >= 45 && $age_male <= 54){
                                $i_45_male++;
                            }
                            if ($age_male >= 55 && $age_male <= 64){
                                $i_55_male++;
                            }
                            if ($age_male >= 65){
                                $i_65_male++;
                            }
                        }

                        $statistics['12_male'] = $i_12_male;
                        $statistics['13_male'] = $i_13_male;
                        $statistics['18_male'] = $i_18_male;
                        $statistics['25_male'] = $i_25_male;
                        $statistics['35_male'] = $i_35_male;
                        $statistics['45_male'] = $i_45_male;
                        $statistics['55_male'] = $i_55_male;
                        $statistics['65_male'] = $i_65_male;


                        foreach ($users_reg_female as $user_female){
                            if (is_null($user_female) || $user_female == '') continue;
                            $diff_female = date_diff($user_female, $today_new);
                            $age_female = $diff_female->format('%y');

                            if ($age_female < 12){
                                $i_12_female++;
                            }
                            if ($age_female >= 13 && $age_female <= 17){
                                $i_13_female++;
                            }
                            if ($age_female >= 18 && $age_female <= 24){
                                $i_18_female++;
                            }
                            if ($age_female >= 25 && $age_female <= 34){
                                $i_25_female++;
                            }
                            if ($age_female >= 35 && $age_female <= 44){
                                $i_35_female++;
                            }
                            if ($age_female >= 45 && $age_female <= 54){
                                $i_45_female++;
                            }
                            if ($age_female >= 55 && $age_female <= 64){
                                $i_55_female++;
                            }
                            if ($age_female >= 65){
                                $i_65_female++;
                            }
                        }

                        $statistics['12_female'] = $i_12_female;
                        $statistics['13_female'] = $i_13_female;
                        $statistics['18_female'] = $i_18_female;
                        $statistics['25_female'] = $i_25_female;
                        $statistics['35_female'] = $i_35_female;
                        $statistics['45_female'] = $i_45_female;
                        $statistics['55_female'] = $i_55_female;
                        $statistics['65_female'] = $i_65_female;

                    } else{

                        /** User age */
                        $users_all = $users_all->pluck('birthday');

                        $users_reg_veri = $users_reg_veri->where('verified', '=', 1)->pluck('birthday');

                        $users_reg_male = $users_reg_male->where('gender', 'LIKE', 'male')->pluck('birthday');

                        $users_reg_female = $users_reg_female->where('gender', 'LIKE', 'female')->pluck('birthday');


                        foreach ($users_all as $user){
                            if (is_null($user) || $user == '') continue;
                            $diff = date_diff($user, $today_new);
                            $age = $diff->format('%y');

                            if ($age < 12){
                                $i_12++;
                            }
                            if ($age >= 13 && $age <= 17){
                                $i_13++;
                            }
                            if ($age >= 18 && $age <= 24){
                                $i_18++;
                            }
                            if ($age >= 25 && $age <= 34){
                                $i_25++;
                            }
                            if ($age >= 35 && $age <= 44){
                                $i_35++;
                            }
                            if ($age >= 45 && $age <= 54){
                                $i_45++;
                            }
                            if ($age >= 55 && $age <= 64){
                                $i_55++;
                            }
                            if ($age >= 65){
                                $i_65++;
                            }

                        }

                        $statistics['12'] = $i_12;
                        $statistics['13'] = $i_13;
                        $statistics['18'] = $i_18;
                        $statistics['25'] = $i_25;
                        $statistics['35'] = $i_35;
                        $statistics['45'] = $i_45;
                        $statistics['55'] = $i_55;
                        $statistics['65'] = $i_65;


                        foreach ($users_reg_veri as $user_veri){
                            if (is_null($user_veri) || $user_veri == '') continue;
                            $diff_veri = date_diff($user_veri, $today_new);
                            $age_veri = $diff_veri->format('%y');

                            if ($age_veri < 12){
                                $i_12_veri++;
                            }
                            if ($age_veri >= 13 && $age_veri <= 17){
                                $i_13_veri++;
                            }
                            if ($age_veri >= 18 && $age_veri <= 24){
                                $i_18_veri++;
                            }
                            if ($age_veri >= 25 && $age_veri <= 34){
                                $i_25_veri++;
                            }
                            if ($age_veri >= 35 && $age_veri <= 44){
                                $i_35_veri++;
                            }
                            if ($age_veri >= 45 && $age_veri <= 54){
                                $i_45_veri++;
                            }
                            if ($age_veri >= 55 && $age_veri <= 64){
                                $i_55_veri++;
                            }
                            if ($age_veri >= 65){
                                $i_65_veri++;
                            }
                        }

                        $statistics['12_veri'] = $i_12_veri;
                        $statistics['13_veri'] = $i_13_veri;
                        $statistics['18_veri'] = $i_18_veri;
                        $statistics['25_veri'] = $i_25_veri;
                        $statistics['35_veri'] = $i_35_veri;
                        $statistics['45_veri'] = $i_45_veri;
                        $statistics['55_veri'] = $i_55_veri;
                        $statistics['65_veri'] = $i_65_veri;


                        foreach ($users_reg_male as $user_male){
                            if (is_null($user_male) || $user_male == '') continue;
                            $diff_male = date_diff($user_male, $today_new);
                            $age_male = $diff_male->format('%y');

                            if ($age_male < 12){
                                $i_12_male++;
                            }
                            if ($age_male >= 13 && $age_male <= 17){
                                $i_13_male++;
                            }
                            if ($age_male >= 18 && $age_male <= 24){
                                $i_18_male++;
                            }
                            if ($age_male >= 25 && $age_male <= 34){
                                $i_25_male++;
                            }
                            if ($age_male >= 35 && $age_male <= 44){
                                $i_35_male++;
                            }
                            if ($age_male >= 45 && $age_male <= 54){
                                $i_45_male++;
                            }
                            if ($age_male >= 55 && $age_male <= 64){
                                $i_55_male++;
                            }
                            if ($age_male >= 65){
                                $i_65_male++;
                            }
                        }

                        $statistics['12_male'] = $i_12_male;
                        $statistics['13_male'] = $i_13_male;
                        $statistics['18_male'] = $i_18_male;
                        $statistics['25_male'] = $i_25_male;
                        $statistics['35_male'] = $i_35_male;
                        $statistics['45_male'] = $i_45_male;
                        $statistics['55_male'] = $i_55_male;
                        $statistics['65_male'] = $i_65_male;


                        foreach ($users_reg_female as $user_female){
                            if (is_null($user_female) || $user_female == '') continue;
                            $diff_female = date_diff($user_female, $today_new);
                            $age_female = $diff_female->format('%y');

                            if ($age_female < 12){
                                $i_12_female++;
                            }
                            if ($age_female >= 13 && $age_female <= 17){
                                $i_13_female++;
                            }
                            if ($age_female >= 18 && $age_female <= 24){
                                $i_18_female++;
                            }
                            if ($age_female >= 25 && $age_female <= 34){
                                $i_25_female++;
                            }
                            if ($age_female >= 35 && $age_female <= 44){
                                $i_35_female++;
                            }
                            if ($age_female >= 45 && $age_female <= 54){
                                $i_45_female++;
                            }
                            if ($age_female >= 55 && $age_female <= 64){
                                $i_55_female++;
                            }
                            if ($age_female >= 65){
                                $i_65_female++;
                            }
                        }

                        $statistics['12_female'] = $i_12_female;
                        $statistics['13_female'] = $i_13_female;
                        $statistics['18_female'] = $i_18_female;
                        $statistics['25_female'] = $i_25_female;
                        $statistics['35_female'] = $i_35_female;
                        $statistics['45_female'] = $i_45_female;
                        $statistics['55_female'] = $i_55_female;
                        $statistics['65_female'] = $i_65_female;

                    }


                    break;

                case 3:

                    /** activity 24h */
                    $user_act_24_all = new User;
                    $user_act_24_male = new User;
                    $user_act_24_female = new User;

                    /** activity 7d */
                    $user_act_7d_all = new User;
                    $user_act_7d_male = new User;
                    $user_act_7d_female = new User;

                    /** activity 30d */
                    $user_act_30d_all = new User;
                    $user_act_30d_male = new User;
                    $user_act_30d_female = new User;

                    /** activity 90d */
                    $user_act_90d_all = new User;
                    $user_act_90d_male = new User;
                    $user_act_90d_female = new User;


                    if ($sporttype !== ''){
                        /** User Activity */
                        /** activity 24h */
                        $user_act_24_all = $user_act_24_all
                            ->whereBetween('login_at', [$past_1, $today])
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })->count();
                        $statistics['user_act_24_all'] = $user_act_24_all;

                        $user_act_24_male = $user_act_24_male
                            ->whereBetween('login_at', [$past_1, $today])
                            ->where('gender', 'LIKE', 'male')
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_act_24_male'] = $user_act_24_male;

                        $user_act_24_female = $user_act_24_female
                            ->whereBetween('login_at', [$past_1, $today])
                            ->where('gender', 'LIKE', 'female')
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_act_24_female'] = $user_act_24_female;

                        /** activity 7d */
                        $user_act_7d_all = $user_act_7d_all
                            ->whereBetween('login_at', [$past_7, $today])
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_act_7d_all'] = $user_act_7d_all;

                        $user_act_7d_male = $user_act_7d_male
                            ->whereBetween('login_at', [$past_7, $today])
                            ->where('gender', 'LIKE', 'male')
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_act_7d_male'] = $user_act_7d_male;

                        $user_act_7d_female = $user_act_7d_female
                            ->whereBetween('login_at', [$past_7, $today])
                            ->where('gender', 'LIKE', 'female')
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_act_7d_female'] = $user_act_7d_female;

                        /** activity 30d */
                        $user_act_30d_all = $user_act_30d_all
                            ->whereBetween('login_at', [$past_30, $today])
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_act_30d_all'] = $user_act_30d_all;

                        $user_act_30d_male = $user_act_30d_male
                            ->whereBetween('login_at', [$past_30, $today])
                            ->where('gender', 'LIKE', 'male')
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_act_30d_male'] = $user_act_30d_male;

                        $user_act_30d_female = $user_act_30d_female
                            ->whereBetween('login_at', [$past_30, $today])
                            ->where('gender', 'LIKE', 'female')
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_act_30d_female'] = $user_act_30d_female;

                        /** activity 90d */
                        $user_act_90d_all = $user_act_90d_all
                            ->whereBetween('login_at', [$past_90, $today])
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_act_90d_all'] = $user_act_90d_all;

                        $user_act_90d_male = $user_act_90d_male
                            ->whereBetween('login_at', [$past_90, $today])
                            ->where('gender', 'LIKE', 'male')
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_act_90d_male'] = $user_act_90d_male;

                        $user_act_90d_female = $user_act_90d_female
                            ->whereBetween('login_at', [$past_90, $today])
                            ->where('gender', 'LIKE', 'female')
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_act_90d_female'] = $user_act_90d_female;
                    } else{

                        /** User Activity */
                        /** activity 24h */
                        $user_act_24_all = $user_act_24_all
                            ->whereBetween('login_at', [$past_1, $today])->count();
                        $statistics['user_act_24_all'] = $user_act_24_all;

                        $user_act_24_male = $user_act_24_male
                            ->whereBetween('login_at', [$past_1, $today])
                            ->where('gender', 'LIKE', 'male')->count();
                        $statistics['user_act_24_male'] = $user_act_24_male;

                        $user_act_24_female = $user_act_24_female
                            ->whereBetween('login_at', [$past_1, $today])
                            ->where('gender', 'LIKE', 'female')->count();
                        $statistics['user_act_24_female'] = $user_act_24_female;

                        /** activity 7d */
                        $user_act_7d_all = $user_act_7d_all
                            ->whereBetween('login_at', [$past_7, $today])->count();
                        $statistics['user_act_7d_all'] = $user_act_7d_all;

                        $user_act_7d_male = $user_act_7d_male
                            ->whereBetween('login_at', [$past_7, $today])
                            ->where('gender', 'LIKE', 'male')->count();
                        $statistics['user_act_7d_male'] = $user_act_7d_male;

                        $user_act_7d_female = $user_act_7d_female
                            ->whereBetween('login_at', [$past_7, $today])
                            ->where('gender', 'LIKE', 'female')->count();
                        $statistics['user_act_7d_female'] = $user_act_7d_female;

                        /** activity 30d */
                        $user_act_30d_all = $user_act_30d_all
                            ->whereBetween('login_at', [$past_30, $today])->count();
                        $statistics['user_act_30d_all'] = $user_act_30d_all;

                        $user_act_30d_male = $user_act_30d_male
                            ->whereBetween('login_at', [$past_30, $today])
                            ->where('gender', 'LIKE', 'male')->count();
                        $statistics['user_act_30d_male'] = $user_act_30d_male;

                        $user_act_30d_female = $user_act_30d_female
                            ->whereBetween('login_at', [$past_30, $today])
                            ->where('gender', 'LIKE', 'female')->count();
                        $statistics['user_act_30d_female'] = $user_act_30d_female;

                        /** activity 90d */
                        $user_act_90d_all = $user_act_90d_all
                            ->whereBetween('login_at', [$past_90, $today])->count();
                        $statistics['user_act_90d_all'] = $user_act_90d_all;

                        $user_act_90d_male = $user_act_90d_male
                            ->whereBetween('login_at', [$past_90, $today])
                            ->where('gender', 'LIKE', 'male')->count();
                        $statistics['user_act_90d_male'] = $user_act_90d_male;

                        $user_act_90d_female = $user_act_90d_female
                            ->whereBetween('login_at', [$past_90, $today])
                            ->where('gender', 'LIKE', 'female')->count();
                        $statistics['user_act_90d_female'] = $user_act_90d_female;

                    }

                    break;

                case 4:

                    /** user mandatory part */
                    $user_mand_part_all = new User;
                    $user_mand_part_male = new User;
                    $user_mand_part_female = new User;

                    /** user mandatory full */
                    $user_mand_all = new User;
                    $user_mand_male = new User;
                    $user_mand_female = new User;

                    /** user sportvita */
                    $user_sportvita_all = new User;
                    $user_sportvita_male = new User;
                    $user_sportvita_female = new User;

                    /** user profile pic */
                    $user_profile_pic_all = new User;
                    $user_profile_pic_male = new User;
                    $user_profile_pic_female = new User;


                    if ($sporttype !== ''){

                        /** User intensity */
                        /** user profile mandatory part */
                        $user_mand_part_all = $user_mand_part_all
                            ->where(function($query){
                                $query->whereNull('firstname')
                                      ->orWhereNull('lastname')
                                      ->orWhereNull('birthday')
                                      ->orWhereNull('city')
                                      ->orWhereNull('zip');
                            })
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_mand_part_all'] = $user_mand_part_all;

                        $user_mand_part_male = $user_mand_part_male->where('gender', 'LIKE', 'male')
                                                                   ->where(function($query){
                                                                       $query->whereNull('firstname')
                                                                             ->orWhereNull('lastname')
                                                                             ->orWhereNull('birthday')
                                                                             ->orWhereNull('city')
                                                                             ->orWhereNull('zip');
                                                                   })
                                                                   ->whereHas('sports', function($query) use ($sportids){
                                                                       $query->whereIn('sport_id', $sportids);
                                                                   })
                                                                   ->count();
                        $statistics['user_mand_part_male'] = $user_mand_part_male;

                        $user_mand_part_female = $user_mand_part_female->where('gender', 'LIKE', 'female')
                                                                       ->where(function($query){
                                                                           $query->whereNull('firstname')
                                                                                 ->orWhereNull('lastname')
                                                                                 ->orWhereNull('birthday')
                                                                                 ->orWhereNull('city')
                                                                                 ->orWhereNull('zip');
                                                                       })
                                                                       ->whereHas('sports', function($query) use ($sportids){
                                                                           $query->whereIn('sport_id', $sportids);
                                                                       })
                                                                       ->count();
                        $statistics['user_mand_part_female'] = $user_mand_part_female;

                        /** user profile mandatory full */
                        $user_mand_all = $user_mand_all
                            ->whereNotNull('firstname')->whereNotNull('lastname')->whereNotNull('birthday')->whereNotNull('city')
                            ->whereNotNull('gender')->whereNotNull('zip')
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_mand_all'] = $user_mand_all;

                        $user_mand_male = $user_mand_male->where('gender', 'LIKE', 'male')
                                                         ->whereNotNull('firstname')->whereNotNull('lastname')->whereNotNull('birthday')->whereNotNull('city')
                                                         ->whereNotNull('zip')
                                                         ->whereHas('sports', function($query) use ($sportids){
                                                             $query->whereIn('sport_id', $sportids);
                                                         })
                                                         ->count();
                        $statistics['user_mand_male'] = $user_mand_male;

                        $user_mand_female = $user_mand_female->where('gender', 'LIKE', 'female')
                                                             ->whereNotNull('firstname')->whereNotNull('lastname')->whereNotNull('birthday')->whereNotNull('city')
                                                             ->whereNotNull('zip')
                                                             ->whereHas('sports', function($query) use ($sportids){
                                                                 $query->whereIn('sport_id', $sportids);
                                                             })
                                                             ->count();
                        $statistics['user_mand_female'] = $user_mand_female;


                        /** user sportvita */
                        $user_sportvita_all = $user_sportvita_all
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_sportvita_all'] = $user_sportvita_all;

                        $user_sportvita_male = $user_sportvita_male
                            ->where('gender', 'LIKE', 'male')
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_sportvita_male'] = $user_sportvita_male;

                        $user_sportvita_female = $user_sportvita_female
                            ->where('gender', 'LIKE', 'female')
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_sportvita_female'] = $user_sportvita_female;


                        /** user profile pic */
                        $user_profile_pic_all = $user_profile_pic_all
                            ->whereNotNull('avatar_file_name')
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_profile_pic_all'] = $user_profile_pic_all;

                        $user_profile_pic_male = $user_profile_pic_male
                            ->whereNotNull('avatar_file_name')
                            ->where('gender', 'LIKE', 'male')
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_profile_pic_male'] = $user_profile_pic_male;

                        $user_profile_pic_female = $user_profile_pic_female
                            ->whereNotNull('avatar_file_name')
                            ->where('gender', 'LIKE', 'male')
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['user_profile_pic_female'] = $user_profile_pic_female;

                    } else{

                        /** User intensity */
                        /** user profile mandatory part */
                        $user_mand_part_all = $user_mand_part_all
                            ->where(function($query){
                                $query->whereNull('firstname')
                                      ->orWhereNull('lastname')
                                      ->orWhereNull('birthday')
                                      ->orWhereNull('city')
                                      ->orWhereNull('zip');
                            })->count();
                        $statistics['user_mand_part_all'] = $user_mand_part_all;

                        $user_mand_part_male = $user_mand_part_male->where('gender', 'LIKE', 'male')
                                                                   ->where(function($query){
                                                                       $query->whereNull('firstname')
                                                                             ->orWhereNull('lastname')
                                                                             ->orWhereNull('birthday')
                                                                             ->orWhereNull('city')
                                                                             ->orWhereNull('zip');
                                                                   })->count();
                        $statistics['user_mand_part_male'] = $user_mand_part_male;

                        $user_mand_part_female = $user_mand_part_female->where('gender', 'LIKE', 'female')
                                                                       ->where(function($query){
                                                                           $query->whereNull('firstname')
                                                                                 ->orWhereNull('lastname')
                                                                                 ->orWhereNull('birthday')
                                                                                 ->orWhereNull('city')
                                                                                 ->orWhereNull('zip');
                                                                       })->count();
                        $statistics['user_mand_part_female'] = $user_mand_part_female;

                        /** user profile mandatory full */
                        $user_mand_all = $user_mand_all
                            ->whereNotNull('firstname')->whereNotNull('lastname')->whereNotNull('birthday')->whereNotNull('city')
                            ->whereNotNull('gender')->whereNotNull('zip')->count();
                        $statistics['user_mand_all'] = $user_mand_all;

                        $user_mand_male = $user_mand_male->where('gender', 'LIKE', 'male')
                                                         ->whereNotNull('firstname')->whereNotNull('lastname')->whereNotNull('birthday')->whereNotNull('city')
                                                         ->whereNotNull('zip')->count();
                        $statistics['user_mand_male'] = $user_mand_male;

                        $user_mand_female = $user_mand_female->where('gender', 'LIKE', 'female')
                                                             ->whereNotNull('firstname')->whereNotNull('lastname')->whereNotNull('birthday')->whereNotNull('city')
                                                             ->whereNotNull('zip')->count();
                        $statistics['user_mand_female'] = $user_mand_female;


                        /** user sportvita */
                        $user_sportvita_all = $user_sportvita_all->has('sports')->count();
                        $statistics['user_sportvita_all'] = $user_sportvita_all;

                        $user_sportvita_male = $user_sportvita_male
                            ->where('gender', 'LIKE', 'male')
                            ->has('sports')->count();
                        $statistics['user_sportvita_male'] = $user_sportvita_male;

                        $user_sportvita_female = $user_sportvita_female
                            ->where('gender', 'LIKE', 'female')
                            ->has('sports')->count();
                        $statistics['user_sportvita_female'] = $user_sportvita_female;


                        /** user profile pic */
                        $user_profile_pic_all = $user_profile_pic_all
                            ->whereNotNull('avatar_file_name')->count();
                        $statistics['user_profile_pic_all'] = $user_profile_pic_all;

                        $user_profile_pic_male = $user_profile_pic_male
                            ->whereNotNull('avatar_file_name')
                            ->where('gender', 'LIKE', 'male')
                            ->count();
                        $statistics['user_profile_pic_male'] = $user_profile_pic_male;

                        $user_profile_pic_female = $user_profile_pic_female
                            ->whereNotNull('avatar_file_name')
                            ->where('gender', 'LIKE', 'male')
                            ->count();
                        $statistics['user_profile_pic_female'] = $user_profile_pic_female;

                    }

                    break;

                case 5:

                    /** user deletion profile 24h*/
                    $user_del_24_all = new User;
                    $user_del_24_male = new User;
                    $user_del_24_female = new User;

                    /** user deletion profile 7d*/
                    $user_del_7_all = new User;
                    $user_del_7_male = new User;
                    $user_del_7_female = new User;

                    /** user deletion profile 30d*/
                    $user_del_30_all = new User;
                    $user_del_30_male = new User;
                    $user_del_30_female = new User;

                    /** user deletion profile 90d*/
                    $user_del_90_all = new User;
                    $user_del_90_male = new User;
                    $user_del_90_female = new User;


                    if ($sporttype !== ''){

                        /** user deletion profile 24h*/
                        $user_del_24_all = $user_del_24_all->onlyTrashed()
                                                           ->whereBetween('deleted_at', [$past_1, $today])
                                                           ->whereHas('sports', function($query) use ($sportids){
                                                               $query->whereIn('sport_id', $sportids);
                                                           })
                                                           ->count();
                        $statistics['user_del_24_all'] = $user_del_24_all;

                        $user_del_24_male = $user_del_24_male->onlyTrashed()
                                                             ->where('gender', 'LIKE', 'male')
                                                             ->whereBetween('deleted_at', [$past_1, $today])
                                                             ->whereHas('sports', function($query) use ($sportids){
                                                                 $query->whereIn('sport_id', $sportids);
                                                             })
                                                             ->count();
                        $statistics['user_del_24_male'] = $user_del_24_male;

                        $user_del_24_female = $user_del_24_female->onlyTrashed()
                                                                 ->where('gender', 'LIKE', 'female')
                                                                 ->whereBetween('deleted_at', [$past_1, $today])
                                                                 ->whereHas('sports', function($query) use ($sportids){
                                                                     $query->whereIn('sport_id', $sportids);
                                                                 })
                                                                 ->count();
                        $statistics['user_del_24_female'] = $user_del_24_female;

                        /** user deletion profile 7d */
                        $user_del_7_all = $user_del_7_all->onlyTrashed()
                                                         ->whereBetween('deleted_at', [$past_7, $today])
                                                         ->whereHas('sports', function($query) use ($sportids){
                                                             $query->whereIn('sport_id', $sportids);
                                                         })
                                                         ->count();
                        $statistics['user_del_7_all'] = $user_del_7_all;

                        $user_del_7_male = $user_del_7_male->onlyTrashed()
                                                           ->where('gender', 'LIKE', 'male')
                                                           ->whereBetween('deleted_at', [$past_7, $today])
                                                           ->whereHas('sports', function($query) use ($sportids){
                                                               $query->whereIn('sport_id', $sportids);
                                                           })
                                                           ->count();
                        $statistics['user_del_7_male'] = $user_del_7_male;

                        $user_del_7_female = $user_del_7_female->onlyTrashed()
                                                               ->where('gender', 'LIKE', 'female')
                                                               ->whereBetween('deleted_at', [$past_7, $today])
                                                               ->whereHas('sports', function($query) use ($sportids){
                                                                   $query->whereIn('sport_id', $sportids);
                                                               })
                                                               ->count();
                        $statistics['user_del_7_female'] = $user_del_7_female;

                        /** user deletion profile 30d */
                        $user_del_30_all = $user_del_30_all->onlyTrashed()
                                                           ->whereBetween('deleted_at', [$past_30, $today])
                                                           ->whereHas('sports', function($query) use ($sportids){
                                                               $query->whereIn('sport_id', $sportids);
                                                           })
                                                           ->count();
                        $statistics['user_del_30_all'] = $user_del_30_all;

                        $user_del_30_male = $user_del_30_male->onlyTrashed()
                                                             ->where('gender', 'LIKE', 'male')
                                                             ->whereBetween('deleted_at', [$past_30, $today])
                                                             ->whereHas('sports', function($query) use ($sportids){
                                                                 $query->whereIn('sport_id', $sportids);
                                                             })
                                                             ->count();
                        $statistics['user_del_30_male'] = $user_del_30_male;

                        $user_del_30_female = $user_del_30_female->onlyTrashed()
                                                                 ->where('gender', 'LIKE', 'female')
                                                                 ->whereBetween('deleted_at', [$past_30, $today])
                                                                 ->whereHas('sports', function($query) use ($sportids){
                                                                     $query->whereIn('sport_id', $sportids);
                                                                 })
                                                                 ->count();
                        $statistics['user_del_30_female'] = $user_del_30_female;

                        /** user deletion profile 90d */
                        $user_del_90_all = $user_del_90_all->onlyTrashed()
                                                           ->whereBetween('deleted_at', [$past_90, $today])
                                                           ->whereHas('sports', function($query) use ($sportids){
                                                               $query->whereIn('sport_id', $sportids);
                                                           })
                                                           ->count();
                        $statistics['user_del_90_all'] = $user_del_90_all;

                        $user_del_90_male = $user_del_90_male->onlyTrashed()
                                                             ->where('gender', 'LIKE', 'male')
                                                             ->whereBetween('deleted_at', [$past_90, $today])
                                                             ->whereHas('sports', function($query) use ($sportids){
                                                                 $query->whereIn('sport_id', $sportids);
                                                             })
                                                             ->count();
                        $statistics['user_del_90_male'] = $user_del_90_male;

                        $user_del_90_female = $user_del_90_female->onlyTrashed()
                                                                 ->where('gender', 'LIKE', 'female')
                                                                 ->whereBetween('deleted_at', [$past_90, $today])
                                                                 ->whereHas('sports', function($query) use ($sportids){
                                                                     $query->whereIn('sport_id', $sportids);
                                                                 })
                                                                 ->count();
                        $statistics['user_del_90_female'] = $user_del_90_female;

                    } else{

                        /** user deletion profile 24h*/
                        $user_del_24_all =
                            DB::table('delusers')
                              ->whereBetween('created_at', [$past_1, $today])->count();
                        $statistics['user_del_24_all'] = $user_del_24_all;

                        $user_del_24_male =
                            DB::table('delusers')
                              ->whereBetween('created_at', [$past_1, $today])->count();
                        $statistics['user_del_24_male'] = $user_del_24_male;

                        $user_del_24_female =
                            DB::table('delusers')
                              ->whereBetween('created_at', [$past_1, $today])->count();
                        $statistics['user_del_24_female'] = $user_del_24_female;

                        /** user deletion profile 7d */
                        $user_del_7_all =
                            DB::table('delusers')
                              ->whereBetween('created_at', [$past_7, $today])->count();
                        $statistics['user_del_7_all'] = $user_del_7_all;

                        $user_del_7_male =
                            DB::table('delusers')
                              ->whereBetween('created_at', [$past_7, $today])->count();
                        $statistics['user_del_7_male'] = $user_del_7_male;

                        $user_del_7_female =
                            DB::table('delusers')
                              ->whereBetween('created_at', [$past_7, $today])->count();
                        $statistics['user_del_7_female'] = $user_del_7_female;

                        /** user deletion profile 30d */
                        $user_del_30_all =
                            DB::table('delusers')
                              ->whereBetween('created_at', [$past_30, $today])->count();
                        $statistics['user_del_30_all'] = $user_del_30_all;

                        $user_del_30_male =
                            DB::table('delusers')
                              ->whereBetween('created_at', [$past_30, $today])->count();
                        $statistics['user_del_30_male'] = $user_del_30_male;

                        $user_del_30_female =
                            DB::table('delusers')
                              ->whereBetween('created_at', [$past_30, $today])->count();
                        $statistics['user_del_30_female'] = $user_del_30_female;

                        /** user deletion profile 90d */
                        $user_del_90_all =
                            DB::table('delusers')
                              ->whereBetween('created_at', [$past_90, $today])->count();
                        $statistics['user_del_90_all'] = $user_del_90_all;

                        $user_del_90_male =
                            DB::table('delusers')
                              ->whereBetween('created_at', [$past_90, $today])->count();
                        $statistics['user_del_90_male'] = $user_del_90_male;

                        $user_del_90_female =
                            DB::table('delusers')
                              ->whereBetween('created_at', [$past_90, $today])->count();
                        $statistics['user_del_90_female'] = $user_del_90_female;

                    }

                    break;
            }

        }

        return view('admin.dashboard.user.index', [
            'statistics'        => $statistics,
            'abschnitt'         => $abschnitt,
            'abschnitt_auswahl' => $abschnitt_auswahl,
            'query'             => [
                'state' => $fed_state
            ]
        ]);

    }


    /**
     * Club Statistics for new Admin Dashboard
     */

    public function clubStatistics(Request $request)
    {

        $federal_states = State::pluck('name', 'id')->toArray();

        $fed_state = trim($request->input('state', ''));

        $sporttype = $request->input('sport', '');

        $statistics = array();

        $abschnitt = ['Vereine', 'Aktivität', 'Intensität'];

        $abschnitt_auswahl = trim($request->input('abschnitt'));

        $subabschnitt_auswahl = $request->input('subabschnitt');

        $today = date('Y-m-d');
        $today_new = date_create($today);
        $past_1 = date('Y-m-d', strtotime("-1 day"));
        $past_7 = date('Y-m-d', strtotime("-7 days"));
        $past_30 = date('Y-m-d', strtotime("-30 days"));
        $past_90 = date('Y-m-d', strtotime("-90 days"));

        $role = new Role;
        $adminClub = serialize($role->getByConstant(Role::CLUB_ADMIN)->id);


        /** UNPUBLISHED CLUBS with ADMIN */
        //                        SELECT COUNT(*) FROM club c JOIN user_club_role u ON c.id = u.club_id WHERE u.role_id = 4 AND c.published = 0
        $unpublished_clubs_with_admin = DB::table('club')
                                          ->join('user_club_role', function($join){
                                              $join->on('club.id', '=', 'user_club_role.club_id')
                                                   ->where('user_club_role.role_id', '=', 4);
                                          })
                                          ->where('club.published', '=', 0)->count();


        /** ALL CLUBS */
        $all_clubs = new Club;
        $all_clubs = $all_clubs->count();
        $statistics['all_clubs'] = $all_clubs;


        /** Sport IDs */
        $sportids = array();
        $sporty = Sport::where('title', 'LIKE', '%' . $sporttype . '%')->get();
        foreach ($sporty as $sp){
            array_push($sportids, $sp->id);
        }

        $statistics['federal'] = 0;
        $statistics['sport'] = 0;


        if ($fed_state !== '' && $fed_state !== 'all'){

            $statistics['federal'] = 1;

            switch ($abschnitt_auswahl){

                case 0:

                    /** CLUBS federal*/
                    $clubs_all_fed = new Club;
                    $clubs_accepted_fed = new Club;
                    $clubs_sports_fed = new Club;

                    $i_clubsport_1_fed = 0;
                    $i_clubsport_n_fed = 0;

                    /** CLUBS */
                    $clubs_all_fed = $clubs_all_fed->where('federal_state', 'LIKE', $fed_state)->get()->count();
                    $statistics['clubs_all_fed'] = $clubs_all_fed;

                    $clubs_accepted_fed = $clubs_accepted_fed->where('federal_state', 'LIKE', $fed_state)
                                                             ->whereHas('users')->get()->count();
                    $statistics['clubs_accepted_fed'] = $clubs_accepted_fed;


                    $clubs_sports_fed = $clubs_sports_fed
                        ->where('federal_state', 'LIKE', $fed_state)
                        ->withCount('sports')->get();

                    foreach ($clubs_sports_fed as $club){
                        if ($club->sports_count == 1){
                            $i_clubsport_1_fed++;
                        }
                        if ($club->sports_count > 1){
                            $i_clubsport_n_fed++;
                        }
                    }

                    $statistics['clubs_1_branch_fed'] = $i_clubsport_1_fed;
                    $statistics['clubs_n_branch_fed'] = $i_clubsport_n_fed;

                    break;


                case 1:

                    switch ($subabschnitt_auswahl){

                        case "24h":
                            /** Club activity 24h fed*/
                            $clubs_24_all_fed = new Club;
                            $clubs_24_accepted_fed = new Club;
                            $clubs_sports_24_fed = new Club;
                            $i_clubsport_act24_1_fed = 0;
                            $i_clubsport_act24_n_fed = 0;

                            /** Club activity 24h */
                            $clubs_24_all_fed = $clubs_24_all_fed
                                ->where('federal_state', 'LIKE', $fed_state)
                                ->whereBetween('updated_at', [$past_1, $today])
                                ->orWhere(function($query) use ($past_1, $today){
                                    $query->whereHas('posts', function($query2) use ($past_1, $today){
                                        $query2->whereBetween('post.created_at', [$past_1, $today]);
                                    });
                                    //$query->has('posts')->whereBetween('created_at', [$past_1, $today]);
                                })
                                ->get()->count();
                            $statistics['clubs_24_all_fed'] = $clubs_24_all_fed;

                            $clubs_24_accepted_fed = $clubs_24_accepted_fed
                                ->where('federal_state', 'LIKE', $fed_state)
//                                ->whereHas('users', function($query) use ($adminClub){
//                                    $query->where('user_club_role.role_id', '=', $adminClub);
//                                })
                                ->whereHas('users')
                                ->whereBetween('updated_at', [$past_1, $today])
                                ->orWhere(function($query) use ($past_1, $today){
                                    $query->whereHas('posts', function($query2) use ($past_1, $today){
                                        $query2->whereBetween('post.created_at', [$past_1, $today]);
                                    });
                                    //$query->has('posts')->whereBetween('created_at', [$past_1, $today]);
                                })
                                ->get()->count();
                            $statistics['clubs_24_accepted_fed'] = $clubs_24_accepted_fed;


                            $clubs_sports_24_fed = $clubs_sports_24_fed->where('federal_state', 'LIKE', $fed_state)
                                                                       ->whereBetween('updated_at', [$past_1, $today])
                                                                       ->orWhere(function($query) use ($past_1, $today){
                                                                           $query->whereHas('posts', function($query2) use ($past_1, $today){
                                                                               $query2->whereBetween('post.created_at', [$past_1, $today]);
                                                                           });
                                                                           //$query->has('posts')->whereBetween('created_at', [$past_1, $today]);
                                                                       })
                                                                       ->withCount('sports')->get();

                            foreach ($clubs_sports_24_fed as $club){
                                if ($club->sports_count == 1){
                                    $i_clubsport_act24_1_fed++;
                                }
                                if ($club->sports_count > 1){
                                    $i_clubsport_act24_n_fed++;
                                }
                            }

                            $statistics['clubs_sports_24_1_fed'] = $i_clubsport_act24_1_fed;
                            $statistics['clubs_sports_24_n_fed'] = $i_clubsport_act24_n_fed;

                            break;


                        case "7Tage":
                            /** Club activity 7d fed*/
                            $clubs_7_all_fed = new Club;
                            $clubs_7_accepted_fed = new Club;
                            $clubs_sports_7_fed = new Club;
                            $i_clubsport_act7_1_fed = 0;
                            $i_clubsport_act7_n_fed = 0;

                            /** Club activity 7d */
                            $clubs_7_all_fed = $clubs_7_all_fed
                                ->where('federal_state', 'LIKE', $fed_state)
                                ->whereBetween('updated_at', [$past_7, $today])
                                ->orWhere(function($query) use ($past_7, $today){
                                    $query->whereHas('posts', function($query2) use ($past_7, $today){
                                        $query2->whereBetween('post.created_at', [$past_7, $today]);
                                    });
                                    //$query->has('posts')->whereBetween('created_at', [$past_7, $today]);
                                })
                                ->get()->count();
                            $statistics['clubs_7_all_fed'] = $clubs_7_all_fed;

                            $clubs_7_accepted_fed = $clubs_7_accepted_fed
                                ->where('federal_state', 'LIKE', $fed_state)
                                ->whereHas('users')
                                ->whereBetween('updated_at', [$past_7, $today])
                                ->orWhere(function($query) use ($past_7, $today){
                                    $query->whereHas('posts', function($query2) use ($past_7, $today){
                                        $query2->whereBetween('post.created_at', [$past_7, $today]);
                                    });
                                    //$query->has('posts')->whereBetween('created_at', [$past_7, $today]);
                                })
                                ->get()->count();
                            $statistics['clubs_7_accepted_fed'] = $clubs_7_accepted_fed;


                            $clubs_sports_7_fed = $clubs_sports_7_fed->where('federal_state', 'LIKE', $fed_state)
                                                                     ->whereBetween('updated_at', [$past_7, $today])
                                                                     ->orWhere(function($query) use ($past_7, $today){
                                                                         $query->whereHas('posts', function($query2) use ($past_7, $today){
                                                                             $query2->whereBetween('post.created_at', [$past_7, $today]);
                                                                         });
                                                                         //$query->has('posts')->whereBetween('created_at', [$past_7, $today]);
                                                                     })
                                                                     ->withCount('sports')->get();

                            foreach ($clubs_sports_7_fed as $club){
                                if ($club->sports_count == 1){
                                    $i_clubsport_act7_1_fed++;
                                }
                                if ($club->sports_count > 1){
                                    $i_clubsport_act7_n_fed++;
                                }
                            }

                            $statistics['clubs_sports_7_1_fed'] = $i_clubsport_act7_1_fed;
                            $statistics['clubs_sports_7_n_fed'] = $i_clubsport_act7_n_fed;

                            break;


                        case "30Tage":
                            /** club activity 30d fed*/
                            $clubs_30_all_fed = new Club;
                            $clubs_30_accepted_fed = new Club;
                            $clubs_sports_30_fed = new Club;
                            $i_clubsport_act30_1_fed = 0;
                            $i_clubsport_act30_n_fed = 0;

                            /** Club activity 30d */
                            $clubs_30_all_fed = $clubs_30_all_fed
                                ->where('federal_state', 'LIKE', $fed_state)
                                ->whereBetween('updated_at', [$past_30, $today])
                                ->orWhere(function($query) use ($past_30, $today){
                                    $query->whereHas('posts', function($query2) use ($past_30, $today){
                                        $query2->whereBetween('post.created_at', [$past_30, $today]);
                                    });
                                    //$query->has('posts')->whereBetween('created_at', [$past_30, $today]);
                                })
                                ->get()->count();
                            $statistics['clubs_30_all_fed'] = $clubs_30_all_fed;

                            $clubs_30_accepted_fed = $clubs_30_accepted_fed
                                ->where('federal_state', 'LIKE', $fed_state)
                                ->whereHas('users')
                                ->whereBetween('updated_at', [$past_30, $today])
                                ->orWhere(function($query) use ($past_30, $today){
                                    $query->whereHas('posts', function($query2) use ($past_30, $today){
                                        $query2->whereBetween('post.created_at', [$past_30, $today]);
                                    });
                                    //$query->has('posts')->whereBetween('created_at', [$past_30, $today]);
                                })
                                ->get()->count();
                            $statistics['clubs_30_accepted_fed'] = $clubs_30_accepted_fed;

                            $clubs_sports_30_fed = $clubs_sports_30_fed->where('federal_state', 'LIKE', $fed_state)
                                                                       ->whereBetween('updated_at', [$past_30, $today])
                                                                       ->orWhere(function($query) use ($past_30, $today){
                                                                           $query->whereHas('posts', function($query2) use ($past_30, $today){
                                                                               $query2->whereBetween('post.created_at', [$past_30, $today]);
                                                                           });
                                                                           //$query->has('posts')->whereBetween('created_at', [$past_30, $today]);
                                                                       })
                                                                       ->withCount('sports')->get();

                            foreach ($clubs_sports_30_fed as $club){
                                if ($club->sports_count == 1){
                                    $i_clubsport_act30_1_fed++;
                                }
                                if ($club->sports_count > 1){
                                    $i_clubsport_act30_n_fed++;
                                }
                            }

                            $statistics['clubs_sports_30_1_fed'] = $i_clubsport_act30_1_fed;
                            $statistics['clubs_sports_30_n_fed'] = $i_clubsport_act30_n_fed;

                            break;


                        case "90Tage":
                            /** club activity 90d fed*/
                            $clubs_90_all_fed = new Club;
                            $clubs_90_accepted_fed = new Club;
                            $clubs_sports_90_fed = new Club;
                            $i_clubsport_act90_1_fed = 0;
                            $i_clubsport_act90_n_fed = 0;

                            /** Club activity 90d */
                            $clubs_90_all_fed = $clubs_90_all_fed
                                ->where('federal_state', 'LIKE', $fed_state)
                                ->whereBetween('updated_at', [$past_90, $today])
                                ->orWhere(function($query) use ($past_90, $today){
                                    $query->whereHas('posts', function($query2) use ($past_90, $today){
                                        $query2->whereBetween('post.created_at', [$past_90, $today]);
                                    });
                                    //$query->has('posts')->whereBetween('created_at', [$past_90, $today]);
                                })
                                ->get()->count();
                            $statistics['clubs_90_all_fed'] = $clubs_90_all_fed;

                            $clubs_90_accepted_fed = $clubs_90_accepted_fed
                                ->where('federal_state', 'LIKE', $fed_state)
                                ->whereHas('users')
                                ->whereBetween('updated_at', [$past_90, $today])
                                ->orWhere(function($query) use ($past_90, $today){
                                    $query->whereHas('posts', function($query2) use ($past_90, $today){
                                        $query2->whereBetween('post.created_at', [$past_90, $today]);
                                    });
                                    //$query->has('posts')->whereBetween('created_at', [$past_90, $today]);
                                })
                                ->get()->count();
                            $statistics['clubs_90_accepted_fed'] = $clubs_90_accepted_fed;

                            $clubs_sports_90_fed = $clubs_sports_90_fed
                                ->where('federal_state', 'LIKE', $fed_state)
                                ->whereBetween('updated_at', [$past_90, $today])
                                ->orWhere(function($query) use ($past_90, $today){
                                    $query->whereHas('posts', function($query2) use ($past_90, $today){
                                        $query2->whereBetween('post.created_at', [$past_90, $today]);
                                    });
                                })
                                ->withCount('sports')->get();

                            foreach ($clubs_sports_90_fed as $club){
                                if ($club->sports_count == 1){
                                    $i_clubsport_act90_1_fed++;
                                }
                                if ($club->sports_count > 1){
                                    $i_clubsport_act90_n_fed++;
                                }
                            }

                            $statistics['clubs_sports_90_1_fed'] = $i_clubsport_act90_1_fed;
                            $statistics['clubs_sports_90_n_fed'] = $i_clubsport_act90_n_fed;

                            break;

                    }


                    break;

                case 2:

                    switch ($subabschnitt_auswahl){

                        case "Logo":
                            /** club intensity logo fed*/
                            $clubs_logo_all_fed = new Club;
                            $clubs_logo_accepted_fed = new Club;
                            $clubs_sports_logo_fed = new Club;
                            $i_clubsport_logo_1_fed = 0;
                            $i_clubsport_logo_n_fed = 0;


                            /** Club intensity logo */
                            $clubs_logo_all_fed = $clubs_logo_all_fed
                                ->where('federal_state', 'LIKE', $fed_state)
                                ->whereNotNull('avatar_updated_at')->where('avatar_updated_at', 'NOT LIKE', '')
                                ->get()->count();
                            $statistics['clubs_logo_all_fed'] = $clubs_logo_all_fed;

                            $clubs_logo_accepted_fed = $clubs_logo_accepted_fed
                                ->where('federal_state', 'LIKE', $fed_state)
                                ->whereHas('users')
                                ->whereNotNull('avatar_updated_at')->where('avatar_updated_at', 'NOT LIKE', '')
                                ->get()->count();
                            $statistics['clubs_logo_accepted_fed'] = $clubs_logo_accepted_fed;

                            $clubs_sports_logo_fed = $clubs_sports_logo_fed
                                ->where('federal_state', 'LIKE', $fed_state)
                                ->whereNotNull('avatar_updated_at')->where('avatar_updated_at', 'NOT LIKE', '')
                                ->withCount('sports')->get();

                            foreach ($clubs_sports_logo_fed as $club){
                                if ($club->sports_count == 1){
                                    $i_clubsport_logo_1_fed++;
                                }
                                if ($club->sports_count > 1){
                                    $i_clubsport_logo_n_fed++;
                                }
                            }

                            $statistics['clubsport_logo_1_fed'] = $i_clubsport_logo_1_fed;
                            $statistics['clubsport_logo_n_fed'] = $i_clubsport_logo_n_fed;

                            break;


                        case "Header":
                            /** club intensity header fed*/
                            $clubs_header_all_fed = new Club;
                            $clubs_header_accepted_fed = new Club;
                            $clubs_sports_header_fed = new Club;
                            $i_clubsport_header_1_fed = 0;
                            $i_clubsport_header_n_fed = 0;

                            /** Club intensity header */
                            $clubs_header_all_fed = $clubs_header_all_fed
                                ->where('federal_state', 'LIKE', $fed_state)
                                ->whereNotNull('header_updated_at')->where('header_updated_at', 'NOT LIKE', '')
                                ->get()->count();
                            $statistics['clubs_header_all_fed'] = $clubs_header_all_fed;

                            $clubs_header_accepted_fed = $clubs_header_accepted_fed
                                ->where('federal_state', 'LIKE', $fed_state)
                                ->whereHas('users')
                                ->whereNotNull('header_updated_at')->where('header_updated_at', 'NOT LIKE', '')
                                ->get()->count();
                            $statistics['clubs_header_accepted_fed'] = $clubs_header_accepted_fed;

                            $clubs_sports_header_fed = $clubs_sports_header_fed
                                ->where('federal_state', 'LIKE', $fed_state)
                                ->whereNotNull('header_updated_at')->where('header_updated_at', 'NOT LIKE', '')
                                ->withCount('sports')->get();

                            foreach ($clubs_sports_header_fed as $club){
                                if ($club->sports_count == 1){
                                    $i_clubsport_header_1_fed++;
                                }
                                if ($club->sports_count > 1){
                                    $i_clubsport_header_n_fed++;
                                }
                            }

                            $statistics['clubsport_header_1_fed'] = $i_clubsport_header_1_fed;
                            $statistics['clubsport_header_n_fed'] = $i_clubsport_header_n_fed;

                            break;


                        case "Infos":
                            /** club intensity infos fed*/
                            $clubs_infos_all_fed = new Club;
                            $clubs_info_accepted_fed = new Club;
                            $clubs_sports_info_fed = new Club;
                            $i_clubsport_infos_1_fed = 0;
                            $i_clubsport_infos_n_fed = 0;

                            /** Club intensity info */
                            $clubs_infos_all_fed = $clubs_infos_all_fed
                                ->where('federal_state', 'LIKE', $fed_state)
                                ->whereNotNull('description')->where('description', 'NOT LIKE', '')
                                ->get()->count();
                            $statistics['clubs_infos_all_fed'] = $clubs_infos_all_fed;

                            $clubs_info_accepted_fed = $clubs_info_accepted_fed
                                ->where('federal_state', 'LIKE', $fed_state)
                                ->whereHas('users')
                                ->whereNotNull('description')->where('description', 'NOT LIKE', '')
                                ->get()->count();
                            $statistics['clubs_infos_accepted_fed'] = $clubs_info_accepted_fed;

                            $clubs_sports_info_fed = $clubs_sports_info_fed
                                ->where('federal_state', 'LIKE', $fed_state)
                                ->whereNotNull('description')->where('description', 'NOT LIKE', '')
                                ->withCount('sports')->get();


                            foreach ($clubs_sports_info_fed as $club){
                                if ($club->sports_count == 1){
                                    $i_clubsport_infos_1_fed++;
                                }
                                if ($club->sports_count > 1){
                                    $i_clubsport_infos_n_fed++;
                                }
                            }

                            $statistics['clubsport_infos_1_fed'] = $i_clubsport_infos_1_fed;
                            $statistics['clubsport_infos_n_fed'] = $i_clubsport_infos_n_fed;

                            break;


                        case "Teams":
                            /** club intensity teams fed*/
                            $clubs_teams_all_fed = new Club;
                            $clubs_teams_accepted_fed = new Club;
                            $clubs_sports_teams_fed = new Club;
                            $i_clubsport_teams_1_fed = 0;
                            $i_clubsport_teams_n_fed = 0;

                            /** Club intensity teams */
                            $clubs_teams_all_fed = $clubs_teams_all_fed
                                ->where('federal_state', 'LIKE', $fed_state)
                                ->has('teams')
                                ->get()->count();
                            $statistics['clubs_teams_all_fed'] = $clubs_teams_all_fed;

                            $clubs_teams_accepted_fed = $clubs_teams_accepted_fed
                                ->where('federal_state', 'LIKE', $fed_state)
                                ->whereHas('users')
                                ->has('teams')
                                ->get()->count();
                            $statistics['clubs_teams_accepted_fed'] = $clubs_teams_accepted_fed;

                            $clubs_sports_teams_fed = $clubs_sports_teams_fed
                                ->where('federal_state', 'LIKE', $fed_state)
                                ->has('teams')
                                ->withCount('sports')->get();

                            foreach ($clubs_sports_teams_fed as $club){
                                if ($club->sports_count == 1){
                                    $i_clubsport_teams_1_fed++;
                                }
                                if ($club->sports_count > 1){
                                    $i_clubsport_teams_n_fed++;
                                }
                            }

                            $statistics['clubsport_teams_1_fed'] = $i_clubsport_teams_1_fed;
                            $statistics['clubsport_teams_n_fed'] = $i_clubsport_teams_n_fed;

                            break;

                        case "Weblinks":
                            /** club intensity Weblinks fed*/
                            $clubs_web_all_fed = new Club;
                            $clubs_web_accepted_fed = new Club;
                            $clubs_sports_web_fed = new Club;
                            $i_clubsport_web_1_fed = 0;
                            $i_clubsport_web_n_fed = 0;

                            /** Club intensity weblinks */
                            $clubs_web_all_fed = $clubs_web_all_fed
                                ->where('federal_state', 'LIKE', $fed_state)
                                ->WhereNotNull('m_social_facebook')
                                ->OrWhereNotNull('m_social_twitter')
                                ->OrWhereNotNull('m_social_googleplus')
                                ->OrWhereNotNull('m_social_youtube')
                                ->OrWhereNotNull('m_social_instagram')
                                ->OrWhereNotNull('m_social_flickr')
                                ->get()->count();
                            $statistics['clubs_web_all_fed'] = $clubs_web_all_fed;

                            $clubs_web_accepted_fed = $clubs_web_accepted_fed
                                ->where('federal_state', 'LIKE', $fed_state)
                                ->whereHas('users')
                                ->WhereNotNull('m_social_facebook')
                                ->OrWhereNotNull('m_social_twitter')
                                ->OrWhereNotNull('m_social_googleplus')
                                ->OrWhereNotNull('m_social_youtube')
                                ->OrWhereNotNull('m_social_instagram')
                                ->OrWhereNotNull('m_social_flickr')
                                ->has('teams')
                                ->get()->count();
                            $statistics['clubs_web_accepted_fed'] = $clubs_web_accepted_fed;

                            $clubs_sports_web_fed = $clubs_sports_web_fed
                                ->where('federal_state', 'LIKE', $fed_state)
                                ->WhereNotNull('m_social_facebook')
                                ->OrWhereNotNull('m_social_twitter')
                                ->OrWhereNotNull('m_social_googleplus')
                                ->OrWhereNotNull('m_social_youtube')
                                ->OrWhereNotNull('m_social_instagram')
                                ->OrWhereNotNull('m_social_flickr')
                                ->has('teams')
                                ->withCount('sports')->get();

                            foreach ($clubs_sports_web_fed as $club){
                                if ($club->sports_count == 1){
                                    $i_clubsport_web_1_fed++;
                                }
                                if ($club->sports_count > 1){
                                    $i_clubsport_web_n_fed++;
                                }
                            }

                            $statistics['clubsport_web_1_fed'] = $i_clubsport_web_1_fed;
                            $statistics['clubsport_web_n_fed'] = $i_clubsport_web_n_fed;

                            break;


                        case "Erfolge":
                            /** club intensity success fed*/
                            $clubs_success_all_fed = new Club;
                            $clubs_success_accepted_fed = new Club;
                            $clubs_sports_success_fed = new Club;
                            $i_clubsport_success_1_fed = 0;
                            $i_clubsport_success_n_fed = 0;

                            /** Club intensity success */
                            $clubs_success_all_fed = $clubs_success_all_fed
                                ->where('federal_state', 'LIKE', $fed_state)
                                ->has('achievements')
                                ->get()->count();
                            $statistics['clubs_success_all_fed'] = $clubs_success_all_fed;

                            $clubs_success_accepted_fed = $clubs_success_accepted_fed
                                ->where('federal_state', 'LIKE', $fed_state)
                                ->whereHas('users')
                                ->has('achievements')
                                ->get()->count();
                            $statistics['clubs_success_accepted_fed'] = $clubs_success_accepted_fed;

                            $clubs_sports_success_fed = $clubs_sports_success_fed
                                ->where('federal_state', 'LIKE', $fed_state)
                                ->has('achievements')
                                ->withCount('sports')->get();

                            foreach ($clubs_sports_success_fed as $club){
                                if ($club->sports_count == 1){
                                    $i_clubsport_success_1_fed++;
                                }
                                if ($club->sports_count > 1){
                                    $i_clubsport_success_n_fed++;
                                }
                            }

                            $statistics['clubsport_success_1_fed'] = $i_clubsport_success_1_fed;
                            $statistics['clubsport_success_n_fed'] = $i_clubsport_success_n_fed;

                            break;


                        case "Posts":
                            /** club intensity posts fed*/
                            $clubs_posts_all_fed = new Club;
                            $clubs_posts_accepted_fed = new Club;
                            $clubs_sports_posts_fed = new Club;
                            $i_clubsport_posts_1_fed = 0;
                            $i_clubsport_posts_n_fed = 0;

                            /** Club intensity posts */
                            $clubs_posts_all_fed = $clubs_posts_all_fed
                                ->where('federal_state', 'LIKE', $fed_state)
                                ->has('posts')
                                ->get()->count();
                            $statistics['clubs_posts_all_fed'] = $clubs_posts_all_fed;

                            $clubs_posts_accepted_fed = $clubs_posts_accepted_fed
                                ->where('federal_state', 'LIKE', $fed_state)
                                ->whereHas('users')
                                ->has('posts')
                                ->get()->count();
                            $statistics['clubs_posts_accepted_fed'] = $clubs_posts_accepted_fed;

                            $clubs_sports_posts_fed = $clubs_sports_posts_fed
                                ->where('federal_state', 'LIKE', $fed_state)
                                ->has('posts')
                                ->withCount('sports')->get();

                            foreach ($clubs_sports_posts_fed as $club){
                                if ($club->sports_count == 1){
                                    $i_clubsport_posts_1_fed++;
                                }
                                if ($club->sports_count > 1){
                                    $i_clubsport_posts_n_fed++;
                                }
                            }

                            $statistics['clubsport_posts_1_fed'] = $i_clubsport_posts_1_fed;
                            $statistics['clubsport_posts_n_fed'] = $i_clubsport_posts_n_fed;

                            break;

                    }

                    break;

            }


        }


        if ($sporttype !== ''){

            $statistics['sport'] = 1;

            switch ($abschnitt_auswahl){

                case 0:
                    /** CLUBS Sport*/
                    $clubs_all_sp = new Club;
                    $clubs_accepted_sp = new Club;
                    $clubs_sports_sp = new Club;

                    $i_clubsport_1_sp = 0;
                    $i_clubsport_n_sp = 0;

                    /** CLUBS */
                    $clubs_all_sp = $clubs_all_sp
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })
                        ->count();
                    $statistics['clubs_all_sp'] = $clubs_all_sp;

                    $clubs_accepted_sp = $clubs_accepted_sp
                        ->whereHas('users')
                        ->whereHas('sports', function($query) use ($sportids){
                            $query->whereIn('sport_id', $sportids);
                        })
                        ->get()->count();
                    $statistics['clubs_accepted_sp'] = $clubs_accepted_sp;


//                    $clubs_sports_sp = $clubs_sports_sp
//                        ->withCount('sports')->get();
//
//                    foreach ($clubs_sports_sp as $club){
//                        if ($club->sports_count == 1){
//                            $i_clubsport_1_sp++;
//                        }
//                        if ($club->sports_count > 1){
//                            $i_clubsport_n_sp++;
//                        }
//                    }

                    $statistics['clubs_1_branch_sp'] = $i_clubsport_1_sp;
                    $statistics['clubs_n_branch_sp'] = $i_clubsport_n_sp;

                    break;


                case 1:

                    switch ($subabschnitt_auswahl){

                        case "24h":
                            /** Club activity 24h sp*/
                            $clubs_24_all_sp = new Club;
                            $clubs_24_accepted_sp = new Club;
                            $clubs_sports_24_sp = new Club;
                            $i_clubsport_act24_1_sp = 0;
                            $i_clubsport_act24_n_sp = 0;

                            /** Club activity 24h */
                            $clubs_24_all_sp = $clubs_24_all_sp
                                ->whereHas('sports', function($query) use ($sportids){
                                    $query->whereIn('sport_id', $sportids);
                                })
                                ->whereBetween('updated_at', [$past_1, $today])
                                ->orWhere(function($query) use ($past_1, $today){
                                    $query->whereHas('posts', function($query2) use ($past_1, $today){
                                        $query2->whereBetween('post.created_at', [$past_1, $today]);
                                    });
                                    //$query->has('posts')->whereBetween('created_at', [$past_1, $today]);
                                })
                                ->get()->count();
                            $statistics['clubs_24_all_sp'] = $clubs_24_all_sp;

                            $clubs_24_accepted_sp = $clubs_24_accepted_sp
                                ->whereHas('users')
                                ->whereHas('sports', function($query) use ($sportids){
                                    $query->whereIn('sport_id', $sportids);
                                })
                                ->whereBetween('updated_at', [$past_1, $today])
                                ->orWhere(function($query) use ($past_1, $today){
                                    $query->whereHas('posts', function($query2) use ($past_1, $today){
                                        $query2->whereBetween('post.created_at', [$past_1, $today]);
                                    });
                                    //$query->has('posts')->whereBetween('created_at', [$past_1, $today]);
                                })
                                ->get()->count();
                            $statistics['clubs_24_accepted_sp'] = $clubs_24_accepted_sp;

                            $clubs_sports_24_sp = $clubs_sports_24_sp
                                ->whereBetween('updated_at', [$past_1, $today])
                                ->orWhere(function($query) use ($past_1, $today){
                                    $query->whereHas('posts', function($query2) use ($past_1, $today){
                                        $query2->whereBetween('post.created_at', [$past_1, $today]);
                                    });
                                    //$query->has('posts')->whereBetween('created_at', [$past_1, $today]);
                                })
                                ->withCount(['sports' => function($query) use ($sportids){
                                    $query->whereIn('sport_id', $sportids);
                                }])->get();

                            foreach ($clubs_sports_24_sp as $club){
                                if ($club->sports_count == 1){
                                    $i_clubsport_act24_1_sp++;
                                }
                                if ($club->sports_count > 1){
                                    $i_clubsport_act24_n_sp++;
                                }
                            }

                            $statistics['clubs_sports_24_1_sp'] = $i_clubsport_act24_1_sp;
                            $statistics['clubs_sports_24_n_sp'] = $i_clubsport_act24_n_sp;

                            break;


                        case "7Tage":
                            /** Club activity 7d sp*/
                            $clubs_7_all_sp = new Club;
                            $clubs_7_accepted_sp = new Club;
                            $clubs_sports_7_sp = new Club;
                            $i_clubsport_act7_1_sp = 0;
                            $i_clubsport_act7_n_sp = 0;

                            /** Club activity 7d */
                            $clubs_7_all_sp = $clubs_7_all_sp
                                ->whereHas('sports', function($query) use ($sportids){
                                    $query->whereIn('sport_id', $sportids);
                                })
                                ->whereBetween('updated_at', [$past_7, $today])
                                ->orWhere(function($query) use ($past_7, $today){
                                    $query->whereHas('posts', function($query2) use ($past_7, $today){
                                        $query2->whereBetween('post.created_at', [$past_7, $today]);
                                    });
                                    //$query->has('posts')->whereBetween('created_at', [$past_7, $today]);
                                })
                                ->get()->count();
                            $statistics['clubs_7_all_sp'] = $clubs_7_all_sp;

                            $clubs_7_accepted_sp = $clubs_7_accepted_sp
                                ->whereHas('users')
                                ->whereHas('sports', function($query) use ($sportids){
                                    $query->whereIn('sport_id', $sportids);
                                })
                                ->whereBetween('updated_at', [$past_7, $today])
                                ->orWhere(function($query) use ($past_7, $today){
                                    $query->whereHas('posts', function($query2) use ($past_7, $today){
                                        $query2->whereBetween('post.created_at', [$past_7, $today]);
                                    });
                                    //$query->has('posts')->whereBetween('created_at', [$past_7, $today]);
                                })
                                ->get()->count();
                            $statistics['clubs_7_accepted_sp'] = $clubs_7_accepted_sp;

                            $clubs_sports_7_sp = $clubs_sports_7_sp
                                ->whereBetween('updated_at', [$past_7, $today])
                                ->orWhere(function($query) use ($past_7, $today){
                                    $query->whereHas('posts', function($query2) use ($past_7, $today){
                                        $query2->whereBetween('post.created_at', [$past_7, $today]);
                                    });
                                    //$query->has('posts')->whereBetween('created_at', [$past_7, $today]);
                                })
                                ->withCount(['sports' => function($query) use ($sportids){
                                    $query->whereIn('sport_id', $sportids);
                                }])->get();

                            foreach ($clubs_sports_7_sp as $club){
                                if ($club->sports_count == 1){
                                    $i_clubsport_act7_1_sp++;
                                }
                                if ($club->sports_count > 1){
                                    $i_clubsport_act7_n_sp++;
                                }
                            }

                            $statistics['clubs_sports_7_1_sp'] = $i_clubsport_act7_1_sp;
                            $statistics['clubs_sports_7_n_sp'] = $i_clubsport_act7_n_sp;

                            break;


                        case "30Tage":
                            /** club activity 30d sport*/
                            $clubs_30_all_sp = new Club;
                            $clubs_30_accepted_sp = new Club;
                            $clubs_sports_30_sp = new Club;
                            $i_clubsport_act30_1_sp = 0;
                            $i_clubsport_act30_n_sp = 0;

                            /** Club activity 30d */
                            $clubs_30_all_sp = $clubs_30_all_sp
                                ->whereHas('sports', function($query) use ($sportids){
                                    $query->whereIn('sport_id', $sportids);
                                })
                                ->whereBetween('updated_at', [$past_30, $today])
                                ->orWhere(function($query) use ($past_30, $today){
                                    $query->whereHas('posts', function($query2) use ($past_30, $today){
                                        $query2->whereBetween('post.created_at', [$past_30, $today]);
                                    });
                                    //$query->has('posts')->whereBetween('created_at', [$past_30, $today]);
                                })
                                ->get()->count();
                            $statistics['clubs_30_all_sp'] = $clubs_30_all_sp;

                            $clubs_30_accepted_sp = $clubs_30_accepted_sp
                                ->whereHas('users')
                                ->whereHas('sports', function($query) use ($sportids){
                                    $query->whereIn('sport_id', $sportids);
                                })
                                ->whereBetween('updated_at', [$past_30, $today])
                                ->orWhere(function($query) use ($past_30, $today){
                                    $query->whereHas('posts', function($query2) use ($past_30, $today){
                                        $query2->whereBetween('post.created_at', [$past_30, $today]);
                                    });
                                    //$query->has('posts')->whereBetween('created_at', [$past_30, $today]);
                                })
                                ->get()->count();
                            $statistics['clubs_30_accepted_sp'] = $clubs_30_accepted_sp;

                            $clubs_sports_30_sp = $clubs_sports_30_sp
                                ->whereBetween('updated_at', [$past_30, $today])
                                ->orWhere(function($query) use ($past_30, $today){
                                    $query->whereHas('posts', function($query2) use ($past_30, $today){
                                        $query2->whereBetween('post.created_at', [$past_30, $today]);
                                    });
                                    //$query->has('posts')->whereBetween('created_at', [$past_30, $today]);
                                })
                                ->withCount(['sports' => function($query) use ($sportids){
                                    $query->whereIn('sport_id', $sportids);
                                }])
                                ->get();

                            foreach ($clubs_sports_30_sp as $club){
                                if ($club->sports_count == 1){
                                    $i_clubsport_act30_1_sp++;
                                }
                                if ($club->sports_count > 1){
                                    $i_clubsport_act30_n_sp++;
                                }
                            }

                            $statistics['clubs_sports_30_1_sp'] = $i_clubsport_act30_1_sp;
                            $statistics['clubs_sports_30_n_sp'] = $i_clubsport_act30_n_sp;

                            break;


                        case "90Tage":
                            /** club activity 90d sport*/
                            $clubs_90_all_sp = new Club;
                            $clubs_90_accepted_sp = new Club;
                            $clubs_sports_90_sp = new Club;
                            $i_clubsport_act90_1_sp = 0;
                            $i_clubsport_act90_n_sp = 0;

                            /** Club activity 90d */
                            $clubs_90_all_sp = $clubs_90_all_sp
                                ->whereHas('sports', function($query) use ($sportids){
                                    $query->whereIn('sport_id', $sportids);
                                })
                                ->whereBetween('updated_at', [$past_90, $today])
                                ->orWhere(function($query) use ($past_90, $today){
                                    $query->whereHas('posts', function($query2) use ($past_90, $today){
                                        $query2->whereBetween('post.created_at', [$past_90, $today]);
                                    });
                                    //$query->has('posts')->whereBetween('created_at', [$past_90, $today]);
                                })
                                ->get()->count();
                            $statistics['clubs_90_all_sp'] = $clubs_90_all_sp;

                            $clubs_90_accepted_sp = $clubs_90_accepted_sp
                                ->whereHas('sports', function($query) use ($sportids){
                                    $query->whereIn('sport_id', $sportids);
                                })
                                ->whereHas('users')
                                ->whereBetween('updated_at', [$past_90, $today])
                                ->orWhere(function($query) use ($past_90, $today){
                                    $query->whereHas('posts', function($query2) use ($past_90, $today){
                                        $query2->whereBetween('post.created_at', [$past_90, $today]);
                                    });
                                    //$query->has('posts')->whereBetween('created_at', [$past_90, $today]);
                                })
                                ->get()->count();
                            $statistics['clubs_90_accepted_sp'] = $clubs_90_accepted_sp;

                            $clubs_sports_90_sp = $clubs_sports_90_sp
                                ->whereBetween('updated_at', [$past_90, $today])
                                ->orWhere(function($query) use ($past_90, $today){
                                    $query->whereHas('posts', function($query2) use ($past_90, $today){
                                        $query2->whereBetween('post.created_at', [$past_90, $today]);
                                    });
                                    //$query->has('posts')->whereBetween('created_at', [$past_90, $today]);
                                })
                                ->withCount(['sports' => function($query) use ($sportids){
                                    $query->whereIn('sport_id', $sportids);
                                }])
                                ->get();

                            foreach ($clubs_sports_90_sp as $club){
                                if ($club->sports_count == 1){
                                    $i_clubsport_act90_1_sp++;
                                }
                                if ($club->sports_count > 1){
                                    $i_clubsport_act90_n_sp++;
                                }
                            }

                            $statistics['clubs_sports_90_1_sp'] = $i_clubsport_act90_1_sp;
                            $statistics['clubs_sports_90_n_sp'] = $i_clubsport_act90_n_sp;

                            break;

                    }


                    break;


                case 2:

                    switch ($subabschnitt_auswahl){

                        case "Logo":
                            /** club intensity logo sport*/
                            $clubs_logo_all_sp = new Club;
                            $clubs_logo_accepted_sp = new Club;
                            $clubs_sports_logo_sp = new Club;
                            $i_clubsport_logo_1_sp = 0;
                            $i_clubsport_logo_n_sp = 0;


                            /** Club intensity logo */
                            $clubs_logo_all_sp = $clubs_logo_all_sp
                                ->whereNotNull('avatar_updated_at')->where('avatar_updated_at', 'NOT LIKE', '')
                                ->whereHas('sports', function($query) use ($sportids){
                                    $query->whereIn('sport_id', $sportids);
                                })
                                ->get()->count();
                            $statistics['clubs_logo_all_sp'] = $clubs_logo_all_sp;

                            $clubs_logo_accepted_sp = $clubs_logo_accepted_sp
                                ->whereHas('users')
                                ->whereNotNull('avatar_updated_at')->where('avatar_updated_at', 'NOT LIKE', '')
                                ->whereHas('sports', function($query) use ($sportids){
                                    $query->whereIn('sport_id', $sportids);
                                })
                                ->get()->count();
                            $statistics['clubs_logo_accepted_sp'] = $clubs_logo_accepted_sp;

                            $clubs_sports_logo_sp = $clubs_sports_logo_sp
                                ->whereNotNull('avatar_updated_at')->where('avatar_updated_at', 'NOT LIKE', '')
                                ->withCount(['sports' => function($query) use ($sportids){
                                    $query->whereIn('sport_id', $sportids);
                                }])->get();

                            foreach ($clubs_sports_logo_sp as $club){
                                if ($club->sports_count == 1){
                                    $i_clubsport_logo_1_sp++;
                                }
                                if ($club->sports_count > 1){
                                    $i_clubsport_logo_n_sp++;
                                }
                            }

                            $statistics['clubsport_logo_1_sp'] = $i_clubsport_logo_1_sp;
                            $statistics['clubsport_logo_n_sp'] = $i_clubsport_logo_n_sp;

                            break;


                        case "Header":
                            /** club intensity header sport*/
                            $clubs_header_all_sp = new Club;
                            $clubs_header_accepted_sp = new Club;
                            $clubs_sports_header_sp = new Club;
                            $i_clubsport_header_1_sp = 0;
                            $i_clubsport_header_n_sp = 0;

                            /** Club intensity header */
                            $clubs_header_all_sp = $clubs_header_all_sp
                                ->whereNotNull('header_updated_at')->where('header_updated_at', 'NOT LIKE', '')
                                ->get()->count();
                            $statistics['clubs_header_all_sp'] = $clubs_header_all_sp;

                            $clubs_header_accepted_sp = $clubs_header_accepted_sp
                                ->whereHas('users')
                                ->whereNotNull('header_updated_at')->where('header_updated_at', 'NOT LIKE', '')
                                ->whereHas('sports', function($query) use ($sportids){
                                    $query->whereIn('sport_id', $sportids);
                                })
                                ->get()->count();
                            $statistics['clubs_header_accepted_sp'] = $clubs_header_accepted_sp;

                            $clubs_sports_header_sp = $clubs_sports_header_sp
                                ->whereNotNull('header_updated_at')->where('header_updated_at', 'NOT LIKE', '')
                                ->withCount(['sports' => function($query) use ($sportids){
                                    $query->whereIn('sport_id', $sportids);
                                }])->get();

                            foreach ($clubs_sports_header_sp as $club){
                                if ($club->sports_count == 1){
                                    $i_clubsport_header_1_sp++;
                                }
                                if ($club->sports_count > 1){
                                    $i_clubsport_header_n_sp++;
                                }
                            }

                            $statistics['clubsport_header_1_sp'] = $i_clubsport_header_1_sp;
                            $statistics['clubsport_header_n_sp'] = $i_clubsport_header_n_sp;

                            break;


                        case "Infos":
                            /** club intensity infos sport*/
                            $clubs_infos_all_sp = new Club;
                            $clubs_info_accepted_sp = new Club;
                            $clubs_sports_info_sp = new Club;
                            $i_clubsport_infos_1_sp = 0;
                            $i_clubsport_infos_n_sp = 0;

                            /** Club intensity info */
                            $clubs_infos_all_sp = $clubs_infos_all_sp
                                ->whereNotNull('description')->where('description', 'NOT LIKE', '')
                                ->whereHas('sports', function($query) use ($sportids){
                                    $query->whereIn('sport_id', $sportids);
                                })
                                ->get()->count();
                            $statistics['clubs_infos_all_sp'] = $clubs_infos_all_sp;

                            $clubs_info_accepted_sp = $clubs_info_accepted_sp
                                ->whereHas('users')
                                ->whereNotNull('description')->where('description', 'NOT LIKE', '')
                                ->whereHas('sports', function($query) use ($sportids){
                                    $query->whereIn('sport_id', $sportids);
                                })
                                ->get()->count();
                            $statistics['clubs_infos_accepted_sp'] = $clubs_info_accepted_sp;

                            $clubs_sports_info_sp = $clubs_sports_info_sp
                                ->whereNotNull('description')->where('description', 'NOT LIKE', '')
                                ->withCount(['sports' => function($query) use ($sportids){
                                    $query->whereIn('sport_id', $sportids);
                                }])->get();

                            foreach ($clubs_sports_info_sp as $club){
                                if ($club->sports_count == 1){
                                    $i_clubsport_infos_1_sp++;
                                }
                                if ($club->sports_count > 1){
                                    $i_clubsport_infos_n_sp++;
                                }
                            }

                            $statistics['clubsport_infos_1_sp'] = $i_clubsport_infos_1_sp;
                            $statistics['clubsport_infos_n_sp'] = $i_clubsport_infos_n_sp;

                            break;


                        case "Teams":
                            /** club intensity teams sport*/
                            $clubs_teams_all_sp = new Club;
                            $clubs_teams_accepted_sp = new Club;
                            $clubs_sports_teams_sp = new Club;
                            $i_clubsport_teams_1_sp = 0;
                            $i_clubsport_teams_n_sp = 0;

                            /** Club intensity teams */
                            $clubs_teams_all_sp = $clubs_teams_all_sp
                                ->has('teams')
                                ->whereHas('sports', function($query) use ($sportids){
                                    $query->whereIn('sport_id', $sportids);
                                })
                                ->get()->count();
                            $statistics['clubs_teams_all_sp'] = $clubs_teams_all_sp;

                            $clubs_teams_accepted_sp = $clubs_teams_accepted_sp
                                ->whereHas('users')
                                ->has('teams')
                                ->whereHas('sports', function($query) use ($sportids){
                                    $query->whereIn('sport_id', $sportids);
                                })
                                ->get()->count();
                            $statistics['clubs_teams_accepted_sp'] = $clubs_teams_accepted_sp;

                            $clubs_sports_teams_sp = $clubs_sports_teams_sp
                                ->has('teams')
                                ->withCount(['sports' => function($query) use ($sportids){
                                    $query->whereIn('sport_id', $sportids);
                                }])->get();

                            foreach ($clubs_sports_teams_sp as $club){
                                if ($club->sports_count == 1){
                                    $i_clubsport_teams_1_sp++;
                                }
                                if ($club->sports_count > 1){
                                    $i_clubsport_teams_n_sp++;
                                }
                            }

                            $statistics['clubsport_teams_1_sp'] = $i_clubsport_teams_1_sp;
                            $statistics['clubsport_teams_n_sp'] = $i_clubsport_teams_n_sp;

                            break;


                        case "Weblinks":
                            /** club intensity Weblinks sport*/
                            $clubs_web_all_sp = new Club;
                            $clubs_web_accepted_sp = new Club;
                            $clubs_sports_web_sp = new Club;
                            $i_clubsport_web_1_sp = 0;
                            $i_clubsport_web_n_sp = 0;

                            /** Club intensity weblinks */
                            $clubs_web_all_sp = $clubs_web_all_sp
                                ->whereHas('sports', function($query) use ($sportids){
                                    $query->whereIn('sport_id', $sportids);
                                })
                                ->WhereNotNull('m_social_facebook')
                                ->OrWhereNotNull('m_social_twitter')
                                ->OrWhereNotNull('m_social_googleplus')
                                ->OrWhereNotNull('m_social_youtube')
                                ->OrWhereNotNull('m_social_instagram')
                                ->OrWhereNotNull('m_social_flickr')
                                ->get()->count();
                            $statistics['clubs_web_all_sp'] = $clubs_web_all_sp;

                            $clubs_web_accepted_sp = $clubs_web_accepted_sp
                                ->whereHas('users')
                                ->whereHas('sports', function($query) use ($sportids){
                                    $query->whereIn('sport_id', $sportids);
                                })
                                ->WhereNotNull('m_social_facebook')
                                ->OrWhereNotNull('m_social_twitter')
                                ->OrWhereNotNull('m_social_googleplus')
                                ->OrWhereNotNull('m_social_youtube')
                                ->OrWhereNotNull('m_social_instagram')
                                ->OrWhereNotNull('m_social_flickr')
                                ->has('teams')
                                ->get()->count();
                            $statistics['clubs_web_accepted_sp'] = $clubs_web_accepted_sp;

                            $clubs_sports_web_sp = $clubs_sports_web_sp
                                ->WhereNotNull('m_social_facebook')
                                ->OrWhereNotNull('m_social_twitter')
                                ->OrWhereNotNull('m_social_googleplus')
                                ->OrWhereNotNull('m_social_youtube')
                                ->OrWhereNotNull('m_social_instagram')
                                ->OrWhereNotNull('m_social_flickr')
                                ->has('teams')
                                ->withCount(['sports' => function($query) use ($sportids){
                                    $query->whereIn('sport_id', $sportids);
                                }])->get();

                            foreach ($clubs_sports_web_sp as $club){
                                if ($club->sports_count == 1){
                                    $i_clubsport_web_1_sp++;
                                }
                                if ($club->sports_count > 1){
                                    $i_clubsport_web_n_sp++;
                                }
                            }

                            $statistics['clubsport_web_1_sp'] = $i_clubsport_web_1_sp;
                            $statistics['clubsport_web_n_sp'] = $i_clubsport_web_n_sp;

                            break;

                        case "Erfolge":
                            /** club intensity success sport*/
                            $clubs_success_all_sp = new Club;
                            $clubs_success_accepted_sp = new Club;
                            $clubs_sports_success_sp = new Club;
                            $i_clubsport_success_1_sp = 0;
                            $i_clubsport_success_n_sp = 0;

                            /** Club intensity success */
                            $clubs_success_all_sp = $clubs_success_all_sp
                                ->has('achievements')
                                ->whereHas('sports', function($query) use ($sportids){
                                    $query->whereIn('sport_id', $sportids);
                                })
                                ->get()->count();
                            $statistics['clubs_success_all_sp'] = $clubs_success_all_sp;

                            $clubs_success_accepted_sp = $clubs_success_accepted_sp
                                ->whereHas('users')
                                ->has('achievements')
                                ->whereHas('sports', function($query) use ($sportids){
                                    $query->whereIn('sport_id', $sportids);
                                })
                                ->get()->count();
                            $statistics['clubs_success_accepted_sp'] = $clubs_success_accepted_sp;

                            $clubs_sports_success_sp = $clubs_sports_success_sp
                                ->has('achievements')
                                ->withCount(['sports' => function($query) use ($sportids){
                                    $query->whereIn('sport_id', $sportids);
                                }])->get();

                            foreach ($clubs_sports_success_sp as $club){
                                if ($club->sports_count == 1){
                                    $i_clubsport_success_1_sp++;
                                }
                                if ($club->sports_count > 1){
                                    $i_clubsport_success_n_sp++;
                                }
                            }

                            $statistics['clubsport_success_1_sp'] = $i_clubsport_success_1_sp;
                            $statistics['clubsport_success_n_sp'] = $i_clubsport_success_n_sp;

                            break;


                        case "Posts":
                            /** club intensity posts sport*/
                            $clubs_posts_all_sp = new Club;
                            $clubs_posts_accepted_sp = new Club;
                            $clubs_sports_posts_sp = new Club;
                            $i_clubsport_posts_1_sp = 0;
                            $i_clubsport_posts_n_sp = 0;

                            /** Club intensity posts */
                            $clubs_posts_all_sp = $clubs_posts_all_sp
                                ->has('posts')
                                ->whereHas('sports', function($query) use ($sportids){
                                    $query->whereIn('sport_id', $sportids);
                                })
                                ->get()->count();
                            $statistics['clubs_posts_all_sp'] = $clubs_posts_all_sp;

                            $clubs_posts_accepted_sp = $clubs_posts_accepted_sp
                                ->whereHas('users')
                                ->has('posts')
                                ->whereHas('sports', function($query) use ($sportids){
                                    $query->whereIn('sport_id', $sportids);
                                })
                                ->get()->count();
                            $statistics['clubs_posts_accepted_sp'] = $clubs_posts_accepted_sp;

                            $clubs_sports_posts_sp = $clubs_sports_posts_sp
                                ->has('posts')
                                ->withCount(['sports' => function($query) use ($sportids){
                                    $query->whereIn('sport_id', $sportids);
                                }])->get();

                            foreach ($clubs_sports_posts_sp as $club){
                                if ($club->sports_count == 1){
                                    $i_clubsport_posts_1_sp++;
                                }
                                if ($club->sports_count > 1){
                                    $i_clubsport_posts_n_sp++;
                                }
                            }

                            $statistics['clubsport_posts_1_sp'] = $i_clubsport_posts_1_sp;
                            $statistics['clubsport_posts_n_sp'] = $i_clubsport_posts_n_sp;

                            break;
                    }


                    break;
            }

        }


        /** Bundesland ausgewählt , Daten nur für das ausgewählte Bundesland holen*/
        if ($fed_state !== '' && $fed_state !== 'all'){

            $statistics['federal'] = 1;

            /** CLUBS */
            $clubs_all = new Club;
            $clubs_accepted = new Club;
            $clubs_sports = new Club;

            $i_clubsport_1 = 0;
            $i_clubsport_n = 0;


            /** Club activity 24h */
            $clubs_24_all = new Club;
            $clubs_24_accepted = new Club;
            $clubs_sports_24 = new Club;
            $i_clubsport_act24_1 = 0;
            $i_clubsport_act24_n = 0;

            /** Club activity 7d */
            $clubs_7_all = new Club;
            $clubs_7_accepted = new Club;
            $clubs_sports_7 = new Club;
            $i_clubsport_act7_1 = 0;
            $i_clubsport_act7_n = 0;

            /** club activity 30d */
            $clubs_30_all = new Club;
            $clubs_30_accepted = new Club;
            $clubs_sports_30 = new Club;
            $i_clubsport_act30_1 = 0;
            $i_clubsport_act30_n = 0;

            /** club activity 90d */
            $clubs_90_all = new Club;
            $clubs_90_accepted = new Club;
            $clubs_sports_90 = new Club;
            $i_clubsport_act90_1 = 0;
            $i_clubsport_act90_n = 0;

            /** club intensity logo */
            $clubs_logo_all = new Club;
            $clubs_logo_accepted = new Club;
            $clubs_sports_logo = new Club;
            $i_clubsport_logo_1 = 0;
            $i_clubsport_logo_n = 0;

            /** club intensity header*/
            $clubs_header_all = new Club;
            $clubs_header_accepted = new Club;
            $clubs_sports_header = new Club;
            $i_clubsport_header_1 = 0;
            $i_clubsport_header_n = 0;

            /** club intensity infos */
            $clubs_infos_all = new Club;
            $clubs_info_accepted = new Club;
            $clubs_sports_info = new Club;
            $i_clubsport_infos_1 = 0;
            $i_clubsport_infos_n = 0;

            /** club intensity teams */
            $clubs_teams_all = new Club;
            $clubs_teams_accepted = new Club;
            $clubs_sports_teams = new Club;
            $i_clubsport_teams_1 = 0;
            $i_clubsport_teams_n = 0;

            /** club intensity Weblinks */
            $clubs_web_all = new Club;
            $clubs_web_accepted = new Club;
            $clubs_sports_web = new Club;
            $i_clubsport_web_1 = 0;
            $i_clubsport_web_n = 0;

            /** club intensity success */
            $clubs_success_all = new Club;
            $clubs_success_accepted = new Club;
            $clubs_sports_success = new Club;
            $i_clubsport_success_1 = 0;
            $i_clubsport_success_n = 0;

            /** club intensity posts */
            $clubs_posts_all = new Club;
            $clubs_posts_accepted = new Club;
            $clubs_sports_posts = new Club;
            $i_clubsport_posts_1 = 0;
            $i_clubsport_posts_n = 0;


            if ($sporttype !== ''){

                $statistics['sport'] = 1;

                switch ($abschnitt_auswahl){

                    case 0:

                        /** CLUBS */
                        $clubs_all = $clubs_all
                            ->where('federal_state', 'LIKE', $fed_state)
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->get()->count();
                        $statistics['clubs_all'] = $clubs_all;

                        $clubs_accepted = $clubs_accepted
                            ->where('federal_state', 'LIKE', $fed_state)
                            ->whereHas('users')
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->get()->count();
                        $statistics['clubs_accepted'] = $clubs_accepted;

//                        $clubs_sports = $clubs_sports
//                            ->where('federal_state', 'LIKE', $fed_state)
//                            ->withCount(['sports' => function($query) use ($sportids){
//                              $query->whereIn('sport_id, $sportids);
//                             }])->get();
//
//                        foreach ($clubs_sports as $club){
//                            if ($club->sports_count == 1){
//                                $i_clubsport_1++;
//                            }
//                            if ($club->sports_count > 1){
//                                $i_clubsport_n++;
//                            }
//                        }

                        $statistics['clubs_1_branch'] = $i_clubsport_1;
                        $statistics['clubs_n_branch'] = $i_clubsport_n;


                    case 1:

                        switch ($subabschnitt_auswahl){

                            case "24h":
                                /** Club activity 24h */
                                $clubs_24_all = $clubs_24_all
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->whereBetween('updated_at', [$past_1, $today])
                                    ->orWhere(function($query) use ($past_1, $today){
                                        $query->whereHas('posts', function($query2) use ($past_1, $today){
                                            $query2->whereBetween('post.created_at', [$past_1, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_1, $today]);
                                    })
                                    ->get()->count();
                                $statistics['clubs_24_all'] = $clubs_24_all;


                                $clubs_24_accepted = $clubs_24_accepted
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereHas('users')
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->whereBetween('updated_at', [$past_1, $today])
                                    ->orWhere(function($query) use ($past_1, $today){
                                        $query->whereHas('posts', function($query2) use ($past_1, $today){
                                            $query2->whereBetween('post.created_at', [$past_1, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_1, $today]);
                                    })
                                    ->get()->count();
                                $statistics['clubs_24_accepted'] = $clubs_24_accepted;


                                $clubs_sports_24 = $clubs_sports_24
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereBetween('updated_at', [$past_1, $today])
                                    ->orWhere(function($query) use ($past_1, $today){
                                        $query->has('posts')->whereBetween('created_at', [$past_1, $today]);
                                    })
                                    ->withCount(['sports' => function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    }])->get();

                                foreach ($clubs_sports_24 as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_act24_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_act24_n++;
                                    }
                                }

                                $statistics['clubs_sports_24_1'] = $i_clubsport_act24_1;
                                $statistics['clubs_sports_24_n'] = $i_clubsport_act24_n;

                                break;

                            case "7Tage":
                                /** Club activity 7d */
                                $clubs_7_all = $clubs_7_all
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->whereBetween('updated_at', [$past_7, $today])
                                    ->orWhere(function($query) use ($past_7, $today){
                                        $query->whereHas('posts', function($query2) use ($past_7, $today){
                                            $query2->whereBetween('post.created_at', [$past_7, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_7, $today]);
                                    })
                                    ->get()->count();
                                $statistics['clubs_7_all'] = $clubs_7_all;

                                $clubs_7_accepted = $clubs_7_accepted
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereHas('users')
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->whereBetween('updated_at', [$past_7, $today])
                                    ->orWhere(function($query) use ($past_7, $today){
                                        $query->whereHas('posts', function($query2) use ($past_7, $today){
                                            $query2->whereBetween('post.created_at', [$past_7, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_7, $today]);
                                    })
                                    ->get()->count();
                                $statistics['clubs_7_accepted'] = $clubs_7_accepted;


                                $clubs_sports_7 = $clubs_sports_7
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereBetween('updated_at', [$past_7, $today])
                                    ->orWhere(function($query) use ($past_7, $today){
                                        $query->has('posts')->whereBetween('created_at', [$past_7, $today]);
                                    })
                                    ->withCount(['sports' => function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    }])->get();

                                foreach ($clubs_sports_7 as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_act7_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_act7_n++;
                                    }
                                }

                                $statistics['clubs_sports_7_1'] = $i_clubsport_act7_1;
                                $statistics['clubs_sports_7_n'] = $i_clubsport_act7_n;

                                break;


                            case "30Tage":
                                /** Club activity 30d */
                                $clubs_30_all = $clubs_30_all
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->whereBetween('updated_at', [$past_30, $today])
                                    ->orWhere(function($query) use ($past_30, $today){
                                        $query->whereHas('posts', function($query2) use ($past_30, $today){
                                            $query2->whereBetween('post.created_at', [$past_30, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_30, $today]);
                                    })
                                    ->get()->count();
                                $statistics['clubs_30_all'] = $clubs_30_all;

                                $clubs_30_accepted = $clubs_30_accepted
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereHas('users')
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->whereBetween('updated_at', [$past_30, $today])
                                    ->orWhere(function($query) use ($past_30, $today){
                                        $query->whereHas('posts', function($query2) use ($past_30, $today){
                                            $query2->whereBetween('post.created_at', [$past_30, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_30, $today]);
                                    })
                                    ->get()->count();
                                $statistics['clubs_30_accepted'] = $clubs_30_accepted;

                                $clubs_sports_30 = $clubs_sports_30
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereBetween('updated_at', [$past_30, $today])
                                    ->orWhere(function($query) use ($past_30, $today){
                                        $query->whereHas('posts', function($query2) use ($past_30, $today){
                                            $query2->whereBetween('post.created_at', [$past_30, $today]);
                                        });
                                    })
                                    ->withCount(['sports' => function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    }])->get();

                                foreach ($clubs_sports_30 as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_act30_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_act30_n++;
                                    }
                                }

                                $statistics['clubs_sports_30_1'] = $i_clubsport_act30_1;
                                $statistics['clubs_sports_30_n'] = $i_clubsport_act30_n;

                                break;


                            case "90Tage":
                                /** Club activity 90d */
                                $clubs_90_all = $clubs_90_all
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->whereBetween('updated_at', [$past_90, $today])
                                    ->orWhere(function($query) use ($past_90, $today){
                                        $query->whereHas('posts', function($query2) use ($past_90, $today){
                                            $query2->whereBetween('post.created_at', [$past_90, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_90, $today]);
                                    })
                                    ->get()->count();
                                $statistics['clubs_90_all'] = $clubs_90_all;

                                $clubs_90_accepted = $clubs_90_accepted
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereHas('users')
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->whereBetween('updated_at', [$past_90, $today])
                                    ->orWhere(function($query) use ($past_90, $today){
                                        $query->whereHas('posts', function($query2) use ($past_90, $today){
                                            $query2->whereBetween('post.created_at', [$past_90, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_90, $today]);
                                    })
                                    ->get()->count();
                                $statistics['clubs_90_accepted'] = $clubs_90_accepted;

                                $clubs_sports_90 = $clubs_sports_90->where('federal_state', 'LIKE', $fed_state)
                                                                   ->whereBetween('updated_at', [$past_90, $today])
                                                                   ->orWhere(function($query) use ($past_90, $today){
                                                                       $query->whereHas('posts', function($query2) use ($past_90, $today){
                                                                           $query2->whereBetween('post.created_at', [$past_90, $today]);
                                                                       });
                                                                       //$query->has('posts')->whereBetween('created_at', [$past_90, $today]);
                                                                   })
                                                                   ->withCount(['sports' => function($query) use ($sportids){
                                                                       $query->whereIn('sport_id', $sportids);
                                                                   }])->get();

                                foreach ($clubs_sports_90 as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_act90_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_act90_n++;
                                    }
                                }

                                $statistics['clubs_sports_90_1'] = $i_clubsport_act90_1;
                                $statistics['clubs_sports_90_n'] = $i_clubsport_act90_n;

                                break;


                        }


                        break;


                    case 2:

                        switch ($subabschnitt_auswahl){

                            case "Logo":
                                /** Club intensity logo */
                                $clubs_logo_all = $clubs_logo_all
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereNotNull('avatar_updated_at')->where('avatar_updated_at', 'NOT LIKE', '')
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->get()->count();
                                $statistics['clubs_logo_all'] = $clubs_logo_all;

                                $clubs_logo_accepted = $clubs_logo_accepted
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereHas('users')
                                    ->whereNotNull('avatar_updated_at')->where('avatar_updated_at', 'NOT LIKE', '')
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->get()->count();
                                $statistics['clubs_logo_accepted'] = $clubs_logo_accepted;

                                $clubs_sports_logo = $clubs_sports_logo
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereNotNull('avatar_updated_at')->where('avatar_updated_at', 'NOT LIKE', '')
                                    ->withCount(['sports' => function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    }])->get();

                                foreach ($clubs_sports_logo as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_logo_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_logo_n++;
                                    }
                                }

                                $statistics['clubsport_logo_1'] = $i_clubsport_logo_1;
                                $statistics['clubsport_logo_n'] = $i_clubsport_logo_n;

                                break;

                            case "Header":
                                /** Club intensity header */
                                $clubs_header_all = $clubs_header_all
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereNotNull('header_updated_at')->where('header_updated_at', 'NOT LIKE', '')
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->get()->count();
                                $statistics['clubs_header_all'] = $clubs_header_all;

                                $clubs_header_accepted = $clubs_header_accepted
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereHas('users')
                                    ->whereNotNull('header_updated_at')->where('header_updated_at', 'NOT LIKE', '')
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->get()->count();
                                $statistics['clubs_header_accepted'] = $clubs_header_accepted;

                                $clubs_sports_header = $clubs_sports_header
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereNotNull('header_updated_at')->where('header_updated_at', 'NOT LIKE', '')
                                    ->withCount(['sports' => function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    }])->get();

                                foreach ($clubs_sports_header as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_header_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_header_n++;
                                    }
                                }

                                $statistics['clubsport_header_1'] = $i_clubsport_header_1;
                                $statistics['clubsport_header_n'] = $i_clubsport_header_n;

                                break;

                            case "Infos":
                                /** Club intensity info */
                                $clubs_infos_all = $clubs_infos_all
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereNotNull('description')->where('description', 'NOT LIKE', '')
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->get()->count();
                                $statistics['clubs_infos_all'] = $clubs_infos_all;

                                $clubs_info_accepted = $clubs_info_accepted
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereHas('users')
                                    ->whereNotNull('description')->where('description', 'NOT LIKE', '')
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->get()->count();
                                $statistics['clubs_infos_accepted'] = $clubs_info_accepted;

                                $clubs_sports_info = $clubs_sports_info
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereNotNull('description')->where('description', 'NOT LIKE', '')
                                    ->withCount(['sports' => function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    }])->get();

                                foreach ($clubs_sports_info as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_infos_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_infos_n++;
                                    }
                                }

                                $statistics['clubsport_infos_1'] = $i_clubsport_infos_1;
                                $statistics['clubsport_infos_n'] = $i_clubsport_infos_n;

                                break;


                            case "Teams":
                                /** Club intensity teams */
                                $clubs_teams_all = $clubs_teams_all
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->has('teams')
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->get()->count();
                                $statistics['clubs_teams_all'] = $clubs_teams_all;

                                $clubs_teams_accepted = $clubs_teams_accepted
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereHas('users')
                                    ->has('teams')
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->get()->count();
                                $statistics['clubs_teams_accepted'] = $clubs_teams_accepted;

                                $clubs_sports_teams = $clubs_sports_teams
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->has('teams')
                                    ->withCount(['sports' => function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    }])->get();

                                foreach ($clubs_sports_teams as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_teams_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_teams_n++;
                                    }
                                }

                                $statistics['clubsport_teams_1'] = $i_clubsport_teams_1;
                                $statistics['clubsport_teams_n'] = $i_clubsport_teams_n;

                                break;


                            case "Weblinks":
                                /** Club intensity weblinks */
                                $clubs_web_all = $clubs_web_all
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->WhereNotNull('m_social_facebook')
                                    ->OrWhereNotNull('m_social_twitter')
                                    ->OrWhereNotNull('m_social_googleplus')
                                    ->OrWhereNotNull('m_social_youtube')
                                    ->OrWhereNotNull('m_social_instagram')
                                    ->OrWhereNotNull('m_social_flickr')
                                    ->get()->count();
                                $statistics['clubs_web_all'] = $clubs_web_all;

                                $clubs_web_accepted = $clubs_web_accepted
                                    ->where('federal_state', 'LIKE', $fed_state)
//                                    ->whereHas('users', function ($query) use ($adminClub) {
//                                        $query->where('user_club_role.role_id', '=', $adminClub);
//                                    })
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->whereHas('users')
                                    ->WhereNotNull('m_social_facebook')
                                    ->OrWhereNotNull('m_social_twitter')
                                    ->OrWhereNotNull('m_social_googleplus')
                                    ->OrWhereNotNull('m_social_youtube')
                                    ->OrWhereNotNull('m_social_instagram')
                                    ->OrWhereNotNull('m_social_flickr')
                                    ->has('teams')
                                    ->get()->count();
                                $statistics['clubs_web_accepted'] = $clubs_web_accepted;

                                $clubs_sports_web = $clubs_sports_web
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->WhereNotNull('m_social_facebook')
                                    ->OrWhereNotNull('m_social_twitter')
                                    ->OrWhereNotNull('m_social_googleplus')
                                    ->OrWhereNotNull('m_social_youtube')
                                    ->OrWhereNotNull('m_social_instagram')
                                    ->OrWhereNotNull('m_social_flickr')
                                    ->has('teams')
                                    ->withCount(['sports' => function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    }])->get();

                                foreach ($clubs_sports_web as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_web_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_web_n++;
                                    }
                                }

                                $statistics['clubsport_web_1'] = $i_clubsport_web_1;
                                $statistics['clubsport_web_n'] = $i_clubsport_web_n;

                                break;


                            case "Erfolge":
                                /** Club intensity success */
                                $clubs_success_all = $clubs_success_all
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->has('achievements')
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->get()->count();
                                $statistics['clubs_success_all'] = $clubs_success_all;

                                $clubs_success_accepted = $clubs_success_accepted
                                    ->where('federal_state', 'LIKE', $fed_state)
//                                    ->whereHas('users', function ($query) use ($adminClub) {
//                                        $query->where('user_club_role.role_id', '=', $adminClub);
//                                    })
                                    ->whereHas('users')
                                    ->has('achievements')
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->get()->count();
                                $statistics['clubs_success_accepted'] = $clubs_success_accepted;

                                $clubs_sports_success = $clubs_sports_success
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->has('achievements')
                                    ->withCount('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->withCount(['sports' => function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    }])->get();

                                foreach ($clubs_sports_success as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_success_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_success_n++;
                                    }
                                }

                                $statistics['clubsport_success_1'] = $i_clubsport_success_1;
                                $statistics['clubsport_success_n'] = $i_clubsport_success_n;

                                break;

                            case "Posts":
                                /** Club intensity posts */
                                $clubs_posts_all = $clubs_posts_all
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->has('posts')
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->get()->count();
                                $statistics['clubs_posts_all'] = $clubs_posts_all;

                                $clubs_posts_accepted = $clubs_posts_accepted
                                    ->where('federal_state', 'LIKE', $fed_state)
//                                    ->whereHas('users', function ($query) use ($adminClub) {
//                                        $query->where('user_club_role.role_id', '=', $adminClub);
//                                    })
                                    ->whereHas('users')
                                    ->whereNotNull('m_owner')
                                    ->has('posts')
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->get()->count();
                                $statistics['clubs_posts_accepted'] = $clubs_posts_accepted;

                                $clubs_sports_posts = $clubs_sports_posts
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->has('posts')
                                    ->withCount(['sports' => function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    }])->get();

                                foreach ($clubs_sports_posts as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_posts_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_posts_n++;
                                    }
                                }

                                $statistics['clubsport_posts_1'] = $i_clubsport_posts_1;
                                $statistics['clubsport_posts_n'] = $i_clubsport_posts_n;

                                break;

                        }

                        break;

                }

            } else{

                switch ($abschnitt_auswahl){

                    case 0:

                        /** CLUBS */
                        $clubs_all = $clubs_all->where('federal_state', 'LIKE', $fed_state)->count();
                        $statistics['clubs_all'] = $clubs_all;


                        $clubs_accepted = $clubs_accepted
                            ->where('federal_state', 'LIKE', $fed_state)
                            ->whereHas('users')->count();
                        $statistics['clubs_accepted'] = $clubs_accepted;


//                        $clubs_sports = $clubs_sports
//                            ->where('federal_state', 'LIKE', $fed_state)
//                            ->whereNull('deleted_at')
//                            ->withCount('sports')->get();
//
//                        foreach ($clubs_sports as $club){
//                            if ($club->sports_count == 1){
//                                $i_clubsport_1++;
//                            }
//                            if ($club->sports_count > 1){
//                                $i_clubsport_n++;
//                            }
//                        }

                        $statistics['clubs_1_branch'] = $i_clubsport_1;
                        $statistics['clubs_n_branch'] = $i_clubsport_n;

                        break;


                    case 1:

                        switch ($subabschnitt_auswahl){

                            case "24h":
                                /** Club activity 24h */
                                $clubs_24_all = $clubs_24_all
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereBetween('updated_at', [$past_1, $today])
                                    ->orWhere(function($query) use ($past_1, $today){
                                        $query->whereHas('posts', function($query2) use ($past_1, $today){
                                            $query2->whereBetween('post.created_at', [$past_1, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_1, $today]);
                                    })
                                    ->get()->count();
                                $statistics['clubs_24_all'] = $clubs_24_all;

                                $clubs_24_accepted = $clubs_24_accepted
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereHas('users')
                                    ->whereBetween('updated_at', [$past_1, $today])
                                    ->orWhere(function($query) use ($past_1, $today){
                                        $query->whereHas('posts', function($query2) use ($past_1, $today){
                                            $query2->whereBetween('post.created_at', [$past_1, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_1, $today]);
                                    })
                                    ->get()->count();
                                $statistics['clubs_24_accepted'] = $clubs_24_accepted;


                                $clubs_sports_24 = $clubs_sports_24
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereBetween('updated_at', [$past_1, $today])
                                    ->orWhere(function($query) use ($past_1, $today){
                                        $query->has('posts')->whereBetween('created_at', [$past_1, $today]);
                                    })
                                    ->withCount('sports')->get();

                                foreach ($clubs_sports_24 as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_act24_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_act24_n++;
                                    }
                                }

                                $statistics['clubs_sports_24_1'] = $i_clubsport_act24_1;
                                $statistics['clubs_sports_24_n'] = $i_clubsport_act24_n;

                                break;


                            case "7Tage":
                                /** Club activity 7d */
                                $clubs_7_all = $clubs_7_all
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereBetween('updated_at', [$past_7, $today])
                                    ->orWhere(function($query) use ($past_7, $today){
                                        $query->whereHas('posts', function($query2) use ($past_7, $today){
                                            $query2->whereBetween('post.created_at', [$past_7, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_7, $today]);
                                    })
                                    ->get()->count();
                                $statistics['clubs_7_all'] = $clubs_7_all;

                                $clubs_7_accepted = $clubs_7_accepted
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereHas('users')
                                    ->whereBetween('updated_at', [$past_7, $today])
                                    ->orWhere(function($query) use ($past_7, $today){
                                        $query->whereHas('posts', function($query2) use ($past_7, $today){
                                            $query2->whereBetween('post.created_at', [$past_7, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_7, $today]);
                                    })
                                    ->get()->count();
                                $statistics['clubs_7_accepted'] = $clubs_7_accepted;


                                $clubs_sports_7 = $clubs_sports_7
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereBetween('updated_at', [$past_7, $today])
                                    ->orWhere(function($query) use ($past_7, $today){
                                        $query->whereHas('posts', function($query2) use ($past_7, $today){
                                            $query2->whereBetween('post.created_at', [$past_7, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_7, $today]);
                                    })
                                    ->withCount('sports')->get();

                                foreach ($clubs_sports_7 as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_act7_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_act7_n++;
                                    }
                                }

                                $statistics['clubs_sports_7_1'] = $i_clubsport_act7_1;
                                $statistics['clubs_sports_7_n'] = $i_clubsport_act7_n;

                                break;

                            case "30Tage":
                                /** Club activity 30d */
                                $clubs_30_all = $clubs_30_all
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereBetween('updated_at', [$past_30, $today])
                                    ->orWhere(function($query) use ($past_30, $today){
                                        $query->whereHas('posts', function($query2) use ($past_30, $today){
                                            $query2->whereBetween('post.created_at', [$past_30, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_30, $today]);
                                    })
                                    ->get()->count();
                                $statistics['clubs_30_all'] = $clubs_30_all;

                                $clubs_30_accepted = $clubs_30_accepted
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereHas('users')
                                    ->whereBetween('updated_at', [$past_30, $today])
                                    ->orWhere(function($query) use ($past_30, $today){
                                        $query->whereHas('posts', function($query2) use ($past_30, $today){
                                            $query2->whereBetween('post.created_at', [$past_30, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_30, $today]);
                                    })
                                    ->get()->count();
                                $statistics['clubs_30_accepted'] = $clubs_30_accepted;

                                $clubs_sports_30 = $clubs_sports_30
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereBetween('updated_at', [$past_30, $today])
                                    ->orWhere(function($query) use ($past_30, $today){
                                        $query->whereHas('posts', function($query2) use ($past_30, $today){
                                            $query2->whereBetween('post.created_at', [$past_30, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_30, $today]);
                                    })
                                    ->withCount('sports')->get();

                                foreach ($clubs_sports_30 as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_act30_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_act30_n++;
                                    }
                                }

                                $statistics['clubs_sports_30_1'] = $i_clubsport_act30_1;
                                $statistics['clubs_sports_30_n'] = $i_clubsport_act30_n;

                                break;


                            case "90Tage":
                                /** Club activity 90d */
                                $clubs_90_all = $clubs_90_all
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereBetween('updated_at', [$past_90, $today])
                                    ->orWhere(function($query) use ($past_90, $today){
                                        $query->whereHas('posts', function($query2) use ($past_90, $today){
                                            $query2->whereBetween('post.created_at', [$past_90, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_90, $today]);
                                    })
                                    ->get()->count();
                                $statistics['clubs_90_all'] = $clubs_90_all;

                                $clubs_90_accepted = $clubs_90_accepted
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereHas('users')
                                    ->whereBetween('updated_at', [$past_90, $today])
                                    ->orWhere(function($query) use ($past_90, $today){
                                        $query->whereHas('posts', function($query2) use ($past_90, $today){
                                            $query2->whereBetween('post.created_at', [$past_90, $today]);
                                        });
                                    })
                                    ->get()->count();
                                $statistics['clubs_90_accepted'] = $clubs_90_accepted;

                                $clubs_sports_90 = $clubs_sports_90
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereBetween('updated_at', [$past_90, $today])
                                    ->orWhere(function($query) use ($past_90, $today){
                                        $query->whereHas('posts', function($query2) use ($past_90, $today){
                                            $query2->whereBetween('post.created_at', [$past_90, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_90, $today]);
                                    })
                                    ->withCount('sports')->get();

                                foreach ($clubs_sports_90 as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_act90_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_act90_n++;
                                    }
                                }

                                $statistics['clubs_sports_90_1'] = $i_clubsport_act90_1;
                                $statistics['clubs_sports_90_n'] = $i_clubsport_act90_n;

                                break;

                        }

                        break;

                    case 2:

                        switch ($subabschnitt_auswahl){

                            case "Logo":
                                /** Club intensity logo */
                                $clubs_logo_all = $clubs_logo_all
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereNotNull('avatar_updated_at')
                                    ->where('avatar_updated_at', 'NOT LIKE', '')
                                    ->get()->count();
                                $statistics['clubs_logo_all'] = $clubs_logo_all;

                                $clubs_logo_accepted = $clubs_logo_accepted
                                    ->where('federal_state', 'LIKE', $fed_state)
//                                    ->whereHas('users', function ($query) use ($adminClub) {
//                                        $query->where('user_club_role.role_id', '=', $adminClub);
//                                    })
                                    ->whereHas('users')
                                    ->whereNotNull('avatar_updated_at')
                                    ->where('avatar_updated_at', 'NOT LIKE', '')
                                    ->get()->count();
                                $statistics['clubs_logo_accepted'] = $clubs_logo_accepted;

                                $clubs_sports_logo = $clubs_sports_logo
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereNotNull('avatar_updated_at')
                                    ->where('avatar_updated_at', 'NOT LIKE', '')
                                    ->withCount('sports')->get();

                                foreach ($clubs_sports_logo as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_logo_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_logo_n++;
                                    }
                                }

                                $statistics['clubsport_logo_1'] = $i_clubsport_logo_1;
                                $statistics['clubsport_logo_n'] = $i_clubsport_logo_n;

                                break;

                            case "Header":
                                /** Club intensity header */
                                $clubs_header_all = $clubs_header_all
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereNotNull('header_updated_at')
                                    ->where('header_updated_at', 'NOT LIKE', '')
                                    ->get()->count();
                                $statistics['clubs_header_all'] = $clubs_header_all;

                                $clubs_header_accepted = $clubs_header_accepted
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereHas('users')
                                    ->whereNotNull('header_updated_at')
                                    ->where('header_updated_at', 'NOT LIKE', '')
                                    ->get()->count();
                                $statistics['clubs_header_accepted'] = $clubs_header_accepted;

                                $clubs_sports_header = $clubs_sports_header
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereNotNull('header_updated_at')
                                    ->where('header_updated_at', 'NOT LIKE', '')
                                    ->withCount('sports')->get();

                                foreach ($clubs_sports_header as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_header_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_header_n++;
                                    }
                                }

                                $statistics['clubsport_header_1'] = $i_clubsport_header_1;
                                $statistics['clubsport_header_n'] = $i_clubsport_header_n;

                                break;


                            case "Infos":
                                /** Club intensity info */
                                $clubs_infos_all = $clubs_infos_all
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereNotNull('description')
                                    ->where('description', 'NOT LIKE', '')
                                    ->get()->count();
                                $statistics['clubs_infos_all'] = $clubs_infos_all;

                                $clubs_info_accepted = $clubs_info_accepted
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereHas('users')
                                    ->whereNotNull('description')
                                    ->where('description', 'NOT LIKE', '')
                                    ->get()->count();
                                $statistics['clubs_infos_accepted'] = $clubs_info_accepted;

                                $clubs_sports_info = $clubs_sports_info
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereNotNull('description')
                                    ->where('description', 'NOT LIKE', '')
                                    ->withCount('sports')->get();

                                foreach ($clubs_sports_info as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_infos_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_infos_n++;
                                    }
                                }

                                $statistics['clubsport_infos_1'] = $i_clubsport_infos_1;
                                $statistics['clubsport_infos_n'] = $i_clubsport_infos_n;

                                break;


                            case "Teams":
                                /** Club intensity teams */
                                $clubs_teams_all = $clubs_teams_all
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->has('teams')
                                    ->get()->count();
                                $statistics['clubs_teams_all'] = $clubs_teams_all;

                                $clubs_teams_accepted = $clubs_teams_accepted
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereHas('users')
                                    ->has('teams')
                                    ->get()->count();
                                $statistics['clubs_teams_accepted'] = $clubs_teams_accepted;

                                $clubs_sports_teams = $clubs_sports_teams
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->has('teams')
                                    ->withCount('sports')->get();

                                foreach ($clubs_sports_teams as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_teams_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_teams_n++;
                                    }
                                }

                                $statistics['clubsport_teams_1'] = $i_clubsport_teams_1;
                                $statistics['clubsport_teams_n'] = $i_clubsport_teams_n;

                                break;

                            case "Weblinks":
                                /** Club intensity weblinks */
                                $clubs_web_all = $clubs_web_all
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->WhereNotNull('m_social_facebook')
                                    ->OrWhereNotNull('m_social_twitter')
                                    ->OrWhereNotNull('m_social_googleplus')
                                    ->OrWhereNotNull('m_social_youtube')
                                    ->OrWhereNotNull('m_social_instagram')
                                    ->OrWhereNotNull('m_social_flickr')
                                    ->get()->count();
                                $statistics['clubs_web_all'] = $clubs_web_all;

                                $clubs_web_accepted = $clubs_web_accepted
                                    ->where('federal_state', 'LIKE', $fed_state)
//                                    ->whereHas('users', function ($query) use ($adminClub) {
//                                        $query->where('user_club_role.role_id', '=', $adminClub);
//                                    })
                                    ->whereHas('users')
                                    ->has('teams')
                                    ->WhereNotNull('m_social_facebook')
                                    ->OrWhereNotNull('m_social_twitter')
                                    ->OrWhereNotNull('m_social_googleplus')
                                    ->OrWhereNotNull('m_social_youtube')
                                    ->OrWhereNotNull('m_social_instagram')
                                    ->OrWhereNotNull('m_social_flickr')
                                    ->get()->count();
                                $statistics['clubs_web_accepted'] = $clubs_web_accepted;

                                $clubs_sports_web = $clubs_sports_web
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->WhereNotNull('m_social_facebook')
                                    ->OrWhereNotNull('m_social_twitter')
                                    ->OrWhereNotNull('m_social_googleplus')
                                    ->OrWhereNotNull('m_social_youtube')
                                    ->OrWhereNotNull('m_social_instagram')
                                    ->OrWhereNotNull('m_social_flickr')
                                    ->has('teams')
                                    ->withCount('sports')->get();

                                foreach ($clubs_sports_web as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_web_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_web_n++;
                                    }
                                }

                                $statistics['clubsport_web_1'] = $i_clubsport_web_1;
                                $statistics['clubsport_web_n'] = $i_clubsport_web_n;

                                break;


                            case "Erfolge":
                                /** Club intensity success */
                                $clubs_success_all = $clubs_success_all
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->has('achievements')
                                    ->get()->count();
                                $statistics['clubs_success_all'] = $clubs_success_all;

                                $clubs_success_accepted = $clubs_success_accepted
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->whereHas('users')
                                    ->has('achievements')
                                    ->get()->count();
                                $statistics['clubs_success_accepted'] = $clubs_success_accepted;

                                $clubs_sports_success = $clubs_sports_success
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->has('achievements')
                                    ->withCount('sports')->get();

                                foreach ($clubs_sports_success as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_success_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_success_n++;
                                    }
                                }

                                $statistics['clubsport_success_1'] = $i_clubsport_success_1;
                                $statistics['clubsport_success_n'] = $i_clubsport_success_n;

                                break;


                            case "Posts":
                                /** Club intensity posts */
                                $clubs_posts_all = $clubs_posts_all
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->has('posts')
                                    ->get()->count();
                                $statistics['clubs_posts_all'] = $clubs_posts_all;

                                $clubs_posts_accepted = $clubs_posts_accepted
                                    ->where('federal_state', 'LIKE', $fed_state)
//                                    ->whereHas('users', function ($query) use ($adminClub) {
//                                        $query->where('user_club_role.role_id', '=', $adminClub);
//                                    })
                                    ->whereHas('users')
                                    ->whereNotNull('m_owner')
                                    ->has('posts')
                                    ->get()->count();
                                $statistics['clubs_posts_accepted'] = $clubs_posts_accepted;

                                $clubs_sports_posts = $clubs_sports_posts
                                    ->where('federal_state', 'LIKE', $fed_state)
                                    ->has('posts')
                                    ->withCount('sports')->get();

                                foreach ($clubs_sports_posts as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_posts_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_posts_n++;
                                    }
                                }

                                $statistics['clubsport_posts_1'] = $i_clubsport_posts_1;
                                $statistics['clubsport_posts_n'] = $i_clubsport_posts_n;

                                break;

                        }

                        break;
                }

            }

        }


        /** Bundesland nicht ausgewählt, alle Daten anzeigen */
        if ($fed_state == '' || $fed_state == 'all'){

            /** CLUBS */
            $clubs_all = new Club;
            $clubs_accepted = new Club;
            $clubs_sports = new Club;

            $i_clubsport_1 = 0;
            $i_clubsport_n = 0;

            /** Club activity 24h */
            $clubs_24_all = new Club;
            $clubs_24_accepted = new Club;
            $clubs_sports_24 = new Club;
            $i_clubsport_act24_1 = 0;
            $i_clubsport_act24_n = 0;

            /** Club activity 7d */
            $clubs_7_all = new Club;
            $clubs_7_accepted = new Club;
            $clubs_sports_7 = new Club;
            $i_clubsport_act7_1 = 0;
            $i_clubsport_act7_n = 0;

            /** club activity 30d */
            $clubs_30_all = new Club;
            $clubs_30_accepted = new Club;
            $clubs_sports_30 = new Club;
            $i_clubsport_act30_1 = 0;
            $i_clubsport_act30_n = 0;

            /** club activity 90d */
            $clubs_90_all = new Club;
            $clubs_90_accepted = new Club;
            $clubs_sports_90 = new Club;
            $i_clubsport_act90_1 = 0;
            $i_clubsport_act90_n = 0;

            /** club intensity logo */
            $clubs_logo_all = new Club;
            $clubs_logo_accepted = new Club;
            $clubs_sports_logo = new Club;
            $i_clubsport_logo_1 = 0;
            $i_clubsport_logo_n = 0;

            /** club intensity header*/
            $clubs_header_all = new Club;
            $clubs_header_accepted = new Club;
            $clubs_sports_header = new Club;
            $i_clubsport_header_1 = 0;
            $i_clubsport_header_n = 0;

            /** club intensity infos */
            $clubs_infos_all = new Club;
            $clubs_info_accepted = new Club;
            $clubs_sports_info = new Club;
            $i_clubsport_infos_1 = 0;
            $i_clubsport_infos_n = 0;

            /** club intensity teams */
            $clubs_teams_all = new Club;
            $clubs_teams_accepted = new Club;
            $clubs_sports_teams = new Club;
            $i_clubsport_teams_1 = 0;
            $i_clubsport_teams_n = 0;

            /** club intensity Weblinks */
            $clubs_web_all = new Club;
            $clubs_web_accepted = new Club;
            $clubs_sports_web = new Club;
            $i_clubsport_web_1 = 0;
            $i_clubsport_web_n = 0;

            /** club intensity success */
            $clubs_success_all = new Club;
            $clubs_success_accepted = new Club;
            $clubs_sports_success = new Club;
            $i_clubsport_success_1 = 0;
            $i_clubsport_success_n = 0;

            /** club intensity posts */
            $clubs_posts_all = new Club;
            $clubs_posts_accepted = new Club;
            $clubs_sports_posts = new Club;
            $i_clubsport_posts_1 = 0;
            $i_clubsport_posts_n = 0;

            if ($sporttype !== ''){

                $statistics['sport'] = 1;

                switch ($abschnitt_auswahl){

                    case 0:

                        /** CLUBS */
                        $clubs_all = $clubs_all
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->count();
                        $statistics['clubs_all'] = $clubs_all;

                        $clubs_accepted = $clubs_accepted
                            ->whereHas('users', function($query) use ($adminClub){
                                $query->where('user_club_role.role_id', '=', $adminClub);
                            })
                            ->whereHas('sports', function($query) use ($sportids){
                                $query->whereIn('sport_id', $sportids);
                            })
                            ->get()->count();
                        $statistics['clubs_accepted'] = $clubs_accepted;


//                        $clubs_sports = $clubs_sports
//                            ->withCount('sports')->get();
//
//                        foreach ($clubs_sports as $club){
//                            if ($club->sports_count == 1){
//                                $i_clubsport_1++;
//                            }
//                            if ($club->sports_count > 1){
//                                $i_clubsport_n++;
//                            }
//                        }

                        $statistics['clubs_1_branch'] = $i_clubsport_1;
                        $statistics['clubs_n_branch'] = $i_clubsport_n;

                        break;

                    case 1:

                        switch ($subabschnitt_auswahl){

                            case "24h":
                                /** Club activity 24h */
                                $clubs_24_all = $clubs_24_all
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->whereBetween('updated_at', [$past_1, $today])
                                    ->orWhere(function($query) use ($past_1, $today){
                                        $query->whereHas('posts', function($query2) use ($past_1, $today){
                                            $query2->whereBetween('post.created_at', [$past_1, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_1, $today]);
                                    })
                                    ->get()->count();
                                $statistics['clubs_24_all'] = $clubs_24_all;

                                $clubs_24_accepted = $clubs_24_accepted
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->whereHas('users')
                                    ->whereBetween('updated_at', [$past_1, $today])
                                    ->orWhere(function($query) use ($past_1, $today){
                                        $query->whereHas('posts', function($query2) use ($past_1, $today){
                                            $query2->whereBetween('post.created_at', [$past_1, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_1, $today]);
                                    })
                                    ->get()->count();
                                $statistics['clubs_24_accepted'] = $clubs_24_accepted;

                                $clubs_sports_24 = $clubs_sports_24
                                    ->whereBetween('updated_at', [$past_1, $today])
                                    ->orWhere(function($query) use ($past_1, $today){
                                        $query->whereHas('posts', function($query2) use ($past_1, $today){
                                            $query2->whereBetween('post.created_at', [$past_1, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_1, $today]);
                                    })
                                    ->withCount(['sports' => function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    }])->get();

                                foreach ($clubs_sports_24 as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_act24_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_act24_n++;
                                    }
                                }

                                $statistics['clubs_sports_24_1'] = $i_clubsport_act24_1;
                                $statistics['clubs_sports_24_n'] = $i_clubsport_act24_n;

                                break;

                            case "7Tage":
                                /** Club activity 7d */
                                $clubs_7_all = $clubs_7_all
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->whereBetween('updated_at', [$past_7, $today])
                                    ->orWhere(function($query) use ($past_7, $today){
                                        $query->whereHas('posts', function($query2) use ($past_7, $today){
                                            $query2->whereBetween('post.created_at', [$past_7, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_7, $today]);
                                    })
                                    ->get()->count();
                                $statistics['clubs_7_all'] = $clubs_7_all;

                                $clubs_7_accepted = $clubs_7_accepted
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->whereHas('users')
                                    ->whereBetween('updated_at', [$past_7, $today])
                                    ->orWhere(function($query) use ($past_7, $today){
                                        $query->whereHas('posts', function($query2) use ($past_7, $today){
                                            $query2->whereBetween('post.created_at', [$past_7, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_7, $today]);
                                    })
                                    ->get()->count();
                                $statistics['clubs_7_accepted'] = $clubs_7_accepted;

                                $clubs_sports_7 = $clubs_sports_7
                                    ->whereBetween('updated_at', [$past_7, $today])
                                    ->orWhere(function($query) use ($past_7, $today){
                                        $query->whereHas('posts', function($query2) use ($past_7, $today){
                                            $query2->whereBetween('post.created_at', [$past_7, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_7, $today]);
                                    })
                                    ->withCount(['sports' => function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    }])->get();

                                foreach ($clubs_sports_7 as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_act7_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_act7_n++;
                                    }
                                }

                                $statistics['clubs_sports_7_1'] = $i_clubsport_act7_1;
                                $statistics['clubs_sports_7_n'] = $i_clubsport_act7_n;

                                break;

                            case "30Tage":
                                /** Club activity 30d */
                                $clubs_30_all = $clubs_30_all
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->whereBetween('updated_at', [$past_30, $today])
                                    ->orWhere(function($query) use ($past_30, $today){
                                        $query->whereHas('posts', function($query2) use ($past_30, $today){
                                            $query2->whereBetween('post.created_at', [$past_30, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_30, $today]);
                                    })
                                    ->get()->count();
                                $statistics['clubs_30_all'] = $clubs_30_all;

                                $clubs_30_accepted = $clubs_30_accepted
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->whereHas('users')
                                    ->whereBetween('updated_at', [$past_30, $today])
                                    ->orWhere(function($query) use ($past_30, $today){
                                        $query->whereHas('posts', function($query2) use ($past_30, $today){
                                            $query2->whereBetween('post.created_at', [$past_30, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_30, $today]);
                                    })
                                    ->get()->count();
                                $statistics['clubs_30_accepted'] = $clubs_30_accepted;

                                $clubs_sports_30 = $clubs_sports_30
                                    ->whereBetween('updated_at', [$past_30, $today])
                                    ->orWhere(function($query) use ($past_30, $today){
                                        $query->whereHas('posts', function($query2) use ($past_30, $today){
                                            $query2->whereBetween('post.created_at', [$past_30, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_30, $today]);
                                    })
                                    ->withCount(['sports' => function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    }])->get();

                                foreach ($clubs_sports_30 as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_act30_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_act30_n++;
                                    }
                                }

                                $statistics['clubs_sports_30_1'] = $i_clubsport_act30_1;
                                $statistics['clubs_sports_30_n'] = $i_clubsport_act30_n;

                                break;

                            case "90Tage":
                                /** Club activity 90d */
                                $clubs_90_all = $clubs_90_all
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->whereBetween('updated_at', [$past_90, $today])
                                    ->orWhere(function($query) use ($past_90, $today){
                                        $query->whereHas('posts', function($query2) use ($past_90, $today){
                                            $query2->whereBetween('post.created_at', [$past_90, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_90, $today]);
                                    })
                                    ->get()->count();
                                $statistics['clubs_90_all'] = $clubs_90_all;

                                $clubs_90_accepted = $clubs_90_accepted
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->whereHas('users')
                                    ->whereBetween('updated_at', [$past_90, $today])
                                    ->orWhere(function($query) use ($past_90, $today){
                                        $query->whereHas('posts', function($query2) use ($past_90, $today){
                                            $query2->whereBetween('post.created_at', [$past_90, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_90, $today]);
                                    })
                                    ->get()->count();
                                $statistics['clubs_90_accepted'] = $clubs_90_accepted;

                                $clubs_sports_90 = $clubs_sports_90
                                    ->whereBetween('updated_at', [$past_90, $today])
                                    ->orWhere(function($query) use ($past_90, $today){
                                        $query->whereHas('posts', function($query2) use ($past_90, $today){
                                            $query2->whereBetween('post.created_at', [$past_90, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_90, $today]);
                                    })
                                    ->withCount(['sports' => function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    }])->get();

                                foreach ($clubs_sports_90 as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_act90_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_act90_n++;
                                    }
                                }

                                $statistics['clubs_sports_90_1'] = $i_clubsport_act90_1;
                                $statistics['clubs_sports_90_n'] = $i_clubsport_act90_n;

                                break;

                        }

                        break;

                    case 2:

                        switch ($subabschnitt_auswahl){

                            case "Logo":
                                /** Club intensity logo */
                                $clubs_logo_all = $clubs_logo_all
                                    ->whereNotNull('avatar_updated_at')->where('avatar_updated_at', 'NOT LIKE', '')
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->get()->count();
                                $statistics['clubs_logo_all'] = $clubs_logo_all;

                                $clubs_logo_accepted = $clubs_logo_accepted
                                    ->whereHas('users')
                                    ->whereNotNull('avatar_updated_at')->where('avatar_updated_at', 'NOT LIKE', '')
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->get()->count();
                                $statistics['clubs_logo_accepted'] = $clubs_logo_accepted;

                                $clubs_sports_logo = $clubs_sports_logo
                                    ->whereNotNull('avatar_updated_at')->where('avatar_updated_at', 'NOT LIKE', '')
                                    ->withCount(['sports' => function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    }])->get();

                                foreach ($clubs_sports_logo as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_logo_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_logo_n++;
                                    }
                                }

                                $statistics['clubsport_logo_1'] = $i_clubsport_logo_1;
                                $statistics['clubsport_logo_n'] = $i_clubsport_logo_n;

                                break;

                            case "Header":
                                /** Club intensity header */
                                $clubs_header_all = $clubs_header_all
                                    ->whereNotNull('header_updated_at')->where('header_updated_at', 'NOT LIKE', '')
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->get()->count();
                                $statistics['clubs_header_all'] = $clubs_header_all;

                                $clubs_header_accepted = $clubs_header_accepted
                                    ->whereHas('users')
                                    ->whereNotNull('header_updated_at')->where('header_updated_at', 'NOT LIKE', '')
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->get()->count();
                                $statistics['clubs_header_accepted'] = $clubs_header_accepted;

                                $clubs_sports_header = $clubs_sports_header
                                    ->whereNotNull('header_updated_at')->where('header_updated_at', 'NOT LIKE', '')
                                    ->withCount(['sports' => function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    }])->get();

                                foreach ($clubs_sports_header as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_header_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_header_n++;
                                    }
                                }

                                $statistics['clubsport_header_1'] = $i_clubsport_header_1;
                                $statistics['clubsport_header_n'] = $i_clubsport_header_n;

                                break;

                            case "Infos":
                                /** Club intensity info */
                                $clubs_infos_all = $clubs_infos_all
                                    ->whereNotNull('description')->where('description', 'NOT LIKE', '')
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->get()->count();
                                $statistics['clubs_infos_all'] = $clubs_infos_all;

                                $clubs_info_accepted = $clubs_info_accepted
                                    ->whereHas('users')
                                    ->whereNotNull('description')->where('description', 'NOT LIKE', '')
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })->get()->count();
                                $statistics['clubs_infos_accepted'] = $clubs_info_accepted;

                                $clubs_sports_info = $clubs_sports_info
                                    ->whereNotNull('description')->where('description', 'NOT LIKE', '')
                                    ->withCount(['sports' => function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    }])->get();

                                foreach ($clubs_sports_info as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_infos_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_infos_n++;
                                    }
                                }

                                $statistics['clubsport_infos_1'] = $i_clubsport_infos_1;
                                $statistics['clubsport_infos_n'] = $i_clubsport_infos_n;

                                break;


                            case "Teams":
                                /** Club intensity teams */
                                $clubs_teams_all = $clubs_teams_all
                                    ->has('teams')
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })->get()->count();
                                $statistics['clubs_teams_all'] = $clubs_teams_all;

                                $clubs_teams_accepted = $clubs_teams_accepted
                                    ->whereHas('users')
                                    ->has('teams')
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })->get()->count();
                                $statistics['clubs_teams_accepted'] = $clubs_teams_accepted;

                                $clubs_sports_teams = $clubs_sports_teams
                                    ->has('teams')
                                    ->withCount(['sports' => function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    }])->get();

                                foreach ($clubs_sports_teams as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_teams_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_teams_n++;
                                    }
                                }

                                $statistics['clubsport_teams_1'] = $i_clubsport_teams_1;
                                $statistics['clubsport_teams_n'] = $i_clubsport_teams_n;

                                break;


                            case "Weblinks":
                                /** Club intensity weblinks */
                                $clubs_web_all = $clubs_web_all
                                    ->WhereNotNull('m_social_facebook')
                                    ->OrWhereNotNull('m_social_twitter')
                                    ->OrWhereNotNull('m_social_googleplus')
                                    ->OrWhereNotNull('m_social_youtube')
                                    ->OrWhereNotNull('m_social_instagram')
                                    ->OrWhereNotNull('m_social_flickr')
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->get()->count();
                                $statistics['clubs_web_all'] = $clubs_web_all;

                                $clubs_web_accepted = $clubs_web_accepted
                                    ->whereHas('users')
                                    ->WhereNotNull('m_social_facebook')
                                    ->OrWhereNotNull('m_social_twitter')
                                    ->OrWhereNotNull('m_social_googleplus')
                                    ->OrWhereNotNull('m_social_youtube')
                                    ->OrWhereNotNull('m_social_instagram')
                                    ->OrWhereNotNull('m_social_flickr')
                                    ->has('teams')
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->get()->count();
                                $statistics['clubs_web_accepted'] = $clubs_web_accepted;

                                $clubs_sports_web = $clubs_sports_web
                                    ->WhereNotNull('m_social_facebook')
                                    ->OrWhereNotNull('m_social_twitter')
                                    ->OrWhereNotNull('m_social_googleplus')
                                    ->OrWhereNotNull('m_social_youtube')
                                    ->OrWhereNotNull('m_social_instagram')
                                    ->OrWhereNotNull('m_social_flickr')
                                    ->has('teams')
                                    ->withCount(['sports' => function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    }])->get();

                                foreach ($clubs_sports_web as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_web_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_web_n++;
                                    }
                                }

                                $statistics['clubsport_web_1'] = $i_clubsport_web_1;
                                $statistics['clubsport_web_n'] = $i_clubsport_web_n;

                                break;


                            case "Erfolge":
                                /** Club intensity success */
                                $clubs_success_all = $clubs_success_all
                                    ->has('achievements')
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->get()->count();
                                $statistics['clubs_success_all'] = $clubs_success_all;

                                $clubs_success_accepted = $clubs_success_accepted
                                    ->whereHas('users')
                                    ->has('achievements')
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->get()->count();
                                $statistics['clubs_success_accepted'] = $clubs_success_accepted;

                                $clubs_sports_success = $clubs_sports_success
                                    ->has('achievements')
                                    ->withCount(['sports' => function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    }])->get();

                                foreach ($clubs_sports_success as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_success_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_success_n++;
                                    }
                                }

                                $statistics['clubsport_success_1'] = $i_clubsport_success_1;
                                $statistics['clubsport_success_n'] = $i_clubsport_success_n;

                                break;


                            case "Posts":
                                /** Club intensity posts */
                                $clubs_posts_all = $clubs_posts_all
                                    ->has('posts')
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->get()->count();
                                $statistics['clubs_posts_all'] = $clubs_posts_all;

                                $clubs_posts_accepted = $clubs_posts_accepted
                                    ->whereHas('users')
                                    ->has('posts')
                                    ->whereHas('sports', function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    })
                                    ->get()->count();
                                $statistics['clubs_posts_accepted'] = $clubs_posts_accepted;

                                $clubs_sports_posts = $clubs_sports_posts
                                    ->has('posts')
                                    ->withCount(['sports' => function($query) use ($sportids){
                                        $query->whereIn('sport_id', $sportids);
                                    }])->get();

                                foreach ($clubs_sports_posts as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_posts_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_posts_n++;
                                    }
                                }

                                $statistics['clubsport_posts_1'] = $i_clubsport_posts_1;
                                $statistics['clubsport_posts_n'] = $i_clubsport_posts_n;

                                break;

                        }

                        break;

                }
            } else{

                switch ($abschnitt_auswahl){

                    case 0:

                        /** CLUBS */
                        $clubs_all = $clubs_all->count();
                        $statistics['clubs_all'] = $clubs_all;

                        $clubs_accepted = $clubs_accepted
//                            ->whereHas('users')->count();
                            ->whereHas('users', function($query){
                                $query->where('user_club_role.role_id', '=', 4);
                            })->get()->count();
                        $statistics['clubs_accepted'] = $clubs_accepted + $unpublished_clubs_with_admin;

//                        $clubs_sports = $clubs_sports->withCount('sports')->get();
//                        foreach ($clubs_sports as $club){
//                            if ($club->sports_count == 1){
//                                $i_clubsport_1++;
//                            }
//                            if ($club->sports_count > 1){
//                                $i_clubsport_n++;
//                            }
//                        }

                        $statistics['clubs_1_branch'] = $i_clubsport_1;
                        $statistics['clubs_n_branch'] = $i_clubsport_n;

                        break;

                    case 1:

                        switch ($subabschnitt_auswahl){

                            case "24h":
                                /** Club activity 24h */
                                $clubs_24_all = $clubs_24_all
                                    ->whereBetween('updated_at', [$past_1, $today])
                                    ->orWhere(function($query) use ($past_1, $today){
                                        $query->whereHas('posts', function($query2) use ($past_1, $today){
                                            $query2->whereBetween('post.created_at', [$past_1, $today]);
                                        });
                                    })
                                    ->get()->count();
                                $statistics['clubs_24_all'] = $clubs_24_all;

                                $clubs_24_accepted = $clubs_24_accepted
                                    ->whereHas('users')
                                    ->whereBetween('updated_at', [$past_1, $today])
                                    ->orWhere(function($query) use ($past_1, $today){
                                        $query->whereHas('posts', function($query2) use ($past_1, $today){
                                            $query2->whereBetween('post.created_at', [$past_1, $today]);
                                        });
                                    })
                                    ->get()->count();
                                $statistics['clubs_24_accepted'] = $clubs_24_accepted;

                                $clubs_sports_24 = $clubs_sports_24
                                    ->whereBetween('updated_at', [$past_1, $today])
                                    ->orWhere(function($query) use ($past_1, $today){
                                        $query->whereHas('posts', function($query2) use ($past_1, $today){
                                            $query2->whereBetween('post.created_at', [$past_1, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_1, $today]);
                                    })
                                    ->withCount('sports')->get();

                                foreach ($clubs_sports_24 as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_act24_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_act24_n++;
                                    }
                                }

                                $statistics['clubs_sports_24_1'] = $i_clubsport_act24_1;
                                $statistics['clubs_sports_24_n'] = $i_clubsport_act24_n;

                                break;

                            case "7Tage":
                                /** Club activity 7d */
                                $clubs_7_all = $clubs_7_all
                                    ->whereBetween('updated_at', [$past_7, $today])
                                    ->orWhere(function($query) use ($past_7, $today){
                                        $query->whereHas('posts', function($query2) use ($past_7, $today){
                                            $query2->whereBetween('post.created_at', [$past_7, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_7, $today]);
                                    })
                                    ->get()->count();
                                $statistics['clubs_7_all'] = $clubs_7_all;

                                $clubs_7_accepted = $clubs_7_accepted
                                    ->whereHas('users')
                                    ->whereBetween('updated_at', [$past_7, $today])
                                    ->orWhere(function($query) use ($past_7, $today){
                                        $query->whereHas('posts', function($query2) use ($past_7, $today){
                                            $query2->whereBetween('post.created_at', [$past_7, $today]);
                                        });
                                    })
                                    ->get()->count();
                                $statistics['clubs_7_accepted'] = $clubs_7_accepted;

                                $clubs_sports_7 = $clubs_sports_7
                                    ->whereBetween('updated_at', [$past_7, $today])
                                    ->orWhere(function($query) use ($past_7, $today){
                                        $query->whereHas('posts', function($query2) use ($past_7, $today){
                                            $query2->whereBetween('post.created_at', [$past_7, $today]);
                                        });
                                    })
                                    ->withCount('sports')->get();

                                foreach ($clubs_sports_7 as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_act7_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_act7_n++;
                                    }
                                }

                                $statistics['clubs_sports_7_1'] = $i_clubsport_act7_1;
                                $statistics['clubs_sports_7_n'] = $i_clubsport_act7_n;

                                break;

                            case "30Tage":
                                /** Club activity 30d */
                                $clubs_30_all = $clubs_30_all
                                    ->whereBetween('updated_at', [$past_30, $today])
                                    ->orWhere(function($query) use ($past_30, $today){
                                        $query->whereHas('posts', function($query2) use ($past_30, $today){
                                            $query2->whereBetween('post.created_at', [$past_30, $today]);
                                        });
                                    })
                                    ->get()->count();
                                $statistics['clubs_30_all'] = $clubs_30_all;

                                $clubs_30_accepted = $clubs_30_accepted
                                    ->whereHas('users')
                                    ->whereBetween('updated_at', [$past_30, $today])
                                    ->orWhere(function($query) use ($past_30, $today){
                                        $query->whereHas('posts', function($query2) use ($past_30, $today){
                                            $query2->whereBetween('post.created_at', [$past_30, $today]);
                                        });
                                    })
                                    ->get()->count();
                                $statistics['clubs_30_accepted'] = $clubs_30_accepted;

                                $clubs_sports_30 = $clubs_sports_30
                                    ->whereBetween('updated_at', [$past_30, $today])
                                    ->orWhere(function($query) use ($past_30, $today){
                                        $query->whereHas('posts', function($query2) use ($past_30, $today){
                                            $query2->whereBetween('post.created_at', [$past_30, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_30, $today]);
                                    })
                                    ->withCount('sports')->get();

                                foreach ($clubs_sports_30 as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_act30_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_act30_n++;
                                    }
                                }

                                $statistics['clubs_sports_30_1'] = $i_clubsport_act30_1;
                                $statistics['clubs_sports_30_n'] = $i_clubsport_act30_n;

                                break;

                            case "90Tage":
                                /** Club activity 90d */

                                $clubs_90_all = $clubs_90_all
                                    ->whereBetween('updated_at', [$past_90, $today])
                                    ->orWhere(function($query) use ($past_90, $today){
                                        $query->whereHas('posts', function($query2) use ($past_90, $today){
                                            $query2->whereBetween('post.created_at', [$past_90, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_90, $today]);
                                    })
                                    ->get()->count();
                                $statistics['clubs_90_all'] = $clubs_90_all;


                                $clubs_90_accepted = $clubs_90_accepted
                                    ->whereHas('users')
                                    ->whereBetween('updated_at', [$past_90, $today])
                                    ->orWhere(function($query) use ($past_90, $today){
                                        $query->whereHas('posts', function($query2) use ($past_90, $today){
                                            $query2->whereBetween('post.created_at', [$past_90, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_90, $today]);
                                    })
                                    ->get()->count();
                                $statistics['clubs_90_accepted'] = $clubs_90_accepted;

                                $clubs_sports_90 = $clubs_sports_90
                                    ->whereBetween('updated_at', [$past_90, $today])
                                    ->orWhere(function($query) use ($past_90, $today){
                                        $query->whereHas('posts', function($query2) use ($past_90, $today){
                                            $query2->whereBetween('post.created_at', [$past_90, $today]);
                                        });
                                        //$query->has('posts')->whereBetween('created_at', [$past_90, $today]);
                                    })
                                    ->withCount('sports')->get();

                                foreach ($clubs_sports_90 as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_act90_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_act90_n++;
                                    }
                                }

                                $statistics['clubs_sports_90_1'] = $i_clubsport_act90_1;
                                $statistics['clubs_sports_90_n'] = $i_clubsport_act90_n;

                                break;

                        }

                        break;

                    case 2:

                        switch ($subabschnitt_auswahl){

                            case "Logo":
                                /** Club intensity logo */
                                $clubs_logo_all = $clubs_logo_all
                                    ->whereNotNull('avatar_updated_at')->where('avatar_updated_at', 'NOT LIKE', '')
                                    ->get()->count();
                                $statistics['clubs_logo_all'] = $clubs_logo_all;

                                $clubs_logo_accepted = $clubs_logo_accepted
                                    ->whereHas('users')
                                    ->whereNotNull('avatar_updated_at')->where('avatar_updated_at', 'NOT LIKE', '')
                                    ->get()->count();
                                $statistics['clubs_logo_accepted'] = $clubs_logo_accepted;

                                $clubs_sports_logo = $clubs_sports_logo
                                    ->whereNotNull('avatar_updated_at')->where('avatar_updated_at', 'NOT LIKE', '')
                                    ->withCount('sports')->get();

                                foreach ($clubs_sports_logo as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_logo_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_logo_n++;
                                    }
                                }

                                $statistics['clubsport_logo_1'] = $i_clubsport_logo_1;
                                $statistics['clubsport_logo_n'] = $i_clubsport_logo_n;

                                break;


                            case "Header":
                                /** Club intensity header */
                                $clubs_header_all = $clubs_header_all
                                    ->whereNotNull('header_updated_at')->where('header_updated_at', 'NOT LIKE', '')
                                    ->get()->count();
                                $statistics['clubs_header_all'] = $clubs_header_all;

                                $clubs_header_accepted = $clubs_header_accepted
                                    ->whereHas('users')
                                    ->whereNotNull('header_updated_at')->where('header_updated_at', 'NOT LIKE', '')
                                    ->get()->count();
                                $statistics['clubs_header_accepted'] = $clubs_header_accepted;

                                $clubs_sports_header = $clubs_sports_header
                                    ->whereNotNull('header_updated_at')->where('header_updated_at', 'NOT LIKE', '')
                                    ->withCount('sports')->get();

                                foreach ($clubs_sports_header as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_header_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_header_n++;
                                    }
                                }

                                $statistics['clubsport_header_1'] = $i_clubsport_header_1;
                                $statistics['clubsport_header_n'] = $i_clubsport_header_n;

                                break;


                            case "Infos":
                                /** Club intensity info */
                                $clubs_infos_all = $clubs_infos_all
                                    ->whereNotNull('description')->where('description', 'NOT LIKE', '')
                                    ->get()->count();
                                $statistics['clubs_infos_all'] = $clubs_infos_all;

                                $clubs_info_accepted = $clubs_info_accepted
                                    ->whereHas('users')
                                    ->whereNotNull('description')->where('description', 'NOT LIKE', '')
                                    ->get()->count();
                                $statistics['clubs_infos_accepted'] = $clubs_info_accepted;

                                $clubs_sports_info = $clubs_sports_info
                                    ->whereNotNull('description')->where('description', 'NOT LIKE', '')
                                    ->withCount('sports')->get();

                                foreach ($clubs_sports_info as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_infos_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_infos_n++;
                                    }
                                }

                                $statistics['clubsport_infos_1'] = $i_clubsport_infos_1;
                                $statistics['clubsport_infos_n'] = $i_clubsport_infos_n;

                                break;


                            case "Teams":
                                /** Club intensity teams */
                                $clubs_teams_all = $clubs_teams_all
                                    ->has('teams')
                                    ->get()->count();
                                $statistics['clubs_teams_all'] = $clubs_teams_all;

                                $clubs_teams_accepted = $clubs_teams_accepted
                                    ->whereHas('users')
                                    ->has('teams')
                                    ->get()->count();
                                $statistics['clubs_teams_accepted'] = $clubs_teams_accepted;

                                $clubs_sports_teams = $clubs_sports_teams
                                    ->has('teams')
                                    ->withCount('sports')->get();

                                foreach ($clubs_sports_teams as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_teams_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_teams_n++;
                                    }
                                }

                                $statistics['clubsport_teams_1'] = $i_clubsport_teams_1;
                                $statistics['clubsport_teams_n'] = $i_clubsport_teams_n;

                                break;


                            case "Weblinks":
                                /** Club intensity weblinks */
                                $clubs_web_all = $clubs_web_all
                                    ->WhereNotNull('m_social_facebook')
                                    ->OrWhereNotNull('m_social_twitter')
                                    ->OrWhereNotNull('m_social_googleplus')
                                    ->OrWhereNotNull('m_social_youtube')
                                    ->OrWhereNotNull('m_social_instagram')
                                    ->OrWhereNotNull('m_social_flickr')
                                    ->get()->count();
                                $statistics['clubs_web_all'] = $clubs_web_all;

                                $clubs_web_accepted = $clubs_web_accepted
                                    ->whereHas('users')
                                    ->WhereNotNull('m_social_facebook')
                                    ->OrWhereNotNull('m_social_twitter')
                                    ->OrWhereNotNull('m_social_googleplus')
                                    ->OrWhereNotNull('m_social_youtube')
                                    ->OrWhereNotNull('m_social_instagram')
                                    ->OrWhereNotNull('m_social_flickr')
                                    ->has('teams')
                                    ->get()->count();
                                $statistics['clubs_web_accepted'] = $clubs_web_accepted;

                                $clubs_sports_web = $clubs_sports_web
                                    ->WhereNotNull('m_social_facebook')
                                    ->OrWhereNotNull('m_social_twitter')
                                    ->OrWhereNotNull('m_social_googleplus')
                                    ->OrWhereNotNull('m_social_youtube')
                                    ->OrWhereNotNull('m_social_instagram')
                                    ->OrWhereNotNull('m_social_flickr')
                                    ->has('teams')
                                    ->withCount('sports')->get();

                                foreach ($clubs_sports_web as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_web_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_web_n++;
                                    }
                                }

                                $statistics['clubsport_web_1'] = $i_clubsport_web_1;
                                $statistics['clubsport_web_n'] = $i_clubsport_web_n;

                                break;


                            case "Erfolge":
                                /** Club intensity success */
                                $clubs_success_all = $clubs_success_all
                                    ->has('achievements')
                                    ->get()->count();
                                $statistics['clubs_success_all'] = $clubs_success_all;

                                $clubs_success_accepted = $clubs_success_accepted
                                    ->whereHas('users')
                                    ->has('achievements')
                                    ->get()->count();
                                $statistics['clubs_success_accepted'] = $clubs_success_accepted;

                                $clubs_sports_success = $clubs_sports_success
                                    ->has('achievements')
                                    ->withCount('sports')->get();

                                foreach ($clubs_sports_success as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_success_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_success_n++;
                                    }
                                }

                                $statistics['clubsport_success_1'] = $i_clubsport_success_1;
                                $statistics['clubsport_success_n'] = $i_clubsport_success_n;

                                break;


                            case "Posts":
                                /** Club intensity posts */
                                $clubs_posts_all = $clubs_posts_all
                                    ->has('posts')
                                    ->get()->count();
                                $statistics['clubs_posts_all'] = $clubs_posts_all;

                                $clubs_posts_accepted = $clubs_posts_accepted
                                    ->whereHas('users')
                                    ->has('posts')
                                    ->get()->count();
                                $statistics['clubs_posts_accepted'] = $clubs_posts_accepted;

                                $clubs_sports_posts = $clubs_sports_posts
                                    ->has('posts')
                                    ->withCount('sports')->get();

                                foreach ($clubs_sports_posts as $club){
                                    if ($club->sports_count == 1){
                                        $i_clubsport_posts_1++;
                                    }
                                    if ($club->sports_count > 1){
                                        $i_clubsport_posts_n++;
                                    }
                                }

                                $statistics['clubsport_posts_1'] = $i_clubsport_posts_1;
                                $statistics['clubsport_posts_n'] = $i_clubsport_posts_n;

                                break;

                        }

                        break;

                }

            }

        }

        return view('admin.dashboard.club.clubs', [
            'statistics'           => $statistics,
            'abschnitt'            => $abschnitt,
            'abschnitt_auswahl'    => $abschnitt_auswahl,
            'subabschnitt_auswahl' => $subabschnitt_auswahl,
            'query'                => [
                'state' => $fed_state
            ]
        ]);

    }


}
