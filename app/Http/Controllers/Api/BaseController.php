<?php

namespace Vereinsleben\Http\Controllers\Api;

use Vereinsleben\City;
use Dingo\Api\Routing\Helpers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

abstract class BaseController extends Controller
{
    use Helpers, AuthorizesRequests;
}