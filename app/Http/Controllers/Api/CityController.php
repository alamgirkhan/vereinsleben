<?php

namespace Vereinsleben\Http\Controllers\Api;

use Validator;
use Illuminate\Http\Request;
use Vereinsleben\Http\Controllers\Api\BaseController;

use Vereinsleben\Http\Transformers\CityTransformer;

use Vereinsleben\City;

class CityController extends BaseController
{
    /**
     * Return cities.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $term = $request->input('search', '');
        $cities = City::where('name', 'LIKE', '%'.$term.'%')->take(100);
        return $this->response->collection($cities->get(), new CityTransformer);
    }
}