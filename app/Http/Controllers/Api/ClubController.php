<?php

namespace Vereinsleben\Http\Controllers\Api;

use Illuminate\Http\Request;

use Vereinsleben\Http\Requests;
use Vereinsleben\Http\Controllers\Controller;
use Auth;

use Vereinsleben\Club;
use Vereinsleben\Http\Transformers\ClubTransformer;
use Vereinsleben\Sport;
use Vereinsleben\City;

class ClubController extends BaseController
{
    /**
     * Return cities.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $cityquery = trim($request->input('city', ''));
        $distance = trim($request->input('distance', '50'));
        $keyword = trim($request->input('keyword', ''));
        $sportquery = trim($request->input('sport', ''));

        if ($cityquery != '') {
            $city = City::where('name', '=', $cityquery)->first();

            if ($city != null) {
                $clubsQuery = Club::closest($city->lat, $city->lon, $distance);
            } else {
                $clubsQuery = Club::whereRaw('1 = 2'); # TODO: ??
            }

            if ($keyword != '') {
                $clubsQuery = $clubsQuery->where('name', 'LIKE', '%' . $keyword . '%');
            }

            if ($sportquery != '') {
                $sport = Sport::where('title', '=', $sportquery)->first();

                if ($sport !== null) {
                    $clubsQuery = $clubsQuery->whereHas('sports', function ($query) use ($sport) {
                        $query->where('sport.id', '=', $sport->id);
                    });
                } else {
                    $clubsQuery = $clubsQuery->whereRaw('1 = 2'); # TODO: ??
                }
            }

            $clubs = $clubsQuery->get();
        } else {
            $clubs = [];
        }

        return $this->response->collection($clubs, new ClubTransformer);
    }

    /**
     * Return listing of clubs.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $club = new Club;
        $clubs = $club->get();
        if (policy($club)->index($clubs)) {
            return $clubs;
        }

        return abort(404);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Return clubs for a autocomplete field.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function autocomplete(Request $request)
    {
        $term = $request->input('search', '');
        $federal_state = $request->input('federal_state', '');
        if ($federal_state){
            $clubs = Club::published()->where('federal_state', $federal_state)->where('name', 'LIKE', '%' . $term . '%')->take(50);
        } else{
            $clubs = Club::published()->where('name', 'LIKE', '%' . $term . '%')->take(50);
        }
        return $this->response->collection($clubs->get(), new ClubTransformer);
    }

    public function clubList(Request $request)
    {
        $term = $request->input('search', '');

        $clubs = Club::where('name', 'LIKE', '%' . $term . '%')->take(50);
        return $this->response->collection($clubs->get(), new ClubTransformer);
    }
}
