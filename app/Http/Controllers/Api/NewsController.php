<?php

namespace Vereinsleben\Http\Controllers\Api;

use Validator;
use Illuminate\Http\Request;
use Vereinsleben\Http\Controllers\Api\BaseController;

use Vereinsleben\Http\Transformers\NewsTransformer;

use Vereinsleben\News;

class NewsController extends BaseController
{
    /**
     * Return sports.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $term = $request->input('search', '');
        $news = News::where('title', 'LIKE', '%'.$term.'%')->take(100);
        return $this->response->collection($news->get(), new NewsTransformer);
    }
}