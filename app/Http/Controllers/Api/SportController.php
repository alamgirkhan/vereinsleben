<?php

namespace Vereinsleben\Http\Controllers\Api;

use Validator;
use Illuminate\Http\Request;
use Vereinsleben\Http\Controllers\Api\BaseController;

use Vereinsleben\Http\Transformers\SportTransformer;

use Vereinsleben\Sport;

class SportController extends BaseController
{
    /**
     * Return sports.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $term = $request->input('search', '');
        $sports = Sport::where('title', 'LIKE', '%'.$term.'%')->take(100);
        return $this->response->collection($sports->get(), new SportTransformer);
    }
}