<?php

namespace Vereinsleben\Http\Controllers\Api;

use Illuminate\Http\Request;
use Vereinsleben\Http\Transformers\UserTransformer;
use Vereinsleben\User;

class UserController extends BaseController
{
    public function autocomplete(Request $request)
    {
        $term = $request->input('search', '');

        $users = User::where('username', 'LIKE', '%' . $term . '%')
                     ->orWhere('firstname', 'LIKE', '%' . $term . '%')
                     ->orWhere('lastname', 'LIKE', '%' . $term . '%')
                     ->orWhere('nickname', 'LIKE', '%' . $term . '%')
                     ->take(50);

        return $this->response->collection($users->get(), new UserTransformer);
    }
}
