<?php

namespace Vereinsleben\Http\Controllers\Auth;

use Carbon\Carbon;
use Vereinsleben\Content;
use Vereinsleben\User;
use Vereinsleben\Role;

use Validator;
use Mail;
use Illuminate\Http\Request;
use Storage;
use Auth;
use Vereinsleben\Http\Controllers\Controller;
use Vereinsleben\Http\Controllers\BaseController;
use Vereinsleben\Helpers\VenusNewsletter;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;


class AuthController extends BaseController
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */
    use  ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';
    protected $username = 'login';

    // password hasher
    protected $hasher = null;


    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(Request $request, HasherContract $hasher)
    {
        parent::__construct();

        $this->middleware('guest', ['except' => 'logout']);
        $this->hasher = $hasher;
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * Overwrite the Core method from the trait to allow authentication with both username and e-mail
     * AND make sure login is only possible when the e-mail address was verified.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    protected function getCredentials(Request $request)
    {
        $field = filter_var($request->input('login'), FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        $request->merge([$field => $request->input('login')]);

        $credentials = $request->only($field, 'password');
        $credentials['verified'] = 1;

        return $credentials;
    }

    /**
     * Handle a registration request for the application.
     *
     * Overwrites the Core trait method because we do NOT want to login the user, but ask him to verify his e-mail first.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()){
            $this->throwValidationException(
                $request, $validator
            );
        }

        #Auth::guard($this->getGuard())->login($this->create($request->all()));
        $this->create($request->all());

        $request->session()->flash('flash-info', 'Sie haben eine E-Mail mit einem Bestätigungslink erhalten. Bitte klicken Sie den Link in dieser E-Mail an und loggen sich anschließend ein.');

        return redirect('/login');
    }

    /**
     * E-Mail verification
     */
    public function registerVerify(Request $request, $token)
    {
        if (!$token){
            $request->session()->flash('flash-warning', 'Der Bestätigungslink war fehlerhaft. Bitte kopieren Sie den Link vollständig aus Ihrer E-Mail.');

            return redirect('/login');
        }

        $user = User::where('register_token', $token)->where('verified', 0)->first();

        if (!$user){
            $request->session()->flash('flash-warning', 'Der Bestätigungslink war fehlerhaft oder Sie haben Ihre E-Mail-Adresse bereits bestätigt.');

            return redirect('/login');
        }

        $optinDate = Carbon::now();

        $user->verified = 1;
        $user->OptinDate = $optinDate;
        $user->save();

//        $user->federal_state = 'Rheinland-Pfalz';

        if ($user){
            try{
                $newsletterConfig = config('newsletter');
                if (isset($newsletterConfig['auth_client_id']) && $newsletterConfig['auth_client_id'] !== null){
                    $venus = new VenusNewsletter($newsletterConfig);
                    $venus->subscribeUser($user);
                }
            } catch (\Exception $e){
                // fail silently and capture the exception; we do not want to bother the user with this.
                app('log')->error('Venus newsletter: Failed to register e-mail ' . $user->email . ', error: ' . $e->getMessage());
                //app('sentry')->captureException($e);
            }


        }

        // Auto login after email confirmation
        session(['wizard' => 1]);
		    return redirect('/login')->with('flash-success', 'Deine Email-Adresse wurde erfolgreich bestätigt!');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'firstname'            => 'required|string|max:200',
            'lastname'             => 'required|string|max:200',
            'username'             => [
                'required',
                'unique:user',
                'between:3,30',
                'regex:/^[a-zA-Z0-9-_.]+$/'
            ],
            'email'                => 'required|mogelmail|unique:user',
            'password'             => 'required|confirmed|min:6',
            'birthday-day'         => 'required|numeric|digits_between:1,2|min:1|max:31',
            'birthday-month'       => 'required|numeric|digits_between:1,2|min:1|max:12',
            'birthday-year'        => 'required|numeric|digits:4|min:1900|max:2099',
            'street'               => 'required|string|min:5|max:200',
            'zip'                  => 'required|numeric|digits_between:5,6',
            'house_number'         => 'required',
            'city'                 => 'required|string|max:200',
            'newsletter'           => 'boolean',
            'terms'                => 'required|boolean',
            'gender'               => 'required|string|max:8',
            'g-recaptcha-response' => 'recaptcha',
        ]);


        return $validator;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        $userRole = Role::where('constant', 'USER')->firstOrFail();

        $birthday = $data['birthday-year'] . '-' . str_pad($data['birthday-month'], 2, '0', STR_PAD_LEFT) . '-' .
            str_pad($data['birthday-day'], 2, '0', STR_PAD_LEFT) . ' 12:59:59';

        $fake_mail_file = 'fake-mail-list.txt';

        $data['has_fake_email'] = 0;
        if (Storage::exists($fake_mail_file)){
            $fake_mail_list = explode("\n", str_replace('""', '', Storage::get($fake_mail_file)));

            $domain = explode('@', $data['email'])[1];
            if (in_array($domain, $fake_mail_list)){
                $data['has_fake_email'] = 1;
            }
        }


        $user = User::create([
            'firstname'      => $data['firstname'],
            'lastname'       => $data['lastname'],
            'username'       => trim($data['username']),
            'email'          => $data['email'],
            'gender'         => $data['gender'],
            'street'         => $data['street'],
            'house_number'   => $data['house_number'],
            'zip'            => $data['zip'],
            'city'           => $data['city'],
            'birthday'       => $birthday,
            'newsletter'     => (isset($data['newsletter'])) ? $data['newsletter'] : 0,
            'messages'       => 1, # system messages are active per default
            'has_fake_email' => $data['has_fake_email'],
            'privacy'        => ((isset($data['privacy']) && $data['privacy'] === '1') ? 1 : 0),
        ]);

        // non-mass-assignable fields
        $user->register_token = $this->hasher->make(rand(0, time()));
        $user->password = $this->hasher->make(trim($data['password']));
        $user->verified = 0;
        $user->role_id = $userRole->id;
        $user->save();

        $imprint = Content::select('content')->where('slug', 'imprint')->first();

        Mail::send('auth.emails.register-confirmation', ['user' => $user, 'imprint' => $imprint], function($message) use ($user){
            $message->to($user->email, $user->username)->subject('vereinsleben.de - Registrierung bestätigen');
        });

        return $user;
    }
}
