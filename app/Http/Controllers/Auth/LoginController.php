<?php

namespace Vereinsleben\Http\Controllers\Auth;

use Auth;
use File;
use Hash;
use Image;
use Laravel\Socialite\Facades\Socialite;
use Vereinsleben\Http\Controllers\BaseController;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Vereinsleben\Http\Requests\Request;
use Vereinsleben\Http\Requests\UserProfileFormRequest;
use Vereinsleben\User;

class LoginController extends BaseController
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Check user's role and redirect user based on their role
     * @return
     */
    public function authenticated(\Illuminate\Http\Request $request)
    {
        $user = Auth::user();

        if ($user->verified == 0 || $user->has_fake_email == 1){
            Auth::logout();

            return redirect('/login')->with('flash-error', 'Ihre Emailadresse wurde noch nicht bestätigt');
        }

        $prev_url_form = str_replace(url('/'), '', $request->input('prevurl'));
        $prev_url = str_replace(url('/'), '', url()->previous());
        $curr_url = str_replace(url('/'), '', url()->current());


//        dd("Current: " . $curr_url . " | Previous: " . $prev_url . " | Previous Form: " . $prev_url_form);


        if (($prev_url == '/' || $prev_url == '' || $prev_url == '/login') &&
	        ($prev_url_form == '/' || $prev_url_form == '' || $prev_url_form == '/login' || strpos($prev_url_form, '/register/verify/') !== false)){

            return \Redirect::route('user.detail', $user->username);

        }


        if (($prev_url == '' || $prev_url == '/login') && ($prev_url_form !== '' && $prev_url_form !== '/login')){
            return redirect($prev_url_form)->withInput()->with('flash-success', 'Login erfolgreich!');
        }


        if ($prev_url !== '/login' && $prev_url !== '/' && $prev_url !== ''){
            return redirect($prev_url)->withInput()->with('flash-success', 'Login erfolgreich!');
        }


        //todo the streaming didn t work after lastnews function changed it nees a parameter
//        if ($user->isProfileCompleted()){
//            return \Redirect::route('user.detail', $user->username);
//        } else{
//            //return \Redirect::route('user.profile.edit', $user->username);
//            return \Redirect::route('user.detail', $user->username);
//        }
    }

    /**
     * Get the failed login response instance.
     */
    protected function sendFailedLoginResponse()
    {
        return redirect('/login')->with('flash-error', 'Benutzername/E-Mail oder Passwort ist falsch.');
    }


    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();

    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        $user = Socialite::driver('facebook')->user();


        if (User::where('email', '=', $user->email)->count() == 0){

            $userToAdd = new User;

            if ($user->getEmail() == null || $user->getEmail() == ''){

                //                return \Redirect::route('home')->with('flash-error', 'Sie müssen eine gültige E-Mail Adresse in Ihrem Facebookprofil angeben und uns die Berechtigung erteilen!');

                return Socialite::driver('facebook')->with(['auth_type' => 'rerequest'])->redirect();

            } else{

                $userToAdd->email = $user->getEmail();

                $completeName = array();

                $completeName = explode(' ', $user->getName());

                if (count($completeName) > 2){
                    $vorname = $completeName[0] . ' ' . $completeName[1];
                    $userToAdd->firstname = $vorname;
                    $lastname = $completeName[2];
                } else{
                    $userToAdd->firstname = $completeName[0];
                    $lastname = $completeName[1];
                }

                $userToAdd->lastname = $lastname;
                $userToAdd->nickname = $user->getNickname();
                $userToAdd->username = str_slug($user->name, '-');
                $userToAdd->role_id = 1;
                $userToAdd->verified = 1;
                $userToAdd->newsletter = 0;
                $userToAdd->fullname = $user->getName();
                //$userToAdd->birthday = $user->getBirthday();
                $userToAdd->avatar = $user->getAvatar();

                $userToAdd->save();
            }
        }

        $checkUser = User::where('email', '=', $user->email)->firstOrFail();

        Auth::login($checkUser);

        return \Redirect::route('user.detail', Auth::user()->username);
    }

}
