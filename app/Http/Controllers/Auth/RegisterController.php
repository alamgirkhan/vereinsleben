<?php

namespace Vereinsleben\Http\Controllers\Auth;

use Vereinsleben\Content;
use Vereinsleben\User;
use Vereinsleben\Role;
use Vereinsleben\State;
use Storage;
use Mail;
use Illuminate\Http\Request;
use Vereinsleben\Http\Controllers\Controller;
use Vereinsleben\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;

//class RegisterController extends Controller
class RegisterController extends BaseController
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/logout';

    // password hasher
    protected $hasher = null;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(HasherContract $hasher)
    {
        parent::__construct();

        $this->middleware('guest');
        $this->hasher = $hasher;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email'                => 'required|email|max:255|unique:user',
            'password'             => 'required|confirmed|min:6',
            'terms'                => 'required|boolean',
            'privacy'              => 'required|boolean',
            'g-recaptcha-response' => 'recaptcha',
        ]);
    }

    public function showRegistrationForm()
    {
        $state = State::pluck('name', 'id')->toArray();
        return view('auth.register', ['state' => $state]);
    }


    /**
     * Handle a registration request for the application.
     *
     * Overwrites the Core trait method because we do NOT want to login the user, but ask him to verify his e-mail first.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()){
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        #Auth::guard($this->getGuard())->login($this->create($request->all()));
        $user = $this->create($request->all());
        if ($user == null){
            $request->session()->flash('flash-warning', 'Mit einer E-Mail-Adresse dieses Anbieters ist leider keine Registrierung möglich. 
                                            Bitte benutzen Sie eine andere E-Mail-Adresse, um sich zu registrieren.');

            return redirect('/register');
        }
        $request->session()->flash('flash-info', 'Sie haben eine E-Mail mit einem Bestätigungslink erhalten. Bitte klicken Sie den Link in dieser E-Mail an und loggen sich anschließend ein.');

        return redirect('/login');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */

    protected function create(array $data)
    {
        $userRole = Role::where('constant', 'USER')->firstOrFail();
        $explode = explode('@', $data['email']);
        $username = $explode[0];

        $fake_mail_file = 'fake-mail-list.txt';

        $data['has_fake_email'] = 0;

        $domain = explode('@', $data['email'])[1];
        $domainName = explode('.',$domain)[0];
        if (strlen($domainName)<2)
        {
            $data['has_fake_email'] = 1;
        }

        if (Storage::exists($fake_mail_file)){
            $fake_mail_list = explode("\n", str_replace('""', '', Storage::get($fake_mail_file)));

            $domain = explode('@', $data['email'])[1];
            if (in_array($domain, $fake_mail_list)){
                $data['has_fake_email'] = 1;
            }
        }
        if ($data['has_fake_email'] == 1){
            return null;
        } else{
            $validator = Validator::make(['username' => $username], [
                'username' => 'required|unique:user',
            ]);

            if ($validator->fails()){
                $username = str_slug($username . '.' . rand(100, 999), '.');
            }

            $user = User::create([
//                'password'       => bcrypt($data['password']),
                'password'       => \Hash::make($data['password']),
                'username'       => $username,
                'email'          => $data['email'],
                'newsletter'     => (isset($data['newsletter'])) ? $data['newsletter'] : 0,
                'messages'       => 1, # system messages are active per default
                'has_fake_email' => $data['has_fake_email'],
                'privacy'        => ((isset($data['privacy']) && $data['privacy'] === '1') ? 1 : 0),
            ]);


            $user->register_token = $this->hasher->make(rand(0, time()));
            $user->verified = 0;
            $user->role_id = $userRole->id;
            $user->has_fake_email = $data['has_fake_email'];
            $user->save();

            $imprint = Content::select('content')->where('slug', 'imprint')->first();

            Mail::send('auth.emails.register-confirmation', ['user' => $user, 'imprint' => $imprint], function($message) use ($user){
                $message->to($user->email, $user->username)->subject("Deine Anmeldung bei vereinsleben.de");
            });

            return $user;
        }
    }
}