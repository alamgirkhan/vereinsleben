<?php

namespace Vereinsleben\Http\Controllers;

use Geocoder\Laravel\Facades\Geocoder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Image;
use Vereinsleben\Bandage;
use Vereinsleben\Http\Requests\BandageFormRequest;

use Auth;

use Validator;
use Vereinsleben\Role;
use Vereinsleben\Services\GeocoderHelper;
use Vereinsleben\SocialLink;
use Vereinsleben\Sport;
use Vereinsleben\User;

class BandageController extends BaseController
{
    const RECORDS_PER_PAGE = 12;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $bandages = Bandage::all();

        return view('admin.bandage.index', ['bandages' => $bandages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bandage = new Bandage();
        $bandages = Bandage::all();

        return view('admin.bandage.new', [
            'bandage'  => $bandage,
            'bandages' => $bandages
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BandageFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(BandageFormRequest $request)
    {
        $bandage = new Bandage([
            'name'         => $request['name'],
            'email'        => $request['email'],
            'founded'      => $request['founded'],
            'shorthand'    => $request['shorthand'],
            'street'       => $request['street'],
            'house_number' => $request['house_number'],
            'city'         => $request['city'],
            'zip'          => $request['zip'],
            'type'         => $request['type'],
            'region'       => $request['region']
        ]);
        $bandage->slug = str_slug($bandage->name, '-');

        $validator = Validator::make(['slug' => $bandage->slug], [
            'slug' => 'required|min:3|unique:bandages',
        ]);

        if ($validator->fails()){
            $bandage->slug = str_slug($bandage->name . '-' . rand(100, 999), '-');
        }

        $this->authorize($bandage);
//        try{
//            $geocodeHelper = new GeocoderHelper('Germany');
//            $geocodeHelper->setAddress($request['zip'], $request['city'], $request['street'], $request['house_number']);
//            $bandage->federal_state = $geocodeHelper->getFederalStateInGerman();
//            $bandage->lat = $geocodeHelper->getLat();
//            $bandage->lng = $geocodeHelper->getLon();
//        } catch (NoResultException $e){
//
//        }

        if ($request['parent_bandage'] != ""){
            $fatherBandage = Bandage::where('slug', $request['parent_bandage'])->firstOrFail();
            $fatherBandage->children()->save($bandage);
        } else{
            $bandage->save();
        }

        // store the user administrator
        $adminRole = Role::where('constant', 'BAND_ADMIN')->firstOrFail();
        $bandage->users()->save(Auth::user(), ['role_id' => $adminRole->id]);
	
		    // save sports relations
		    if (isset($request['sports']) && is_array($request['sports'])){
			    $this->storeSports($bandage, $request['sports']);
		    }
        
        return \Redirect::route('bandage.index')->with('flash-success', 'Dein Verband wurde erfolgreich angelegt!');
    }

    public function adminShow($slug){
        $bandage = Bandage::where('slug', $slug)->firstOrFail();
	      $admins = $bandage->administrators()->get();
        $bandages = Bandage::where('id', '!=', $bandage->id);
        return view('admin.bandage.detail', [
            'bandage'         => $bandage,
            'bandages'        => $bandages,
            'socialLinks'     => $bandage->socialLinks()->get()->keyBy('link_type'),
            'socialLinkTypes' => SocialLink::$linkTypes,
	          'admins'         => $admins
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $slug
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $slug)
    {
        $bandage = Bandage::where('slug', $slug)->firstOrFail();
        $this->checkProfileView($request, $bandage);
        $users = User::verified()
                     ->latestProfileCompleted()
                     ->paginate(20);
        $bandages = Bandage::where('id', '!=', $bandage->id);
        $posts = $bandage->posts()->latest('published_from')->latest('published_at');
        $events = $bandage->events()->latest('published_from')->latest('schedule_begin');
        $contacts = $bandage->contacts()->get();
        $contactPersons = $bandage->contactPersons()->get();
        
        $parentBandage = Bandage::where('id', '=', $bandage->parent_id)->first();

        return view('bandage.detail', [
            'bandage'         => $bandage,
            'bandages'        => $bandages,
            'socialLinks'     => $bandage->socialLinks()->get()->keyBy('link_type'),
            'posts'           => $posts,
            'events'          => $events,
            'socialLinkTypes' => SocialLink::$linkTypes,
            'contacts'        => $contacts,
            'users'           => $users,
            'parentBandage'   => $parentBandage,
	          'contactPersons' => $contactPersons
        ]);
    }

    /**
     *
     * @param $slug
     *
     * @return \Illuminate\Http\Response
     */
    public function profile($slug)
    {

        $user = Auth::user();
        $bandage = Bandage::where('slug', $slug)->firstOrFail();
        $role = new Role();
        $subscribeRoleId = $role->getByConstant(Role::BANDAGE_SUBSCRIBE)->id;
        $isSubscribed = $bandage->users()
                                ->wherePivot('user_id', $user->id)
                                ->wherePivot('role_id', $subscribeRoleId)
                                ->count() > 0;

        return view('bandage.profile.index', [
            'is_subscribed' => $isSubscribed,
            'bandage'       => $bandage
        ]);
    }

    /**
     * @param Request $request
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function updateSubscribe(Request $request, $slug)
    {
        $user = Auth::user();
        $bandage = Bandage::where('slug', $slug)->firstOrFail();
        $role = new Role();
        $subscribeRoleId = $role->getByConstant(Role::BANDAGE_SUBSCRIBE)->id;
        $isSubscribed = $bandage->users()
                                ->wherePivot('user_id', $user->id)
                                ->wherePivot('role_id', $subscribeRoleId)
                                ->count() > 0;
        $this->authorize('updateSubscribe', $bandage);
        if ($isSubscribed){
            $bandage->users()->newPivotStatementForId($user->id)->where('role_id', $subscribeRoleId)->delete();
        } else{
            $bandage->users()->save($user, ['role_id' => $subscribeRoleId]);
        }

        $isSubscribed = $bandage->users()
                                ->wherePivot('user_id', $user->id)
                                ->wherePivot('role_id', $subscribeRoleId)
                                ->count() > 0;

        return response()->json(['status' => $isSubscribed]);								
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BandageFormRequest $request
     * @param $slug
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(BandageFormRequest $request, $slug)
    {
        $bandage = Bandage::where('slug', $slug)->firstOrFail();
        $this->authorize('update', $bandage);

//        $this->cropProfileImagesFromRequest($request);

        $fillableAndPresentInRequest = collect($bandage->getFillable())->filter(function($item) use ($request){
            return $request->input($item, null) !== null || $request->hasFile($item);
        })->toArray();

        $fillableRequestData = $request->only($fillableAndPresentInRequest);

        // save social links
        if (isset($request['social_links']) && is_array($request['social_links'])){
            $this->storeSocialLinks($bandage, $request['social_links']);
        } elseif ($request['avatar'] || $request['header']){
            $bandage->update($fillableRequestData);
        } else{

//            try{
//                $geocodeHelper = new GeocoderHelper('Germany');
//                $geocodeHelper->setAddress($request['zip'], $request['city'], $request['street'], $request['house_number']);
//                $bandage->federal_state = $geocodeHelper->getFederalStateInGerman();
//                $bandage->lat = $geocodeHelper->getLat();
//                $bandage->lng = $geocodeHelper->getLon();
//            } catch (\Exception $e){
//            }

            if ($request['parent_bandage'] != ""){
                $fatherBandage = Bandage::where('slug', $request['parent_bandage'])->firstOrFail();
                $bandage->parent_id = $fatherBandage->id;
                $bandage->save();
                $fatherBandage->children()->where('slug', $slug)->update($fillableRequestData);
            } else{
                $bandage->parent_id = null;
                $bandage->update($fillableRequestData);
            }
        }
	
		    // save sports relations
		    if (isset($request['sports']) && is_array($request['sports'])){
			    $this->storeSports($bandage, $request['sports']);
		    }
        
        return \Redirect::route('bandage.index')->with('flash-success', 'Der Verband wurde erfolgreich angelegt! Du kannst jetzt weitere Details Deines Vereins bearbeiten.');
    }
	
	
		public function updateAtPublic(BandageFormRequest $request, $slug)
		{
			$bandage = Bandage::where('slug', $slug)->firstOrFail();
			$fillableAndPresentInRequest = collect($bandage->getFillable())->filter(function($item) use ($request){
				return $request->input($item, null) !== null || $request->hasFile($item);
			})->toArray();
			$fillableRequestData = $request->only($fillableAndPresentInRequest);
			// save social links
			if (isset($request['social_links']) && is_array($request['social_links'])){
				$this->storeSocialLinks($bandage, $request['social_links']);
			} elseif ($request['avatar'] || $request['header']){
				$bandage->update($fillableRequestData);
			} else{
			    /*
				if ($request['parent_bandage'] != ""){
					//$fatherBandage = Bandage::where('slug', $request['parent_bandage'])->firstOrFail();
					//$bandage->parent_id = $fatherBandage->id;
					$bandage->save();
					//$fatherBandage->children()->where('slug', $slug)->update($fillableRequestData);
				} else{
					//$bandage->parent_id = null;
					$bandage->update($fillableRequestData);
				}
				*/
				$bandage->update($fillableRequestData);
			}
			
			// save sports relations
			if (isset($request['sports']) && is_array($request['sports'])){
				$this->storeSports($bandage, $request['sports']);
			}
			return \Redirect::route('bandage.detail', $bandage->slug)->with('flash-success', 'Der Verband wurde erfolgreich angelegt! Du kannst jetzt weitere Details Deines Vereins bearbeiten.');
		}

    /**
     * Remove the specified resource from storage.
     * @param $slug
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */

    public function destroy($slug)
    {
        $bandage = Bandage::where('slug', $slug)->firstOrFail();
        $name = $bandage->name;
        $this->authorize('delete', $bandage);
        foreach ($bandage->children as $verbandChild){
            $verbandChild->parent_id = null;
            $verbandChild->save();
        }
        $bandage->delete();

        return \Redirect::route('bandage.index')->with('flash-success', "Der Verband $name wurde erfolgreich gelöscht!");
    }

    /**
     * @param Bandage $bandage
     * @param array $socialLinks
     * @return void
     */
    protected function storeSocialLinks(Bandage $bandage, array $socialLinks)
    {
        foreach ($socialLinks as $type => $url){
            if (in_array($type, array_keys(SocialLink::$linkTypes))){
                if (trim($url) != ''){
                    // TODO: check if the URL / hostname seems to be correct for the type
                    $link = $bandage->socialLinks()->where('link_type', $type)->first();
                    if (is_null($link)){
                        $link = new SocialLink();
                        $link->link_type = $type;
                    }

                    $link->url = $url;
                    if (!preg_match('/^https?:\/\//', $url)){
                        $link->url = "http://" . $link->url;
                    }
                    $bandage->socialLinks()->save($link);
                } else{
                    $link = $bandage->socialLinks()->where('link_type', $type)->first();
                    if (!is_null($link)){
                        $link->delete();
                    }
                }
            }
        }

    }

    /**
     * Check the request and crop the avatar/header images when the required data is present.
     *
     * Because the upload handling and scaling is handled by Stapler, we crop the original image in temporary
     * storage here before Stapler does its magic and does the rest.
     *
     * @return void
     */
    protected function cropProfileImagesFromRequest(Request $request)
    {
        if ($request->input('crop-props', false) !== false){
            $cropProps = json_decode($request->input('crop-props'), true);
            if ($cropProps !== null){
                foreach (['avatar'] as $type){
                    if ($request->hasFile($type) && $cropProps !== null && (float)$cropProps['scale'] > 0){
                        $image = Image::make($request->file($type)->getRealPath())->rotate((float)$cropProps['angle']);
                        #$image->widen(floor((int)$image->width() * (float)$cropProps['scale']));
                        #$image->crop((int)$cropProps['w'], (int)$cropProps['h'], (int)$cropProps['x'], (int)$cropProps['y']);
                        $image->crop( # keep the image in the original size, calculate the given params up with the given scale factor.
                            floor((int)$cropProps['w'] / (float)$cropProps['scale']),
                            floor((int)$cropProps['h'] / (float)$cropProps['scale']),
                            floor((int)$cropProps['x'] / (float)$cropProps['scale']),
                            floor((int)$cropProps['y'] / (float)$cropProps['scale'])
                        );
                        $image->save($request->file($type)->getRealPath());
                    }
                }
            }
        }
    }

    public function addContactPerson(Request $request, $slug)
    {
        $user = User::where('username', $request['user'])->firstOrFail();
        $bandage = Bandage::where('slug', $slug)->firstOrFail();
        $bandage->contacts()->save($user);

        return \Redirect::route('bandage.detail', $slug)->with('flash-success', 'Die Ansprechpartner wurde erfolgreich geändert!');
    }

    public function removeContactPerson($slug, $id)
    {
        $bandage = Bandage::where('slug', $slug)->firstOrFail();
        $bandage->contacts()->detach($id);

        return json_encode(['success' => true]);
    }

    /**
     * returns subscribers (^= bandage subscribers), rendered as partials for further js usage
     *
     * @param Request $request
     * @return mixed
     * @throws \Throwable
     */
    public function subscribersList(Request $request)
    {
        $partials = [];
        $username = '';
        $me = '';
        if (Auth::user()){
            $me = Auth::user();
            $username = $me->username;
        }

        $bandage = Bandage::findOrFail($request->input('bandage_id'));
        $searchString = $request->input('searchString');

        $users = $bandage->subcribles()
                         ->search($searchString)
                         ->paginate(self::RECORDS_PER_PAGE);

        foreach ($users as $user){
            if (policy($user)->show(Auth::user(), $user)){
                array_push($partials, view('user.partials.member-card', ['user' => $user, 'username' => $username, 'me' => $me])->render());
            }
        }

        $users->appends([
            'bandage_id'   => $request->input('bandage_id'),
            'searchString' => $request->input('searchString'),
        ]);

        array_push($partials, view('vendor.pagination.paginate', ['links' => $users->links()])->render());				

        $users->put('rendered', $partials);

        return $users;
    }

    /**
     * returns vereinen (^= bandage vereinen), rendered as partials for further js usage
     *
     * @param Request $request
     * @return mixed
     * @throws \Throwable
     */
    public function vereinenList(Request $request)
    {
        $partials = [];

        $bandage = Bandage::findOrFail($request->input('bandage_id'));
        $searchString = $request->input('searchString');

        $clubs = $bandage->clubs()
                         ->where('club.name', 'LIKE', '%' . $searchString . '%')
                         ->paginate(self::RECORDS_PER_PAGE);

        foreach ($clubs as $club){
            //if (policy($club)->show(Auth::club(), $club)){
            array_push($partials, view('club.partials.club-card', ['club' => $club])->render());
            //}
        }

        $clubs->appends([
            'bandage_id'   => $request->input('bandage_id'),
            'searchString' => $request->input('searchString'),			
        ]);

        array_push($partials, view('vendor.pagination.paginate', ['links' => $clubs->links()])->render());			

        $clubs->put('rendered', $partials);

        return $clubs;
    }
	
		public function deactivate($id)
		{
			
			$bandage = Bandage::find($id);
			$name = $bandage->name;
			
			if (Auth::user()->isAdmin()){
				
				if ($bandage->published == 1){
					$bandage->published = 0;
					$bandage->save();
					
					return back()->withInput()->with('flash-success', "Der Verband $name wurde deaktiviert!");
				} elseif ($bandage->published == 0){
					$bandage->published = 1;
					$bandage->save();
					
					return back()->withInput()->with('flash-success', "Der Verband $name wurde aktiviert!");
				}
			} else{
				abort(403);
			}
		}
	
	protected function storeSports(Bandage $bandage, array $sports)
	{
		$sportIds = [];
		$sportRules = [
			'title' => ['required', 'min:2', 'max:255'],
		];
		foreach ($sports as $key){
			if (is_numeric($key) && Sport::find($key) !== null){
				$sportIds[] = $key;
			} else{
				$validator = Validator::make(['title' => $key], $sportRules);
				
				if (!$validator->fails()){
					$sport = new Sport([
						'title' => $key
					]);
					$sport->bandage_id = $bandage->id;
					$this->authorize('store', $sport);
					$sport->save();
					$sportIds[] = $sport->id;
				}
			}
		}
		
		$bandage->sports()->sync($sportIds);
	}
	
	public function addAdmin(Request $request)
	{
		if (Auth::user()->isAdmin() && $request){
			
			$role = new Role();
			$user = User::where('id', $request['user_id'])->first();
			
			if(!isset($user)) {
				return \Redirect::to(URL::previous() . '#')->with('flash-error', 'Dieser Benutzer existiert nicht!');
			}
			
			$club = Bandage::where('id', $request['bandage_id'])->firstOrFail();
			$clubAdminId = $role->getByConstant(Role::BANDAGE_ADMIN)->id;
			
			DB::table('user_bandage_role')
				->insert(['user_id' => $request['user_id'], 'bandage_id' => $request['bandage_id'], 'role_id' => $clubAdminId]);
			
			return back()->withInput()->with('flash-success', "Dem Verein $club->name wurde der neue Admin $user->firstname $user->lastname zugeteilt!");
			
		} else{
			abort(403);
		}
	}
	
	public function deleteAdmin($bandage_id, $id)
	{
		if (Auth::user()->isAdmin()){
			
			$role = new Role();
			$clubAdminId = $role->getByConstant(Role::BANDAGE_ADMIN)->id;
			
			DB::table('user_bandage_role')
				->where('user_id', '=', $id)
				->where('bandage_id', '=', $bandage_id)
				->where('role_id', '=', $clubAdminId)
				->delete();
			
			return back()->withInput()->with('flash-warning', "Der Admin mit der ID: $id wurde entfernt!");
			
		} else{
			abort(403);
		}
	}
	
	public function updateColor(Request $request, $slug)
	{
		$bandage = Bandage::where('slug', $slug)->firstOrFail();
		$this->authorize('update', $bandage);
		
		if (isset($request['header_color'])){
			$bandage->header_color = $request['header_color'];
			$bandage->title_color = $request['title_color'];
			$bandage->save();
		}
	}
}
