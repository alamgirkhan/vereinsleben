<?php

namespace Vereinsleben\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

use Vereinsleben\Http\Requests;
use Vereinsleben\Http\Controllers\Controller;
use Vereinsleben\Http\Requests\BandageEventFormRequest;

use Vereinsleben\Event;
use Vereinsleben\Bandage;
use Vereinsleben\Image;

use Auth;
use Geocoder;
use Log;

class BandageEventController extends BaseController
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(BandageEventFormRequest $request)
    {
        $event = new Event;

        $event->title = $request['title'];
        $event->content = $request['content_raw'];
        $event->content_raw = $request['content_raw'];
        $event->street = $request['street'];
        $event->house_number = $request['house_number'];
        $event->zip = $request['zip'];
        $event->city = $request['city'];
        $event->schedule_begin = date('Y-m-d H:i:s', strtotime($request['schedule_begin']));
        $event->schedule_end = date('Y-m-d H:i:s', strtotime($request['schedule_end']));
        $event->published_from = !empty($request['published_from']) ? date('Y-m-d H:i:s', strtotime($request['published_from'])) : null;
        $event->published_to = !empty($request['published_to']) ? date('Y-m-d H:i:s', strtotime($request['published_to'])) : null;
        $event->bandage_id = $request['bandage_id'];
        $event->state = Event::STATE_NEW;

        try{
            $geocoder = Geocoder::geocode($event->fullAddress());

            if ($geocoder !== null
                && $geocoder->get()->first()->getCoordinates()->getLatitude() !== 0
                && $geocoder->get()->first()->getCoordinates()->getLongitude() !== 0){

                $event->lat = $geocoder->get()->first()->getCoordinates()->getLatitude();
                $event->lng = $geocoder->get()->first()->getCoordinates()->getLongitude();
            }
        } catch (\Exception $e){

            $event->lat = null;
            $event->lng = null;

            Log::warning($e->getMessage(), [
                'during' => 'event@store',
                'event'  => $event->id
            ]);
        }

        if (is_null($event->published_from)){
            $event->published_at = date("Y-m-d H:i:s");
        } else{
            $event->published_at = $event->published_from;
        }

        $this->authorize($event);

        $event->save();

        $images = $request->file('images');
        if ($images !== null && count($images) > 0){
            foreach ($images as $imageUploaded){
                if ($imageUploaded !== null){
                    $image = new Image(['picture' => $imageUploaded]);
                    $event->images()->save($image);
                }
            }
        }

        return \Redirect::route('bandage.detail', Bandage::find($event->bandage_id)->slug)->with('flash-success', 'Deine Veranstaltung wurde erfolgreich gespeichert');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Event::findOrFail($id);
        //$bandage = $event->club()->firstOrFail();
        $bandage = $event->firstOrFail();

        $this->authorize($event);

        $form = view('bandage.partials.edit.event', [
            'event'   => $event,
            'bandage' => $bandage,
        ]);

        return $form;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(BandageEventFormRequest $request, $id)
    {
        $event = Event::findOrFail($id);

        $event->title = $request['title'];
        $event->content = $request['content_raw'];
        $event->content_raw = $request['content_raw'];
        $event->street = $request['street'];
        $event->house_number = $request['house_number'];
        $event->zip = $request['zip'];
        $event->city = $request['city'];
        $event->schedule_begin = date('Y-m-d H:i:s', strtotime($request['schedule_begin']));
        $event->schedule_end = date('Y-m-d H:i:s', strtotime($request['schedule_end']));
        $event->published_from = !empty($request['published_from']) ? date('Y-m-d H:i:s', strtotime($request['published_from'])) : null;
        $event->published_to = !empty($request['published_to']) ? date('Y-m-d H:i:s', strtotime($request['published_to'])) : null;

        try{
            $geocoder = Geocoder::geocode($event->fullAddress());

            if ($geocoder !== null
                && $geocoder->get()->first()->getCoordinates()->getLatitude() !== 0
                && $geocoder->get()->first()->getCoordinates()->getLongitude() !== 0){

                $event->lat = $geocoder->get()->first()->getCoordinates()->getLatitude();
                $event->lng = $geocoder->get()->first()->getCoordinates()->getLongitude();
            }
        } catch (\Exception $e){

            $event->lat = null;
            $event->lng = null;

            Log::warning($e->getMessage(), [
                'during' => 'event@update',
                'event'  => $event->id
            ]);
        }

        if (!is_null($event->published_from)){
            $event->published_at = $event->published_from;
        }

        $this->authorize($event);

        $event->save();

        $images = $request->file('images');
        if ($images !== null && count($images) > 0){
            foreach ($images as $imageUploaded){
                if ($imageUploaded !== null){
                    $image = new Image(['picture' => $imageUploaded]);
                    $event->images()->save($image);
                }
            }
        }

        $imagesDelete = $request->input('delete-image');
        if ($imagesDelete !== null && count($imagesDelete) > 0){
            foreach ($imagesDelete as $imageDeleteId){
                if (is_numeric($imageDeleteId)){
                    $image = $event->images()->find($imageDeleteId);
                    if ($image !== null){
                        $image->delete();
                    }
                }
            }
        }

        return \Redirect::route('bandage.detail', Bandage::findOrFail($event->bandage_id)->slug)->with('flash-success', 'Der Eintrag wurde erfolgreich aktualisiert');
    }

    /**
     * Mark the specified event resource as deleted.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $event = Event::findOrFail($id);

        $this->authorize($event);

        $event->delete();

        return json_encode(['success' => true, 'item' => $event]);
    }

    public function latest($bandageSlug)
    {
        $bandage = Bandage::where('slug', $bandageSlug)->firstOrFail();
        $user = Auth::user();
        $events = $bandage->events()->latest('published_from')->latest('schedule_begin');

        //if (Auth::check() && (Auth::user()->isBandageOwner($bandage) || Auth::user()->isAdmin())) {
        if (Auth::check() && (Auth::user()->isAdmin() || Auth::user()->isBandageAdmin($bandage))){
            $events = $events->get();
        } else{
            $upcomingEvent = $bandage->events()->upcoming()->first();
            if (!is_null($upcomingEvent)){
                $events = $events->public()->get()->filter(function($event) use ($upcomingEvent){
                    return $event->id !== $upcomingEvent->id;
                });
            } else{
                $events = $events->public()->get();
            }
        }

        $perPage = 10;
        $currentPage = Paginator::resolveCurrentPage() ?: 1;
        $pagerIndex = ($currentPage * $perPage) - $perPage;
        $paginatedClients = collect($events)->slice($pagerIndex, $perPage);

        $events = new Paginator($paginatedClients, $events->count(), $perPage, $currentPage, [
            'path' => Paginator::resolveCurrentPath()]);

        $partials = [];

        foreach ($events as $event){
            if (policy($event)->show($user, $event)){
                array_push($partials, view('bandage.partials.event', [
                    'event'   => $event,
                    'bandage' => $bandage
                ])->render());
            }
        }

        $events->put('rendered', $partials);

        return $events;
    }

    /**
     * Return a single rendered event.
     */
    public function single($bandageSlug, $id)
    {
        $bandage = Bandage::where('slug', $bandageSlug)->firstOrFail();
        $user = Auth::user();

        //if (Auth::check() && (Auth::user()->isBandageOwner($bandage) || Auth::user()->isAdmin())) {
        if (Auth::check() && (Auth::user()->isAdmin() || Auth::user()->isBandageAdmin($bandage))){
            $event = $bandage->events()->findOrFail($id);
        } else{
            $event = $bandage->events()->public()->findOrFail($id);
        }

        if (policy($event)->show($user, $event)){
            return view('bandage.partials.event', ['event' => $event, 'bandage' => $bandage])->render();
        }

        abort(403);
    }
}
