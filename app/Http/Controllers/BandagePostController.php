<?php

namespace Vereinsleben\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Vereinsleben\Bandage;
use Vereinsleben\Http\Controllers\Controller;
use Vereinsleben\Http\Requests\BandagePostFormRequest;
use Vereinsleben\Image;
use Vereinsleben\Post;

class BandagePostController extends BaseController
{

    public function store(BandagePostFormRequest $request)
    {

        $post = new Post;

        $post->title = $request['title'];
        $post->content = $request['content_raw'];
        $post->content_raw = $request['content_raw'];
        $post->published_from = !empty($request['published_from']) ? date('Y-m-d H:i:s', strtotime($request['published_from'])) : null;
        $post->published_to = !empty($request['published_to']) ? date('Y-m-d H:i:s', strtotime($request['published_to'])) : null;
        $post->bandage_id = $request['bandage_id'];
        $post->state = Post::STATE_NEW;

        if (is_null($post->published_from)){
            $post->published_at = date("Y-m-d H:i:s");
        } else{
            $post->published_at = $post->published_from;
        }

        $this->authorize($post);

        $post->save();

        $images = $request->file('images');
//        if ($images !== null && count($images) > 0){
        if ($images !== null && $images->count() > 0){
            foreach ($images as $imageUploaded){
                if ($imageUploaded !== null){
                    $image = new Image(['picture' => $imageUploaded]);
                    $post->images()->save($image);
                }
            }
        }

        return \Redirect::route('bandage.detail',
            Bandage::find($post->bandage_id)->slug)->with('flash-success', 'Deine Neuigkeiten wurden erfolgreich gespeichert');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        $bandage = $post->bandage()->firstOrFail();

        $this->authorize($post);

        $form = view('bandage.partials.edit.post', [
            'post'    => $post,
            'bandage' => $bandage,
        ]);

        return $form;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function latest($bandageSlug)
    {
        $bandage = Bandage::where('slug', $bandageSlug)->firstOrFail();
        $user = Auth::user();
        $posts = $bandage->posts()->orderBy('published_from', 'desc')->orderBy('published_at', 'desc');

        if (Auth::check() && (Auth::user()->isAdmin())){
            $posts = $posts->paginate(10);
        } else{
            $posts = $posts->public()->paginate(10);
        }

        $partials = [];

        foreach ($posts as $post){
            if (policy($post)->show($user, $post)){
                array_push($partials, view('bandage.partials.post', [
                    'post'    => $post,
                    'bandage' => $bandage
                ])->render());
            }
        }

        $posts->put('rendered', $partials);

        return $posts;
    }

    /**
     * Return a single rendered post.
     */
    public function single($bandageSlug, $id)
    {
        $bandage = Bandage::where('slug', $bandageSlug)->firstOrFail();
        $user = Auth::user();

        if (Auth::user()->isAdmin()){
            $post = $bandage->posts()->findOrFail($id);
        } else{
            $post = $bandage->posts()->public()->findOrFail($id);
        }

        if (policy($post)->show($user, $post)){
            return view('bandage.partials.post', ['post' => $post, 'bandage' => $bandage])->render();
        }

        abort(403);
    }

    /**
     * Mark the specified post resource as deleted.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $post = Post::findOrFail($id);

        $this->authorize($post);

        $post->delete();

        return json_encode(['success' => true, 'item' => $post]);
    }
}
