<?php


namespace Vereinsleben\Http\Controllers;


use Auth;
use Carbon\Carbon;
use PhpParser\Node\Expr\Cast\Object_;
use Vereinsleben\Services\GeocoderHelper;
use Vereinsleben\State;
use Vereinsleben\View;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class BaseController extends Controller
{
    /**
     * BaseController constructor.
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->state = $this->getFederalState();
            $this->federal_states = State::all();
            $this->sessionState = session('federalState') !== null
                ? State::find(session('federalState'))
                : State::where('name', 'Rheinland-Pfalz')->firstOrFail();

            $this->federalState = Auth::user() ? Auth::user()->desiredState() : $this->sessionState;
            $this->isClubAdmin = Auth::user() ? Auth::user()->isAdministratorOfClub() : 0;
            $this->clubsWhereAdminCount = Auth::user() ? Auth::user()->clubsWhereAdmin()->count() : 0;
            $this->isAdmin = Auth::user() ? Auth::user()->isAdmin() : 0;

            view()->share('state', $this->state);
            view()->share('federal_states', $this->federal_states);
            view()->share('sessionState', $this->sessionState);
            view()->share('federalState', $this->federalState);
            view()->share('isClubAdmin', $this->isClubAdmin);
            view()->share('clubsWhereAdminCount', $this->clubsWhereAdminCount);
            view()->share('isAdmin', $this->isAdmin);

            return $next($request);
        });
    }

    /**
     * @param $items
     * @param $perPage
     * @return Paginator
     */
    function custom_paginate($items, $perPage)
    {
        $pageStart = request('page', 1);
        $offSet = ($pageStart * $perPage) - $perPage;
        $itemsForCurrentPage = $items->slice($offSet, $perPage);

        return new Paginator(
            $itemsForCurrentPage, $items->count(), $perPage,
            Paginator::resolveCurrentPage(),
            ['path' => Paginator::resolveCurrentPath()]
        );
    }

    protected function checkProfileView($request, $model)
    {
        $view = $model->views()->where('ip', $request->ip())->first();

        if ($view === null){
            $view = new View();
            $view->ip = $request->ip();
            $model->views()->save($view);
            $model->views = $model->views + 1;
            $model->save();
        } elseif ($view->updated_at < Carbon::today()){
            $model->views()->update(['updated_at' => Carbon::today()]);
            $model->views = $model->views + 1;
            $model->save();
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function checkFederalState($ipAddress, $user)
    {
//        $ipAddress = $ipAddress == "127.0.0.1" ? "212.7.163.49" : $ipAddress;
        //$geocoderHelper = new GeocoderHelper();
//        $geocoderHelper->setIpAddress($ipAddress);
        //$federalState = $geocoderHelper->getFederalStateInGerman();
        $federalState = 'Rheinland-Pfalz';

        if ($user == null){
            if (session('federalState') == null){
                $stateId = State::where('name', $federalState)->first()->id;
                session(['federalState' => $stateId]);
            }
        } else{
            $user = Auth::user();
            if ($user->desiredState() == null){
                $state = State::where('name', $user->federal_state)->first();
                if ($state === null){
                    $state = State::where('name', $federalState)->first();
                }
                $user->states()->save($state);
            }
        }
    }

    public function getFederalState()
    {

        if (Auth::user()){
            $state = Auth::user()->desiredState();
        } else{
            $state = State::find(session('federalState'));
        }

        return $state == null ? State::where('name', 'Rheinland-Pfalz')->first() : $state;

    }

}