<?php
	
	namespace Vereinsleben\Http\Controllers;
	
	use Illuminate\Http\Request;
	use Intervention\Image\Facades\Image;
	use Vereinsleben\Campaign;
	use Auth;
	use Vereinsleben\Option;
	use Vereinsleben\State;
	
	class CampaignController extends BaseController
	{
		public function list()
		{
			$campaigns = new Campaign;
			$campaigns = $campaigns->campaignList();
			
			$option = new Option;
			$bannerSpacingStream = +$option->get('banner_spacing_in_the_stream');
			
			return view('admin.campaign.index', compact(['campaigns', 'bannerSpacingStream']));
		}
		
		public function edit($id)
		{
			$campaign = new Campaign;
			$campaign = $campaign->getCampaignById($id);
			return view('admin.campaign.edit', compact('campaign'));
		}
		
		public function create(Request $request)
		{
			return view('admin.campaign.edit', ['campaign' => new Campaign(['state' => Campaign::STATE_DRAFT])]);
		}
		
		public function store(Request $request)
		{
			$errors = [];
			if (!$request->hasFile('banner_horizontal') && !$request->hasFile('banner_rechteck') && !$request->hasFile('banner_vertikal')) {
				$errors[] = "Werbemittel ist nicht leer";
			}
			
			if (!isset($request->federal_states)) {
				$errors[] = "Bitte mindestens ein Bundesland auswählen!";
			}
			
			$user = Auth::user();
			$newCampain = new Campaign;
			$newCampain->name = $request->name;
			$newCampain->customers_name = $request->customers_name;
			$newCampain->link = $request->link;
			$newCampain->description = $request->description;
			$newCampain->state = $request->state;
			$newCampain->user_id = $user->id;
			$newCampain->published_from = date('Y-m-d H:i:s', strtotime($request->published_from));
			$newCampain->published_to = date('Y-m-d H:i:s', strtotime($request->published_to));
			
			if ($request->hasFile('banner_horizontal')) {
				$banner_horizontal = $request->file('banner_horizontal');
				$banner_horizontal_width = getimagesize($banner_horizontal)[0];
				$banner_horizontal_height = getimagesize($banner_horizontal)[1];
				if ($banner_horizontal_width / $banner_horizontal_height < 2) {
					$errors[] = "Banner horizontal hat nicht die richtige Größe";
				} else {
					$filename = time() . '-' . str_slug($banner_horizontal->getClientOriginalName(), '-') . '.' . $banner_horizontal->getClientOriginalExtension();
                    $image_type = $banner_horizontal->getClientOriginalExtension();
				    if ($image_type == "gif")
                    {
                        move_uploaded_file($banner_horizontal, 'uploaded/campaign/' . $filename);

                    }else{
    					Image::make($banner_horizontal)->save(public_path('/uploaded/campaign/' . $filename));
                    }
					//Image::make($banner_horizontal)->save(public_path('/uploaded/campaign/' . $filename));
					$newCampain->banner_horizontal = $filename;
				}
			}
			
			if ($request->hasFile('banner_rechteck')) {
				$banner_rechteck = $request->file('banner_rechteck');
				$banner_rechteck_width = getimagesize($banner_rechteck)[0];
				$banner_rechteck_height = getimagesize($banner_rechteck)[1];
				if (($banner_rechteck_width / $banner_rechteck_height >= 2) || ($banner_rechteck_width / $banner_rechteck_height < 1)) {
					$errors[] = "Banner rechteck hat nicht die richtige Größe";
				} else {
					$filename2 = time() . '-' . str_slug($banner_rechteck->getClientOriginalName(), '-') . '.' . $banner_rechteck->getClientOriginalExtension();
                    $image_type = $banner_rechteck->getClientOriginalExtension();
				    if ($image_type == "gif")
                    {
                        move_uploaded_file($banner_rechteck, 'uploaded/campaign/' . $filename2);

                    }else{
    					Image::make($banner_rechteck)->save(public_path('/uploaded/campaign/' . $filename2));
                    }
					//Image::make($banner_rechteck)->save(public_path('/uploaded/campaign/' . $filename2));
					$newCampain->banner_rechteck = $filename2;
				}
			}
			
			if ($request->hasFile('banner_vertikal')) {
				$banner_vertikal = $request->file('banner_vertikal');
				$banner_vertikal_width = getimagesize($banner_vertikal)[0];
				$banner_vertikal_height = getimagesize($banner_vertikal)[1];
				if ($banner_vertikal_width / $banner_vertikal_height >= 1) {
					$errors[] = "Banner vertikal hat nicht die richtige Größe";
				} else {
					$filename3 = time() . '-' . str_slug($banner_vertikal->getClientOriginalName(), '-') . '.' . $banner_vertikal->getClientOriginalExtension();
				    $image_type = $banner_vertikal->getClientOriginalExtension();
				    if ($image_type == "gif")
                    {
                        move_uploaded_file($banner_vertikal, 'uploaded/campaign/' . $filename3);

                    }else{
    					Image::make($banner_vertikal)->save(public_path('/uploaded/campaign/' . $filename3));
                    }
					$newCampain->banner_vertikal = $filename3;
				}
			}
			
			if (count($errors) > 0) {
				return back()->withInput()->with('flash-error', $errors);
			}
			
			$newCampain->save();
			$this->saveFederalstate($request->federal_states, $newCampain);
			return redirect()->route('admin.campaigns')->with('flash-success', 'Die Kampagne wurde erfolgreich erstellt!');
		}
		
		public function update(Request $request, $id)
		{
			
			$errors = [];

			if (!$request->hasFile('banner_horizontal') && !$request->hasFile('banner_rechteck') && !$request->hasFile('banner_vertikal')
			&& ($request->banner_horizontal === '') && ($request->banner_rechteck == '') && ($request->banner_vertikal == '')) {
				$errors[] = "Werbemittel ist nicht leer";
			}
			
			if (!isset($request->federal_states)) {
				$errors[] = "Bitte mindestens ein Bundesland auswählen!";
			}
			
			$user = Auth::user();
			$editCampaign = Campaign::find($id);
			$editCampaign->name = $request->name;
			$editCampaign->customers_name = $request->customers_name;
			$editCampaign->link = $request->link;
			$editCampaign->description = $request->description;
			$editCampaign->state = $request->state;
			$editCampaign->user_id = $user->id;
			$editCampaign->published_from = date('Y-m-d H:i:s', strtotime($request->published_from));
			$editCampaign->published_to = date('Y-m-d H:i:s', strtotime($request->published_to));
			
			if ($request->hasFile('banner_horizontal')) {
				$banner_horizontal = $request->file('banner_horizontal');
				$banner_horizontal_width = getimagesize($banner_horizontal)[0];
				$banner_horizontal_height = getimagesize($banner_horizontal)[1];
				if ($banner_horizontal_width / $banner_horizontal_height < 2) {
					$errors[] = "Banner horizontal hat nicht die richtige Größe";
				} else {
					$filename = time() . '-' . str_slug($banner_horizontal->getClientOriginalName(), '-') . '.' . $banner_horizontal->getClientOriginalExtension();
				    $image_type = $banner_horizontal->getClientOriginalExtension();
				    if ($image_type == "gif")
                    {
                        move_uploaded_file($banner_horizontal, 'uploaded/campaign/' . $filename);

                    }else{
    					Image::make($banner_horizontal)->save(public_path('/uploaded/campaign/' . $filename));
                    }
					//Image::make($banner_horizontal)->save(public_path('/uploaded/campaign/' . $filename));
					$editCampaign->banner_horizontal = $filename;
				}
			}
			
			if ($request->hasFile('banner_rechteck')) {
				$banner_rechteck = $request->file('banner_rechteck');
				$banner_rechteck_width = getimagesize($banner_rechteck)[0];
				$banner_rechteck_height = getimagesize($banner_rechteck)[1];
				if (($banner_rechteck_width / $banner_rechteck_height >= 2) || ($banner_rechteck_width / $banner_rechteck_height < 1)) {
					$errors[] = "Banner rechteck hat nicht die richtige Größe";
				} else {
					$filename2 = time() . '-' . str_slug($banner_rechteck->getClientOriginalName(), '-') . '.' . $banner_rechteck->getClientOriginalExtension();
				    $image_type = $banner_rechteck->getClientOriginalExtension();
				    if ($image_type == "gif")
                    {
                        move_uploaded_file($banner_rechteck, 'uploaded/campaign/' . $filename2);

                    }else{
    					Image::make($banner_rechteck)->save(public_path('/uploaded/campaign/' . $filename2));
                    }
					//Image::make($banner_rechteck)->save(public_path('/uploaded/campaign/' . $filename2));
					$editCampaign->banner_rechteck = $filename2;
				}
			}
			
			if ($request->hasFile('banner_vertikal')) {
				$banner_vertikal = $request->file('banner_vertikal');
				$banner_vertikal_width = getimagesize($banner_vertikal)[0];
				$banner_vertikal_height = getimagesize($banner_vertikal)[1];
				if ($banner_vertikal_width / $banner_vertikal_height >= 1) {
					$errors[] = "Banner vertikal hat nicht die richtige Größe";
				} else {
					$filename3 = time() . '-' . str_slug($banner_vertikal->getClientOriginalName(), '-') . '.' . $banner_vertikal->getClientOriginalExtension();
				    $image_type = $banner_vertikal->getClientOriginalExtension();
				    if ($image_type == "gif")
                    {
                        move_uploaded_file($banner_vertikal, 'uploaded/campaign/' . $filename3);

                    }else{
    					Image::make($banner_vertikal)->save(public_path('/uploaded/campaign/' . $filename3));
                    }
					$editCampaign->banner_vertikal = $filename3;
				}
			}
			
			if (count($errors) > 0) {
				return back()->withInput()->with('flash-error', $errors);
			}
			
			$editCampaign->save();
			$this->saveFederalstate($request->federal_states, $editCampaign);
			return redirect()->route('admin.campaigns')->with('flash-success', 'Die Kampagne wurde erfolgreich aktualisiert!');
		}
		
		public function remove(Request $request, $id)
		{
			$campaign = Campaign::find($id);
			if (isset($campaign)) {
				$campaign->delete();
				return redirect()->route('admin.campaigns')->with('flash-success', 'Ihre Kampagne wurde erfolgreich gelöscht!');
			}
		}
		
		public function toggleVisibility(Request $request)
		{
			$campaign = Campaign::find($request->id);
			if ($request->ajax()) {
				$campaign->state = ($campaign->state !== "PUBLISHED") ? "PUBLISHED" : "HIDDEN";
				$campaign->save();
				return response()->json($campaign->state);
			} else {
				abort(403);
			}
		}
		
		public function updateBannerSpacingStream(Request $request) {
			$option = new Option;
			$optionUpdate = $option->updateOption('banner_spacing_in_the_stream', $request->banner_spacing_stream);
			if ($optionUpdate) {
				return back()->withInput()->with('flash-success', 'Erfolgreich aktualisiert');
			}
		}
		
		public function saveFederalstate($federalstates, $campaign)
		{
			$campaign->states()->detach();
			if ($federalstates != null){
				foreach ($federalstates as $stateId){
					if ($campaign->states()->where('state_id', $stateId)->first() === null){
						$state = State::find($stateId);
						$campaign->states()->save($state);
					}
				}
			}
		}
	}
