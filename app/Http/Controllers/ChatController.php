<?php
namespace Vereinsleben\Http\Controllers;

use Illuminate\Http\Request;
use Nahid\Talk\Conversations\Conversation;
use Nahid\Talk\Facades\Talk;
use Predis\Connection\ConnectionException;
use Vereinsleben\Club;
use Vereinsleben\Events\ChatEvent;
use Vereinsleben\User;
use Auth;

class ChatController extends BaseController
{

    protected $authUser;


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($username)
    {

        $user = Auth::user();

        if (Auth::check() && Auth::user()->clubsWhereAdmin()->count() > 0 && $username == 'index'){
            Talk::setAuthUserId($user);
            $users = $user->getFriends();
            $clubs = $user->clubsWhereAdmin()->get();
            $clubs_arr = $clubs->pluck('name', 'id')->toArray();
            return view('chat.index', ['user' => $user, 'chatuser' => '', 'users' => $users, 'clubs' => $clubs_arr]);
        }

        $chatuser = User::where('username', $username)->firstOrFail();
        if ($user->isFriendWith($chatuser)){
            Talk::setAuthUserId($user);
            $users = $user->getFriends();

            return view('chat.index', ['user' => $user, 'chatuser' => $chatuser, 'users' => $users, 'clubs' => '']);
        } else{
            return redirect('/')->with('flash-warning', 'Du musst eingeloggt sein und kannst nur mit deinen Freunden chatten!');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($username, Request $request)
    {
        $clubId = $request['clubs'];
        $body = $request->input('message');
        if ($clubId == ""){
            $userId = User::where('username', $username)->firstOrFail()->id;
            Talk::setAuthUserId(auth()->user()->id);
            if ($message = Talk::user(auth()->user()->id)->sendMessageByUserId($userId, $body)){
                broadcast(new ChatEvent($message))->toOthers();

                return response()->json(['status' => 'success', 'message' => $message], 200);
            }
        } else{
            $userIds = [];
            $club = Club::where('id', $clubId)->firstOrFail();
            Talk::setAuthUserId(auth()->user()->id);
            if ($request['members'] == 1 || $request['membfans']){
                foreach ($club->members as $member){
                    $userIds[] = $member->id;
                }
            }
            if ($request['fans'] == 1 || $request['membfans']){
                foreach ($club->fans as $fan){
                    $userIds[] = $fan->id;
                }
            }

            foreach ($userIds as $userIdToSend){
                if ($message = Talk::user(auth()->user()->id)->sendMessageByUserId($userIdToSend, $body)){
                    try{
                        broadcast(new ChatEvent($message))->toOthers();
                    } catch (ConnectionException $exception){
                        print ($exception->getMessage());
                    }
                }
            }
            return response()->json(['status' => 'success', 'message' => $message], 200);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function messages($username)
    {
        $userId = User::where('username', $username)->firstOrFail()->id;
        $conversationId = $this->getConversationId($userId);
        $conversations = Talk::user(auth()->user()->id)->getMessagesByUserId($userId, 0, 1000);

        foreach ($conversations->messages as $conversation){
            $conversation->message_date = date('d.m.Y - H:i', strtotime($conversation->created_at));
        }

        return response()->json(['messages' => $conversations, 'conversationId' => $conversationId]);
    }

    public function getConversationId($receiverId)
    {
        if ($conversationId = Talk::user(auth()->user()->id)->isConversationExists($receiverId)){

            return $conversationId;
        }

        $conversation = new Conversation();
        $conversation->user_one = $receiverId;
        $conversation->user_two = auth()->user()->id;
        $conversation->status = 1;

        $conversation->save();

        //$conversation = Talk::user(auth()->user()->id)->sendMessageByUserId($receiverId, '');
        return $conversation->id;
    }
}