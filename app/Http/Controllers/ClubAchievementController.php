<?php

namespace Vereinsleben\Http\Controllers;

use Illuminate\Http\Request;
use Vereinsleben\Http\Requests\ClubAchievementRequest;

use Vereinsleben\Club;
use Vereinsleben\ClubAchievement;
use Auth;

class ClubAchievementController extends BaseController
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Vereinsleben\Http\Requests\ClubAchievementRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClubAchievementRequest $request, $clubSlug)
    {

        // TODO: how do we know if day or month was omitted?
        $year = $request->get('year', null);
        $year = (trim($year) === '') ? null : $year;
        $month = $request->get('month', null);
        $month = (trim($month) === '') ? null : str_pad($month, 2, '0', STR_PAD_LEFT);
        $day = $request->get('day', null);
        $day = (trim($day) === '') ? null : str_pad($day, 2, '0', STR_PAD_LEFT);

        $date = $request->get('year') . '-' . (is_null($month) ? '01' : $month) . '-' .
            (is_null($day) ? '01' : $day) . ' 12:59:59';

        $club = Club::where('slug', $clubSlug)->firstOrFail();

        $clubAchievement = new ClubAchievement([
            'title' => $request['title'],
            'description' => $request['description'],
            'date' => $date,
            'year' => $year,
            'month' => $month,
            'day' => $day,
            'picture' => $request->file('achievement_picture'),
        ]);
        $clubAchievement->club_id = $club->id;

        $this->authorize($clubAchievement);

        $achievement = $club->achievements()->save($clubAchievement);

        return \Redirect::route('club.detail', [$club->slug, '#sportliche-erfolge'])->with('flash-success', 'Der Sportliche Erfolg wurde erfolgreich gespeichert');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClubAchievementRequest $request, $clubSlug, $id)
    {
        $clubAchievement = ClubAchievement::find($id);
        $fillableAndPresentInRequest = collect($clubAchievement->getFillable())->filter(function ($item) use ($request) {
            return $request->input($item, null) !== null || $request->hasFile($item);
        })->toArray();

        // TODO: should be after assignment and before save, but since the club_id is not (mass-assignment) fillable we simplify it here
        $this->authorize($clubAchievement);

        if ($request->hasFile('achievement_picture-' . $id)) {
             $clubAchievement->picture = $request->file('achievement_picture-' . $id);
        } else if ($request->input('delete-image') !== null && count($request->input('delete-image')) > 0) {
            $clubAchievement->picture = STAPLER_NULL;
        }

        $clubAchievement->update($request->only($fillableAndPresentInRequest));

        return \Redirect::route('club.detail', Club::find($clubAchievement->club_id)->slug)->with('flash-success', 'Der Erfolg wurde erfolgreich aktualisiert');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug, $id)
    {
        $clubAchievement = ClubAchievement::find($id);
        $club = Club::where('slug', $slug)->firstOrFail();

        $this->authorize($clubAchievement);

        $form = view('club.partials.edit.achievement', [
            'clubAchievement' => $clubAchievement,
            'club' => $club,
        ]);
        return $form;
    }

    /**
     * Mark the specified club achievement resource as deleted.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($slug, $id)
    {
        $clubAchievement = ClubAchievement::find($id);

        $this->authorize($clubAchievement);

        $clubAchievement->delete();

        return json_encode(['success' => true, 'item' => $clubAchievement]);
    }

    public function latest($clubSlug)
    {
        $club = Club::where('slug', $clubSlug)->firstOrFail();
        $latestAchievements = $club->achievements()->latest('year')->latest('month')->latest('day')->paginate(10);
        $user = Auth::user();

        $partials = [];

        foreach ($latestAchievements as $achievement) {
            if (policy($achievement)->show($user, $achievement)) {
                array_push($partials, view('club.partials.achievement', [
                    'achievement' => $achievement,
                    'club' => $club
                ])->render());
            }
        }

        $latestAchievements->put('rendered', $partials);

        return $latestAchievements;
    }
}
