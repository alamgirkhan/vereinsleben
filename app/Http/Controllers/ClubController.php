<?php

namespace Vereinsleben\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;
use League\Csv\Reader;
use Vereinsleben\Helpers\GeocodeHelper;
use Vereinsleben\Member;
use Vereinsleben\Services\GeocoderHelper;
use Vereinsleben\SportCareer;
use Vereinsleben\User;
use Vereinsleben\State;
use Illuminate\Http\Request;

use Mapper;
use Geocoder;


use Vereinsleben\Bandage;
use Vereinsleben\City;
use Vereinsleben\CityZipcode;
use Vereinsleben\Sport;
use Vereinsleben\ClubSport;
use Vereinsleben\Role;
use Vereinsleben\SocialLink;
use Vereinsleben\Department;
use Vereinsleben\Team;

use Vereinsleben\Club;
use Vereinsleben\Post;

use Vereinsleben\Http\Requests\ClubFormRequest;

use Auth;
use Validator;
use Image;

class ClubController extends BaseController
{
    const RECORDS_PER_PAGE = 12;

    /**
     * Club search.
     * @param Request $request
     * @return mixed
     * @throws \Cornford\Googlmapper\Facades\MapperException
     */
    public function search(Request $request)
    {
        $cityquery = trim($request->input('city', ''));
        $distance = trim($request->input('distance', '25'));
        $keyword = trim($request->input('keyword', ''));
        $sportquery = trim($request->input('sport', ''));

        /*
        // in case no city was entered (e.g. initial call) try to find out the location by FreeGeoIP request
        // (max. 10,000 queries per hour - we might cache the results someday)
        if ($cityquery === '') {
            try {
                $ipGeocode = Geocoder::geocode($request->ip());
            } catch (NoResultException $e) {
                $ipGeocode = null;
            }
            if ($ipGeocode !== null && $ipGeocode->getLatitude() !== 0 && $ipGeocode->getCity() !== '') {
                $cityquery = $ipGeocode->getCity();
            }
        }
        */

        if ($cityquery !== '' || $keyword !== '' || $sportquery !== ''){

            $clubsQuery = Club::published();
            $city = null;

            if ($cityquery != ''){
                if (is_numeric(trim($cityquery))){
                    $zipcode = CityZipcode::where('zip', '=', $cityquery)->first();
                    $city = (!is_null($zipcode)) ? $zipcode->city : null;
                } else{
                    $city = City::where('name', '=', $cityquery)->first();
                }

                if ($city != null){
                    $clubsQuery = $clubsQuery->closest($city->lat, $city->lon, $distance);
                } else{
                    $clubsQuery = $clubsQuery->whereRaw('1 = 2');
                }
            }

            if ($keyword != ''){
                $clubsQuery = $clubsQuery->where('name', 'LIKE', '%' . $keyword . '%');
            }

            if ($sportquery != ''){
                $sport = Sport::where('title', '=', $sportquery)->first();

                if ($sport !== null){
                    $clubsQuery = $clubsQuery->whereHas('sports', function($query) use ($sport){
                        $query->where('sport.id', '=', $sport->id);
                    });
                }
            }

            $clubs = $this->paginateSearchResults($request, $clubsQuery, 30);

            if ($city != null && $city->lat !== null && $city->lng !== null){
                $map = Mapper::map($city->lat, $city->lon, ['zoom' => 10, 'clusters' => ['size' => 4, 'center' => true, 'zoom' => 20], 'marker' => false]);
            } else{
                $map = Mapper::map('51.0778879', '5.9595168', ['zoom' => 5, 'marker' => false]); # default germany map
            }

            foreach ($clubs as $club){
                if ($club->lat !== null && $club->lng !== null){
                    Mapper::informationWindow($club->lat, $club->lng, link_to_route('club.detail', $club->name, $club->slug));
                }
            }

        } else{
            $clubs = null;

            $map = Mapper::map('51.0778879', '5.9595168', ['zoom' => 5, 'marker' => false]); # default germany map
        }

        return view('club.search')
            ->with('map', Mapper::render())
            ->with('results', $clubs)
            ->with('query', [
                    'city'     => $cityquery,
                    'distance' => $distance,
                    'keyword'  => $keyword,
                    'sport'    => $sportquery,
                ]
            );
    }

    /**
     * Create a club resource (show creation form).
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create(Request $request)
    {
        $club = new Club();
        $bandages = Bandage::all();

        $this->authorize($club);

        return view('club.new', [
            'club'     => $club,
            'bandages' => $bandages
        ]);
    }

    /**
     * Store the club resource in storage.
     * @param ClubFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(ClubFormRequest $request)
    {
        $bandage = Bandage::where('slug', $request['bandage'])->first();

//        $this->cropProfileImagesFromRequest($request);

        $club = new Club([
            'name'         => $request['name'],
            'street'       => $request['street'],
            'house_number' => $request['house_number'],
            'zip'          => $request['zip'],
            'city'         => $request['city'],
            'email'        => $request['email'],
            'founded'      => $request['founded'],
            'shorthand'    => $request['shorthand'],
            'member_count' => $request['member_count'] ?: null,
            'description'  => $request['description'],
            'about'        => $request['about'],
            'avatar'       => $request->file('avatar'),
            'header'       => $request->file('header'),
        ]);
        $club->slug = str_slug($club->name, '-');

        $validator = Validator::make(['slug' => $club->slug], [
            'slug' => 'required|min:3|unique:club',
        ]);

        if ($validator->fails()){
            $club->slug = str_slug($club->name . '-' . rand(100, 999), '-');
        }

        $club->published = 1;

        $this->authorize($club);

        // geo-code the address
//        try{
//            $geocode = Geocoder::geocode($club->fullAddressForGeocoding());
//        } catch (NoResultException $e){
//            $geocode = null;
//        }
//
//        if ($geocode !== null
//            && $geocode->get()->first()->getCoordinates()->getLatitude() !== 0
//            && $geocode->get()->first()->getCoordinates()->getLongitude() !== 0){
//
//            $club->lat = $geocode->get()->first()->getCoordinates()->getLatitude();
//            $club->lng = $geocode->get()->first()->getCoordinates()->getLongitude();
//            $geocodeHelper = new GeocoderHelper();
//            $geocodeHelper->setGeocode($geocode);
//            $club->federal_state = $geocodeHelper->getFederalStateInGerman();
//
//        }


        if ($bandage != null){
            $bandage->clubs()->save($club);
        } else{
            $club->save();
        }
        // store the owner
//        $ownerRole = Role::where('constant', 'CLUB_OWNER')->firstOrFail();
//        $club->users()->save(Auth::user(), ['role_id' => $ownerRole->id]);

        // save sports relations
        if (isset($request['sports']) && is_array($request['sports'])){
            $this->storeSports($club, $request['sports']);
        }

        // save social links
        if (isset($request['social_links']) && is_array($request['social_links'])){
            $this->storeSocialLinks($club, $request['social_links']);
        }

        return \Redirect::route('club.detail', $club->slug)->with('flash-success', 'Dein Verein wurde erfolgreich angelegt! Du kannst jetzt weitere Details Deines Vereins bearbeiten.');
    }


    public function storeClubContest(Request $request)
    {
        $response = [
            'success' => false,
            'data'    => [],
            'error'   => null,
        ];

        $clubstreet = isset($request['wcstreet']) ? $request['wcstreet'] : null;
        $clubhouseno = isset($request['wchouse_number']) ? $request['wchouse_number'] : null;
        $clubzip = isset($request['wczip']) ? $request['wczip'] : null;
        $clubcity = isset($request['wccity']) ? $request['wccity'] : null;

        try{
            // create a new club
            $newClub = new Club([
                'street'       => $clubstreet,
                'house_number' => $clubhouseno,
                'zip'          => $clubzip,
                'city'         => $clubcity,
                'name'         => $request['wcclub_id'],
                'published'    => 0,
                'unowned'      => 1,
                'avatar'       => $request->file('wcimage'),
            ]);

            if (isset($request['wcweb'])){
                $this->storeWebLink($newClub, $request['wcweb']);
            }

            $newClub->suggested_from = Auth::user()->id;

            $newClub->slug = str_slug($newClub->name, '-');

            $validator = Validator::make(['slug' => $newClub->slug], [
                'slug' => 'required|min:3|unique:club',
            ]);

            if ($validator->fails()){
                $newClub->slug = str_slug($newClub->name . '-' . rand(100, 999), '-');
            }

            $newClub->save();

            // save sports relations
            if (isset($request['wcsports'])){
                $this->storeSports($newClub, $request['sports']);
            }

        } catch (\Exception $ex){
            $response['success'] = false;
            $response['error'] = $ex->getMessage();
        }


        return back()->withInput()->with('flash-success', 'Dein vorgeschlagener Verein wurde angelegt.');
    }


    /**
     * Display a club resource.
     *
     * @param \Illuminate\Http\Request
     * @param string $slug
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $slug)
    {
        $middlename = '';
        $user = Auth::user();
        if ($user){
            $exp = explode(' ', $user->firstname);
            if (isset($exp[1])){
                $user->firstname = $exp[0];
                $user->middlename = $exp[1];
            }
        }

        $club = Club::where('slug', $slug)->firstOrFail();
        $this->checkProfileView($request, $club);
        $bandages = Bandage::all();
        $contacts = $club->contacts()->get();
		$clubid = $club->id;
        //Todo - check ob ->groupBy('title')gul
        $sports = $club->sports()->groupBy('title')->orderBy('title')->get();
        $sports->transform(function($sport) use ($clubid){
            $departmentarr = Department::where('sport_id', $sport->id)->where('club_id', $clubid)->first();
            if (isset($departmentarr)){
                $teams = Team::where('department_id', $departmentarr->id)->latest('created_at')->get();
                $contact = $departmentarr->contacts()->get()->first();
                $departmentexist = true;
            } else{
                $teams = '';
                $contact = '';
                $departmentexist = false;
            }

            $sport->contact = $contact;
            $sport->department = $departmentarr;
            $sport->teams = $teams;
            $sport->departmentexist = $departmentexist;

            return $sport;
        });

//        $usersarray = $club->users()->pluck('firstname', 'user_id')->toArray();
        $usersarray = $club->members()->pluck('fullname', 'user_id')->toArray();
        $posts = $club->posts()->latest();
        $events = $club->events()->latest('published_from')->latest('schedule_begin');

        $clubNetworkPosts = $club->posts()->latest();
        $clubNetworkEvents = $club->events()->latest('published_from')->latest('schedule_begin');
        $clubNetworkPosts = $clubNetworkPosts->public()->get();
        $clubNetworkEvents = $clubNetworkEvents->public()->get();

        $role = new Role;
        $users = User::verified()
                     ->latestProfileCompleted()
                     ->paginate(20);
        if (Auth::check() && (Auth::user()->isClubOwner($club) || Auth::user()->isAdmin())){
            $posts = $posts->get();
            $events = $events->get();
        } else{
            $posts = $posts->public()->get();
            $events = $events->public()->get();
        }

        $overview = $posts->merge($events)->sortByDesc('published_from')->sortByDesc('published_at');
        $networkOverview = $clubNetworkPosts->merge($clubNetworkEvents)->sortByDesc('published_from')->sortByDesc('published_at');
        $fanRoleId = $role->getByConstant(Role::CLUB_FAN)->id;
        $memberRoleId = $role->getByConstant(Role::CLUB_MEMBER)->id;
        $bandage = Bandage::where('id', '=', $club->bandage_id)->first();
        $state = State::pluck('name', 'id')->toArray();

        $previous_url = str_replace(url('/'), '', url()->previous());


        $foundzip = $this->findClubZipInCsv($club->zip);
        
	    $reviews = $club->reviews()->with('children')->get();
	    
	    $reviewAverage = 0;
	    $reviewTotal = 0;
	    if (isset($reviews) && count($reviews) > 0) {
	    	foreach ($reviews as $review) {
			    $reviewTotal = $reviewTotal + $review->rating;
		    }
		    $reviewAverage = ceil($reviewTotal / count($reviews));
	    } else {
		    $reviews = null;
	    }

        if (policy($club)->show(Auth::user(), $club)){ // important - since Auth::user() might be null but anonymous users may show, we need to call the policy directly
            return view('club.detail', [
                'club'          => $club,
                'clubs'         => Club::take(10)->get(),
                'posts'         => $posts,
                'events'        => $events,
                'trainingOrts'  => $club->traininsorts,
                'upcomingEvent' => $club->events()->upcoming()->first(),
                'achievements'       => $club->achievements()->latest('year')->latest('month')->latest('day')->simplePaginate(10),
                'teams'              => $club->teams()->orderBy('name', 'asc')->simplePaginate(10),
                'socialLinks'        => $club->socialLinks()->get()->keyBy('link_type'),
                'socialLinkTypes'    => SocialLink::$linkTypes,
                'completionProgress' => $club->getClubProfileCompletionProgress(),
                'overview'           => $overview,
                'networkOverview'    => $networkOverview,
                'members'       => $club->users()->wherePivot('role_id', $memberRoleId)->get(),
                'fans'          => $club->users()->wherePivot('role_id', $fanRoleId)->get(),
                'bandages'      => $bandages,
                'users'         => $users,
                'sports'        => $sports,
                'contacts'      => $contacts,
                'bandage'       => $bandage,
                'state'         => $state,
                'me'            => $user,
                'usersarray'    => $usersarray,
                'previousurl'   => $previous_url,
                'foundzip'      => $foundzip,
	              'reviews'       => $reviews,
	              'reviewAverage' => $reviewAverage
            ]);
        } else{
            abort(403);
        }
    }


    protected function findClubZipInCsv($zip)
    {
        $filename = 'vrm_plz.csv';
        $filename = storage_path('app' . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . $filename);

        $reader = Reader::createFromPath($filename, "r");
//        $reader->setNewline("\r\n");
//        $reader->getInputBOM(Reader::BOM_UTF8);

        $found = false;

        foreach ($reader->fetchAll() as $index => $row) {

            if($row[0] == $zip){
                $found = true;
            }
        }

        return $found;

    }
	
	
	public function update(Request $request)
	{
		$club = Club::where('slug', $request->slug)->firstOrFail();
		
		$this->authorize('update', $club);
		
		$fillableAndPresentInRequest = collect($club->getFillable())->filter(function($item) use ($request){
			return $request->input($item, null) !== null || $request->hasFile($item);
		})->toArray();
		
		if (trim($request['description']) == ''){
			$request['description'] = null;
		}
		
		if (trim($request['about']) == ''){
			$request['about'] = null;
		}
		
		$fillableRequestData = $request->only($fillableAndPresentInRequest);
		
		if ($request['avatar'] || $request['header']){
			$club->update($fillableRequestData);
		} else{
			
			$bandage = Bandage::where('slug', $request['bandage'])->first();
			if ($bandage != null){
				if ((!$club->bandage) || ($request['bandage'] != $club->bandage->slug)){
					$club->update(['bandage_id' => $bandage->id]);
				}
				$bandage->clubs()->where('slug', $request->slug)->update($fillableRequestData);
			} else{
				$club->update($fillableRequestData);
			}
		}
		
		// save social links
		if (isset($request['social_links']) && is_array($request['social_links'])){
			
			$this->storeSocialLinks($club, $request['social_links']);
		} elseif
		(isset($request['sports']) && is_array($request['sports'])){
			
			$this->storeSports($club, $request['sports']);
		}
		
		return \Redirect::to(URL::previous() . '#')->with('flash-success', 'Deine Änderungen wurden gespeichert!');
	}
    
    /**
     * Update the club resource in storage.
     *
     * @param ClubFormRequest $request
     * @param $slug
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function updatePatch(ClubFormRequest $request, $slug)
    {

        $club = Club::where('slug', $slug)->firstOrFail();

        $this->authorize('update', $club);


        $fillableAndPresentInRequest = collect($club->getFillable())->filter(function($item) use ($request){
            return $request->input($item, null) !== null || $request->hasFile($item);
        })->toArray();

        if (trim($request['description']) == ''){
            $request['description'] = null;
        }

        if (trim($request['about']) == ''){
            $request['about'] = null;
        }

        $fillableRequestData = $request->only($fillableAndPresentInRequest);

        // geo-code the address if changed
        if (isset($fillableRequestData['street']) ||
            isset($fillableRequestData['house_number']) ||
            isset($fillableRequestData['zip']) ||
            isset($fillableRequestData['city'])
        ){
//            try{
//                $geocode = Geocoder::geocode($club->fullAddressForGeocoding());
//            } catch (NoResultException $e){
//                $geocode = null;
//            }
//            if ($geocode !== null
//                && $geocode->get()->first()->getCoordinates()->getLatitude() !== 0
//                && $geocode->get()->first()->getCoordinates()->getLongitude() !== 0){
//
//                $club->lat = $geocode->get()->first()->getCoordinates()->getLatitude();
//                $club->lng = $geocode->get()->first()->getCoordinates()->getLongitude();
//                $geocodeHelper = new GeocoderHelper();
//                $geocodeHelper->setGeocode($geocode);
//                $club->federal_state = $geocodeHelper->getFederalStateInGerman();
//            }
        }

        if ($request['avatar'] || $request['header']){
            $club->update($fillableRequestData);
        } else{

            $bandage = Bandage::where('slug', $request['bandage'])->first();
            if ($bandage != null){
                if ((!$club->bandage) || ($request['bandage'] != $club->bandage->slug)){
                    $club->update(['bandage_id' => $bandage->id]);
                }
                $bandage->clubs()->where('slug', $slug)->update($fillableRequestData);
            } else{
                $club->update($fillableRequestData);
            }
        }

        // save social links
        if (isset($request['social_links']) && is_array($request['social_links'])){

            $this->storeSocialLinks($club, $request['social_links']);
        } elseif
        (isset($request['sports']) && is_array($request['sports'])){

            $this->storeSports($club, $request['sports']);
        }


        return \Redirect::route('club.detail', $club->slug)->with('flash-success', 'Deine Änderungen wurden gespeichert!');
    }

    /**
     * @param Request $request
     * @param $slug
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function updatePublished(Request $request, $slug)
    {
        $club = Club::where('slug', $slug)->firstOrFail();

        $this->authorize('update', $club);

        if (isset($request['published'])){
            $club->published = ($request['published'] == '1' || $request['published'] == 'true') ? true : false;
            $club->save();

            return response()->json($club);
        } else{
            abort(400);
        }
    }

    /**
     * @param Request $request
     * @param $slug
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function updateColor(Request $request, $slug)
    {
        $club = Club::where('slug', $slug)->firstOrFail();
        $this->authorize('update', $club);

        if (isset($request['header_color'])){
            $club->header_color = $request['header_color'];
            $club->title_color = $request['title_color'];
            $club->save();
        }
    }

    /**
     * @param Request $request
     * @param $slug
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function resetColor(Request $request, $slug)
    {
        $club = Club::where('slug', $slug)->firstOrFail();
        $this->authorize('update', $club);
        $club->header_color = '';
        $club->title_color = '';
        $club->save();

        return \Redirect::route('club.detail', $slug . '#informationen')->with('flash-success', 'Deine Änderungen wurden gespeichert!');
    }	

    /**
     * Make the current user a fan or un-fan him.
     *
     * @param Request $request
     * @param $slug
     * @return \Illuminate\Http\JsonResponse|void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function updateFanStatus(Request $request, $slug)
    {
        $role = new Role;
        $member = new Member();
        $user = Auth::user();
        $club = Club::where('slug', $slug)->firstOrFail();
        $this->authorize('updateFanStatus', $club);
        $fanRoleId = $role->getByConstant(Role::CLUB_FAN)->id;
        $fanId = $member->getByConstant(Member::FAN)->id;
        if (isset($request['status'])){
            $fanIntent = ($request['status'] == '1' || $request['status'] == 'true') ? true : false;
            //delete the membership first and then add a new membership with the new member status
            if ($fanIntent == true){
                if (!$user->isClubMember($club)){
                    $club->users()->save($user, ['role_id' => $fanRoleId, 'member_id' => $fanId]);
                }
            } else{
                /** @var TYPE_NAME $user */
                $club->users()->newPivotStatementForId($user->id)->where('role_id', $fanRoleId)->delete();
            }

            return response()->json(['status' => $user->isClubFan($club)]);
        } else{
            return abort(400);
        }
    }

    /**
     * Make the current user a member or un-memberize him.
     * @param Request $request
     * @param $slug
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function updateMemberStatus(Request $request, $slug)
    {
        $role = new Role;
        $user = Auth::user();
        $member = new Member();
        $club = Club::where('slug', $slug)->firstOrFail();
        $this->authorize('updateMemberStatus', $club);

        if (isset($request['status'])){
            $memberIntent = ($request['status'] == '1' || $request['status'] == 'true') ? true : false;

            $fanRoleId = $role->getByConstant(Role::CLUB_FAN)->id;
            $memberRoleId = $role->getByConstant(Role::CLUB_MEMBER)->id;
            $memberActiveId = $member->getByConstant(Member::ACTIVE)->id;
            $memberOldId = $member->getByConstant(Member::OLD)->id;

            $isAlreadyMember = $club->users()
                                    ->wherePivot('user_id', $user->id)
                                    ->wherePivot('role_id', $memberRoleId)
                                    ->count() > 0;
            $isAlreadyFan = $club->users()
                                 ->wherePivot('user_id', $user->id)
                                 ->wherePivot('role_id', $fanRoleId)
                                 ->count() > 0;

            if ($memberIntent === true){
                if ($isAlreadyMember){
                    $club->users()->newPivotStatementForId($user->id)->where('role_id', $memberRoleId)->delete();
                }
                if ($isAlreadyFan){
                    $club->users()->newPivotStatementForId($user->id)->where('role_id', $fanRoleId)->delete();
                }
                $club->users()->save($user, ['role_id' => $memberRoleId, 'member_id' => $memberActiveId]);

                // Synchronization with sport-vila
		            $sportCareer = new SportCareer();
		            $sportCareer->user_id = $user->id;
		            $sportCareer->club()->associate($club);
		            $sportCareer->save();
	            
                // add team relations
                $user->teams()->detach($user->teams()->wherePivot('club_id', $club->id)->pluck('team.id')->toArray());
                $teamIds = isset($request['teams']) && is_array($request['teams']) ? $request['teams'] : [];
                $teamIdsWithClubId = [];
                $sportIds = [];
                foreach ($teamIds as $teamId){
                    if (($team = Team::find($teamId)) && $team !== null && $team->club_id == $club->id){
                        $teamIdsWithClubId[$teamId] = ['club_id' => $club->id];
                        if ($team->sport !== null){
                            $sportIds[] = $team->sport->id;
                        }
                    }
                }
                $user->teams()->attach($teamIdsWithClubId);

                // add sport interests for the team sports to the user with 70% interest
                $sportIds = array_unique($sportIds);
                foreach ($sportIds as $sportId){
                    if (!$user->sports->contains($sportId)){
                        $user->sports()->attach($sportId, ['interest' => '70']);
                    }
                }
            } else{
                if ($isAlreadyMember === true){
                    $club->users()->newPivotStatementForId($user->id)->where('role_id', $memberRoleId)->delete();
		                // Synchronization with sport-vila
		                $sportCareer2 = SportCareer::whereClubId($club->id)->whereUserId($user->id);
		                if (isset($sportCareer2)) {
			                $sportCareer2->delete();
		                }
                }
                $club->users()->save($user, ['role_id' => $memberRoleId, 'member_id' => $memberOldId]);
                // drop team relations
                $user->teams()->detach($user->teams()->wherePivot('club_id', $club->id)->pluck('team.id')->toArray());
            }

            return response()->json(['status' => $user->isClubMember($club)]);
        } else{
            return abrot(400);
        }
    }

    /**
     * @param Request $request
     * @param $slug
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public
    function updateMember(Request $request, $slug)
    {
        $role = new Role;
        $user = User::where('username', $request['user'])->firstOrFail();
        $club = Club::where('slug', $slug)->firstOrFail();
        $this->authorize('updateMember', $club);
        $memberRoleId = $role->getByConstant(Role::CLUB_MEMBER)->id;
        $isAlreadyMember = $club->users()
                                ->wherePivot('user_id', $user->id)
                                ->wherePivot('role_id', $memberRoleId)
                                ->count() > 0;
        //delete the membership first and then add a new membership with the new member status
        if ($isAlreadyMember){
            $club->users()->newPivotStatementForId($user->id)->where('role_id', $memberRoleId)->delete();
        }

        $club->users()->save($user, ['role_id' => $memberRoleId, 'member_id' => $request['relation']]);

        //todo in verein profile when choose a user we have to see the relation directly ! schould be done with javascript
        return \Redirect::route('club.detail', $slug)->with('flash-success', 'Die Nutzer-beziehung wurde erfolgreich geändert!');
    }


    public function requestAdmin(Request $request, $clubSlug)
    {
        if ($request['username'] == null){
            return \Redirect::route('club.detail', $clubSlug)->with('flash-warning', 'Um einen Verein zu übernehmen, musst du eingeloggt sein!');
        } else{
            $role = new Role;
            $club = Club::where('slug', $clubSlug)->firstOrFail();
            $user = User::where('username', $request['username'])->firstOrFail();

            if ($user->firstname == '' || $user->lastname == '' || $user->federal_state == '' || $user->zip == '' || $user->city == ''){


                return \Redirect::route('club.detail', $clubSlug)->with('flash-warning', 'Um einen Verein zu übernehmen, musst du dein Profil vervollständigen!');

            }

            $this->authorize('requestAdmin', $club);
            $adminRequest = $role->getByConstant(Role::CLUB_ADMIN_REQUEST)->id;
            if (!$user->hasClubAdminRequested($club)){
                $userMember = $club->users()->wherePivot('user_id', $user->id)
                                   ->wherePivot('role_id', $adminRequest);
                if ($userMember->count() == 0){
                    $club->users()->save($user, ['role_id' => $adminRequest]);
                }
            }

            return \Redirect::route('club.detail', $clubSlug)->with('flash-success', 'Admin-anfrage wurde erfolgreich gesendet!');
        }
    }

    public function addContactPerson(Request $request, $slug)
    {
        $user = User::where('username', $request['user'])->firstOrFail();
        $club = Club::where('slug', $slug)->firstOrFail();
        $club->contacts()->save($user);

        return \Redirect::route('club.detail', $slug)->with('flash-success', 'Die Ansprechpartner wurde erfolgreich geändert!');
    }

    public
    function removeContactPerson($clubSlug, $id)
    {
        $club = Club::where('slug', $clubSlug)->firstOrFail();
        $club->contacts()->detach($id);

        return json_encode(['success' => true]);
    }

    /**
     * @return mixed
     * @throws \Throwable
     */
    public function latest()
    {
        $latestClubs = Club::published()->latest()->paginate(4);
        $user = Auth::user();

        $partials = [];

        foreach ($latestClubs as $club){
            if (policy($club)->show($user, $club)){
                array_push($partials, view('club.partials.teaser', ['club' => $club])->render());
            }
        }

        $latestClubs->put('rendered', $partials);

        return $latestClubs;
    }

    /**
     * Paginate search results. Workaround since we cannot use the paginate() method because there is currently a bug in
     * combination with HAVING queries.
     */
    protected function paginateSearchResults(Request $request, $query, $perPage = 30)
    {
        $curPage = $request->input('page'); // reads the query string, defaults to 1

        // clone the query to make 100% sure we don't have any overwriting
        $itemQuery = clone $query;

        // this does the sql limit/offset needed to get the correct subset of items
        $items = $itemQuery->forPage($curPage, $perPage)->get();

        // to be sure there are no clubs which may not be visible - filter the clubs by policy
        $user = Auth::user();
        $items = $items->filter(function($item) use ($user){
            return policy($item)->show($user, $item);
        });

        // manually run a query to select the total item count.
        // use a nested query, necessary because we have a HAVING query.
        $totalItems = \DB::table(\DB::raw("({$query->toSql()}) as sub"))
                         ->mergeBindings($query->getQuery())// you need to get underlying Query Builder
                         ->count();

        // make the paginator, which is the same as returned from paginate()
        // all() will return an array of models from the collection.
        $paginatedItems = new Paginator($items->all(), $totalItems, $perPage, Paginator::resolveCurrentPage(), [
            'path' => Paginator::resolveCurrentPath()]);

        // append the search queries to the paginator link
        $paginatedItems->appends([
            'city'     => $request->input('city'),
            'keyword'  => $request->input('keyword'),
            'sport'    => $request->input('sport'),
            'distance' => $request->input('distance'),
        ]);

        return $paginatedItems;
    }

    /**
     * Store social links from a request to a club.
     *
     * @return void
     */
    protected function storeSocialLinks(Club $club, array $socialLinks)
    {
        foreach ($socialLinks as $type => $url){
            if (in_array($type, array_keys(SocialLink::$linkTypes))){
                if (trim($url) != ''){
                    // TODO: check if the URL / hostname seems to be correct for the type
                    $link = $club->socialLinks()->where('link_type', $type)->first();
                    if (is_null($link)){
                        $link = new SocialLink();
                        $link->link_type = $type;
                    }

                    $link->url = $url;
                    if (!preg_match('/^https?:\/\//', $url)){
                        $link->url = "http://" . $link->url;
                    }
                    $club->socialLinks()->save($link);
                } else{
                    $link = $club->socialLinks()->where('link_type', $type)->first();
                    if (!is_null($link)){
                        $link->delete();
                    }
                }
            }
        }
    }


    protected function storeWebLink(Club $club, $socialLinks)
    {
        // TODO: check if the URL / hostname seems to be correct for the type
        $link = $club->socialLinks()->where('link_type', '=', 'web')->first();
        if (is_null($link)){
            $link = new SocialLink();
            $link->link_type = 'web';
        }

        $link->url = $socialLinks;
        if (!preg_match('/^https?:\/\//', $socialLinks)){
            $link->url = "http://" . $link->url;
        }
        $club->socialLinks()->save($link);
    }






    /**
     * Store sports from a request to a club.
     *
     * @param Club $club
     * @param array $sports
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    protected function storeSports(Club $club, array $sports)
    {
        $sportIds = [];
        $sportRules = [
            'title' => ['required', 'min:2', 'max:255'],
        ];
        foreach ($sports as $key){
            if (is_numeric($key) && Sport::find($key) !== null){ // TODO: check later if the sport is universal or belongs to the club?
                $sportIds[] = $key;
            } else{
                $validator = Validator::make(['title' => $key], $sportRules);

                if (!$validator->fails()){
                    $sport = new Sport([
                        'title' => $key
                    ]);
                    $sport->club_id = $club->id;
                    $this->authorize('store', $sport);
                    $sport->save();
                    $sportIds[] = $sport->id;
                }
            }
        }

        $club->sports()->sync($sportIds);
    }

    /**
     * Check the request and crop the avatar/header images when the required data is present.
     *
     * Because the upload handling and scaling is handled by Stapler, we crop the original image in temporary
     * storage here before Stapler does its magic and does the rest.
     *
     * @return void
     */
    protected function cropProfileImagesFromRequest(Request $request)
    {
        if ($request->input('crop-props', false) !== false){
            $cropProps = json_decode($request->input('crop-props'), true);
            if ($cropProps !== null){
                foreach (['avatar', 'header'] as $type){
                    if ($request->hasFile($type) && $cropProps !== null && (float)$cropProps['scale'] > 0){
                        $image = Image::make($request->file($type)->getRealPath())->rotate((float)$cropProps['angle']);
                        #$image->widen(floor((int)$image->width() * (float)$cropProps['scale']));
                        #$image->crop((int)$cropProps['w'], (int)$cropProps['h'], (int)$cropProps['x'], (int)$cropProps['y']);
                        $image->crop( # keep the image in the original size, calculate the given params up with the given scale factor.
                            floor((int)$cropProps['w'] / (float)$cropProps['scale']),
                            floor((int)$cropProps['h'] / (float)$cropProps['scale']),
                            floor((int)$cropProps['x'] / (float)$cropProps['scale']),
                            floor((int)$cropProps['y'] / (float)$cropProps['scale'])
                        );
                        $image->save($request->file($type)->getRealPath());
                    }
                }
            }
        }
    }

    /**
     * @param $clubSlug
     * @return Paginator
     * @throws \Throwable
     */
    public function eventPostOverview($clubSlug)
    {
        $club = Club::where('slug', $clubSlug)->firstOrFail();
        $user = Auth::user();

        $posts = $club->posts()->latest('published_from')->latest('published_at');
        $events = $club->events()->latest('published_from')->latest('schedule_begin');

        if (Auth::check() && (Auth::user()->isClubOwner($club) || Auth::user()->isAdmin())){
            $posts = $posts->get();
            $events = $events->get();
        } else{
            $upcomingEvent = $club->events()->upcoming()->first();
            $posts = $posts->public()->get();
            if (!is_null($upcomingEvent)){
                $events = $events->public()->get()->filter(function($event) use ($upcomingEvent){
                    return $event->id !== $upcomingEvent->id;
                });
            } else{
                $events = $events->public()->get();
            }
        }

        $overview = $posts->merge($events)->sortByDesc('published_from')->sortByDesc('published_at');

        $perPage = 15;
        $currentPage = Paginator::resolveCurrentPage() ?: 1;
        $pagerIndex = ($currentPage * $perPage) - $perPage;
        $paginatedClients = collect($overview)->slice($pagerIndex, $perPage);

        $paginator = new Paginator($paginatedClients, $overview->count(), $perPage, $currentPage, [
            'path' => Paginator::resolveCurrentPath()]);

        $partials = [];

        foreach ($paginator as $item){
            if (policy($item)->show($user, $item)){
                if (get_class($item) === 'Vereinsleben\Post'){
                    array_push($partials, view('club.partials.post', [
                        'post' => $item,
                        'club' => $club
                    ])->render());
                } elseif (get_class($item) === 'Vereinsleben\Event'){
                    array_push($partials, view('club.partials.event', [
                        'event' => $item,
                        'club'  => $club
                    ])->render());
                }
            }
        }

        $paginator->put('rendered', $partials);

        return $paginator;
    }


    public
    function networkPostOverview($clubSlug)
    {
        $club = Club::where('slug', $clubSlug)->firstOrFail();
        $user = Auth::user();
        $friends = $club->getFriends();
        $frinedsIds = [];

        foreach ($friends as $friend){
            $frinedsIds[] = $friend->id;
        }

        $posts = Post::whereIn('club_id', $frinedsIds)->latest('published_from')->latest('published_at');
        $posts = $posts->public()->get();
        $posts = $this->custom_paginate($posts, 1);
        $partials = [];
        foreach ($posts as $post){
            if (policy($post)->show($user, $post)){
                array_push($partials, view('club.partials.post', [
                    'post'    => $post,
                    'club'    => $club,
                    'network' => 1,
                ])->render());
            }
        }

        $posts->put('rendered', $partials);

        /*$perPage = 15;
        $currentPage = Paginator::resolveCurrentPage() ?: 1;
        $pagerIndex = ($currentPage * $perPage) - $perPage;
        $paginatedClients = collect($posts)->slice($pagerIndex, $perPage);

        $paginator = new Paginator($paginatedClients, $posts->count(), $perPage, $currentPage, [
            'path' => Paginator::resolveCurrentPath()]);

        $partials = [];

        foreach ($paginator as $item){
            if (policy($item)->show($user, $item)){
                array_push($partials, view('club.partials.post', [
                    'post' => $item,
                    'club' => $club
                ])->render());
            }
        }

        $paginator->put('rendered', $partials);
        */

        return $posts;
    }

    /**
     * returns users (^= club member|fan), rendered as partials for further js usage
     *
     * @param Request $request
     * @return mixed
     * @throws \Throwable
     */
    public
    function userList(Request $request)
    {
        $partials = [];
        $username = '';
        $me = '';
        if (Auth::user()){
            $me = Auth::user();
            $username = $me->username;
        }

        $role = new Role;
        $club = Club::findOrFail($request->input('club_id'));
        $users = $club->users()->verified();

        $userRequest = $request->input('user');
        $searchString = $request->input('searchString');

        switch ($userRequest){
            case 'member':
                $users = $club->members()->search($searchString)->paginate(self::RECORDS_PER_PAGE);
                break;
            case 'old_member':
                $users = $club->oldMembers()->search($searchString)->paginate(self::RECORDS_PER_PAGE);
                break;
            case 'fan':
                $users = $users->wherePivot('role_id', $role->getByConstant(Role::CLUB_FAN)->id)
                               ->search($searchString)->paginate(self::RECORDS_PER_PAGE);
                break;

            default:
                $users = $users->get();
        }

        foreach ($users as $user){
            if (policy($user)->show(Auth::user(), $user)){
                array_push($partials, view('user.partials.member-card', ['user' => $user, 'username' => $username, 'me' => $me])->render());
            }
        }

        $users->appends([
            'club_id'      => $request->input('club_id'),
            'user'         => $request->input('user'),
            'searchString' => $request->input('searchString'),
        ]);

        array_push($partials, view('vendor.pagination.paginate', ['links' => $users->links()])->render());

        $users->put('rendered', $partials);

        return $users;
    }


    /**
     * returns clubs (^= club freinds), rendered as partials for further js usage
     *
     * @param Request $request
     * @return mixed
     * @throws \Throwable
     */
    public
    function friendList(Request $request)
    {
        $partials = [];

        $club = Club::findOrFail($request->input('club_id'));
        $searchString = strtolower($request->input('searchString'));

        if ($searchString !== null & $searchString != ""){
            $clubs = $club->getFriends();
            $clubs = $clubs->filter(function($item) use ($searchString){
                return strpos($item->name, $searchString);
            });
            $clubs = $this->custom_paginate($clubs, self::RECORDS_PER_PAGE);
        } else{
            $clubs = $club->getFriends($perPage = self::RECORDS_PER_PAGE);
        }

        foreach ($clubs as $club){
            //if (policy($user)->show(Auth::user(), $user)){
            array_push($partials, view('club.partials.club-card', ['club' => $club])->render());
            //}
        }

        $clubs->appends([
            'club_id'      => $request->input('club_id'),
            'searchString' => $request->input('searchString'),
        ]);

        array_push($partials, view('vendor.pagination.paginate', ['links' => $clubs->links()])->render());

        $clubs->put('rendered', $partials);

        return $clubs;
    }

    /**
     * Mark the specified club resource as deleted.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse|void
     * @throws \Exception
     */
    public
    function delete($id)
    {
        $club = Club::findOrFail($id);

        if (policy($club)->delete(Auth::user(), $club)){
            $status = $club->delete();

            return response()->json($status);
        }

        return abort(404);

    }

    /**
     * claim an unowned club
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public
    function claim($id)
    {
        if (!Auth::check()){
            return redirect('/register');
        }

        $role = new Role;
        $user = Auth::user();
        $club = Club::where('id', $id)->where('unowned', 1)->firstOrFail();

        $club->users()->attach($user->id, ['role_id' => $role->getByConstant(Role::CLUB_OWNER)->id]);
        $club->update(
            ['unowned' => 0]
        );

        return redirect()->route('club.detail', $club->slug)->with('flash-success', 'Herzlichen Glückwunsch! Du bist jetzt der Vereinseigentümer.');
    }


    /**
     * @param $senderClubSlug
     * @param $recipientClubSlug
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public
    function createFriendship($senderClubSlug, $recipientClubSlug)
    {
        $relation = Input::get('relation');

        $senderClub = Club::where('slug', $senderClubSlug)->firstOrFail();
        $this->authorize('createFriendship', $senderClub);

        $recipientClub = Club::where('slug', $recipientClubSlug)->firstOrFail();
        switch ($relation){
            case 'send-friend-request':
                $senderClub->befriend($recipientClub);
                break;
            case 'accept':
                $senderClub->acceptFriendRequest($recipientClub);
                break;

            case 'deny':
                $senderClub->denyFriendRequest($recipientClub);
                break;

        }

        return \Redirect::route('club.detail', $senderClubSlug);
    }

    /**
     * @param $clubSlug
     * @param $friendsClubSlug
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public
    function editFriendship($clubSlug, $friendsClubSlug)
    {
        $relation = Input::get('relation');

        $club = Club::where('slug', $clubSlug)->firstOrFail();
        $this->authorize('updateFriendship', $club);
        $friendClub = Club::where('slug', $friendsClubSlug)->firstOrFail();
        switch ($relation){
            case 'remove':
                $club->unfriend($friendClub);
                break;
            case 'block':
                $club->blockFriend($friendClub);
                break;

            case 'unblock':
                $club->unblockFriend($friendClub);
                break;

        }

        return \Redirect::route('club.detail', $clubSlug);
    }


}
