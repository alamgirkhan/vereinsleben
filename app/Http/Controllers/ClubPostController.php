<?php

namespace Vereinsleben\Http\Controllers;

use Illuminate\Http\Request;

use League\Csv\Reader;
use Vereinsleben\Http\Requests;
use Vereinsleben\Http\Requests\ClubPostFormRequest;

use Vereinsleben\Post;
use Vereinsleben\Club;
use Vereinsleben\Image;
use Auth;

class ClubPostController extends BaseController
{
    /**
     * @param ClubPostFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(ClubPostFormRequest $request)
    {
        $post = new Post;

        $post->title = $request['title'];
        $post->content = $request['content_raw'];
        $post->content_raw = $request['content_raw'];
        $post->published_from = !empty($request['published_from']) ? date('Y-m-d H:i:s', strtotime($request['published_from'])) : null;
        $post->published_to = !empty($request['published_to']) ? date('Y-m-d H:i:s', strtotime($request['published_to'])) : null;
        $post->club_id = $request['club_id'];
        $post->state = Post::STATE_NEW;

        if (is_null($post->published_from)) {
            $post->published_at = date("Y-m-d H:i:s");
        } else {
            $post->published_at = $post->published_from;
        }

        $this->authorize($post);

        $post->save();

        $images = $request->file('images');
        if ($images !== null && count($images) > 0) {
            foreach ($images as $imageUploaded) {
                if ($imageUploaded !== null) {
                    $image = new Image(['picture' => $imageUploaded]);
                    $post->images()->save($image);
                }
            }
        }

        return \Redirect::route('club.detail', Club::find($post->club_id)->slug)->with('flash-success', 'Deine Neuigkeiten wurden erfolgreich gespeichert');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        $club = $post->club()->firstOrFail();

        $this->authorize($post);

        $found = $this->findZipInCsv($club->zip);

        $form = view('club.partials.edit.post', [
            'post' => $post,
            'club' => $club,
            'found' => $found,
        ]);
        return $form;
    }

    /*Specification from Alex to add VRM ZIP List and show VRM Logo with text*/
    protected function findZipInCsv($zip)
    {
        $filename = 'vrm_plz.csv';
        $filename = storage_path('app' . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . $filename);

        $reader = Reader::createFromPath($filename, "r");
//        $reader->setNewline("\r\n");
//        $reader->getInputBOM(Reader::BOM_UTF8);

        $found = false;

        foreach ($reader->fetchAll() as $index => $row) {

            if($row[0] == $zip){
                $found = true;
            }
        }

        return $found;

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClubPostFormRequest $request, $id)
    {
        $post = Post::findOrFail($id);

        $post->title = $request['title'];
        $post->content = $request['content_raw'];
        $post->content_raw = $request['content_raw'];
        $post->published_from = !empty($request['published_from']) ? date('Y-m-d H:i:s', strtotime($request['published_from'])) : null;
        $post->published_to = !empty($request['published_to']) ? date('Y-m-d H:i:s', strtotime($request['published_to'])) : null;

        if (!is_null($post->published_from)) {
            $post->published_at = $post->published_from;
        }

        $this->authorize($post);

        $post->save();

        $images = $request->file('images');
        if ($images !== null && count($images) > 0) {
            foreach ($images as $imageUploaded) {
                if ($imageUploaded !== null) {
                    $image = new Image(['picture' => $imageUploaded]);
                    $post->images()->save($image);
                }
            }
        }

        $imagesDelete = $request->input('delete-image');
        if ($imagesDelete !== null && count($imagesDelete) > 0) {
            foreach ($imagesDelete as $imageDeleteId) {
                if (is_numeric($imageDeleteId)) {
                    $image = $post->images()->find($imageDeleteId);
                    if ($image !== null) {
                        $image->delete();
                    }
                }
            }
        }

     
	    return back()->withInput()->with('flash-success', 'Der Eintrag wurde erfolgreich aktualisiert');
//        return \Redirect::route('club.detail', Club::findOrFail($post->club_id)->slug)->with('flash-success', 'Der Eintrag wurde erfolgreich aktualisiert');
    }

    /**
     * Mark the specified post resource as deleted.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $post = Post::findOrFail($id);

        $this->authorize($post);

        $post->delete();

        return json_encode(['success' => true, 'item' => $post]);
    }

    public function latest($clubSlug)
    {
        $club = Club::where('slug', $clubSlug)->firstOrFail();
        $user = Auth::user();
        $posts = $club->posts()->orderBy('created_at', 'desc');

        if (Auth::check() && (Auth::user()->isClubOwner($club) || Auth::user()->isAdmin())) {
            $posts = $posts->paginate(10);
        } else {
            $posts = $posts->public()->paginate(10);
        }

        $partials = [];

        foreach ($posts as $post) {
            if (policy($post)->show($user, $post)) {
                array_push($partials, view('club.partials.post', [
                    'post'    => $post,
                    'club'    => $club,
                    'network' => 0,
                ])->render());
            }
        }

        $posts->put('rendered', $partials);

        return $posts;
    }

    /**
     * Return a single rendered post.
     */
    public function single($clubSlug, $id)
    {
        $club = Club::where('slug', $clubSlug)->firstOrFail();
        $user = Auth::user();

        if (Auth::check() && (Auth::user()->isClubOwner($club) || Auth::user()->isAdmin())) {
            $post = $club->posts()->findOrFail($id);
        } else {
            $post = $club->posts()->public()->findOrFail($id);
        }

        if (policy($post)->show($user, $post)) {
            return view('club.partials.post', ['post' => $post, 'club' => $club, 'network' => 1])->render();
        }

        abort(403);
    }
}
