<?php

namespace Vereinsleben\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Vereinsleben\Bandage;
use Vereinsleben\ContactPerson;
use Auth;
use Vereinsleben\Event;

class ContactPersonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contact_person = new ContactPerson;
        $contact_person->order_number = $request->order_number;
        $contact_person->department = $request->department;
        $contact_person->gender = $request->gender;
        $contact_person->name = $request->name;
        $contact_person->position = $request->position;
        $contact_person->phone = $request->phone;
        $contact_person->email = $request->email;
        $contact_person->bandage_id = $request->bandage_id;
	
        $bandate = Bandage::findOrFail($request->bandage_id);
        
		    $contact_person->save();
	    return \Redirect::route('bandage.detail', $bandate->slug.'#ansprechpartner')->with('flash-success', 'Die Ansprechpartner wurde erfolgreich geändert!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($bandage_id, $id)
    {
	    $contact = ContactPerson::findOrFail($id);
	    $bandage = Bandage::findOrfail($bandage_id);
	
	    $form = view('bandage.partials.edit.contact-person', [
		    'contact'   => $contact,
		    'bandage' => $bandage,
	    ]);
	
	    return $form;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	
        $contact = ContactPerson::findOrFail($id);
        $contact->order_number = $request->order_number;
        $contact->department = $request->department;
        $contact->gender = $request->gender;
        $contact->name = $request->name;
        $contact->position = $request->position;
        $contact->phone = $request->phone;
        $contact->email = $request->email;
        
        $bandage = Bandage::findOrfail($request->bandage_id);
        $contact->save();
	      return \Redirect::route('bandage.detail', $bandage->slug.'#ansprechpartner')->with('flash-success', 'Die Ansprechpartner wurde erfolgreich geändert!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact_persion = ContactPerson::find($id);
	      $contact_persion->delete();
	      return json_encode(['success' => true]);
    }
    
    public function list($bandageSlug){
	    $bandage = Bandage::where('slug', $bandageSlug)->firstOrFail();
	    $user = Auth::user();
	    $contacts = $bandage->contactPersons();
	    
	    $contacts = $contacts
		    ->orderBy('order_number', 'ASC')
		    ->orderBy('name', 'DESC')
		    ->get();
	    
	    $perPage = 10;
	    $currentPage = Paginator::resolveCurrentPage() ?: 1;
	    $pagerIndex = ($currentPage * $perPage) - $perPage;
	    $paginatedClients = collect($contacts)->slice($pagerIndex, $perPage);
	
	    $contacts = new Paginator($paginatedClients, $contacts->count(), $perPage, $currentPage, [
		    'path' => Paginator::resolveCurrentPath()]);
	
	    $partials = [];
	
	    foreach ($contacts as $contact){
		    array_push($partials, view('bandage.partials.contact-person', [
			    'contact'   => $contact,
			    'bandage' => $bandage
		    ])->render());
	    }
	
	    $contacts->put('rendered', $partials);
	
	    return $contacts;
    }
	
	public function single($bandageSlug, $id)
	{
		$bandage = Bandage::where('slug', $bandageSlug)->firstOrFail();
		$user = Auth::user();
		
		//if (Auth::check() && (Auth::user()->isBandageOwner($bandage) || Auth::user()->isAdmin())) {
		if (Auth::check() && (Auth::user()->isAdmin() || Auth::user()->isBandageAdmin($bandage))){
			$contact = $bandage->contactPersons()->findOrFail($id);
		} else{
			$contact = $bandage->contactPersons()->public()->findOrFail($id);
		}
		
		
		return view('bandage.partials.contact-person', ['contact' => $contact, 'bandage' => $bandage])->render();
	}
}
