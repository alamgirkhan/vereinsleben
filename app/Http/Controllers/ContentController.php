<?php
	
	namespace Vereinsleben\Http\Controllers;
	
	use Illuminate\Http\Request;
	
	use Illuminate\Support\Facades\Log;
	use Vereinsleben\Http\Requests;
	use Vereinsleben\Meta;
	use Vereinsleben\Content;
	use Mail;
  use Vereinsleben\State;

  class ContentController extends BaseController
	{
		/**
		 * Display a listing of the resource.
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function view($slug)
		{
			$content = Content::where('slug', $slug)->firstOrFail();
			
			return view('sites.content', [
				'content' => $content
			]);
			
		}
		
		
		public function kontakt()
		{
      $states = State::pluck('name', 'id')->toArray();
      $state = auth() && auth()->user() ? auth()->user()->federal_state : '';
      $state_id = 0;
      if ($state !== '') {
        $st = State::where('name', $state)->first();
        $state_id = $st->id;
      }
			return view('sites.contact', ['states' => $states, 'state_id' => $state_id]);
		}
		
		
		public function stellenBoerse()
		{
			return view('news.job-exchange');
		}
		
		/**
		 * Display a listing of the resource.
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function index()
		{
			return view('admin.sites.content', [
				'contents' => Content::orderBy('created_at', 'desc')->get()
			]);
		}
		
		public function addContent()
		{
			return view('admin.sites.edit', ['content' => new Content()]);
		}
		
		public function store(Request $request)
		{
			$content = new Content;
			
			$content->title = !empty($request['content_title']) ? $request['content_title'] : null;
			$content->content = !empty($request['content']) ? $request['content'] : null;
			$content->slug = !empty($request['content_slug']) ? $request['content_slug'] : null;
			
			$content->save();
			$this->saveMetaData($request, $content);
			return \Redirect::route('admin.content.index')->with('flash-success', 'Die content wurde angelegt!');
		}
		
		public function edit($id)
		{
			$content = Content::find($id);
			
			if (!isset($content->meta)) {
				$content->meta()->create([]);
			}
			
			return view('admin.sites.edit', [
				'content' => Content::find($id),
			]);
		}
		
		public function update(Request $request, $id)
		{
			$content = Content::findOrFail($id);
			
			$content->title = !empty($request['content_title']) ? $request['content_title'] : null;
			$content->content = !empty($request['content']) ? $request['content'] : null;
			$content->slug = !empty($request['content_slug']) ? $request['content_slug'] : null;
			
			$content->save();
			$this->saveMetaData($request, $content);
			return \Redirect::route('admin.content.index')->with('flash-success', 'Deine Änderungen wurden gespeichert!');
		}
		
		public function destroy(Request $request, $id)
		{
			$content = Content::find($id);
			
			$content->delete();
			
			return \Redirect::route('admin.content.index')->with('flash-success', 'Datensatz erfolgreich gelöscht');
		}
		
		/**
		 * @param NewsFormRequest $request
		 * @param $news
		 */
		public function saveMetaData(Request $request, $content)
		{
			$meta = new Meta();
			$meta->meta_bild_alt = $request->content_title;
			$meta->meta_description = $request->meta_description;
			$meta->meta_keywords = $request->meta_keywords;
			$meta->meta_author = $request->meta_author;
			$meta->meta_language = $request->meta_language;
			
			if ($content->meta === null) {
				$content->meta()->create($meta->toArray());
			} else {
				$content->meta()->update($meta->toArray());
			}
		}
		
		public function contactUs(Request $request)
		{
			$this->validate($request, ['name' => 'required', 'email' => 'required|email', 'message' => 'required', 'g-recaptcha-response' => 'recaptcha']);
			$from = $request->get('email');
      $imprint = Content::select('content')->where('slug', 'imprint')->first();
			Mail::send('auth.emails.contact-email',
				array(
					'firma' => $request->get('firma'),
					'name' => $request->get('name'),
					'vorname' => $request->get('vorname'),
					'strasse' => $request->get('strasse'),
					'plz' => $request->get('plz'),
					'ort' => $request->get('ort'),
					'bundesland' => $request->get('bundesland'),
					'telefon' => $request->get('telefon'),
					'email' => $request->get('email'),
					'bksmt' => $request->get('bksmt'),
					'user_message' => $request->get('message'),
					'imprint' => $imprint
				), function ($message) {
					$message->from('info@vereinsleben.de');
					$message->to('info@vereinsleben.de', 'Admin')->subject('Kontakt - vereinsleben.de');
				});
			
			return back()->with('flash-success', 'Danke, dass sie uns kontaktiert haben!');
		}

		public function addOrEditImprint() {
      $content = Content::where('slug', 'imprint')->first();

      return view('admin.sites.add_edit_imprint', [
        'content' => $content,
      ]);
    }
}
