<?php

namespace Vereinsleben\Http\Controllers;

use Illuminate\Http\Request;

use Mail;
use Redirect;
use Vereinsleben\Club;
use Vereinsleben\Content;
use Vereinsleben\Contest;
use Vereinsleben\State;
use Auth;
use Vereinsleben\User;


class ContestController extends BaseController
{

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contestReq(Request $request)
    {
        if (Auth::check()){

            $club = Club::whereName($request['club'])->firstOrFail();
            $user = Auth::user();

            $cont = new Contest;
            $cont = $cont->where('user_id', '=', $user->id)->first();

            if ($cont){
                abort(403);
            }

            $user->lastname = $request['name'];
            $user->firstname = $request['vorname'];
            $user->federal_state = $request['federal'];
            $user->zip = $request['plz'];
            $user->city = $request['ort'];

            $contest = new Contest;

            $contest->user_id = $user->id;
            $contest->club_id = $club->id;
            $contest->user_firstname = $request['vorname'];
            $contest->user_lastname = $request['name'];
            $contest->username = $user->username;
            $contest->club_name = $request['club'];
            $contest->clubslug = $club->slug;
            $contest->save();
            $user->save();

            return view('contest.thanks', [
                'club' => $club->name,
                'user' => $user
            ]);
        } else{
            abort(403);
        }
    }


    public function sendInvitation(Request $request)
    {

        if (Auth::check()){

            $arr = count($request->all());

            $user = new User;
            $user = $user->where('id', '=', $request['userid'])->firstOrFail();

            $club = $request['clubid'];

            $imprint = Content::select('content')->where('slug', 'imprint')->first();

            for ($i = 0; $i < $arr; $i++){

                if ($request->input('email' . $i) != '' && $request->input('email' . $i) != null){

                    $mail = $request->input('email' . $i);

                    Mail::send('contest.invitation', ['user' => $user->firstname, 'club' => $club, 'imprint' => $imprint], function($message) use ($mail, $user){
                        $message->to($mail)->subject($user->fullname . " hat Dich eingeladen");
                    });

                    $mail = '';
                }
            }

            return Redirect::route('home')->with('flash-success', 'Deine Einladung(en) wurde(n) erfolgreich versendet!');
        } else{
            abort(403);
        }

    }

    public function index()
    {

        $federal = State::all();
        $voted = 0;
        $suggestedClub = new Club;

        if (Auth::user()){

            $user = Auth::user();

            $suggestedClub = $suggestedClub->where('suggested_from', '=', $user->id)->orderBy('created_at', 'desc')->first();

            $contest = new Contest;
            $contest = $contest->where('user_id', '=', $user->id)->first();

            if ($contest){
                $voted = 1;

                return view('contest.index', [
                    'user'        => $user,
                    'federal'     => $federal,
                    'voted'       => $voted,
                    'suggested'   => $suggestedClub,
                    'contestClub' => $contest->club_name
                ]);
            } else{
                return view('contest.index', [
                    'user'      => $user,
                    'federal'   => $federal,
                    'voted'     => $voted,
                    'suggested' => $suggestedClub
                ]);
            }

        }

        return view('contest.index', [
            'user'    => null,
            'federal' => $federal,
            'voted'   => $voted
        ]);

    }

}