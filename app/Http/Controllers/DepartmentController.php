<?php

namespace Vereinsleben\Http\Controllers;

use Illuminate\Http\Request;

use Vereinsleben\Http\Requests;
use Vereinsleben\Http\Requests\DepartmentFormRequest;

use Vereinsleben\Department;
use Vereinsleben\Club;
use Vereinsleben\User;
use Vereinsleben\Sport;
use Vereinsleben\Team;
use Vereinsleben\ClubSport;

use Auth;

class DepartmentController extends BaseController
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(DepartmentFormRequest $request)
    {
        $club_id = $request['club_id'];
        $user = User::where('id', $request['user_id'])->firstOrFail();
        $club = Club::where('id', $club_id)->firstOrFail();

        $department = new Department();
        $department->sport_id = $request['sport_id'];
        $department->club_id = $club->id;
        $department->description = $request['description'];

        $club->departments()->save($department);


        if($user !== '' && $user !== null){

            $department->contacts()->save($user);
        }

        return \Redirect::route('club.detail', Club::find($club_id)->slug)->with('flash-success', 'Deine Abteilung wurde erfolgreich gespeichert.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug, $id)
    {
        $department = Department::findOrFail($id);
        $club = $department->club()->firstOrFail();
        $contact = $department->contacts()->get()->first();

        if(isset($contact)){
            $user_id = $contact->id;
        }
        else{
            $user_id = null;
        }

        $sport = $club->sports()->where('sport_id', $department->sport_id)->pluck('title', 'sport_id')->toArray();
        $users = $club->members()->pluck('fullname', 'user_id')->toArray();
        $form = view('club.partials.edit.department', [
            'department'    => $department,
            'user_id'       => $user_id,
            'contact'       => $contact,
            'club'          => $club,
            'sport_id'      => $department->sport_id,
            'users'         => $users,
            'newDepartment' => false,
        ]);

        return $form;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(DepartmentFormRequest $request, $id)
    {
        $user_id = $request['user_id'];
        $phone = $request['phone'];
        $ap_email = $request['ap_email'];
        $description = $request['description'];


        $department = Department::findOrFail($id);
        $department->sport_id = $request['sport_id'];
        $department->description = $description;

        $department->save();

        if ($user_id == 'Keiner ausgewählt' || $user_id == ''){

            $department->contacts()->detach();

        }
        else{
            $user = User::where('id', $user_id)->firstOrFail();
            if ($user->phone !== $phone || $user->ap_email !== $ap_email){
                $user->phone = $phone;
                $user->ap_email = $ap_email;
            }

            $department->contacts()->detach();
            $department->contacts()->save($user);
        }

        return \Redirect::route('club.detail', Club::findOrFail($department->club_id)->slug)->with('flash-success', 'Der Eintrag wurde erfolgreich aktualisiert');
    }

    /**
     * Mark the specified department resource as deleted.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($clubSlug, $id)
    {
        $department = Department::findOrFail($id);
        $department->contacts()->detach();
        $department->delete();

        return json_encode(['success' => true, 'item' => $department]);
    }

    public function latest($clubSlug, $sport_id)
    {
        $sport = Sport::where('id', $sport_id)->firstOrFail();
        $club = Club::where('slug', $clubSlug)->firstOrFail();
        $department = Department::where('sport_id', $sport_id)->first();

        //dd($department);
        $users = $club->members()->pluck('fullname', 'user_id')->toArray();
        if (isset($department)){
            $teams = Team::where('department_id', $department->id)->latest('created_at')->get();
            $contact = $department->contacts()->get()->first();
            $departmentexist = true;
        } else{
            $teams = '';
            $contact = '';
            $departmentexist = false;
        }

        return view('club.partials.department', [
            'department'      => $department,
            'club'            => $club,
            'contact'         => $contact,
            'sport'           => $sport,
            'teams'           => $teams,
            'departmentexist' => $departmentexist,
            'users'           => $users,
        ])->render();
        abort(403);
    }

    /**
     * Return a single rendered department.
     */
    public function single($clubSlug, $id)
    {
        $club = Club::where('slug', $clubSlug)->firstOrFail();
        $department = $club->departments()->findOrFail($id);
        $contact = $department->contacts()->get()->first();
        $users = $club->members()->pluck('fullname', 'user_id')->toArray();
        $sport = Sport::where('id', $department->sport_id)->firstOrFail();
        $teams = $department->teams()->latest('created_at')->get();

        return view('club.partials.department', [
            'department'      => $department,
            'club'            => $club,
            'contact'         => $contact,
            'sport'           => $sport,
            'teams'           => $teams,
            'departmentexist' => true,
            'users'           => $users,
        ])->render();
        abort(403);
    }

    public function showRendered($clubSlug, $id)
    {
        $response = [
            'success'  => false,
            'template' => null,
            'error'    => null,
        ];

        try{
            $department = new Department();
            $response['success'] = true;
            $response['template'] = view('club.partials.department', [
                'department' => $department->where('id', $id)->first(),
                'user'       => Auth::user(),
                'slug'       => $clubSlug,
            ])->render();
        } catch (\Exception $ex){
            $response['success'] = false;
            $response['error'] = $ex->getMessage();
        }

        return response()->json($response);
    }

    public function editUser(Request $request, $id)
    {

        $department = Department::findOrFail($id);
        $user = new User;
        $user = $user->where('id', '=', $request['userid'])->firstOrFail();

        if ($request['phone'] == $user->phone && $request['mobile'] == $user->mobile){

            return back()->with('flash-warning', "Keine Änderungen vorgenommen.");
        } else{
            $user->phone = $request['phone'];
            $user->mobile = $request['mobile'];
            $user->save();

            return \Redirect::route('club.detail', Club::findOrFail($department->club_id)->slug)->with('flash-success', 'Der Eintrag wurde erfolgreich aktualisiert');
        }
    }
}
