<?php

namespace Vereinsleben\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

use Vereinsleben\City;
use Vereinsleben\CityZipcode;
use Vereinsleben\Http\Requests;
use Vereinsleben\Http\Requests\EventFormRequest;

use Vereinsleben\Event;
use Vereinsleben\Club;
use Vereinsleben\Image;

use Auth;
use Geocoder;
use Log;
use Vereinsleben\Sport;
use Vereinsleben\State;

class EventController extends BaseController
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */


    public function eventList(Request $request)
    {

        $partials = [];
        $eventCount = 0;

        $federal = $request->input('federal_state');
        $ortplz = $request->input('ort_plz');
        $searchtext = $request->input('searchtext');
        $start = $request->input('beginn');
        $end = $request->input('ende');
        $distance = $request->input('distance');
        $ajax = trim($request->input('ajax', ''));


        $i = 0;
        $events = new Event;
        $state = new State;
        $clubs = new Club;



        if ($searchtext != '' && $searchtext != null){
            $events = $events->public()
                             ->where('title', 'LIKE', '%' . $searchtext . '%')
                             ->orWhere('content', 'LIKE', '%' . $searchtext . '%')
                             ->orWhere('content_raw', 'LIKE', '%' . $searchtext . '%');
            $i++;

        }

        if ($federal != '' && $federal != null && $federal != '0' && $ortplz == '' && $ortplz == null){
            $state = $state->where('name', '=', $federal)->firstOrFail();

            $clubs = $clubs->published()->whereHas('states', function($query) use ($state){
                $query->where('state_id', '=', $state->id);
            });


            foreach ($clubs as $club){

                $events = $events->where('club_id', '=', $club->id);

            }

            $i++;
        }

        if ($start != '' && $start != null){
            $anfang = Carbon::parse($start);
            $anfang = $anfang->format('Y-m-d');
            $anfang2 = $anfang . ' 23:59:59';
            $anfang = $anfang . ' 00:00:00';


//            $events = $events->public()
//                             ->whereBetween('schedule_begin', [$anfang, $anfang2]);
            $events = $events->public()
                             ->where('schedule_begin', '>=', $anfang);
            $i++;
        }

        if ($end != '' && $end != null){
            $ende = Carbon::parse($end);
            $ende = $ende->format('Y-m-d');
            $ende2 = $ende . ' 23:59:59';
            $ende = $ende . ' 00:00:00';

//            $events = $events->public()
//                             ->whereBetween('schedule_end', [$ende, $ende2]);
            $events = $events->public()
                             ->where('schedule_end', '<=', $ende);
            $i++;
        }


        if ($ortplz != '' && $ortplz != null){

            if (is_numeric(trim($ortplz))){

                $zipcode = CityZipcode::where('zip', '=', $ortplz)->first();
                $city = (!is_null($zipcode)) ? $zipcode->city : null;

            } else{

                $city = City::where('name', '=', $ortplz)->first();
            }

            if ($city != null && $distance != null && $distance != ''){

                $events = $events->public()->closest($city->lat, $city->lon, $distance);
            } else{

                $events = $events->public()->where('city', '=', $city);
            }

            $i++;
        }

        if ($i <= 0){
            $events = $events->public();
        }

        //$events = $events->with('images')->orderBy('schedule_begin', 'desc')->get();
        $events = $events->with('images')->orderBy('schedule_begin', 'desc');

        $events_new = $this->paginateSearchResults($request, $events, 16);
        $eventCount = $events_new->total();


        if ($ajax){
            foreach ($events_new as $event){
                array_push($partials, view('event.partials.eventItems', ['event' => $event])->render());
            }

            if ($events_new->nextPageUrl()){
                array_push($partials, view('event.more-result', ['more' => $events_new, 'type' => 'event'])->render());
            }

            $events_new->put('rendered', $partials);

            return $events_new;
        }


        return view('event.index', [
            'events'        => $events_new,
            'federal_state' => $federal,
            'query'         => [
                'searchtext' => $searchtext,
                'beginn'     => $start,
                'ende'       => $end,
                'ort_plz'    => $ortplz,
                'distance'   => $distance
            ]
        ]);
    }





//    public function eventList(Request $request)
//    {
//
//        $partials = [];
//
//        $federal = $request->input('federal_state');
//        $ortplz = $request->input('ort_plz');
//        $searchtext = $request->input('searchtext');
//        $start = $request->input('beginn');
//        $end = $request->input('ende');
//        $distance = $request->input('distance');
//
//
//        $i = 0;
//        $events = new Event;
//        $state = new State;
//
//        if ($searchtext != '' && $searchtext != null){
//            $events = $events->public()
//                             ->where('title', 'LIKE', '%' . $searchtext . '%')
//                             ->orWhere('content', 'LIKE', '%' . $searchtext . '%')
//                             ->orWhere('content_raw', 'LIKE', '%' . $searchtext . '%');
//            $i++;
//
//        }
//
//        if ($federal != '' && $federal != null && $federal != '0' && $ortplz == '' && $ortplz == null){
//            $state = $state->where('name', '=', $federal)->firstOrFail();
//        }
//
//        if ($federal != '' && $federal != null && $federal != '0' && $ortplz == '' && $ortplz == null){
//            $events = $events->public()
//                             ->ByState($state);
//            $i++;
//        }
//
//        if ($start != '' && $start != null){
//            $anfang = Carbon::parse($start);
//            $anfang = $anfang->format('Y-m-d');
//            $anfang2 = $anfang . ' 23:59:59';
//            $anfang = $anfang . ' 00:00:00';
//
//
////            $events = $events->public()
////                             ->whereBetween('schedule_begin', [$anfang, $anfang2]);
//            $events = $events->public()
//                             ->where('schedule_begin', '>=', $anfang);
//            $i++;
//        }
//
//        if ($end != '' && $end != null){
//            $ende = Carbon::parse($end);
//            $ende = $ende->format('Y-m-d');
//            $ende2 = $ende . ' 23:59:59';
//            $ende = $ende . ' 00:00:00';
//
////            $events = $events->public()
////                             ->whereBetween('schedule_end', [$ende, $ende2]);
//            $events = $events->public()
//                             ->where('schedule_end', '<=', $ende);
//            $i++;
//        }
//
//
//        if ($ortplz != '' && $ortplz != null){
//
//            if (is_numeric(trim($ortplz))){
//
//                $zipcode = CityZipcode::where('zip', '=', $ortplz)->first();
//                $city = (!is_null($zipcode)) ? $zipcode->city : null;
//
//            } else{
//
//                $city = City::where('name', '=', $ortplz)->first();
//            }
//
//            if ($city != null && $distance != null && $distance != ''){
//
//                $events = $events->public()->closest($city->lat, $city->lon, $distance);
//            } else{
//
//                $events = $events->public()->where('city', '=', $city);
//            }
//
//            $i++;
//        }
//
//
//        if ($i <= 0){
//            $events = $events->public();
//        }
//
////        $events = $events->with('images')->orderBy('schedule_begin', 'desc')->get();
//        $events = $events->with('images')->orderBy('schedule_begin', 'desc');
//
//        $events_new = $this->paginateSearchResults($request, $events, 16);
//
//
//        return view('event.index', [
//            'events'        => $events_new,
//            'federal_state' => $federal,
//            'query'         => [
//                'searchtext' => $searchtext,
//                'beginn'     => $start,
//                'ende'       => $end,
//                'ort_plz'    => $ortplz,
//                'distance'   => $distance
//            ]
//        ]);
//
//    }


    /**
     * Return a single event.
     */
    public function detail($id)
    {
        $user = Auth::user();

        $event = Event::whereId($id)->firstOrFail();

//        if (Auth::check() && (Auth::user()->isClubOwner($club) || Auth::user()->isAdmin())) {
//            $event = $club->events()->findOrFail($id);
//        } else {
//            $event = $club->events()->public()->findOrFail($id);
//        }
//
//        if (policy($event)->show($user, $event)) {
//            return view('club.partials.event', ['event' => $event, 'club' => $club])->render();
//        }

        return view('event.detail', [
            'event' => $event
        ]);
    }


    public function store(EventFormRequest $request)
    {

        $event = new Event;

        $event->title = $request['title'];
        $event->content = $request['content_raw'];
        $event->content_raw = $request['content_raw'];
        $event->street = $request['street'];
        $event->house_number = $request['house_number'];
        $event->zip = $request['zip'];
        $event->city = $request['city'];
        $event->schedule_begin = date('Y-m-d H:i:s', strtotime($request['schedule_begin']));
        $event->schedule_end = date('Y-m-d H:i:s', strtotime($request['schedule_end']));
        $event->published_from = !empty($request['published_from']) ? date('Y-m-d H:i:s', strtotime($request['published_from'])) : null;
        $event->published_to = !empty($request['published_to']) ? date('Y-m-d H:i:s', strtotime($request['published_to'])) : null;
        $event->club_id = $request['club_id'];
        $event->state = Event::STATE_NEW;
        $event->location = $request['location'];
        $event->price = $request['price'];


        if ($request['sport'] != '' && $request['sport'] != null){

            $sport = new Sport;


            if(is_numeric($request['sport'])){

                $sport = $sport->where('id', '=', $request['sport'])->firstOrFail();
                $event->sport = $sport->id;

            }
            else{

                $sport->title = $request['sport'];

                if($request['club_id'] != '' && $request['club_id'] != null){

                    $sport->club_id = $request['club_id'];
                }

                $sport->save();

                $event->sport = $sport->id;

            }

        }

        try {
            $geocoder = Geocoder::geocode($event->fullAddress());

            if ($geocoder !== null
                && $geocoder->get()->first()->getCoordinates()->getLatitude() !== 0
                &&  $geocoder->get()->first()->getCoordinates()->getLongitude() !== 0) {

                $event->lat = $geocoder->get()->first()->getCoordinates()->getLatitude();
                $event->lng = $geocoder->get()->first()->getCoordinates()->getLongitude();
            }
        } catch (\Exception $e) {

            $event->lat = null;
            $event->lng = null;

            Log::warning($e->getMessage(), [
                'during' => 'event@store',
                'event'  => $event->id
            ]);
        }

        if (is_null($event->published_from)) {
            $event->published_at = date("Y-m-d H:i:s");
        } else {
            $event->published_at = $event->published_from;
        }

        $this->authorize($event);

        $event->save();

        $images = $request->file('images');
        if ($images !== null && count($images) > 0) {
            foreach ($images as $imageUploaded) {
                if ($imageUploaded !== null) {
                    $image = new Image(['picture' => $imageUploaded]);
                    $event->images()->save($image);
                }
            }
        }

        return \Redirect::route('club.detail', Club::find($event->club_id)->slug)->with('flash-success', 'Deine Veranstaltung wurde erfolgreich gespeichert');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Event::findOrFail($id);
        $club = $event->club()->firstOrFail();

        $sport = new Sport;

        $this->authorize($event);

        if (isset($event->sport)){
            $sport = $sport->findOrFail($event->sport);
        } else{
            $sport = null;
        }

        $form = view('club.partials.edit.event', [
            'event'       => $event,
            'club'        => $club,
            'sport_event' => $sport
        ]);

        return $form;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EventFormRequest $request, $id)
    {
        $event = Event::findOrFail($id);

        $event->title = $request['title'];
        $event->content = $request['content_raw'];
        $event->content_raw = $request['content_raw'];
        $event->street = $request['street'];
        $event->house_number = $request['house_number'];
        $event->zip = $request['zip'];
        $event->city = $request['city'];
        $event->schedule_begin = date('Y-m-d H:i:s', strtotime($request['schedule_begin']));
        $event->schedule_end = date('Y-m-d H:i:s', strtotime($request['schedule_end']));
        $event->published_from = !empty($request['published_from']) ? date('Y-m-d H:i:s', strtotime($request['published_from'])) : null;
        $event->published_to = !empty($request['published_to']) ? date('Y-m-d H:i:s', strtotime($request['published_to'])) : null;
        $event->location = $request['location'];
        $event->price = $request['price'];

        if ($request['sport'] != '' && $request['sport'] != null){

            $sport = new Sport;
            $sport = $sport->where('id', '=', $request['sport'])->firstOrFail();
            $event->sport = $sport->id;

        }

        try {
            $geocoder = Geocoder::geocode($event->fullAddress());

            if ($geocoder !== null
                && $geocoder->get()->first()->getCoordinates()->getLatitude() !== 0
                &&  $geocoder->get()->first()->getCoordinates()->getLongitude() !== 0) {

                $event->lat = $geocoder->get()->first()->getCoordinates()->getLatitude();
                $event->lng = $geocoder->get()->first()->getCoordinates()->getLongitude();
            }
        } catch (\Exception $e) {

            $event->lat = null;
            $event->lng = null;

            Log::warning($e->getMessage(), [
                'during' => 'event@update',
                'event'  => $event->id
            ]);
        }

        if (!is_null($event->published_from)) {
            $event->published_at = $event->published_from;
        }

        $this->authorize($event);

        $event->save();

        $images = $request->file('images');
        if ($images !== null && count($images) > 0) {
            foreach ($images as $imageUploaded) {
                if ($imageUploaded !== null) {
                    $image = new Image(['picture' => $imageUploaded]);
                    $event->images()->save($image);
                }
            }
        }

        $imagesDelete = $request->input('delete-image');
        if ($imagesDelete !== null && count($imagesDelete) > 0) {
            foreach ($imagesDelete as $imageDeleteId) {
                if (is_numeric($imageDeleteId)) {
                    $image = $event->images()->find($imageDeleteId);
                    if ($image !== null) {
                        $image->delete();
                    }
                }
            }
        }
        
	    return back()->withInput()->with('flash-success', 'Der Eintrag wurde erfolgreich aktualisiert');
    }

    /**
     * Mark the specified event resource as deleted.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $event = Event::findOrFail($id);

        $this->authorize($event);

        $event->delete();

        return json_encode(['success' => true, 'item' => $event]);
    }

    public function latest($clubSlug)
    {
        $club = Club::where('slug', $clubSlug)->firstOrFail();
        $user = Auth::user();
        $events = $club->events()->latest('published_from')->latest('schedule_begin');

        if (Auth::check() && (Auth::user()->isClubOwner($club) || Auth::user()->isAdmin())) {
            $events = $events->get();
        } else {
            $upcomingEvent = $club->events()->upcoming()->first();
            if (!is_null($upcomingEvent)) {
                $events = $events->public()->get()->filter(function ($event) use ($upcomingEvent) {
                    return $event->id !== $upcomingEvent->id;
                });
            } else {
                $events = $events->public()->get();
            }
        }

        $perPage = 10;
        $currentPage = Paginator::resolveCurrentPage() ?: 1;
        $pagerIndex = ($currentPage * $perPage) - $perPage;
        $paginatedClients = collect($events)->slice($pagerIndex, $perPage);

        $events = new Paginator($paginatedClients, $events->count(), $perPage, $currentPage, [
            'path' => Paginator::resolveCurrentPath()]);

        $partials = [];

        foreach ($events as $event) {
            if (policy($event)->show($user, $event)) {
                array_push($partials, view('club.partials.event', [
                    'event' => $event,
                    'club'  => $club
                ])->render());
            }
        }

        $events->put('rendered', $partials);

        return $events;
    }

    /**
     * Return a single rendered event.
     */
    public function single($clubSlug, $id)
    {
        $club = Club::where('slug', $clubSlug)->firstOrFail();
        $user = Auth::user();

        if (Auth::check() && (Auth::user()->isClubOwner($club) || Auth::user()->isAdmin())) {
            $event = $club->events()->findOrFail($id);
        } else {
            $event = $club->events()->public()->findOrFail($id);
        }

        if (policy($event)->show($user, $event)) {
            return view('club.partials.event', ['event' => $event, 'club' => $club])->render();
        }

        abort(403);
    }

    /**
     * Paginate search results. Workaround since we cannot use the paginate() method because there is currently a bug in
     * combination with HAVING queries.
     */
    protected function paginateSearchResults(Request $request, $query, $perPage = 16)
    {
        $curPage = $request->input('page'); // reads the query string, defaults to 1

        // clone the query to make 100% sure we don't have any overwriting
        $itemQuery = clone $query;

        // this does the sql limit/offset needed to get the correct subset of items
        $items = $itemQuery->forPage($curPage, $perPage)->get();

        // manually run a query to select the total item count.
        // use a nested query, necessary because we have a HAVING query.
        $totalItems = \DB::table(\DB::raw("({$query->toSql()}) as sub"))
                         ->mergeBindings($query->getQuery())// you need to get underlying Query Builder
                         ->count();

        // make the paginator, which is the same as returned from paginate()
        // all() will return an array of models from the collection.
        $paginatedItems = new Paginator($items->all(), $totalItems, $perPage, Paginator::resolveCurrentPage(), [
            'path' => Paginator::resolveCurrentPath()]);

        // append the search queries to the paginator link
        $paginatedItems->appends([
            'query' => [
                'searchtext'    => $request->input('searchtext'),
                'beginn'        => $request->input('begin'),
                'ende'          => $request->input('ende'),
                'ort_plz'       => $request->input('ort_plz'),
                'distance'      => $request->input('distance'),
                'federal_state' => $request->input('federal_state')
            ]
        ]);

        return $paginatedItems;
    }
}
