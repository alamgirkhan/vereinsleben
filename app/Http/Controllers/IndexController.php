<?php

namespace Vereinsleben\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use Vereinsleben\Club;
use Vereinsleben\News;
use Vereinsleben\Category;
use Vereinsleben\Event;
use Vereinsleben\NumberOfWeek;
use Vereinsleben\Review;
use Vereinsleben\Slider;
use Vereinsleben\State;
use Vereinsleben\Spitzensport;
use Auth;
//use Imagine\Image\ImageInterface;
//use Imagine\Imagick\Imagine;

class IndexController extends BaseController
{
    const LATEST_NEWS_ENTRIES     = 27;
    const LATEST_NEWS_POS_ENTRIES = 9;

    public function __construct(){
        parent::__construct();
    }
    /**
     * handle authorized and unauthorized requests to the index action
     * specifies custom data and view handling
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        $this->checkFederalState($request->ip(), $request->user());
        $news = new News;
        $spitzensport = new Spitzensport;
        $slider = new Slider;
        $category = new Category;
        $event = new Event;
        $pos = 1;
        $latestNewsMerge = collect([]);
        $featuredNewsMerge = new News;
        $otherStates = new State;

        $latestNews = $news->latestSportsNews($category->sportsID(), self::LATEST_NEWS_ENTRIES, $this->getFederalState());
        $latestNewsWithPos = $news->latestSportsNewsWithPos($category->sportsID(), self::LATEST_NEWS_POS_ENTRIES, $this->getFederalState());

        //20-08-2018: regional news for only one federal state
        $state = $this->getFederalState();

        $otherFeds = $otherStates->whereNotBetween('id', [$state->id, $state->id]);
        $otherFeds = $otherFeds->pluck('id', 'id');
        $otherFeds = $otherFeds->toArray();

        $latestNewsFed = $news->public()->whereHas('states', function($query) use ($state){
            $query->where('state_id', $state->id);
        })->orderBy('published_at', 'desc')->get();

        $latestNewsOtherFeds = $news->public()->whereHas('states', function($query) use ($otherFeds){
            $query->whereIn('state_id', $otherFeds);
        })->orderBy('published_at', 'desc')->get();

        $spitzensports = $spitzensport->public()->whereHas('states', function($query) use ($otherFeds){
            $query->whereIn('state_id', $otherFeds);
        })->orderBy('published_at', 'desc')->get();

        $latestNewsSelectedFed = $latestNewsFed->diff($latestNewsOtherFeds);

        $latestNewsSelectedFed->all();

        //12-07-2018: delete news from collection latestNews (ordered by date desc)
        $latestNews = $latestNews->keyBy('id');

        foreach ($latestNewsWithPos as $latPos){

            if ($latestNews->has($latPos->id)){
                $latestNews->forget($latPos->id);
            } else{
                continue;
            }
        }

        $i = 0;

        if ($latestNewsSelectedFed->count() > 3){

            foreach ($latestNewsSelectedFed as $newsFed){

                if ($i >= 4){
                    break;
                }

                if ($latestNews->has($newsFed->id)){
                    $latestNews->forget($newsFed->id);

                } else{
                    continue;
                }
                $i++;
            }
        } else{
            $latestNewsSelectedFed = null;
        }


        foreach ($latestNews as $latest){

            foreach ($latestNewsWithPos as $latestPos){

                if ($latestPos->position == $pos){

                    $latestNewsMerge->push($latestPos);
                    $latestNewsWithPos->shift();
                    $pos++;

                } else{
                    break;
                }
            }
            $latestNewsMerge->push($latest);
            $pos++;
        }

        $featuredNews = $news->featuredNews(1, $this->getFederalState());
        $featuredNewsWithPos = $news->featuredNewsWithPos(1, $this->getFederalState());

        if (count($featuredNewsWithPos) > 0){
            $fn = $featuredNewsWithPos->first();
            $fn_pos = $fn->position;
            if ($fn_pos == 8){
                $featuredNewsMerge = $featuredNewsWithPos;
            }
        } else{
            $featuredNewsMerge = $featuredNews;
        }

        $upcomingEvents = Event::latest('schedule_begin')->public()->where('is_tip', '=', 1)->take(4)->get()->sortBy('schedule_begin');
        $sliderImages = $slider->sliderByState($this->getFederalState());

        //$sliderImages = $slider->where('published', '=', 1)->get()->sortBy('position');

	    // get all number of week
	    $numbers = NumberOfWeek::where('published_from', '<=', Carbon::now())->get();
//	    $numbers = NumberOfWeek::all();
	    
        return view('index', [
//            'latestNews'     => $latestNews,
//            'featuredNews'   => $featuredNews,
'regionalNews'   => $latestNewsSelectedFed,
'latestNews'     => $latestNewsMerge,
'featuredNews'   => $featuredNewsMerge,
'sliderImages'   => $sliderImages,
'spitzensports' => $spitzensports,
'upcomingEvents' => $upcomingEvents,
'numbers' => $numbers
        ]);
    }

    public function kontakt(){
        return view('sites.contact');
    }
}