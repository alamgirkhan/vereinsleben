<?php
	
	namespace Vereinsleben\Http\Controllers;
	
	use Auth;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\DB;
	use Vereinsleben\City;
	use Vereinsleben\CityZipcode;
	use Vereinsleben\Club;
	use Vereinsleben\KeyWord;
	use Vereinsleben\Sport;
	use Illuminate\Pagination\LengthAwarePaginator as Paginator;
	
	class LandingPageController extends BaseController
	{
		/**
		 * Display a listing of the resource.
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function index(Request $request, $sportQuery, $cityQuery)
		{
			
			$curPage = $request->input('page');
			$keyword = $request->input('keyword');
			$perPage = (int)($request->input('per-page') ? $request->input('per-page') : 10);
			$distance = 1000;
			$perPageList = [10, 20, 50, 100];
			
			/*-------Logic from search global function------------*/
			/*if ($sportQuery || $cityQuery !== '') {
				$clubsQuery = Club::where('published', '=', 1);
				if ($cityQuery != '') {
					$city = City::where('name', 'LIKE', '%' . $cityQuery . '%')->first();
					if (isset($city)) {
						$clubsQuery = $clubsQuery->closest($city->lat, $city->lon, $distance);
					}
				}
				$sport = Sport::where('title', 'LIKE', '%' . $sportQuery . '%')->first();
				if (isset($sport)) {
					$clubsQuery = $clubsQuery->whereHas('sports', function ($query) use ($sport) {
						$query->where('sport.id', '=', $sport->id);
					});
				}
				$clubCount = $clubsQuery->get()->count();
				$clubs = $clubsQuery->forPage($curPage, $perPage)->get();
				$user = Auth::user();
				$items = $clubs->filter(function ($item) use ($user) {
					return policy($item)->show($user, $item);
				});
				$paginatedItems = new Paginator($items->all(), $clubCount, $perPage, Paginator::resolveCurrentPage(), [
					'path' => Paginator::resolveCurrentPath()]);
			} else {
				$clubs = null;
				$clubCount = 0;
			}*/
			
			$sfc = DB::table('sport_federalstate_city')->where('sport', $sportQuery)->where('location', $cityQuery)->first();
			if (isset($sfc)) {
				/*The code applies to the current logic*/
				if ($sportQuery || $cityQuery !== '') {
					$clubsQuery = Club::where('published', '=', 1);

                    if ($sportQuery == "Basketballverein") $sportSearchString = "%Basketball%";
                    if ($sportQuery == "Fußballverein") $sportSearchString = "Fu%ball%";
                    if ($sportQuery == "Golfverein") $sportSearchString = "Golf%";
                    if ($sportQuery == "Handballverein") $sportSearchString = "%Handball%";
                    if ($sportQuery == "Reitverein") $sportSearchString = "%Reiten%";
                    if ($sportQuery == "Schützenverein") $sportSearchString = "%Schützen%";
                    if ($sportQuery == "Schwimmverein") $sportSearchString = "Schwimm%";
                    if ($sportQuery == "Sportverein") $sportSearchString = "%Sport%";
                    if ($sportQuery == "Tennisverein") $sportSearchString = "%Tennis%";
                    if ($sportQuery == "Tischtennisverein") $sportSearchString = "Tischtennis%";
                    if ($sportQuery == "Turnverein") $sportSearchString = "%Turn%";

                    $citySearchString = $cityQuery . "%";

					if (isset($citySearchString) && isset($sportSearchString)) {

						$clubsQuery = $clubsQuery->whereHas('sports', function ($q) use ($sportSearchString, $citySearchString) {
							$q->where('city', 'LIKE', $citySearchString)->where('sport.title', 'LIKE', $sportSearchString);
						});


						if (isset($keyword)) {
							$clubsQuery = $clubsQuery->where('name', 'LIKE', '%' . $keyword . '%');
						}
						$clubCount = $clubsQuery->get()->count();
						$clubs = $clubsQuery->forPage($curPage, $perPage)->get();
						$user = Auth::user();
						$items = $clubs->filter(function ($item) use ($user) {
							return policy($item)->show($user, $item);
						});
						$paginatedItems = new Paginator($items->all(), $clubCount, $perPage, Paginator::resolveCurrentPage(), [
							'path' => Paginator::resolveCurrentPath()]);
						$paginatedItems->appends([
							'per-page' => $request->input('per-page'),
							'keyword' => $request->input('keyword'),
						]);
					} else {
						$paginatedItems = [];
						$clubCount = 0;
					}
				} else {
					$paginatedItems = [];
					$clubCount = 0;
				}
				$data = [
					'sport' => $sportQuery,
					'city' => $cityQuery,
					'totalCount' => $clubCount,
					'clubs' => $paginatedItems,
					'perPageList' => $perPageList,
					'currentPerPage' => $perPage,
					'keyword' => $keyword
				];
				return view('landing-page.index', ['data' => $data]);
			} else {
				return view('errors.404');
			}
		}
		
		public function keyWordTable()
		{
			$keywords = KeyWord::simplePaginate(10);
			return view('landing-page.keyword-table', ['keywords' => $keywords]);
		}
	}
