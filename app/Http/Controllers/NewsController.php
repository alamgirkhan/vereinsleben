<?php
	
	namespace Vereinsleben\Http\Controllers;
	
	use Carbon\Carbon;
	use Illuminate\Http\Request;
	use Illuminate\Pagination\LengthAwarePaginator as Paginator;
	
	use Vereinsleben\Campaign;
	use Vereinsleben\Category;
	use Vereinsleben\Helpers\RandomCampaign;
	use Vereinsleben\Http\Controllers\Controller;
	
	use Vereinsleben\News;
	use Vereinsleben\NewsCategory;
	use \DB;
	use Auth;
	use Vereinsleben\State;
	use Vereinsleben\Tag;
	
	class NewsController extends BaseController
	{
		
		public function __construct()
		{
			parent::__construct();
		}
		
		/**
		 * News category index.
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function categoryIndex($slug)
		{
			$category = NewsCategory::where('slug', $slug)->firstOrFail();
			$news = News::forCategory($category)->public()->with('category')->orderBy('published_at', 'desc')->get();
			
			return view('news.category', [
				'news' => $news,
				'category' => $category,
			]);
			
		}
		
		/**
		 * Get News of selection in Frontend
		 * @param $slug
		 * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
		 * @throws \Throwable
		 */
		public function selectedNews(Request $request)
		{
			$searchtext = $request->input('searchtext');
			$type = $request->input('news_type');
			$federal = $request->input('federal_state');


			//      9	Sportbund Pfalz
			//      15	Sportbund Rheinhessen
			//      22	Landessportbund Rheinland-Pfalz
			//      23	Sportbund Rheinland
			//      30	Sportjugend Rheinland-Pfalz
			$category_array = [9, 15, 22, 23, 30];
			
			$news = new News;
			$news = $news->public();
			$newsColl = new News;
			$tags = new Tag;
			$state = new State;
			$givenState = $this->getFederalState();
			
			$j = 0;
			
			$ids = array();
			
			$newsColl = $newsColl->public()->where('exclusive', '=', 1)->get();
			
			foreach ($newsColl as $newsItem) {
				
				foreach ($newsItem->states as $fed) {
					
					if ($fed->id == $givenState->id || $fed->id === $givenState->id) {
						$j++;
						
					}
				}
				
				if ($j <= 0) {
					
					array_push($ids, $newsItem->id);
				}
				
				$j = 0;
				
			}
			
			// new changes 07-12-2018
			
			if ($federal !== '' && $federal !== null && $federal !== '0') {
				$state = $state->where('name', '=', $federal)->firstOrFail();
				
				
				if ($state->id != $givenState->id) {
					
					$news->whereHas('states', function ($query) use ($state) {
						$query->where('state_id', $state->id);
					});
					
					$news->where('exclusive', '=', 0);
					
				} else {
					
					$news->whereHas('states', function ($query) use ($state) {
						$query->where('state_id', $state->id);
					});
				}
				
			}
			
			
			if (($type == 1 || $type == '1') && count($ids) > 0) {
				
				$news->where('has_gallery', '=', '0');
				
			}
			
			if ($type == 2 || $type == '2') {
				
				$news->where('has_gallery', '=', '1');
				
			}
			
			if ($type == 3 || $type == '3') {
				
				$news->forCategories($category_array)->with('category');
				
			}
			
			if ($type == 4 || $type == '4') {
				
				$category = NewsCategory::where('slug', '=', 'eure-vereine')->firstOrFail();
				$news->forCategory($category)->with('category');
				
			}
			
			
			if (count($ids) > 0) {
				
				$news->whereNotIn('id', $ids);
				
			}
			
			
			if ($searchtext !== '' && $searchtext !== null) {
				
				$tagid = DB::table('tags')->where('name', 'LIKE', '%' . $searchtext . '%')->value('id');
				
				$news->where('title', 'LIKE', '%' . $searchtext . '%')
					->orWhere('sub_title', 'LIKE', '%' . $searchtext . '%')
					->orWhere('content', 'LIKE', '%' . $searchtext . '%')
					->orWhere('content_teaser', 'LIKE', '%' . $searchtext . '%')
					->orWhereHas('tags', function ($q) use ($tagid) {
						$q->where('tag_id', '=', $tagid);
					});
				
			}
			
			$news->orderBy('created_at', 'desc')->paginate(16);
			
			
			return view('news.category', [
				'news' => $news,
				'federal_state' => $federal,
				'searchtext' => $searchtext,
				'news_type' => $type
			]);
			
		}
		
		
		public function newsItems(Request $request)
		{
			
			$searchtext = $request->input('searchtext');
			$type = $request->input('news_type');
			$federal = $request->input('federal_state');
			$page = $request['page'];

//      9	Sportbund Pfalz
//      15	Sportbund Rheinhessen
//      22	Landessportbund Rheinland-Pfalz
//      23	Sportbund Rheinland
//      30	Sportjugend Rheinland-Pfalz
			$category_array = [9, 15, 22, 23, 30];
			
			
			$news = new News;
			$news = $news->public();
			$tags = new Tag;
			$newsColl = new News;
			$state = new State;
			$givenState = $this->getFederalState();
			
			$j = 0;
			
			$ids = array();
			
			$newsColl = $newsColl->public()->where('exclusive', '=', 1)->get();
			
			foreach ($newsColl as $newsItem) {
				
				
				foreach ($newsItem->states as $fed) {
					
					if ($fed->id == $givenState->id || $fed->id === $givenState->id) {
						$j++;
					}
				}
				
				if ($j <= 0) {
					array_push($ids, $newsItem->id);
				}
				
				$j = 0;
			}
			
			
			// new changes 07-12-2018
			
			if ($federal !== '' && $federal !== null && $federal !== '0') {
				$state = $state->where('name', '=', $federal)->firstOrFail();
				
				
				if ($state->id != $givenState->id) {
					
					$news->whereHas('states', function ($query) use ($state) {
						$query->where('state_id', $state->id);
					});
					
					$news->where('exclusive', '=', 0);
					
				} else {
					
					$news->whereHas('states', function ($query) use ($state) {
						$query->where('state_id', $state->id);
					});
				}
				
			}
			
			
			if (($type == 1 || $type == '1') && count($ids) > 0) {
				
				$news->where('has_gallery', '=', '0');
				
			}
			
			if ($type == 2 || $type == '2') {
				
				$news->where('has_gallery', '=', '1');
				
			}
			
			if ($type == 3 || $type == '3') {
				
				$news->forCategories($category_array)->with('category');
				
			}
			
			if ($type == 4 || $type == '4') {
				
				$category = NewsCategory::where('slug', '=', 'eure-vereine')->firstOrFail();
				$news->forCategory($category)->with('category');
				
			}
			
			
			if (count($ids) > 0) {
				
				$news->whereNotIn('id', $ids);
				
			}
			
			
			if ($searchtext !== '' && $searchtext !== null) {
				
				$tagid = DB::table('tags')->where('name', 'LIKE', '%' . $searchtext . '%')->value('id');
				
				$news->where('title', 'LIKE', '%' . $searchtext . '%')
					->orWhere('sub_title', 'LIKE', '%' . $searchtext . '%')
					->orWhere('content', 'LIKE', '%' . $searchtext . '%')
					->orWhere('content_teaser', 'LIKE', '%' . $searchtext . '%')
					->orWhereHas('tags', function ($q) use ($tagid) {
						$q->where('tag_id', '=', $tagid);
					});
				
				
			}
			
			$newsOverview = $news->orderBy('published_at', 'desc')->paginate(16);
			
			$newsOverview->appends([
				'searchtext' => $request->input('searchtext'),
				'news_type' => $request->input('news_type'),
				'federal_state' => $request->input('federal_state'),
			]);
			$partials = [];
			
			foreach ($newsOverview as $key => $news) {
				if (policy($news)->show(Auth::user(), $news)) {
					array_push($partials, view('news.partials.async-teaser', [
						'news' => $news,
						'key' => $key,
						'page' => $page,
					])->render());
				}
			}
			
			$newsOverview->put('rendered', $partials);
			
			return $newsOverview;
		}
		
		
		/**
		 * Get regional News
		 * @param $slug
		 * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
		 * @throws \Throwable
		 */
		
		public function regionalNews(Request $request)
		{
			$searchtext = $request->input('searchtext');
			$type = $request->input('news_type');

//      9	Sportbund Pfalz
//      15	Sportbund Rheinhessen
//      22	Landessportbund Rheinland-Pfalz
//      23	Sportbund Rheinland
//      30	Sportjugend Rheinland-Pfalz
			$category_array = [9, 15, 22, 23, 30];
			
			$news = new News;
			$state = $this->getFederalState();
			$otherStates = new State;
			$i = 0;
			
			
			$otherFeds = $otherStates->whereNotBetween('id', [$state->id, $state->id]);
			$otherFeds = $otherFeds->pluck('id', 'id');
			$otherFeds = $otherFeds->toArray();
			
			
			if ($searchtext != '' && $searchtext != null) {
				
				$latestNewsFed = $news->public()->whereHas('states', function ($query) use ($state) {
					$query->where('state_id', $state->id);
				})->where('title', 'LIKE', '%' . $searchtext . '%')
					->orWhere('sub_title', 'LIKE', '%' . $searchtext . '%')
					->orWhere('content', 'LIKE', '%' . $searchtext . '%')
					->orWhere('content_teaser', 'LIKE', '%' . $searchtext . '%')
					->orderBy('published_at', 'desc')->get();
				
				$latestNewsOtherFeds = $news->public()->whereHas('states', function ($query) use ($otherFeds) {
					$query->whereIn('state_id', $otherFeds);
				})->where('title', 'LIKE', '%' . $searchtext . '%')
					->orWhere('sub_title', 'LIKE', '%' . $searchtext . '%')
					->orWhere('content', 'LIKE', '%' . $searchtext . '%')
					->orWhere('content_teaser', 'LIKE', '%' . $searchtext . '%')
					->orderBy('published_at', 'desc')->get();
				
				
				$latestNewsSelectedFed = $latestNewsFed->diff($latestNewsOtherFeds);
				
				$i++;
			}
			
			if ($type == 1 || $type == '1') {
				
				$latestNewsFed = $news->public()->whereHas('states', function ($query) use ($state) {
					$query->where('state_id', $state->id);
				})->where('has_gallery', '=', '0')
					->orderBy('published_at', 'desc')->get();
				
				$latestNewsOtherFeds = $news->public()->whereHas('states', function ($query) use ($otherFeds) {
					$query->whereIn('state_id', $otherFeds);
				})->where('has_gallery', '=', '0')
					->orderBy('published_at', 'desc')->get();
				
				$latestNewsSelectedFed = $latestNewsFed->diff($latestNewsOtherFeds);
				
				$i++;
				
			}
			
			if ($type == 2 || $type == '2') {
				
				$latestNewsFed = $news->public()->whereHas('states', function ($query) use ($state) {
					$query->where('state_id', $state->id);
				})->where('has_gallery', '=', '1')
					->orderBy('published_at', 'desc')->get();
				
				$latestNewsOtherFeds = $news->public()->whereHas('states', function ($query) use ($otherFeds) {
					$query->whereIn('state_id', $otherFeds);
				})->where('has_gallery', '=', '1')
					->orderBy('published_at', 'desc')->get();
				
				$latestNewsSelectedFed = $latestNewsFed->diff($latestNewsOtherFeds);
				
				$i++;
			}
			
			if ($type == 3 || $type == '3') {
				
				$latestNewsFed = $news->forCategories($category_array)->public()->with('category')->whereHas('states', function ($query) use ($state) {
					$query->where('state_id', $state->id);
				})->orderBy('published_at', 'desc')->get();
				
				$latestNewsOtherFeds = $news->forCategories($category_array)->public()->with('category')->whereHas('states', function ($query) use ($otherFeds) {
					$query->whereIn('state_id', $otherFeds);
				})->orderBy('published_at', 'desc')->get();
				
				$latestNewsSelectedFed = $latestNewsFed->diff($latestNewsOtherFeds);
				
				$i++;
			}
			
			if ($type == 4 || $type == '4') {
				
				$category = NewsCategory::where('slug', '=', 'eure-vereine')->firstOrFail();
				
				$latestNewsFed = $news->forCategory($category)->public()->with('category')->whereHas('states', function ($query) use ($state) {
					$query->where('state_id', $state->id);
				})->orderBy('published_at', 'desc')->get();
				
				$latestNewsOtherFeds = $news->forCategory($category)->public()->with('category')->whereHas('states', function ($query) use ($otherFeds) {
					$query->whereIn('state_id', $otherFeds);
				})->orderBy('published_at', 'desc')->get();
				
				$latestNewsSelectedFed = $latestNewsFed->diff($latestNewsOtherFeds);
				
				$i++;
			}
			
			if ($i <= 0) {
				
				$latestNewsFed = $news->public()->whereHas('states', function ($query) use ($state) {
					$query->where('state_id', $state->id);
				})->orderBy('published_at', 'desc')->get();
				
				$latestNewsOtherFeds = $news->public()->whereHas('states', function ($query) use ($otherFeds) {
					$query->whereIn('state_id', $otherFeds);
				})->orderBy('published_at', 'desc')->get();
				
				
				$latestNewsSelectedFed = $latestNewsFed->diff($latestNewsOtherFeds);
				
			}
			
			
			$latestNewsSelectedFed = $latestNewsSelectedFed->all();
			
			return view('news.regional', [
				'news' => $latestNewsSelectedFed,
				'searchtext' => $searchtext,
				'news_type' => $type
			]);
			
		}
		
		
		public function regionalItems(Request $request)
		{
			
			$searchtext = $request->input('searchtext');
			$type = $request->input('news_type');
			$page = $request['page'];

//      9	Sportbund Pfalz
//      15	Sportbund Rheinhessen
//      22	Landessportbund Rheinland-Pfalz
//      23	Sportbund Rheinland
//      30	Sportjugend Rheinland-Pfalz
			$category_array = [9, 15, 22, 23, 30];
			
			$news = new News;
			$state = $this->getFederalState();
			$otherStates = new State;
			
			
			$otherFeds = $otherStates->whereNotBetween('id', [$state->id, $state->id]);
			$otherFeds = $otherFeds->pluck('id', 'id');
			$otherFeds = $otherFeds->toArray();
			
			$latestNewsFed = $news->public()->whereHas('states', function ($query) use ($state) {
				$query->where('state_id', $state->id);
			})->orderBy('created_at', 'desc')->get();
			
			$latestNewsOtherFeds = $news->public()->whereHas('states', function ($query) use ($otherFeds) {
				$query->whereIn('state_id', $otherFeds);
			})->orderBy('created_at', 'desc')->get();
			
			$latestNewsSelectedFedColl = $latestNewsFed->diff($latestNewsOtherFeds);
			$latestNewsSelectedFed = new News;
			foreach ($latestNewsSelectedFedColl as $fedColl) {
				
				$latestNewsSelectedFed->push($fedColl);
			}
			
			
			if ($searchtext != '' && $searchtext != null) {
				$latestNewsSelectedFed = $latestNewsSelectedFed->where('title', 'LIKE', '%' . $searchtext . '%')
					->orWhere('sub_title', 'LIKE', '%' . $searchtext . '%')
					->orWhere('content', 'LIKE', '%' . $searchtext . '%')
					->orWhere('content_teaser', 'LIKE', '%' . $searchtext . '%');
			}
			
			switch ($type) {
				
				case 1 || '1':
					$latestNewsSelectedFed = $latestNewsSelectedFed->where('has_gallery', '=', '0')
						->orWhereNull('has_gallery')
						->orWhere('has_gallery', '=', '');
					break;
				
				case 2 || '2':
					$latestNewsSelectedFed = $latestNewsSelectedFed->where('has_gallery', '=', '1');
					break;
				
				case 3 || '3':
					$latestNewsSelectedFed = $latestNewsSelectedFed->forCategories($category_array)->public()->with('category');
					break;
				
				case 4 || '4':
					$category = NewsCategory::where('slug', '=', 'eure-vereine')->firstOrFail();
					$latestNewsSelectedFed = $latestNewsSelectedFed->forCategory($category)->public()->with('category');
					break;
			}
			
			$newsOverview = $latestNewsSelectedFed->orderBy('published_at', 'desc')->paginate(16);
			
			$newsOverview->appends([
				'searchtext' => $request->input('searchtext'),
				'news_type' => $request->input('news_type'),
			]);
			$partials = [];
			
			foreach ($newsOverview as $key => $news) {
				if (policy($news)->show(Auth::user(), $news)) {
					array_push($partials, view('news.partials.regional-async-teaser', [
						'news' => $news,
						'key' => $key,
						'page' => $page,
					])->render());
				}
			}
			
			$newsOverview->put('rendered', $partials);
			
			return $newsOverview;
		}
		
		
		public function categoryItems($id)
		{
			$category = NewsCategory::where('id', $id)->first();
			$newsOverview = News::forCategory($category)->public()->with('category')->orderBy('published_at', 'desc')->paginate(16);
			
			$partials = [];
			
			foreach ($newsOverview as $key => $news) {
				if (policy($news)->show(Auth::user(), $news)) {
					array_push($partials, view('news.partials.async-teaser', [
						'news' => $news,
						'category' => $category,
						'key' => $key,
					])->render());
				}
			}
			
			$newsOverview->put('rendered', $partials);
			
			return $newsOverview;
		}
		
		
		/**
		 * News detail page.
		 *
		 * @return \Illuminate\Http\Response
		 */
			
		public function show($categoryId, $newsSlug, Request $request)
		{
			$campaign = RandomCampaign::instance()->getRandomForNews();
			
			$nextImgUrl = '';
			$prevImgUrl = '';
			$imageRes = '';
			$news = News::with('category')->where('slug', $newsSlug)->firstOrFail();
			
			$category = NewsCategory::where('id', $categoryId)->firstOrFail();
			$related = News::forCategory($category)->public()->orderBy('published_at', 'desc')->take(3)->get()->keyBy('id')->except($news->id);
			$latest = $news->latestUpdatedNews(3);
			$latestNews = $news->latestNews(9);
			
			$newsImgCnt = $news->images()->count();
			$seq = trim($request->input('seq', ''));
			if (!$seq) {
				$seq = 1;
			}
			
			if ($newsImgCnt) {
				$newsImages = $news->images();
				$nextSeq = $seq + 1;
				foreach ($news->images as $key => $image) {
					if (($key + 1) == $seq) {
						$imageRes = $image;
					}
					if (($key + 1) == $nextSeq) {
						$nextImgUrl = route('news.detail', [$news->category_id, $news->slug]) . '?seq=' . $nextSeq;
					}
					if ($key == ($seq - 1) && $seq > 1) {
						if ($seq == 2)
							$prevImgUrl = route('news.detail', [$news->category_id, $news->slug]);
						else
							$prevImgUrl = route('news.detail', [$news->category_id, $news->slug]) . '?seq=' . ($seq - 1);
					}
				}
			}
			
			if (policy($news)->show(Auth::user(), $news)) {
				return view('news.detail', [
					'news' => $news,
					'related' => $related,
					'latest' => $latest,
					'popularNews' => $latestNews,
					'seq' => $seq,
					'imageRes' => $imageRes,
					'nextImgUrl' => $nextImgUrl,
					'prevImgUrl' => $prevImgUrl,
					'campaign' => $campaign,
				]);
			} else {
				abort(403);
			}
		}
		
		
		public function related($categoryId, $newsId)
		{
			$user = Auth::user();
			$currentNews = News::find($newsId);
			$category = NewsCategory::find($categoryId);
			$related = News::forCategory($category)->public()->orderBy('published_at', 'desc')->get()->keyBy('id')->except($currentNews->id);
			
			$perPage = 3;
			$currentPage = Paginator::resolveCurrentPage() ?: 1;
			$pagerIndex = ($currentPage * $perPage) - $perPage;
			$paginatedClients = collect($related)->slice($pagerIndex, $perPage);
			
			$news = new Paginator($paginatedClients, $related->count(), $perPage, $currentPage, [
				'path' => Paginator::resolveCurrentPath()]);
			
			$partials = [];
			
			foreach ($news as $item) {
				if (policy($item)->show($user, $item)) {
					array_push($partials, view('news.partials.related-item', [
						'news' => $item
					])->render());
				}
			}
			
			$news->put('rendered', $partials);
			
			return $news;
		}
		
	}