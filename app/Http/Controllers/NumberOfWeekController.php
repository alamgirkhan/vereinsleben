<?php

namespace Vereinsleben\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Vereinsleben\NumberOfWeek;

class NumberOfWeekController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $numbers = NumberOfWeek::all();
      return view('admin.number-of-week.index', ['numbers' => $numbers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    return view('admin.number-of-week.edit', ['numberOfWeek' => new NumberOfWeek()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	    $errors = [];
      $newNumber = new NumberOfWeek();
      
	    if ($request->hasFile('image')) {
		    $filename = time() . '-' . str_slug($request->image->getClientOriginalName(), '-') . '.' . $request->image->getClientOriginalExtension();
		    Image::make($request->image)->save(public_path('/uploaded/number-of-week/' . $filename));
		    $newNumber->image = $filename;
	    } else {
		    $errors[] = "Das Bild darf nicht leer sein";
	    }
	    
	    $newNumber->number = $request->number;
	    $newNumber->short_description = $request->short_description;
	    $newNumber->opacity = $request->opacity;
	    $newNumber->sub_line = $request->sub_line;
	    $newNumber->published_from = date('Y-m-d H:i:s', strtotime($request->published_from));
	
	    if (count($errors) > 0) {
		    return back()->withInput()->with('flash-error', $errors);
	    }
	    
	    $newNumber->save();
	    return redirect()->route('number-of-week.index')->with('flash-success', 'Erfolgreich erstellt!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
	    $numberOfWeek = NumberOfWeek::find($id);
	    return view('admin.number-of-week.edit', compact('numberOfWeek'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    $errors = [];
	    $number = NumberOfWeek::find($id);
	    
	    if ($request->hasFile('image')) {
		    $filename = time() . '-' . str_slug($request->image->getClientOriginalName(), '-') . '.' . $request->image->getClientOriginalExtension();
		    Image::make($request->image)->save(public_path('/uploaded/number-of-week/' . $filename));
		    $number->image = $filename;
	    }
	    
	    if (!$request->hasFile('image') && !isset($number->image)) {
		    $errors[] = "Das Bild darf nicht leer sein";
	    }
	    
	    $number->number = $request->number;
	    $number->short_description = $request->short_description;
	    $number->opacity = $request->opacity;
	    $number->sub_line = $request->sub_line;
	    $number->published_from = date('Y-m-d H:i:s', strtotime($request->published_from));
	
	    if (count($errors) > 0) {
		    return back()->withInput()->with('flash-error', $errors);
	    }
	    
	    $number->save();
	    return redirect()->route('number-of-week.index')->with('flash-success', 'Erfolgreich aktualisiert!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
	    $number = NumberOfWeek::find($id);
	    $number->delete();
	    return redirect()->route('number-of-week.index')->with('flash-success', 'Erfolgreich gelöscht!');
    }
}
