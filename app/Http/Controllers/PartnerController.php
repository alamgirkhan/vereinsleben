<?php

namespace Vereinsleben\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

use Vereinsleben\Http\Controllers\Controller;
use Vereinsleben\Partner;
use Vereinsleben\Meta;
use \DB;
use Vereinsleben\State;

class PartnerController extends BaseController
{

    /**
     * Partner index.
     *
     * @return \Illuminate\Http\Response
     */
    public function partnerIndex()
    {
        $partner = new Partner;
        $partner = $partner->byState($this->getFederalState())->orderBy('pos', 'asc')->get();

        return view('partner.list', [
            'partner' => $partner,
        ]);
    }


    public function partnerList()
    {
        $partials = [];
	
		    $partners = new Partner;
		    $partnerOverview = $partners->byState($this->getFederalState())->where('published', 1)->orderBy('pos', 'asc')->paginate(12);

        foreach ($partnerOverview as $partner){
            array_push($partials, view('partner.single', [
                'partner' => $partner,
            ])->render());
        }

        $partnerOverview->put('rendered', $partials);

        return $partnerOverview;
    }


    /**
     * Partner detail page.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($slug, Request $request)
    {
        $partner = Partner::where('slug', $slug)->firstOrFail();
        $website = $partner->website;
        if (strpos($website,"//") !== false)
        {
            $temp = explode("//", $website);
            $website = $temp[1];
        }else{
            $partner->website = "http://" . $website;
        }
        $temp = explode("/",$website);
        $website = $temp[0];
        $partner->websiteKurz = $website;

        return view('partner.detail', [
            'partner' => $partner,
        ]);
    }

    public function index()
    {
        return view('admin.partner.index', ['partners' => Partner::orderBy('pos', 'asc')->orderBy('created_at', 'desc')->get()]);
    }

    public function add()
    {
        return view('admin.partner.edit', ['partner' => new Partner()]);
    }

    public function store(Request $request)
    {
		    if (!isset($request->federal_states)) {
			    return \Redirect::route('admin.partner.add')->with('flash-error', 'Bitte mindestens ein Bundesland auswählen!');
		    }
    	
        $partner = new Partner;

        $partner->title = $request->title;
        $partner->sub_title = $request->sub_title;
        $partner->slug = $request->slug;
        $partner->pos = $request->pos;
        $partner->content_teaser = $request->content_teaser;
        $partner->content = $request['content'];
        $partner->email = $request->email;
        $partner->phone = $request->phone;
        $partner->address = $request->address;
        $partner->website = $request->website;
        $partner->published = $request->published;
        $partner->main_image = $request['main_image'];
        $partner->logo = $request['logo'];

        $partner->save();

        $this->saveMetaData($request, $partner);
	      $this->saveFederalstate($request->federal_states, $partner);

        return \Redirect::route('admin.partner.index')->with('flash-success', 'Die partner wurde angelegt!');
    }


    public function edit($id)
    {
        $partner = Partner::find($id);
        if (!isset($partner->meta)){
            $partner->meta()->create([]);
        }

        return view('admin.partner.edit', [
            'partner' => Partner::find($id),
        ]);
    }

    public function update(Request $request, $id)
    {
        $partner = Partner::findOrFail($id);
	
		    if (!isset($request->federal_states)) {
			    return \Redirect::route('admin.partner.edit', [
				    'partner' => $partner,
			    ])->with('flash-error', 'Bitte mindestens ein Bundesland auswählen!');
		    }

        $partner->title = !empty($request['title']) ? $request['title'] : null;
        $partner->sub_title = !empty($request['sub_title']) ? $request['sub_title'] : null;
        $partner->slug = !empty($request['slug']) ? $request['slug'] : null;
        $partner->pos = !empty($request['pos']) ? $request['pos'] : null;
        $partner->content_teaser = !empty($request['content_teaser']) ? $request['content_teaser'] : null;
        $partner->content = !empty($request['content']) ? $request['content'] : null;
        $partner->email = !empty($request['email']) ? $request['email'] : null;
        $partner->phone = !empty($request['phone']) ? $request['phone'] : null;
        $partner->address = !empty($request['address']) ? $request['address'] : null;
        $partner->website = !empty($request['website']) ? $request['website'] : null;
        $partner->published = $request['published'];
        $partner->main_image = $request['main_image'];
        $partner->logo = $request['logo'];
        $partner->save();

        $this->saveMetaData($request, $partner);
	      $this->saveFederalstate($request->federal_states, $partner);

        return \Redirect::route('admin.partner.index')->with('flash-success', 'Deine Änderungen wurden gespeichert!');
    }

    /**
     * @param FormRequest $request
     * @param $partner
     */
    public function saveMetaData(Request $request, $partner)
    {
        $meta = new Meta();

        $meta->meta_bild_alt = $request->meta_bild_alt;
        $meta->meta_description = $request->meta_description;
        $meta->meta_keywords = $request->meta_keywords;
        $meta->meta_author = $request->meta_author;
        $meta->meta_language = $request->meta_language;

        if ($partner->meta === null){
            $partner->meta()->create($meta->toArray());
        } else{
            $partner->meta()->update($meta->toArray());
        }
    }

    public function destroy(Request $request, $id)
    {
        $partner = Partner::find($id);

        $partner->delete();

        return \Redirect::route('admin.partner.index')->with('flash-success', 'Datensatz erfolgreich gelöscht');
    }
	
		public function saveFederalstate($federalstates, $partner)
		{
			$partner->states()->detach();
			if ($federalstates != null){
				foreach ($federalstates as $stateId){
					if ($partner->states()->where('state_id', $stateId)->first() === null){
						$state = State::find($stateId);
						$partner->states()->save($state);
					}
				}
			}
		}
}