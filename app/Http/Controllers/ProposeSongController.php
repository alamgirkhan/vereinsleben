<?php

namespace Vereinsleben\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Vereinsleben\Club;
use Vereinsleben\ProposeSong;
use Auth;

class ProposeSongController extends BaseController
{
	public function index()
	{
		$suggestedClub = new Club;
		if (Auth::user()) {
			$user = Auth::user();
			$suggestedClub = $suggestedClub->where('suggested_from', '=', $user->id)->orderBy('created_at', 'desc')->first();
			return view('propose-song.index', [
				'user' => $user,
				'suggested' => $suggestedClub
			]);
		} else {
			return view('propose-song.index', [
				'user' => null,
				'suggested' => $suggestedClub
			]);
		}
	}
	
	public function list() {
		$proposeSongList = ProposeSong::all();
		return view('admin.propose-song.list', ['proposeSongList' => $proposeSongList]);
	}
	
	public function store(Request $request) {
		if (Auth::check()){
			$club = Club::whereName($request->club_name)->firstOrFail();
			$user = Auth::user();
			
			$newProposeSong = new ProposeSong;
			$newProposeSong->title_song = $request->title_song;
			$newProposeSong->artist_name = $request->artist_name;
			$newProposeSong->first_name = $request->first_name;
			$newProposeSong->last_name = $request->last_name;
			$newProposeSong->email = $request->email;
			$newProposeSong->club_name = $request->club_name;
			$newProposeSong->username = $user->username;
			$newProposeSong->user_id = $user->id;
			$newProposeSong->club_id = $club->id;
			$newProposeSong->club_slug = $club->slug;
			$newProposeSong->save();
			
			return view('propose-song.thanks', [
				'club' => $club->name,
			]);
		}
	}
	
	public function destroy($id) {
		if (Auth::user()->isAdmin()){
			$deleteProposeSong = ProposeSong::findOrFail($id);
			$deleteProposeSong->delete();
			return back()->withInput()->with('flash-success', 'Eintrag wurde gelöscht.');
		} else{
			return back()->withInput()->with('flash-error', 'Sie haben keine Berechtigung!');
		}
	}
	
	public function edit($id) {
		$editProposeSong = ProposeSong::findOrFail($id);
		return view('admin.propose-song.edit', ['editProposeSong' => $editProposeSong]);
	}
	
	public function view($id) {
		$proposeSong = ProposeSong::findOrFail($id);
		return view('admin.propose-song.view', ['proposeSong' => $proposeSong]);
	}
	
	public function update(Request $request) {
		if (Auth::user()->isAdmin()){
			$club = Club::whereName($request->club_name)->firstOrFail();
			$updateProposeSong = ProposeSong::findOrFail($request->id);
			
			$updateProposeSong->title_song = $request->title_song;
			$updateProposeSong->artist_name = $request->artist_name;
			$updateProposeSong->first_name = $request->first_name;
			$updateProposeSong->last_name = $request->last_name;
			$updateProposeSong->email = $request->email;
			$updateProposeSong->club_name = $request->club_name;
			$updateProposeSong->club_id = $club->id;
			$updateProposeSong->club_slug = $club->slug;
			$updateProposeSong->save();
			
			return Redirect::route('admin.propose-song.list')->with('flash-success', 'Deine Änderungen wurden gespeichert!');
		} else {
			return Redirect::route('admin.propose-song.list')->with('flash-error', 'Sie haben keine Berechtigung!');
		}
	}
}
