<?php
	
namespace Vereinsleben\Http\Controllers;

use Vereinsleben\Helpers\RandomCampaign;

class RandomCampaignController extends BaseController
{
	public function index() {
		return response()->json(RandomCampaign::instance()->getRandomForStream());
	}
}