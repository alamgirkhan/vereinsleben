<?php
	
	namespace Vereinsleben\Http\Controllers;
	
	use Auth;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Redirect;
	use Illuminate\Support\Facades\URL;
	use Vereinsleben\Club;
	use Vereinsleben\Review;
	use Vereinsleben\ReviewReport;
	
	class ReviewController extends Controller
	{
		/**
		 * Display a listing of the resource.
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function index($sort_by)
		{
			$reivews = [];
			if ($sort_by == 'all') {
				$reivews = Review::orderBy('created_at', 'desc')->get();
				return view('admin.reviews.list', ['sort_by' => $sort_by, 'reviews' => $reivews]);
			} else {
				$reivewsReport = ReviewReport::orderBy('created_at', 'desc')->get();
				foreach ($reivewsReport as $reviewReport) {
					$review = $reviewReport->review()->first();
					if (isset($review)) {
						$reivews[] = $reviewReport->review()->first();
					}
				}
				
				return view('admin.reviews.list', ['sort_by' => $sort_by, 'reviews' => $reivews]);
			}
		}
		
		/**
		 * Show the form for creating a new resource.
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function create()
		{
			//
			
		}
		
		/**
		 * Store a newly created resource in storage.
		 *
		 * @param  \Illuminate\Http\Request $request
		 * @return \Illuminate\Http\Response
		 */
		public function store(Request $request)
		{
			if (!$request->rating && (!isset($request->parent_id) || $request->parent_id == 0)) {
				return Redirect::to(URL::previous() . "#bewertungen")->with([
					'rating-error' => 'Bitte die Sterne nicht vergessen!',
					'commentTmp' => $request->comment,
				]);
			}
			if ($request->comment == '' && (!isset($request->parent_id) || $request->parent_id == 0)) {
				return Redirect::to(URL::previous() . "#bewertungen")->with([
					'rating-error' => 'Bitte noch einen Bewertungstext ergänzen!',
					'ratingTmp' => $request->rating
				]);
			}
			$club = Club::where('id', '=', $request->club_id)->firstOrFail();
			
			if (Auth::user()->isAdmin()) {
				if (!isset($request->parent_id) || $request->parent_id == 0) {
					$numberReview = count($club->reviews()->where('user_id', '=', $request->user_id)->get());
					if ($numberReview > 0) {
						return Redirect::to(URL::previous() . "#bewertungen")->with('rating-error', 'Du kannst diesen Verein nur einmal bewerten.');
					}
				}
			} else if (!(Auth::check() && (Auth::user()->isClubAdmin($club)))) {
				$numberReview = count($club->reviews()->where('user_id', '=', $request->user_id)->get());
				if ($numberReview > 0) {
					return Redirect::to(URL::previous() . "#bewertungen")->with('rating-error', 'Du kannst diesen Verein nur einmal bewerten.');
				}
			} else {
				if (!isset($request->parent_id) || $request->parent_id == 0) {
					return Redirect::to(URL::previous() . "#bewertungen")->with('rating-error', 'Please choose a review to reply');
				}
			}
			
			$newReview = new Review;
			$newReview->club_id = $request->club_id;
			$newReview->user_id = $request->user_id;
			$newReview->rating = $request->rating;
			$newReview->comment = $request->comment;
			$newReview->parent_id = $request->parent_id;
			$newReview->save();
			return Redirect::to(URL::previous() . "#bewertungen")->with('rating-success', $newReview->parent_id == 0 ? 'Vielen Dank für Deine Bewertung.' : 'Antwortete auf den Kommentar');
		}
		
		/**
		 * Display the specified resource.
		 *
		 * @param  int $id
		 * @return \Illuminate\Http\Response
		 */
		public function show($id)
		{
			//
		}
		
		/**
		 * Show the form for editing the specified resource.
		 *
		 * @param  int $id
		 * @return \Illuminate\Http\Response
		 */
		public function edit($id)
		{
			//
		}
		
		/**
		 * Update the specified resource in storage.
		 *
		 * @param  \Illuminate\Http\Request $request
		 * @param  int $id
		 * @return \Illuminate\Http\Response
		 */
		public function update(Request $request, $id)
		{
			//
		}
		
		/**
		 * Remove the specified resource from storage.
		 *
		 * @param  int $id
		 * @return \Illuminate\Http\Response
		 */
		
		public function destroy($id)
		{
			$deleteReview = Review::findOrFail($id);
			$deleteReview->delete();
			return Redirect::to(URL::previous() . "#bewertungen")->with('rating-success', 'Die Bewertung wurde entfernt.');
		}
		
		public function ratingNoLogin() {
			return Redirect::to(URL::previous() . "#bewertungen")->with('rating-error', 'Um einen Verein zu bewerten, musst du eingeloggt sein!');
		}
	}
