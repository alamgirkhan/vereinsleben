<?php
	
	namespace Vereinsleben\Http\Controllers;
	
	use Illuminate\Http\Request;
	use Vereinsleben\ReviewReport;
	use Illuminate\Support\Facades\Redirect;
	use Illuminate\Support\Facades\URL;
	
	class ReviewReportController extends Controller
	{
		/**
		 * Display a listing of the resource.
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function index()
		{
            return view('admin.reviews.list', ['report' => ReviewReport::orderBy('created_at', 'desc')->get()]);
		}
		
		/**
		 * Show the form for creating a new resource.
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function create()
		{
			//
		}
		
		/**
		 * Store a newly created resource in storage.
		 *
		 * @param  \Illuminate\Http\Request $request
		 * @return \Illuminate\Http\Response
		 */
		public function store(Request $request)
		{
			$reviewReport = new ReviewReport;
			$reviewReport->review_id = $request->review_id;
			$reviewReport->user_id = $request->user_id;
			$reviewReport->save();
			return Redirect::to(URL::previous() . "#bewertungen")->with('rating-success', 'Die Bewertung wurde gemeldet');
		}
		
		/**
		 * Display the specified resource.
		 *
		 * @param  int $id
		 * @return \Illuminate\Http\Response
		 */
		public function show($id)
		{
			//
		}
		
		/**
		 * Show the form for editing the specified resource.
		 *
		 * @param  int $id
		 * @return \Illuminate\Http\Response
		 */
		public function edit($id)
		{
			//
		}
		
		/**
		 * Update the specified resource in storage.
		 *
		 * @param  \Illuminate\Http\Request $request
		 * @param  int $id
		 * @return \Illuminate\Http\Response
		 */
		public function update(Request $request, $id)
		{
			//
		}
		
		/**
		 * Remove the specified resource from storage.
		 *
		 * @param  int $id
		 * @return \Illuminate\Http\Response
		 */
		public function destroy($id)
		{
			$deleteReviewReport = ReviewReport::findOrFail($id);
			$deleteReviewReport->delete();
			return Redirect::to(URL::previous() . "#bewertungen")->with('rating-success', 'The rating report is deleted');
		}
	}
