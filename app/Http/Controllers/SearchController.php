<?php

namespace Vereinsleben\Http\Controllers;

use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Input;
use League\Csv\Writer;
use Vereinsleben\Http\Requests;

//use Mapper;
use Geocoder;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

use Vereinsleben\Club;
use Vereinsleben\Bandage;
use Vereinsleben\User;
use Vereinsleben\City;
use Vereinsleben\CityZipcode;
use Vereinsleben\Sport;
use Vereinsleben\News;
use Vereinsleben\Event;

use Auth;

class SearchController extends BaseController
{
    public function __construct(Request $request){
        parent::__construct($request);
    }
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request)
    {

    }

    /**
     * Paginate search results. Workaround since we cannot use the paginate() method because there is currently a bug in
     * combination with HAVING queries.
     */
    protected function paginateSearchResults(Request $request, $query, $perPage = 30)
    {

        $curPage = $request->input('page'); // reads the query string, defaults to 1

        // clone the query to make 100% sure we don't have any overwriting
        $itemQuery = clone $query;

        // this does the sql limit/offset needed to get the correct subset of items
        $items = $itemQuery->forPage($curPage, $perPage)->get();

        // to be sure there are no clubs which may not be visible - filter the clubs by policy
        $user = Auth::user();
        $items = $items->filter(function ($item) use ($user) {
            return policy($item)->show($user, $item);
        });

        // manually run a query to select the total item count.
        // use a nested query, necessary because we have a HAVING query.
        $totalItems = \DB::table(\DB::raw("({$query->toSql()}) as sub"))
            ->mergeBindings($query->getQuery())// you need to get underlying Query Builder
            ->count();
        
        // make the paginator, which is the same as returned from paginate()
        // all() will return an array of models from the collection.
        $paginatedItems = new Paginator($items->all(), $totalItems, $perPage, Paginator::resolveCurrentPage(), [
            'path' => Paginator::resolveCurrentPath()]);

        // append the search queries to the paginator link
        $paginatedItems->appends([
            'city'     => $request->input('city'),
            'keyword'  => $request->input('keyword'),
            'sport'    => $request->input('sport'),
            'distance' => $request->input('distance'),
            'type'     => $request->input('type'),
            'ajax'     => $request->input('ajax'),
        ]);

        return $paginatedItems;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function clubSearch(Request $request)
    {

        $partials = [];
        $users = '';
        $clubs = '';
        $news = new News;
        $events = '';
        $bandages = '';
        $latestNews = new News();
        $userCount = 0;
        $clubCount = 0;
        $newsCount = 0;
        $eventsCount = 0;
        $bandageCount = 0;

        $cityquery = trim($request->input('city', ''));
        $distance = trim($request->input('distance', '25'));
        $keyword = trim($request->input('keyword', ''));
        if (!$keyword){
            $keyword = trim($request->input('keyword1', ''));
        }
        $sportquery = trim($request->input('sport', ''));
        $type = trim($request->input('type', ''));
        $ajax = trim($request->input('ajax', ''));

        /*
        // in case no city was entered (e.g. initial call) try to find out the location by FreeGeoIP request
        // (max. 10,000 queries per hour - we might cache the results someday)
        if ($cityquery === '') {
            try {
                $ipGeocode = Geocoder::geocode($request->ip());
            } catch (NoResultException $e) {
                $ipGeocode = null;
            }
            if ($ipGeocode !== null && $ipGeocode->getLatitude() !== 0 && $ipGeocode->getCity() !== '') {
                $cityquery = $ipGeocode->getCity();
            }
        }
        */
        if ($type == 'vereine' || $type == ''){
            if ($cityquery !== '' || $keyword !== '' || $sportquery !== ''){

                $clubsQuery = Club::where('published', '=', 1);
                $city = null;

                if ($cityquery == '' && $sportquery == ''){

                    $clubsQuery = $clubsQuery->where(function($query) use ($keyword){
	                    $query->orWhere(DB::raw("REPLACE(name, 'ß', 'ss')"), 'LIKE', DB::raw("concat('%', REPLACE('".$keyword."', 'ß', 'ss'), '%')"))->orWhere('city', $keyword)->orWhere('zip', $keyword);
                    }
                    );

                    $sport = Sport::where('title', '=', $keyword)->first();

                    if ($sport !== null){
                        $clubsQuery = $clubsQuery->orWhereHas('sports', function($query) use ($sport){
                            $query->where('sport.id', '=', $sport->id);
                        });
                    }

                } else{

                    if ($cityquery != ''){
                        if (is_numeric(trim($cityquery))){
                            $zipcode = CityZipcode::where('zip', '=', $cityquery)->first();
                            $city = (!is_null($zipcode)) ? $zipcode->city : null;
                        } else{
                            $city = City::where('name', 'LIKE', '%' . $cityquery . '%')->first();
                        }

                        if ($distance != null){
                            $clubsQuery = $clubsQuery->closest($city->lat, $city->lon, $distance);
                        } else{
                            $clubsQuery = $clubsQuery->whereRaw('1 = 2'); # TODO: ??
                        }
                    }

                    if ($keyword != '' && $keyword != $sportquery){
                        $clubsQuery = $clubsQuery->where(DB::raw("REPLACE(name, 'ß', 'ss')"), 'LIKE', DB::raw("concat('%', REPLACE('".$keyword."', 'ß', 'ss'), '%')"));
                    }

                    if ($sportquery != ''){
                        $sport = Sport::where('title', '=', $sportquery)->first();
                    } else{
                        $sport = Sport::where('title', '=', $keyword)->first();
                    }

                    if ($sport !== null){

                        if (trim($request->input('sport', '')) == ''){
                            $clubsQuery = $clubsQuery->orWhereHas('sports', function($query) use ($sport){
                                $query->where('sport.id', '=', $sport->id);
                            });
                        } else{
                            $clubsQuery = $clubsQuery->whereHas('sports', function($query) use ($sport){
                                $query->where('sport.id', '=', $sport->id);
                            });
                        }
                    }

                }
                $clubs = $this->paginateSearchResults($request, $clubsQuery, 6);
                $clubCount = $clubs->total();

                /*if ($city != null && $city->lat !== null && $city->lng !== null) {
                    $map = Mapper::map($city->lat, $city->lon, ['zoom' => 10, 'clusters' => ['size' => 4, 'center' => true, 'zoom' => 20], 'marker' => false]);
                } else {
                    $map = Mapper::map('51.0778879', '5.9595168', ['zoom' => 5, 'marker' => false]); # default germany map
                }*/

                /*foreach ($clubs as $club) {
                    if ($club->lat !== null && $club->lng !== null) {
                        Mapper::informationWindow($club->lat, $club->lng, link_to_route('club.detail', $club->name, $club->slug));
                    }
                }*/

            } else{
                $clubs = null;
                $clubCount = 0;
                //$map = Mapper::map('51.0778879', '5.9595168', ['zoom' => 5, 'marker' => false]); # default germany map
            }

            if ($ajax){
                foreach ($clubs as $club){
                    array_push($partials, view('search.partials.result.item.club', ['club' => $club])->render());
                }

                if ($clubs->nextPageUrl()){
                    array_push($partials, view('search.partials.result.more-result', ['more' => $clubs, 'name' => 'VEREINE', 'type' => 'club'])->render());
                }

                $clubs->put('rendered', $partials);

                return $clubs;
            }
        }

        if ($type == 'personen' || $type == ''){
            $users = $this->userSearch($request);
            if ($users){
                $userCount = $users->total();
            }
        }
        if ($type == 'news' || $type == ''){
            $news = $this->newsSearch($request);
            if (!empty($news)){
                $newsCount = $news->total();
            }
        }
        if ($type == 'events' || $type == ''){
            $events = $this->eventsSearch($request);
            if (!empty($events)){
                $eventsCount = $events->total();
            }
        }


        if (Auth::user()){
            $username = Auth::user()->username;
        } else{
            $username = 'PUBLIC';
        }
        $resultcount = $userCount + $clubCount;

        $this->logSearch($request, $username, $resultcount);


//        if($type == 'verbande' || $type == '') {
        if($type == '') {
            $bandages = $this->bandageSearch($request);
            if ($bandages){
                $bandageCount = $bandages->total();
            }
        }
        return view('search.index', [
            //'map'         => Mapper::render(),
            'clubs'       => $clubs,
            'clubCount'   => $clubCount,
            'users'       => $users,
            'userCount'   => $userCount,
            'newsCount'   => $newsCount,
            'bandages'    => $bandages,
            'bandageCount'=> $bandageCount,
            'news'      => $news,
            'events'       => $events,
            'eventsCount'   => $eventsCount,
            'latest'      => $latestNews->latestUpdatedNews(3),
//            'popularNews' => $news->latestNews(9),
            'query'       => [
                'city'     => $cityquery,
                'distance' => $distance,
                'keyword'  => $keyword,
                'sport'    => $sportquery,
                'type'     => $type,
            ]
        ]);
    }

    
    public function clubSearchNew (Request $request) {
    	if (!$request->keyword) {
		    return view('search.index');
	    }
	    $keyword = $request->keyword;
    	$keyword = preg_replace('/[^a-zA-Z0-9äÄöÖüÜß ]/', '', $keyword); //Filter only allowed signs
        //$keyword = strtolower($keyword);

        $umlaute = array();
        $umlaute["ß"] = "ss";
        $umlaute["ä"] = "ae";
        $umlaute["ö"] = "oe";
        $umlaute["ü"] = "ue";

        $specialNames = array();
        $specialNames[] = "kung fu";

        foreach($umlaute as $umlaut=>$changeTo)
        {
            $keyword = str_replace($umlaut, $changeTo, $keyword);
        }

        foreach($specialNames as $specialName)
        {
            $changeTo = str_replace(" ", "", $specialName);
            $keyword = str_replace($specialName, $changeTo, $keyword);
        }

        $keywords = explode(" ", $keyword);
        foreach($keywords as $key=>$keyword)
        {
            if (strlen($keyword)<2)unset($keywords[$key]);
        }

        $thisStringsAreSports = array();
        if (is_array($keywords) and count($keywords)>0)
        {
            //Search for IDs in Sportstable
            $resultClubIDs = array();
            $resultSportIDs = array();
            foreach($keywords as $keyword)
            {
                $keyword = trim($keyword);
                foreach($specialNames as $specialName)
                {
                    $changeFrom = str_replace(" ", "", $specialName);
                    if ($keyword == $changeFrom)$keyword = $specialName;
                }

                if (strlen($keyword)>1)
                {
                    //$sql = "SELECT id FROM sport WHERE TRIM(REPLACE(REPLACE(REPLACE(REPLACE(title, 'ß', 'ss'), 'ä', 'ae'), 'ö', 'oe'), 'ü', 'ue')) LIKE '$keyword' AND deleted_at IS NULL";
                    $sql = "SELECT id FROM sport WHERE TRIM(REPLACE(REPLACE(REPLACE(REPLACE(title, 'ß', 'ss'), 'ä', 'ae'), 'ö', 'oe'), 'ü', 'ue')) LIKE '$keyword' AND deleted_at IS NULL";
                    $result = DB::select(DB::raw($sql));
                    foreach ($result as $value)
                    {
                        $resultSportIDs[]=$value->id;
                        if (!in_array($keyword,$thisStringsAreSports))$thisStringsAreSports[]=$keyword;
                    }
                }
            }

            $sqlNamesString1 = "(";
            $sqlNamesString2 = "(";
            $sqlNamesString3 = "(";
            foreach($keywords as $keyword)
            {
                $keyword = trim($keyword);
                foreach ($specialNames as $specialName)
                {
                    $changeFrom = str_replace(" ", "", $specialName);
                    if ($keyword == $changeFrom) $keyword = $specialName;
                }

                if (strlen($keyword) > 1)
                {
                    if (!in_array($keyword,$thisStringsAreSports))
                    {
                        if ($sqlNamesString1 != "(") $sqlNamesString1 .= " OR ";
                        $sqlNamesString1 .= "REPLACE(REPLACE(REPLACE(REPLACE(club.name, 'ß', 'ss'), 'ä', 'ae'), 'ö', 'oe'), 'ü', 'ue') LIKE '%$keyword%' ";
                    }else{
                        if ($sqlNamesString3 != "(") $sqlNamesString3 .= " OR ";
                        $sqlNamesString3 .= "REPLACE(REPLACE(REPLACE(REPLACE(club.name, 'ß', 'ss'), 'ä', 'ae'), 'ö', 'oe'), 'ü', 'ue') LIKE '%$keyword%' ";
                    }
                    if ($sqlNamesString2 != "(") $sqlNamesString2.=" AND ";
                    $sqlNamesString2.="REPLACE(REPLACE(REPLACE(REPLACE(club.name, 'ß', 'ss'), 'ä', 'ae'), 'ö', 'oe'), 'ü', 'ue') LIKE '%$keyword%' ";
                }
            }
            $sqlNamesString1.=")";
            $sqlNamesString2.=")";
            $sqlNamesString3.=")";

            $sqlAdressString = "(";
            foreach($keywords as $keyword)
            {
                $keyword = trim($keyword);
                foreach ($specialNames as $specialName)
                {
                    $changeFrom = str_replace(" ", "", $specialName);
                    if ($keyword == $changeFrom) $keyword = $specialName;
                }

                if (strlen($keyword) > 2 and !in_array($keyword,$thisStringsAreSports))
                {
                    if ($sqlAdressString != "(") $sqlAdressString.=" OR ";
                    $sqlAdressString.="REPLACE(REPLACE(REPLACE(REPLACE(club.city, 'ß', 'ss'), 'ä', 'ae'), 'ö', 'oe'), 'ü', 'ue') LIKE '%$keyword%' OR REPLACE(REPLACE(REPLACE(REPLACE(club.federal_state, 'ß', 'ss'), 'ä', 'ae'), 'ö', 'oe'), 'ü', 'ue') LIKE '%$keyword%' ";
                }
            }
            $sqlAdressString.=")";

            $sqlSportsString = "(";
            foreach($resultSportIDs as $sportID)
            {
                    if ($sqlSportsString != "(") $sqlSportsString.=" OR ";
                    $sqlSportsString.="club_sport.sport_id = '$sportID' ";
            }
            $sqlSportsString.=")";

            $sql_kombi1 = "";
            $sql_kombi2 = "";
            $sql_kombi3 = "";
            $sql_kombi4 = "";
            $sql_kombi5 = "";

            $sql_kombi1 = $sqlNamesString2;
            if (count($keywords)>1)
            {
                if ($sqlNamesString1 != "()" AND $sqlSportsString != "()") $sql_kombi2 = "OR ($sqlNamesString1 AND $sqlSportsString)";
                if ($sqlNamesString3 != "()" AND $sqlAdressString != "()") $sql_kombi3 = "OR ($sqlNamesString3 AND $sqlAdressString)";
                if ($sqlAdressString != "()" AND $sqlSportsString != "()") $sql_kombi4 = "OR ($sqlAdressString AND $sqlSportsString)";
                if ($sqlNamesString1 != "()" AND $sqlAdressString != "()") $sql_kombi5 = "OR ($sqlNamesString1 AND $sqlAdressString)";
            }else{
                if ($sqlSportsString != "()") $sql_kombi2 = "OR  $sqlSportsString ";
                if ($sqlAdressString != "()") $sql_kombi3 = "OR  $sqlAdressString ";
            }
            $sql = "SELECT club.id AS id, club.name AS name, club.city AS city, club.federal_state AS federal_state, club_sport.sport_id AS sportID FROM club LEFT JOIN club_sport ON club.id = club_sport.club_id WHERE $sql_kombi1 $sql_kombi2 $sql_kombi3 $sql_kombi4 $sql_kombi5";
            $result = DB::select(DB::raw($sql));
            foreach ($result as $value)
            {
                $allFound = true;
                $city =  $value->city;
                $name = $value->name;
                $federal_state = $value->federal_state;
                foreach($umlaute as $umlaut=>$changeTo)
                {
                    $city = str_replace($umlaut, $changeTo, $city);
                }
                foreach($umlaute as $umlaut=>$changeTo)
                {
                    $name = str_replace($umlaut, $changeTo, $name);
                }
                foreach($umlaute as $umlaut=>$changeTo)
                {
                    $federal_state = str_replace($umlaut, $changeTo, $federal_state);
                }
                foreach($keywords as $keyword)
                {
                    $foundThisOne = false;
                    if (in_array($keyword,$thisStringsAreSports))
                    {
                        if (in_array($value->sportID, $resultSportIDs)) $foundThisOne = true;
                    }
                    if (strpos(strtolower($city), strtolower($keyword)) !== false)$foundThisOne = true;
                    if (strpos(strtolower($name), strtolower($keyword)) !== false)$foundThisOne = true;
                    if (strpos(strtolower($federal_state), strtolower($keyword)) !== false)$foundThisOne = true;
                    if ($foundThisOne === false) $allFound = false;
                }
                if ($allFound === true) $resultClubIDs[]=$value->id;
            }

            $keyword = $request->keyword;

            $clubsQuery = Club::where('published', '=', 1);
            if (is_array($resultClubIDs) and count($resultClubIDs)>0)
            {
                $clubsQuery = $clubsQuery->where(function ($query) use ($resultClubIDs) {
                    foreach ($resultClubIDs as $resultClubID)
                    {
                        $query->orWhere('id', '=', $resultClubID);
                    }
                });
            }else{
                $clubsQuery = $clubsQuery->where('id', '<', '0');
            }

            $clubTotal = $clubsQuery->get()->count();
            $clubs = $clubsQuery->get();

            $clubs = $clubs->sortByDesc(function ($club, $key) {
                return [$club->average_rating, $club->hasAvatar, $club->percent_club_profile_progress];
            });

            $clubs = $this->paginate($clubs->values())->appends(request()->query());

            return view('search.index', [
                'clubs'       => $clubs,
                'clubTotal'  => $clubTotal,
                'keyword'    => $keyword
            ]);


        }else{
            $keyword = $request->keyword;
            return view('search.index');
	    }


    }



    public function clubSearchNew_sureByDirk (Request $request) {
    	if (!$request->keyword) {
		    return view('search.index');
	    }
	    $keyword = strtolower($request->keyword);

	    if (strpos(strtolower($keyword), 'kung fu') !== false) {
		    $keywordsForName = str_replace('kung fu', 'kungfu', $keyword);
		    $keywordsForName = explode(' ' ,$keywordsForName);
	    } else $keywordsForName = explode(' ' ,$keyword);

	    $clubsQuery = Club::where('published', '=', 1);

	    if (is_array($keywordsForName)) {
	    	if(in_array('kungfu', $keywordsForName) || in_array('kampfsport', $keywordsForName)) {
			    foreach ($keywordsForName as $keyForName) {
			        $keyForName = trim($keyForName);
			        if (strlen($keyForName)>0)
                    {
                        $clubsQuery = $clubsQuery->where(function ($query) use ($keyForName) {
                            $query->orWhere(DB::raw("REPLACE(REPLACE(REPLACE(REPLACE(name, 'ß', 'ss'), 'ä', 'ae'), 'ö', 'oe'), 'ü', 'ue')"), 'LIKE',
                                DB::raw("concat('%', REPLACE(REPLACE(REPLACE(REPLACE('Kung Fu', 'ß', 'ss'), 'ä', 'ae'), 'ö', 'oe'), 'ü', 'ue'), '%')"))
                                ->orWhere(DB::raw("REPLACE(REPLACE(REPLACE(REPLACE(name, 'ß', 'ss'), 'ä', 'ae'), 'ö', 'oe'), 'ü', 'ue')"), 'LIKE',
                                    DB::raw("concat('%', REPLACE(REPLACE(REPLACE(REPLACE('Kampfsport', 'ß', 'ss'), 'ä', 'ae'), 'ö', 'oe'), 'ü', 'ue'), '%')"))
                                ->orWhere(DB::raw("REPLACE(REPLACE(REPLACE(REPLACE(name, 'ß', 'ss'), 'ä', 'ae'), 'ö', 'oe'), 'ü', 'ue')"), 'LIKE',
                                    DB::raw("concat('%', REPLACE(REPLACE(REPLACE(REPLACE('" . $keyForName . "', 'ß', 'ss'), 'ä', 'ae'), 'ö', 'oe'), 'ü', 'ue'), '%')"));
                        });
                    }
			    }
		    } else {
			    foreach ($keywordsForName as $keyForName) {
				    $clubsQuery = $clubsQuery->where(function($query) use ($keyForName){
					    $query->orWhere(DB::raw("REPLACE(REPLACE(REPLACE(REPLACE(name, 'ß', 'ss'), 'ä', 'ae'), 'ö', 'oe'), 'ü', 'ue')"), 'LIKE',
						    DB::raw("concat('%', REPLACE(REPLACE(REPLACE(REPLACE('".$keyForName."', 'ß', 'ss'), 'ä', 'ae'), 'ö', 'oe'), 'ü', 'ue'), '%')"));
				    });
			    }
		    }
	    }

	    $keywordArray = explode(' ', $keyword);
	    if (count($keywordArray) > 1) {

		    $clubsQuery = $clubsQuery->orWhere(function($query) use ($keywordArray){
			    foreach($keywordArray as $kw) {
			         $kw = trim($kw);
			          if (strlen($kw)>0)
                      {
                          $query->orWhere('city', $kw);
                      }
			    }
		    });

		    //$sportQuery = Sport::where('deleted_at', '=', null);
		    $sportQuery = Sport::where('id', '>', '0');
		      $sportQuery = $sportQuery->where(function($query) use ($keywordArray){
			      foreach($keywordArray as $kw) {
			          $kw = trim($kw);
			          if (strlen($kw)>0)
                      {
                          $query->orWhere(DB::raw("TRIM(REPLACE(REPLACE(REPLACE(REPLACE(title, 'ß', 'ss'), 'ä', 'ae'), 'ö', 'oe'), 'ü', 'ue'))"), 'LIKE',
                              DB::raw("REPLACE(REPLACE(REPLACE(REPLACE('" . $kw . "', 'ß', 'ss'), 'ä', 'ae'), 'ö', 'oe'), 'ü', 'ue')"));
                      }
			      }
		      });

		      $sport = $sportQuery->get();
	    } else {

		    $clubsQuery = $clubsQuery->orWhere('city', $keyword);
		    if (strpos(strtolower($keyword), 'kung fu') !== false || strpos(strtolower($keyword), 'kampfsport') !== false) {
			    $sport = Sport::where(DB::raw("TRIM(REPLACE(REPLACE(REPLACE( REPLACE(title, 'ß', 'ss'), 'ä', 'ae'), 'ö', 'oe'), 'ü', 'ue'))"), 'LIKE',
				    DB::raw("concat(REPLACE(REPLACE(REPLACE(REPLACE('Kung Fu', 'ß', 'ss'), 'ä', 'ae'), 'ö', 'oe'), 'ü', 'ue'))"))
				    ->orWhere(DB::raw("TRIM(REPLACE(REPLACE(REPLACE( REPLACE(title, 'ß', 'ss'), 'ä', 'ae'), 'ö', 'oe'), 'ü', 'ue'))"), 'LIKE',
					    DB::raw("concat(REPLACE(REPLACE(REPLACE(REPLACE('Kampfsport', 'ß', 'ss'), 'ä', 'ae'), 'ö', 'oe'), 'ü', 'ue'))"))
				    ->orWhere(DB::raw("TRIM(REPLACE(REPLACE(REPLACE( REPLACE(title, 'ß', 'ss'), 'ä', 'ae'), 'ö', 'oe'), 'ü', 'ue'))"), 'LIKE',
					    DB::raw("concat(REPLACE(REPLACE(REPLACE(REPLACE('".$keyword."', 'ß', 'ss'), 'ä', 'ae'), 'ö', 'oe'), 'ü', 'ue'))"))->first();
		    } else {
			    $sport = Sport::orWhere(DB::raw("TRIM(REPLACE(REPLACE(REPLACE( REPLACE(title, 'ß', 'ss'), 'ä', 'ae'), 'ö', 'oe'), 'ü', 'ue'))"), 'LIKE',
				    DB::raw("concat(REPLACE(REPLACE(REPLACE(REPLACE('".$keyword."', 'ß', 'ss'), 'ä', 'ae'), 'ö', 'oe'), 'ü', 'ue'))"))->first();
		    }
	    }

	    $clubsQuery = $clubsQuery->leftJoin('reviews','club.id','=','reviews.club_id')
		    ->where('club.published', '=', 1)
		    ->select('club.*',DB::raw('CEIL(avg(rating)) as average_rating'))
		    ->with(array('teams', 'socialLinks', 'achievements', 'posts'))
		    ->groupBy('club.id');

	    if ($sport !== null && is_array($sport) && count($sport) > 1){
		    foreach ($sport as $s) {
			    $clubsQuery = $clubsQuery->orWhereHas('sports', function($query) use ($s){
					    $query->where('sport.id', '=', $s->id);
			    });
		    }
	    }
	    else {
	        if ($sport !== null && $sport->first() !== null )
            {
                $sport = $sport->first();

                if (isset($sport->id))
                {
                    $clubsQuery = $clubsQuery->orWhereHas('sports', function ($query) use ($sport) {
                        $query->where('sport.id', '=', $sport->id);
                    });
                }
            }
	    }

	    $clubTotal = $clubsQuery->get()->count();

	    $clubs = $clubsQuery->get();

	    $clubs = $clubs->sortByDesc(function ($club, $key) {
		    return [$club->average_rating, $club->hasAvatar, $club->percent_club_profile_progress];
	    });

	    $clubs = $this->paginate($clubs->values())->appends(request()->query());

	    return view('search.index', [
		    'clubs'       => $clubs,
		    'clubTotal'  => $clubTotal,
		    'keyword'    => $keyword
	    ]);
    }




    public function paginate($items, $perPage = 20, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ?
            $items : Collection::make($items);

        $lap = new LengthAwarePaginator($items->forPage($page, $perPage),
            $items->count(),
            $perPage, $page, $options);

        $lap->setPath(url('/suche/vereine'));

        return $lap;
    }

	/**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function bandageSearch(Request $request)
    {
        $partials = [];
        $cityquery = trim($request->input('city', ''));
        $distance = trim($request->input('distance', '25'));
        $keyword = trim($request->input('keyword', ''));
        if (!$keyword){
            $keyword = trim($request->input('keyword1', ''));
        }
        $type = trim($request->input('type', ''));
        $ajax = trim($request->input('ajax', ''));
        $bandageCount = 0;
        $bandages = null;


        if ($cityquery !== '' || $keyword !== ''){

            $bandagesQuery = Bandage::published();
            $city = null;

            if ($cityquery != ''){
                if (is_numeric(trim($cityquery))){
                    $zipcode = CityZipcode::where('zip', '=', $cityquery)->first();
                    $city = (!is_null($zipcode)) ? $zipcode->city : null;
                } else{
                    $city = City::where('name', '=', $cityquery)->first();
                }

                if ($city != null){
                    $bandagesQuery = $bandagesQuery->closest($city->lat, $city->lon, $distance);
                }
            }

            if ($cityquery == ''){
                $bandagesQuery = $bandagesQuery->where(function($query) use ($keyword){
                    $query->orWhere('name', 'LIKE', '%' . $keyword . '%')->orWhere('city', $keyword)->orWhere('zip', $keyword);
                }
                );
            } else{
                $bandagesQuery = $bandagesQuery->where('name', 'LIKE', '%' . $keyword . '%');
            }

            $bandages = $this->paginateSearchResults($request, $bandagesQuery, 6)->withPath(route('bandage.search'));
            $bandageCount = $bandages->total();
        }

        if ($ajax){
            foreach ($bandages as $bandage){
                array_push($partials, view('search.partials.result.item.bandage', ['bandage' => $bandage])->render());
            }

            if ($bandages->nextPageUrl()){
                array_push($partials, view('search.partials.result.more-result', ['more' => $bandages, 'name' => 'BANDAGE', 'type' => 'bandage'])->render());
            }

            $bandages->put('rendered', $partials);
        }

        return $bandages;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function eventsSearch(Request $request){
        $keywordarray = explode(' ', $request->input('keyword'));
        $partials = [];
        $ajax = trim($request->input('ajax', ''));

        $sportquery = trim($request->input('sport', ''));
        $cityquery = trim($request->input('city', ''));
        $distance = trim($request->input('distance', '25'));
        $eventsQuery = new Event;
        $eventsQuery = $eventsQuery->with('images')->SearchByKeyword($keywordarray);
        if ($cityquery != ''){
            if (is_numeric(trim($cityquery))){
                $zipcode = CityZipcode::where('zip', '=', $cityquery)->first();
                $city = (!is_null($zipcode)) ? $zipcode->city : null;
            } else{
                $city = City::where('name', 'LIKE', '%' . $cityquery . '%')->first();
            }

            if ($city != null){
                $eventsQuery = $eventsQuery->closest($city->lat, $city->lon, $distance);
            } else{
                $eventsQuery = $eventsQuery->whereRaw('1 = 2'); # TODO: ??
            }
        }
        $events = $this->paginateSearchResults($request, $eventsQuery, 6);
        $events->setPath('/suche/events');
        $eventsCount = $events->total();
        if ($ajax){
            foreach ($events as $event){
                array_push($partials, view('search.partials.result.item.event', ['event' => $event])->render());
            }

            if ($events->nextPageUrl()){
                array_push($partials, view('search.partials.result.more-result', ['more' => $events, 'name' => 'EVENTS', 'type' => 'events'])->render());
            }

            $events->put('rendered', $partials);
        }
//        print("<pre>");
//        print_r($events->first()->images()->first());
//        print("</pre>");
//        exit;
        return $events;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function newsSearch(Request $request){
        $keywordarray = explode(' ', $request->input('keyword'));
        $partials = [];
        $ajax = trim($request->input('ajax', ''));

        //$newsQuery = News::with(['images','category']);
        $newsQuery = new News;
        $newsQuery = $newsQuery->SearchByKeyword($keywordarray);
        $news = $this->paginateSearchResults($request, $newsQuery, 6);
        $news->setPath('/suche/news');
        $newsCount = $news->total();
        if ($ajax){
            foreach ($news as $single_news){
                array_push($partials, view('search.partials.result.item.news', ['single_news' => $single_news])->render());
            }

            if ($news->nextPageUrl()){
                array_push($partials, view('search.partials.result.more-result', ['more' => $news, 'name' => 'NEUIGKEITEN', 'type' => 'news'])->render());
            }

            $news->put('rendered', $partials);
        }
//        print("<pre>");
//        dd($news->first()->picture);
//        dd($news->first()->images);
//        print("</pre>");
//        exit;
        return $news;
    }
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function userSearch(Request $request)
    {

//        if(Auth::user()){
//            $username = Auth::user()->username;
//        }
//        else{
//            $username = 'PUBLIC';
//        }
//
//        $this->logSearch($request, $username);


        $keyword = trim($request->input('keyword', ''));
        $keywordarray = explode(' ', $request->input('keyword'));
        if (!$keyword){
            $keyword = trim($request->input('keyword1', ''));
        }
        $sportquery = trim($request->input('sport', ''));
        $cityquery = trim($request->input('city', ''));
        $distance = trim($request->input('distance', '25'));
        $type = trim($request->input('type', ''));
        $ajax = trim($request->input('ajax', ''));
        $userCount = 0;
        $users = null;
        $partials = [];

        //$map = Mapper::map('51.0778879', '5.9595168', ['zoom' => 6, 'marker' => false]); # default germany map

        if ($cityquery !== '' || $keyword !== '' || $sportquery !== ''){

            $userQuery = User::verified();
            $city = null;

            if ($cityquery == '' && $sportquery == ''){

//                $userQuery = $userQuery->search($keyword);


                if (count($keywordarray) < 2){

                    $userQuery = $userQuery->search($keyword);
                }
                if (count($keywordarray) > 1){

                    $userQuery = $userQuery->where('firstname', 'LIKE', '%' . $keywordarray[0] . '%')
                                           ->orWhere('lastname', 'LIKE', '%' . $keywordarray[0] . '%')
                                           ->orWhere('firstname', 'LIKE', '%' . $keywordarray[1] . '%')
                                           ->orWhere('lastname', 'LIKE', '%' . $keywordarray[1] . '%');
                }


                $sport = Sport::where('title', '=', $keyword)->first();

                if ($sport !== null){
                    $userQuery = $userQuery->orWhereHas('sports', function($query) use ($sport){
                        $query->where('sport.id', '=', $sport->id);
                    });
                }

            } else{

                if ($cityquery != ''){
                    if (is_numeric(trim($cityquery))){
                        $zipcode = CityZipcode::where('zip', '=', $cityquery)->first();
                        $city = (!is_null($zipcode)) ? $zipcode->city : null;
                    } else{
                        $city = City::where('name', 'LIKE', '%' . $cityquery . '%')->first();
                    }

                    if ($city != null){
                        $userQuery = $userQuery->closest($city->lat, $city->lon, $distance);
                    } else{
                        $userQuery = $userQuery->whereRaw('1 = 2'); # TODO: ??
                    }
                }

                if ($keyword != '' && $keyword != $sportquery){

                    if (count($keywordarray) < 2){

                        $userQuery = $userQuery->search($keyword);
                    }
                    if (count($keywordarray) > 1){

                        $userQuery = $userQuery->where('firstname', 'LIKE', '%' . $keywordarray[0] . '%')
                                               ->orWhere('lastname', 'LIKE', '%' . $keywordarray[0] . '%')
                                               ->orWhere('firstname', 'LIKE', '%' . $keywordarray[1] . '%')
                                               ->orWhere('lastname', 'LIKE', '%' . $keywordarray[1] . '%');
                    }

                }

                if ($sportquery != ''){
                    $sport = Sport::where('title', '=', $sportquery)->first();
//                    $sport = Sport::where('title', 'LIKE', '%' . $sportquery . '%')->get();
//                    $sportarray = $sport->pluck('title', 'id');
//                    $sportarray->toArray();

                } else{
                    $sport = Sport::where('title', '=', $keyword)->first();
//                    $sport = Sport::where('title', 'LIKE', '%' . $keyword . '%')->get();
//                    $sportarray = $sport->pluck('id', 'id');
//                    $sportarray->toArray();
                }

                if ($sport != null){
                    if (trim($request->input('sport', '')) == ''){
                        $userQuery = $userQuery->orWhereHas('sports', function($query) use ($sport){
//                        $userQuery = $userQuery->orWhereHas('sports', function($query) use ($sportarray){
                            $query->where('sport.id', '=', $sport->id);
//                            $query->whereIn('sport.id', $sportarray);
                        });
                    } else{
                        $userQuery = $userQuery->whereHas('sports', function($query) use ($sport){
//                        $userQuery = $userQuery->whereHas('sports', function($query) use ($sportarray){
                            $query->where('sport.id', '=', $sport->id);
//                            $query->whereIn('sport.id', $sportarray);
                        });
                    }
                }
            }

            $users = $this->paginateSearchResults($request, $userQuery, 6)->withPath(route('user.search'));
            $userCount = $users->total();
        }

        if ($ajax){
            foreach ($users as $user){
                array_push($partials, view('search.partials.result.item.user', ['user' => $user])->render());
            }

            if ($users->nextPageUrl()){
                array_push($partials, view('search.partials.result.more-result', ['more' => $users, 'name' => 'PERSONEN', 'type' => 'user'])->render());
            }

            $users->put('rendered', $partials);
        }

        return $users;
    }


    protected function logSearch(Request $request, $username, $resultcount)
    {


        $keyword = $request->input('keyword', '');
        $sportquery = $request->input('sport', '');
        $cityquery = $request->input('city', '');
        $distance = $request->input('distance', '25');
        $type = $request->input('type', '');


        $filename = 'searchlog.csv';
        $filename = storage_path('app' . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . $filename);

        $writer = Writer::createFromPath($filename, "a");
        $writer->setDelimiter(";");
        $writer->setNewline("\r"); //use windows line endings for compatibility with some csv libraries
        $writer->setOutputBOM(Writer::BOM_UTF8); //adding the BOM sequence on output

        // set csv first (header) row
        //$writer->insertOne("\xEF\xBB\xBF");
        $writer->insertOne([
            $username,
            $keyword,
            $sportquery,
            $cityquery,
            $distance,
            $type,
            Carbon::now(),
            $resultcount,
        ]);

    }
}