<?php

namespace Vereinsleben\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

use Vereinsleben\Http\Controllers\Controller;
use Vereinsleben\State;
use Vereinsleben\Slider;
use Vereinsleben\Image;
use \DB;
use Auth;


class SliderController extends BaseController
{

    public function index()
    {
        return view('admin.slider.sliderList', ['sliders' => Slider::orderBy('created_at', 'desc')->get()]);
    }

    public function addSlider()
    {
        return view('admin.slider.edit', ['slider' => new Slider()]);
    }

    public function store(Request $request)
    {
        $slider = new Slider;

        $slider->title = $request->slider_title;
        $slider->description = $request->slider_description;
        $slider->h2 = $request->slider_h2;
        $slider->h3 = $request->slider_h3;
        $slider->btn = $request->slider_btn;
        $slider->link = $request->slider_btn_link;
        $slider->slider_image = $request['slider_image'];
        if (is_null($slider->published) || $slider->published == null || $slider->published == ''){

            $slider->published = 0;
        } else{
            $slider->published = $request['published'];
        }
        $slider->position = $request['position'];
		
        $slider->save();
        $this->saveFederalstate($request['federal_states'], $slider);


        return \Redirect::route('admin.slider.index')->with('flash-success', 'Der Slider wurde angelegt!');
    }

    public function edit($id)
    {
        return view('admin.slider.edit', [
            'slider' => Slider::find($id),
        ]);
    }

    public function update(Request $request, $id)
    {
        $slider = Slider::findOrFail($id);

        $slider->title = !empty($request['slider_title']) ? $request['slider_title'] : null;
        $slider->description = !empty($request['slider_description']) ? $request['slider_description'] : null;
        $slider->h2 = !empty($request['slider_h2']) ? $request['slider_h2'] : null;
        $slider->h3 = !empty($request['slider_h3']) ? $request['slider_h3'] : null;
        $slider->btn = !empty($request['slider_btn']) ? $request['slider_btn'] : null;
        $slider->link = !empty($request['slider_btn_link']) ? $request['slider_btn_link'] : null;
        $slider->logo1 = $request['slider_logo1'];
        $slider->logo2 = $request['slider_logo2'];
        $slider->logo3 = $request['slider_logo3'];
        $slider->logo4 = $request['slider_logo4'];
        $slider->logo5 = $request['slider_logo5'];
        $slider->logo6 = $request['slider_logo6'];
        $slider->slider_image = $request['slider_image'];
        $slider->published = $request['published'];
        $slider->position = $request['position'];
		
        $slider->save();

        $this->saveFederalstate($request['federal_states'], $slider);

        return \Redirect::route('admin.slider.index')->with('flash-success', 'Deine Änderungen wurden gespeichert!');
    }

    public function destroy(Request $request, $id)
    {
        $slider = Slider::find($id);

        $slider->delete();

        return \Redirect::route('admin.slider.index')->with('flash-success', 'Datensatz erfolgreich gelöscht');
    }

    public function saveFederalstate($federalstates, $slider)
    {
        $slider->states()->detach();
        if ($federalstates != null){
            foreach ($federalstates as $stateId){
                if ($slider->states()->where('stateable_id', $stateId)->first() === null){
                    $state = State::find($stateId);
                    $slider->states()->save($state);
                }
            }
        }
    }

	
}