<?php

namespace Vereinsleben\Http\Controllers;

use Vereinsleben\Spitzensport;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Vereinsleben\Role;

use Vereinsleben\News;

use Vereinsleben\Helpers\RandomCampaign;
use Vereinsleben\Http\Requests\SpitzensportFormRequest;
use Vereinsleben\NewsCategory;  // can be remove later
use Vereinsleben\User;  // can be remove later
use Illuminate\Support\Facades\URL;
use Auth;
use Validator;
use Vereinsleben\Meta;
use Vereinsleben\State;
use Vereinsleben\Tag;
use Redirect;

class SpitzensportController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $spitzensports = Spitzensport::orderBy('spitzensports.created_at', 'desc')->with('user')->get();
    	
        return view('admin.spitzensport.all', ['spitzensports' => $spitzensports]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = new Role();
        return view('admin.spitzensport.new', [
            'spitzensport' => new Spitzensport(['state' => Spitzensport::STATE_DRAFT]),
            'newsCategories' => NewsCategory::whereNotNull('parent_id')->orderBy('title', 'asc')->pluck('title', 'id')->all(),
            'newsCategories' => NewsCategory::orderBy('title', 'asc')->pluck('title', 'id')->all(),
		        'usersContentManager' => User::where('role_id', $role->getByConstant(Role::CONTENT_MANAGER)->id)
			        ->orWhere('role_id', $role->getByConstant(Role::ADMIN)->id)->orderBy('role_id', 'asc')->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SpitzensportFormRequest $request)
    {
        if (!isset($request->federal_states)) {
            return \Redirect::to(URL::previous() . '#')->with('flash-error', 'Bitte mindestens ein Bundesland auswählen!');
        }
    	
        $spitzensport = new Spitzensport;

        $spitzensport->slug = $request['slug'];

        $validator = Validator::make(['slug' => $spitzensport->slug], [
            'slug' => 'required|min:3|unique:spitzensports',
        ]);

        if ($validator->fails()){
            $spitzensport->slug = str_slug($spitzensport->slug . '-' . rand(100, 999), '-');
        }

        $spitzensport->published_from = !empty($request['published_from']) ? date('Y-m-d H:i:s', strtotime($request['published_from'])) : null;
        $spitzensport->published_until = !empty($request['published_until']) ? date('Y-m-d H:i:s', strtotime($request['published_until'])) : null;
        $spitzensport->element_order = !empty($request['element_order']) ? $request['element_order'] : null;

        $spitzensport->category_id = $request['category_id'];
        $spitzensport->title = $request['title'];
        $spitzensport->sub_title = $request['sub_title'];
        $spitzensport->content_teaser = $request['content_teaser'];
        $spitzensport->content = $request['content'];
        $spitzensport->steckbrief = $request['steckbrief'];
        $spitzensport->state = $request['state'];
        $spitzensport->full_name = $request['full_name'];
        $spitzensport->profession = $request['profession'];

        if ($request['exclusive'] == 1 || $request['exclusive'] == '1'){
            $spitzensport->exclusive = 1;
        } else{
            $spitzensport->exclusive = 0;
        }

        $spitzensport->main_image = $request['main_image'];
        $spitzensport->main_image_source = $request['main_image_source'];

        $spitzensport->owner_id = Auth::user()->id;
	    $spitzensport->user_id = $request->contentManagerId;

        if ($spitzensport->state === Spitzensport::STATE_PUBLISHED){
            $spitzensport->published_at = date("Y-m-d H:i:s");
        }

        $spitzensport->save();
        
        $this->saveMetaData($request, $spitzensport);
        $this->saveFederalstate($request['federal_states'], $spitzensport);
        $this->saveTags($request['tag'], $spitzensport);

        $images = $request['spitzensportImages'];
        $imgTitle = $request['imgTitle'];
        $imgDesc = $request['imgDesc'];

        if ($images !== null && count($images) > 0){
            $imgOrder = 0;
            foreach ($images as $key => $image){
                $title = $imgTitle[$key];
                $desc = $imgDesc[$key];
                $image = new Image(['picture' => $image, 'title' => $title, 'description' => $desc, 'order' => $imgOrder]);
                $spitzensport->images()->save($image);
                $imgOrder++;
            }
        }

        if ($spitzensport->images()->count()){
            $spitzensport->has_gallery = 1;
            $spitzensport->save();
        }

        return Redirect::route('spitzensports.index')->with('flash-success', 'Spitzensport Nachricht war geboren!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \Vereinsleben\Spitzensport  $spitzensport
     * @return \Illuminate\Http\Response
     */
    public function show($categoryId, $spitzensportSlug, Request $request) // Spitzensport $spitzensport old param
    {
        $campaign = RandomCampaign::instance()->getRandomForNews();
		
        $nextImgUrl = '';
        $prevImgUrl = '';
        $imageRes = '';
        $spitzensport = Spitzensport::with('category')->where('slug', $spitzensportSlug)->firstOrFail();
        
        $category = NewsCategory::where('id', $categoryId)->firstOrFail();
        // $related = Spitzensport::forCategory($category)->public()->orderBy('published_at', 'desc')->take(3)->get()->keyBy('id')->except($spitzensport->id);
        $related = [];
        // $latest = $spitzensport->latestUpdatedNews(3);
        $latest = [];
        // $latestNews = $spitzensport->latestNews(9);
        $latestNews = [];
        
        $spitzensportImgCnt = $spitzensport->images()->count();

        $seq = trim($request->input('seq', ''));
        if (!$seq) {
            $seq = 1;
        }
        
        if ($spitzensportImgCnt) {
            $spitzensportImages = $spitzensport->images();
            $nextSeq = $seq + 1;
            foreach ($spitzensport->images as $key => $image) {
                if (($key + 1) == $seq) {
                    $imageRes = $image;
                }
                if (($key + 1) == $nextSeq) {
                    $nextImgUrl = route('news.detail', [$spitzensport->category_id, $spitzensport->slug]) . '?seq=' . $nextSeq;
                }
                if ($key == ($seq - 1) && $seq > 1) {
                    if ($seq == 2)
                        $prevImgUrl = route('news.detail', [$spitzensport->category_id, $spitzensport->slug]);
                    else
                        $prevImgUrl = route('news.detail', [$spitzensport->category_id, $spitzensport->slug]) . '?seq=' . ($seq - 1);
                }
            }
        }

        
        // if (policy($spitzensport)->show(Auth::user(), $spitzensport)) {
            return view('spitzensport.detail', [
                'spitzensport' => $spitzensport,
                'related' => $related,
                'latest' => $latest,
                'popularNews' => $latestNews,
                'seq' => $seq,
                'imageRes' => $imageRes,
                'nextImgUrl' => $nextImgUrl,
                'prevImgUrl' => $prevImgUrl,
                'campaign' => $campaign,
            ]);
        // } else {
            // abort(403);
        // }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Vereinsleben\Spitzensport  $spitzensport
     * @return \Illuminate\Http\Response
     */
    public function edit(Spitzensport $spitzensport)
    {
        $role = new Role();
        if (!isset($spitzensport->meta)){
            $spitzensport->meta()->create([]);
        }

        return view('admin.spitzensport.new', [
            'spitzensport'           =>$spitzensport,
            'spitzensportImages'     => Spitzensport::find($spitzensport->id)->images()->orderBy('order', 'ASC')->get(),
            // 'newsCategories' => NewsCategory::whereNotNull('parent_id')->orderBy('title', 'asc')->pluck('title', 'id')->all(),
            'newsCategories' => NewsCategory::orderBy('title', 'asc')->pluck('title', 'id')->all(),
            'usersContentManager' => User::where('role_id', $role->getByConstant(Role::CONTENT_MANAGER)->id)
	                                        ->orWhere('role_id', $role->getByConstant(Role::ADMIN)->id)->orderBy('role_id', 'asc')->get(),
            'userContentManager' => $spitzensport->userContentManager()->first(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Vereinsleben\Spitzensport  $spitzensport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'published_from' => 'date_format:d.m.Y H:i',
            'published_until' => 'date_format:d.m.Y H:i',
            'element_order' => 'digits_between:1,12',
            'category_id' => 'required|numeric', 
            'title' => 'required|string',
            'full_name' => 'required',
            'profession' => 'required',
            'sub_title' => 'string',
        ]);

        if (!isset($request->federal_states)) {
            return \Redirect::to(URL::previous() . '#')->with('flash-error', 'Bitte mindestens ein Bundesland auswählen!');
        }
    	
        $spitzensport = Spitzensport::findOrFail($id);

        $slug = $request['slug'];

        $validator = Validator::make(['slug' => $slug], [
            'slug' => 'required|min:3|unique:news',
        ]);

        if ($validator->fails()){
            if ($slug != $spitzensport->slug){
                $spitzensport->slug = str_slug($spitzensport->slug . '-' . rand(100, 999), '-');
            }
        }
        $spitzensport->slug = $slug;
        $spitzensport->published_from = !empty($request['published_from']) ? date('Y-m-d H:i:s', strtotime($request['published_from'])) : null;
        $spitzensport->published_until = !empty($request['published_until']) ? date('Y-m-d H:i:s', strtotime($request['published_until'])) : null;
        $spitzensport->element_order = !empty($request['element_order']) ? $request['element_order'] : null;

        $spitzensport->category_id = $request['category_id'];
        $spitzensport->title = $request['title'];
        $spitzensport->sub_title = $request['sub_title'];
        $spitzensport->content_teaser = $request['content_teaser'];
        $spitzensport->content = $request['content'];
        $spitzensport->steckbrief = $request['steckbrief'];
        $spitzensport->is_popular = $request->is_popular;
        $spitzensport->is_featured = $request->is_featured;
        $spitzensport->state = $request['state'];
        $spitzensport->full_name = $request['full_name'];
        $spitzensport->profession = $request['profession'];

        if ($request['exclusive'] == 1 || $request['exclusive'] == '1'){
            $spitzensport->exclusive = 1;
        } else{
            $spitzensport->exclusive = 0;
        }

        $spitzensport->main_image = $request['main_image'];
        $spitzensport->main_image_source = $request['main_image_source'];

        #$spitzensport->owner_id = Auth::user()->id;
        $spitzensport->user_id = $request->contentManagerId;

        $spitzensport->is_popular = $request['is_popular'];
        $spitzensport->is_featured = $request['is_featured'];

        // only update the published_at date if the state changes and no date have been set previously
        if ($spitzensport->state === Spitzensport::STATE_PUBLISHED and $spitzensport->getOriginal('published_at') == null){
            $spitzensport->published_at = date("Y-m-d H:i:s");
        }

        $spitzensport->save();
        
        $this->saveMetaDataNormalRequest($request, $spitzensport);
        $this->saveFederalstate($request['federal_states'], $spitzensport);
        $this->saveTags($request['tag'], $spitzensport);
        $images = $request['spitzensportImages'];
        $imgTitle = $request['imgTitle'];
        $imgDesc = $request['imgDesc'];
        $imgUpdateTitle = $request['imgUpdateTitle'];
        $imgUpdateDesc = $request['imgUpdateDesc'];
        $deleteImage = $request['delete-image'];

        if ($images !== null && count($images) > 0){
            $imgMaxOrder = $spitzensport->images()->count() == 0 ? 0 : $spitzensport->images()->max('order');
            foreach ($images as $key => $image){
                $imgMaxOrder++;
                $title = $imgTitle[$key];
                $desc = $imgDesc[$key];
                $image = new Image(['picture' => $image, 'title' => $title, 'description' => $desc, 'order' => $imgMaxOrder]);
                $spitzensport->images()->save($image);
            }
        }
        if ($imgUpdateTitle !== null && count($imgUpdateTitle) > 0){
            $imgOrder = 0;
            foreach ($imgUpdateTitle as $key => $imgUpdate){
                $title = $imgUpdateTitle[$key];
                $desc = $imgUpdateDesc[$key];
                Image::find($key)->update(['title' => $title, 'description' => $desc, 'order' => $imgOrder]);
                $imgOrder++;
            }
        }

        if ($deleteImage !== null && count($deleteImage) > 0){
            foreach ($deleteImage as $key => $val){
                Image::find($val)->delete();
            }
        }

        if ($spitzensport->images()->count()){
            $spitzensport->has_gallery = 1;
            $spitzensport->save();
        } else{
            $spitzensport->has_gallery = 0;
            $spitzensport->save();
        }

        return Redirect::route('spitzensports.index')->with('flash-success', 'Deine Änderungen wurden gespeichert!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Vereinsleben\Spitzensport  $spitzensport
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $spitzensport = Spitzensport::find($request->id);
        if ($request->ajax()) {
            $spitzensport->deleted_at = date("Y-m-d H:i:s");
            $spitzensport->save();

            return response()->json($spitzensport->deleted_at);
        } else{
            abort(403);
        }
    }

    /**
     * @param SpitzensportFormRequest $request
     * @param $spitzensport
     */
    public function saveMetaData(SpitzensportFormRequest $request, $spitzensport)
    {
        $meta = new Meta();

        $meta->meta_bild_alt = $request->meta_bild_alt;
        $meta->meta_description = $request->meta_description;
        $meta->meta_keywords = $request->meta_keywords;
        $meta->meta_author = $request->meta_author;
        $meta->meta_language = $request->meta_language;

        if ($spitzensport->meta === null){
            $spitzensport->meta()->create($meta->toArray());
        } else{
            $spitzensport->meta()->update($meta->toArray());
        }
    }
    public function saveMetaDataNormalRequest(Request $request, $spitzensport)
    {
        $meta = new Meta();

        $meta->meta_bild_alt = $request->meta_bild_alt;
        $meta->meta_description = $request->meta_description;
        $meta->meta_keywords = $request->meta_keywords;
        $meta->meta_author = $request->meta_author;
        $meta->meta_language = $request->meta_language;

        if ($spitzensport->meta === null){
            $spitzensport->meta()->create($meta->toArray());
        } else{
            $spitzensport->meta()->update($meta->toArray());
        }
    }


    public function saveFederalstate($federalstates, $spitzensport)
    {
        $spitzensport->states()->detach();
        if ($federalstates != null){
            foreach ($federalstates as $stateId){
                if ($spitzensport->states()->where('stateable_id', $stateId)->first() === null){
                    $state = State::find($stateId);
                    $spitzensport->states()->save($state);
                }
            }
        }
    }

    public function saveTags($tagLine, $spitzensport)
    {
        $spitzensport->tags()->detach();
        if ($tagLine != ""){
            $tags = explode(" ", $tagLine);
            foreach ($tags as $tagWord){
                if ($tagWord != " " and $tagWord != ""){
                    $tag = Tag::where('name', $tagWord)->first();
                    if ($tag === null){
                        $tag = new Tag(['name' => $tagWord]);
                    }
                    $spitzensport->tags()->save($tag);
                }
            }
        }
    }

    public function toggleVisibility(Request $request)
    {
        $spitzensport = Spitzensport::find($request->id);
        if ($request->ajax()){
            $spitzensport->state = ($spitzensport->state !== Spitzensport::STATE_PUBLISHED) ? Spitzensport::STATE_PUBLISHED : Spitzensport::STATE_HIDDEN;
            $spitzensport->save();

            return response()->json($spitzensport->state);
        } else{
            abort(403);
        }
    }
}
