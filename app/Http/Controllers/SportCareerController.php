<?php 
namespace Vereinsleben\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Validator;
use Vereinsleben\Http\Requests;
use Vereinsleben\Member;
use Vereinsleben\Role;
use Vereinsleben\Sport;
use Vereinsleben\SportCareer;
use Vereinsleben\User;
use Vereinsleben\Image;
use Vereinsleben\Club;
use Vereinsleben\Http\Requests\SportCareerFormRequest;
use Vereinsleben\Http\Requests\SportCareerClubFormRequest;
use Vereinsleben\SocialLink;


class SportCareerController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Vereinsleben\Http\Requests\SportCareerFormRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(SportCareerFormRequest $request)
    {
        $response = [
            'success' => false,
            'data' => [],
            'error' => null,
        ];

        try {
            $role = new Role;
            $member = new Member();
            $user = Auth::user();

            $clubId = trim($request->input('club_id'));
	
		        $sportCareers = ($user->sport_careers()->count() > 0) ? $user->sport_careers()->orderBy(\DB::raw('-timeline_end'), 'asc')->get() : collect([]);
		
		        if(isset($sportCareers) && count($sportCareers) > 0) {
			        foreach ($sportCareers as $sc) {
								if ($clubId == $sc->club_id) {
									$response['success'] = false;
									$response['error'] = 'Dieser verein existiert bereits in Ihrer Liste.';
									return response()->json($response);
								}
			        }
		        }
            $club = Club::where('id', (int)$clubId)->orWhere('slug', str_slug($clubId))->first();

            $timeline_begin = $request['timeline_begin_year'] . "-" . str_pad($request['timeline_begin_month'], 2, "0", STR_PAD_LEFT);
            $timeline_end = '';
            if ($request['timeline_end_year'] != '' && $request['timeline_end_month'] != ''){
                $timeline_end = $request['timeline_end_year'] . "-" . str_pad($request['timeline_end_month'], 2, "0", STR_PAD_LEFT);
            } elseif ($request['timeline_end_year'] != '' && $request['timeline_end_month'] == ''){
                $timeline_end = $request['timeline_end_year'];
            }
            if ($request['timeline_end_month'] != '' && $request['timeline_end_year'] == ''){
                $timeline_end = str_pad($request['timeline_end_month'], 2, "0", STR_PAD_LEFT);
            }

            if ($club !== null) {
		            $sportCareer = new SportCareer;
		            $sc = $sportCareer->create([
			            'content'        => $request['content'],
			            'layout'         => $request['layout'],
			            'user_id'        => Auth::user()->id,
			            'timeline_begin' => $timeline_begin,
			            'timeline_end'   => $timeline_end,
			            'career_type'    => $request['career_type'],
		            ]);
                $sc->club()->associate($club);
                $sc->save();
            } else {
	            $response['success'] = false;
	            $response['error'] = 'Der Verein existiert nicht';
	            return response()->json($response);
            }

            $club = Club::where('id', $sc->club->id)->firstOrFail();
	
		        $memberRoleId = $role->getByConstant(Role::CLUB_MEMBER)->id;
		        $fanRoleId = $role->getByConstant(Role::CLUB_FAN)->id;
		        $memberActiveId = $member->getByConstant(Member::ACTIVE)->id;
		
		        $isAlreadyFan = $club->users()->wherePivot('user_id', $user->id)->wherePivot('role_id', $fanRoleId)->count() > 0;
		        $isAlreadyMember = $club->users()->wherePivot('user_id', $user->id)->wherePivot('role_id', $memberRoleId)->wherePivot('member_id', $memberActiveId)->count() > 0;
	
//	        return response()->json($isAlreadyMember);
		        
		        if (!$isAlreadyMember) {
			        if ($isAlreadyFan){
				        $club->users()->newPivotStatementForId($user->id)->where('role_id', $fanRoleId)->delete();
			        }
			        $club->users()->newPivotStatementForId($user->id)->where('role_id', $memberRoleId)->delete();
			        $club->users()->save($user, ['role_id' => $memberRoleId, 'member_id' => $memberActiveId]);
		        }
	      
//            if (!isset($request['timeline_end']) || empty($request['timeline_end'])) {
//                // if entries end date is omitted, set current user as member of selected club
//                $club->users()->attach($user->id, ['role_id' => $role->getByConstant(Role::CLUB_MEMBER)->id]);
//            } else {
//                // set current user as fan of selected club
//                $club->users()->attach($user->id, ['role_id' => $role->getByConstant(Role::CLUB_FAN)->id]);
//            }
//
//            if (isset($request['sports'])){
//                $sports = json_decode($request['sports']);
//                $sc->sports()->attach($sports);
//            }


            $imageUpload = $request->file('image');
            if ($imageUpload !== null) {
                $image = new Image(['picture' => $request['image']]);
                $sc->images()->save($image);
            }

            $response['success'] = true;
            $response['data'] = $sc->toArray();
            $response['data']['url'] = route('sport.career.template', $sc->id);

        } catch (\Exception $ex) {
	        
            $response['success'] = false;
            $response['error'] = $ex->getMessage();
        }


        return response()->json($response);


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Display the rendered resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function showRendered($id)
    {
        $response = [
            'success' => false,
            'template' => null,
            'error' => null,
        ];

        try {
            $sportCareer = new SportCareer;
            $response['success'] = true;
            $response['template'] = view('user.profile.partials.sport-career.entry', [
                'sportCareer' => $sportCareer->where('id', $id)->first(),
                'user' => Auth::user()
            ])->render();
        } catch (\Exception $ex) {
            $response['success'] = false;
            $response['error'] = $ex->getMessage();
        }

        return response()->json($response);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $response = [
            'success' => false,
            'template' => null,
            'error' => null,
        ];

        try {
            $sportCareer = new SportCareer;
            $response['success'] = true;
            $response['template'] = view('user.profile.partials.sport-career.edit.form', [
                'sportCareer' => $sportCareer->where('id', $id)->firstOrFail(),
            ])->render();

        } catch (\Exception $ex) {
            $response['success'] = false;
            $response['error'] = $ex->getMessage();
        }

        return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Vereinsleben\Http\Requests\SportCareerFormRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(SportCareerFormRequest $request, $id)
    {
        $response = [
            'success' => false,
            'data' => [],
            'error' => null,
        ];

        try {
            $role = new Role;
		        $member = new Member();
            $user = Auth::user();

            $sportCareer = new SportCareer;
            $clubId = trim($request->input('club_id'));
	
//	        return response()->json($clubId);
          
		        $sportCareers = ($user->sport_careers()->count() > 0) ? $user->sport_careers()->orderBy(\DB::raw('-timeline_end'), 'asc')->get() : collect([]);
		        
						if ($clubId != $request->old_club_id) {
							if(isset($sportCareers) && count($sportCareers) > 0) {
								foreach ($sportCareers as $sc) {
									if ($clubId == $sc->club_id) {
										$response['success'] = false;
										$response['error'] = 'Dieser verein existiert bereits in Ihrer Liste.';
										return response()->json($response);
									}
								}
							}
						}
	          $club = Club::where('id', (int)$clubId)->orWhere('slug', str_slug($clubId))->first();
            $timeline_end = '';
            $timeline_begin = $request['timeline_begin_year'] . "-" . str_pad($request['timeline_begin_month'], 2, "0", STR_PAD_LEFT);

            if ($request['timeline_end_year'] != '' && $request['timeline_end_month'] != ''){
                $timeline_end = $request['timeline_end_year'] . "-" . str_pad($request['timeline_end_month'], 2, "0", STR_PAD_LEFT);
            } elseif ($request['timeline_end_year'] != '' && $request['timeline_end_month'] == ''){
                $timeline_end = $request['timeline_end_year'];
            }
            if ($request['timeline_end_month'] != '' && $request['timeline_end_year'] == ''){
                $timeline_end = str_pad($request['timeline_end_month'], 2, "0", STR_PAD_LEFT);
            }

            if (isset($request['delete-image']) && $request['delete-image'] !== null) {
                foreach ($request['delete-image'] as $deleteImageId) {
                    $image = $sc->images()->find($deleteImageId);
                    if ($image !== null) {
                        $image->delete();
                    }
                }
            }

            if ($club !== null) {
		            $sportCareer->where('id', $id)
			            ->update([
				            'title'          => $request['title'],
				            'content'        => $request['content'],
				            'layout'         => $request['layout'],
				            'timeline_begin' => $timeline_begin,
				            'timeline_end'   => $timeline_end,
				            'club_id'        => ($club !== null) ? $club->id : null,
				            'career_type'    => $request['career_type'],
			            ]);
		            
		            $sc = SportCareer::findOrFail($id);
                $sc->club()->associate($club);
                $sc->save();
            } else {
	            $response['success'] = false;
	            $response['error'] = 'Der Verein existiert nicht';
	            return response()->json($response);
            }

            $club = Club::where('id', $sc->club->id)->firstOrFail();
	
	        $fanRoleId = $role->getByConstant(Role::CLUB_FAN)->id;
	        $memberActiveId = $member->getByConstant(Member::ACTIVE)->id;
	        $memberOldId = $member->getByConstant(Member::OLD)->id;
	        $memberRoleId = $role->getByConstant(Role::CLUB_MEMBER)->id;
	        $isAlreadyFan = $club->users()->wherePivot('user_id', $user->id)->wherePivot('role_id', $fanRoleId)->count() > 0;
	        $isAlreadyMember = $club->users()->wherePivot('user_id', $user->id)->wherePivot('role_id', $memberRoleId)->count() > 0;
	        $isAlreadyOldMember = $club->users()->wherePivot('user_id', $user->id)->wherePivot('member_id', $memberOldId)->count() > 0;
	
	        if ($request->club_id != $request->old_club_id) {
		        $oldClub = Club::where('id', $request->old_club_id)->firstOrFail();
		        $oldClub->users()->newPivotStatementForId($user->id)->where('role_id', $memberRoleId)->delete();
		        $oldClub->users()->save($user, ['role_id' => $memberRoleId, 'member_id' => $memberOldId]);
	        }
	        
	        
	        if (!$isAlreadyMember) {
		        if ($isAlreadyFan){
			        $club->users()->newPivotStatementForId($user->id)->where('role_id', $fanRoleId)->delete();
		        }
		        $club->users()->save($user, ['role_id' => $memberRoleId, 'member_id' => $memberActiveId]);
	        } else {
		        if ($isAlreadyOldMember){
			        $club->users()->newPivotStatementForId($user->id)->where('member_id', $memberOldId)->delete();
			        $club->users()->save($user, ['role_id' => $memberRoleId, 'member_id' => $memberActiveId]);
		        }
	        }
//            if (!isset($request['timeline_end']) || empty($request['timeline_end'])) {
//                // if entries end date is omitted, set current user as member of selected club
//                $club->users()->attach($user->id, ['role_id' => $role->getByConstant(Role::CLUB_MEMBER)->id]);
//            } else {
//                // set current user as fan of selected club
//                $club->users()->attach($user->id, ['role_id' => $role->getByConstant(Role::CLUB_FAN)->id]);
//            }

            if (isset($request['sports'])) {
                $sports = json_decode($request['sports']);
                $sc->sports()->attach($sports);
            }

            if (isset($request['removeSports'])) {
                $removeSports = json_decode($request['removeSports']);
                $sc->sports()->detach($removeSports);
            }

            $imageUpload = $request->file('image');
            if ($imageUpload !== null) {
                $sc->images()->delete();

                $image = new Image(['picture' => $request['image']]);
                $sc->images()->save($image);
            }

            $response['data'] = $sc->toArray();
            $response['success'] = true;

        } catch (\Exception $ex) {
            $response['success'] = false;
            $response['error'] = $ex->getMessage();
        }

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
	      $role = new Role;
		    $member = new Member();
        $response = [
            'success' => false,
            'data' => [],
            'error' => null,
        ];

        try {
            $sc = SportCareer::findOrFail($id);
	          $club = Club::findOrFail($sc->club_id);
	          $user = User::findOrFail($sc->user_id);
	          $memberRoleId = $role->getByConstant(Role::CLUB_MEMBER)->id;
	          
	          $isAlreadyMember = $club->users()->wherePivot('user_id', $user->id)
			          ->wherePivot('role_id', $memberRoleId)->count() > 0;
	          
	          if($isAlreadyMember) {
		          $memberOldId = $member->getByConstant(Member::OLD)->id;
		          $memberRoleId = $role->getByConstant(Role::CLUB_MEMBER)->id;
		          $club->users()->newPivotStatementForId($user->id)->where('role_id', $memberRoleId)->delete();
		          $club->users()->save($user, ['role_id' => $memberRoleId, 'member_id' => $memberOldId]);
	          }
	          
            $response['success'] = true;
            $response['data'] = $sc->get()->toArray();
            $sc->delete();

        } catch (\Exception $ex) {
            $response['success'] = false;
            $response['error'] = $ex->getMessage();
        }

        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Vereinsleben\Http\Requests\SportCareerClubFormRequest $request
     * @return \Illuminate\Http\Response
     */
    public function storeWizard(SportCareerClubFormRequest $request)
    {
        $response = [
            'success' => false,
            'data'    => [],
            'error'   => null,
        ];

        try{
            $role = new Role;
            $user = Auth::user();

            $clubId = trim($request->input('wcclub_id'));
            $club = Club::published()->where('id', (int)$clubId)->orWhere('slug', str_slug($clubId))->first();

            $timeline_begin = $request['wctimeline_begin_year'] . "-" . str_pad($request['wctimeline_begin_month'], 2, "0", STR_PAD_LEFT);
            $timeline_end = '';

            if ($request['wctimeline_end_year'] != '' && $request['wctimeline_end_month'] != ''){
                $timeline_end = $request['wctimeline_end_year'] . "-" . str_pad($request['wctimeline_end_month'], 2, "0", STR_PAD_LEFT);
            } elseif ($request['wctimeline_end_year'] != '' && $request['wctimeline_end_month'] == ''){
                $timeline_end = $request['wctimeline_end_year'];
            }
            if ($request['wctimeline_end_month'] != '' && $request['wctimeline_end_year'] == ''){
                $timeline_end = str_pad($request['wctimeline_end_month'], 2, "0", STR_PAD_LEFT);
            }

            $sportCareer = new SportCareer;
            $sc = $sportCareer->create([
                'user_id'        => Auth::user()->id,
                'timeline_begin' => $timeline_begin,
                'timeline_end'   => $timeline_end,
                'career_type'    => $request['wccareer_type'],
            ]);

            if ($club !== null){
                $sc->club()->associate($club);
                $sc->save();
            } else if (isset($clubId) && !empty($clubId)){
                $slug = str_slug($clubId);
                $validator = Validator::make(['slug' => $slug], [
                    'slug' => 'required|min:3|unique:club',
                ]);

                if ($validator->fails()){
                    $slug = str_slug($clubId . '-' . rand(100, 999), '-');
                }

                // create a new club               
                $newClub = Club::create([
                    'street'         => $request['wcstreet'],
                    'house_number'   => $request['wchouse_number'],
                    'zip'            => $request['wczip'],
                    'city'           => $request['wccity'],
                    'suggested_from' => Auth::user()->id,
                    'name'           => (string)$clubId,
                    'published'      => 1,
                    'slug'           => $slug,
                    'unowned'        => 1,
                ]);

                $sc->club()->associate($newClub);
                $sc->save();
            }

            $club = Club::where('id', $sc->club->id)->firstOrFail();

            if (trim($request['wcweb']) != ''){
                $link = new SocialLink();

                $link->link_type = 'web';

                if (!preg_match('/^https?:\/\//', trim($request['wcweb']))){
                    $link->url = "http://" . trim($request['wcweb']);
                }
                $club->socialLinks()->save($link);
            }

            if (!isset($request['wctimeline_end']) || empty($request['wctimeline_end'])){
                // if entries end date is omitted, set current user as member of selected club
                $club->users()->attach($user->id, ['role_id' => $role->getByConstant(Role::CLUB_MEMBER)->id]);
            } else{
                // set current user as fan of selected club
                $club->users()->attach($user->id, ['role_id' => $role->getByConstant(Role::CLUB_FAN)->id]);
            }

            if (isset($request['wcsports'])){
                $sports = array();
                $sports = $request['wcsports'];
                $sc->sports()->attach($sports);
            }

            $imageUpload = $request->file('wcimage');

            if ($imageUpload !== null){
                $image = new Image(['picture' => $request['wcimage']]);
                $sc->images()->save($image);
            }

        } catch (\Exception $ex){
            $response['success'] = false;
            $response['error'] = $ex->getMessage();
        }

        return \Redirect::route('user.detail', $user->username . "#sportvita")->with('flash-success', 'Dein Social-Linds sind erfolgreich gespeichrt!');
    }
}