<?php
namespace Vereinsleben\Http\Controllers;

use Illuminate\Http\Request;

use Vereinsleben\Http\Requests;
use Vereinsleben\Http\Requests\ClubTeamRequest;

use Vereinsleben\Team;
use Vereinsleben\Club;
use Vereinsleben\ClubSport;
use Vereinsleben\Department;
use Vereinsleben\User;
use Auth;

class TeamController extends BaseController
{
    /**
     * Show the form for add the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function add($id)
    {
        $department = Department::where('id', $id)->firstOrFail();

        $club = Club::where('id', $department->club_id)->firstOrFail();
        $users = $club->members()->pluck('fullname', 'user_id')->toArray();

        $form = view('club.partials.edit.teams', [
            'newTeam'    => true,
            'department' => $department,
            'club'       => $club,
            'users'      => $users,
            'dynamicId'  => $id,
        ]);

        return $form;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClubTeamRequest $request)
    {
        $team = new Team;

        $team->name = $request['name'];
        $team->description = $request['description'];
        $team->club_sport_id = $request['sport_id'];
        $team->club_id = $request['club_id'];
        $team->league = $request['league'];
        $team->age_class = $request['age_class'];
        $user_id = $request['user_id'];
        $team->department_id = $request['department_id'];

        $club = Club::where('id', $team->club_id)->firstOrFail();

        if ($request->hasFile('picture')) {
            $team->picture = $request->file('picture');
        }

        $clubSport = ClubSport::where('club_id', $club->id)->where('sport_id', $request['sport_id'])->first();
        $team->club_sport_id = $clubSport->id;

        $this->authorize($team);
        $team->save();


        if($user_id !== '' && $user_id !== null && $user_id !== 'Keiner ausgewählt'){

            $ansprechPartner = User::where('id', $user_id)->firstOrFail();

            if ($request['phone'] !== '' || $request['ap_email'] !== ''){
                $ansprechPartner->phone = $request['phone'];
                $ansprechPartner->ap_email = $request['ap_email'];
            }
            $team->contacts()->save($ansprechPartner);
        }


        return \Redirect::route('club.detail', $club->slug)->with('flash-success', 'Die Mannschaft wurde erfolgreich hinzugefügt');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $team = Team::findOrFail($id);
        $department = Department::where('id', $team->department_id)->firstOrFail();

        $club = Club::where('id', $department->club_id)->firstOrFail();
        $users = $club->members()->pluck('fullname', 'user_id')->toArray();
        $userid = $team->contacts()->get()->first();
        $this->authorize($team);

        $form = view('club.partials.edit.teams', [
            'team'       => $team,
            'department' => $department,
            'club'       => $club,
            'users'      => $users,
            'user'       => $userid,
            'dynamicId'  => $id,
        ]);
        return $form;
    } 

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClubTeamRequest $request, $id)
    {
        $team = Team::findOrFail($id);
        $club = Club::where('id', $team->club_id)->firstOrFail();
        $user_id = $request['user_id'];

        $fillableAndPresentInRequest = collect($team->getFillable())->filter(function ($item) use ($request) {
            return $request->input($item, null) !== null || $request->hasFile($item);
        })->toArray();

        $this->authorize($team);

        if ($request->input('delete-image') !== null && count($request->input('delete-image')) > 0) {
            $team->picture = STAPLER_NULL; // there is only one image for a team, so just delete
        }

        $team->update($request->only($fillableAndPresentInRequest));
        $team->save();


        if($user_id == '' || $user_id == null || $user_id == 'Keiner ausgewählt'){

            $team->contacts()->detach();
        }
        else{

            $ansprechPartner = User::where('id', $user_id)->firstOrFail();

            if ($request['phone'] !== '' || $request['ap_email'] !== ''){
                $ansprechPartner->phone = $request['phone'];
                $ansprechPartner->ap_email = $request['ap_email'];
            }

            $team->contacts()->detach();
            $team->contacts()->save($ansprechPartner);
        }


        return \Redirect::route('club.detail', $club->slug)->with('flash-success', 'Die Mannschaft wurde erfolgreich aktualisiert');
    }

    /**
     * Mark the specified team resource as deleted.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $team = Team::findOrFail($id);
        $team->contacts()->detach();
        $team->delete();

        return json_encode(['success' => true, 'item' => $team]);
    }

    /**
     * Return a single rendered event.
     */
    public function single($clubSlug, $id)
    {
        $team = Team::findOrFail($id);
        $teamcontactperson = $team->contacts()->get()->first();

        if($teamcontactperson){
            $contactpers = $teamcontactperson->firstname;
        }
        else{
            $contactpers = null;
        }

        $club = Club::where('id', $team->club_id)->firstOrFail();

        return view('club.partials.team', [
            'team'          => $team,
            'club'          => $club,
            'contactperson' => $contactpers,
        ])->render();
        abort(403);
    }
}