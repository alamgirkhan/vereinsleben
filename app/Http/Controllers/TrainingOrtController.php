<?php

namespace Vereinsleben\Http\Controllers;

use Geocoder;
use Illuminate\Http\Request;
use Log;
use Vereinsleben\Club;
use Vereinsleben\Helpers\GeocodeHelper;
use Vereinsleben\Http\Controllers\Controller;
use Vereinsleben\Http\Requests\TrainingortFormRequest;
use Vereinsleben\Services\GeocoderHelper;
use Vereinsleben\Trainingsort;
use Auth;

class TrainingOrtController extends BaseController
{
    /**
     * @param TrainingortFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(TrainingortFormRequest $request)
    {

        $club = Club::where('id', $request['club_id'])->firstOrFail();
        $trainingsort = new Trainingsort();
        $trainingsort->name = $request['name'];
        $trainingsort->title = $request['title'];
        $trainingsort->city = $request['city'];
        $trainingsort->street = $request['street'];
        $trainingsort->house_number = $request['house_number'];
        $trainingsort->zip = $request['zip'];
//        try{
//            $geocoderHelper = new GeocoderHelper('Germany');
//            $geocoderHelper->setAddress($request['zip'], $request['city'], $request['street'], $request['house_number']);
//
//            $trainingsort->federal_state = $geocoderHelper->getFederalState();
//            $trainingsort->lat = $geocoderHelper->getLat();
//            $trainingsort->lng = $geocoderHelper->getLon();
//
//        } catch (\Exception $e){
//
//            $trainingsort->lat = null;
//            $trainingsort->lng = null;
//
//            Log::warning($e->getMessage(), [
//                'during'       => 'trainingsort@store',
//                'trainingsort' => $trainingsort->id
//            ]);
//        }
        $this->authorize($club);
        $club->traininsorts()->save($trainingsort);

        return \Redirect::route('club.detail', $club->slug)->with('flash-success', 'Deine Trainingsort wurde erfolgreich gespeichert');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $training = Trainingsort::findOrFail($id);
        $club = $training->clubs()->firstOrFail();
        $this->authorize($club);

        $form = view('club.partials.edit.training-orte', [
            'training' => $training,
            'club'     => $club
        ]);

        return $form;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $club = Club::where('id', $request['club_id'])->firstOrFail();
        $trainisort = Trainingsort::findOrFail($id);
        $trainisort->name = $request['name'];
        $trainisort->title = $request['title'];
        $trainisort->city = $request['city'];
        $trainisort->street = $request['street'];
        $trainisort->house_number = $request['house_number'];
        $trainisort->zip = $request['zip'];
//        try{
//            $geocoderHelper = new GeocoderHelper('Germany');
//            $geocoderHelper->setAddress($request['zip'], $request['city'], $request['street'], $request['house_number']);
//
//            $trainisort->federal_state = $geocoderHelper->getFederalState();
//            $trainisort->lat = $geocoderHelper->getLat();
//            $trainisort->lng = $geocoderHelper->getLon();
//
//        } catch (\Exception $e){
//
//            $trainisort->lat = null;
//            $trainisort->lng = null;
//
//            Log::warning($e->getMessage(), [
//                'during'       => 'trainingsort@store',
//                'trainingsort' => $trainisort->id
//            ]);
//        }
        $this->authorize($club);
        $club->traininsorts()->save($trainisort);

        return \Redirect::route('club.detail', $club->slug)->with('flash-success', 'Deine Trainingsort wurde erfolgreich gespeichert');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($clubSlug, $id)
    {
        $club = Club::where('slug', $clubSlug)->firstOrFail();
        $trainingsort = Trainingsort::findOrFail($id);
        //$this->authorize($trainingsort);
        $club->traininsorts()->detach($id);
        return json_encode(['success' => true, 'item' => $trainingsort]);
    }


    /**
     * Return a single rendered training ort.
     */
    public function single($clubSlug, $id)
    {
        $club = Club::where('slug', $clubSlug)->firstOrFail();
        $user = Auth::user();
        $trainingOrt = $club->traininsorts->where('id', $id)->first();

        //if (policy($trainingOrt)->show($user, $trainingOrt)) {
        return view('club.partials.training-orte', ['trainingOrt' => $trainingOrt, 'club' => $club])->render();
        //}

        abort(403);
    }	
}
