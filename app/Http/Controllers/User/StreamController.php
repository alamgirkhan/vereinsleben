<?php
	
	namespace Vereinsleben\Http\Controllers\User;
	
	use Carbon\Carbon;
	use Illuminate\Http\Request;
	
	use Vereinsleben\Campaign;
	use Vereinsleben\Http\Controllers\BaseController;
	use Vereinsleben\Http\Requests;
	use Vereinsleben\Http\Controllers\Controller;
	
	use \DB;
	
	use Vereinsleben\Helpers\LocationHelper;
	use Vereinsleben\Helpers\CustomPaginator;
	use Vereinsleben\Post;
	use Vereinsleben\User;
	use Vereinsleben\News;
	use Vereinsleben\Event;
	use Vereinsleben\Club;
	use Auth;
	
	
	class StreamController extends BaseController
	{
		
		const QUERY_FALLBACK_LIMIT = 12;
		
		public function __construct(Request $request)
		{
			parent::__construct();
		}
		
		
		/**
		 * Return a unified resource object
		 *
		 * @param Request $request
		 * @return \Illuminate\Http\Response
		 */
		
		public function index(Request $request)
		{
			if (!(Auth::check())) {
				return response()->json(['error' => 'Not authorized.'], 403);
			}
			$userid = Auth::user()->id;
			$data = collect([]);
			$clubPosts = collect([]);
			$bandagePosts = collect([]);
			$clubEvents = collect([]);
			$bandageEvents = collect([]);
			
			$user = User::where('id', $userid)->firstOrFail();
			
			$news = new News;
			$paginator = new CustomPaginator;
			
			$latestNews = $news->latestUpdatedNews(50);
			$posts = collect([]);
//        $posts = $user->posts()->with('videos', 'images')->latest('published_at')->get();
			foreach ($user->getFriends() as $friend) {
				$friendPost = $friend->posts()->latest('published_at')->get();
				$posts = $friend->posts()->latest('published_at')->get();
				$posts = $posts->merge($friendPost);
			}
			
			$posts->transform(function ($post) {
				$user = $post->user;
				$post->name = $user->fullname;
				$post->avatar = $user->avatar->url('singleView');
				$post->userUrl = isset($user->username) ? route('user.detail', $user->username) : '';
				
				return $post;
			});
			
			
			$clubs = $user->clubsWhereFan()
				->with([
					'posts' => function ($query) {
						$query->with('images')->public();
					},
					'events' => function ($query) {
						$query->with('images')->latest('schedule_begin')->public();
					}])
				->get();
			$memberClubs = $user->clubsWhereMember()
				->with([
					'posts' => function ($query) {
						$query->with('images')->public();
					},
					'events' => function ($query) {
						$query->with('images')->latest('schedule_begin')->public();
					}])
				->get();
			
			$bandages = $user->bandagesWhereSubscriber()
				->with([
					'posts' => function ($query) {
						$query->with('images')->public();
					},
					'events' => function ($query) {
						$query->with('images')->latest('schedule_begin')->public();
					}])
				->get();
			
			
//			return response()->json($bandages);
			
			$clubs = $clubs->merge($memberClubs);
			
			$clubs->each(function ($club) use ($clubPosts, $clubEvents) {
				$club->imageUrl = $club->avatar->url('singleView');
				
				$club->events->transform(function ($event) use ($club) {
					$event->club = [
						'id' => $club->id,
						'name' => $club->name,
						'avatar' => $club->avatar->url('singleView'),
						'url' => isset($club->slug) ? route('club.detail', $club->slug) : '',
					];
					
					return $event;
				});
				
				$club->posts->transform(function ($post) use ($club) {
					$post->club = [
						'id' => $club->id,
						'name' => $club->name,
						'avatar' => $club->avatar->url('singleView'),
						'url' => isset($club->slug) ? route('club.detail', $club->slug) : '',
					];
					
					return $post;
				});
				
				$clubEvents->push($club->events);
				$clubPosts->push($club->posts);
			});
			
			
			$bandages->each(function ($bandage) use ($bandagePosts, $bandageEvents) {
				$bandage->imageUrl = $bandage->avatar->url('singleView');
				
				$bandage->events->transform(function ($event) use ($bandage) {
					$event->bandage = [
						'id' => $bandage->id,
						'name' => $bandage->name,
						'avatar' => $bandage->avatar->url('singleView'),
						'url' => isset($bandage->slug) ? route('bandage.detail', $bandage->slug) : '',
					];
					
					return $event;
				});
				
				$bandage->posts->transform(function ($post) use ($bandage) {
					$post->bandage = [
						'id' => $bandage->id,
						'name' => $bandage->name,
						'avatar' => $bandage->avatar->url('singleView'),
						'url' => isset($bandage->slug) ? route('bandage.detail', $bandage->slug) : '',
					];
					
					return $post;
				});
				
				$bandageEvents->push($bandage->events);
				$bandagePosts->push($bandage->posts);
			});
			
			$bandageEvents->flatten(2)->transform(function ($event) {
				$event->type = 'bandageEvent';
				$event->sort_date = $event->published_at;
				
				if ($event->images->count() > 0) {
					
					$event->images->transform(function ($image) {
						$image->imageUrl = $image->picture->url('singleView');
						
						return $image;
					});
				}
				
				return $event;
			});
			
			$bandagePosts->flatten(2)->transform(function ($post) {
				$post->type = 'bandagePost';
				$post->sort_date = $post->published_at;
				
				if ($post->images->count() > 0) {
					
					$post->images->transform(function ($image) {
						$image->imageUrl = $image->picture->url('singleView');
						
						return $image;
					});
				}
				
				return $post;
			});
			
			$clubEvents->flatten(2)->transform(function ($event) {
				$event->type = 'clubEvent';
				$event->sort_date = $event->published_at;
				
				if ($event->images->count() > 0) {
					
					$event->images->transform(function ($image) {
						$image->imageUrl = $image->picture->url('singleView');
						
						return $image;
					});
				}
				
				return $event;
			});
			
			$clubPosts->flatten(2)->transform(function ($post) {
				$post->type = 'clubPost';
				$post->sort_date = $post->published_at;
				
				if ($post->images->count() > 0) {
					
					$post->images->transform(function ($image) {
						$image->imageUrl = $image->picture->url('singleView');
						
						return $image;
					});
				}
				
				return $post;
			});
			
			$posts->transform(function ($post) {
				$post->type = 'post';
				$post->sort_date = $post->published_at;
				
				if ($post->images->count() > 0) {
					
					$post->images->transform(function ($image) {
						$image->imageUrl = $image->picture->url('singleView');
						
						return $image;
					});
				}
				
				return $post;
			});
			
			
			$latestNews = $latestNews->filter(function ($val, $key) {
				return $val->element_order <= 12;
			});
			
			$latestNews->transform(function ($newsItem) {
				$newsItem->type = 'news';
				$newsItem->sort_date = $newsItem->published_at;
				$newsItem->image = $newsItem->main_image->url('singleview');
				$newsItem->parentCategory = $newsItem->category->parentCategory->title;
				$newsItem->link = route('news.detail', [$newsItem->category->id, $newsItem->slug]);
				
				return $newsItem;
			});
			
			$data->push($posts);
			$data->push($clubPosts->flatten(2));
			$data->push($clubEvents->flatten(2));
			$data->push($bandagePosts->flatten(2));
			$data->push($bandageEvents->flatten(2));
			$data->push($latestNews);
			
			$result = $data->flatten(1)->sortByDesc('sort_date');
			
			$result->transform(function ($r, $key) {
				$r->sort_order = $key;
				
				return $r;
			});
			
			$items = $result->flatten(2)->toArray();
			$paginator->page = ($request->input('page') != null) ? (int)$request->input('page') : 1;
//        $paginator->size = ($request->input('page') != null) ? 3 : 12;
			$paginator->size = ($request->input('page') != null) ? 8 : 12;
			$paginator->extraOffset = ($request->input('page') != null) ? 9 : 0;
			$paginator->data = $items;
			
			$paginated = $paginator->paginate();
			
			return response()->json($paginated);
		}
		
		/**
		 * @param Request $request
		 * @return \Illuminate\Http\JsonResponse
		 */
		public function events(Request $request)
		{
			if (!(Auth::check())) {
				return response()->json(['error' => 'Not authorized.'], 403);
			}
			$paginator = new CustomPaginator;
			$locationHelper = new LocationHelper;
			
			$excludeClubs = $request->user()->clubsWhereOwner()->get()->pluck('id');
			
			$locationHelper->entity = 'event';
			
			$eventsByLocation = $locationHelper->find();
			
			if ($eventsByLocation) {
				$eventIds = collect($eventsByLocation->get())->keyBy('id');
				$events = Event::with('club')->whereIn('id', $eventIds->keys())->public()->whereNotIn('id', $excludeClubs)->latest('schedule_begin')
					->where('schedule_begin' ,'>=', Carbon::now()->addMonths(-2))
					->where('schedule_end' ,'<=', Carbon::now()->addMonths(2))
					->get();
			} else {
				$events = Event::with('club')->latest('schedule_begin')->public()->whereNotIn('id', $excludeClubs)->take(self::QUERY_FALLBACK_LIMIT)
					->where('schedule_begin' ,'>=', Carbon::now()->addMonths(-2))
					->where('schedule_end' ,'<=', Carbon::now()->addMonths(2))
					->get();
			}
			
			$events->transform(function ($event) {
				$event->type = 'clubEvent';
				$event->url = isset($event->club->slug) ? route('club.detail', $event->club->slug) : '';
				
				if ($event->images->count() > 0) {
					
					$event->images->transform(function ($image) {
						$image->imageUrl = $image->picture->url('singleView');
						
						return $image;
					});
				}
				
				return $event;
			});
			
			
			$paginator->page = ($request->input('page') != null) ? (int)$request->input('page') : 1;
//        $paginator->size = 3;
			$paginator->size = 4;
			$paginator->data = $events->all();
			
			return response()->json($paginator->paginate());
		}
		
		/**
		 * @param Request $request
		 * @return \Illuminate\Http\JsonResponse
		 */
		public function clubs(Request $request)
		{
			if (!(Auth::check())) {
				return response()->json(['error' => 'Not authorized.'], 403);
			}
			$paginator = new CustomPaginator;
			$locationHelper = new LocationHelper;
			
			$excludeClubs = $request->user()->clubsWhereOwner()->get()->pluck('id');
			
			$locationHelper->entity = 'club';
			
			$clubsByLocation = $locationHelper->find();
			
			if ($clubsByLocation) {
				$clubIds = collect($clubsByLocation->get())->keyBy('id');
				$clubs = Club::published()->whereIn('id', $clubIds->keys())->whereNotIn('id', $excludeClubs)->get();
			} else {
				$clubs = Club::published()->latest()->whereNotIn('id', $excludeClubs)->take(self::QUERY_FALLBACK_LIMIT)->get();
			}
			
			$clubs->transform(function ($club) {
				$club->imageUrl = $club->avatar->url('singleView');
				$club->url = route('club.detail', $club->slug);
				
				return $club;
			});
			
			$paginator->page = ($request->input('page') != null) ? (int)$request->input('page') : 1;
//        $paginator->size = 3;
			$paginator->size = 4;
			$paginator->data = $clubs->all();
			
			return response()->json($paginator->paginate());
		}
		
		/**
		 * @param Request $request
		 * @return \Illuminate\Http\JsonResponse
		 */
		public function users(Request $request)
		{
			if (!(Auth::check())) {
				return response()->json(['error' => 'Not authorized.'], 403);
			}
			$paginator = new CustomPaginator;
			$locationHelper = new LocationHelper;
			$userIds = null;
			
			$locationHelper->entity = 'user';
			
			$usersByLocation = $locationHelper->find();
			
			if ($usersByLocation) {
				$userIds = collect($usersByLocation->get())->keyBy('id')->except($request->user()->id);
				$users = User::whereIn('id', $userIds->keys())->get();
			} else {
				$users = User::verified()->latest()->take(self::QUERY_FALLBACK_LIMIT)->get()->except($request->user()->id);
			}
			
			$users->transform(function ($user) use ($userIds) {
				$user->imageUrl = $user->avatar->url('singleView');
				$user->distance = ($userIds) ? $userIds->get($user->id)->distance : 0;
				$user->url = route('user.detail', $user->username);
//
//            $user->clubs->map(function($club){
//                $club->imageUrl = $club->avatar->url('singleView');
//                $club->url = route('club.detail', $club->slug);
//
//                return $club;
//            })->slice(0, 2);
//
//            $user->sports->map(function($sport){
//                return $sport;
//            })->slice(0, 2);
				
				
				return $user->makeVisible(['url', 'imageUrl', 'clubs', 'sports', 'distance']);
			});
			
			$paginator->page = ($request->input('page') != null) ? (int)$request->input('page') : 1;
//        $paginator->size = 3;
			$paginator->size = 4;
			$paginator->data = $users->sortBy('distance')->all();
			
			return response()->json($paginator->paginate());
		}
		
		/**
		 * Display the users profile streaming
		 *
		 * @return \Vereinsleben\Http\Controllers\StreamController
		 */
		function streaming(Request $request)
		{
			if (!(Auth::check())) {
				return redirect('/')->with('flash-warning', 'Sie sind nicht eingeloggt!');
			}
			return view("user.profile.stream");
		}
		
		/**
		 * Show the form for creating a new resource.
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function create()
		{
			//
		}
		
		/**
		 * Store a newly created resource in storage.
		 *
		 * @param  \Illuminate\Http\Request $request
		 * @return \Illuminate\Http\Response
		 */
		public function store(Request $request)
		{
			//
		}
		
		/**
		 * Display the specified resource.
		 *
		 * @param  int $id
		 * @return \Illuminate\Http\Response
		 */
		public function show($id)
		{
			//
		}
		
		/**
		 * Show the form for editing the specified resource.
		 *
		 * @param  int $id
		 * @return \Illuminate\Http\Response
		 */
		public function edit($id)
		{
			//
		}
		
		/**
		 * Update the specified resource in storage.
		 *
		 * @param  \Illuminate\Http\Request $request
		 * @param  int $id
		 * @return \Illuminate\Http\Response
		 */
		public function update(Request $request, $id)
		{
			//
		}
		
		/**
		 * Remove the specified resource from storage.
		 *
		 * @param  int $id
		 * @return \Illuminate\Http\Response
		 */
		public function destroy($id)
		{
			//
		}
	}