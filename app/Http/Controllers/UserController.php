<?php

namespace Vereinsleben\Http\Controllers;


use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;

use Carbon\Carbon;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Redirect;
use Vereinsleben\Content;
use Vereinsleben\Helpers\VenusNewsletter;
use Vereinsleben\Http\Requests\UserProfileFormRequest;

use Vereinsleben\Services\GeocoderHelper;
use Vereinsleben\SocialLink;
use Vereinsleben\SportCareer;
use Vereinsleben\State;
use Vereinsleben\User;
use Vereinsleben\Role;
use Vereinsleben\Club;
use Vereinsleben\Sport;

use Auth;
use Mapper;
use Image;
use Cache;
use Mail;


class UserController extends BaseController
{
    const CACHE_LIFETIME_MINUTES = 30;
    const RECORDS_PER_PAGE       = 12;

    protected $_auth;

    /**
     * Constructor.
     *
     * @param \Illuminate\Contracts\Auth\Guard
     * @return \Illuminate\Http\Response
     */
    public function __construct(Request $request, Guard $auth)
    {
        parent::__construct($request);

//        $this->_auth = $auth;
//        $this->middleware('guest', ['except' => ['create', 'show', 'store', 'logout', 'update', 'patch', 'authenticate']]);
    }

    /**
     * Display the dashboard
     *
     * @param \Illuminate\Http\Request
     * @return \Vereinsleben\Http\Controllers\UserController
     */
    public function dashboard(Request $request)
    {
        $user = Auth::user();
        $clubs = $user->clubsWhereAdmin()->get();

        if ($user->firstname == '' || $user->firstname == null){
            $request->session()->flash('flash-info', 'Bitte ' . link_to_route('user.profile.edit', '&raquo; vervollständige Dein Benutzerprofil') . '!');
        }

        return view('user.dashboard', [
            'user'  => $user,
            'clubs' => $clubs,
            'map'   => $this->getCurrentUserMap(),
        ]);
    }


    public function getContact($userid)
    {
        if (Auth::user() && $userid){
            $user = User::where('id', $userid)->firstOrFail();

            return $user->phone . ':' . $user->ap_email;
        }
    }


    /**
     * Display the profile edit form.
     *
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function editProfile(Request $request)
    {
        $user = Auth::user();
        $clubs = $user->clubs;

        return view('user.edit-profile', [
            'user' => $user,
        ]);
    }

    /**
     * Update the profile in storage.
     *
     * @param  \Vereinsleben\Http\Requests\UserProfileFormRequest $request
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(UserProfileFormRequest $request)
    {

        $user = Auth::user();

        $birthday = $request->get('birthday-year') . '-' . str_pad($request->get('birthday-month'), 2, '0', STR_PAD_LEFT) . '-' .
            str_pad($request->get('birthday-day'), 2, '0', STR_PAD_LEFT) . ' 12:59:59';

        $newsletty = ($request->get('newsletter') === '1') ? 1 : 0;


        try{

            $geocoderHelper = new GeocoderHelper('Germany');
            $geocoderHelper->setAddress($request->get('zip'), $request->get('city'), $request['street'], $request['house_number']);
        } catch (\Exception $e){
            $geocoderHelper = null;
        }

        //unsubscribe from Venus Newsletter, if newsletter set to 0 *
        $user->newsletter = $newsletty;

        try
        {
            $newsletterConfig = config('newsletter');

            if (isset($newsletterConfig['auth_client_id']) && $newsletterConfig['auth_client_id'] !== NULL)
            {

                $venus = new VenusNewsletter($newsletterConfig);
                $venus->updateUser($user);
            }
        } catch (\Exception $e)
        {

            //return \Redirect::route('user.detail', $user->username)->with('flash-warning', 'Newsletter-Ab-/Anmeldung gescheitert.');
            // fail silently and capture the exception; we do not want to bother the user with this.
            app('log')->error('Venus newsletter: Failed to unsubscribe e-mail ' . $user->email . ', error: ' . $e->getMessage());
            //app('sentry')->captureException($e);
        }

        if ($geocoderHelper !== null){
            $firstname = mb_convert_case(mb_strtolower($request->get('firstname')), MB_CASE_TITLE, "UTF-8");
            $lastname = mb_convert_case(mb_strtolower($request->get('lastname')), MB_CASE_TITLE, "UTF-8");
            $street = mb_convert_case(mb_strtolower($request->get('street')), MB_CASE_TITLE, "UTF-8");
            $city = mb_convert_case(mb_strtolower($request->get('city')), MB_CASE_TITLE, "UTF-8");
            $firstname = $user->checkNameFormat($firstname);
            $street = $user->checkNameFormat($street);
            $city = $user->checkNameFormat($city);
            $lastname = $user->checkNameFormat($lastname);

            $user->update([
                'gender'        => $request->get('gender'),
                'firstname'     => $firstname,
                'lastname'      => $lastname,
                'nickname'      => $request->get('nickname'),
                'birthday'      => $birthday,
                'street'        => $street,
                'house_number'  => $request->get('house_number'),
                'zip'           => $request->get('zip'),
                'city'          => $city,
                'federal_state' => $request->get('federal_state'),
                'newsletter'    => $newsletty,
                'messages'      => ($request->get('messages') === '1') ? 1 : 0,
            ]);

            $user->states()->detach();
            $state = State::where('name', $request->get('federal_state'))->first();
            if ($state){
                $user->states()->save($state);
            }

            return \Redirect::route('user.detail', $user->username . '#informationen')->with('flash-success', 'Ihr Profil wurde erfolgreich gespeichert.');
        }
    }


    /**
     * Display profile info wizard
     *
     * @param \Illuminate\Http\Request
     * @return \Vereinsleben\Http\Controllers\UserController
     */
    public function profileWizard()
    {
        if (Auth::user()){
            $user = Auth::user();
            $state = State::pluck('name', 'id')->toArray();

            return view('user.profile.modal.profile-info-wizard', ['me' => $user, 'state' => $state]);
        }
    }


    /**
     * Update the profile in storage.
     *
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function updateWizardProfile(Request $request)
    {
        $user = Auth::user();
        $current = $request->get('current');
        if ($user && $request->get('save')){

            if ($request->get('state')){
                $user->states()->detach();
                $state = State::where('id', $request->state)->first();
                $user->update(['federal_state' => $state->name]);
                $user->states()->save($state);
                session(['wizard' => 0]);
            }

            if ($request->get('firstname') && $current == 'anrede'){
                $firstname = mb_convert_case(mb_strtolower($request->get('firstname')), MB_CASE_TITLE, "UTF-8");
                $lastname = mb_convert_case(mb_strtolower($request->get('lastname')), MB_CASE_TITLE, "UTF-8");
                $firstname = $user->checkNameFormat($firstname);
                $lastname = $user->checkNameFormat($lastname);

                $specialSigns=array('.',',','-','!','*','+','#','!','?','$','%','&','/','(',')','=','0','1','2','3','4','5','6','7','8','9',';',':','_','<','>','|');
                $cntLastname=$lastname;
                foreach($specialSigns as $ssign)
                {
                    $cntLastname = str_replace($ssign,'',$cntLastname);
                }
                $cntFirsttname=$firstname;
                foreach($specialSigns as $ssign)
                {
                    $cntFirsttname = str_replace($ssign,'',$cntFirsttname);
                }

                $fullname = $firstname . ' ' . $lastname;
                if (strlen($cntFirsttname)>1 && strlen($cntLastname)>1 && strlen($request->get('gender'))>1)
                {
                    $user->update([
                        'gender'    => $request->get('gender'),
                        'firstname' => $firstname,
                        'lastname'  => $lastname,
                        'fullname'  => $fullname,
                    ]);
                    session(['wizard' => 0]);
                }
            } else
                if ($request->get('birthday-year') && $request->get('birthday-month') && $request->get('birthday-day') && $current == 'dob'){

                $birthday = $request->get('birthday-year') . '-' . str_pad($request->get('birthday-month'), 2, '0', STR_PAD_LEFT) . '-' .
                    str_pad($request->get('birthday-day'), 2, '0', STR_PAD_LEFT) . ' 12:59:59';

                $user->update(['birthday' => $birthday]);
                session(['wizard' => 0]);
            } else
                if ($request->get('city') && $request->get('zip') && $current == 'postleitzahl'){
                $city = mb_convert_case(mb_strtolower($request->get('city')), MB_CASE_TITLE, "UTF-8");
                $city = $user->checkNameFormat($city);
                $user->update(['city' => $city, 'zip' => $request->get('zip')]);
                session(['wizard' => 0]);
            }
            /*else{

            }*/
        }

        if ($request->get('save') || ($current == 'final')){
            if ($current == 'state'){
                if ($user->isProfileCompleted()){
                    echo 'thanks';
                } else{
                    echo 'info';
                }
            } else if ($user->gender == '' || $user->firstname == '' || $user->lastname == ''){
                echo 'anrede';
            } else if ($user->birthday == ''){
                echo 'dob';
            } else if ($user->city == '' || $user->zip == ''){
                echo 'postleitzahl';
            } else{
                echo 'thanks';
            }
        } else{
            $skipArr = [];
            if ($user->gender == '' || $user->firstname == '' || $user->lastname == ''){
                $skipArr[] = 'anrede';
            }
            if ($user->birthday == ''){
                $skipArr[] = 'dob';
            }
            if ($user->city == '' || $user->zip == ''){
                $skipArr[] = 'postleitzahl';
            }
            $key = array_search($current, $skipArr);

            if (array_key_exists(($key + 1), $skipArr)){
                echo $skipArr[($key + 1)];
            } else{
                echo 'final';
            }
        }
        try
        {
            $newsletterConfig = config('newsletter');
            if (isset($newsletterConfig['auth_client_id']) && $newsletterConfig['auth_client_id'] !== NULL)
            {

                $venus = new VenusNewsletter($newsletterConfig);
                $venus->updateUser($user);
            }
        } catch (\Exception $e)
        {

            //return \Redirect::route('user.detail', $user->username)->with('flash-warning', 'Newsletter-Ab-/Anmeldung gescheitert.');
            // fail silently and capture the exception; we do not want to bother the user with this.
            app('log')->error('Venus newsletter: Failed to unsubscribe e-mail ' . $user->email . ', error: ' . $e->getMessage());
            //app('sentry')->captureException($e);
        }
    }

    /**
     * Update the email in storage.
     *
     * @param  \Vereinsleben\Http\Requests\UserProfileFormRequest $request
     */
    public function updateEmail(Request $request)
    {
        $user = Auth::user();

        $new_email = $request->get('new_email');

        $validator = Validator::make(['email' => $new_email], [
            'email' => 'email',
        ]);

        if ($validator->fails()){
            return \Redirect::route('user.detail', $user->username)->with('flash-error', 'E-Mail format is wrong');
        }

        $validator = Validator::make(['email' => $new_email], [
            'email' => 'unique:user',
        ]);

        if ($validator->fails()){
            return \Redirect::route('user.detail', $user->username)->with('flash-error', 'E-Mail ist bereits vergeben');
        }

        $user->update([
            'change_email_token'            => str_random(054),
            'change_email_unconfirmedEmail' => $new_email,
        ]);

        $imprint = Content::select('content')->where('slug', 'imprint')->first();

        Mail::send('auth.emails.update-email', ['user' => $user, 'imprint' => $imprint], function($message) use ($user, $new_email){
            $message->to($new_email, $user->username)->subject('vereinsleben.de - Registrierung bestätigen');
        });

        return \Redirect::route('user.detail', $user->username)->with('flash-success', 'Ihr Profil wurde erfolgreich gespeichert.');

    }


    function verifyEmail(Request $request, $token)
    {
        if (!$token){
            $request->session()->flash('flash-error', 'Der Bestätigungslink war fehlerhaft. Bitte kopieren Sie den Link vollständig aus Ihrer E-Mail.');

            return redirect('/');
        }

        $user = User::where('change_email_token', $token)->first();

        if (!$user){
            $request->session()->flash('flash-error', 'Der Bestätigungslink war fehlerhaft');

            return redirect('/');
        }

        $newEmail = $user->change_email_unconfirmedEmail;
        $users = User::where('email', $newEmail)->get();
        if (!count($users)){
            $user->email = $newEmail;
            $user->save();
            $request->session()->flash('flash-success', 'Die E-Mail wurde erfolgreich aktualisiert');

            return redirect('/');
        } else{
            $request->session()->flash('flash-error', 'Etwas ist schief gelaufen');

            return redirect('/');
        }
    }

    /**
     * Display the password edit form.
     *
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function editPassword(Request $request)
    {
        $user = Auth::user();
        $clubs = $user->clubs;

        return view('user.edit-password', [
            'user' => $user,
            'map'  => $this->getCurrentUserMap(),
        ]);
    }

    /**
     * Update the password in storage.
     *
     * @param  \Vereinsleben\Http\Requests\UserAccountFormRequest $request
     * @param  int $tenantId
     * @param  int $matterId
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(Request $request, HasherContract $hasher)
    {
        $user = Auth::user();

        if ($request->get('current-password')){
            if (!$hasher->check(trim($request->get('current-password')), trim($user->password))){
                return \Redirect::route('user.detail', $user->username)->with('flash-warning', 'Bitte überprüfen Sie Ihr aktuelles Passwort.');
            }

            if (trim($request->get('new-password')) !== trim($request->get('new-password-confirmation'))){
                return \Redirect::route('user.detail', $user->username)->with('flash-warning', 'Ihr neues Passwort wurde nicht korrekt bestätigt. Bitte versuchen Sie es erneut.');
            }

            $user->password = $hasher->make(trim($request->get('new-password')));
            $user->save();
        }

        return \Redirect::route('user.detail', $user->username)->with('flash-info', 'Ihr Passwort wurde erfolgreich gespeichert.');
    }

    /**
     * Display the friends
     *
     * @param \Illuminate\Http\Request
     * @return \Vereinsleben\Http\Controllers\UserController
     */
    public function searchFriends($username)
    {
        $user = User::where('username', $username)->firstOrFail();

        //todo change the suggested friends to a function that suggest really good friends with parameters
        $suggestedFriends = User::where('username', '!=', $username)
                                ->verified()
                                ->latestProfileCompleted()
                                ->paginate(20);

        return view('user.profile.suggested-friends', [
            'user'             => $user,
            'suggestedFriends' => $suggestedFriends
        ]);
    }


    /**
     * Display the friends
     *
     * @param \Illuminate\Http\Request
     * @return \Vereinsleben\Http\Controllers\UserController
     */
    public function showFriends($username)
    {

        $user = User::where('username', $username)->firstOrFail();

        return view('user.profile.friends', [
            'user' => $user
        ]);
    }

    /**
     * @param $senderUsername
     * @param $recipientUsername
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createFriendship($senderUsername, $recipientUsername)
    {
        $relation = Input::get('relation');
        $ajax = Input::get('ajax');
        $senderUser = User::where('username', $senderUsername)->firstOrFail();
        $this->authorize('createFriendship', $senderUser);
        $recipientUser = User::where('username', $recipientUsername)->firstOrFail();

        switch ($relation){
            case 'send-friend-request':
                $senderUser->befriend($recipientUser);
                $msg = 'Anfrage gesendet';

                $imprint = Content::select('content')->where('slug', 'imprint')->first();

                //27-03-2018: Recipient should get an E-Mail that he has a friendrequest
                Mail::send('/user/friend-request', ['user' => $senderUser, 'recipient' => $recipientUser, 'imprint' => $imprint], function($message) use ($senderUser, $recipientUser){
                    $message->to($recipientUser->email)->subject('Freundschaftsanfrage von ' . $senderUser->firstname);
                });

                break;
            case 'accept':
                $senderUser->acceptFriendRequest($recipientUser);
                $msg = 'akzeptiert';
                break;
            case 'deny':
                $senderUser->denyFriendRequest($recipientUser);
                $msg = 'abgelehnt';
                break;
        }
        if ($ajax){
            return $msg;
        }

        return \Redirect::route('user.profile.search.friends', $senderUsername);
    }

    /**
     * @param $username
     * @param $friendsname
     * @return \Illuminate\Http\RedirectResponse
     */
    public function editFriendship($username, $friendsname)
    {
        $relation = Input::get('relation');
        $ajax = Input::get('ajax');
        $user = User::where('username', $username)->firstOrFail();
        $this->authorize('updateFriendship', $user);
        $friend = User::where('username', $friendsname)->firstOrFail();

        switch ($relation){
            case 'remove':
                $user->unfriend($friend);
                $msg = 'Removed';
                break;
            case 'block':
                $user->blockFriend($friend);
                $msg = 'Blocked';
                break;

            case 'unblock':
                $user->unblockFriend($friend);
                $msg = 'Unblocked';
                break;
        }
        if ($ajax){
            return $msg;
        }

        return \Redirect::route('user.profile.friends', $username);
    }

    /**
     * @param $username
     */
    public function getFriendRequests($username)
    {
        $user = User::where('username', $username)->firstOrFail();
        $friendsIds = [];

        $me = '';
        if (Auth::user()){
            $me = Auth::user();
        }

        if ($user){
            $pendingFriendships = $user->getFriendRequests();
            foreach ($pendingFriendships as $friend){
                $friendsIds[] = $friend->sender_id;
            }
        }

        $friends = User::whereIn('id', $friendsIds)->verified()->paginate(self::RECORDS_PER_PAGE);


        /*$partials = [];
       foreach ($friends as $user){
           array_push($partials, view('user.partials.member-card', ['user' => $user, 'username' => $username, 'me' => $me])->render());
          array_push($partials, view('user.profile.partials.friends.pending', [
               'user'     => $user,
               'username' => $username
           ])->render());
       }

       $friends->put('rendered', $partials);*/

        return $friends;
    }


    /**
     * Build a map for the user with his clubs.
     *
     * @return string
     */
    protected function getCurrentUserMap()
    {
        $user = Auth::user();
        $clubs = $user->clubs;

        if ($clubs->count() > 0){
            $map = Mapper::map($clubs[0]->lat, $clubs[0]->lng, ['zoom' => 10, 'clusters' => ['size' => 4, 'center' => true, 'zoom' => 20]]);

            foreach ($clubs as $club){
                Mapper::informationWindow($club->lat, $club->lng, link_to_route('club.detail', $club->name, $club->slug));
            }
        } else{
            // TODO: fallback - user profile location, user geolocation api location
            $map = Mapper::location("{$user->city}, Germany")->map(['zoom' => 10, 'clusters' => ['size' => 10, 'center' => true, 'zoom' => 20]]);
        }

        return Mapper::render();
    }

    /**
     * Display the users public profile
     *
     * @return \Vereinsleben\Http\Controllers\UserController
     */
    public function show(Request $request, $username)
    {
        $this->checkFederalState($request->ip(), $request->user());
        $image = '';
        $user = User::with(array('posts' => function($query){
            $query->orderBy('published_at', 'DESC');
        }, 'posts.images', 'posts.videos'))->where('username', $username)->firstOrFail();
        if (Auth::user()){
            if ($username != Auth::user()->username){
                $this->checkProfileView($request, $user);
            }
            $me = Auth::user();
            $getFriendRequests = $me->getFriendRequests();
            $clubs = $user->clubsWhereAdmin()->get();
        } else{
            $this->checkProfileView($request, $user);
            $me = '';
            $getFriendRequests = '';
            $clubs = '';
        }

        if($getFriendRequests){
            $pendingCnt = $getFriendRequests->count();
        }
        else{
            $pendingCnt = '';
        }

        $posts = $user->posts()->orderBy('published_at', 'DESC')->get();
        $state = State::pluck('name', 'id')->toArray();
        $wizard = session('wizard');
        //session(['wizard' => 0]); //Wurde in Speicherroutine des Bundesland verschoben, damit wizard 1 solange noch kein Bundesland gewählt wurde.

        if (policy($user)->show(Auth::user(), $user)){ // important - since Auth::user() might be null but anonymous users may show, we need to call the policy directly
            return view('user.profile.detail', [
                'user'            => $user,
                'me'              => $me,
                'posts'           => $posts,
                'clubs'           => $clubs,
                'pendingCnt'      => $pendingCnt,
                'image'           => $image,
                'state'           => $state,
                'wizard'          => $wizard,
                'socialLinks'     => $user->socialLinks()->get()->keyBy('link_type'),
                'socialLinkTypes' => SocialLink::$linkTypes,
                'userSports'      => ($user->user_sports()->count() > 0) ? $user->user_sports()->orderBy('interest', 'desc')->get() : collect([]),
                'sportCareers'    => ($user->sport_careers()->count() > 0) ? $user->sport_careers()->orderBy(\DB::raw('-timeline_end'), 'asc')->get() : collect([]),
                'federal_states' => $this->federal_states,
                'sessionState' => $this->sessionState,
                'federalState' => $this->federalState,
            ]);
        } else{
            abort(403);
        }
    }

    /**
     * Update the user resource in storage.
     *
     * @param  \Vereinsleben\Http\Requests\UserProfileFormRequest $request
     * @param  string $username
     * @return \Illuminate\Http\Response
     */
    public function updatePatch(UserProfileFormRequest $request, $username)
    {

        $user = User::where('username', $username)->firstOrFail();

        try{
            $this->authorize('update', $user);

        } catch (AuthorizationException $e){

        }

        $this->cropProfileImagesFromRequest($request);

        $fillableAndPresentInRequest = collect($user->getFillable())->filter(function($item) use ($request){
            return $request->input($item, null) !== null || $request->hasFile($item);
        })->toArray();

        $fillableRequestData = $request->only($fillableAndPresentInRequest);

        $user->update($fillableRequestData);

        return \Redirect::route('user.detail', $user->username)->with('flash-success', 'Deine Änderungen wurden gespeichert!');
    }

    /**
     * Check the request and crop the avatar/header images when the required data is present.
     *
     * Because the upload handling and scaling is handled by Stapler, we crop the original image in temporary
     * storage here before Stapler does its magic and does the rest.
     *
     * @return void
     */
    protected function cropProfileImagesFromRequest(Request $request)
    {
        if ($request->input('crop-props', false) !== false){
            $cropProps = json_decode($request->input('crop-props'), true);
            if ($cropProps !== null){
                foreach (['avatar', 'header'] as $type){
                    if ($request->hasFile($type) && $cropProps !== null && (float)$cropProps['scale'] > 0){
                        $image = Image::make($request->file($type)->getRealPath())->rotate((float)$cropProps['angle']);
                        #$image->widen(floor((int)$image->width() * (float)$cropProps['scale']));
                        #$image->crop((int)$cropProps['w'], (int)$cropProps['h'], (int)$cropProps['x'], (int)$cropProps['y']);
                        $image->crop( # keep the image in the original size, calculate the given params up with the given scale factor.
                            floor((int)$cropProps['w'] / (float)$cropProps['scale']),
                            floor((int)$cropProps['h'] / (float)$cropProps['scale']),
                            floor((int)$cropProps['x'] / (float)$cropProps['scale']),
                            floor((int)$cropProps['y'] / (float)$cropProps['scale'])
                        );
                        $image->save($request->file($type)->getRealPath());
                    }
                }
            }
        }
    }

    /**
     * returns latest users, rendered as partials for further js usage
     *
     * @return mixed
     */
    public function latest(Request $request)
    {
        $user = new User;

        $users = $user->verified()->latestProfileCompleted()->paginate(3);

        $partials = [];

        foreach ($users->shuffle() as $user){
            if (policy($user)->show(Auth::user(), $user)){
                array_push($partials, view('user.partials.member-card', ['user' => $user, 'inverted' => true])->render());
            }
        }

        $users->put('rendered', $partials);

        return $users;
    }

    /**
     * return the current user
     *
     * @param Request $request
     * @return mixed
     */
    public function me(Request $request)
    {
        $userId = $request->user()->id;
        $cacheKey = 'user.me:' . $userId;

        if (Cache::has($cacheKey)){
            $user = Cache::get($cacheKey);
        } else{
            $expiresAt = Carbon::now()->addMinutes(self::CACHE_LIFETIME_MINUTES);

            $user = User::whereId($userId)->with([
                'clubs' => function($club){
                    $role = Role::where('constant', 'CLUB_OWNER')->firstOrFail();
                    $club->wherePivot('role_id', $role->id);
                }
            ])->get();

            Cache::put($cacheKey, $user, $expiresAt);
        }

        return response()->json($user);
    }


    /**
     * returns users (^= user freinds), rendered as partials for further js usage
     *
     * @param Request $request
     * @return mixed
     * @throws \Throwable
     */
    public function friendList(Request $request)
    {
        $partials = [];
        $userIds = [];
        $me = '';
        $username = '';

        $profile = User::findOrFail($request->input('user_id'));
        $searchString = $request->input('searchString');

        if (Auth::user()){
            $me = Auth::user();
            $username = $me->username;
            if ($me->id == $profile->id){
                $friends = $profile->getFriendRequests();
                foreach ($friends as $friend){
                    $userIds[] = $friend->sender_id;
                }
            }
        }

        $users = User::whereIn('id', $userIds)->get();

        if ($searchString !== null & $searchString != ""){

            $users = $users->merge($profile->getFriends());
            $users = $users->filter(function($item) use ($searchString){
                return ((stripos($item->username, $searchString) !== false) ||
                    (stripos($item->firstname, $searchString) !== false) ||
                    (stripos($item->lastname, $searchString) !== false)
                );
            });
            $users = $this->custom_paginate($users, self::RECORDS_PER_PAGE);

        } else{
            $users = $users->merge($profile->getFriends());
            $users = $this->custom_paginate($users, self::RECORDS_PER_PAGE);
        }

        foreach ($users as $user){
            if (policy($user)->show(Auth::user(), $user)){
                array_push($partials, view('user.partials.member-card', ['user' => $user, 'username' => $username, 'me' => $me, 'profile' => $profile])->render());
            }
        }

        $users->appends([
            'user_id'      => $request->input('user_id'),
            'searchString' => $request->input('searchString'),
        ]);

        array_push($partials, view('vendor.pagination.paginate', ['links' => $users->links()])->render());

        $users->put('rendered', $partials);

        return $users;
    }

    /**
     * returns clubs (^= user clubs), rendered as partials for further js usage
     *
     * @param Request $request
     * @return mixed
     * @throws \Throwable
     */
    public function vereineList(Request $request)
    {
        $partials = [];

        $user = User::findOrFail($request->input('user_id'));
        $searchString = $request->input('searchString');

        $clubs = $user->clubsWhereAll()
                      ->where('club.name', 'LIKE', '%' . $searchString . '%')
                      ->paginate(self::RECORDS_PER_PAGE);

        foreach ($clubs as $club){
            //if (policy($club)->show(Auth::club(), $club)){
            array_push($partials, view('club.partials.club-card', ['club' => $club])->render());
            //}
        }

        $clubs->appends([
            'user_id'      => $request->input('user_id'),
            'searchString' => $request->input('searchString'),
        ]);

        array_push($partials, view('vendor.pagination.paginate', ['links' => $clubs->links()])->render());

        $clubs->put('rendered', $partials);

        return $clubs;
    }


    /**
     * returns bandages (^= user bandages), rendered as partials for further js usage
     *
     * @param Request $request
     * @return mixed
     * @throws \Throwable
     */
    public function bandageList(Request $request)
    {
        $partials = [];

        $role = new Role;
        $user = User::findOrFail($request->input('user_id'));
        $searchString = $request->input('searchString');

        $bandages = $user->bandages()->wherePivot('role_id', $role->getByConstant(Role::BANDAGE_SUBSCRIBE)->id)
                         ->where('bandages.name', 'LIKE', '%' . $searchString . '%')
                         ->paginate(self::RECORDS_PER_PAGE);

        foreach ($bandages as $bandage){
            //if (policy($bandage)->show(Auth::bandage(), $bandage)){
            array_push($partials, view('bandage.partials.bandage-card', ['bandage' => $bandage])->render());
            //}
        }

        $bandages->appends([
            'user_id'      => $request->input('user_id'),
            'searchString' => $request->input('searchString'),
        ]);

        array_push($partials, view('vendor.pagination.paginate', ['links' => $bandages->links()])->render());

        $bandages->put('rendered', $partials);

        return $bandages;
    }

    public function changeFederalState($stateId)
    {
        $user = Auth::user();
        if ($user === null){
            session(['federalState' => $stateId]);
        } else{
            $user->states()->detach();
            $state = State::find($stateId);
            $user->states()->save($state);
        }

        return redirect()->back();
    }

    //13.03.2018: User soll die Möglichkeit haben sein Profil zu löschen
    /**
     * protected function delete()
     * {
     * $user = User::find(Auth::user()->id);
     *
     * //Auth::logout();
     * $user->delete();
     * Post::where('user_id', $user)->delete(); //to delete all posts from that user
     * //Post::whereUserId($user)->forceDelete();
     * SportCareer::where('user_id', $user)->delete();
     * //SportCareer::whereUserId($user)->forceDelete();
     * DB::table('user_team')->where('user_id', '=', $user)->delete();
     *
     * return Redirect::route('home')->with('flash-info', 'Dein Konto wurde erfolgreich gelöscht!');
     * //return Redirect::route('home')->with('flash-info', 'Dein Konto wurde erfolgreich gelöscht!');
     * }
     */

    //21.03.2018: User request to delete his account and gets confirmation mail with link
    public function ReqDelete()
    {
        if (Auth::check()){

            $user = User::find(Auth::user()->id);
            $hashy = \Hash::make(rand(0, time()));
            $now = Carbon::today();

            DB::table('delusers')->insert(
                ['user_id' => $user->id, 'deleted_token' => $hashy, 'email' => $user->email, 'username' => $user->username, 'federal_state' => $user->federal_state, 'created_at' => $now]
            );

            $imprint = Content::select('content')->where('slug', 'imprint')->first();

            Mail::send('/user/account-delete-confirmation', ['user' => $user, 'deleted_token' => $hashy, 'imprint' => $imprint], function($message) use ($user, $hashy){
                $message->from('noreply@vereinsleben.de', 'noreply')->to($user->email, $user->username)->subject('vereinsleben.de - Konto löschen bestätigen');
            });

            Auth::logout();

            return Redirect::route('home')->with('flash-info', 'Sie haben eine E-Mail mit einem Bestätigungslink erhalten. Bitte klicken Sie den Link in dieser E-Mail an, um das Löschen ihres Kontos zu bestätigen.');

        }

        return Redirect::route('home')->with('flash-info', 'Sie sind nicht eingeloggt.');
    }

    //22.03.2018: after clicking on link in Email, user is soft deleted and admin gets email with relevant data
    public function deletion(Request $request, $token)
    {
        if (!$token){
            //$request->session()->flash('flash-warning', 'Der Link war fehlerhaft. Bitte kopieren Sie den Link vollständig aus Ihrer E-Mail.');
            return Redirect::route('home')->with('flash-warning', 'Der Link war fehlerhaft. Bitte kopieren Sie den Link vollständig aus Ihrer E-Mail.');
            //return redirect('/login');
        }

        $user = DB::table('delusers')->select('user_id', 'username', 'email')->where('deleted_token', 'LIKE', '%' . $token)->first();

        $usernew = User::find($user->user_id);

        if (!$user){

            return Redirect::route('home')->with('flash-warning', 'Der Link war fehlerhaft. Bitte kopieren Sie den Link vollständig aus Ihrer E-Mail.');

        }

        $usernew->delete();

        $imprint = Content::select('content')->where('slug', 'imprint')->first();

        Mail::send('/user/account-delete-request', ['user' => $user, 'imprint' => $imprint], function($message) use ($user){
            $message->from('noreply@vereinsleben.de', 'noreply')->to(explode(';', env('MAIL_RECIPIENT_DEFAULT')))->subject('Benutzerkonto löschen');
        });

        //$request->session()->flash('flash-info', 'Ihre Anfrage wurde erfolgreich versendet. Ihr Konto wird innerhalb von 48 Stunden gelöscht.');
        return Redirect::route('home')->with('flash-warning', 'Ihre Anfrage wurde erfolgreich versendet. Ihr Konto wird innerhalb von 48 Stunden gelöscht.');
        //return redirect('/login');

    }

    public function updateSocial(Request $request, $username)
    {

        $user = User::where('username', $username)->firstOrFail();
        $socialLinks = $request['social_links'];

        foreach ($socialLinks as $type => $url){
            if (in_array($type, array_keys(SocialLink::$linkTypes))){
                if (trim($url) != ''){
                    $link = $user->socialLinks()->where('link_type', $type)->first();
                    if (is_null($link)){

                        $link = new SocialLink();
                        $link->link_type = $type;
                    }

                    $link->url = $url;

                    if (!preg_match('/^https?:\/\//', $url)){
                        $link->url = "http://" . $link->url;
                    }
                    $user->socialLinks()->save($link);
                } else{
                    $link = $user->socialLinks()->where('link_type', $type)->first();
                    if (!is_null($link)){

                        $link->delete();
                    }
                }
            }
        }

        return \Redirect::route('user.detail', $user->username)->with('flash-success', 'Dein Social-Linds sind erfolgreich gespeichrt!');
    }

    /**
     * Display club wizard
     *
     * @param \Illuminate\Http\Request
     * @return \Vereinsleben\Http\Controllers\UserController
     */
    public function clubwizard()
    {
        if (Auth::user()){
            $user = Auth::user();
            $sports = Sport::all()->sortBy('title')->pluck('title', 'id')->toArray();

            return view('user.profile.modal.club-wizard', ['me' => $user, 'sports' => $sports]);
        }
    }

    public function clubwizardContest()
    {
        if (Auth::user()){
            $user = Auth::user();
            $sports = Sport::all()->sortBy('title')->pluck('title', 'id')->toArray();

            return view('user.profile.modal.club-wizard-contest', ['me' => $user, 'sports' => $sports]);
        }
    }



}