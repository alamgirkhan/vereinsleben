<?php

namespace Vereinsleben\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Vereinsleben\Http\Requests;
use Vereinsleben\Http\Requests\UserPostRequest;
use Vereinsleben\Post;
use Vereinsleben\Image;
use Vereinsleben\Video;
use Vereinsleben\User;
use Illuminate\Support\Facades\Input;

class UserPostController extends BaseController
{
    public function __construct(){
        parent::__construct();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserPostRequest $request, $username = null)
    {
        $response = [
            'success' => false,
            'data' => [],
            'error' => null,
        ];

        try {
//            $user = User::where('username', $username)->firstOrFail();
            $user = $request->user();
            $post = new Post;
            $video = new Video;

            // TODO: UserPostFormRequest for validation!

            $post->title = null;
            $post->content = $request['content'];
            $post->content_raw = $request['content'];
            $post->user_id = $user->id;
            $post->state = Post::STATE_NEW;
            $post->published_at = date("Y-m-d H:i:s");

            $this->authorize($post);

            $post->save();

            $images = $request->file('images');
            if ($images !== null && count($images) > 0) {
                foreach ($images as $imageUploaded) {
                    if ($imageUploaded !== null) {
                        $image = new Image(['picture' => $imageUploaded]);
                        $post->images()->save($image);
                    }
                }
            }

            $videoUrl = $request->input('video_youtube', null);
            $videoData = $video->extractData($videoUrl);

            if(count($videoData) > 0) {
                $post->videos()->save(new Video($videoData));
            }

            $response['success'] = true;
            $response['data'] = $post->toArray();
            $response['href'] = route('user.post.template', [$user->username, $post->id]);

        } catch (\Exception $ex) {
            print_r(json_encode($ex));
            $response['success'] = false;
            $response['error'] = $ex->getMessage();
        }

        return response()->json($response);
    }

    /**
     * Display the rendered resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function showRendered($username, $id)
    {
        $response = [
            'success' => false,
            'template' => null,
            'error' => null,
        ];

        try {
            $user = User::where('username', $username)->firstOrFail();
            $post = $user->posts()->findOrFail($id);
            $this->authorize('show', $post);
            $response['success'] = true;
            $response['template'] = view('user.profile.partials.post.single', [
                'post' => $post,
                'user' => $user
            ])->render();
        } catch (\Exception $ex) {
            $response['success'] = false;
            $response['error'] = $ex->getMessage();
        }

        return response()->json($response);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($username, $id)
    {
        $response = [
            'success' => false,
            'template' => null,
            'error' => null,
        ];

        try {
            $user = User::where('username', $username)->firstOrFail();
            $post = $user->posts()->findOrFail($id);
            $this->authorize($post);
            $response['success'] = true;
            $response['template'] = view('user.profile.partials.post.edit.form', [
                'user' => $user,
                'post' => $post,
            ])->render();

        } catch (\Exception $ex) {
            $response['success'] = false;
            $response['error'] = $ex->getMessage();
        }

        return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $username, $id)
    {
        $response = [
            'success' => false,
            'data' => [],
            'error' => null,
        ];

        try {
            $user = User::where('username', $username)->firstOrFail();
            $post = $user->posts()->findOrFail($id);
            $this->authorize($post);

            // TODO: UserPostFormRequest for validation!

            $post->content = $request['content'];
            $post->content_raw = $request['content'];

            $post->save();

            // delete image
            $imagesDelete = $request->input('delete-image');
            if ($imagesDelete !== null && count($imagesDelete) > 0) {
                foreach ($imagesDelete as $imageDeleteId) {
                    if (is_numeric($imageDeleteId)) {
                        $image = $post->images()->find($imageDeleteId);
                        if ($image !== null) {
                            $image->delete();
                        }
                    }
                }
            }

            // add image
            $images = $request->file('images');
            if ($images !== null && count($images) > 0) {
                foreach ($images as $imageUploaded) {
                    if ($imageUploaded !== null) {
                        $image = new Image(['picture' => $imageUploaded]);
                        $post->images()->save($image);
                    }
                }
            }

            $videoUrl = $request->input('video_youtube', null);
            if ($videoUrl !== null && trim($videoUrl) !== '') {
                // delete existing video first
                // TODO: should we check if the URL of the first video is identical and skip the rest?
                if ($post->videos()->count() > 0) {
                    $post->videos()->delete();
                }

                $patterns = [
                    'youtube' => [
                        '#(?:<\>]+href=\")?(?:http(s)?://)?((?:[a-zA-Z]{1,4}\.)?youtube.com/(?:watch)?\?v=(.{11}?))[^"]*(?:\"[^\<\>]*>)?([^\<\>]*)(?:)?#',
                        '%(?:youtube\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i',
                    ]
                ];
                foreach ($patterns['youtube'] as $index => $pattern) {
                    if (preg_match($pattern, $videoUrl, $match) != false) {
                        if ($index === 0 && isset($match[3])) {
                            $videoId = $match[3];
                        }
                        if ($index === 1 && empty($videoId)) {
                            $videoId = $match[1];
                        }

                        if (!empty($videoId)) {
                            $video = new Video(['type' => 'youtube', 'identifier' => $videoId, 'url' => 'https://www.youtube.com/watch?v='.$videoId]);
                            $post->videos()->save($video);
                        }
                        break;
                    }
                }
            } else {
                if ($post->videos()->count() > 0) {
                    $post->videos()->delete();
                }
            }

            $response['success'] = true;
            $response['data'] = $post->toArray();
            $response['href'] = route('user.post.template', [$user->username, $post->id]);

        } catch (\Exception $ex) {
            $response['success'] = false;
            $response['error'] = $ex->getMessage();
        }

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($username, $id)
    {
        $response = [
            'success' => false,
            'data' => [],
            'error' => null,
        ];

        try {
            $user = User::where('username', $username)->firstOrFail();
            $post = $user->posts()->findOrFail($id);
            $this->authorize($post);

            $response['success'] = true;
            $response['data'] = $post->get()->toArray();
            $post->delete();

        } catch (\Exception $ex) {
            $response['success'] = false;
            $response['error'] = $ex->getMessage();
        }

        return response()->json($response);
    }
}
