<?php

namespace Vereinsleben\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Vereinsleben\Http\Requests;
use Vereinsleben\User;
use Vereinsleben\UserSport;

class UserSportController extends BaseController
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $this->authorize('update', $user);

        $response = [
            'success' => false,
            'data' => [],
            'error' => null,
        ];

        try {
            if (!$request->isXmlHttpRequest()) {
                abort(403);
            }

            $interestData = $request->input('interestData');

            // remove existing datasets
            UserSport::where('user_id', $user->id)->delete();

            // create new datasets from users input
            foreach ($interestData as $key => $item) {
                $userSport = new UserSport;
                $userSport->sport_id = $item['sport'];
                $userSport->user_id = $user->id;
                $userSport->interest = $item['interest'];
                $userSport->save();
                array_push($response['data'], $userSport->toArray());
            }
        } catch (\Exception $ex) {
            $response['success'] = false;
            $response['error'] = $ex->getMessage();
        }

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $userId
     * @return \Illuminate\Http\Response
     */
    public function show($userId)
    {
        $userSport = new UserSport;

        return $userSport->where('user_id', $userId)->get();
    }

    /**
     * Return the specified resource as rendered template.
     *
     * @param  int $userId
     * @return \Illuminate\Http\Response
     */
    public function showRendered($userId)
    {
        $userSports = new UserSport;
        $userSports = $userSports->where('user_id', $userId)->with('sport')->get();

        return response()->view('user.profile.partials.sport-interest', [
            'userSports' => $userSports
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $userId
     * @return \Illuminate\Http\Response
     */
    public function edit($userId)
    {
        $user = User::findOrFail($userId);
        $this->authorize('edit', $user);

        try {
            $userSports = new UserSport;
            $userSports = $userSports->where('user_id', $userId)->with('sport')->get();

            $partials = [];

            foreach ($userSports as $userSport) {
                array_push($partials, view('user.profile.partials.edit.sport-interest', [
                    'userSport' => $userSport
                ])->render());
            }

            return response()->json($partials);
        } catch (\Exception $ex) {
            return response()->json($ex->getMessage());
        }


    }

    /**
     * Remove a set of resources from storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $response = [
            'success' => false,
            'deleted' => [],
            'error' => null
        ];

        try {
            $ids = $request->input('deleteIds');

            if (!is_array($ids)) {
                $ids = [$ids];
            }

            foreach ($ids as $id) {
                $id = (int)$id;
                array_push($response['deleted'], $id);
                $userSport = UserSport::findOrFail($id);
                $this->authorize('update', $userSport->user);
                $userSport->delete();
            }
            $response['success'] = true;
        } catch (\Exception $ex) {
            $response['success'] = false;
            $response['error'] = $ex->getMessage();
        }

        return response()->json($response);
    }


}
