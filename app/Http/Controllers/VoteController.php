<?php

namespace Vereinsleben\Http\Controllers;

use DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use Redirect;
use Vereinsleben\Application;
use Vereinsleben\Content;
use Vereinsleben\Http\Requests;
use Vereinsleben\Http\Requests\ApplicationRequest;
use Vereinsleben\Http\Requests\VoteParticipateRequest;
use Vereinsleben\Image;
use Vereinsleben\User;
use Vereinsleben\Vote;
use Vereinsleben\VoteNominee;
use Vereinsleben\Club;
use Vereinsleben\Mail\VoteApplication;
use Vereinsleben\VoteUser;
use Vereinsleben\State;

class VoteController extends BaseController
{
    public function __construct(Request $request){
        parent::__construct($request);
    }

    public function overview()
    {
        $vote = new Vote;
        $rankings = collect([]);
        //$ranked = collect([]);
        $hasVoted = null;

        $dt = Carbon::now()->format('Y-m-d');
        $dt_first_day_of_year = Carbon::now()->format('Y');
        $dt_first_day_of_year.= "-01-01";
	      $last_day_of_previous_month = date("Y-n-j", strtotime("last day of previous month"));
	      
        $current = $vote
            ->where('period_start', '<=', $dt)
            ->where('period_end', '>=', $dt)
            /*->with(['voteNominees' => function ($query) {
                $query->with(['users', 'voteable']);
                $query->withCount('users');
            }])*/
            ->latest()
            ->first();


        $notActive = $vote
            ->where('period_start', '>=', $dt)
            ->latest()
            ->first();

        if ($current instanceof Vote){
            /**
             * @TODO Issue: VdM page by reload exhausts php memory limit
             */
            /*$previous = $vote
                ->where('id', '<', $current->id)
                ->latest()
                ->limit(1)
                ->with(['voteNominees' => function ($query) {
                    $query->with(['users', 'voteable']);
                    $query->withCount('users');
                }])
                ->first();*/

            $previous = $vote
                ->where('id', '<', $current->id)
                ->latest()
                ->first();

            /*
            $current->voteNominees->each(function ($nominee) use ($rankings) {
                $nominees = $nominee->users->filter(function($user) use ($nominee) {
                    return $user->has_fake_email != 1;
                });

                $rankings->push($nominee);
            });

            $rankings->sortByDesc('users_count')->take(10)->each(function ($rank) use ($ranked) {
                $ranked->push($rank);
            });
            */


            // new
            $current->voteNominees->each(function($nominee) use ($rankings){
                /** @var \Illuminate\Support\Collection $validUsers */
                //$validUsers = $nominee->users->filter(function($user) use ($nominee) {
                //return true;
                //return $user->has_fake_email != 1;
                //});

                //$validUserCountSortable  = $validUsers->count() * 10;
                //$nominee->validUserCount = $validUsers->count();  // TODO: In model unknown, and dynamic (means non-db).

                // Attention: Only valid for up to 10 nominees with same vote count.
                //while( isset($rankings[$validUserCountSortable]) ) {
                //$validUserCountSortable++;
                //}
                $rankings->offsetSet($nominee->count_cleared, $nominee);
            });

            // Sorted by valid user votes.
            // ATTENTION: Relation "users" will get any users - even if not counted due to its fakeness.
            $rankings = $rankings
                ->sortByDesc('count_cleared')
                ->values();
        } else {
            $previous = Vote::where('period_end', '<=', $dt)
                ->where('period_start', '>=', $dt_first_day_of_year)
                ->latest()
                ->first();
        }
        $previous_winners = Vote::with(['voteNominees' => function($query) {
          $query->with(['club'])->where('winner', 1);
        }])->whereDate('period_end', '<=', $last_day_of_previous_month)
          ->whereDate('period_start', '>=', $dt_first_day_of_year)->get();
        // most repetitive! ...
        // @todo extract this
        $now = Carbon::now()->format('Y-m-d');


        $hasVoted = DB::table('user_vote_nominee')
                      ->where('user_id', Auth::id())
                      ->where('created_at', $now)
                      ->where('updated_at', $now)
                      ->first();

        $state = State::pluck('name', 'id')->toArray();
        if (Auth::user()){
            $me = Auth::user();
        } else{
            $me = '';
        }
        return view('vote.index', [
            'thisIsVotingPage' => '1',
            'state'     => $state,
            'me'        => $me,
            'vote'      => $current,
            'notActive' => $notActive,
            'previous'  => $previous,
            'rankings'  => $rankings,
            'previous_winners'  => $previous_winners,
            'hasVoted'  => $hasVoted,
            'remember'  => $this->getRememberEmai($current),
            'month'     => array(
                1  => 'Januar',
                2  => 'Februar',
                3  => 'März',
                4  => 'April',
                5  => 'Mai',
                6  => 'Juni',
                7  => 'Juli',
                8  => 'August',
                9  => 'September',
                10 => 'Oktober',
                11 => 'November',
                12 => 'Dezember'
            )
        ]);
    }


    public function about(){
        return view('vote.about');
    }

    public function teilnehmen(){
        return view('vote.participate');
    }

    public function teilnahmebedingungen(){
        return view('vote.agreement');
    }

    public function voteFor(Request $request, $voteId, $nomineeId)
    {
        $vote = new Vote;
        $user = $request->user();
        $current = $vote->isCurrent(intval($voteId)) ? Vote::find(intval($voteId)) : false;

        $now = Carbon::now()->format('Y-m-d');

        $hasVoted = DB::table('user_vote_nominee')
                      ->where('user_id', Auth::id())
                      ->where('created_at','like', $now . '%')
                      ->where('updated_at', 'like', $now. '%')
                      ->first();

        if ($current && !$hasVoted){
            $nominee = $current->voteNominees()->find($nomineeId);
            $nominee->users()->save($user, ['created_at' => $now,
                                            'updated_at' => $now,
            ]);
            $voteUser = VoteUser::where('vote_id', $voteId)->where('user_id', $user->id)->first();
            if ($voteUser == null){
                $voteUser = new VoteUser();
                $voteUser->user_id = $user->id;
                $voteUser->vote_id = $voteId;
            }
            $voteUser->reminder = 0;
            $voteUser->save();
        }
    }

    public function nominee($slug)
    {
        $club = Club::published()->where('slug', $slug)->firstOrFail();
        $nominee = VoteNominee::where('voteable_id', $club->id)
                              ->where('voteable_type', 'Club')
                              ->firstOrFail();
        $now = Carbon::now()->format('Y-m-d');

        if (Auth::check()){
            $hasVoted = DB::table('user_vote_nominee')
                          ->where('user_id', '=', Auth::id())
                          ->where('created_at','like', $now . '%')
                            ->where('updated_at', 'like', $now. '%')
                            ->first();
        } else{
            $hasVoted = false;
        }
        $current = Vote::where('period_start', '<=', $now)
                       ->where('period_end', '>=', $now)
                       ->latest()
                       ->first();

        $state = State::pluck('name', 'id')->toArray();
        if (Auth::user()){
            $me = Auth::user();
        } else{
            $me = '';
        }

        return view('vote.detail', [
            'thisIsVotingPage' => '1',
            'nominee'  => $nominee,
            'state'    => $state,
            'me'       => $me,
            'club'     => $club,
            'hasVoted' => $hasVoted,
            'remember' => $this->getRememberEmai($current),
        ]);
    }

    public function bewerben(ApplicationRequest $request)
    {

        //$club = Club::where('slug', 'bujinkan-senryaku-dojo-koblenz-e-v')->firstOrFail();
//        dd($club);
        dd($request->all());

        if ($club->application() === null){
            $user = Auth::user();
            $application = new Application();
            $application->user_id = $user->id;
            $application->club_id = $club->id;
            $application->title = $request->get('title');
            $application->description = $request->get('description');
            $application->start_date = $request->get('start_date');
            $application->end_date = $request->get('end_date');
            $application->logo = $request->get('logo');
            $application->description_image = $request->get('projectDescription');
            $application->project_image1 = $request->get('projectImages')[0];

            $club->application()->save($application);

            return \Redirect::route('vote.index')->with('flash-success', 'Ihre Bewerbung wurde erfolgreich geschickt!');
        }

        return \Redirect::route('vote.index')->with('flash-info', 'Für die Verein ist schon eine Bewerbung angelegt!');
    }

    public function participate(VoteParticipateRequest $request)
    {
        $data = $request->input();
        $projectImages = $request->file('projectImages');
        $logo = $request->file('logo');
        $clubImage = $request->file('clubImage');
        $projectDescription = $request->file('projectDescription');


        foreach ($data as $key => $item){
            if (!is_array($item) && !is_object($item)){
                $data[$key] = json_decode(json_decode($item, true), true);
            }
        }

        if ($projectImages !== null && count($projectImages) > 0){
            $data['projectImages'] = [];
            foreach ($projectImages as $key => $img){
                if ($img !== null){
                    try{
                        $image = new Image(['picture' => $img]);

                        $image->save();
                    } catch (\Exception $e){
                        Log::error($e->getMessage(), array('img' => $img, 'club' => $data['club']));
                        continue;
                    }

                    array_push($data['projectImages'], $image->picture->url('detail'));
                }
            }
        }

        if (isset($logo)){
            $img = new Image(['picture' => $logo]);
            $img->save();

            $data['logo'] = $img->picture->url('detail');
        }

        if (isset($clubImage)){
            $img = new Image(['picture' => $clubImage]);
            $img->save();

            $data['clubImage'] = $img->picture->url('detail');
        }

        if (isset($projectDescription)){
            $description = $projectDescription->move(storage_path('app'), hash('sha256', $projectDescription->getClientOriginalName() . time()) . '.' . $projectDescription->getClientOriginalExtension());

            $data['projectDescription'] = $description->getRealPath();
        }

        $imprint = Content::select('content')->where('slug', 'imprint')->first();
        $data['imprint'] = $imprint->content;
        VoteApplication::send($data);

        return response()->json(['status' => 'success']);
    }

    public function remember(Request $request)
    {
        $dt = Carbon::now()->format('Y-m-d');
        $vote = Vote::where('period_start', '<=', $dt)
                    ->where('period_end', '>=', $dt)
                    ->latest()
                    ->first();

        $voteUser = VoteUser::where('vote_id', $vote->id)
                            ->where('user_id', Auth()->id())
                            ->first();
        if ($voteUser != null){
            $voteUser->reminder = 1;
            $voteUser->updated_at = $dt;
            $voteUser->save();
        } else{
            $voteUser = new VoteUser();
            $voteUser->user_id = Auth::id();
            $voteUser->vote_id = $vote->id;
            $voteUser->reminder = 1;
            $voteUser->save();
        }
    }

    public function notRemember()
    {
        $user = User::find(Auth::id())->firstOrFail();
        $this->doNotRemember($user->id);

    }

    public function notRememberAnymore($token)
    {

        $userVote = VoteUser::where('deleted_token', $token)->first();

        if ($userVote === null or $this->doNotRemember($userVote->user_id) == null){
            return Redirect::route('vote.index')->with('flash-warning', 'Dein Link ist nicht vollstandig koppiert oder veraltet!');
        }

        return Redirect::route('vote.index')->with('flash-success', 'Die Erinnerungsfunktion wurde erfolgreich deaktiviert!');
    }

    private function doNotRemember($userId)
    {
        $dt = Carbon::now()->format('Y-m-d');
        $vote = Vote::where('period_start', '<=', $dt)
                    ->where('period_end', '>=', $dt)
                    ->latest()
                    ->first();
        if ($vote === null)
            return null;
        $voteUser = VoteUser::where('vote_id', $vote->id)
                            ->where('user_id', $userId)
                            ->first();
        if ($voteUser != null){
            $voteUser->reminder = 0;
            $voteUser->updated_at = $dt;
            $voteUser->update();
        } else{
            $voteUser = new VoteUser();
            $voteUser->user_id = Auth::id();
            $voteUser->vote_id = $vote->id;
            $voteUser->reminder = 0;
            $voteUser->save();
        }

        return $voteUser;
    }

    /**
     * @param $vote
     * @return int
     */
    public function getRememberEmai($vote)
    {
        if ($vote != null){
            $fiveDaysAgo = Carbon::now()->subDays(5);
            $voteUser = VoteUser::where('vote_id', $vote->id)
                                ->where('user_id', Auth::id())
                                ->first();
            if ($voteUser == null){
                $rememberEmail = 1;
            } else{
                if ($voteUser->reminder == 0 && $voteUser->updated_at < $fiveDaysAgo){
                    $rememberEmail = 1;
                } else{
                    $rememberEmail = 0;
                }
            }

            return $rememberEmail;
        }

        return 0;
    }


    public function showTeilnehmen(){
        return view('vote.participate');
    }

    public function showTeilnahmebedingungen(){
        return view('vote.agreement');
    }

    public function showUeberDenWettbewerb(){
        return view('vote.about');
    }

    public function showSponsorFortuna(){
        return view('vote.sponsor-fortuna');
    }

    public function showSponsorLSB(){
        return view('vote.sponsor-lsb');
    }

    public function showSponsorSparda(){
        return view('vote.sponsor-sparda');
    }

    public function showSponsorRPR1(){
        return view('vote.sponsor-rpr1');
    }
    
    public function showSponsorBigfm(){
        return view('vote.bigfm');
    }

    public function showSponsorABC(){
        return url("partner/abc-europe-werbeagentur");
    }


}
