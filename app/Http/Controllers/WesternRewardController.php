<?php

namespace Vereinsleben\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Vereinsleben\WesternRewards;
use function GuzzleHttp\Promise\all;

class WesternRewardController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    return view('western-reward.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$western_rewards = new WesternRewards;
	    if (Auth::check()){
		    $user = Auth::user();
		    
		    $western_rewards->user_id = $user->id;
		    $western_rewards->username = $user->username;
		    $western_rewards->fullname = $user->fullname;
		    $western_rewards->firstname = $user->firstname;
		    $western_rewards->lastname = $user->lastname;
		    
		    return json_encode(['status' => $western_rewards->save()]);
	    }else
        {
            return json_encode(['status' => FALSE]);
        }
    }

    public function list() {
	    $western_rewards = WesternRewards::all();
	    return view('admin.western-rewards.list', ['western_rewards' => $western_rewards]);
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
