<?php

namespace Vereinsleben\Http\Middleware;

use Closure;

class ContentManager
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->user() || (!$request->user()->isContentManager() && !$request->user()->isAdmin())) {
            return redirect('/');
        }
        return $next($request);
    }
}
