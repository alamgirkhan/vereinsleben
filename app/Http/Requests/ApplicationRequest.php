<?php

namespace Vereinsleben\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ApplicationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $endDate = $this->get('end_date');

        return [
            //            'club_id' => 'required|integer',
            'title'       => 'required|string',
            'description' => 'required|string|min:10',
            'start_date'  => 'required|date|before:end_date',
            'end_date'    => 'required|date',
        ];
    }
}
