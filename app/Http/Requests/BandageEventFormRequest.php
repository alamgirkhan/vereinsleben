<?php

namespace Vereinsleben\Http\Requests;

use Vereinsleben\Http\Requests\Request;

class BandageEventFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title'          => 'required|string|max:128',
            'content_raw'    => 'required|string',
            'bandage_id'     => 'required|numeric',
            'street'         => 'required|string',
            'house_number'   => 'string|max:5',
            'zip'            => 'required|string|max:5',
            'city'           => 'required|string|max:128',
            'schedule_begin' => 'required|date_format:d.m.Y H:i',
            'schedule_end'   => 'required|date_format:d.m.Y H:i',
            'published_from' => 'date_format:d.m.Y H:i',
            'published_to'   => 'date_format:d.m.Y H:i',
        ];

//        $nbr = count($this->input('images')) - 1;
        $nbr = is_array($this->input('images')) ? count($this->input('images')) - 1 : 0;

        foreach (range(0, $nbr) as $index){
            $rules['image.' . $index] = 'file|image|mimes:jpeg,jpg,png,gif|max:4096'; # image, max. 4MB
        }

        return $rules;
    }
}
