<?php

namespace Vereinsleben\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BandageFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'         => 'required|string|min:3',
            'email'        => 'email',
            'founded'      => 'numeric|digits:4|before:tomorrow|date_format:Y',
            'street'       => 'required|string|min:2',
            'house_number' => 'string|min:1',
            'zip'          => 'numeric|min:5',
            'city'         => 'required|string|min:3',
            'social_links' => 'array',
            'type'         => 'required|string|min:3',
            'region'       => 'required|string|min:3',
            'header'       => 'file|image|mimes:jpeg,jpg,png,gif|max:4096', # image, max. 4MB
            'avatar'       => 'file|image|mimes:jpeg,jpg,png,gif|max:4096', # image, max. 4MB
        ];

        switch ($this->method()){
            case 'PATCH': # remove the required since the PATCH might just not contain this key
                # rules as above, but without required
                $optionalPatchRules = [
                    'name'   => 'string|min:3',
                    'street' => 'string|min:2',
                    'city'   => 'string|min:3',
                    'sports' => 'array',
                    'type'   => 'string|min:3',
                    'region' => 'string|min:3',
                ];

                foreach ($optionalPatchRules as $key => $rule){
                    if ($this->input($key, null) === null){
                        $rules[$key] = $rule; # if the request does not contain the key
                    }
                }
                break;
        }

        return $rules;
    }
}
