<?php

namespace Vereinsleben\Http\Requests;

use Vereinsleben\Http\Requests\Request;

class ClubAchievementRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required|string|min:3|max:255',
            'description' => 'string|min:2|max:1000',
            'day' => 'numeric|digits_between:1,2|min:1|max:31',
            'month' => 'numeric|digits_between:1,2|min:1|max:12',
            'year' => 'required|numeric|digits:4|min:1900|max:2099',
            'picture' => 'file|image|mimes:jpeg,jpg,png,gif|max:4096', # image, max. 4MB
        ];

        switch($this->method()) {
            case 'PATCH':
                $rules['title'] = 'string|min:3|max:255'; # remove the required since the PATCH might just not contain this key
                $rules['year'] = 'numeric|digits:4|min:1900|max:2099'; # remove the required since the PATCH might just not contain this key
                break;
        }

        return $rules;
    }
}
