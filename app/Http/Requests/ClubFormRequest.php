<?php

namespace Vereinsleben\Http\Requests;

use Vereinsleben\Http\Requests\Request;

class ClubFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string|min:3',
            'street' => 'required|string|min:2',
            'house_number' => 'string|min:1',
            'zip' => 'numeric|min:5',
            'city' => 'required|string|min:3',
            'email' => 'email',
            'founded' => 'numeric|digits:4|before:tomorrow|date_format:Y',
            'shorthand' => 'string|min:2',
            'member_count' => 'in:1-10,11-50,51-100,101-250,251-500,501-750,751-1000,1001-1500,1500+',
            'description' => 'string|max:32765',
            'about' => 'string|max:32765',
            'header' => 'file|image|mimes:jpeg,jpg,png,gif|max:4096', # image, max. 4MB
            'avatar' => 'file|image|mimes:jpeg,jpg,png,gif|max:4096', # image, max. 4MB
            'sports' => 'required|array',
            'social_links' => 'array',
        ];

        switch ($this->method()) {
            case 'PATCH': # remove the required since the PATCH might just not contain this key
                # rules as above, but without required
                $optionalPatchRules = [
                    'name' => 'string|min:3',
                    'street' => 'string|min:2',
                    'city' => 'string|min:3',
                    'sports' => 'array',
                ];

                foreach ($optionalPatchRules as $key => $rule) {
                    if ($this->input($key, null) === null) {
                        $rules[$key] = $rule; # if the request does not contain the key
                    }
                }

                break;
        }

        return $rules;
    }
}
