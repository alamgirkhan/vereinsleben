<?php

namespace Vereinsleben\Http\Requests;

use Vereinsleben\Http\Requests\Request;

class ClubPostFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required|string|max:128',
            'content_raw' => 'required|string|max:32000',
            'published_from' => 'date_format:d.m.Y H:i',
            'published_to' => 'date_format:d.m.Y H:i',
        ];

//        $nbr = count($this->input('images')) - 1;
        $nbr = is_array($this->input('images')) ? count($this->input('images')) - 1 : 0;
        foreach(range(0, $nbr) as $index) {
            $rules['image.' . $index] = 'file|image|mimes:jpeg,jpg,png,gif|max:4096'; # image, max. 4MB
        }

        return $rules;
    }
}
