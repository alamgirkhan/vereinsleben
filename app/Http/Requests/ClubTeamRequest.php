<?php

namespace Vereinsleben\Http\Requests;

use Vereinsleben\Http\Requests\Request;

class ClubTeamRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string|max:128',
            'picture' => 'file|image|mimes:jpeg,jpg,png,gif|max:4096', # image, max. 4MB
        ];

        switch($this->method()) {
            case 'PATCH': # remove the required since the PATCH might just not contain this key
                # rules which are required when present, but not when not present.
                $optionalRequiredPatchRules = [
                    'name',
                    'sport_id',
                ];

                foreach ($optionalRequiredPatchRules as $key) {
                    if ($this->input($key, null) === null) {
                        $rules[$key] = str_replace('required|', '', $rules[$key]); # if the request does not contain the key
                    }
                }

                break;
        }

        return $rules;
    }
}
