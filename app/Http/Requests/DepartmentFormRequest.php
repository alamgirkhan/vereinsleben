<?php

namespace Vereinsleben\Http\Requests;

use Vereinsleben\Http\Requests\Request;

class DepartmentFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'sport_id' => 'required',
//            'user_id'  => 'required',
        ];

        switch ($this->method()){
            case 'PATCH': # remove the required since the PATCH might just not contain this key
                # rules as above, but without required
                $optionalPatchRules = [

                ];

                foreach ($optionalPatchRules as $key => $rule){
                    if ($this->input($key, null) === null){
                        $rules[$key] = $rule; # if the request does not contain the key
                    }
                }

                break;
        }

        return $rules;
    }
}
