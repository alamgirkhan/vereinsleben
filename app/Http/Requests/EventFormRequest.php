<?php

namespace Vereinsleben\Http\Requests;

use Vereinsleben\Http\Requests\Request;

class EventFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title'          => 'required|string|max:128',
            'content_raw'    => 'required|string',
            'club_id'        => 'required|numeric',
            'street'         => 'required|string',
            'house_number'   => 'string|max:5',
            'zip'            => 'required|string|max:5',
            'city'           => 'required|string|max:128',
            'sport'          => 'required|numeric',
            'schedule_begin' => 'required|date_format:d.m.Y H:i',
            'schedule_end'   => 'required|date_format:d.m.Y H:i',
            'published_from' => 'date_format:d.m.Y H:i',
            'published_to'   => 'date_format:d.m.Y H:i',
        ];

//        $nbr = count($this->input('images')) - 1;
        $nbr = is_array($this->input('images')) ? count($this->input('images')) - 1 : 0;
        foreach(range(0, $nbr) as $index) {
            $rules['image.' . $index] = 'file|image|mimes:jpeg,jpg,png,gif|max:4096'; # image, max. 4MB
        }
        switch ($this->method()){
            case 'PATCH':
                $rules['club_id'] = 'alpha_dash'; # remove the required and unique
                $rules['sport'] = 'alpha_dash';
                break;

            case 'POST':
                $rules['club_id'] = 'alpha_dash'; # remove the required and unique
                $rules['sport'] = 'alpha_dash';
                break;
        }

        return $rules;
    }
}
