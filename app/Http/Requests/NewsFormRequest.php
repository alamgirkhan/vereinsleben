<?php

namespace Vereinsleben\Http\Requests;

use Vereinsleben\Http\Requests\Request;

class NewsFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'published_from' => 'date_format:d.m.Y H:i',
            'published_until' => 'date_format:d.m.Y H:i',
            'element_order' => 'digits_between:1,12',
            'category_id' => 'required|numeric',
            'title' => 'required|string',
            'sub_title' => 'string',
            'main_image' => 'file|image|mimes:jpeg,jpg,png,gif|max:4096',
            'main_image_source' => 'string',
            'slug' => 'required|alpha_dash|unique:news,slug',
        ];

        switch ($this->method()) {
            case 'PATCH':
                $rules['slug'] = 'alpha_dash'; # remove the required and unique since the PATCH does not update the slug
                break;
        }

        return $rules;
    }
}
