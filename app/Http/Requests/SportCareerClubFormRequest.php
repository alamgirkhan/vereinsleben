<?php

namespace Vereinsleben\Http\Requests;

use Vereinsleben\Http\Requests\Request;

class SportCareerClubFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'wctimeline_begin_month' => 'required',
            'wctimeline_begin_year'  => 'required',
            'wcclub_id'              => 'required|string|max:128'
        ];
    }

    public function messages()
    {
        return [
            'timeline_begin.required' => 'Bitte gib ein Datum / Jahr ein',
        ];
    }
}
