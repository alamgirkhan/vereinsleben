<?php

namespace Vereinsleben\Http\Requests;

use Vereinsleben\Http\Requests\Request;

class SportCareerFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'timeline_begin_month' => 'required',
            'timeline_begin_year'  => 'required',
            'content'              => 'string',
            'layout'               => 'string|max:64',
            'club_id'              => 'required|string|max:128'
        ];
    }
    
    public function messages() {
        return [
            'timeline_begin.required' => 'Bitte gib ein Datum / Jahr ein',
        ];
    }
}
