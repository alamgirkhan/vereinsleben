<?php

namespace Vereinsleben\Http\Requests;

use Vereinsleben\Http\Requests\Request;

class TrainingortFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string|max:64',
            'title' => 'required|string|max:128',
            'street' => 'required|string|max:128',
            'zip' => 'required|string|max:16',
            'city' => 'required|string|max:128',
        ];

        return $rules;
    }
}
