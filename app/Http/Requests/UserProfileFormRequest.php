<?php

namespace Vereinsleben\Http\Requests;

use Vereinsleben\Http\Requests\Request;

class UserProfileFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    protected $dontFlash = ['gender', 'firstname', 'lastname', 'city'];

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'gender'         => 'required|string|max:200',
            'firstname'      => 'required|string|min:2|max:200',
            'lastname'       => 'required|string|min:2|max:200',
            'birthday-day'   => 'required|numeric|digits_between:1,2|min:1|max:31',
            'birthday-month' => 'required|numeric|digits_between:1,2|min:1|max:12',
            'birthday-year'  => 'required|numeric|digits:4|min:1900|max:2099',
            'nickname'       => 'string|max:200',
            'street'         => 'string|max:200',
            'house_number'   => 'string|min:1|max:5',
            'zip'            => 'numeric',
            'city'           => 'string|min:2|max:200',
            'federal_state'  => 'string|max:200',
            'newsletter'     => 'string|max:1',
            'messages'       => 'string|max:1',

            'header' => 'file|image|mimes:jpeg,jpg,png,gif|max:4096', # image, max. 4MB
            'avatar' => 'file|image|mimes:jpeg,jpg,png,gif|max:4096', # image, max. 4MB
        ];

        switch ($this->method()) {
            case 'PATCH': # remove the required since the PATCH might just not contain this key
                # rules which are required when present, but not when not present.
                $optionalRequiredPatchRules = [
                    'gender',
                    'firstname',
                    'lastname',
                    'birthday-day',
                    'birthday-month',
                    'birthday-year',
                ];

                foreach ($optionalRequiredPatchRules as $key) {
                    if ($this->input($key, null) === null) {
                        $rules[$key] = str_replace('required|', '', $rules[$key]); # if the request does not contain the key
                    }
                }

                break;
        }

        return $rules;
    }
}
