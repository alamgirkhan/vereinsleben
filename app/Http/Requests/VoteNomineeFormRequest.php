<?php

namespace Vereinsleben\Http\Requests;

use Vereinsleben\Http\Requests\Request;

class VoteNomineeFormRequest extends Request
{
    public function authorize() {
        return true;
    }

    public function rules()
    {
        return [
            'club_id' => 'required|integer',
            'project' => 'required|string|max:255'
        ];
    }

    public function messages()
    {
        return [
            'club_id.required' => 'Bitte wähle einen Verein aus',
            'project.required' => 'Bitte geben einen Projektnamen ein'
        ];
    }
}