<?php

namespace Vereinsleben\Http\Transformers;

use Vereinsleben\City;
use League\Fractal;

class CityTransformer extends Fractal\TransformerAbstract
{
    public function transform(City $city)
    {
        return [
            'id' => (int) $city->id,
            'name' => (string) $city->name,
            'lat' => (string) $city->lat,
            'lon' => (string) $city->lon,
        ];
    }
}