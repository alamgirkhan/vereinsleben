<?php

namespace Vereinsleben\Http\Transformers;

use Vereinsleben\Club;
use League\Fractal;

class ClubTransformer extends Fractal\TransformerAbstract
{
    public function transform(Club $club)
    {
        return [
            'id' => (int) $club->id,
            'name' => (string) $club->name,
            'shorthand' => (string) $club->shorthand,
            'founded' => (string) $club->founded,
            'member_count' => (string) $club->member_count,
            'description' => (string) $club->description,
            'street' => (string) $club->street,
            'house_number' => (string) $club->house_number,
            'city_id' => (string) $club->city_id,
            'zip' => (string) $club->zip,
            'federal_state_id' => (string) $club->federal_state_id,
            'lat' => (string) $club->lat,
            'lng' => (string) $club->lng,
            'email' => (string) $club->email,
            'published' => (string) $club->published,
            'slug' => (string) $club->slug,
            'about' => (string) $club->about,
            'gallery_id' => (string) $club->gallery_id,
            'created_at' => (string) $club->created_at,
            'modified_at' => (string) $club->modified_at,
            'images' => [
                'avatar' => [
                    'singleView' => (string) asset($club->avatar->url('singleView')),
                    'startPage' => (string) asset($club->avatar->url('startPage')),
                ],
            ],
        ];
    }
}