<?php

namespace Vereinsleben\Http\Transformers;

use Vereinsleben\News;
use League\Fractal;

class NewsTransformer extends Fractal\TransformerAbstract
{
    public function transform(News $news)
    {
        return [
            'id' => (int) $news->id,
            'title' => (string) $news->title,
            'sub_title' => (string) $news->sub_title,
            'slug' => (string) $news->slug,
            'content_teaser' => (string) $news->content_teaser,
            'content' => (string) $news->content,
            'published_at' => (string) $news->published_at,
            'published_from' => (string) $news->published_from,
            'published_until' => (string) $news->published_until,
            'element_order' => (string) $news->element_order,
            'category_id' => (int) $news->category_id,
            'main_image_teaser' => ($news->main_image === null) ? null : asset($news->main_image->url('startpage')),
            'main_image_full' => ($news->main_image === null) ? null : asset($news->main_image->url('singleview')),
            'main_image_source' => (string) $news->main_image_source,
            'category_slug' => ($news->category !== null) ? (string) $news->category->slug : '',
        ];
    }
}