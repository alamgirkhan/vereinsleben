<?php

namespace Vereinsleben\Http\Transformers;

use Vereinsleben\Sport;
use League\Fractal;

class SportTransformer extends Fractal\TransformerAbstract
{
    public function transform(Sport $sport)
    {
        return [
            'id' => (int) $sport->id,
            'title' => (string) $sport->title,
        ];
    }
}