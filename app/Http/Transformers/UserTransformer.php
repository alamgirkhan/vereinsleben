<?php

namespace Vereinsleben\Http\Transformers;

use Vereinsleben\User;
use League\Fractal;

class UserTransformer extends Fractal\TransformerAbstract
{
    public function transform(User $user)
    {
        return [
            'id'                    => (int)$user->id,
            'username'              => (string)$user->username,
            'firstname'             => (string)$user->firstname,
            'lastname'              => (string)$user->lastname,
            'fullname'              => (string)$user->fullname,
            'email'                 => (string)$user->email,
            'street'                => (string)$user->street,
            'house_number'          => (string)$user->house_number,
            'city'                  => (string)$user->city,
            'zip'                   => (string)$user->zip,
            'federal_state'         => (string)$user->federal_state,
            'desired_federal_state' => (string)$user->desired_federal_state,
            'lat'                   => (string)$user->lat,
            'lng'                   => (string)$user->lng,
            'newsletter'            => (string)$user->newsletter,
            'birthday'              => (string)$user->birthday,
            'nickname'              => (string)$user->nickname,
            'gallery_id'            => (string)$user->gallery_id,
            'created_at'            => (string)$user->created_at,
            'modified_at'           => (string)$user->modified_at,
            'images'                => [
                'avatar' => [
                    'singleView' => (string)asset($user->avatar->url('singleView')),
                    'startPage'  => (string)asset($user->avatar->url('startPage')),
                ],
            ],
        ];
    }
}