<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

# news
Route::group(['prefix' => 'neuigkeiten'], function(){
    Route::get('/', function(){
        return abort(404);
    });
    Route::get('/related/category/{categoryId?}/news/{newsId?}', 'NewsController@related')->name('news.related');
    Route::get('/categoryItems/{categorySlug}', 'NewsController@categoryItems')->name('news.category.items');
    Route::get('/{categorySlug}', 'NewsController@categoryIndex')->name('news.category');
    Route::get('/{categorySlug}/{newsSlug}', 'NewsController@show')->name('news.detail');
});

Route::group(['prefix' => 'service'], function(){
    Route::get('/stellenboerse', function(){
        return view('news.job-exchange');
    });
});

# club detail, edit
Route::group(['prefix' => '/vereine/{slug}'], function(){
    Route::get('/', [
        'as'   => 'club.detail',
        'uses' => 'ClubController@show'
    ]);
    Route::patch('updatePublished', [
        'as'   => 'club.updatePublished',
        'uses' => 'ClubController@updatePublished'
    ]);
    Route::patch('/', [
        'as'   => 'club.update',
        'uses' => 'ClubController@updatePatch'
    ]);

    # club achievements
    Route::group(['prefix' => 'achievement'], function(){
        Route::post('/', 'ClubAchievementController@store')->name('club.achievement.store');
        Route::get('/edit/{id}', 'ClubAchievementController@edit')->name('club.achievement.edit');
        Route::get('/latest', 'ClubAchievementController@latest')->name('club.achievement.latest');
        Route::patch('/{id}', 'ClubAchievementController@update')->name('club.achievement.update');
        Route::delete('/{id}', 'ClubAchievementController@delete')->name('club.achievement.delete');
    });

    # teams
    Route::group(['prefix' => 'team'], function(){
        Route::post('/', 'TeamController@store')->name('team.store');
        Route::get('/edit/{id}', 'TeamController@edit')->name('team.edit');
        Route::get('/latest', 'TeamController@latest')->name('team.latest');
        Route::patch('/{id}', 'TeamController@update')->name('team.update');
        Route::delete('/{id}', 'TeamController@delete')->name('team.delete');
    });

    # posts
    Route::group(['prefix' => 'post'], function(){
        Route::get('/latest', 'ClubPostController@latest')->name('post.latest');
        Route::get('/single/{id}', 'ClubPostController@single')->name('post.single');
    });

    # events
    Route::group(['prefix' => 'event'], function(){
        Route::get('/latest', 'EventController@latest')->name('event.latest');
        Route::get('/single/{id}', 'EventController@single')->name('event.single');
    });

    # post / event overview
    Route::get('/network-event-post-overview', 'ClubController@networkEventPostOverview')->name('network.event.post.overview');
    Route::get('/event-post-overview', 'ClubController@eventPostOverview')->name('event.post.overview');


    # become fan or member or un-fanize/un-memberize
    Route::post('/update-fan-status', 'ClubController@updateFanStatus')->name('club.fan.update');
    Route::post('/update-member-status', 'ClubController@updateMemberStatus')->name('club.member.update');
});

# club creation
Route::get('/verein-anlegen', [
    'as'   => 'club.create',
    'uses' => 'ClubController@create'
]);
Route::post('/vereine', [
    'as'   => 'club.store',
    'uses' => 'ClubController@store'
]);
Route::get('/vereine', function(){
    return redirect()->route('club.search');
});

# club
Route::group(['prefix' => 'club'], function(){
    Route::get('latest', 'ClubController@latest')->name('club.latest');
    Route::get('user-list', 'ClubController@userList')->name('club.user.list');
    Route::get('claim/{id}', 'ClubController@claim')->name('club.claim');
});

# authentication, registration
Route::auth();

Route::get('/register/verify/{token}', [
    'as'   => 'register_verify',
    'uses' => 'Auth\AuthController@registerVerify'
]);

# user, dashboard, profile

Route::group(['prefix' => 'user'], function(){
    Route::get('dashboard', [
        'as'         => 'user.dashboard',
        'uses'       => 'UserController@dashboard',
        'middleware' => 'auth',
    ]);

    Route::get('profile/edit', [
        'as'         => 'user.profile.edit',
        'uses'       => 'UserController@editProfile',
        'middleware' => 'auth',
    ]);

    Route::post('profile/update', [
        'as'         => 'user.profile.update',
        'uses'       => 'UserController@updateProfile',
        'middleware' => 'auth',
    ]);

    Route::get('password/edit', [
        'as'         => 'user.password.edit',
        'uses'       => 'UserController@editPassword',
        'middleware' => 'auth',
    ]);

    Route::post('password/update', [
        'as'         => 'user.password.update',
        'uses'       => 'UserController@updatePassword',
        'middleware' => 'auth',
    ]);

    Route::get('latest', 'UserController@latest')->name('user.latest');
});

// API - prefix: /api
$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function($api){
    $api->group(['namespace' => 'Vereinsleben\Http\Controllers\Api'], function($api){
        $api->get('cities', 'CityController@search')->name('api.cities');
        $api->get('sports', 'SportController@search')->name('api.sports');
        $api->group(['prefix' => 'clubs'], function($club) use ($api){
            $club->get('/', 'ClubController@index')->name('api.clubs');
            $club->get('search', 'ClubController@search');
            $club->get('autocomplete', 'ClubController@autocomplete')->name('api.club.autocomplete');
            $club->get('list', 'ClubController@clubList')->name('api.club.list');
        });
    });
});

// administration
Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function(){
    Route::get('/', 'AdminController@index')->name('admin.index');
    Route::get('/create', 'AdminController@create')->name('admin.news.create');
    Route::get('/edit/{id}', 'AdminController@edit')->name('admin.news.edit');
    Route::post('/store', 'AdminController@store')->name('admin.news.store');
    Route::patch('/update/{id}', 'AdminController@update')->name('admin.news.update');
    Route::post('/upload-news-image', 'AdminController@uploadNewsImage');
    Route::patch('/news/toggle-visibility', [
        'uses' => 'AdminController@toggleVisibility',
        'as'   => 'admin.news.togglevisibility',
    ]);
    Route::patch('/news/destroy', [
        'uses' => 'AdminController@destroy',
        'as'   => 'admin.news.destroy',
    ]);


    # sports
    Route::get('sports', function(){
        return view('admin.sport.index');
    });

    # clubs
    Route::get('clubs', function(){
        return view('admin.club.index');
    });

    # exports
    Route::get('exports', function(){
        return view('admin.export.index');
    });

    Route::group(['prefix' => 'club'], function(){
        # review club
        Route::patch('review', 'AdminController@reviewClub')->name('admin.club.review');

        # export
        Route::get('export', 'AdminController@exportClubs')->name('admin.club.export');

        # retrieve clubs by type (new-unowned, new-owned, ...)
        Route::get('{type}', 'AdminController@clubList')->name('admin.club.list');

        # this is an update call to flag the resource as deleted, so no DELETE method and /delete
        Route::patch('{id}/delete', 'ClubController@delete')->name('admin.club.delete');
    });

    Route::group(['prefix' => 'user'], function(){
        # export
        Route::get('export', 'AdminController@exportUsers')->name('admin.user.export');
    });

    Route::group(['prefix' => 'sport'], function(){
        Route::patch('{id}/review', 'AdminController@reviewSport')->name('admin.sport.review');
        Route::get('{type}', 'AdminController@sportList')->name('admin.sport.list');
        Route::patch('{id}/delete', 'SportController@delete')->name('admin.sport.delete');
    });

    Route::group(['namespace' => 'Admin'], function(){
        Route::resource('vote', 'VoteController');

        Route::get('vote/create', 'VoteController@create');

        Route::group(['prefix' => 'vote/{vote_id}'], function(){
            Route::resource('nominee', 'VoteNomineeController');

            Route::get('nominee/{id}/delete', 'VoteNomineeController@delete')->name('admin.vote.{vote_id}.nominee.{nominee_id}.delete');
            Route::get('nominee/{id}/winner', 'VoteNomineeController@setWinner')->name('admin.vote.{vote_id}.nominee.{nominee_id}.winner');
        });

        Route::get('checking/fake', 'CheckingController@fake');
        Route::get('checking/doublets', 'CheckingController@proofDoublet');
        Route::get('checking/nominee-add-votes/{nominee_id}', 'CheckingController@addVotesToIdNominee');

        //Route::get('deletion/user/{value}', 'CheckingController@deleteUser');
        Route::get('deletion/user/{data}/club/{deleteRelatedClubs}', 'CheckingController@deleteUser');
        Route::get('deletion/fake', 'CheckingController@deleteFake');

        Route::get('verification/user/{email}', 'CheckingController@verifyUser');
    });

});

// posts
Route::group(['prefix' => 'post'], function(){
    Route::post('/', 'ClubPostController@store')->name('post.store');
    Route::get('/edit/{id}', 'ClubPostController@edit')->name('post.edit');
    Route::patch('/update/{id}', 'ClubPostController@update')->name('post.update');
    Route::delete('/delete/{id}', 'ClubPostController@delete')->name('post.delete');
});

// events
Route::group(['prefix' => 'event'], function(){
    Route::post('/', 'EventController@store')->name('event.store');
    Route::get('/edit/{id}', 'EventController@edit')->name('event.edit');
    Route::patch('/update/{id}', 'EventController@update')->name('event.update');
    Route::delete('/delete/{id}', 'EventController@delete')->name('event.delete');
});

// static / content pages
Route::group([], function(){
    Route::get('/', 'IndexController@index')->name('home');
    Route::get('/kontakt', function(){
        return view('sites.contact');
    });

    Route::get('/faq', function(){
        return view('sites.faq');
    });

    Route::get('/agbs', function(){
        return view('sites.terms');
    });

    Route::get('/impressum', function(){
        return view('sites.imprint');
    });

    Route::get('/podcast', function(){
        return view('sites.podcast');
    });
});

# club voting
Route::get('verein-des-monats', 'VoteController@overview')->name('vote.index');

Route::get('verein-des-monats/teilnehmen', function(){
    return view('vote.participate');
});

Route::get('verein-des-monats/teilnahmebedingungen', function(){
    return view('vote.agreement');
});
Route::get('verein-des-monats/ueber-den-wettbewerb', function(){
    return view('vote.about');
});

Route::get('verein-des-monats/sponsoren/fortuna-sportgeraete', function(){
    return view('vote.sponsor-fortuna');
});

Route::get('verein-des-monats/sponsoren/landessportbund-rheinland-pfalz', function(){
    return view('vote.sponsor-lsb');
});

Route::get('verein-des-monats/sponsoren/sparda-bank', function(){
    return view('vote.sponsor-sparda');
});

Route::get('verein-des-monats/sponsoren/rpr1', function(){
    return view('vote.sponsor-rpr1');
});


Route::post('verein-des-monats/teilnehmen', 'VoteController@participate')->name('club.vote.participate');


Route::get('verein-des-monats/{clubSlug}', 'VoteController@nominee')->name('club.vote.nominee');

Route::group(['prefix' => 'vote'], function(){
    Route::post('{voteId}/nominee/{nomineeId}', 'VoteController@voteFor')->name('vote.for.nominee');
});


# redirect deprecated urls
Route::get('/news-{slug}', function($slug){
    $news = \Vereinsleben\News::where('slug', $slug)->firstOrFail();

    return redirect()->route('news.detail', [$news->category->slug, $news->slug], 301);
});

Route::get('vereine-finden', function(){
    return redirect()->route('club.search');
});

Route::get('/verein-{slug}', function($slug){
    $club = \Vereinsleben\Club::where('slug', $slug)->firstOrFail();

    return redirect()->route('club.detail', $club->slug, 301);
});

// XML sitemap
Route::get('sitemap.xml', function(){

    $sitemap = App::make("sitemap");

    // check if there is cached sitemap and build new only if is not
    if (!$sitemap->isCached()){

        // add item to the sitemap (url, date, priority, freq)
        $sitemap->add(URL::to('/'), date('c', time()), '1.0', 'daily');
        $sitemap->add(URL::to('/kontakt'), date('c', time()), '0.5', 'monthly');
        $sitemap->add(URL::to('/faq'), date('c', time()), '0.5', 'monthly');
        $sitemap->add(URL::to('/agbs'), date('c', time()), '0.5', 'monthly');
        $sitemap->add(URL::to('/impressum'), date('c', time()), '0.5', 'monthly');

        $sitemap->add(URL::to('/verein-finden'), date('c', time()), '1.0', 'weekly');

        $categories = \Vereinsleben\NewsCategory::unarchived()->withAnyParent()->get();
        foreach ($categories as $category){
            $sitemap->add(URL::route('news.category', $category->slug), date('c', time()), '1.0', 'daily');
        }

        $newses = \Vereinsleben\News::published()->public()->get();
        foreach ($newses as $news){
            $sitemap->add(URL::route('news.detail', [($news->category !== null ? $news->category->slug : null), $news->slug]), date('c', is_null($news->updated_at) ? strtotime($news->created_at) : strtotime($news->updated_at)), '1.0', 'weekly');
        }

        $clubs = \Vereinsleben\Club::published()->get();
        foreach ($clubs as $club){
            $sitemap->add(URL::route('club.detail', $club->slug), date('c', is_null($club->updated_at) ? strtotime($club->created_at) : strtotime($club->updated_at)), '1.0', 'daily');
        }

    }

    // show your sitemap (options: 'xml' (default), 'html', 'txt', 'ror-rss', 'ror-rdf')
    return $sitemap->render('xml');

});

# user profile
Route::group(['prefix' => '/benutzer'], function(){
    Route::get('/{username}', 'UserController@show')->name('user.detail');
    Route::patch('/{username}/update', 'UserController@updatePatch')->name('user.profile.patch');

    // user posts
    Route::group(['prefix' => '/{username?}/post'], function(){
        Route::get('/{id}/template', 'UserPostController@showRendered')->name('user.post.template');
        Route::post('/', 'UserPostController@store')->name('user.post.store');
        Route::get('/{id}/edit', 'UserPostController@edit')->name('user.post.edit');
        Route::post('/{id}', 'UserPostController@update')->name('user.post.update');
        Route::delete('/{id}', 'UserPostController@destroy')->name('user.post.destroy');
    });

    // @todo see routes above and change them to be rest-like
    Route::post('post', 'UserPostController@store')->name('user.post.save');
});

# user sport
Route::group(['prefix' => '/user-sport'], function(){
    Route::post('/', 'UserSportController@store')->name('user.sport.store');
    Route::get('/{userId}', 'UserSportController@show')->name('user.sport');
    Route::get('/edit/{userId}', 'UserSportController@edit')->name('user.sport.edit');
    Route::get('/template/{userId}', 'UserSportController@showRendered')->name('user.sport.template');
    Route::patch('/delete', 'UserSportController@delete')->name('user.sport.delete');
});

# user sport career
Route::group(['prefix' => '/sport-career'], function(){
    Route::post('/', 'SportCareerController@store')->name('sport.career.store');
    Route::get('/edit/{id}', 'SportCareerController@edit')->name('sport.career.edit');
    Route::post('/{id}', 'SportCareerController@update')->name('sport.career.update');
    Route::delete('/{id}', 'SportCareerController@destroy')->name('sport.career.destroy');
    Route::get('/template/{id}', 'SportCareerController@showRendered')->name('sport.career.template');
});

# dynamic view rendering
Route::get('render-view/{viewName}', function($viewName){
    return view($viewName);
});

use Vereinsleben\Club;

# dynamic view rendering
Route::get('team-member/{slug}', function($slug){
    $club = Club::where('slug', $slug)->firstOrFail();

    return view('club.partials.modal.team-member', ['club' => $club]);
})->name('team-member');

# search
Route::group(['prefix' => 'suche'], function(){
    Route::get('/', 'SearchController@search')->name('search');
    Route::get('/vereine', 'SearchController@clubSearch')->name('club.search');
    Route::get('/benutzer', 'SearchController@userSearch')->name('user.search');
});

# user stream
// it's me, mario! ...returns the current user :)
Route::get('/user/me', 'UserController@me')->name('user.me');
// index
Route::get('user/stream', 'User\StreamController@index')->name('user.stream');
// events
Route::get('user/events', 'User\StreamController@events')->name('user.stream.events');
// clubs
Route::get('user/clubs', 'User\StreamController@clubs')->name('user.stream.clubs');
// users
Route::get('user/users', 'User\StreamController@users')->name('user.stream.users');