<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;

use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait as StaplerEloquentTrait;

/**
 * Vereinsleben\Image
 *
 * @property int $id
 * @property string|null $url
 * @property int|null $imageable_id
 * @property string|null $imageable_type
 * @property int|null $image_meta_id
 * @property string|null $image_size
 * @property string|null $image_type
 * @property string|null $picture_file_name
 * @property int|null $picture_file_size
 * @property string|null $picture_content_type
 * @property string|null $picture_updated_at
 * @property string|null $title
 * @property string|null $description
 * @property string|null $source
 * @property int|null $gallery_id
 * @property string|null $m_image_original
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $imageable
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Image whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Image whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Image whereGalleryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Image whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Image whereImageMetaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Image whereImageSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Image whereImageType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Image whereImageableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Image whereImageableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Image whereMImageOriginal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Image wherePictureContentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Image wherePictureFileName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Image wherePictureFileSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Image wherePictureUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Image whereSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Image whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Image whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Image whereUrl($value)
 * @mixin \Eloquent
 */
class Image extends Model implements StaplerableInterface
{
    use StaplerEloquentTrait;

    protected $table = 'image';

    // fillable via mass assignment
    protected $fillable = ['title', 'description', 'source', 'picture', 'order'];

    /**
     * Constructor
     *
     * @return Club
     */
    public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('picture', [
            'styles' => [
                'startPage' => '360x240#', # resize then crop
                'singleView' => '750', # resize, no crop
                'detail' => '600x600', # automagically resize by preserving ratio
            ],
            'url' => '/uploaded/image/:id/:attachment/:style-:secure_hash.:extension',
            #'default_url' => '/default_images/club/default-:attachment-:style.jpg',
        ]);

        parent::__construct($attributes);
    }

    public function imageable()
    {
        return $this->morphTo();
    }
}
