<?php

namespace Vereinsleben\Jobs;

use League\Csv\Writer;
use Mail;
use Storage;
use Vereinsleben\Club;
use Vereinsleben\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Vereinsleben\Role;

class ClubExport extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        set_time_limit(3000);

        $club = new Club;
        $role = new Role;
        $date = date('d-m-Y');
        $datum = explode("-", $date);
        //$filename = 'vlde-vereinsliste_' . $date . '.csv';
        $filename = $datum[2] . '_' . $datum[1] . '_' . $datum[0] . '_Verein.csv';
        $file = storage_path('app/tmp/' . $filename);
        $writer = Writer::createFromPath($file, 'w');
        $roleMapping = [
            Role::CLUB_OWNER  => $role->getByConstant(Role::CLUB_OWNER)->id,
            Role::CLUB_MEMBER => $role->getByConstant(Role::CLUB_MEMBER)->id,
            Role::CLUB_FAN    => $role->getByConstant(Role::CLUB_FAN)->id,
            Role::CLUB_ADMIN  => $role->getByConstant(Role::CLUB_ADMIN)->id,
        ];

        $writer->setNewline("\r\n"); //use windows line endings for compatibility with some csv libraries
        $writer->setOutputBOM(Writer::BOM_UTF8); //adding the BOM sequence on output

        // set csv first (header) row
        $writer->insertOne([
            'Name',
            'Straße',
            'Hausnummer',
            'PLZ',
            'Ort',
            'E-Mail',
            'Gegründet',
            'Kürzel',
            'Mitglieder',
            'Fans',
            'Mitglieder Anzahl',
            'Mannschaften',
            'Sportarten',
            'Posts',
            'Events',
            'Profilstatus',
            'Eigentümer',
            'Veröffentlicht',
            'Erstellt'
        ]);


        foreach ($club->with('teams')->with('posts')->with('events')->with('users')->cursor() as $club){

            $clubOwner = null;
            $clubUsers = [
                $roleMapping[Role::CLUB_OWNER]  => 0,
                $roleMapping[Role::CLUB_MEMBER] => 0,
                $roleMapping[Role::CLUB_FAN]    => 0,
                $roleMapping[Role::CLUB_ADMIN]  => 0,
            ];

            $club->users->each(function($user) use (&$clubOwner, &$clubUsers, $roleMapping){
                if ($user->pivot->role_id === $roleMapping[Role::CLUB_OWNER]){
                    $clubOwner = $user->username;
                }

                $clubUsers[$user->pivot->role_id]++;
            });

            $writer->insertOne([
                $club->name,
                $club->street,
                $club->house_number,
                $club->zip,
                $club->city,
                $club->email,
                $club->founded,
                $club->shorthand,
                $clubUsers[$roleMapping[Role::CLUB_MEMBER]],
                $clubUsers[$roleMapping[Role::CLUB_FAN]],
                $club->member_count,
                $club->teams->count(),
                $club->allSportsString(),
                $club->posts->count(),
                $club->events->count(),
                $club->getClubProfileCompletionProgress()['progress_percentage'] . '%',
                ($clubOwner !== null) ? $clubOwner : 'Kein Eigentümer',
                $club->isPublished() ? 'Ja' : 'Nein',
                date('d.m.Y', strtotime($club->created_at)),
            ]);

        }

        Mail::raw('Du findest den Vereinsexport ' . $filename . ' im Anhang dieser E-Mail.', function($message) use ($file){
            $message->subject('Der Vereinsexport vom ' . date('d.m.Y') . ' steht zum Download bereit!');
            $message->from('noreply@vereinsleben.de');
            $message->to(explode(';', env('MAIL_RECIPIENT_DEFAULT')));
            $message->cc(explode(';', env('MAIL_RECIPIENT_COPY')));
            $message->attach($file);
        });

        Storage::delete('tmp/' . $filename);

    }
}
