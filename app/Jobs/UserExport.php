<?php

namespace Vereinsleben\Jobs;

use Mail;
use Storage;
use Vereinsleben\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Vereinsleben\User;
use League\Csv\Writer;

class UserExport extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        set_time_limit(3000);

        $user = new User;
        $date = date('d-m-Y');
        $datum = explode("-", $date);
        $filename = $datum[2] . '_' . $datum[1] . '_' . $datum[0] . '_User.csv';
        //$filename = 'vlde-benutzerliste_' . $date . '.csv';
        $file = storage_path('app/tmp/' . $filename);
        $writer = Writer::createFromPath($file, 'w');

        $writer->setNewline("\r\n"); //use windows line endings for compatibility with some csv libraries
        $writer->setOutputBOM(Writer::BOM_UTF8); //adding the BOM sequence on output

        // set csv first (header) row
        $writer->insertOne([
            'Name',
            'Benutzername',
            'Straße',
            'Hausnummer',
            'PLZ',
            'Ort',
            'E-Mail',
            'Geburtstag',
            'Sportliche Interessen',
            'Vereine',
            'Posts',
            'Laufbahn',
            'Bestätigt',
            'Newsletter',
            'Datenschutz',
            'Ist Fake',
            'Registriert',
        ]);


        foreach ($user->with('clubs')->with('posts')->with('sport_careers')->with('sports')->cursor() as $user){
            $sports = $user->sports->flatMap(function($sport){
                return [$sport->title . ':' . $sport->pivot->interest];
            });

            $writer->insertOne([
                $user->fullname,
                $user->username,
                $user->street,
                $user->house_number,
                $user->zip,
                $user->city,
                $user->email,
                date('d.m.Y', strtotime($user->birthday)),
                $sports->implode(', '),
                $user->clubs->implode('name', ', '),
                !is_null($user->posts) ? $user->posts->count() : 0,
                !is_null($user->sport_careers) ? $user->sport_careers->count() : 0,
                !is_null($user->verified) ? $user->verified : 0,
                $user->newsletter,
                !is_null($user->privacy) ? $user->privacy : 0,
                !is_null($user->has_fake_email) ? $user->has_fake_email : 0,
                date('d.m.Y', strtotime($user->created_at)),
            ]);

        }

        $sent = Mail::raw('Du findest den Benutzerexport ' . $filename . ' im Anhang dieser E-Mail.', function($message) use ($file){
            $message->subject('Der Benutzerexport vom ' . date('d.m.Y') . ' steht zum Download bereit!');
            $message->from('noreply@vereinsleben.de');
            $message->to(explode(';', env('MAIL_RECIPIENT_DEFAULT')));
            $message->cc(explode(';', env('MAIL_RECIPIENT_COPY')));
            $message->attach($file);
        });


        if ($sent !== 0){
            Storage::delete('tmp/' . $filename);
        } else{
            Mail::raw($filename . ' wurde nicht versandt.', function($message){
                $message->subject('Der Versand des Benutzerexport vom ' . date('d.m.Y') . ' ist fehlgeschlagen!');
                $message->from('noreply@vereinsleben.de');
                $message->to(explode(';', env('MAIL_RECIPIENT_DEVELOPERS')));
            });
        }

    }
}