<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;

class KeyWord extends Model
{
	protected $table = 'sport_federalstate_city';
	
	protected $fillable = [
		'sport',
		'location',
		'description',
	];
	
	public $timestamps = false;
}
