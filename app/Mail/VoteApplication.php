<?php

namespace Vereinsleben\Mail;

use Illuminate\Support\Facades\Mail;

class VoteApplication
{
    static public function send($data)
    {
        return Mail::send('vote.emails.application', $data, function ($message) use ($data) {
            $message->subject('Bewerbung Verein des Monats');
            $message->from('verein-des-monats@vereinsleben.de');

            if ($data['projectImages'] != null && count($data['projectImages']) > 0) {
                foreach ($data['projectImages'] as $item) {
                    $message->attach(public_path() . $item, ['as' => 'Projektbild.' . pathinfo($item)['extension']]);
                }
            }

            if (isset($data['logo']) && $data['logo'] != null) {
                $message->attach(public_path() . ($data['logo']), ['as' => 'Vereinslogo.' . pathinfo($data['logo'])['extension']]);
            }

            if (isset($data['clubImage']) && $data['clubImage'] != null) {
                $message->attach(public_path() . ($data['clubImage']), ['as' => 'Vereinsbild.' . pathinfo($data['clubImage'])['extension']]);
            }

            if (isset($data['projectDescription']) && $data['projectDescription'] != null) {
                $message->attach($data['projectDescription'], ['as' => 'Vereinsbeschreibung.' . pathinfo($data['projectDescription'])['extension']]);
            }



            $message->to(VoteApplication::envStringToArray(env('MAIL_RECIPIENT_DEFAULT')));
            $message->cc(VoteApplication::envStringToArray(env('MAIL_RECIPIENT_COPY')));
            $message->bcc(VoteApplication::envStringToArray(env('MAIL_RECIPIENT_BLIND_COPY')));
        });
    }

    static public function envStringToArray($string, $divider = ';')
    {
        if( strpos($string, $divider) !== false && strpos($string, $divider) > 0 ) {
            return explode($divider, $string);
        } else {
            return $string;
        }
    }
}