<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{


    const FAN    = 'fan';
    const ACTIVE = 'active';
    const OLD    = 'old';


    protected $table = 'members';

    /**
     * @param $constant
     * @return mixed id
     */
    public function getByConstant($constant)
    {
        return $this->where('name', $constant)->select('id')->firstOrFail();
    }
    //
}
