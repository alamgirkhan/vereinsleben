<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
    protected $fillable = [
        'meta_bild_alt',
        'meta_description',
        'meta_keywords',
        'meta_author',
        'meta_language'
    ];


    public function metaabel()
    {
        return $this->morphTo();
    }
}
