<?php

namespace Vereinsleben;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait as StaplerEloquentTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

/**
 * Vereinsleben\News
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $sub_title
 * @property string|null $slug
 * @property string|null $content
 * @property string|null $content_teaser
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property string|null $published_at
 * @property string|null $published_from
 * @property string|null $published_until
 * @property string|null $element_order
 * @property int|null $user_id
 * @property int|null $category_id
 * @property string|null $state
 * @property int|null $owner_id
 * @property int|null $gallery_id
 * @property string|null $m_category
 * @property string|null $m_owner
 * @property string|null $m_author
 * @property string|null $m_label
 * @property string|null $m_image_singleview
 * @property string|null $m_image_original
 * @property string|null $m_image_source
 * @property string|null $m_image_startpage
 * @property string|null $m_id
 * @property string|null $main_image_file_name
 * @property int|null $main_image_file_size
 * @property string|null $main_image_content_type
 * @property string|null $main_image_updated_at
 * @property string|null $main_image_source
 * @property int|null $club_id
 * @property-read \Vereinsleben\NewsCategory|null $category
 * @property-read \Vereinsleben\Gallery|null $gallery
 * @property-read \Illuminate\Database\Eloquent\Collection|\Vereinsleben\Image[] $images
 * @property-read \Vereinsleben\User|null $owner
 * @property-read \Vereinsleben\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News forCategory(\Vereinsleben\NewsCategory $category)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News latest($limit = 10)
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\News onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News public ()
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News published()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News whereClubId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News whereContentTeaser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News whereElementOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News whereGalleryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News whereMAuthor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News whereMCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News whereMId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News whereMImageOriginal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News whereMImageSingleview($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News whereMImageSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News whereMImageStartpage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News whereMLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News whereMOwner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News whereMainImageContentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News whereMainImageFileName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News whereMainImageFileSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News whereMainImageSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News whereMainImageUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News wherePublishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News wherePublishedFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News wherePublishedUntil($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News whereSubTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\News whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\News withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\News withoutTrashed()
 * @mixin \Eloquent
 */
class News extends Model implements StaplerableInterface
{
    use SoftDeletes;
    use StaplerEloquentTrait;

    const STATE_DRAFT     = 'DRAFT';
    const STATE_HIDDEN    = 'HIDDEN';
    const STATE_PUBLISHED = 'PUBLISHED';

    protected $table = 'news';
    protected $morphClass = 'news';

    // fillable via mass assignment
    protected $fillable = [
        'state',
        'title',
        'sub_title',
        'slug',
        'position',
        'content_teaser',
        'content',
        'published_at',
        'published_from',
        'published_until',
        'element_order',
        'category_id',
        'main_image',
        'main_image_source',
    ];

    /**
     * Constructor
     *
     * @return News
     */
    public function __construct(array $attributes = array())
    {
        $this->hasAttachedFile('main_image', [
            'styles'      => [
                'startpage'  => '360',
                'singleview' => '850',
            ],
            'url'         => '/uploaded/news/:id/:attachment/:style-:secure_hash.:extension',
            'default_url' => '/default_images/news/default-:attachment-:style.jpg',
        ]);

        parent::__construct($attributes);
    }

    /**
     * The user this news belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('Vereinsleben\User', 'user_id');
    }

    /**
     * The owner this news belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo('Vereinsleben\User', 'owner_id');
    }
	
		/**
		 * The users with roles in this news.
		 *
		 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
		 */
		public function users()
		{
			return $this->belongsToMany('Vereinsleben\User', 'user_news_role')
				->withPivot('role_id');
		}
	
		/**
		 * news with user content manager
		 *
		 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
		 */
		public function userContentManager()
		{
			$role = new Role();
			return $this->users()
				->wherePivot('role_id', $role->getByConstant(Role::CONTENT_MANAGER)->id);
		}
		
    /**
     * The category this news belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('Vereinsleben\NewsCategory', 'category_id');
    }

    /**
     * The gallery which belongs to this news
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gallery()
    {
        return $this->belongsTo('Vereinsleben\Gallery', 'gallery_id');
    }

    /**
     * The images which belong to this news
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function images()
    {
        return $this->morphMany('Vereinsleben\Image', 'imageable');
    }

    /**
     * Scope a query to fetch the latest news
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublished($query)
    {
        return $query->where('state', '=', self::STATE_PUBLISHED);
    }

    /**
     * @deprecated eloquent's query builder provides a latest method
     *
     * Scope a query to fetch the latest news
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeLatest($query, $limit = 10)
    {
        return $query->orderBy('created_at', 'desc')->take($limit);
    }

    /**
     * Scope a query to fetch the news from a category
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeForCategory($query, NewsCategory $category)
    {
        return $query->where('category_id', '=', $category->id);
    }


    /**
     * scope a query to fetch the news from a given category array
     * @param $query
     * @param $category
     * @return mixed
     */
    public function scopeForCategories($query, $category)
    {
        return $query->whereIn('category_id', $category);
    }


    /**
     * Select latest news by their order
     * @param $limit
     * @return mixed latest news
     */
    public function latestUpdatedNews($limit)
    {
        $newsData = $this->public()
                         ->where('main_image_file_name', '!=', '')
                         ->orderBy('updated_at', 'desc')
                         ->limit($limit)
                         ->get();

        return $newsData;
    }


    /**
     * Select latest news from sports category
     * @param $sportsid
     * @param $limit
     * @param $state
     * @return mixed latest news
     */
    public function latestSportsNews($sportsid, $limit, $state)
    {

        $elements_sportsid = collect([]);
        foreach ($sportsid as $key => $value){
            $elements_sportsid->put($key, $value->id);
        }

        $news = $this->public()
                     ->whereIn('category_id', $elements_sportsid)
                     ->where('has_gallery', '=', 0)
                     ->where('main_image_file_name', '!=', '')
                     ->byState($state)
                     ->orderBy('created_at', 'desc')
                     ->limit($limit)
                     ->get();

        return $news;
    }


    /** 30-05-2018
     * @param $sportsid
     * @param $limit
     * @param $state
     * @return mixed
     *
     * Get News by Position for display at Home
     */
    public function latestSportsNewsWithPos($sportsid, $limit, $state)
    {

        $elements_sportsid = collect([]);
        foreach ($sportsid as $key => $value){
            $elements_sportsid->put($key, $value->id);
        }

        $news = $this->public()
                     ->whereBetween('position', [1, 8])
                     ->whereIn('category_id', $elements_sportsid)
                     ->where('has_gallery', '=', 0)
                     ->where('main_image_file_name', '!=', '')
                     ->byState($state)
                     ->orderBy('position', 'asc')
                     ->limit($limit)
                     ->get();

        return $news;
    }



    /**
     * @param $limit
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function latestNews($limit)
    {
        $newsData = $this->public()
                         ->orderBy('created_at', 'desc')
                         ->limit($limit)
                         ->get();

        return $newsData;
    }


    /**
     * Latest News with federal state
     */
    public function latestNewsWithFed($state)
    {
        $newsData = $this->public()
                         ->orderBy('created_at', 'desc')
                         ->byState($state);

        return $newsData;
    }



    /**
     * @param $limit
     * @param $state
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function featuredNews($limit = 1, $state)
    {
        $newsData = $this->public()
                         ->where('has_gallery', '=', 1)
                         ->byState($state)
                         ->orderBy('created_at', 'desc')
                         ->limit($limit)
                         ->get();

        return $newsData;
    }


    /** 30-05-2018
     * @param $limit
     * @param $state
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     *
     * Get featured News by Position for display at Home
     */
    public function featuredNewsWithPos($limit = 1, $state)
    {
        $newsData = $this->public()
                         ->where('position', '=', 8)
                         ->where('has_gallery', '=', 1)
                         ->byState($state)
                         ->orderBy('position', 'desc')
                         ->limit($limit)
                         ->get();

        return $newsData;
    }


    /**
     * A scope to retrieve public news.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublic($query)
    {
        return $query
            ->published()
            ->where(function($query){
                $query
                    ->where(function($query){
                        $query
                            ->where('published_from', '<=', Carbon::now())
                            ->where('published_until', '>=', Carbon::now());
                    })
                    ->orWhere(function($query){
                        $query
                            ->where('published_from', '<=', Carbon::now())
                            ->whereNull('published_until');
                    })
                    ->orWhere(function($query){
                        $query
                            ->where('published_until', '>=', Carbon::now())
                            ->whereNull('published_from');
                    })
                    ->orWhere(function($query){
                        $query
                            ->whereNull('published_until')
                            ->whereNull('published_from');
                    });
            });
    }

    function scopeByState($query, $state){
        return $query->whereHas('states', function($q) use ($state){
//            $q->where('state_id', $state->id);
            $q->where('state_id', $state->id);
        });
    }


    /**
     * Check if the news is published
     *
     * @return boolean
     */
    public function isPublished()
    {
        return $this->state === self::STATE_PUBLISHED;
    }


    /**
     * Search the news with keyword
     *
     * @return boolean
     */
    public function scopeSearchByKeyword($query, $keywords)
    {
//        $query->select('id', 'title', 'sub_title');
        if (empty($keywords)){

            $query = $query->all();

        }else{
            foreach($keywords as $keyword){
                $query->where('state', '=', 'PUBLISHED')
	                    ->where(function($query) use ($keyword){
		                    $query->orWhere(DB::raw("REPLACE(title, 'ß', 'ss')"), 'LIKE', DB::raw("concat('%', REPLACE('".$keyword."', 'ß', 'ss'), '%')"));
		                    $query->orWhere(DB::raw("REPLACE(sub_title, 'ß', 'ss')"), 'LIKE', DB::raw("concat('%', REPLACE('".$keyword."', 'ß', 'ss'), '%')"));
		                    $query->orWhere(DB::raw("REPLACE(content_teaser, 'ß', 'ss')"), 'LIKE', DB::raw("concat('%', REPLACE('".$keyword."', 'ß', 'ss'), '%')"));
		                    $query->orWhere(DB::raw("REPLACE(content, 'ß', 'ss')"), 'LIKE', DB::raw("concat('%', REPLACE('".$keyword."', 'ß', 'ss'), '%')"));
	                    });
            }
        }
        return $query;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function meta()
    {
        return $this->morphOne('Vereinsleben\Meta', 'metaable');
    }

    public function states()
    {
        return $this->morphToMany('Vereinsleben\State', 'stateable');
    }

    public function tags()
    {
        return $this->morphToMany('Vereinsleben\Tag', 'taggable');
    }
}
