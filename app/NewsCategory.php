<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;

class NewsCategory extends Model
{
    protected $table = 'category';

    /**
     * The parent category this category belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parentCategory()
    {
        return $this->belongsTo('Vereinsleben\NewsCategory', 'parent_id');
    }

    /**
     * The news which belong to this category
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function news()
    {
        return $this->hasMany('Vereinsleben\News', 'category_id');
    }

    /**
     * Scope a query to fetch the categories with a parent category
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithAnyParent($query)
    {
        return $query->where('parent_id', '!=', 'NULL');
    }

    /**
     * Scope a query to fetch the categories with a parent category
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUnarchived($query)
    {
        return $query->whereNull('archive');
    }
}
