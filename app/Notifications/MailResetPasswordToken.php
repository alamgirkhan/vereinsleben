<?php

namespace Vereinsleben\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Vereinsleben\Content;

class MailResetPasswordToken extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */

    public $token;

    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $imprint = Content::select('content')->where('slug', 'imprint')->first();
        return (new MailMessage)
                    ->greeting('Hallo ' . $notifiable->firstname . ',')
                    ->line('Du erhälst diese eMail, weil wir eine Anfrage zur Änderung deines Passworts erhalten haben.')
                    ->action('Passwort zurücksetzen', url(config('app.url').route('password.reset', $this->token, false)))
                    ->line('Solltest Du diese Anfrage nicht gestartet haben, ist keine weitere Aktion deinerseits notwendig.')
                    ->salutation(' ')
                    ->line('Mit sportlichen Grüßen')
                    ->line('Dein Vereinsleben-Team')
                    ->line($imprint->content);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
