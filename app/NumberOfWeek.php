<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;

class NumberOfWeek extends Model
{
	protected $table = 'number_of_week';
}
