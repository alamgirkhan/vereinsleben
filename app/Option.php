<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
	protected $table = 'options';
	
	public $timestamps = false;
	
	public function get($name) {
		$option = Option::where('option_name', '=', $name)->first();
		return $option->option_value;
	}
	
	public function updateOption($name, $value) {
		$option = Option::where('option_name', '=', $name)->firstOrFail();
		$option->option_value = $value;
		$option->save();
		return $option;
	}
}
