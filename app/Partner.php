<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait as StaplerEloquentTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Vereinsleben\Partner
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $sub_title
 * @property string|null $slug
 * @property string|null $content
 * @property string|null $content_teaser
 * @property string|null $main_image_file_name
 * @property int|null $main_image_file_size
 * @property string|null $main_image_content_type
 * @property string|null $main_image_updated_at
 * @property string|null $main_image_source
 * @property published|0 $published
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static bool|null restore()
 * @mixin \Eloquent
 */
class Partner extends Model implements StaplerableInterface
{
    use SoftDeletes;
    use StaplerEloquentTrait;

    protected $table = 'partner';

    // fillable via mass assignment
    protected $fillable = [
        'title',
        'sub_title',
        'slug',
        'pos',
        'content_teaser',
        'content',
        'email',
        'phone',
        'address',
        'website',
        'logo',
        'main_image',
        'published',
    ];

    /**
     * Constructor
     *
     * @return image
     */
    public function __construct(array $attributes = array())
    {
        $this->hasAttachedFile('main_image', [
            'styles'      => [
                'startpage'  => '360',
                'singleview' => '750',
            ],
            'url'         => '/uploaded/partner/:id/:attachment/:style-:secure_hash.:extension',
            'default_url' => '/default_images/partner/default-:attachment-:style.jpg',
        ]);

        $this->hasAttachedFile('logo', [
            'url'         => '/uploaded/partner/:id/:attachment/:style-:secure_hash.:extension',
            'default_url' => '/default_images/partner/default-logo-:attachment.jpg',
        ]);		

        parent::__construct($attributes);
    }

    /**
     * Scope a query to fetch published partner
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublished($query)
    {
        return $query->where('published', '=', 1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function meta()
    {
        return $this->morphOne('Vereinsleben\Meta', 'metaable');
    }
	
		public function states()
		{
			return $this->morphToMany('Vereinsleben\State', 'stateable');
		}
	
		function scopeByState($query, $state)
		{
			return $query->whereHas('states', function($q) use ($state){
				$q->where('state_id', $state->id);
			});
		}
}
