<?php

namespace Vereinsleben\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Http\Request;

use Vereinsleben\Bandage;
use Vereinsleben\User;

class BandagePolicy
{
    use HandlesAuthorization;


    /**
     * The request object
     *
     * @var Illuminate\Http\Request
     */
    protected $request = null;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct(Request $request = null)
    {
        $this->request = $request;
    }
    /**
     * Determine whether the user can view the bandage.
     *
     * @param  \Vereinsleben\User  $user
     * @param  \Vereinsleben\Bandage  $bandage
     * @return mixed
     */
    public function view(User $user, Bandage $bandage)
    {
        //
    }

    /**
     * Determine whether the user can create bandages.
     *
     * @param  \Vereinsleben\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        if ($user->isAdmin()) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can update the bandage.
     *
     * @param  \Vereinsleben\User  $user
     * @param  \Vereinsleben\Bandage  $bandage
     * @return mixed
     */
    public function update(User $user, Bandage $bandage)
    {
        //
        if ($user->isAdmin()) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @param $bandage
     * @return bool
     */
    public function edit(User $user, $bandage){
	    return $this->defaultWritePolicy($user, $bandage);
    }

    /**
     * @param User $user
     * @param Bandage $bandage
     * @return bool
     */
    public function updateSubscribe(User $user, Bandage $bandage)
    {
        return !is_null($user);
    }

    /**
     * Determine whether the user can delete the bandage.
     *
     * @param  \Vereinsleben\User  $user
     * @param  \Vereinsleben\Bandage  $bandage
     * @return mixed
     */
    public function delete(User $user, Bandage $bandage)
    {
        //
        if ($user->isAdmin()) {
            return true;
        }

        return false;
    }


    public function store(User $user, Bandage $bandage)
    {
        if ($user->isAdmin()) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the model can be viewed by the user.
     *
     * @param  User $user
     * @param  Bandage $bandage
     * @return bool
     */
    public function show($user, Bandage $bandage)
    {
        // everyone (even non-authenticated visitors) can show a published bandage ...
        if ($bandage->isPublished()){
            return true;
        }

        // unpublished bandages: the admin can show all bandages and the bandage owner can show his bandage
		    return $this->defaultWritePolicy($user, $bandage);
    }
	
		protected function defaultWritePolicy(User $user, Bandage $bandage)
		{
			if ($user->isAdmin() || $user->isBandageAdmin($bandage)){
				return true;
			}
		}
}
