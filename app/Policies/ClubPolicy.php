<?php

namespace Vereinsleben\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Http\Request;

use Vereinsleben\Club;
use Vereinsleben\User;

class ClubPolicy
{
    use HandlesAuthorization;

    /**
     * The request object
     *
     * @var Illuminate\Http\Request
     */
    protected $request = null;

    /**
     * ClubPolicy constructor.
     * @param Request|null $request
     */
    public function __construct(Request $request = null)
    {
        $this->request = $request;
    }

    /**
     * Determine if the user may index models. Depends on the model ownership, so make only
     * basic checks here, additional scope filtering needed.
     *
     * @return bool
     */
    public function index($clubs)
    {
        // everyone (even non-authenticated visitors) has access to a published club
        if ($clubs){
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @param Club $club
     * @return bool
     */
    public function updateFanStatus(User $user, Club $club)
    {
        return !is_null($user);
    }

    /**
     * Determine if the user can become a fan of the club or un-fan.
     *
     * @param  User $user
     * @param  Club $club
     * @return bool
     */
    public function updateFriendship(User $user, Club $club)
    {
        if (!is_null($user) && ($user->isAdmin() || $user->isClubAdmin($club) || $user->isClubOwner($club) )){
            return true;
        }

        return false;
    }

    /**
     * Determine if the user can become a fan of the club or un-fan.
     *
     * @param  User $user
     * @param  Club $club
     * @return bool
     */
    public function createFriendship(User $user, Club $club)
    {
        if (!is_null($user) && ($user->isAdmin() || $user->isClubAdmin($club) || $user->isClubOwner($club) )){
            return true;
        }

        return false;
    }

    /**
     * Determine if the user can become a member of the club or un-memberize.
     *
     * @param  User $user
     * @param  Club $club
     * @return bool
     */
    public function updateMemberStatus(User $user, Club $club)
    {
        return !is_null($user);
    }

    /**
     * Determine if the user can become a member of the club or un-memberize.
     *
     * @param  User $user
     * @param  Club $club
     * @return bool
     */
    public function updateMember(User $user, Club $club)
    {
        if (!is_null($user) && ($user->isAdmin() || $user->isClubAdmin($club) || $user->isClubOwner($club) )){
            return true;
        }

        return false;
    }

    /**
     * Determine if the user can become a member of the club or un-memberize.
     *
     * @param  User $user
     * @param  Club $club
     * @return bool
     */
    public function requestAdmin(User $user, Club $club)
    {
        if (!is_null($user)){
            return true;
        }

        return false;
    }

    /**
     * Determine if the model can be created (creation form) by the user.
     *
     * @param  User $user
     * @param  Club $club
     * @return bool
     */
    public function create(User $user, Club $club)
    {
        return !is_null($user);
    }

    /**
     * Determine if the model can be created (in storage) by the user.
     *
     * @param  User $user
     * @param  Club $club
     * @return bool
     */
    public function store(User $user, Club $club)
    {
        return $user->isAdmin();
    }

    /**
     * Determine if the model can be viewed by the user.
     *
     * @param  User $user
     * @param  Club $club
     * @return bool
     */
    public function show($user, Club $club)
    {
        // everyone (even non-authenticated visitors) can show a published club ...
        if ($club->isPublished()){
            return true;
        }

        // unpublished clubs: the admin can show all clubs and the club owner can show his club
        if (!is_null($user) && ($user->isAdmin() || $user->isClubAdmin($club) || $user->isClubOwner($club) )){
            return true;
        }

        return false;
    }

    /**
     * Determine if the model can be edited (edit form) by the user.
     *
     * @param  User $user
     * @param  Club $club
     * @return bool
     */
    public function edit(User $user, Club $club)
    {
        return $this->defaultWritePolicy($user, $club);
    }

    /**
     * Determine if the model can be updated (in storage) by the user.
     *
     * @param  User $user
     * @param  Club $club
     * @return bool
     */
    public function update(User $user, Club $club)
    {
        return $this->defaultWritePolicy($user, $club);
    }

    /**
     * Determine if the model can be destroyed by the user.
     *
     * @param  User $user
     * @param  Club $club
     * @return bool
     */
    public function destroy(User $user, Club $club)
    {
        return $this->defaultWritePolicy($user, $club);
    }

    /**
     * Determine if the model can be destroyed by the user.
     *
     * @param  User $user
     * @param  Club $club
     * @return bool
     */
    public function delete(User $user, Club $club)
    {
        return $this->destroy($user, $club);
    }

    /**
     * Default write policy which applies to most cases: a user may perform an action on a model if it
     * belongs to his tenant. Separated out for DRY reasons.
     *
     * @param  User $user
     * @param  Club $club
     * @return bool
     */
    protected function defaultWritePolicy(User $user, Club $club)
    {
        // the admin can write all clubs ...
        if ($user->isAdmin()){
            return true;
        }

        // and the club owner can write his club, nobody else.
        if ($user->isClubAdmin($club) || $user->isClubOwner($club))
        {
            return true;
        }
    }
}
