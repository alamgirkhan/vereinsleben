<?php

namespace Vereinsleben\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Http\Request;

use Vereinsleben\Club;
use Vereinsleben\Event;
use Vereinsleben\User;

class EventPolicy
{
    use HandlesAuthorization;

    /**
     * The request object
     *
     * @var Illuminate\Http\Request
     */
    protected $request = null;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct(Request $request = null)
    {
        $this->request = $request;
    }

    /**
     * Determine if the user may index models. Depends on the model ownership, so make only
     * basic checks here, additional scope filtering needed.
     *
     * @param User $user
     * @return bool
     */
    public function index(User $user)
    {
        return true; # indexing is allowed in general
    }

    /**
     * Determine if the model can be created (creation form) by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Event $event
     * @return bool
     */
    public function create(User $user, Event $event)
    {
        return $this->defaultWritePolicy($user, $event);
    }

    /**
     * Determine if the model can be created (in storage) by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Event $event
     * @return bool
     */
    public function store(User $user, Event $event)
    {
        return $this->defaultWritePolicy($user, $event);
    }

    /**
     * Determine if the model can be viewed by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Event $event
     * @return bool
     */
    public function show($user, Event $event)
    {
        // everyone (even non-authenticated visitors) can show a published club ...
        if ($event->club !== null && $event->club->isPublished()) {
            return true;
        }

        // everyone (even non-authenticated visitors) can show all bandage event
        if ($event->bandage !== null){
            return true;
        }		

        // unpublished clubs: the admin can show all clubs and the club owner can show his club
        if (!is_null($user) && ($user->isAdmin() || ($event->club !== null && ($user->isClubOwner($event->club) || $user->isClubAdmin($event->club)  )) || $user->isBandageAdmin($event->bandage))) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the model can be edited (edit form) by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Event $event
     * @return bool
     */
    public function edit(User $user, Event $event)
    {
        return $this->defaultWritePolicy($user, $event);
    }

    /**
     * Determine if the model can be updated (in storage) by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Event $event
     * @return bool
     */
    public function update(User $user, Event $event)
    {
        return $this->defaultWritePolicy($user, $event);
    }

    /**
     * Determine if the model can be destroyed by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Event $event
     * @return bool
     */
    public function destroy(User $user, Event $event)
    {
        return $this->defaultWritePolicy($user, $event);
    }

    /**
     * Determine if the model can be destroyed by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Event $event
     * @return bool
     */
    public function delete(User $user, Event $event)
    {
        return $this->destroy($user, $event);
    }

    /**
     * Default write policy which applies to most cases: a user may perform an action on a model if it
     * belongs to his tenant. Separated out for DRY reasons.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Event $event
     * @return bool
     */
    protected function defaultWritePolicy(User $user, Event $event)
    {
        // the admin can write all clubs ...
        if ($user->isAdmin()) {
            return true;
        }

        // and the club owner can write his club, nobody else.
        return $event->club !== null && ($user->isClubOwner($event->club) || $user->isClubAdmin($event->club));
    }
}
