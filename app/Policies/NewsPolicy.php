<?php

namespace Vereinsleben\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Http\Request;

use Vereinsleben\News;
use Vereinsleben\User;

class NewsPolicy
{
    use HandlesAuthorization;

    /**
     * The request object
     *
     * @var Illuminate\Http\Request
     */
    protected $request = null;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct(Request $request = null)
    {
        $this->request = $request;
    }

    /**
     * Determine if the user may index models. Depends on the model ownership, so make only
     * basic checks here, additional scope filtering needed.
     *
     * @param User $user
     * @return bool
     */
    public function index(User $user)
    {
        return true; # indexing is allowed in general
    }

    /**
     * Determine if the model can be created (creation form) by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\News $news
     * @return bool
     */
    public function create(User $user, News $news)
    {
        return $this->defaultWritePolicy($user, $news);
    }

    /**
     * Determine if the model can be created (in storage) by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\News $news
     * @return bool
     */
    public function store(User $user, News $news)
    {
        return $this->defaultWritePolicy($user, $news);
    }

    /**
     * Determine if the model can be viewed by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\News $news
     * @return bool
     */
    public function show($user, News $news)
    {
        // everyone can show a published news ...
        if ($news->isPublished()) {
            return true;
        }

        // unpublished news: the admin can show all
        if (!is_null($user) && $user->isAdmin()) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the model can be edited (edit form) by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\News $news
     * @return bool
     */
    public function edit(User $user, News $news)
    {
        return $this->defaultWritePolicy($user, $news);
    }

    /**
     * Determine if the model can be updated (in storage) by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\News $news
     * @return bool
     */
    public function update(User $user, News $news)
    {
        return $this->defaultWritePolicy($user, $news);
    }

    /**
     * Determine if the model can be destroyed by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\News $news
     * @return bool
     */
    public function destroy(User $user, News $news)
    {
        return $this->defaultWritePolicy($user, $news);
    }

    /**
     * Determine if the model can be destroyed by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\News $news
     * @return bool
     */
    public function delete(User $user, News $news)
    {
        return $this->destroy($user, $news);
    }

    /**
     * Default write policy which applies to most cases: a user may perform an action on a model if it
     * belongs to his tenant. Separated out for DRY reasons.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\News $news
     * @return bool
     */
    protected function defaultWritePolicy(User $user, News $news)
    {
        // the admin can write all clubs ...
        if ($user->isAdmin()) {
            return true;
        }

        // ...nobody else.
        return false;
    }
}
