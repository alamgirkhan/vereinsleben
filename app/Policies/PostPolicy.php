<?php

namespace Vereinsleben\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Http\Request;

use Vereinsleben\Club;
use Vereinsleben\Post;
use Vereinsleben\User;

class PostPolicy
{
    use HandlesAuthorization;

    /**
     * The request object
     *
     * @var Illuminate\Http\Request
     */
    protected $request = null;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct(Request $request = null)
    {
        $this->request = $request;
    }

    /**
     * Determine if the user may index models. Depends on the model ownership, so make only
     * basic checks here, additional scope filtering needed.
     *
     * @param User $user
     * @return bool
     */
    public function index(User $user)
    {
        return true; # indexing is allowed in general
    }

    /**
     * Determine if the model can be created (creation form) by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Post $post
     * @return bool
     */
    public function create(User $user, Post $post)
    {
        return $this->defaultWritePolicy($user, $post);
    }

    /**
     * Determine if the model can be created (in storage) by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Post $post
     * @return bool
     */
    public function store(User $user, Post $post)
    {
        return $this->defaultWritePolicy($user, $post);
    }

    /**
     * Determine if the model can be viewed by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Post $post
     * @return bool
     */
    public function show($user, Post $post)
    {
        // everyone (even non-authenticated visitors) can show all user posts
        if ($post->user !== null && $post->club === null){
            return true;
        }

        // everyone (even non-authenticated visitors) can show all bandage posts
        if ($post->bandage !== null){
            return true;
        }

        // everyone (even non-authenticated visitors) can show a published club post ...
        if ($post->club !== null && $post->club->isPublished()) {
            return true;
        }

        // unpublished clubs: the admin can show all clubs and the club owner can show his club
        if (!is_null($user) && ($user->isAdmin() || ($post->club !== null && $user->isClubOwner($post->club)))) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the model can be edited (edit form) by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Post $post
     * @return bool
     */
    public function edit(User $user, Post $post)
    {
        return $this->defaultWritePolicy($user, $post);
    }

    /**
     * Determine if the model can be updated (in storage) by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Post $post
     * @return bool
     */
    public function update(User $user, Post $post)
    {
        return $this->defaultWritePolicy($user, $post);
    }

    /**
     * Determine if the model can be destroyed by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Post $post
     * @return bool
     */
    public function destroy(User $user, Post $post)
    {
        return $this->defaultWritePolicy($user, $post);
    }

    /**
     * Determine if the model can be destroyed by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Post $post
     * @return bool
     */
    public function delete(User $user, Post $post)
    {
        return $this->destroy($user, $post);
    }

    /**
     * Default write policy which applies to most cases: a user may perform an action on a model if it
     * belongs to his tenant. Separated out for DRY reasons.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Post $post
     * @return bool
     */
    protected function defaultWritePolicy(User $user, Post $post)
    {
        // the admin can write all posts ...
        if ($user->isAdmin()) {
            return true;
        }

        // and the club owner can write his club ...
        if ($post->club !== null && ($user->isClubAdmin($post->club) || $user->isClubOwner($post->club))) {
            return true;
        }

        // the user can write his own ...
        if ($post->user !== null && $post->user->id === $user->id) {
            return true;
        }

        // nobody else.
        return false;
    }
}
