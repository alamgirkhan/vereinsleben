<?php

namespace Vereinsleben\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Http\Request;

use Vereinsleben\Club;
use Vereinsleben\Sport;
use Vereinsleben\User;

class SportPolicy
{
    use HandlesAuthorization;

    /**
     * The request object
     *
     * @var Illuminate\Http\Request
     */
    protected $request = null;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct(Request $request = null)
    {
        $this->request = $request;
    }

    /**
     * Determine if the user may index models. Depends on the model ownership, so make only
     * basic checks here, additional scope filtering needed.
     *
     * @param User $user
     * @return bool
     */
    public function index(User $user)
    {
        return true; # indexing is allowed in general
    }

    /**
     * Determine if the model can be created (creation form) by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Sport $sport
     * @return bool
     */
    public function create(User $user, Sport $sport)
    {
        return $this->defaultWritePolicy($user, $sport);
    }

    /**
     * Determine if the model can be created (in storage) by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Sport $sport
     * @return bool
     */
    public function store(User $user, Sport $sport)
    {
        return $this->defaultWritePolicy($user, $sport);
    }

    /**
     * Determine if the model can be viewed by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Sport $sport
     * @return bool
     */
    public function show($user, Sport $sport)
    {
        // everyone (even non-authenticated visitors) can show all sports ...
        return true;
    }

    /**
     * Determine if the model can be edited (edit form) by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Sport $sport
     * @return bool
     */
    public function edit(User $user, Sport $sport)
    {
        return $this->defaultWritePolicy($user, $sport);
    }

    /**
     * Determine if the model can be updated (in storage) by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Sport $sport
     * @return bool
     */
    public function update(User $user, Sport $sport)
    {
        return $this->defaultWritePolicy($user, $sport);
    }

    /**
     * Determine if the model can be destroyed by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Sport $sport
     * @return bool
     */
    public function destroy(User $user, Sport $sport)
    {
        return $this->defaultWritePolicy($user, $sport);
    }

    /**
     * Determine if the model can be destroyed by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Sport $sport
     * @return bool
     */
    public function delete(User $user, Sport $sport)
    {
        return $this->destroy($user, $sport);
    }

    /**
     * Default write policy which applies to most cases: a user may perform an action on a model if it
     * belongs to his club. Separated out for DRY reasons.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Sport $sport
     * @return bool
     */
    protected function defaultWritePolicy(User $user, Sport $sport)
    {
        // the admin can write all sports ...
        if ($user->isAdmin()) {
            return true;
        }

        if (is_null($sport->club_id)) {
            return false;
        }

        $club = Club::findOrFail($sport->club_id);

        // and the club owner can create new sports, but nobody else.
        return ($user->isClubOwner($club) || $user->isClubAdmin($club) );
    }
}
