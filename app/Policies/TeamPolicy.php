<?php

namespace Vereinsleben\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Http\Request;

use Vereinsleben\Club;
use Vereinsleben\Team;
use Vereinsleben\User;

class TeamPolicy
{
    use HandlesAuthorization;

    /**
     * The request object
     *
     * @var Illuminate\Http\Request
     */
    protected $request = null;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct(Request $request = null)
    {
        $this->request = $request;
    }

    /**
     * Determine if the user may index models. Depends on the model ownership, so make only
     * basic checks here, additional scope filtering needed.
     *
     * @param User $user
     * @return bool
     */
    public function index(User $user)
    {
        return true; # indexing is allowed in general
    }

    /**
     * Determine if the model can be created (creation form) by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Team $team
     * @return bool
     */
    public function create(User $user, Team $team)
    {
        return $this->defaultWritePolicy($user, $team);
    }

    /**
     * Determine if the model can be created (in storage) by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Team $team
     * @return bool
     */
    public function store(User $user, Team $team)
    {
        return $this->defaultWritePolicy($user, $team);
    }

    /**
     * Determine if the model can be viewed by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Team $team
     * @return bool
     */
    public function show($user, Team $team)
    {
        // everyone (even non-authenticated visitors) can show a published club ...
        if ($team->club !== null && $team->club->isPublished()) {
            return true;
        }

        // unpublished clubs: the admin can show all clubs and the club owner can show his club
        if (!is_null($user) && ($user->isAdmin() || ($team->club !== null && ($user->isClubOwner($team->club) || $user->isClubAdmin($team->club) )))) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the model can be edited (edit form) by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Team $team
     * @return bool
     */
    public function edit(User $user, Team $team)
    {
        return $this->defaultWritePolicy($user, $team);
    }

    /**
     * Determine if the model can be updated (in storage) by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Team $team
     * @return bool
     */
    public function update(User $user, Team $team)
    {
        return $this->defaultWritePolicy($user, $team);
    }

    /**
     * Determine if the model can be destroyed by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Team $team
     * @return bool
     */
    public function destroy(User $user, Team $team)
    {
        return $this->defaultWritePolicy($user, $team);
    }

    /**
     * Determine if the model can be destroyed by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Team $team
     * @return bool
     */
    public function delete(User $user, Team $team)
    {
        return $this->destroy($user, $team);
    }

    /**
     * Default write policy which applies to most cases: a user may perform an action on a model if it
     * belongs to his tenant. Separated out for DRY reasons.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Team $team
     * @return bool
     */
    protected function defaultWritePolicy(User $user, Team $team)
    {
        // the admin can write all clubs ...
        if ($user->isAdmin()) {
            return true;
        }

        // and the club owner can write his club, nobody else.
        return $team->club !== null && ($user->isClubOwner($team->club) || $user->isClubAdmin($team->club) );
    }
}
