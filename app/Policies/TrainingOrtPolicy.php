<?php

namespace Vereinsleben\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Http\Request;

use Vereinsleben\Club;
use Vereinsleben\Trainingsort;
use Vereinsleben\User;

class TrainingOrtPolicy
{
    use HandlesAuthorization;

    /**
     * @var Request|null
     */
    protected $request = null;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct(Request $request = null)
    {
        $this->request = $request;
    }

    /**
     * Determine if the user may index models. Depends on the model ownership, so make only
     * basic checks here, additional scope filtering needed.
     *
     * @param User $user
     * @return bool
     */
    public function index(User $user)
    {
        return true; # indexing is allowed in general
    }


    /**
     * @param User $user
     * @param Club $club
     * @return bool
     */
    public function store(User $user, Club $club)
    {
        return $this->defaultWritePolicy($user, $club);
    }

    /**
     * @param User $user
     * @param Trainingsort $trainingsort
     * @return bool
     */
    protected function defaultWritePolicy(User $user, Club $club)
    {
        // the admin can write all clubs ...
        if ($user->isAdmin()){
            return true;
        }

        // and the club owner can write his club, nobody else.
        return ($user->isClubOwner($club) || $user->isClubAdmin($club) );
    }
}
