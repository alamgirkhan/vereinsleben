<?php

namespace Vereinsleben\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Http\Request;

use Vereinsleben\User;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * The request object
     *
     * @var Illuminate\Http\Request
     */
    protected $request = null;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct(Request $request = null)
    {
        $this->request = $request;
    }

    /**
     * Determine if the user may index models. Depends on the model ownership, so make only
     * basic checks here, additional scope filtering needed.
     *
     * @return bool
     */
    public function index()
    {
        return false;
    }

    /**
     * Determine if the model can be created (creation form) by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\User $targetUser
     * @return bool
     */
    public function create(User $user, User $targetUser)
    {
        return true;
    }

    /**
     * Determine if the model can be created (in storage) by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\User $targetUser
     * @return bool
     */
    public function store(User $user, User $targetUser)
    {
        return true;
    }

    /**
     * Determine if the model can be viewed by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\User $targetUser
     * @return bool
     */
    public function show($user, User $targetUser)
    {
        return true; // everyone can show everyones' user profile
    }

    /**
     * Determine if the model can be viewed by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\User $targetUser
     * @return bool
     */
    public function showProfile($user, User $targetUser)
    {

        // the admin can view user's complete profile
        if ($user->isAdmin()){
            return true;
        }

        // if user friend with other friend can view complete profile
        if ($user->isFriendWith($targetUser)){
            return true;
        }

        // and the user itself can chang himself, nobody else.
        return $user->id === $targetUser->id;
    }	

    /**
     * Determine if the model can be edited (edit form) by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\User $targetUser
     * @return bool
     */
    public function edit(User $user, User $targetUser)
    {
        return $this->defaultWritePolicy($user, $targetUser);
    }

    /**
     * Determine if the model can be updated (in storage) by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\User $targetUser
     * @return bool
     */
    public function update(User $user, User $targetUser)
    {
        return $this->defaultWritePolicy($user, $targetUser);
    }

    /**
     * Determine if the model can be updated (in storage) by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\User $targetUser
     * @return bool
     */
    public function updateState(User $user, User $targetUser)
    {
        return $this->defaultWritePolicy($user, $targetUser);
    }

    /**
     * Determine if the model can be destroyed by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\User $targetUser
     * @return bool
     */
    public function destroy(User $user, User $targetUser)
    {
        return $this->defaultWritePolicy($user, $targetUser);
    }

    /**
     * Determine if the model can be destroyed by the user.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\User $targetUser
     * @return bool
     */
    public function delete(User $user, User $targetUser)
    {
        return $this->destroy($user, $user);
    }

    /**
     * Determine if the user can become a fan of the club or un-fan.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Club $club
     * @return bool
     */
    public function updateFriendship(User $user)
    {
        if (!is_null($user)){
            return true;
        }

        return false;
    }

    /**
     * Determine if the user can become a fan of the club or un-fan.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\Club $club
     * @return bool
     */
    public function createFriendship(User $user)
    {
        if (!is_null($user)){
            return true;
        }

        return false;
    }

    /**
     * Default write policy which applies to most cases: a user may perform an action on a model if it
     * belongs to his tenant. Separated out for DRY reasons.
     *
     * @param  \Mit\User $user
     * @param  Vereinsleben\User $targetUser
     * @return bool
     */
    protected function defaultWritePolicy(User $user, User $targetUser)
    {
        // the admin can write all clubs ...
        if ($user->isAdmin()) {
            return true;
        }

        // and the user itself can chang himself, nobody else.
        return $user->id === $targetUser->id;
    }
}
