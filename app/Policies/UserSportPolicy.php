<?php

namespace Vereinsleben\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

use Vereinsleben\User;
use Vereinsleben\UserSport;

class UserSportPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
}
