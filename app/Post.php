<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

/**
 * Vereinsleben\Post
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $content_raw
 * @property string|null $content
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $published_at
 * @property string|null $published_from
 * @property string|null $published_to
 * @property string|null $state
 * @property int|null $club_id
 * @property int|null $user_id
 * @property int|null $gallery_id
 * @property string|null $m_club
 * @property string|null $m_id
 * @property string|null $m_gallery
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Vereinsleben\Club|null $club
 * @property-read \Illuminate\Database\Eloquent\Collection|\Vereinsleben\Image[] $images
 * @property-read \Vereinsleben\User|null $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\Vereinsleben\Video[] $videos
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\Post onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Post public ()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Post whereClubId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Post whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Post whereContentRaw($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Post whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Post whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Post whereGalleryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Post whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Post whereMClub($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Post whereMGallery($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Post whereMId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Post wherePublishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Post wherePublishedFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Post wherePublishedTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Post whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Post whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Post whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Post whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\Post withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\Post withoutTrashed()
 * @mixin \Eloquent
 */
class Post extends Model
{
    use SoftDeletes;

    const STATE_NEW     = 'NEW';
    const STATE_DELETED = 'DELETED';

    protected $table = 'post';
    protected $morphClass = 'post';

    protected $fillable = [
        'title',
        'content',
        'content_raw',
        'published_from',
        'published_to',
        'state',
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * The images of this post
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function images()
    {
        return $this->morphMany('Vereinsleben\Image', 'imageable');
    }

    /**
     * The videos of this post
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function videos()
    {
        return $this->morphMany('Vereinsleben\Video', 'videoable');
    }

    /**
     * The club this post belongs to. (Either belongs to a club or user, should not! both)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function club()
    {
        return $this->belongsTo('Vereinsleben\Club', 'club_id');
    }

    /**
     * The user this post belongs to. (Either belongs to a club or user, should not! both)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('Vereinsleben\User', 'user_id');
    }

    /**
     * The bandage this post belongs to. (Either belongs to a bandage or user, should not! both)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bandage()
    {
        return $this->belongsTo('Vereinsleben\Bandage', 'bandage_id');
    }

    /**
     * A scope to retrieve public posts.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublic($query)
    {
        return $query
            ->whereNotNull('published_at')
            ->where(function($query){
                $query
                    ->where(function($query){
                        $query
                            ->where('published_from', '<=', Carbon::now())
                            ->where('published_to', '>=', Carbon::now());
                    })
                    ->orWhere(function($query){
                        $query
                            ->where('published_from', '<=', Carbon::now())
                            ->whereNull('published_to');
                    })
                    ->orWhere(function($query){
                        $query
                            ->where('published_to', '>=', Carbon::now())
                            ->whereNull('published_from');
                    })
                    ->orWhere(function($query){
                        $query
                            ->whereNull('published_to')
                            ->whereNull('published_from');
                    });
            });
    }
}