<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;

class ProposeSong extends Model
{
	protected $table = 'propose_song';
}
