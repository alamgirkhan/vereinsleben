<?php

namespace Vereinsleben\Providers;

use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Hashing\HashManager;
use Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    protected $mogelmail_link = 'https://www.mogelmail.de/q/';
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('mogelmail', function($attribute, $value, $paramenters, $validator) {
            $validator = Validator::make(array($attribute => $value), array($attribute => 'email'));

            if($validator->passes()) {
                foreach ( $validator->getData() as $attribute => $value ) {
                    $mailArray = explode('@', $value);
                    $domain = $this->mogelmail_link . $mailArray[1];

                    $curl = curl_init();
                    curl_setopt($curl, CURLOPT_URL, $domain);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true );

                    $isFake = intval(curl_exec($curl));

                    curl_close($curl);

                    $validator->setCustomMessages(array(
                        'validation.' . $attribute => 'Die E-Mail-Adresse ist ungültig. Bitte gebe eine gültige E-Mail-Adresse an.'
                    ));

                    if($isFake) {
                        return false;
                    }
                }

                return true;
            } else {
                return false;
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->singleton(Hasher::class, HashManager::class);

        if ($this->app->environment() === 'development') {
//            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }
}
