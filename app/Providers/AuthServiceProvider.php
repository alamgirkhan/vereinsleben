<?php

namespace Vereinsleben\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'Vereinsleben\Club'            => 'Vereinsleben\Policies\ClubPolicy',
        'Vereinsleben\ClubAchievement' => 'Vereinsleben\Policies\ClubAchievementPolicy',
        'Vereinsleben\Event'           => 'Vereinsleben\Policies\EventPolicy',
        'Vereinsleben\News'            => 'Vereinsleben\Policies\NewsPolicy',
        'Vereinsleben\Post'            => 'Vereinsleben\Policies\PostPolicy',
        'Vereinsleben\Sport'           => 'Vereinsleben\Policies\SportPolicy',
        'Vereinsleben\Team'            => 'Vereinsleben\Policies\TeamPolicy',
        'Vereinsleben\User'            => 'Vereinsleben\Policies\UserPolicy',
        'Vereinsleben\Bandage'         => 'Vereinsleben\Policies\BandagePolicy',
        'Vereinsleben\Trainingsort'    => 'Vereinsleben\Policies\TrainingOrtPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);
    }
}
