<?php

namespace Vereinsleben\Providers;

use Vereinsleben\Sha256Hasher;
use Illuminate\Hashing\HashServiceProvider;

class AuthUserProvider extends HashServiceProvider
{
    public function register()
    {
        $this->app->singleton('hash', function () {
            return new Sha256Hasher;
        });
    }
}
