<?php

namespace Vereinsleben\Providers;

use Auth;
use Illuminate\Support\ServiceProvider;
use View;
use Vereinsleben\State;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        /*View::composer('*', function($view){
            $federal_states = State::all();
            $sessionState = session('federalState') !== null
                                    ? State::find(session('federalState'))
                                    : State::where('name', 'Rheinland-Pfalz')->firstOrFail();

            $federalState = Auth::user() ? Auth::user()->desiredState() : $sessionState;

            $view->with(compact('federal_states', 'sessionState' , 'federalState'));
        });*/
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
