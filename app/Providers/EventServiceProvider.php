<?php

namespace Vereinsleben\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

use Vereinsleben\User;
use Vereinsleben\Event;
use Vereinsleben\Helpers\GeocodeHelper;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Illuminate\Auth\Events\Login'  => [
            'Vereinsleben\Listeners\LogSuccessfulLogin',
        ],
        'Vereinsleben\Events\ChatEvent' => [
            'Vereinsleben\Listeners\ChatListener',
        ]

    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher $events
     * @return void
     */
    public function boot( )
    {
        parent::boot();

        User::saving(function ($user) {
            $coordinates = GeocodeHelper::geocodeEntity($user);
            
            $user->lat = $coordinates['lat'];
            $user->lng = $coordinates['lng'];

            return true;
        });
    }
}
