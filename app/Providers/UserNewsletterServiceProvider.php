<?php

namespace Vereinsleben\Providers;

use Illuminate\Support\ServiceProvider;
use Vereinsleben\User;
use Vereinsleben\Helpers\VenusNewsletter;

class UserNewsletterServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /*User::saved(function ($user) {
            // sync the user to Venus when the newsletter subscription flag changed
            // and is set.
            // TODO: we should also sync when it changed (to zero), but never when the
            // user just registered and it is zero
            if (
                $user->getOriginal('newsletter') != $user->newsletter &&
                $user->newsletter == 1
            ) {
                self::handleSubscription($user);
            }
        });*/

        /*User::created(function ($user) {
            if($user->newsletter == 1) {
                self::handleSubscription($user);
            }
            \Log::info('user created. into newsletter subscription');
        });*/

        /*User::updated(function ($user) {
            self::handleUpdate($user);

            \Log::info('user updated. update newsletter subscription');
        });*/
    }

    protected static function handleSubscription(User $user)
    {
        try {
            $newsletterConfig = config('newsletter');
            if (isset($newsletterConfig['auth_client_id']) && $newsletterConfig['auth_client_id'] !== null) {
                $venus = new VenusNewsletter($newsletterConfig);
                $venus->subscribeUser($user);
            }
        } catch (\Exception $e) {
            // fail silently and capture the exception; we do not want to bother the user with this.
            app('log')->error('Venus newsletter: Failed to register e-mail '.$user->email.', error: '.$e->getMessage());
            //app('sentry')->captureException($e);
        }
    }

    protected static function handleUpdate(User $user)
    {
        try {
            $newsletterConfig = config('newsletter');
            if (isset($newsletterConfig['auth_client_id']) && $newsletterConfig['auth_client_id'] !== null) {
                $venus = new VenusNewsletter($newsletterConfig);
                $venus->updateUser($user);
            }
        } catch (\Exception $e) {
            // fail silently and capture the exception; we do not want to bother the user with this.
            app('log')->error('Venus newsletter: Failed to update e-mail '.$user->email.', error: '.$e->getMessage());
            //app('sentry')->captureException($e);
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
