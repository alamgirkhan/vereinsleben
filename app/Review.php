<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
	protected $table = 'reviews';
	
	public function user()
	{
		return $this->belongsTo(User::class);
	}
	
	public function club()
	{
		return $this->belongsTo(Club::class);
	}
	
	public function children()
	{
		return $this->hasMany(Review::class, 'parent_id', 'id');
	}
	
	public function reviewReportByUserId($user_id)
	{
		return $this->hasOne(ReviewReport::class, 'review_id', 'id')->where('user_id', $user_id);
	}
}
