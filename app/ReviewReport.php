<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;

class ReviewReport extends Model
{
	protected $table = 'reviews_report';
	
	public function review()
	{
		return $this->belongsTo(Review::class);
	}
	
	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
