<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

    const USER               = 'USER';
    const ADMIN              = 'ADMIN';
    const CLUB_OWNER         = 'CLUB_OWNER';
    const CLUB_ADMIN         = 'CLUB_ADMIN';
    const CLUB_ADMIN_REQUEST = 'CLUB_ADMIN_REQUEST';
    const CLUB_MEMBER        = 'CLUB_MEMBER';
    const CLUB_FAN           = 'CLUB_FAN';
    const BANDAGE_SUBSCRIBE  = 'BAND_SUBSCRIBE';
    const BANDAGE_ADMIN      = 'BAND_ADMIN';
    const SUB_ADMINISTRATOR  = 'SUB_ADMIN';
    const CONTENT_MANAGER    = 'CON_MANAGER';
    const EDITOR             = 'EDITOR';


    protected $table = 'role';

    /**
     * @param $constant
     * @return mixed id
     */
    public function getByConstant($constant)
    {
        return $this->where('constant', $constant)->select('id')->firstOrFail();
    }
}
