<?php


namespace Vereinsleben\Services;

use Dompdf\Exception;
use Geocoder;

class GeocoderHelper
{

    private $geocode;
    private $city;
    private $land;
    private $address;
    private $street;
    private $housNr;


    function __construct($land = '')
    {
        $this->land = $land;
    }

    function initGeocode($address)
    {
        $this->geocode = Geocoder::geocode($address);

    }
    function setGeocode($geocode){
        $this->geocode = $geocode;
    }

    function getAddress()
    {

        return $this->address;
    }

    function getFederalState()
    {
        if ($this->geocode->get()->first()->getAdminLevels()->first() !== null){

            $federalState = $this->geocode->get()->first()->getAdminLevels()->first()->getName();
            return in_array($federalState, $this->statesInEnglish) ? $federalState : 'Rheinland-Pfalz';
        }

        return 'Rheinland-Pfalz';
    }

    function getLat()
    {

        return $this->geocode->get()->first()->getCoordinates()->getLatitude();
    }

    function getLon()
    {

        return $this->geocode->get()->first()->getCoordinates()->getLongitude();
    }

    function getFederalStateInGerman()
    {

        return str_replace($this->statesInEnglish, $this->statesInGerman, $this->getFederalState());
    }


    function setAddress($zip, $city, $street='', $houseNr='')
    {
        $this->city = $city;
        $this->zip = $zip;
        $this->street = $street;
        $this->housNr = $houseNr;
        $this->address = $this->street . " " . $this->housNr . " " .  $this->zip . " " . $this->city . ", " . $this->land;
        $this->initGeocode($this->address);
    }

    function setIpAddress($ipAddress){
        $this->initGeocode($ipAddress);
    }

    private $statesInGerman = [
        'Nordrhein-Wesfalen',
        'Niedersachsen',
        'Bayern',
        'Reinland-Pfalz',
        'Hessen',
        'Saarland',
        'Berlin',
        'Brandenburg',
        'Schleswig-Holstein',
        'Mecklenburg-Vorpommern',
        'Thüringen',
        'Sachsen',
        'Sachsen-Anhalt',
        'Bremen',
        'Baden-Württemberg',
        'Hamburg',
    ];

    private $statesInEnglish = [
        'North Rhine-Westphalia',
        'Lower Saxony',
        'Bavaria',
        'Rhineland-Palatinate',
        'Hesse',
        'Saarland',
        'Berlin',
        'Brandenburg',
        'Schleswig-Holstein',
        'Mecklenburg-Vorpommern',
        'Thuringia',
        'Sachsen',
        'Saxony-Anhalt',
        'Bremen',
        'Baden-Württemberg Region',
        'Hamburg',
    ];


}