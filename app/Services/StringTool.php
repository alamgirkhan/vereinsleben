<?php


namespace Vereinsleben\Services;

//that is just an example for later to make classes an test of them
class StringTool
{
    var $str;

    function __construct($str)
    {
        $this->str = $str;
    }

    public function cleanState()
    {
        $this->str = 'cleaned';

        return $this;
    }

    public function toString()
    {

        return $this->str;
    }
}