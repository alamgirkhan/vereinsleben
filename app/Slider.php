<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait as StaplerEloquentTrait;

class Slider extends Model implements StaplerableInterface
{
    use StaplerEloquentTrait;

    protected $table = 'slider';
    protected $morphClass = 'slider';


    // fillable via mass assignment
    protected $fillable = [
        'title',
        'description',
        'h2',
        'h3',
        'btn',
        'link',
        'logo1',
        'logo2',
        'logo3',
        'logo4',
        'logo5',
        'logo6',
        'slider_image',
        'published',
    ];

    /**
     * Constructor
     *
     * @return Slider
     */
    public function __construct(array $attributes = array())
    {
        $this->hasAttachedFile('slider_image', [
            'url'         => '/uploaded/slider/:id/:attachment/:style-:secure_hash.:extension',
            'default_url' => '/default_images/slider/default-:attachment-:style.jpg',
        ]);
        $this->hasAttachedFile('logo1', [
            'url'         => '/uploaded/slider/:id/:attachment/:style-:secure_hash.:extension',
            'default_url' => '/default_images/slider/logo1-:attachment-:style.jpg',
        ]);
        $this->hasAttachedFile('logo2', [
            'url'         => '/uploaded/slider/:id/:attachment/:style-:secure_hash.:extension',
            'default_url' => '/default_images/slider/logo2-:attachment-:style.jpg',
        ]);
        $this->hasAttachedFile('logo3', [
            'url'         => '/uploaded/slider/:id/:attachment/:style-:secure_hash.:extension',
            'default_url' => '/default_images/slider/logo3-:attachment-:style.jpg',
        ]);
        $this->hasAttachedFile('logo4', [
            'url'         => '/uploaded/slider/:id/:attachment/:style-:secure_hash.:extension',
            'default_url' => '/default_images/slider/logo4-:attachment-:style.jpg',
        ]);
        $this->hasAttachedFile('logo5', [
            'url'         => '/uploaded/slider/:id/:attachment/:style-:secure_hash.:extension',
            'default_url' => '/default_images/slider/logo5-:attachment-:style.jpg',
        ]);
        $this->hasAttachedFile('logo6', [
            'url'         => '/uploaded/slider/:id/:attachment/:style-:secure_hash.:extension',
            'default_url' => '/default_images/slider/logo6-:attachment-:style.jpg',
        ]);

        parent::__construct($attributes);
    }


    public function sliderImages()
    {
        //$sliderImages = DB::table($this->table)->get();
        //return $sliderImages;
    }


    public function sliderByState($state)
    {
        $slider = $this->where('published', '=', 1)->byState($state)->get()->sortBy('position');

        return $slider;
    }


    function scopeByState($query, $state)
    {
        return $query->whereHas('states', function($q) use ($state){
            $q->where('state_id', $state->id);
        });
    }

    public function states()
    {
        return $this->morphToMany('Vereinsleben\State', 'stateable');
    }
}