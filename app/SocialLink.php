<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;

class SocialLink extends Model
{
    protected $table = 'social_link';

    public $timestamps = false;

    public static $linkTypes = [
        'web' => 'Web',
        'facebook' => 'Facebook',
        'twitter' => 'Twitter',
        'googleplus' => 'Google+',
        'youtube' => 'YouTube',
        'instagram' => 'Instagram',
        'flickr' => 'Flickr',
    ];

    public function linkable()
    {
        return $this->morphTo();
    }
}
