<?php

namespace Vereinsleben;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\SoftDeletes;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait as StaplerEloquentTrait;
use DB;

class Spitzensport extends Model implements StaplerableInterface
{
    use SoftDeletes;
    use StaplerEloquentTrait;

    const STATE_DRAFT     = 'DRAFT';
    const STATE_HIDDEN    = 'HIDDEN';
    const STATE_PUBLISHED = 'PUBLISHED';

    protected $morphClass = 'spitzensports';

    protected $guarded = [];

    /**
     * Constructor
     *
     * @return Spitzensport
     */
    public function __construct(array $attributes = array())
    {
        $this->hasAttachedFile('main_image', [
            'styles'      => [
                'startpage'  => '360',
                'singleview' => '850',
            ],
            'url'         => '/uploaded/spitzensports/:id/:attachment/:style-:secure_hash.:extension',
            'default_url' => '/default_images/news/default-:attachment-:style.jpg',
        ]);

        parent::__construct($attributes);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function meta()
    {
        return $this->morphOne('Vereinsleben\Meta', 'metaable');
    }

    public function states()
    {
        return $this->morphToMany('Vereinsleben\State', 'stateable');
    }

    public function tags()
    {
        return $this->morphToMany('Vereinsleben\Tag', 'taggable');
    }

    /**
     * The images which belong to this spitzensport
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function images()
    {
        return $this->morphMany('Vereinsleben\Image', 'imageable');
    }
    
    public function user()
    {
        return $this->belongsTo('Vereinsleben\User', 'user_id');
    }

    /**
     * The category this spitzensport belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('Vereinsleben\NewsCategory', 'category_id');
    }

    /**
     * spitzensport with user content manager
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function userContentManager()
    {
        $role = new Role();
        return $this->users()
            ->wherePivot('role_id', $role->getByConstant(Role::CONTENT_MANAGER)->id);
    }

    /**
     * The users with roles in this spitzensport.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        // public function roles()
        // {
        //     return $this->belongsToMany('App\Role', 'role_user_table', 'user_id', 'role_id');
        // }
        return $this->belongsToMany('Vereinsleben\User', 'user_news_role', 'user_id', 'news_id')
            ->withPivot('role_id');
    }

    /**
     * A scope to retrieve public news.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublic($query)
    {
        return $query
            ->where('state', '=', 'PUBLISHED')
            ->where(function($query){
                $query
                    ->where(function($query){
                        $query
                            ->where('published_from', '<=', Carbon::now())
                            ->where('published_until', '>=', Carbon::now());
                    })
                    ->orWhere(function($query){
                        $query
                            ->where('published_from', '<=', Carbon::now())
                            ->whereNull('published_until');
                    })
                    ->orWhere(function($query){
                        $query
                            ->where('published_until', '>=', Carbon::now())
                            ->whereNull('published_from');
                    })
                    ->orWhere(function($query){
                        $query
                            ->whereNull('published_until')
                            ->whereNull('published_from');
                    });
            });
    }

    // // Following added just for testing
    // /**
    //  * Check if the news is published
    //  *
    //  * @return boolean
    //  */
    // public function isPublished()
    // {
    //     return $this->state === self::STATE_PUBLISHED;
    // }

    function scopeByState($query, $state){
        return $query->whereHas('states', function($q) use ($state){
//            $q->where('state_id', $state->id);
            $q->where('state_id', $state->id);
        });
    }
}
