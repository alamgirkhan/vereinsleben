<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Vereinsleben\Sport
 *
 * @property int $id
 * @property string|null $title
 * @property int|null $club_id
 * @property string|null $m_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int $reviewed
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Vereinsleben\Club|null $club
 * @property-read \Illuminate\Database\Eloquent\Collection|\Vereinsleben\Club[] $clubs
 * @property-read \Illuminate\Database\Eloquent\Collection|\Vereinsleben\User[] $users
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\Sport onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Sport whereClubId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Sport whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Sport whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Sport whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Sport whereMId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Sport whereReviewed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Sport whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Sport whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\Sport withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\Sport withoutTrashed()
 * @mixin \Eloquent
 */
class Sport extends Model
{
    use SoftDeletes;

    const STATUS_REVIEWED = 1;

    protected $table = 'sport';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $fillable = [
        'title',
        'm_id',
        'reviewed'
    ];

    /**
     * The sport can belong to a club (in this case it is a specific sport created by the club).
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function club()
    {
        return $this->belongsTo('Vereinsleben\Club', 'club_id');
    }

    /**
     * The users of this sport.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('Vereinsleben\User', 'user_sport');
    }

    /**
     * The clubs of this sport.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function clubs()
    {
        return $this->belongsToMany('Vereinsleben\Club', 'club_sport');
    }
}
