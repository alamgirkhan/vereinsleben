<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;

/**
 * Vereinsleben\SportCareer
 *
 * @property int $id
 * @property string $title
 * @property string|null $content
 * @property string|null $timeline_begin
 * @property string|null $timeline_end
 * @property string|null $layout
 * @property int $user_id
 * @property int|null $club_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Vereinsleben\Club|null $club
 * @property-read \Illuminate\Database\Eloquent\Collection|\Vereinsleben\Image[] $images
 * @property-read \Illuminate\Database\Eloquent\Collection|\Vereinsleben\Sport[] $sports
 * @property-read \Vereinsleben\User $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\SportCareer onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\SportCareer whereClubId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\SportCareer whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\SportCareer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\SportCareer whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\SportCareer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\SportCareer whereLayout($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\SportCareer whereTimelineBegin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\SportCareer whereTimelineEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\SportCareer whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\SportCareer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\SportCareer whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\SportCareer withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\SportCareer withoutTrashed()
 * @mixin \Eloquent
 */
class SportCareer extends Model
{
    use SoftDeletes;

    const DAY_INDEX = 2;
    const MONTH_INDEX = 1;
    const YEAR_INDEX = 0;

    protected $table = 'sport_career';
    protected $morphClass = 'sport_career';
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'user_id',
        'club_id',
        'content',
        'layout',
        'title',
        'timeline_begin',
        'timeline_end',
    ];

    /**
     * The user this sport career is associated with
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('Vereinsleben\User', 'user_id');
    }

    /**
     * The club this sport career is associated with
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function club()
    {
        return $this->belongsTo('Vereinsleben\Club', 'club_id');
    }

    /**
     * The images of this sport career
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function images()
    {
        return $this->morphMany('Vereinsleben\Image', 'imageable');
    }

    /**
     * The sports of this sport career
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function sports()
    {
        return $this->belongsToMany('Vereinsleben\Sport', 'sport_career_sport');
    }

    public function hasDate()
    {

        return ($this->timeline_end !== null && trim($this->timeline_end) !== '') || ($this->timeline_begin !== null && trim($this->timeline_begin) !== '');
    }

    /**
     * formats the date matching forms input and fitting a consistent DB structure
     *
     * @param $dateString
     * @param string $separator
     * @param string $glue
     * @return null|string formatted date
     */
    public function formatDbDate($dateString, $separator = '.', $glue = '-')
    {
        $date = null;

        if ($dateString !== null && trim($dateString) !== '') {
            // check if date is year only
            if (strpos($dateString, $separator) === false && strlen(trim($dateString)) === 4) {
                $date = $dateString;
            } else {
                // date has month or day
                $dateArr = explode($separator, $dateString);

                // date has...
                switch ($dateArr) {
                    // month & year
                    case 2:
                        $date = implode($glue, [$dateArr[self::MONTH_INDEX], $dateArr[self::YEAR_INDEX]]);
                        break;
                    // day, month & year
                    case 3:
                        $date = implode($glue, [$dateArr[self::DAY_INDEX], $dateArr[self::MONTH_INDEX], $dateArr[self::YEAR_INDEX]]);
                        break;
                }
            }
        }

        return $date;
    }

    /**
     * Create a formatted date
     *
     * @return null|string
     */
    public function getFormattedDate()
    {
        $date = 'Aktuell';

        if ($this->hasDate()) {
            if (isset($this->timeline_begin) && !empty(trim($this->timeline_begin))) {
                $date = $this->formatDbDate($this->timeline_begin, '-', '.');
                $date .= ' - ';

                if (isset($this->timeline_end) && !empty(trim($this->timeline_end))) {
                    $date .= $this->formatDbDate($this->timeline_end, '-', '.');
                } else {
                    $date .= 'Heute';
                }
            }
        }

        return $date;
    }

    /**
     * Create a formatted time period
     *
     * @return null|string
     */
    public function getTimePeriod()
    {
        $difference = null;

        if (!empty($this->timeline_begin)) {

            $default = array_fill(0, 3, '1');

            $timelineStart = $this->timeline_begin;
            $timelineEnd = !empty($this->timeline_end) ? $this->timeline_end : Carbon::now()->toDateString();

            list($startYear, $startMonth, $startDay) = array_merge(explode('-', $timelineStart), $default);
            list($endYear, $endMonth, $endDay) = array_merge(explode('-', $timelineEnd), $default);

            $startDate = Carbon::create($startYear, $startMonth, $startDay);
            $endDate = Carbon::create($endYear, $endMonth, $endDay);

            if ($endDate->diffInMonths($startDate) >= 12) {
                $difference = $endDate->diffInYears($startDate);
                $difference .= ($difference > 1) ? ' Jahre' : ' Jahr';
            } elseif ($endDate->diffInMonths($startDate) === 0) {
                $difference = $endDate->diffInDays($startDate);
                $difference .= ($difference > 1) ? ' Tage' : ' Tag';
            } else {
                $difference = $endDate->diffInMonths($startDate);
                $difference .= ($difference > 1) ? ' Monate' : ' Monat';
            }
        }

        return $difference;
    }

}
