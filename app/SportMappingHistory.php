<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Vereinsleben\SportMappingHistory
 *
 * @property int $id
 * @property int $legacy_sport_id
 * @property int $new_sport_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\SportMappingHistory onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\SportMappingHistory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\SportMappingHistory whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\SportMappingHistory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\SportMappingHistory whereLegacySportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\SportMappingHistory whereNewSportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\SportMappingHistory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\SportMappingHistory withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\SportMappingHistory withoutTrashed()
 * @mixin \Eloquent
 */
class SportMappingHistory extends Model
{
    use SoftDeletes;

    protected $table = 'sport_mapping_history';

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $fillable = [
        'legacy_sport_id',
        'new_sport_id'
    ];
}
