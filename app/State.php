<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $fillable = ['name'];

    public function news()
    {
        return $this->morphedByMany('Vereinsleben\News', 'stateable');
    }

    public function users()
    {
        return $this->morphedByMany('Vereinsleben\Users', 'stateable');
    }

    public function sliders()
    {
        return $this->morphedByMany('Vereinsleben\Slider', 'stateable');
    }
    
    public function campaigns()
    {
        return $this->morphedByMany('Vereinsleben\Campaign', 'stateable');
    }

    public function partner()
    {
        return $this->morphedByMany('Vereinsleben\Partner', 'stateable');
    }

    // public function spitzensports()
    // {
    //     return $this->morphedByMany('Vereinsleben\Spitzensport', 'stateable');
    // }
}
