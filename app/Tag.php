<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = ['name'];

    public function news()
    {
        return $this->morphedByMany('Vereinsleben\News', 'taggable');
    }
}
