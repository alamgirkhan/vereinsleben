<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait as StaplerEloquentTrait;

/**
 * Vereinsleben\Team
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $description
 * @property string|null $contact_name
 * @property string|null $contact_email
 * @property string|null $m_club
 * @property string|null $m_sport
 * @property string|null $m_id
 * @property int|null $club_id
 * @property int|null $club_sport_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property string|null $picture_file_name
 * @property int|null $picture_file_size
 * @property string|null $picture_content_type
 * @property string|null $picture_updated_at
 * @property-read \Vereinsleben\Club|null $club
 * @property-read \Vereinsleben\ClubSport|null $club_sport
 * @property-read \Illuminate\Database\Eloquent\Collection|\Vereinsleben\User[] $users
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\Team onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Team whereClubId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Team whereClubSportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Team whereContactEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Team whereContactName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Team whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Team whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Team whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Team whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Team whereMClub($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Team whereMId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Team whereMSport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Team whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Team wherePictureContentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Team wherePictureFileName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Team wherePictureFileSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Team wherePictureUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Team whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\Team withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\Team withoutTrashed()
 * @mixin \Eloquent
 */
class Team extends Model implements StaplerableInterface
{
    use StaplerEloquentTrait;
    use SoftDeletes;

    protected $table = 'team';

    protected $fillable = [
        'name',
        'description',
        'contact_name',
        'contact_email',
        'league',
        'age_class',
        'picture',
    ];

    protected $dates = ['deleted_at'];

    /**
     * Constructor
     *
     * @return Club
     */
    public function __construct(array $attributes = array())
    {
        $this->hasAttachedFile('picture', [
            'styles' => [
                'singleView' => '840',
            ],
            'url' => '/uploaded/clubteam/:id/:attachment/:style-:secure_hash.:extension',
            'default_url' => '/default_images/clubteam/default-:attachment-:style.jpg',
        ]);

        parent::__construct($attributes);
    }

    /**
     * The club team belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function club()
    {
        return $this->belongsTo('Vereinsleben\Club', 'club_id');
    }

    /**
     * The club team belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function club_sport()
    {
        return $this->belongsTo('Vereinsleben\ClubSport', 'club_sport_id');
    }

    /**
     * Sports of this team.
     *
     * @return \Illuminate\Database\Eloquent\Relations\Relation
     */
    public function sport()
    {
        // does not work with hasManyThrough relation here :()
        return ($this->club_sport()->first() !== null) ? $this->club_sport()->first()->sport() : null;
    }

    /**
     * The users of this team.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('Vereinsleben\User', 'user_team');
    }

    public function contacts()
    {
        return $this->morphtomany('Vereinsleben\User', 'contactable');
    }	
}
