<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;

class Trainingsort extends Model
{

    protected $fillable = [
        'name',
        'title',
        'street',
        'house_number',
        'federal_state',
        'zip',
        'lat',
        'lng'
    ];

    public function clubs()
    {
        return $this->belongsToMany('Vereinsleben\club');
    }

    /**
     * Returns the full concatenated address.
     *
     * @return string
     */
    public function fullAddress()
    {
        if ($this->street !== null && trim($this->street) != '') {
            return sprintf('%s %s, %s %s',
                $this->street,
                $this->house_number,
                $this->zip,
                $this->city
            );
        } else if ($this->city !== null && trim($this->city) != '') {
            return sprintf('%s %s',
                $this->zip,
                $this->city
            );
        } else {
            return '';
        }
    }
}
