<?php

namespace Vereinsleben;

use DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Hootlex\Friendships\Traits\Friendable;

use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait as StaplerEloquentTrait;
use Vereinsleben\Notifications\MailResetPasswordToken;

class User extends Authenticatable implements StaplerableInterface
{
    use StaplerEloquentTrait,
        Notifiable,
        Friendable,
        SoftDeletes;


    protected $table = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'newsletter', 'messages', 'role_id',
        'gender', 'firstname', 'lastname', 'birthday', 'street', 'house_number', 'zip', 'city', 'federal_state',
        'header', 'avatar', 'privacy', 'nickname', 'password', 'fullname', 'desired_federal_state',
        'change_email_token', 'change_email_unconfirmedEmail', 'phone', 'ap_email'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'birthday' => 'date',
    ];

    protected $visible = [
        'id', 'username', 'firstname', 'lastname', 'fullname'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Constructor
     *
     * @return Club
     */
    public function __construct(array $attributes = array())
    {
        $this->hasAttachedFile('avatar', [
            'styles'      => [
                'startPage'  => '140x140#', # resize then crop
                'singleView' => '293x293#', # resize then crop
            ],
            'url'         => '/uploaded/user/:id/:attachment/:style-:secure_hash.:extension',
            #'default_url' => '/default_images/user/default-:attachment-:style.jpg',
            'default_url' => '/default_images/user/default-:attachment.jpg',
        ]);
        $this->hasAttachedFile('header', [
            'styles'      => [
                'singleView' => '1920x492#', # resize then crop
            ],
            'url'         => '/uploaded/user/:id/:attachment/:style-:secure_hash.:extension',
            #'default_url' => '/default_images/user/default-:attachment-:style.jpg',
            'default_url' => '/default_images/user/default-:attachment.jpg',
        ]);

        parent::__construct($attributes);
    }

    /**
     * The posts of this user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany('Vereinsleben\Post', 'user_id');
    }

    /**
     * The social links of this user
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function socialLinks()
    {
        return $this->morphMany('Vereinsleben\SocialLink', 'linkable');
    }

    /**
     * The clubs of this user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function clubs()
    {
        return $this->belongsToMany('Vereinsleben\Club', 'user_club_role')->withPivot('role_id');
    }


    /**
     * The bandages of this user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function bandages()
    {
        return $this->belongsToMany('Vereinsleben\Bandage', 'user_bandage_role')->withPivot('role_id');
    }
	
		/**
		 * The news of this user.
		 *
		 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
		 */
		public function news()
		{
			return $this->belongsToMany('Vereinsleben\News', 'user_news_role')
				->withPivot('role_id');
		}
	
		/**
		 * The news of this user.
		 *
		 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
		 */
		public function newsByContentManager()
		{
			$role = new Role();
			return $this->belongsToMany('Vereinsleben\News', 'user_news_role')
				->withPivot('role_id')
				->wherePivot('role_id', $role->getByConstant(Role::CONTENT_MANAGER)->id);
		}
    
    /**
     * The clubs of this user where the user is CLUB_OWNER.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function clubsWhereOwner()
    {
        $role = Role::where('constant', 'CLUB_OWNER')->firstOrFail();

        return $this->clubs()->wherePivot('role_id', $role->id);
    }

    /**
     * The clubs of this user where the user is CLUB_OWNER.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function clubsWhereAdminRequested()
    {
        $role = Role::where('constant', 'CLUB_ADMIN_REQUEST')->firstOrFail();

        return $this->clubs()->wherePivot('role_id', $role->id);
    }

    /**
     * The clubs of this user where the user is CLUB_ADMIN.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function clubsWhereAdmin()
    {
        $role = Role::where('constant', 'CLUB_ADMIN')->firstOrFail();

        return $this->clubs()->wherePivot('role_id', $role->id);
    }
	
		/**
		 * The clubs of this user where the user is BAND_ADMIN.
		 *
		 * @return \Illuminate\Database\Eloquent\Builder
		 */
		public function bandagesWhereAdmin()
		{
		
			$role = Role::where('constant', 'BAND_ADMIN')->firstOrFail();
			
			return $this->bandages()->wherePivot('role_id', $role->id);
		}

    /**
     * The clubs of this user where the user is CLUB_MEMBER.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function clubsWhereMember()
    {
        $role = Role::where('constant', 'CLUB_MEMBER')->firstOrFail();
        $member = Member::where('name', 'old')->firstOrFail();

        return $this->clubs()->wherePivot('role_id', $role->id)->where('member_id', '!=', $member->id);
    }

    /**
     * The clubs of this user where the user is CLUB_FAN.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function clubsWhereFan()
    {
        $member = Member::where('name', 'fan')->firstOrFail();

        return $this->clubs()->wherePivot('member_id', $member->id);
    }

    /**
     * The clubs of this user where the user is any kind of member
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function clubsWhereAll()
    {
        $active = Member::where('name', 'active')->firstOrFail();
        $passive = Member::where('name', 'passive')->firstOrFail();
        $old = Member::where('name', 'old')->firstOrFail();
        $trainer = Member::where('name', 'trainer')->firstOrFail();
        $fan = Member::where('name', 'fan')->firstOrFail();
        $functionary = Member::where('name', 'functionary')->firstOrFail();

        return $this->clubs()->wherePivotIn('member_id', [$active->id, $passive->id, $old->id, $trainer->id, $fan->id, $functionary->id]);
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  array $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        //
    }

    /**
     * User role
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo('Vereinsleben\Role', 'role_id');
    }

    /**
     * The teams of this user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function teams()
    {
        return $this->belongsToMany('Vereinsleben\Team', 'user_team')->withPivot('club_id');
    }

    /**
     * The teams of the given club where the user is member.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function teamMembershipsForClub(Club $club)
    {
        return $this->teams()->wherePivot('club_id', $club->id);
    }

    /**
     * The sports (interests) of this user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function sports()
    {
        return $this->belongsToMany('Vereinsleben\Sport', 'user_sport')->withPivot('interest');
    }

    /**
     * The user sports (interest) pivot model of this user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user_sports()
    {
        return $this->hasMany('Vereinsleben\UserSport');
    }

    /**
     * The sport career of this user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function sport_careers()
    {
        return $this->hasMany('Vereinsleben\SportCareer', 'user_id');
    }

    /**
     * Get vote nominees voted for user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function voteNominees()
    {
        return $this->belongsToMany('Vereinsleben\VoteNominees');
    }

    /**
     * Checks if the user role is ADMIN.
     *
     * @return boolean
     */
    public function isAdmin()
    {
        if (is_null($this->role)){
            return false;
        }

        return $this->role->constant === 'ADMIN';
    }
	
		/**
		 * Checks if the user role is ADMIN.
		 *
		 * @return boolean
		 */
		public function isContentManager()
		{
			if (is_null($this->role)){
				return false;
			}
			
			return $this->role->constant === 'CON_MANAGER';
		}

    /**
     * Checks if the user is CLUB_OWNER of the given club.
     *
     * @param \Vereinsleben\Club $club
     * @return bool
     */
    public function isClubOwner(Club $club)
    {
        return $this->clubsWhereOwner()->find($club->id) !== null;
    }

    /**
     * Checks if the user is CLUB_OWNER of the given club.
     *
     * @param \Vereinsleben\Club $club
     * @return bool
     */
    public function hasClubAdminRequested(Club $club)
    {
        return $this->clubsWhereAdminRequested()->find($club->id) !== null;
    }

    /**
     * Checks if the user is CLUB_ADMIN of the given club.
     *
     * @param \Vereinsleben\Club $club
     * @return bool
     */
    public function isClubAdmin(Club $club)
    {
        if ($this->isAdmin()){
            return true;
        }

        return $this->clubsWhereAdmin()->find($club->id) !== null;
    }
		
		/**
		 * Checks if the user is BAND_ADMIN of the given club.
		 *
		 * @param \Vereinsleben\Club $club
		 * @return bool
		 */
		public function isBandageAdmin(Bandage $bandage)
		{
			if ($this->isAdmin()){
				return true;
			}
			
			return $this->bandagesWhereAdmin()->find($bandage->id) !== null;
		}

    /**
     * The bandages of this user where the user is BAND_SUBSCRIBE.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function bandagesWhereSubscriber()
    {
        $role = Role::where('constant', 'BAND_SUBSCRIBE')->firstOrFail();

        return $this->bandages()->wherePivot('role_id', $role->id);
    }

    /**
     * Checks if the user is BAND_ADMIN of the given club.
     *
     * @param \Vereinsleben\Bandage $bandage
     * @return bool
     */
    public function isBandageOwner(Bandage $bandage)
    {
        return $this->bandagesWhereOwner()->find($bandage->id) !== null;
    }

    /**
     * Checks if the user is BAND_SUBSCRIBE of the given bandage.
     *
     * @return boolean
     */
    public function isBandageSubscriber(Bandage $bandage)
    {
        return $this->bandagesWhereSubscriber()->find($bandage->id) !== null;
    }

    /**
     * Checks if the user is CLUB_FAN of the given club.
     *
     * @return boolean
     */
    public function isClubFan(Club $club)
    {
        return $this->clubsWhereFan()->find($club->id) !== null;
    }

    /**
     * Checks if the user is CLUB_MEMBER of the given club.
     *
     * @return boolean
     */
    public function isClubMember(Club $club)
    {
        return $this->clubsWhereMember()->find($club->id) !== null;
    }

    /**
     * Checks if the user is member of a team of which club he is CLUB_MEMBER.
     *
     * @return boolean
     */
    public function isTeamMember(Team $team)
    {
        return $this->teams()->find($team->id) !== null;
    }

    /**
     * Returns the full name of the user
     *
     * @return boolean
     */
    public function fullName()
    {
        return "{$this->firstname} {$this->lastname}";
    }

    /**
     * Scope a query to fetch only verified users
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeVerified($query)
    {
        return $query->where('verified', 1);
    }

    /**
     * Scoped keyword search query
     *
     * @param $query
     * @param $keyword
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeKeywordSearch($query, $keyword)
    {
        return $this->where('username', 'LIKE', '%' . $keyword . '%')
                    ->orWhere('fullname', 'LIKE', '%' . $keyword . '%')
                    ->orWhere('city', 'LIKE', '%' . $keyword . '%');
    }

    /**
     * Scope a query to fetch users ordered by their profile completion
     * (ordered by having: avatar, club, sports)
     *
     * @param $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeLatestProfileCompleted($query)
    {
        return $query
            ->leftJoin(\DB::raw('(SELECT GROUP_CONCAT(user_club_role.id), user_club_role.user_id FROM user_club_role INNER JOIN club ON user_club_role.club_id = club.id WHERE club.published = 1 GROUP BY user_club_role.user_id) AS ucr'), function($join){
                $join
                    ->on('user.id', '=', 'ucr.user_id');
            })
            ->leftJoin(\DB::raw('(SELECT GROUP_CONCAT(user_sport.id), user_sport.user_id FROM user_sport GROUP BY user_sport.user_id) AS us'), function($join){
                $join
                    ->on('user.id', '=', 'us.user_id');
            })
            ->orderByRaw('user.avatar_file_name IS NOT NULL DESC')
            ->orderByRaw('ucr.user_id IS NOT NULL DESC')
            ->orderByRaw('us.user_id IS NOT NULL DESC')
            ->orderBy('created_at', 'desc');
    }

    /**
     * Returns the full concatenated address.
     *
     * @return string
     */
    public function fullAddress()
    {
        if ($this->street !== null && trim($this->street) != ''){
            return sprintf('%s %s, %s %s',
                $this->street,
                $this->house_number,
                $this->zip,
                $this->city
            );
        } else if ($this->city !== null && trim($this->city) != ''){
            return sprintf('%s %s',
                $this->zip,
                $this->city
            );
        } else{
            return '';
        }
    }

    public function ToAddStates()
    {
        return State::whereNotIn('id', $this->states->modelKeys())->get();
    }

    public function isProfileCompleted()
    {
        if ($this->gender && $this->firstname && $this->lastname && $this->city && $this->zip
            && $this->federal_state && $this->birthday){

            return true;
        }

        return false;
    }

    public function scopeSearch($query, $searchString)
    {
        $query = $query->where(function($query) use ($searchString){
            $query->where('username', 'like', '%' . $searchString . '%')
                  ->orWhere('firstname', 'like', '%' . $searchString . '%')
                  ->orWhere('lastname', 'like', '%' . $searchString . '%')
                  ->orWhere('fullname', 'like', '%' . $searchString . '%');
        });

        return $query;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     *
     */
    public function views()
    {
        return $this->morphMany('Vereinsleben\View', 'viewable');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function meta()
    {
        return $this->morphOne('Vereinsleben\Meta', 'metaable');
    }

    public function states()
    {
        return $this->morphToMany('Vereinsleben\State', 'stateable');
    }


    public function isAdministratorOfClub()
    {

        return $this->clubsWhereAdmin()->count() > 0;
    }

    public function desiredState()
    {
        return $this->states()->first();
    }

    /**
     * Scope a query to fetch the closest clubs
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeClosest($query, $lat, $lon, $distance = 50)
    {
        // TODO: optimize? http://stackoverflow.com/a/29931224/650203

        return $query->select(DB::raw('*, (
          6371 * acos (
            cos ( radians(' . (float)$lat . ') )
            * cos( radians( lat ) )
            * cos( radians( lng ) - radians(' . (float)$lon . ') )
            + sin ( radians(' . (float)$lat . ') )
            * sin( radians( lat ) )
          )
        ) AS distance'))->having('distance', '<=', $distance)->orderBy('distance', 'asc');

        // Using ST_Distance and ST_Within function in MySQL 5.6.1
        #$lat = 50.9469188;
        #$lon = 10.7092884;
        #$maximumDistance = 1000;
        #$lonBound1 = $lon - $maximumDistance / abs(cos(deg2rad($lat)) * 69);
        #$lonBound2 = $lon + $maximumDistance / abs(cos(deg2rad($lat)) * 69);
        #$latBound1 = $lat - ($maximumDistance / 69);
        #$latBound2 = $lat + ($maximumDistance / 69);
        #$clubs = Club::whereRaw("
        #  ST_Within(
        #    geolocation,
        #    envelope(
        #      linestring(
        #        point($lonBound1, $latBound1),
        #        point($lonBound2, $latBound2)
        #      )
        #    )
        #  )")->orderByRaw("
        #  ST_Distance(
        #    geolocation,
        #    GeomFromText('POINT($lat $lon)')
        #  )"
        #)->get();
    }

    /**
     * Send a password reset email to the user
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MailResetPasswordToken($token));
    }

    public function suggestedClubs()
    {
        return $this->hasMany('Vereinsleben\Club', 'suggested_from');
    }


    public function checkNameFormat($name)
    {
        $titel=array('von','van','de','zu','und','bei','di');
        $name = trim($name);
        if (strlen($name)<2)$name = "";
        if (strlen($name)>1)
        {
            $ausgabe = "";
            $temp = explode(' ',$name);
            foreach($temp as $temp2)
            {
                $temp2 = strtolower($temp2);
                if (!in_array($temp2,$titel))
                {
                    if (strpos($temp2,"-") !== false && strlen($temp2)>1)
                    {
                        $temp3 = explode("-",$temp2);
                        $temp2="";
                        $first_temp4=true;
                        foreach($temp3 as $temp4)
                        {
                            if ($first_temp4!==true)
                            {
                                $temp2.="-";
                            }
                            $temp4[0]=strtoupper($temp4[0]);
                            $temp2.=$temp4;
                            $first_temp4 = false;
                        }
                    }else
                    {
                        $temp2[0] = strtoupper($temp2[0]);
                    }
                }
                $ausgabe.=$temp2 . " ";
            }

            $name = trim($ausgabe);
        }

        return $name;
    }

}
