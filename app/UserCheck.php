<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Vereinsleben\Club;

use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait as StaplerEloquentTrait;

class UserCheck extends Authenticatable implements StaplerableInterface
{
    use StaplerEloquentTrait,
        SoftDeletes;

    protected $table = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'newsletter', 'messages', 'role_id',
        'gender', 'firstname', 'lastname', 'birthday', 'street', 'zip', 'city', 'federal_state',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'birthday' => 'date',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Constructor
     *
     * @return Club
     */
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
    }

    /**
     * The posts of this user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany('Vereinsleben\Post', 'user_id');
    }

    /**
     * The clubs of this user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function clubs()
    {
        return $this->belongsToMany('Vereinsleben\Club', 'user_club_role')->withPivot('role_id');
    }

    /**
     * The clubs of this user where the user is CLUB_OWNER.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function clubsWhereOwner()
    {
        $role = Role::where('constant', 'CLUB_OWNER')->firstOrFail();
        return $this->clubs()->wherePivot('role_id', $role->id);
    }

    /**
     * The clubs of this user where the user is CLUB_ADMIN.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function clubsWhereAdmin()
    {
        $role = Role::where('constant', 'CLUB_ADMIN')->firstOrFail();
        return $this->clubs()->wherePivot('role_id', $role->id);
    }

    /**
     * The clubs of this user where the user is CLUB_MEMBER.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function clubsWhereMember()
    {
        $role = Role::where('constant', 'CLUB_MEMBER')->firstOrFail();
        return $this->clubs()->wherePivot('role_id', $role->id);
    }

    /**
     * The clubs of this user where the user is CLUB_FAN.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function clubsWhereFan()
    {
        $role = Role::where('constant', 'CLUB_FAN')->firstOrFail();
        return $this->clubs()->wherePivot('role_id', $role->id);
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  array $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        //
    }

    /**
     * User role
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo('Vereinsleben\Role', 'role_id');
    }

    /**
     * The teams of this user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function teams()
    {
        return $this->belongsToMany('Vereinsleben\Team', 'user_team')->withPivot('club_id');
    }

    /**
     * The teams of the given club where the user is member.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function teamMembershipsForClub(Club $club)
    {
        return $this->teams()->wherePivot('club_id', $club->id);
    }

    /**
     * The sports (interests) of this user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function sports()
    {
        return $this->belongsToMany('Vereinsleben\Sport', 'user_sport')->withPivot('interest');
    }

    /**
     * The user sports (interest) pivot model of this user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user_sports()
    {
        return $this->hasMany('Vereinsleben\UserSport');
    }

    /**
     * The sport career of this user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function sport_careers()
    {
        return $this->hasMany('Vereinsleben\SportCareer', 'user_id');
    }

    /**
     * Get vote nominees voted for user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function voteNominees()
    {
        return $this->belongsToMany('Vereinsleben\VoteNominees');
    }

    /**
     * Checks if the user role is ADMIN.
     *
     * @return boolean
     */
    public function isAdmin()
    {
        if (is_null($this->role)) {
            return false;
        }
        return $this->role->constant === 'ADMIN';
    }

    /**
     * Checks if the user is CLUB_OWNER of the given club.
     *
     * @param \Vereinsleben\Club $club
     * @return bool
     */
    public function isClubOwner(Club $club)
    {
        return $this->clubsWhereOwner()->find($club->id) !== null;
    }

    /**
     * Checks if the user is CLUB_ADMIN of the given club.
     *
     * @param \Vereinsleben\Club $club
     * @return bool
     */
    public function isClubAdmin(Club $club)
    {
        return $this->clubsWhereAdmin()->find($club->id) !== null;
    }

    /**
     * Checks if the user is CLUB_FAN of the given club.
     *
     * @return boolean
     */
    public function isClubFan(Club $club)
    {
        return $this->clubsWhereFan()->find($club->id) !== null;
    }

    /**
     * Checks if the user is CLUB_MEMBER of the given club.
     *
     * @return boolean
     */
    public function isClubMember(Club $club)
    {
        return $this->clubsWhereMember()->find($club->id) !== null;
    }

    /**
     * Checks if the user is member of a team of which club he is CLUB_MEMBER.
     *
     * @return boolean
     */
    public function isTeamMember(Team $team)
    {
        return $this->teams()->find($team->id) !== null;
    }

    /**
     * Returns the full name of the user
     *
     * @return boolean
     */
    public function fullName()
    {
        return "{$this->firstname} {$this->lastname}";
    }

    /**
     * Scope a query to fetch only verified users
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeVerified($query)
    {
        return $query->where('verified', 1);
    }

    /**
     * Scoped keyword search query
     *
     * @param $query
     * @param $keyword
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeKeywordSearch($query, $keyword)
    {
        return $this->where('username', 'LIKE', '%' . $keyword . '%')
            ->orWhere('fullname', 'LIKE', '%' . $keyword . '%')
            ->orWhere('city', 'LIKE', '%' . $keyword . '%');
    }

    /**
     * Scope a query to fetch users ordered by their profile completion
     * (ordered by having: avatar, club, sports)
     *
     * @param $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeLatestProfileCompleted($query)
    {
        return $query
            ->leftJoin(\DB::raw('(SELECT GROUP_CONCAT(user_club_role.id), user_club_role.user_id FROM user_club_role INNER JOIN club ON user_club_role.club_id = club.id WHERE club.published = 1 GROUP BY user_club_role.user_id) AS ucr'), function ($join) {
                $join
                    ->on('user.id', '=', 'ucr.user_id');
            })
            ->leftJoin(\DB::raw('(SELECT GROUP_CONCAT(user_sport.id), user_sport.user_id FROM user_sport GROUP BY user_sport.user_id) AS us'), function ($join) {
                $join
                    ->on('user.id', '=', 'us.user_id');
            })
            ->orderByRaw('user.avatar_file_name IS NOT NULL DESC')
            ->orderByRaw('ucr.user_id IS NOT NULL DESC')
            ->orderByRaw('us.user_id IS NOT NULL DESC')
            ->orderBy('created_at', 'desc');
    }

    /**
     * Returns the full concatenated address.
     *
     * @return string
     */
    public function fullAddress()
    {
        if ($this->street !== null && trim($this->street) != '') {
            return sprintf('%s %s, %s %s',
                $this->street,
                $this->house_number,
                $this->zip,
                $this->city
            );
        } else if ($this->city !== null && trim($this->city) != '') {
            return sprintf('%s %s',
                $this->zip,
                $this->city
            );
        } else {
            return '';
        }
    }

}
