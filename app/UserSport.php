<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;

/**
 * Vereinsleben\UserSport
 *
 * @property int $id
 * @property int $user_id
 * @property int $sport_id
 * @property int|null $interest
 * @property-read \Vereinsleben\Sport $sport
 * @property-read \Vereinsleben\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\UserSport whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\UserSport whereInterest($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\UserSport whereSportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\UserSport whereUserId($value)
 * @mixin \Eloquent
 */
class UserSport extends Model
{
    protected $table = 'user_sport';

    protected $fillable = [
        'user_id',
        'sport_id',
        'interest'
    ];

    public $timestamps = false;

    /**
     * Associated user
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function user()
    {
        return $this->belongsTo('Vereinsleben\User', 'user_id');
    }

    /**
     * Associated sport
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function sport()
    {
        return $this->belongsTo('Vereinsleben\Sport', 'sport_id');
    }
}
