<?php

namespace Vereinsleben;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UserVoteNominee extends Model
{
    protected $table = 'user_vote_nominee';

    protected $fillable = [
        'user_id',
        'vote_nominee_id',
        'is_fake'
    ];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
    }

    public function user()
    {
        return $this->hasOne('Vereinsleben\User', 'id', 'user_id');
    }

}
