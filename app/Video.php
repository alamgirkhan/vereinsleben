<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;

/**
 * Vereinsleben\Video
 *
 * @property int $id
 * @property string $type
 * @property string|null $title
 * @property string|null $description
 * @property string|null $url
 * @property string|null $identifier
 * @property int|null $videoable_id
 * @property string|null $videoable_type
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $videoable
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Video whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Video whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Video whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Video whereIdentifier($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Video whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Video whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Video whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Video whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Video whereVideoableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Video whereVideoableType($value)
 * @mixin \Eloquent
 */
class Video extends Model
{
    protected $table = 'video';

    // fillable via mass assignment
    protected $fillable = ['type', 'title', 'description', 'url', 'identifier'];

    const TYPE_YOUTUBE = 'youtube';
    const TYPE_VIMEO   = 'vimeo';

    /**
     * Polymorphic relationship to other models.
     */
    public function videoable()
    {
        return $this->morphTo();
    }

    public function extractData($videoUrl)
    {
        $video = [];
        
        if ($videoUrl !== null && trim($videoUrl) !== '') {
            
            $patterns = [
                'youtube' => [
                    '#(?:<\>]+href=\")?(?:http://)?((?:[a-zA-Z]{1,4}\.)?youtube.com/(?:watch)?\?v=(.{11}?))[^"]*(?:\"[^\<\>]*>)?([^\<\>]*)(?:)?#',
                    '%(?:youtube\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i',
                ],
                'vimeo'   => [
                    '/(http|https)?:\/\/(www\.|player\.)?vimeo\.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|video\/|)(\d+)(?:|\/\?)/'
                ]
            ];
            
            foreach ($patterns['youtube'] as $pattern) {
                if (preg_match($pattern, $videoUrl, $match) != false) {
                    if (isset($match[2])) {
                        $videoId = $match[2];
                    }
                    if (empty($videoId)) {
                        $videoId = $match[1];
                    }

                    if (!empty($videoId)) {
                        $video = ['type' => 'youtube', 'identifier' => $videoId, 'url' => 'https://www.youtube.com/watch?v=' . $videoId];

                        return $video;
//                        $video = new Video(['type' => 'youtube', 'identifier' => $videoId, 'url' => 'https://www.youtube.com/watch?v='.$videoId]);
//                        $post->videos()->save($video);
                    }
                    
                    break;
                }
            }

            foreach ($patterns['vimeo'] as $pattern){
                if (preg_match($pattern, $videoUrl, $match) != false){
                    if (isset($match[4])){
                        $videoId = $match[4];
                    }
                    if (empty($videoId)){
                        $videoId = $match[1];
                    }

                    if (!empty($videoId)){
                        $video = ['type' => 'vimeo', 'identifier' => $videoId, 'url' => $match[0]];

                        return $video;
                    }

                    break;
                }
            }


        }

        return $video;
    }
}
