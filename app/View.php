<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;

class View extends Model
{
    public $fillable = ['ip'];

    //

    public function viewable()
    {
        return $this->morphTo();
    }
}
