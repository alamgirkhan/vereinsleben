<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DateTime;

use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait as StaplerEloquentTrait;

use Carbon\Carbon;

/**
 * Vereinsleben\Vote
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $pre_name
 * @property string|null $description
 * @property string|null $pre_description
 * @property string|null $teaser
 * @property string|null $teaser_headline
 * @property string|null $period_start
 * @property string|null $period_end
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property string|null $winner_title
 * @property string|null $winner_description
 * @property string|null $winner_image_file_name
 * @property int|null $winner_image_file_size
 * @property string|null $winner_image_content_type
 * @property string|null $winner_image_updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Vereinsleben\VoteNominee[] $voteNominees
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Vote current()
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\Vote onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Vote whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Vote whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Vote whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Vote whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Vote whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Vote wherePeriodEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Vote wherePeriodStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Vote wherePreDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Vote wherePreName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Vote whereTeaser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Vote whereTeaserHeadline($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Vote whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Vote whereWinnerDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Vote whereWinnerImageContentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Vote whereWinnerImageFileName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Vote whereWinnerImageFileSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Vote whereWinnerImageUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\Vote whereWinnerTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\Vote withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\Vote withoutTrashed()
 * @mixin \Eloquent
 */
class Vote extends Model implements StaplerableInterface
{
    use StaplerEloquentTrait,
        SoftDeletes;

    protected $table = 'vote';

    protected $fillable = [
        'name',
        'pre_name',
        'description',
        'pre_description',
        'teaser',
        'teaser_headline',
        'period_start',
        'period_end',
        'winner_title',
        'winner_description',
        'winner_image',
    ];

    /**
     * Vote constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = array())
    {
        $this->hasAttachedFile('winner_image', [
            'styles' => [
                'startpage' => '360',
                'singleview' => '750',
            ],
            'url' => '/uploaded/vote/:id/:attachment/:style-:secure_hash.:extension',
            'default_url' => '/default_images/club/default-:attachment-:style.jpg',
        ]);

        parent::__construct($attributes);
    }

    /**
     * Get nominees for this vote
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function voteNominees()
    {
        return $this->hasMany('Vereinsleben\VoteNominee');
    }

    /**
     * Scope the current Voting
     *
     * @param $query
     * @return mixed $query
     */
    public function scopeCurrent($query)
    {
        $dt = Carbon::now()->format('Y-m-d');

        return $query
            ->where('period_start', '<=', $dt)
            ->where('period_end', '>=', $dt);
    }

    /**
     * Determine if the requested vote is the actual current one
     *
     * @param int  $voteId
     * @return bool
     */
    public function isCurrent($voteId = null)
    {
        if( is_null($voteId) ) {
            $current = Vote::find($this->attributes['id']);
        } else {
            $voteId = intval($voteId);
            $current = Vote::find($voteId);
        }

        $dt = Carbon::now()->format('Ymd');
        $start = DateTime::createFromFormat('Y-m-d', $current->period_start)->format('Ymd');
        $end = DateTime::createFromFormat('Y-m-d', $current->period_end)->format('Ymd');

        if($start <= $dt && $dt <= $end) {
            return true;
        }
        return false;
    }

    /**
     * Determine if the current vote has a previous vote
     *
     * @return bool
     */
    public function hasPreviousVote()
    {
        if( isset($this->attributes['id']) && $this->attributes['id'] > 1) {
            $current = Vote::findOrFail($this->attributes['id']);
            $previous = Vote::findOrFail($current->id - 1);
        } else {
            $previous = Vote::orderBy('id', 'desc')->first();
        }

        if( $previous instanceof Vote) {
            return true;
        }

        return false;
    }

    /**
     * Returns the previous vote
     *
     * @return bool
     */
    public function getPreviousVote()
    {
        if( $this->hasPreviousVote() ) {
            if( isset($this->attributes['id']) && $this->attributes['id'] > 1 ) {
                return Vote::findOrFail($this->attributes['id'] - 1);
            }
            return Vote::orderBy('id', 'desc')->first();
        }
        return false;
    }

    public function getWinner()
    {
        if( date('Ymd') >= DateTime::createFromFormat('Y-m-d', $this->attributes['period_end'])->format('Ymd') ) {
            $winner = VoteNominee::where('vote_id', '=', (int)$this->attributes['id'])
                ->where('winner', 1)
                ->latest()
                ->first();
            return ($winner instanceof VoteNominee) ? $winner->club : false;
        }
        return false;
    }
}
