<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Vereinsleben\VoteNominee
 *
 * @property int $id
 * @property int $vote_id
 * @property string $voteable_type
 * @property string $voteable_id
 * @property string $project
 * @property string $project_description
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property string $slug
 * @property int $winner
 * @property-read \Vereinsleben\Club $club
 * @property-read \Illuminate\Database\Eloquent\Collection|\Vereinsleben\User[] $users
 * @property-read \Vereinsleben\Vote $vote
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $voteable
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\VoteNominee onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\VoteNominee whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\VoteNominee whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\VoteNominee whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\VoteNominee whereProject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\VoteNominee whereProjectDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\VoteNominee whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\VoteNominee whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\VoteNominee whereVoteId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\VoteNominee whereVoteableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\VoteNominee whereVoteableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Vereinsleben\VoteNominee whereWinner($value)
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\VoteNominee withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Vereinsleben\VoteNominee withoutTrashed()
 * @mixin \Eloquent
 */
class VoteNominee extends Model
{
    use SoftDeletes;

    protected $table = 'vote_nominee';
    protected $fillable = [
        'project',
        'project_description',
        'slug',
        'winner',
    ];

    /**
     * Get vote for this nominee
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function vote()
    {
        return $this->belongsTo('Vereinsleben\Vote');
    }


    /**
     * Get users voted for nominee
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('Vereinsleben\User');
    }

    /**
     * Get voteable entities
     *
     * @return \Illuminate\Database\Eloquent\Relations\morphTo
     */
    public function voteable()
    {
        return $this->morphTo()->withTrashed();
    }

    public function club()
    {
        return $this->hasOne('Vereinsleben\Club', 'id', 'voteable_id');
    }
}
