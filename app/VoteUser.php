<?php

namespace Vereinsleben;

use Illuminate\Database\Eloquent\Model;

class VoteUser extends Model
{

    protected $table = 'vote_user';
    protected $fillable = [
        'id',
        'user_id',
        'vote_id',
        'remainder',
        'created_at',
        'updated_at',
    ];


}
