#!/bin/bash
git pull
composer install
composer dump-autoload -o
php artisan migrate
php artisan clear-compiled # Remove the compiled class file
npm install
gulp --production
