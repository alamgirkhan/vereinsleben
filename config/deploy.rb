# config valid only for current version of Capistrano
lock '3.5.0'

set :application, 'vereinsleben-www'
set :repo_url, 'git@github.com:RPR1/vereinsleben-www.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, '/var/www/my_app_name'

# Default value for :scm is :git
set :scm, :git

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: 'log/capistrano.log', color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')
set :linked_files, fetch(:linked_files, []).push('.env')

# Default value for linked_dirs is []
# set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'public/system')
set :linked_dirs, fetch(:linked_dirs, []).push('storage', 'public/uploaded', 'node_modules')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 5

# hipchat notification integration
# Required
set :hipchat_token, "KZpKHhX6O1oATW5n1zG29Seo9gqmupCUnp0F06sL"
set :hipchat_room_name, "20" # If you pass an array such as ["room_a","room_b"] you can send announcements to multiple rooms.
# Optional
set :hipchat_enabled, false # set to false to prevent any messages from being sent
set :hipchat_announce, false # notify users
set :hipchat_color, 'yellow' # normal message color
set :hipchat_success_color, 'green' # finished deployment message color
set :hipchat_failed_color, 'red' # cancelled deployment message color
set :hipchat_message_format, 'html' # Sets the deployment message format, see https://www.hipchat.com/docs/api/method/rooms/message
set :hipchat_options, {
  :api_version  => "v2", # Set "v2" to send messages with API v2
  :server_url => 'https://hipchat.trio-group.de'
}
set :needed_space, 200000 # needed space to deploy


namespace :deploy do

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

  desc "Shutdown Laravel App"
  before :starting, :artisan_down do
    on roles(:web) do
      within current_path do
        needed_space = fetch(:needed_space)
        output = capture(:df, "/ | grep 'dev' | awk '{print $4}'")
        if Integer(output) < needed_space
          abort "Nicht genügend Speicherkapazität verfügbar. Es werden mindestens #{needed_space/1024} MiB benötigt."
        end
        execute :php, "artisan down"
      end
    end
  end

  desc "Build"
  after :updated, :build do
    on roles(:web) do
      within release_path do
        execute :composer, "install --optimize-autoloader --prefer-dist --quiet --no-interaction"
        execute :composer, "dump-autoload -o"
        execute :php, "artisan clear-compiled"
        #execute :php, "artisan config:cache" # seems to make problems on the production machines with FPM / Bytecode cache?!
        #execute :php, "artisan api:cache" # do not use route:cache here because this breaks Dingo routes; api:cache caches both.
        execute :php, "artisan optimize --force"
        execute :php, "artisan migrate --force"

        execute :npm, :install
        execute :"node_modules/.bin/gulp", "--production"
      end
    end
  end

  desc "Restart Laravel App"
  after :finished, :artisan_up do
    on roles(:web) do
      within current_path do
        execute :chmod, "-R 775 storage/"
        execute :chmod, "-R 775 bootstrap/cache/"

        # important: clear the PHP OpCache because it might not update with Capistrano's symlink-based deployment
        execute :php, "cachetool.phar opcache:reset"

        execute :php, "artisan up"
      end
    end
  end

end
