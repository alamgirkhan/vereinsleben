# Customizing Slackistrano
module Slackistrano
  class CustomMessaging < Messaging::Base

    # Send failed message to #ops. Send all other messages to default channels.
    # The #ops channel must exist prior.
    def channels_for(action)
      #if action == :failed
      #  "#ops"
      #else
      #  super
      #end
      super
    end

    # Fancy updating message.
    def payload_for_updating
      {
        attachments: [{
          color: 'warning',
          title: 'Application deploy started! :hourglass_flowing_sand: :shipit:',
          fields: [{
            title: 'Environment',
            value: stage,
            short: true
          }, {
            title: 'Branch',
            value: branch,
            short: true
          }, {
            title: 'Deployer',
            value: deployer,
            short: true
          }],
          fallback: super[:text]
        }]
      }
    end

    # Supress reverting message.
    def payload_for_reverting
      nil
    end

    # Default reverted message.  Alternatively simply do not redefine this
    # method.
    def payload_for_reverted
      super
    end

    # Fancy updated message.
    # See https://api.slack.com/docs/message-attachments
    def payload_for_updated
      {
        attachments: [{
          color: 'good',
          title: 'Application successfully deployed! :white_check_mark: :ok_hand: :shipit:',
          fields: [{
            title: 'Environment',
            value: stage,
            short: true
          }, {
            title: 'Branch',
            value: branch,
            short: true
          }, {
            title: 'Deployer',
            value: deployer,
            short: true
          }, {
            title: 'Time',
            value: elapsed_time,
            short: true
          }],
          fallback: super[:text]
        }]
      }
    end

    # Slightly tweaked failed message.
    # See https://api.slack.com/docs/message-formatting
    def payload_for_failed
      payload = super
      payload[:text] = "OMG :fire: #{payload[:text]}"
      payload
    end

    # Override the deployer helper to pull the full name from the password file.
    # See https://github.com/phallstrom/slackistrano/blob/master/lib/slackistrano/messaging/helpers.rb
    def deployer
      #Etc.getpwnam(ENV['USER']).gecos
      ENV['USER']
    end
  end
end