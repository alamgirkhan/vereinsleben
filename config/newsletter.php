<?php

return [

    /*
    |--------------------------------------------------------------------------
    | VENUS Newsletter system API configuration
    |--------------------------------------------------------------------------
    |
    | Set the public and private API keys as provided by reCAPTCHA.
    |
    | In version 2 of reCAPTCHA, public_key is the Site key,
    | and private_key is the Secret key.
    |
    */
    'auth_uri'       => env('VENUS_AUTH_URI', null),
    'auth_username'  => env('VENUS_AUTH_USERNAME', null),
    'auth_client_id' => env('VENUS_AUTH_CLIENT_ID', null),
    'auth_password'  => env('VENUS_AUTH_PASSWORD', null),
    'import_uri'     => env('VENUS_IMPORT_URI', null),

];
