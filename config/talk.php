<?php

return [
    'user'      => [
        'model' => 'Vereinsleben\User',
    ],
    'broadcast' => [
        'enable'   => false,
        'app_name' => 'parched-brook-474',
        'pusher'   => [
            'app_id'     => '454734',
            'app_key'    => '1c28df510acafb17f66c',
            'app_secret' => 'e64475814f18a67bb3a3',
            'options'    => [
                'cluster'   => 'eu',
                'encrypted' => true
            ]
        ],
    ],
];
