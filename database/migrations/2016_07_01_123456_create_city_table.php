<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCityTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('city', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->integer('opengeodb_loc_id')->nullable();
            $table->string('name_ascii')->nullable();
            $table->string('name')->nullable();
            $table->float('lat', 10, 0)->nullable();
            $table->float('lon', 10, 0)->nullable();
        });
        DB::statement('ALTER TABLE city ADD coords POINT DEFAULT NULL' ); # schema creator does not support POINT datatype
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('city');
    }

}
