<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClubSportTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('club_sport', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('club_id')->nullable();
			$table->integer('sport_id')->nullable();
			$table->string('m_club_id', 128)->nullable();
			$table->string('m_sport_id', 128)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('club_sport');
	}

}
