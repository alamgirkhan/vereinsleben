<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEventTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('title')->nullable();
			$table->text('content')->nullable();
			$table->text('content_raw')->nullable();
			$table->dateTime('created_at')->nullable();
			$table->dateTime('modified_at')->nullable();
			$table->dateTime('published_at')->nullable();
			$table->dateTime('published_from')->nullable();
			$table->dateTime('published_to')->nullable();
			$table->integer('club_id')->nullable();
			$table->integer('user_id')->nullable();
			$table->integer('gallery_id')->nullable();
			$table->dateTime('schedule_begin')->nullable();
			$table->dateTime('schedule_end')->nullable();
			$table->string('street')->nullable();
			$table->string('house_number', 45)->nullable();
			$table->string('city', 128)->nullable();
			$table->string('zip', 45)->nullable();
			$table->string('state', 32)->nullable();
			$table->float('lat', 10, 0)->nullable();
			$table->float('lng', 10, 0)->nullable();
			$table->string('m_id', 128)->nullable();
			$table->string('m_gallery', 128)->nullable();
			$table->string('m_club', 128);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('event');
	}

}
