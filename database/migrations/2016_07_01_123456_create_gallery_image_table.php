<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGalleryImageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('gallery_image', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('url')->nullable();
			$table->string('title')->nullable();
			$table->text('description', 65535)->nullable();
			$table->integer('gallery_id')->nullable();
			$table->string('source')->nullable();
			$table->dateTime('created_at')->nullable();
			$table->dateTime('modified_at')->nullable();
			$table->string('m_id', 128)->nullable();
			$table->string('m_image_original');
			$table->string('m_image_singleview');
			$table->string('m_image_preview');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gallery_image');
	}

}
