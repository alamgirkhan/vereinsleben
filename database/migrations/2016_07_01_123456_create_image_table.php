<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateImageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('image', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('url')->nullable();
			$table->integer('imageable_id')->nullable();
			$table->string('imageable_type', 64)->nullable();
			$table->integer('image_meta_id')->nullable();
			$table->string('image_size')->nullable();
			$table->string('image_type')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('image');
	}

}
