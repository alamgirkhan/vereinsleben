<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMGalleryImageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('m_gallery_image', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('m_gallery_id', 128)->nullable();
			$table->string('m_gallery_image_id', 128)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('m_gallery_image');
	}

}
