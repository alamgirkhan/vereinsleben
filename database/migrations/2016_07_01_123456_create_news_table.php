<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('news', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('title')->nullable();
			$table->string('sub_title')->nullable();
			$table->string('slug', 128)->nullable();
			$table->text('content', 65535)->nullable();
			$table->text('content_teaser', 65535)->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->dateTime('modified_at')->nullable();
			$table->dateTime('published_at')->nullable();
			$table->dateTime('published_from')->nullable();
			$table->dateTime('published_until')->nullable();
			$table->string('element_order', 128)->nullable();
			$table->integer('user_id')->nullable();
			$table->integer('category_id')->nullable();
			$table->string('state', 32)->nullable();
			$table->integer('owner_id')->nullable();
			$table->integer('gallery_id')->nullable();
			$table->string('m_category', 64)->nullable();
			$table->string('m_owner', 64)->nullable();
			$table->string('m_author', 64)->nullable();
			$table->string('m_label', 64)->nullable();
			$table->string('m_image_singleview')->nullable();
			$table->string('m_image_original')->nullable();
			$table->string('m_image_source')->nullable();
			$table->string('m_image_startpage')->nullable();
			$table->string('m_id', 64)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('news');
	}

}
