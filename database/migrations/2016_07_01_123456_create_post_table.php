<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('post', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('title')->nullable();
			$table->text('content_raw')->nullable();
			$table->text('content')->nullable();
			$table->dateTime('created_at')->nullable();
			$table->dateTime('modified_at')->nullable();
			$table->dateTime('published_at')->nullable();
			$table->dateTime('published_from')->nullable();
			$table->dateTime('published_to')->nullable();
			$table->string('state', 32)->nullable();
			$table->integer('club_id')->nullable();
			$table->integer('user_id')->nullable();
			$table->integer('gallery_id')->nullable();
			$table->string('m_club', 128)->nullable();
			$table->string('m_id', 128)->nullable();
			$table->string('m_gallery', 128)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('post');
	}

}
