<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRoleTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role', function(Blueprint $table){
            $table->integer('id', true);
            $table->string('name', 64)->nullable();
            $table->string('constant', 64)->nullable();
            $table->string('description')->nullable();
            $table->dateTime('created')->nullable();
            $table->dateTime('modified')->nullable();
        });

        DB::table('role')->insert(
            [
                ['name' => 'Benutzer', 'constant' => 'USER'],
                ['name' => 'Administrator', 'constant' => 'ADMIN'],
                ['name' => 'Vereinseigner', 'constant' => 'CLUB_OWNER'],
                ['name' => 'Vereinsadministrator', 'constant' => 'CLUB_ADMIN'],
                ['name' => 'Vereinsmitglied', 'constant' => 'CLUB_MEMBER'],
                ['name' => 'Vereinsfan', 'constant' => 'CLUB_FAN'],
                ['name' => 'Verbandadministrator', 'constant' => 'BAND_ADMIN'],
                ['name' => 'Subadministrator', 'constant' => 'SUB_ADMIN'],
                ['name' => 'Contentmanager', 'constant' => 'CON_MANAGER'],
                ['name' => 'Verbandabonnieren', 'constant' => 'BAND_SUBSCRIBE'],
                ['name' => 'VereinsAdminRequest', 'constant' => 'CLUB_ADMIN_REQUEST'],
            ]
        );
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('role');
    }

}
