<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSocialLinkTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('social_link', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('url')->nullable();
			$table->string('link_type', 45)->nullable();
			$table->string('linkable_type')->nullable();
			$table->integer('linkable_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('social_link');
	}

}
