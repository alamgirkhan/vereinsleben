<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTeamTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('team', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name')->nullable();
			$table->text('description', 65535)->nullable();
			$table->string('contact_name', 128)->nullable();
			$table->string('contact_email')->nullable();
			$table->string('m_club', 64)->nullable();
			$table->string('m_sport')->nullable();
			$table->string('m_id')->nullable();
			$table->integer('club_id')->nullable();
			$table->integer('club_sport_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('team');
	}

}
