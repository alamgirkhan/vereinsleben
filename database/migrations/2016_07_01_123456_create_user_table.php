<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('username', 64)->nullable();
			$table->string('firstname', 128)->nullable();
			$table->string('lastname', 128)->nullable();
			$table->string('fullname', 128)->nullable();
			$table->string('email', 128)->nullable();
			$table->integer('role_id')->nullable();
			$table->string('street', 128)->nullable();
			$table->string('house_number', 45)->nullable();
			$table->string('city', 128)->nullable();
			$table->string('zip', 45)->nullable();
			$table->string('federal_state', 64)->nullable();
			$table->string('password')->nullable();
			$table->integer('newsletter')->nullable();
			$table->dateTime('birthday')->nullable();
			$table->timestamps();
			$table->dateTime('modified_at')->nullable();
			$table->dateTime('login_at')->nullable();
			$table->softDeletes();
			$table->integer('deleted')->nullable();
			$table->string('gravatar')->nullable();
			$table->string('change_email_token')->nullable();
			$table->integer('verified')->nullable();
			$table->string('register_token')->nullable();
			$table->string('password_recovery_token')->nullable();
			$table->string('state', 64)->nullable();
			$table->string('register_state', 16)->nullable();
			$table->string('gender', 8)->nullable();
			$table->string('password_recovery_state', 16)->nullable();
			$table->string('remember_token')->nullable();
			$table->string('m_id', 128);
			$table->string('m_role', 128);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user');
	}

}
