<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameTimestampColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('club', function ($table) {
            $table->renameColumn('modified_at', 'updated_at');
        });
        Schema::table('event', function ($table) {
            $table->renameColumn('modified_at', 'updated_at');
        });
        Schema::table('gallery', function ($table) {
            $table->renameColumn('modified_at', 'updated_at');
        });
        Schema::table('gallery_image', function ($table) {
            $table->renameColumn('modified_at', 'updated_at');
        });
        Schema::table('image_meta', function ($table) {
            $table->renameColumn('modified_at', 'updated_at');
        });
        Schema::table('news', function ($table) {
            $table->dropColumn('modified_at');
        });
        Schema::table('post', function ($table) {
            $table->renameColumn('modified_at', 'updated_at');
        });
        Schema::table('role', function ($table) {
            $table->renameColumn('created', 'created_at');
            $table->renameColumn('modified', 'updated_at');
        });
        Schema::table('social_link', function ($table) {
            $table->timestamps();
        });
        Schema::table('sport', function ($table) {
            $table->timestamps();
        });
        Schema::table('team', function ($table) {
            $table->timestamps();
        });
        Schema::table('user', function ($table) {
            $table->dropColumn('modified_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('club', function ($table) {
            $table->renameColumn('updated_at', 'modified_at');
        });
        Schema::table('event', function ($table) {
            $table->renameColumn('updated_at', 'modified_at');
        });
        Schema::table('gallery', function ($table) {
            $table->renameColumn('updated_at', 'modified_at');
        });
        Schema::table('gallery_image', function ($table) {
            $table->renameColumn('updated_at', 'modified_at');
        });
        Schema::table('image_meta', function ($table) {
            $table->renameColumn('updated_at', 'modified_at');
        });
        Schema::table('news', function ($table) {
            $table->dateTime('modified_at')->nullable();
        });
        Schema::table('post', function ($table) {
            $table->renameColumn('updated_at', 'modified_at');
        });
        Schema::table('role', function ($table) {
            $table->renameColumn('created_at', 'created');
            $table->renameColumn('updated_at', 'modified');
        });
        Schema::table('social_link', function ($table) {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });
        Schema::table('sport', function ($table) {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });
        Schema::table('team', function ($table) {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });
        Schema::table('user', function ($table) {
            $table->dateTime('modified_at')->nullable();
        });
    }
}
