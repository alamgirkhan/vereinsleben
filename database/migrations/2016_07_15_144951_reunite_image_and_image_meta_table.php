<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReuniteImageAndImageMetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('image', function ($table) {
            $table->string('title')->nullable();
            $table->text('description', 65535)->nullable();
            $table->string('source')->nullable();
            $table->integer('gallery_id')->nullable();
        });
        Schema::drop('image_meta');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('image', function ($table) {
            $table->dropColumn('title');
            $table->dropColumn('description');
            $table->dropColumn('source');
            $table->dropColumn('gallery_id');
        });
        Schema::create('image_meta', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->string('title')->nullable();
            $table->text('description', 65535)->nullable();
            $table->dateTime('created_at')->nullable();
            $table->dateTime('modified_at')->nullable();
            $table->string('source')->nullable();
            $table->integer('gallery_id')->nullable();
        });
    }
}
