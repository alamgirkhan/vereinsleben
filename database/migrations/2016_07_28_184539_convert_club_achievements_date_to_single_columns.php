<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ConvertClubAchievementsDateToSingleColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('club_achievement', function ($table) {
            $table->string('year')->nullable();
            $table->string('month')->nullable();
            $table->string('day')->nullable();
        });

        // migrate the existing data
        DB::statement('UPDATE `club_achievement` SET `year` = YEAR(`date`), `month` = MONTH(`date`), `day` = DAY(`date`)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('club_achievement', function ($table) {
            $table->dropColumn('year');
            $table->dropColumn('month');
            $table->dropColumn('day');
        });
    }
}
