<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCityZipcodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('city_zipcode', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->string('zip', 10);
            $table->float('lat', 10, 0)->nullable();
            $table->float('lon', 10, 0)->nullable();
            $table->integer('city_id');
            $table->string('m_city_id');

            $table->index('zip', 'city_zipcode_zip_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('city_zipcode');
    }
}
