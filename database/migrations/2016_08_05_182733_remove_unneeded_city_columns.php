<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveUnneededCityColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('city', function ($table) {
            $table->dropColumn('zips');
            $table->dropColumn('opengeodb_loc_id');
            $table->dropColumn('name_ascii');

            $table->string('slug');
            $table->string('m_id');

            $table->index('name', 'city_name_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('city', function ($table) {
            $table->text('zips', 65535)->nullable()->after('coords');
            $table->integer('opengeodb_loc_id')->nullable();
            $table->string('name_ascii')->nullable();

            $table->dropColumn('slug');
            $table->dropColumn('m_id');

            $table->dropIndex('city_name_index');
        });
    }
}
