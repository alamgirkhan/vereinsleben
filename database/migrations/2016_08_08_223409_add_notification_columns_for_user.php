<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotificationColumnsForUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user', function (Blueprint $table) {

            $table->string('change_email_unconfirmedEmail')->nullable();

            $table->boolean('notification_introEnabled')->nullable();
            $table->datetime('notification_intro1')->nullable();
            $table->datetime('notification_intro2')->nullable();
            $table->datetime('notification_intro3')->nullable();
            $table->datetime('notification_intro4')->nullable();
            $table->boolean('notification_missingEnabled')->nullable();
            $table->datetime('notification_missing1')->nullable();
            $table->datetime('notification_missing2')->nullable();
            $table->datetime('notification_missing3')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user', function (Blueprint $table) {

            $table->dropColumn('change_email_unconfirmedEmail');

            $table->dropColumn('notification_introEnabled');
            $table->dropColumn('notification_intro1');
            $table->dropColumn('notification_intro2');
            $table->dropColumn('notification_intro3');
            $table->dropColumn('notification_intro4');
            $table->dropColumn('notification_missingEnabled');
            $table->dropColumn('notification_missing1');
            $table->dropColumn('notification_missing2');
            $table->dropColumn('notification_missing3');

        });
    }
}
