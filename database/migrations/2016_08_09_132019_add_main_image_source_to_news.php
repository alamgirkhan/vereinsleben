<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMainImageSourceToNews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('news', function(Blueprint $table) {

            $table->string('main_image_source')->nullable();

        });

        // migrate the existing data
        DB::statement('UPDATE `news` SET `main_image_source` = `m_image_source`');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('news', function(Blueprint $table) {

            $table->dropColumn('main_image_source');

        });
    }
}
