<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeRelationToUserInUserTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_team', function(Blueprint $table) {
            $table->renameColumn('user_club_role_id', 'user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_team', function(Blueprint $table) {
            $table->renameColumn('user_id', 'user_club_role_id');
        });
    }
}
