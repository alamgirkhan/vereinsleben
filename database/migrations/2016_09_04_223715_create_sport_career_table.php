<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSportCareerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sport_career', function ($table) {
            $table->increments('id');
            $table->string('title');
            $table->text('content')->nullable();
            $table->string('timeline_begin', 10)->nullable();
            $table->string('timeline_end', 10)->nullable();
            $table->string('layout')->nullable();
            $table->integer('user_id');
            $table->integer('club_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
            
//            $table->foreign('user_id')->references('id')->on('user')->onDelete('cascade');
//            $table->foreign('club_id')->references('id')->on('club')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sport_career');
    }
}
