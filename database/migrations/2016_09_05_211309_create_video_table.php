<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->string('type');
            $table->string('title')->nullable();
            $table->text('description', 65535)->nullable();
            $table->string('url')->nullable();
            $table->string('identifier')->nullable();
            $table->integer('videoable_id')->nullable();
            $table->string('videoable_type', 64)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('video');
    }
}
