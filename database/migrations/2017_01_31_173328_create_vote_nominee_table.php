<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoteNomineeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vote_nominee', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('vote_id');
            $table->string('voteable_type');
            $table->string('voteable_id');
            $table->string('project');
            $table->text('project_description');
            // @todo check how to solve dynamic pages with dynamic(?) layouts
//            $table->string('slug');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vote_nominee');
    }
}