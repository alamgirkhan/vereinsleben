<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePeriodAtVoteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vote', function (Blueprint $table) {
            $table->dropColumn('period_month');
            $table->dropColumn('period_year');

            $table->date('period_start')->nullable()->after('description');
            $table->date('period_end')->nullable()->after('period_start');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vote', function (Blueprint $table) {
            $table->string('period_month')->nullable()->after('description');
            $table->string('period_year')->nullable()->after('period_month');
            
            $table->dropColumn('period_start');
            $table->dropColumn('period_end');

        });
    }
}
