<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWinnerContentFieldsToVoteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vote', function(Blueprint $table) {
            $table->string('winner_title')->nullable();
            $table->text('winner_description')->nullable();
            $table->string('winner_image_file_name')->nullable();
            $table->integer('winner_image_file_size')->nullable()->after('winner_image_file_name');
            $table->string('winner_image_content_type')->nullable()->after('winner_image_file_size');
            $table->timestamp('winner_image_updated_at')->nullable()->after('winner_image_content_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vote', function(Blueprint $table) {
            $table->dropColumn('winner_title');
            $table->dropColumn('winner_description');
            $table->dropColumn('winner_image_file_name');
            $table->dropColumn('winner_image_file_size');
            $table->dropColumn('winner_image_content_type');
            $table->dropColumn('winner_image_updated_at');
        });
    }
}
