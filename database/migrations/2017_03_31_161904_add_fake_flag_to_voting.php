<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFakeFlagToVoting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_vote_nominee', function(Blueprint $table) {
            $table->tinyInteger('is_fake');
        });

        Schema::table('user', function(Blueprint $table) {
            $table->tinyInteger('has_fake_email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_vote_nominee', function(Blueprint $table) {
            $table->dropColumn('is_fake');
        });
        Schema::table('user', function(Blueprint $table) {
            $table->dropColumn('has_fake_email');
        });
    }
}
