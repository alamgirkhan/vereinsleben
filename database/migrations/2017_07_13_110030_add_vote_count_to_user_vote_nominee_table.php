<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVoteCountToUserVoteNomineeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vote_nominee', function(Blueprint $table) {
            $table->integer('count_voted')->default(0);
            $table->integer('count_cleared')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vote_nominee', function(Blueprint $table) {
            $table->dropColumn('count_voted');
            $table->dropColumn('count_cleared');
        });
    }
}
