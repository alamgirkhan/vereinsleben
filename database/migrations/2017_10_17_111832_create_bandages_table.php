<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBandagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bandages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('shorthand', 64)->nullable();
            $table->integer('founded')->nullable();
            $table->text('description', 65535)->nullable();
            $table->string('street', 128)->nullable();
            $table->string('house_number', 8)->nullable();
            $table->string('city', 128)->nullable();
            $table->string('federal_state', 64)->nullable();
            $table->string('zip', 16)->nullable();
            $table->float('lat', 10, 0)->nullable();
            $table->float('lng', 10, 0)->nullable();
            $table->string('email', 128)->nullable();
            $table->integer('published')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('created_at')->nullable();
            $table->string('slug')->nullable();
            $table->string('type')->nullable();
            $table->string('region')->nullable();
            $table->text('about')->nullable();
            $table->integer('gallery_id')->nullable();
            $table->string('member_count', 32)->nullable();
            $table->string('m_id', 128);
            $table->string('m_owner', 128);
            $table->text('m_sports', 65535)->nullable();
            $table->text('m_achievements', 65535)->nullable();
            $table->text('m_social_links', 65535)->nullable();
            $table->string('m_social_web')->nullable();
            $table->string('m_social_facebook')->nullable();
            $table->string('m_social_twitter')->nullable();
            $table->string('m_social_googleplus')->nullable();
            $table->string('m_social_youtube')->nullable();
            $table->string('m_social_instagram')->nullable();
            $table->string('m_social_flickr')->nullable();
            $table->string('m_avatar_original')->nullable();
            $table->string('m_avatar_singleview')->nullable();
            $table->string('m_avatar_startpage')->nullable();
            $table->string('m_avatar_list')->nullable();
            $table->string('m_avatar_map')->nullable();
            $table->string('m_header_original')->nullable();
            $table->string('m_header_singleview')->nullable();
            $table->integer('parent_id')->nullable();
            $table->string('avatar_file_name')->nullable();
            $table->integer('avatar_file_size')->nullable();
            $table->string('avatar_content_type')->nullable();
            $table->timestamp('avatar_updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bandages');
    }
}
