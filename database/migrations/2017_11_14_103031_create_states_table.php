<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('states', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('constant')->nullable();
            $table->timestamps();
        });

        DB::table('states')->insert(
            [
                ['name' => 'Baden-Württemberg', 'constant' => 'BW'],
                ['name' => 'Bayern', 'constant' => 'BY'],
                ['name' => 'Berlin', 'constant' => 'BE'],
                ['name' => 'Brandenburg', 'constant' => 'BB'],
                ['name' => 'Bremen', 'constant' => 'HB'],
                ['name' => 'Hamburg', 'constant' => 'HH'],
                ['name' => 'Hessen', 'constant' => 'HE'],
                ['name' => 'Mecklenburg-Vorpommern', 'constant' => 'MV'],
                ['name' => 'Niedersachsen', 'constant' => 'NI'],
                ['name' => 'Nordrhein-Westfalen', 'constant' => 'NW'],
                ['name' => 'Rheinland-Pfalz', 'constant' => 'RP'],
                ['name' => 'Saarland', 'constant' => 'SL'],
                ['name' => 'Sachsen', 'constant' => 'SN'],
                ['name' => 'Sachsen-Anhalt', 'constant' => 'ST'],
                ['name' => 'Schleswig-Holstein', 'constant' => 'SH'],
                ['name' => 'Thüringen', 'constant' => 'TH'],
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('states');
    }
}
