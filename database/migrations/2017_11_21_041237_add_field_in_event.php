<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldInEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('event', function (Blueprint $table) {
            $table->tinyInteger('is_tip')->default('0')->after('deleted_at');
            $table->string('info')->nullable()->after('state');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('event', function (Blueprint $table) {
            $table->dropColumn('is_tip');
            $table->dropColumn('info');
        });
    }
}
