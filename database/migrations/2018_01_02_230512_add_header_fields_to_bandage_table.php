<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddHeaderFieldsToBandageTable extends Migration
{

    /**
     * Make changes to the table.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bandages', function(Blueprint $table){

            $table->string('header_file_name')->nullable();
            $table->integer('header_file_size')->nullable()->after('header_file_name');
            $table->string('header_content_type')->nullable()->after('header_file_size');
            $table->timestamp('header_updated_at')->nullable()->after('header_content_type');
            $table->integer('views')->default(0);
        });

    }

    /**
     * Revert the changes to the table.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bandages', function(Blueprint $table){

            $table->dropColumn('header_file_name');
            $table->dropColumn('header_file_size');
            $table->dropColumn('header_content_type');
            $table->dropColumn('header_updated_at');
            $table->dropColumn('views');

        });
    }

}