<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingsortsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainingsorts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 64);
            $table->string('title', 128);
            $table->string('city', 128);
            $table->string('street', 128);
            $table->string('house_number', 8);
            $table->string('federal_state', 64);
            $table->string('zip', 16);
            $table->float('lat', 10, 0);
            $table->float('lng', 10, 0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainingsorts');
    }
}
