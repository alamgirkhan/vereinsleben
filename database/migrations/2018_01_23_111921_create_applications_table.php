<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 128);
            $table->string('description', 128);
            $table->string('start_date', 32);
            $table->string('end_date', 32);
            $table->string('logo', 128);
            $table->string('project_image1', 128);
            $table->string('project_image2', 128)->nullable();
            $table->string('project_image3', 128)->nullable();
            $table->string('project_image4', 128)->nullable();
            $table->string('project_image5', 128)->nullable();
            $table->string('description_image', 128)->nullable();
            $table->string('club_image', 128)->nullable();
            $table->tinyInteger('applied')->default(0);
            $table->tinyinteger('status')->default(0);
            $table->integer('club_id');
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
