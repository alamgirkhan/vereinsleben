<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metas', function(Blueprint $table){
            $table->increments('id');
            $table->string('meta_bild_alt', '500')->nullable()->default(null);
            $table->string('meta_description', '500')->nullable()->default(null);
            $table->string('meta_keywords', '500')->nullable()->default(null);
            $table->string('meta_author', '500')->nullable()->default(null);
            $table->string('meta_language', '500')->nullable()->default(null);
            $table->integer('metaable_id');
            $table->string('metaable_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metas');
    }
}
