<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partner', function(Blueprint $table){
            $table->integer('id', true);
            $table->string('title')->nullable();
            $table->string('sub_title')->nullable();
            $table->string('slug', 128)->nullable();
            $table->text('content_teaser', 65535)->nullable();
            $table->text('content', 65535)->nullable();
            $table->string('main_image_file_name')->nullable();
            $table->integer('main_image_file_size')->nullable();
            $table->string('main_image_content_type')->nullable();
            $table->timestamp('main_image_updated_at')->nullable();
            $table->tinyInteger('published')->nullable()->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('partner');
    }
}
