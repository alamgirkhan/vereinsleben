<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSliderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slider', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 128)->nullable();
            $table->string('description', 256)->nullable();
            $table->string('url', 128)->nullable();
            $table->string('slider_image_file_name', 128)->nullable();
            $table->string('slider_image_file_size', 128)->nullable();
            $table->string('slider_image_content_type', 128)->nullable();
            $table->string('slider_image_updated_at', 128)->nullable();
            $table->tinyInteger('published');
            $table->string('m_id', 128)->nullable();
            $table->string('image', 128)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slider');
    }
}
