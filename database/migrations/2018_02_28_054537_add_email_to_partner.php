<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmailToPartner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('partner', function(Blueprint $table){
            $table->string('email', 255)->nullable()->after('content');
            $table->string('phone', 20)->nullable()->after('email');
            $table->string('address', 255)->nullable()->after('phone');
            $table->string('website', 64)->nullable()->after('address');
            $table->string('logo_file_name')->nullable()->after('website');
            $table->integer('logo_file_size')->nullable()->after('logo_file_name');
            $table->string('logo_content_type')->nullable()->after('logo_file_size');
            $table->timestamp('logo_updated_at')->nullable()->after('logo_content_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('partner', function(Blueprint $table){
            $table->dropColumn('email');
            $table->dropColumn('phone');
            $table->dropColumn('address');
            $table->dropColumn('website');
            $table->dropColumn('logo_file_name');
            $table->dropColumn('logo_file_size');
            $table->dropColumn('logo_content_type');
            $table->dropColumn('logo_updated_at');
        });
    }
}
