<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDelusersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delusers', function(Blueprint $table) {
            $table->integer('id', true);
            $table->integer('user_id')->notnull();
            $table->string('deleted_token')->notnull();
            $table->string('email', 128)->nullable();
            $table->String('username', 64)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        chema::drop('delusers');
    }
}
