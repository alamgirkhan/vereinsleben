<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHeaderColorToClub extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('club', function(Blueprint $table){
            $table->string('header_color', 12)->nullable()->after('header_updated_at');
            $table->string('title_color', 12)->nullable()->after('header_color');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('club', function(Blueprint $table){
            $table->dropColumn('header_color');
            $table->dropColumn('title_color');
        });
    }
}
