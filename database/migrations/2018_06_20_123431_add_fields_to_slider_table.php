<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToSliderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('slider', function(Blueprint $table){
            //
            $table->text('h2')->nullable()->after('updated_at');
            $table->text('h3')->nullable()->after('h2');
            $table->string('btn')->nullable()->after('h3');
            $table->string('logo1_file_name')->nullable()->after('btn');
            $table->integer('logo1_file_size')->nullable()->after('logo1_file_name');
            $table->string('logo1_content_type')->nullable()->after('logo1_file_size');
            $table->timestamp('logo1_updated_at')->nullable()->after('logo1_content_type');
            $table->string('logo2_file_name')->nullable()->after('logo1_updated_at');
            $table->integer('logo2_file_size')->nullable()->after('logo2_file_name');
            $table->string('logo2_content_type')->nullable()->after('logo2_file_size');
            $table->timestamp('logo2_updated_at')->nullable()->after('logo2_content_type');
            $table->string('logo3_file_name')->nullable()->after('logo2_updated_at');
            $table->integer('logo3_file_size')->nullable()->after('logo3_file_name');
            $table->string('logo3_content_type')->nullable()->after('logo3_file_size');
            $table->timestamp('logo3_updated_at')->nullable()->after('logo3_content_type');
            $table->string('logo4_file_name')->nullable()->after('logo3_updated_at');
            $table->integer('logo4_file_size')->nullable()->after('logo4_file_name');
            $table->string('logo4_content_type')->nullable()->after('logo4_file_size');
            $table->timestamp('logo4_updated_at')->nullable()->after('logo4_content_type');
            $table->string('logo5_file_name')->nullable()->after('logo4_updated_at');
            $table->integer('logo5_file_size')->nullable()->after('logo5_file_name');
            $table->string('logo5_content_type')->nullable()->after('logo5_file_size');
            $table->timestamp('logo5_updated_at')->nullable()->after('logo5_content_type');
            $table->string('logo6_file_name')->nullable()->after('logo5_updated_at');
            $table->integer('logo6_file_size')->nullable()->after('logo6_file_name');
            $table->string('logo6_content_type')->nullable()->after('logo6_file_size');
            $table->timestamp('logo6_updated_at')->nullable()->after('logo6_content_type');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('slider', function(Blueprint $table){
            //
            $table->dropColumn('h2');
            $table->dropColumn('h3');
            $table->dropColumn('btn');
            $table->dropColumn('logo1_file_name');
            $table->dropColumn('logo1_file_size');
            $table->dropColumn('logo1_content_type');
            $table->dropColumn('logo1_updated_at');
            $table->dropColumn('logo2_file_name');
            $table->dropColumn('logo2_file_size');
            $table->dropColumn('logo2_content_type');
            $table->dropColumn('logo2_updated_at');
            $table->dropColumn('logo3_file_name');
            $table->dropColumn('logo3_file_size');
            $table->dropColumn('logo3_content_type');
            $table->dropColumn('logo3_updated_at');
            $table->dropColumn('logo4_file_name');
            $table->dropColumn('logo4_file_size');
            $table->dropColumn('logo4_content_type');
            $table->dropColumn('logo4_updated_at');
            $table->dropColumn('logo5_file_name');
            $table->dropColumn('logo5_file_size');
            $table->dropColumn('logo5_content_type');
            $table->dropColumn('logo5_updated_at');
            $table->dropColumn('logo6_file_name');
            $table->dropColumn('logo6_file_size');
            $table->dropColumn('logo6_content_type');
            $table->dropColumn('logo6_updated_at');

        });
    }
}
