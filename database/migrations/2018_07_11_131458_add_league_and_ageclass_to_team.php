<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLeagueAndAgeclassToTeam extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('team', function(Blueprint $table){
            $table->string('league')->after('contact_email')->nullable();
            $table->string('age_class')->after('league')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('team', function(Blueprint $table){
            $table->dropColumn('league');
            $table->dropColumn('age_class');
        });
    }
}
