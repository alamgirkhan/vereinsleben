<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserClubToContest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contest', function(Blueprint $table){
            $table->string('user_firstname')->after('user_id')->nullable();
            $table->string('user_lastname')->after('user_firstname')->nullable();
            $table->string('club_name')->after('club_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contest', function(Blueprint $table){
            $table->dropColumn('user_firstname');
            $table->dropColumn('user_lastname');
            $table->dropColumn('club_name');
        });
    }
}
