<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsernameClubslugToContest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contest', function(Blueprint $table){
            $table->string('username')->after('user_lastname')->nullable();
            $table->string('clubslug')->after('club_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contest', function(Blueprint $table){
            $table->dropColumn('username');
            $table->dropColumn('clubslug');
        });
    }
}
