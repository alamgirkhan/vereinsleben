<?php
	
	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;
	
	class CreateCampaignTable extends Migration
	{
		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up()
		{
			Schema::create('campaign', function (Blueprint $table) {
				$table->increments('id');
				$table->string('name')->nullable();
				$table->string('customers_name')->nullable();
				$table->string('link')->nullable();
				$table->string('description')->nullable();
				$table->string('image_1')->nullable();
				$table->string('image_2')->nullable();
				$table->string('image_3')->nullable();
				$table->string('create_by')->nullable();
				$table->dateTime('published_from')->nullable();
				$table->dateTime('published_to')->nullable();
				$table->timestamps();
			});
		}
		
		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down()
		{
			Schema::dropIfExists('campaign');
		}
	}
