<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProposeSongTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propose_song', function (Blueprint $table) {
	        $table->increments('id');
	        $table->string('title_song')->nullable();
	        $table->string('artist_name')->nullable();
	        $table->string('first_name')->nullable();
	        $table->string('last_name')->nullable();
	        $table->string('email')->nullable();
	        $table->integer('club_id')->nullable();
	        $table->string('club_name')->nullable();
	        $table->string('club_slug')->nullable();
	        $table->string('username')->nullable();
	        $table->string('user_id')->nullable();
	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('propose_song');
    }
}
