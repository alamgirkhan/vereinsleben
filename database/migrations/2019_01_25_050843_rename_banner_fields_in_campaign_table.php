<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameBannerFieldsInCampaignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('campaign', function(Blueprint $table) {
		    $table->renameColumn('image_1', 'banner_horizontal');
		    $table->renameColumn('image_2', 'banner_rechteck');
		    $table->renameColumn('image_3', 'banner_vertikal');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('campaign', function(Blueprint $table) {
		    $table->renameColumn('banner_horizontal', 'image_1');
		    $table->renameColumn('banner_rechteck', 'image_2');
		    $table->renameColumn('banner_vertikal', 'image_3');
	    });
    }
}
