<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBandageSportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bandage_sport', function (Blueprint $table) {
            $table->increments('id');
	        $table->integer('bandage_id')->nullable();
	        $table->integer('sport_id')->nullable();
	        $table->string('m_club_id', 128)->nullable();
	        $table->string('m_sport_id', 128)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bandage_sport');
    }
}
