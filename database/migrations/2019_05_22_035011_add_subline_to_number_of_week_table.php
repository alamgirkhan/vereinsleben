<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSublineToNumberOfWeekTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('number_of_week', function(Blueprint $table){
		    $table->string('sub_line')->nullable()->after('opacity');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('number_of_week', function(Blueprint $table){
		    $table->dropColumn('sub_line');
	    });
    }
}
