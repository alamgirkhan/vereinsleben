<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToWesternRewardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('western_reward', function(Blueprint $table){
		    $table->string('fullname')->nullable()->after('lastname');
		    $table->string('username')->nullable()->after('fullname');
		    $table->integer('user_id')->nullable()->after('id');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('western_reward', function(Blueprint $table){
		    $table->dropColumn('fullname');
		    $table->dropColumn('username');
		    $table->dropColumn('user_id');
	    });
    }
}
