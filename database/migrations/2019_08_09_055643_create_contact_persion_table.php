<?php
	
	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;
	
	class CreateContactPersionTable extends Migration
	{
		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up()
		{
			Schema::create('contact_persion', function (Blueprint $table) {
				$table->increments('id');
				$table->integer('bandage_id');
				$table->string('department');
				$table->string('gender');
				$table->string('name');
				$table->string('position');
				$table->string('phone');
				$table->string('email');
				$table->softDeletes();
				$table->timestamps();
			});
		}
		
		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down()
		{
			Schema::dropIfExists('contact_persion');
		}
	}
