<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpitzensportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spitzensports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
			$table->string('sub_title')->nullable();
			$table->string('slug', 128)->nullable();
			$table->text('content', 65535)->nullable();
			$table->text('content_teaser', 65535)->nullable();
			$table->boolean('exclusive')->default(false);
			$table->timestamps();
			$table->softDeletes();
			$table->dateTime('published_at')->nullable();
			$table->dateTime('published_from')->nullable();
			$table->dateTime('published_until')->nullable();
			$table->string('element_order', 128)->nullable();
			$table->integer('user_id')->nullable();
			$table->integer('category_id')->nullable();
			$table->string('state', 32)->nullable();
			$table->integer('owner_id')->nullable();
			$table->integer('gallery_id')->nullable();
			$table->string('m_category', 64)->nullable();
			$table->string('m_owner', 64)->nullable();
			$table->string('m_author', 64)->nullable();
			$table->string('m_label', 64)->nullable();
			$table->string('full_name', 64)->nullable();
			$table->string('profession', 64)->nullable();
			$table->string('m_image_singleview')->nullable();
			$table->string('m_image_original')->nullable();
			$table->string('m_image_source')->nullable();
			$table->string('m_image_startpage')->nullable();
			$table->string('m_id', 64)->nullable();
			$table->string('main_image_file_name')->nullable();
			$table->integer('main_image_file_size')->nullable();
			$table->integer('main_image_content_type')->nullable();
			$table->timestamp('main_image_updated_at')->nullable();
			$table->string('main_image_source', 255)->nullable();
			$table->integer('club_id')->nullable();
			$table->boolean('is_popular')->default(false);
			$table->boolean('is_featured')->default(false);
			$table->boolean('has_gallery')->default(false);
			$table->text('steckbrief')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spitzensports');
    }
}
