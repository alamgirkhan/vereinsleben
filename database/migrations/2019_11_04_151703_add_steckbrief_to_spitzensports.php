<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSteckbriefToSpitzensports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('spitzensports', function (Blueprint $table) {
            $table->text('steckbrief')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('spitzensports', function (Blueprint $table) {
            $table->dropColumn(['steckbrief']);
        });
    }
}
