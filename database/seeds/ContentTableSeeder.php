<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('content')->where('slug', 'imprint')->delete();
      DB::table('content')->updateOrInsert([
        'title' => 'Imprint',
        'content' => '<p>
    Impressum
    <br/>
    Rheinland-Pfälzische Rundfunk GmbH & co. KG
    <br/>
    Turmstraße 10
    <br/>
    67059 Ludwigshafen
    <br/>
    Telefon: +49 (0) 621 / 59000 - O Telefax: +49 (0) 621 | 622750
    <br/>
    E-Mail: info@rprl.de
    <br/>
    Registergericht Ludwigshafen, HRA 3373
    <br/>
    USt-lD: DE 149 116 004
    <br/>Vertretungsberechtigung und persönlich haftende Gesellschafterin: Rheinland-Pfälzische Rundfunkbetriebsgesellschaft mbH Geschäftsführer: Kristian Kropp Beiratsvorsitzender: Dr. Oliver C. Dubber Registergericht Ludwigshafen, HRB 3082
    <br/>
    <br/>
    Informationen zum Datenschutz erhalten Sie in unseren <a href="https://www.vereinsleben.de">Datenschutzhinweisen</a>.
    <br/>
    <br/>
    <a href="https://www.vereinsleben.de">https://www.vereinsleben.de</a>
</p>',
        'slug' => 'imprint',
        'meta_title' => '',
        'meta_keywords' => '',
        'meta_description' => ''
      ]);
    }
}
