<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Faker\Factory;

use Vereinsleben\Club;
use Vereinsleben\VoteNominee;
use Vereinsleben\Vote;


class DummyVoteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        if (App::environment('local', 'staging')) {

            $vote = new Vote;
            $voteNominee = new VoteNominee;
            $faker = Faker\Factory::create('de_DE');
            $clubs = Club::published()->inRandomOrder()->take(10)->get();
            $now = Carbon::now();
            $start = $now->format('Y-m-d');
            $end = $now->addDays(14)->format('Y-m-d');

            $voteNominee->truncate();
            $vote->truncate();

            $createdVote = $vote->create([
                'name' => 'Eure Vereinshelden',
                'pre_name' => 'Bewerbt euch jetzt!',
                'pre_description' => $faker->realText(rand(150, 450)),
                'description' => $faker->realText(rand(150, 450)),
                'teaser_headline' => 'Macht mit bei der Wahl zum Verein des Monats',
                'teaser' => $faker->realText(rand(150, 450)),
                'period_start' => $start,
                'period_end' => $end,
            ]);

            $clubs->each(function ($club) use ($voteNominee, $createdVote, $faker) {
                $voteNominee->create([
                    'vote_id' => $createdVote->id,
                    'voteable_type' => 'Club',
                    'voteable_id' => $club->id,
                    'project' => $faker->sentence(rand(3, 7), true),
                    'project_description' => $faker->realText(rand(150, 450)),
                ]);
            });


        }
    }
}
