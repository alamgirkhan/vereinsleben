<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
	use Vereinsleben\Option;
	
	class OptionsTableSeeder extends Seeder
{
	protected $options = [
		[
			'option_name' => 'banner_spacing_in_the_stream',
			'option_value' => 5,
		],
	];
	
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('options')->truncate();
		DB::table('options')->insert($this->options);
	}
}
