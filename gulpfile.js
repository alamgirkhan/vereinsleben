var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
elixir.config.css.cssnano.pluginOptions.autoprefixer = false;

elixir.config.css.autoprefix.options = {
    browsers: ['last 2 versions', 'Safari >= 8'],
    cascade: false,
    remove: false
};


elixir(function (mix) {
    mix.sass('app.scss')
       .sass('vendor.scss')
       .browserify('main.js')
       .browserify('user-profile/index.js', 'public/js/user-profile.js')
       .browserify('vote/vote.js', 'public/js/vote.js')
       .browserify('form/form.js', 'public/js/form.js')
       .browserify('user/index.js', 'public/js/user-stream.js')
       .browserify('admin/index.js', 'public/js/admin.js')
       .browserify('chat/index.js', 'public/js/chat.js')
       .browserify('vendor/jscolor/jscolor.js', 'public/js/jscolor.js')
       .scripts([
            'vendor/jquery/jquery.min.js',
            'vendor/typeahead.js/typeahead.bundle.js',
            'vendor/hypher/jquery.hypher.js',
            'vendor/hypher/de.js',
            'vendor/slick-carousel/slick.min.js',
            'vendor/moment.js/moment.min.js',
            'vendor/moment.js/locale/de.js',
            'vendor/datatables/jquery.dataTables.min.js',
            'vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',
            'vendor/select2/select2.min.js',
            'vendor/select2/locale/de.js',
            'vendor/fast-levenshtein/levenshtein.js',
            'vendor/classie/classie.js',
            'vendor/modernizr/modernizr.custom.js',
            'vendor/jquery/jquery.dlmenu.js',
           'vendor/jquery/jquery-ui.js',
           'vendor/lightbox/lightbox.min.js',
        ], 'public/js/vendor.js')
       .scripts('vendor/google/tag-manager.js', 'public/js/vendor-head.js');

    mix.copy('resources/assets/js/vendor/ckeditor', 'public/js/vendor/ckeditor');
    mix.copy('resources/assets/js/vendor/i18n/dataTables.german.json', 'public/js/vendor/i18n/dataTables.german.json');

    if (elixir.config.production) {
        // prod only
        mix.version(["css/app.css", "css/vendor.css", "js/vendor.js", "js/main.js", "js/user-profile.js", "js/user-stream.js"]);
    } else {
        // dev only
    }
});
