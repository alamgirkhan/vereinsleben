import Request from './Request';

class ClubHandler {

    constructor() {
        this.handles = {
            delete: '[data-club-delete]'
        };
    }

    registerDeleteHandles() {
        let deleteHandles = Array.from(document.querySelectorAll(`[data-club-delete]`));

        if (deleteHandles.length > 0) {
            deleteHandles.map((deleteHandle) => {
                deleteHandle.addEventListener('click', this.handleAction, false);
            });
        }
    }

    handleAction(evt) {
        let deleteConfirm = confirm('Möchtest du diese Aufzeichnung wirklich löschen?');
        if (deleteConfirm) {
            const tableRow = $(evt.currentTarget).closest('tr');

            evt.preventDefault();

            let url = evt.currentTarget.dataset.clubDelete;
            let req = Request.http();

            req.patch(url).then((res) => {
                tableRow.fadeOut();
            });
        }
    }
}

export default ClubHandler;