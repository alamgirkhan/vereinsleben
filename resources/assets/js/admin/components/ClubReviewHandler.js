import axios from 'axios';

class ClubReviewHandler {

    constructor() {
        this.state = {
            mappingId: null,
            clubId: null,
        };
        
        if(document.querySelector('[data-token]')) {
            this.http = axios.create({
                headers: {
                    'X-CSRF-TOKEN': document.querySelector('[data-token]').dataset.token,
                    'X-Requested-With': 'XMLHttpRequest',
                },
            });
        }

    }

    registerHandles() {
        this.editHandles = Array.from(document.querySelectorAll('[data-review-club]'));

        if (this.editHandles.length > 0) {
            this.editHandles.map((editHandle) => {
                editHandle.addEventListener('click', this.handleEdit.bind(this), false);
            });
        }
    };

    handleEdit(evt) {
        const tableRow = $(evt.currentTarget).closest('tr');
        const mappingId = tableRow.find('[data-club-mapping]').val();
        const clubId = $(evt.currentTarget).data('reviewClub');

        if (this.state.clubId != clubId && this.state.mappingId != mappingId) {
            this.http.patch('/admin/club/review', {
                club_id: clubId,
                map_club_id: mappingId
            }).then((res) => {
                tableRow.fadeOut();
            });
        }

        this.state = {
            mappingId: mappingId,
            clubId: clubId
        };

        evt.preventDefault();
    }
}

export default ClubReviewHandler;