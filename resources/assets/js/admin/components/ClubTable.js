import ClubReviewHandler from './ClubReviewHandler';
import ClubHandler from './ClubHandler';

class ClubTable {

    constructor() {
        this.sportReviewHandler = new ClubReviewHandler;
        this.sportHandler = new ClubHandler;
        this.tables = $('[data-club-table]');

        this.handleTables();
        this.handleAutoComplete();
    }

    handleTables() {
        this.tables.each((key, table) => {
            (function (table) {
                this.renderTable(table);
            }.bind(this))(table);
        })
    }

    renderTable(table) {
        $.fn.dataTable.ext.errMode = 'throw';
        $(table).DataTable({
            ajax: {
                url: $(table).data('clubTable')
            },
            processing: true,
            serverSide: true,
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'name_mapping', name: 'name_mapping', orderable: false, searchable: false},
                {data: 'options', name: 'options', orderable: false, searchable: false},
            ],
            language: {
                url: '/js/vendor/i18n/dataTables.german.json'
            },
            bLengthChange: false,
            drawCallback: function (settings) {
                this.handleAutoComplete();
                this.sportReviewHandler.registerHandles();
                this.sportHandler.registerDeleteHandles();
            }.bind(this)
        });
    }

    handleAutoComplete() {
        $('[data-club-mapping]').each((key, element) => {
            this.initializeAutoComplete(element);
        });
    }

    initializeAutoComplete(element) {
        $(element).select2({
            language: 'de',
            tags: true,
            minimumInputLength: 1,
            dropdownAutoWidth: false,
            width: 'resolve',
            cache: true,
            placeholder: {
                id: -1,
                text: 'Behalten'
            },
            ajax: {
                url: $('[data-mapping-url]').data('mappingUrl'),
                data: function (params) {
                    return {
                        search: params.term
                    };
                },
                dataType: 'json',
                delay: 250,
                processResults: function (data, params) {
                    var results = data['data'].map(function (element) {
                        return {
                            id: element.id,
                            text: element.name,
                            avatar: element.images.avatar.startPage
                        }
                    });

                    const sorter = function (a, b) {
                        let input_string = params.term;
                        if (input_string.length > 3) {
                            return Levenshtein.get(a.text, input_string) - Levenshtein.get(b.text, input_string);
                        }
                    };

                    results.sort(sorter);

                    return {
                        results: results
                    };
                },
            },
            templateResult: function (state) {
                let data = {
                    avatar: '/default_images/club/default-avatars-startPage.jpg',
                    text: state.text
                };

                if (state.hasOwnProperty('id') && Number(state.id) > 0 && typeof state.avatar !== 'undefined') {
                    data.avatar = state.avatar;
                }

                return $(
                    `<div class="profile-athletic-runway__club">
                            <div class="profile-athletic-runway__club-image-wrapper">
                                <img src="${data.avatar}" class="profile-athletic-runway__club-image" />
                            </div>
                            <span>${data.text}</span>
                        </div>`
                );
            },
        });
    };
}

export default ClubTable;