import Request from './Request';

class NewsHandler {

    constructor() {
        this.handles = {
            actions: {
                edit: {
                    selector: '[data-news-edit]',
                },
                delete: {
                    selector: '[data-news-delete]'
                }
            },
            registered: []
        };
    }

    registerHandles() {
        for (let action in this.handles.actions) {
            if (this.handles.actions.hasOwnProperty(action)) {
                this.registerHandle(action);
            }
        }
    }

    registerHandle(action) {
        let handles = Array.from(document.querySelectorAll(`${this.handles.actions[action].selector}`));

        if (handles.length > 0) {
            handles.map((handle) => {
                handle.addEventListener('click', (evt) => {
                    evt.preventDefault();

                    let registeredHandles = this.handles.registered;
                    let identifier = evt.currentTarget.dataset['handleUrl'];

                    if (registeredHandles.indexOf(identifier) === -1) {
                        this.handleAction(evt, action);

                        this.handles.registered.push(evt.currentTarget.dataset['handleUrl']);
                    }
                });
            });
        }
    }

    handleAction(evt, action) {
        const tableRow = $(evt.currentTarget).closest('tr');
        const mappingId = tableRow.find('[data-news-mapping]').val();
        const url = evt.currentTarget.dataset['handleUrl'];
        const req = Request.http();

        switch (action) {
            case 'edit':
                req.patch(url, {
                    map_id: mappingId
                }).then((res) => {
                    tableRow.fadeOut();
                });
                break;
            case 'delete':
                req.patch(url).then((res) => {
                    tableRow.fadeOut();
                });
                break;

        }
    }

}

export default NewsHandler;