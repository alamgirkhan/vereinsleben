import axios from 'axios';

class NewsReviewHandler {

    constructor() {
        this.state = {
            mappingId: null,
            newsId: null,
        };

        this.http = axios.create({
            headers: {
                'X-CSRF-TOKEN': document.querySelector('[data-token]').dataset.token,
                'X-Requested-With': 'XMLHttpRequest',
            },
        });

    }

    registerHandles() {
        this.editHandles = Array.from(document.querySelectorAll('[data-review-news]'));

        if (this.editHandles.length > 0) {
            this.editHandles.map((editHandle) => {
                editHandle.addEventListener('click', this.handleEdit.bind(this), false);
            });
        }
    };

    handleEdit(evt) {
        const tableRow = $(evt.currentTarget).closest('tr');
        const mappingId = tableRow.find('[data-news-mapping]').val();
        const newsId = $(evt.currentTarget).data('reviewNews');

        if (this.state.newsId != newsId && this.state.mappingId != mappingId) {
            this.http.patch('/admin/news/review', {
                sport_id: newsId,
                map_sport_id: mappingId
            }).then((res) => {
                tableRow.fadeOut();
            });
        }

        this.state = {
            mappingId: mappingId,
            sportId: newsId
        };

        evt.preventDefault();
    }
}

export default NewsReviewHandler;