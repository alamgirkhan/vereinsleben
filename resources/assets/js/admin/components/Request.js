import axios from 'axios';

class Request {
    constructor() {
    }

    static http() {
        return axios.create({
            headers: {
                'X-CSRF-TOKEN': document.querySelector('[data-token]').dataset.token,
                'X-Requested-With': 'XMLHttpRequest',
            },
        });
    }
}

export default Request;