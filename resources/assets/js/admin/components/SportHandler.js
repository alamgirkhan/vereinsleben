import Request from './Request';

class SportHandler {

    constructor() {
        this.handles = {
            actions: {
                review: {
                    selector: '[data-sport-review]',
                },
                delete: {
                    selector: '[data-sport-delete]'
                }
            },
            registered: []
        };
    }

    registerHandles() {
        for (let action in this.handles.actions) {
            if (this.handles.actions.hasOwnProperty(action)) {
                this.registerHandle(action);
            }
        }
    }

    registerHandle(action) {
        let handles = Array.from(document.querySelectorAll(`${this.handles.actions[action].selector}`));

        if (handles.length > 0) {
            handles.map((handle) => {
                handle.addEventListener('click', (evt) => {
                    evt.preventDefault();

                    let registeredHandles = this.handles.registered;
                    let identifier = evt.currentTarget.dataset['handleUrl'];

                    if (registeredHandles.indexOf(identifier) === -1) {
                        this.handleAction(evt, action);

                        this.handles.registered.push(evt.currentTarget.dataset['handleUrl']);
                    }
                });
            });
        }
    }

    handleAction(evt, action) {
        const tableRow = $(evt.currentTarget).closest('tr');
        const mappingId = tableRow.find('[data-sport-mapping]').val();
        const url = evt.currentTarget.dataset['handleUrl'];
        const req = Request.http();

        switch (action) {
            case 'review':
                req.patch(url, {
                    map_id: mappingId
                }).then((res) => {
                    tableRow.fadeOut();
                });
                break;
            case 'delete':
                let deleteConfirm = confirm('Möchtest du diese Aufzeichnung wirklich löschen?');
                if (deleteConfirm) {
                    req.patch(url).then((res) => {
                        tableRow.fadeOut();
                    });
                }
                break;

        }
    }

}

export default SportHandler;