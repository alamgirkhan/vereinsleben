import axios from 'axios';

class SportReviewHandler {

    constructor() {
        this.state = {
            mappingId: null,
            sportId: null,
        };

        this.http = axios.create({
            headers: {
                'X-CSRF-TOKEN': document.querySelector('[data-token]').dataset.token,
                'X-Requested-With': 'XMLHttpRequest',
            },
        });

    }

    registerHandles() {
        this.editHandles = Array.from(document.querySelectorAll('[data-review-sport]'));

        if (this.editHandles.length > 0) {
            this.editHandles.map((editHandle) => {
                editHandle.addEventListener('click', this.handleEdit.bind(this), false);
            });
        }
    };

    handleEdit(evt) {
        const tableRow = $(evt.currentTarget).closest('tr');
        const mappingId = tableRow.find('[data-sport-mapping]').val();
        const sportId = $(evt.currentTarget).data('reviewSport');

        if (this.state.sportId != sportId && this.state.mappingId != mappingId) {
            this.http.patch('/admin/sport/review', {
                sport_id: sportId,
                map_sport_id: mappingId
            }).then((res) => {
                tableRow.fadeOut();
            });
        }

        this.state = {
            mappingId: mappingId,
            sportId: sportId
        };

        evt.preventDefault();
    }
}

export default SportReviewHandler;