import SportHandler from './SportHandler';

class SportTable {

    constructor() {
        this.sportHandler = new SportHandler;
        this.tables = $('[data-sport-table]');

        this.handleTables();
        this.handleAutoComplete();
    }

    handleTables() {
        this.tables.each((key, table) => {
            (function (table) {
                this.renderTable(table);
            }.bind(this))(table);
        })
    }

    renderTable(table) {
        $(table).DataTable({
            ajax: {
                url: $(table).data('sportTable')
            },
            processing: true,
            serverSide: true,
            columns: [
                {data: 'id', name: 'id'},
                {data: 'title', name: 'title'},
                {data: 'name_mapping', name: 'name_mapping', orderable: false, searchable: false},
                {data: 'options', name: 'options', orderable: false, searchable: false},
            ],
            language: {
                url: '/js/vendor/i18n/dataTables.german.json'
            },
            bLengthChange: false,
            drawCallback: function (settings) {
                this.sportHandler.registerHandles();
                this.handleAutoComplete();
            }.bind(this)
        });
    }

    handleAutoComplete() {
        $('[data-sport-mapping]').each((key, element) => {
            this.initializeAutoComplete(element);
        });
    }

    initializeAutoComplete(element) {
        $(element).select2({
            language: 'de',
            minimumInputLength: 1,
            dropdownAutoWidth: false,
            width: 'resolve',
            cache: true,
            placeholder: {
                id: -1,
                text: 'Behalten'
            },
            ajax: {
                url: $('[data-mapping-url]').data('mappingUrl'),
                data: function (params) {
                    return {
                        search: params.term
                    };
                },
                dataType: 'json',
                delay: 250,
                processResults: function (data, params) {
                    var results = data['data'].map(function (element) {
                        return {
                            id: element.id,
                            text: element.title,
                        }
                    });

                    const sorter = function (a, b) {
                        let input_string = params.term;
                        if (input_string.length > 3) {
                            return Levenshtein.get(a.text, input_string) - Levenshtein.get(b.text, input_string);
                        }
                    };

                    results.sort(sorter);

                    return {
                        results: results
                    };
                },
            },
            escapeMarkup: function (markup) {
                return markup;
            },
        });
    };
}

export default SportTable;