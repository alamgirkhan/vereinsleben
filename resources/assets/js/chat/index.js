import axios from 'axios';
import Vue from 'vue/dist/vue';
import VueRouter from 'vue-router';
import Message from './components/message';
import moment from 'moment';
import Echo from "laravel-echo";

window.io = require('socket.io-client');
if (typeof io !== 'undefined') {
    window.Echo = new Echo({
        broadcaster: 'socket.io',
        namespace  : 'Vereinsleben.Events',
        host       : window.location.hostname + ':6001',
    });
}

// window.Pusher = require('pusher-js');
//
// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key        : 'f57cdebc494d4f5f5d21',
//     namespace  : 'Vereinsleben.Events',
//     cluster    : 'eu',
//     encrypted  : true
// });

Vue.use(VueRouter);
Vue.component('message', Message);

var router = new VueRouter({
    mode  : 'history',
    routes: [
        {path: '/chat/:username'}
    ]
});

const app = new Vue({
    el     : '#chat',
    router,
    data   : {
        message       : '',
        receivedUser  : '',
        conversationId: '',
        chatting      : {
            messages: []
        },
    },
    mounted: function () {
        this.receivedUser = this.$route.params.username;
        axios.get(`${this.receivedUser}/messages`).then(response => {
            this.conversationId = response.data.conversationId;
            if (response.data.messages) {
                this.chatting.messages = response.data.messages.messages;
            }

            window.Echo.private(`chat.${this.conversationId}`)
                  .listen('ChatEvent', (e) => {
                      this.chatting.messages.push(e.message);
                      $('#messagewindow').animate({
                          scrollTop: $('#messagewindow')[0].scrollHeight
                      }, 1000);
                  });
        });
    },

    methods: {
        send() {
            var m = this.message;
            var clubs = $('#clubs').val();
            console.log(clubs);
            var members = $('#members').val();
            console.log(members);
            var fans = $('#fans').val();
            console.log(fans);
            this.message = '';

            if (m.length > 0) {
                axios.post(`${this.receivedUser}/store`, {
                    message: m,
                    clubs  : clubs,
                    members: members,
                    fans   : fans
                }).then(response => {
                });			
            }
        },
    },

    filters: {
        formatDate: function (value) {
            if (value) {
                return moment(String(value)).format('DD.MM.YYYY - HH:mm');
            }
        }
    }
	
});
//export default app;