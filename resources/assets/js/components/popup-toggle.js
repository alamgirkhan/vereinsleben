import EventBus from '../utils/EventBus';

export default {
    props: ['label'],
    template: `
        <button @click="toggle" class="btn btn--default btn--upper btn--large btn--full-width">
            {{ label }}
        </button>
    `,
    methods: {
        toggle() {
            EventBus.$emit('toggle-modal');
        }
    }
}