import Club from '../../models/Club';
import Select2Initializer from '../../utils/select2-initializer';
import FormError from './form-error';
import EventBus from '../utils/EventBus';

const FormClub = {
    template: `
        <form method="POST">
        
            <h1 class="hl hl--large hl--underlined hl--inverted hl--centered hl--upper">Informationen über den Verein</h1>
            <p class="text-block text-block--compressed">Das Wichtigste natürlich zuerst: Alle relevanten Informationen über den Verein, denn um den geht es ja schließlich.</p>
        
            <div class="row">
                <div class="col-md-12">
                    <label for="name" class="input-label">Vereinsname *</label>
                    <div class="input-group" :class="errors && errors.name ? 'input-group--error' : ''">
                        <input v-model="club.name" required="required" class="input" placeholder="Vereinsname" title="Vereinsname" name="name" type="text" id="name">
                        <form-error :error="errors.name" v-if="errors && errors.name"></form-error>
                    </div>
                </div>
                <div class="col-md-9">
                    <label for="street" class="input-label">Straße *</label>
                    <div class="input-group" :class="errors && errors.street ? 'input-group--error' : ''" >
                        <input v-model="club.street" required="required" class="input" placeholder="Straße" title="Straße" name="street" type="text" id="street">
                        <form-error :error="errors.street" v-if="errors && errors.street"></form-error>
                    </div>
                </div>
                <div class="col-md-3">
                    <label for="house_number" class="input-label">Hausnummer *</label>
                    <div class="input-group" :class="errors && errors.house_number ? 'input-group--error' : ''">
                        <input v-model="club.house_number" class="input" placeholder="Hausnummer" title="Hausnummer" name="house_number" type="text" id="house_number">
                        <form-error :error="errors.house_number" v-if="errors && errors.house_number"></form-error>
                    </div>
                </div>
                <div class="col-md-3">
                    <label for="zip" class="input-label">Postleitzahl *</label>
                    <div class="input-group" :class="errors && errors.zip ? 'input-group--error' : ''">
                        <input v-model="club.zip" class="input" placeholder="PLZ" title="PLZ" name="zip" type="text" id="zip">
                        <form-error :error="errors.zip" v-if="errors && errors.zip"></form-error>
                    </div>
            
                </div>
                <div class="col-md-9">
                    <label for="city" class="input-label">Stadt *</label>
                    <div class="input-group" :class="errors && errors.city ? 'input-group--error' : ''">
                        <input v-model="club.city" required="required" class="input" placeholder="Stadt" title="Stadt" name="city" type="text" id="city">
                        <form-error :error="errors.city" v-if="errors && errors.city"></form-error>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-4">
                    <div class="club-detail__overview-item">
                        <label for="founded" class="input-label">Gegründet</label>
                        <div class="input-group">
                            <input v-model="club.founded" class="input" placeholder="z.B. 2017" title="Gegründet" name="founded" type="text" id="founded">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="club-detail__overview-item">
                        <label for="shorthand" class="input-label">Kürzel</label>
                        <div class="input-group">
                            <input v-model="club.shorthand" class="input" placeholder="Kürzel" title="Kürzel" name="shorthand" type="text" id="shorthand">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="club-detail__overview-item">
                        <label for="member_count" class="input-label">Mitglieder *</label>
                        <div class="input-group" :class="errors && errors.member_count ? 'input-group--error' : ''">
                            <select v-model="club.member_count" class="select" title="Mitglieder" id="member_count" name="member_count">
                                <option value="">Anzahl auswählen</option>
                                <option value="1-10">1-10</option>
                                <option value="11-50">11-50</option>
                                <option value="51-100">51-100</option>
                                <option value="101-250">101-250</option>
                                <option value="251-500">251-500</option>
                                <option value="501-750">501-750</option>
                                <option value="751-1000">751-1000</option>
                                <option value="1001-1500">1001-1500</option>
                                <option value="1500+">1500+</option>
                            </select>
                            <form-error :error="errors.member_count" v-if="errors && errors.member_count"></form-error>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <label for="sports" class="input-label">Sportart(en) auswählen *</label>
                    <div class="input-group" :class="errors && errors.sports ? 'input-group--error' : ''">
                        <select class="club-detail__select-sports" name="sports[]" multiple="" data-select-sports="true" required="required"></select>
                        <form-error :error="errors.sports" v-if="errors && errors.sports"></form-error>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-xs-12">
                    <transition-group name="slide">
                        <suggest-tag v-for="(name, id) in club.sports" :id="id" :name="name" key="id"></suggest-tag>
                    </transition-group>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <p class="vote-footnote vote-footnote--no-border vote-footnote--no-spacing">* Pflichtfeld</p>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12 section__content--centered">
                    <a class="btn btn--default btn--inline btn--large btn--upper btn--fixed-width" @click.prevent="submit" href="#">Weiter</a>
                </div>
            </div>
        </form>
    `,

    methods: {
        submit() {
            this.errors = this.club.validate();

            if (!this.errors) {
                this.club.store();
                this.club.redirect('ansprechpartner');
            }
        },
    },

    components: {
        FormError,
        'suggest-tag': {
            props: ['id', 'name'],
            template: `
                <span @click="removeTag(id)" class="select2-element select2-element--selected">
                    <i class="fa fa-times"></i> {{ name }}
                </span>
            `,
            methods: {
                removeTag(id) {
                    EventBus.$emit('remove-sport-tag', id);
                }
            }
        }
    },

    data() {
        return {
            club: new Club('club', this.$router),
            errors: null,
        }
    },

    beforeRouteEnter(to, from, next) {
        next((vm) => {
            vm.club.populate();
        });
    },

    created() {
        this.club.populate();

        EventBus.$on('remove-sport-tag', (id) => this.$delete(this.club.sports, id));
    },

    mounted() {
        let selectEl = $('[data-select-sports]');
        let select2 = new Select2Initializer(selectEl, '/api/sports');

        // add sport on select
        select2.on('select2:select', (evt) => {
            this.$set(this.club.sports, evt.params.data.text, evt.params.data.text);
            selectEl.val(null).trigger('change');
        });

        // remove sport on unselect
        select2.on('select2:unselect', (evt) => delete this.club.sports[evt.params.data.id]);
    },

};

export default FormClub;