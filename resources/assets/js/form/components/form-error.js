const FormError = {
    props: ['error', 'modifier'],
    template: `
            <span class="input-notification input-notifcation--error" :class="modifier">
                {{ error[0] }}
            </span>
    `,
};

export default FormError;