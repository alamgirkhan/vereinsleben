const FormSuccess = {
    template: `
        <div class="row">
            <div class="col-xs-12">
                <h1 class="hl hl--large hl--underlined hl--inverted hl--centered hl--upper">Vielen Dank für eure Bewerbung</h1>
                <p class="text-block text-block--compressed">
                    <span>
                        Eure Bewerbungsunterlagen werden nun von uns gesichtet. Gegebenenfalls setzen wir uns auch direkt mit euch in Verbindung. Die Kandidaten für nächsten Monat werden aus allen bis zum Ende dieses Monats eingegangenen Bewerbungen ausgewählt. Die nicht direkt im Folgemonat ausgewählten Kandidaten bleiben bis zum Ende des Aktionszeitraums im Bewerber-Pool und können in einem späteren Monat * als Kandidaten ausgewählt werden.
                    </span>
                </p>
                <p class="text-block text-block--compressed text-block--align-right text-block--mini">* Jeden Monat im Aktionszeitraum März bis November 2018</p>
            </div>
            <div class="col-xs-12 section__content--centered">
                <a href="/verein-des-monats" class="btn btn--grey btn--inline btn--large btn--upper">
                    Zurück zur Hauptseite
                </a>
            </div>
        </div>
    `,
};

export default FormSuccess;