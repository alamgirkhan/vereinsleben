import EventBus from '../utils/EventBus';
import User from '../../models/User';
import FormError from './form-error';

const FormUser = {
    template: `
        <form action="#" method="POST">
        
            <h1 class="hl hl--large hl--underlined hl--inverted hl--centered hl--upper">Informationen zum Ansprechpartner</h1>
            <p class="text-block text-block--compressed">Ein Ansprechpartner ist für uns auch sehr wichtig, falls wir etwa beim Auswahlprozess noch Fragen haben oder auch im Falle einer Vorstellung eures Vereins noch weitere Details besprochen werden müssen, brauchen wir eine Person, an die wir uns wenden können.</p>
        
            <div class="row">
            
                <div class="col-md-12">
                    <label for="gender">Anrede *</label>
                    <div class="input-group" :class="errors && errors.gender ? 'input-group--error' : ''">
                        <select v-model="user.gender" name="gender" id="gender" class="select">
                            <option value="">Bitte wählen</option>
                            <option value="male">Herr</option>
                            <option value="female">Frau</option>
                        </select>
                        <form-error :error="errors.gender" v-if="errors && errors.gender"></form-error>
                    </div>
                </div>
    
                <div class="col-md-6">
                    <label class="input-label" for="firstname">Vorname *</label>
                    <div class="input-group" :class="errors && errors.firstname ? 'input-group--error' : ''" >
                        <input v-model="user.firstname" placeholder="Vorname" class="input" type="text" name="firstname" id="firstname"
                               required="required">
                       <form-error :error="errors.firstname" v-if="errors && errors.firstname"></form-error>
                    </div>
                </div>
    
                <div class="col-md-6">
                    <label class="input-label" for="lastname">Nachname *</label>
                    <div class="input-group" :class="errors && errors.lastname ? 'input-group--error' : ''">
                        <input v-model="user.lastname" placeholder="Nachname" class="input" type="text" name="lastname" id="lastname" required="required">
                        <form-error :error="errors.lastname" v-if="errors && errors.lastname"></form-error>
                    </div>
                </div>
                
                <div class="col-md-9">
                    <label for="street" class="input-label">Straße *</label>
                    <div class="input-group" :class="errors && errors.street ? 'input-group--error' : ''">
                        <input v-model="user.street" required="required" class="input" placeholder="Straße" title="Straße" name="street" type="text" id="street">
                        <form-error :error="errors.street" v-if="errors && errors.street"></form-error>
                    </div>
                </div>
                <div class="col-md-3">
                    <label for="house_number" class="input-label">Hausnummer *</label>
                    <div class="input-group" :class="errors && errors.house_number ? 'input-group--error' : ''">
                        <input v-model="user.house_number" class="input" placeholder="Hausnummer" title="Hausnummer" name="house_number" type="text" id="house_number">
                        <form-error :error="errors.house_number" v-if="errors && errors.house_number"></form-error>
                    </div>
                </div>
                
                <div class="col-md-4">
                    <label class="input-label" for="zip">Postleitzahl *</label>
                    <div class="input-group" :class="errors && errors.zip ? 'input-group--error' : ''">
                        <input v-model="user.zip" placeholder="Postleitzahl" class="input" type="text" name="zip" id="zip" required="required">
                        <form-error :error="errors.zip" v-if="errors && errors.zip"></form-error>
                    </div>
                </div>
                
                <div class="col-md-8">
                    <label class="input-label" for="city">Stadt *</label>
                    <div class="input-group" :class="errors && errors.city ? 'input-group--error' : ''" >
                        <input v-model="user.city" placeholder="Stadt" class="input" type="text" name="city" id="city" required="required">
                        <form-error :error="errors.city" v-if="errors && errors.city"></form-error>
                    </div>
                </div>
    
                <div class="col-md-6">
                    <label class="input-label" for="email">E-Mail *</label>
                    <div class="input-group" :class="errors && errors.email ? 'input-group--error' : ''">
                        <input v-model="user.email" placeholder="E-Mail" class="input" type="email" name="email" id="email" required="required">
                        <form-error :error="errors.email" v-if="errors && errors.email"></form-error>
                    </div>
                </div>
                
                <div class="col-md-6">
                    <label class="input-label" for="phone">Telefon- oder Mobilnummer *</label>
                    <div class="input-group" :class="errors && errors.phone ? 'input-group--error' : ''">
                        <input v-model="user.phone" placeholder="Telefon" class="input" type="text" name="phone" id="phone" required="required">
                        <form-error :error="errors.phone" v-if="errors && errors.phone"></form-error>
                    </div>
                </div>
                
                <div class="col-md-12 rgs-slt-area"><br>
                    <div class="input-group" :class="errors && errors.terms ? 'input-group--error' : ''">
                        <input v-model="user.terms" class="input-checkbox" type="checkbox" name="terms" id="terms" value="1">
                        <label class="input-checkbox__label" for="terms">Ich habe die
                            <a class="input-checkbox__label-link" href="/content/agbs" target="_blank">
                                AGBs gelesen</a> und bin mit ihnen einverstanden. *</label>
                        <form-error class="input-notification--relieved" :error="errors.terms" v-if="errors && errors.terms"></form-error>
                    </div>
                </div>
                
                <div class="col-md-12 rgs-slt-area"><br>
                    <div class="input-group" :class="errors && errors.requirements ? 'input-group--error' : ''">
                        <input v-model="user.requirements" class="input-checkbox" type="checkbox" name="requirements" id="requirements" value="1">
                        <label class="input-checkbox__label" for="requirements">Ich habe die
                            <a class="input-checkbox__label-link" href="/verein-des-monats/teilnahmebedingungen" target="_blank">
                                Teilnahmebedingungen gelesen</a> und bin mit ihnen einverstanden. *</label>
                        <form-error class="input-notification--relieved" :error="errors.requirements" v-if="errors && errors.requirements"></form-error>
                    </div>
                </div>
                
                <div class="col-md-12 rgs-slt-area"><br>
                    <div class="input-group">
                        <input v-model="user.newsletter" class="input-checkbox" type="checkbox" name="newsletter" id="newsletter" value="1">
                        <label class="input-checkbox__label" for="newsletter">
                            Ich möchte den kostenlosen Newsletter erhalten und erkläre hierfür meine ausdrückliche Einwilligung. 
                            Weitere Informationen zu Protokollierung der Anmeldung, Versand des Newsletters und Abmeldemöglichkeit sind in den
                            <a class="input-checkbox__label-link" href="{{ url('/content/datenschutz') }}" target="_blank">
                            Datenschutzhinweisen</a> zu finden.
                        </label>
                    </div>
                </div>
                
                <div class="col-md-12 rgs-slt-area"><br>
                    <div class="input-group" :class="errors && errors.privacy ? 'input-group--error' : ''">
                        <input v-model="user.privacy" class="input-checkbox" type="checkbox" name="privacy" id="privacy">
                        <label class="input-checkbox__label" for="privacy">
                            Ich erkläre meine Einwilligung darin, dass meine Daten entsprechend dem in den
                            <a class="input-checkbox__label-link" href="{{ url('/content/datenschutz') }}" target="_blank">Datenschutzhinweisen</a> 
                            genannten Umfang zum Zweck der Bearbeitung meiner Bewerbung
                            verarbeitet werden und ich über die angegebenen Kommunikationswege kontaktiert
                            werden kann. Diese Daten werden dann gelöscht, wenn Ihre Anfrage erkennbar endgültig
                            erledigt ist, die Einwilligung widerrufen wird bzw. Sie wirksam der weiteren Verarbeitung
                            Ihrer Daten durch uns widersprechen. Weitere Informationen dazu sind in den
                            <a class="input-checkbox__label-link" href="{{ url('/content/datenschutz') }}" target="_blank">Datenschutzhinweisen</a> zu finden. *
                            <form-error class="input-notification--relieved" :error="errors.privacy" v-if="errors && errors.privacy"></form-error>
                        </label>
                    </div>
                </div>
                
                <div class="col-md-12"><br>
                    <p class="vote-footnote vote-footnote--no-border vote-footnote--no-spacing">* Pflichtfeld</p>
                </div>
    
                <div class="col-md-12 section__content--centered">
                    <a class="btn btn--grey btn--inline btn--large btn--upper btn--fixed-width" @click.prevent="back" href="#">Zurück</a>
                    <a class="btn btn--default btn--inline btn--large btn--upper btn--fixed-width" @click.prevent="submit" href="#">Weiter</a>
                </div>
            </div>
        </form>
    `,

    methods: {
        submit() {
            this.errors = this.user.validate();

            if (!this.errors) {
                this.user.store();

                this.user.redirect('projekt');
            }
        },

        back() {
            this.user.redirect('verein');
        }
    },

    components: {
        FormError
    },

    beforeRouteEnter(to, from, next) {
        next((vm) => {
            vm.user.populate();
        });
    },

    created() {
        this.user.populate();
    },

    data() {
        return {
            user: new User('user', this.$router),
            errors: null,
        }
    },

};

export default FormUser;