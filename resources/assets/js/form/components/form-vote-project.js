import VoteProject from '../../models/VoteProject';
import FormError from './form-error';

const FormVoteProject = {
    template: `
        <form action="#" method="POST">
        
            <h1 class="hl hl--large hl--underlined hl--inverted hl--centered hl--upper">Informationen zum Projekt</h1>
            <p class="text-block text-block--compressed">Die folgenden Informationen sind besonders wichtig. Einerseits für uns zur Auswahl von geeigneten Kandidaten und andererseits damit euer Verein möglichst umfassend vorgestellt werden kann. Denn zwischen allen vorgestellten Vereinen wird ja abgestimmt. Je präziser die Beschreibung, desto besser. </p>
        
            <div class="row">
                <div class="col-md-12">
                    <label class="input-label" for="title">Titel des Projekts *</label>
                    <div class="input-group" :class="errors && errors.title ? 'input-group--error' : ''" >
                        <input v-model="voteProject.title" placeholder="Titel des Projekts" class="input" type="text" name="title" id="title" required="required">
                        <form-error :error="errors.title" v-if="errors && errors.title"></form-error>
                    </div>
                </div>
                
                <div class="col-md-12">
                    <label class="input-label" for="description">Kurzbeschreibung *</label>
                    <div class="input-group" :class="errors && errors.description ? 'input-group--error' : ''" >
                        <textarea cols="30" v-model="voteProject.description" placeholder="Kurzbeschreibung" rows="10"  class="input" name="description" id="description" required="required"></textarea>
                        <form-error :error="errors.description" v-if="errors && errors.description"></form-error>
                    </div>
                </div>
    
                <div class="col-md-6">
                    <label class="input-label" for="start_date">Beginn des Projekts *</label>
                    <div class="input-group" :class="errors && errors.start_date ? 'input-group--error' : ''" >
                        <input @blur="setDate($event.target.value, 'start')" v-model="voteProject.start_date" 
                                   placeholder="Beginn des Projekts" class="input" type="text" name="start_date" id="start_date"
                                   required="required">
                       <form-error :error="errors.start_date" v-if="errors && errors.start_date"></form-error>
                    </div>
                </div>
    
                <div class="col-md-6">
                    <label class="input-label" for="end_date">Ende des Projekts</label>
                    <div class="input-group">
                        <input @blur="setDate($event.target.value, 'end')" v-model="voteProject.end_date" 
                                    placeholder="Ende des Projekts" class="input" type="text" name="end_date" id="end_date"
                                    required="required">
                    </div>
                </div>
                
                <div class="col-md-12">
                    <p class="vote-footnote vote-footnote--no-border vote-footnote--no-spacing">* Pflichtfeld</p>
                </div>
                
                <div class="col-md-12 section__content--centered">
                    <a class="btn btn--grey btn--inline btn--large btn--upper btn--fixed-width" @click.prevent="back" href="#">Zurück</a>
                    <a class="btn btn--default btn--inline btn--large btn--upper btn--fixed-width" @click.prevent="submit" href="#">Weiter</a>
                </div>
            </div>
        </form>
    `,

    methods: {
        submit() {
            this.errors = this.voteProject.validate();

            if (!this.errors) {
                this.voteProject.store();
                this.voteProject.redirect('datei-upload');
            }
        },

        setDate(val, scope) {
            this.voteProject[`${scope}_date`] = val;
        },

        back() {
            this.voteProject.redirect('ansprechpartner');
        }
    },

    components: {
        FormError
    },


    beforeRouteEnter(to, from, next) {
        next((vm) => {
            vm.voteProject.populate();
        });
    },

    created() {
        this.voteProject.populate();
    },

    data() {
        return {
            voteProject: new VoteProject('voteProject', this.$router),
            errors: null,
        }
    },
};

export default FormVoteProject;