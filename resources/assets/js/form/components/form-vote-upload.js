import VoteUpload from '../../models/VoteProjectUpload';
import EventBus from '../utils/EventBus';
import FormError from './form-error';

const FormVoteUpload = {
    template: `
        <form id="vote-project-upload" action="#" method="POST" enctype="multipart/form-data">
        
            <h1 class="hl hl--large hl--underlined hl--inverted hl--centered hl--upper">Daten hochladen</h1>
            <p class="text-block text-block--compressed">Zur Auswahl zwischen allen Bewerbungen und später für die Darstellung eures Vereins und eures Projektes ist es sehr wichtig für uns, möglichst viele Informationen zu haben. Wir benötigen unbedingt das Vereinslogo oder -wappen, mindestens ein Bild vom Projekt in ausreichender Auflösung, besser mehrere. Besonders wichtig ist eine ausführliche Beschreibung des Projektes, bzw. der Aktion als PDF-Datei oder Word-Dokument.</p>
        
            <div class="input-group">
                <label for="logo" class="btn btn--grey btn--full-width btn--large"><span class="fa fa-upload"></span> Vereinslogo oder -wappen *</label>
                <input @change="handleInput('logo', $event.target.value)" class="input-file" type="file" name="logo" id="logo" required="required">
                <div id="upload-result" class="thumbnail__list"></div>
                <form-error :error="errors.logo" v-if="errors && errors.logo"></form-error>
            </div>
            <br><br>
            <div class="input-group">
                <label for="projectImages" class="btn btn--grey btn--full-width btn--large"><span class="fa fa-upload"></span> Bilder vom Projekt *</label>
                <input @change="handleInput('projectImages', $event.target.value)" multiple type="file" name="projectImages[]" id="projectImages" class="input-file">
                <form-error :error="errors.projectImages" v-if="errors && errors.projectImages"></form-error>
            </div>
            <br><br>
            <div class="input-group">
                <label for="projectDescription" class="btn btn--grey btn--full-width btn--large"><span class="fa fa-upload"></span> Ausführliche Projektbeschreibung *</label>
                <input @change="handleInput('projectDescription', $event.target.value)" type="file" name="projectDescription" id="projectDescription" class="input-file">
                <form-error :error="errors.projectDescription" v-if="errors && errors.projectDescription"></form-error>
            </div>
            <br><br>
            <div class="input-group">    
                <label for="clubImage" class="btn btn--grey btn--full-width btn--large"><span class="fa fa-upload"></span> Bild vom Verein</label>
                <input @change="handleInput('clubImage', $event.target.value)" type="file" name="clubImage" id="clubImage" class="input-file">
            </div>
            <br><br>    
            <div class="row">
                <div class="col-xs-12 col-md-4 col-md-offset-4" v-if="progress > 0">
                    <div class="meter meter--spaced">
                        <span :style="{width: uploadProgress}"></span>
                        <label>Daten werden verarbeitet</label>
                    </div>
                </div>

                <div class="col-md-12">
                    <p class="vote-footnote vote-footnote--no-border vote-footnote--no-spacing">* Pflichtfeld</p>
                </div>
                
                <div class="col-md-12 section__content--centered">
                    <a class="btn btn--grey btn--inline btn--large btn--upper btn--fixed-width" @click.prevent="back" href="#">Zurück</a>
                    <a class="btn btn--inline btn--default btn--large btn--full-width btn--upper btn--fixed-width" @click.prevent="submit" href="#" :disabled="progress > 0">Jetzt bewerben</a>
                </div>
            </div>
        </form>
    `,

    computed: {
        uploadProgress() {
            return `${this.progress}%`
        }
    },

    components: {
        FormError,
    },

    methods: {
        submit() {
            let formData = new FormData(document.getElementById('vote-project-upload'));
            this.errors = this.voteUpload.validate();

            if (!this.errors && this.progress == 0) {
                EventBus.$emit('add-files', formData);
                EventBus.$emit('complete-wizard');
            }
        },

        back() {
            this.voteUpload.redirect('projekt');
        },

        handleInput(key, val) {
            this.voteUpload[key] = val;
        },
    },

    created() {
        EventBus.$on('upload-progress', progress => {
            this.progress = progress;
        });
    },

    data() {
        return {
            voteUpload: new VoteUpload('vote-upload', this.$router),
            formData: {},
            files: [],
            progress: 0,
            errors: null,
        }
    }
};

export default FormVoteUpload;