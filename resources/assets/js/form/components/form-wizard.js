import EventBus from '../utils/EventBus';
import axios from 'axios';

const FormWizard = {
    template: `
        <div class="container">
            <transition name="swipe" mode="out-in">
                <router-view></router-view>
            </transition>
         </div>
    `,

    data() {
        return {
            forms: {},
            files: null,
            progress: 0,
        }
    },

    created() {
        EventBus.$on('add-data', (formData, origin) => this.forms[origin] = formData);
        EventBus.$on('add-files', (files) => this.files = files);

        EventBus.$on('complete-wizard', () => {
            for (let form in this.forms) {
                if (this.forms.hasOwnProperty(form)) {
                    this.files.append(form, JSON.stringify(this.forms[form]));
                }
                
                localStorage.clear();
            }

            let config = {
                onUploadProgress(progressEvent) {
                    this.progress = Math.round((progressEvent.loaded * 100) / progressEvent.total);

                    EventBus.$emit('upload-progress', this.progress);
                }
            };

            axios.post('/verein-des-monats/teilnehmen', this.files, config)
                .then(fileRes => {
                    this.$router.push('erfolg');
                    this.forms = Object.assign({});
                })
                .catch(err => console.error(err));
        });
        
        if(Object.keys(this.forms).length === 0) {
            this.$router.push('verein');
        }
    }
};

export default FormWizard;