import Vue from 'vue/dist/vue';
import VueRouter from 'vue-router'
import Routes from './routes/routes';
import FormWizard from './components/form-wizard';

Vue.use(VueRouter);

Vue.component('form-wizard', FormWizard);

const router = new VueRouter({
    routes: Routes
});

const formApp = new Vue({
    router: router,
    methods: {
        afterEnter: function (el) {
            this.$refs.view.afterEnter(el)
        }
    }
}).$mount('#form-app');

export default formApp;