import FormUser from '../components/form-user';
import FormClub from '../components/form-club';
import FormVoteProject from '../components/form-vote-project';
import FormVoteUpload from '../components/form-vote-upload';
import FormSuccess from '../components/form-success';

const Routes = [
    {path: '/verein', component: FormClub},
    {path: '/ansprechpartner', component: FormUser},
    {path: '/projekt', component: FormVoteProject},
    {path: '/datei-upload', component: FormVoteUpload},
    {path: '/erfolg', component: FormSuccess},
];

export default Routes;