import Constraint from './validators/Club';
import Model from './Model';

export default class Club extends Model {
    constructor(instance = null, router = null) {
        super(instance, router);

        this.name = null;
        this.street = null;
        this.house_number = null;
        this.zip = null;
        this.city = null;
        this.member_count = null;
        this.founded = null;
        this.shorthand = null;
        this.sports = {};
    }

    validate() {
        return super.validate(Constraint);
    }
}