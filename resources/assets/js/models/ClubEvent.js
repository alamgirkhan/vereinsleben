import validate from 'validate.js';
import ClubEventConstraint from './validators/ClubEvent';

export default class ClubEvent {
    constructor() {
        this.title = '';
        this.content = '';
        this.street = '';
        this.house_number = '';
        this.zip = '';
        this.city = '';
        this.published_from = '';
        this.published_to = '';
        this.schedule_begin = '';
        this.schedule_end = '';
        this.club_id = '';
        this.images = [];
        this.type = 'clubEvent';
        this.club = null;
    }

    validate() {
        let clubEvent = this;

        return validate(clubEvent, ClubEventConstraint, {fullMessages: false});
    }
}