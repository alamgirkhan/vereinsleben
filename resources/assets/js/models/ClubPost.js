import validate from 'validate.js';
import ClubPostConstraint from './validators/ClubPost';

export default class ClubPost {
    constructor() {
        this.title = '';
        this.content = '';
        this.published_from = '';
        this.published_to = '';
        this.club_id = '';
        this.images = [];
        this.type = 'clubPost';
        this.club = null;
    }

    validate() {
        let clubPost = this;

        return validate(clubPost, ClubPostConstraint, {fullMessages: false});
    }
}