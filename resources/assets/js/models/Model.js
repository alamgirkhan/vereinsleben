import EventBus from '../form/utils/EventBus';
import validate from 'validate.js';

export default class Model {

    constructor(instance = null, router = null) {
        this.storage = localStorage;
        this.instance = instance;
        this.router = router;
        this.errors = null;
        this.className = this.constructor.name;
        this.key = this.className.toLowerCase();
    }

    store() {
        let data = this.prepareData();

        this.storage.setItem(`wizard-${this.instance}`, data);
        EventBus.$emit('add-data', data, (this.instance).toLowerCase());

        let offset = $('.section--vote-wizard').offset();
        let lotto = $('.sponsor-ribbon__wrapper').height();
        let header = $('.navi-wrapper').height();
        $('html, body').animate({
            scrollTop: offset.top - lotto - header
        }, 400);
    }

    populate() {
        let data = JSON.parse(this.storage.getItem(`wizard-${this.instance}`));

        if (data !== null) {
            for (let key in data) {
                if (data.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            }
        }
    }

    validate(Constraint) {
        return validate(this, Constraint, {fullMessages: false});
    }

    redirect(redirectTo = 'verein') {
        this.router.push(redirectTo);
    }

    getData() {
        let data = JSON.parse(this.storage.getItem(`wizard-${this.instance}`));
        return (data != null) ? data : this.redirect();
    }

    prepareData() {
        let data = Object.assign({}, this);

        delete data['router'];
        delete data['storage'];

        return JSON.stringify(data);
    }

}