import validate from 'validate.js';
import PostConstraint from './validators/Post';

export default class Post {
    constructor() {
        this.content = null;
        this.published_from = null;
        this.published_to = null;
        this.state = null;
        this.video_youtube = null;
        this.video = null;
        this.type = 'post';
        this.images = [];
        this.videos = [];
    }

    validate() {
        let post = this;
        let hasContent = null;

        let validation = validate(post, PostConstraint, {fullMessages: false});
        
        if((this.content == '' || this.content == null) && (this.video_youtube == '' || this.video_youtube == null) && !(this.images.length)) {
            hasContent = { general: ['Bitte füge Inhalt, Bilder oder ein Video hinzu'] };
            
            validation = Object.assign({}, validation, hasContent);
        }
        
        return validation;
    }

    store() {
        let post = this;
        let result = {
            errors: this.validate(post)
        };

        if (!result.errors) {
            
        }

        return result;

    }
}