import Constraint from './validators/User';
import Model from './Model';

export default class User extends Model {
    constructor(instance = null, router = null) {
        super(instance, router);

        this.gender = null;
        this.firstname = null;
        this.lastname = null;
        this.email = null;
        this.street = null;
        this.house_number = null;
        this.zip = null;
        this.city = null;
        this.phone = null;
        this.mobile = null;
        this.terms = null;
        this.newsletter = null;
        this.requirements = null;
        this.privacy = null;
    }

    validate() {
        return super.validate(Constraint);
    }
}