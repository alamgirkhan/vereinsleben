export default class Video {
    constructor() {
        this.type = '';
        this.url = null;
        this.identifier = null;
        this.identifier_regex = /youtube\.?.*(\?v=|\/embed\/)(.{11})/;
    }
}