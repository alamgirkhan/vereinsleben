import Constraint from './validators/VoteProject';
import Model from "./Model";

export default class VoteProject extends Model {
    constructor(instance = null, router = null) {
        super(instance, router);
        
        this.title = null;
        this.description = null;
        this.start_date = null;
        this.end_date = null;
    }

    validate() {
        return super.validate(Constraint);
    }
}