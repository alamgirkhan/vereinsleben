import Constraint from './validators/VoteProjectUpload';
import Model from "./Model";

export default class VoteProject extends Model {
    constructor(instance = null, router = null) {
        super(instance, router);
        
        this.logo = null;
        this.clubImage = null;
        this.projectImages = null;
        this.projectDescription = null;
    }

    validate() {
        return super.validate(Constraint);
    }
}