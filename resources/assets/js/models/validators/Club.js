const ClubConstraint = {
    name: {
        presence: {
            message: 'Bitte gib einen Vereinsnamen an'
        },
        length: {
            minimum: 3,
            message: 'Bitte mindestens drei Zeichen verwenden'

        }
    },

    street: {
        presence: {
            message: 'Bitte gib eine Straße an'
        },
        length: {
            minimum: 5,
            message: 'Bitte mindestens fünf Zeichen verwenden'
        },
    },

    house_number: {
        length: {
            tooLong: 'Die Hausnummer ist zu lang',
            maximum: 5
        },
        presence: {
            message: 'Bitte gib eine Hausnummer an'
        },
    },

    city: {
        presence: {
            message: 'Bitte gib eine Stadt an'
        },
    },

    zip: {
        numericality: {
            notValid: 'Die Postleitzahl darf nur aus Zahlen bestehen'
        },
        length: {
            minimum: 5,
            maximum: 6,
            tooShort: 'Bitte mindestens 5 Zeichen angeben',
            tooLong: 'Bitte maximal 6 Zeichen angeben'
        },
        presence: {
            message: 'Bitte gib eine Postleitzahl an'
        },
    },
    
    sports: {
        presence: {
            message: 'Bitte gib mindestens eine Sportart an'
        },
    },
    
    member_count: {
        presence: {
            message: 'Bitte gib an, wie viele Mitglieder im Verein sind'
        },
    },

};

export default ClubConstraint;