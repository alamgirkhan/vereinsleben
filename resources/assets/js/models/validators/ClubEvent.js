const ClubEventConstraint = {
    title: {
        presence: {
            message: 'Bitte gib einen Titel für deine Veranstaltung an'
        },
        length: {
            minimum: 3,
            message: 'Bitte mindestens drei Zeichen verwenden'

        }
    },

    content: {
        presence: {
            message: 'Bitte gib eine Beschreibung deiner Veranstaltung an'
        },
        length: {
            minimum: 5,
            message: 'Bitte mindestens fünf Zeichen verwenden'
        },
    },

    club: {
        presence: {
            message: 'Bitte wähle einen Verein aus'
        },
    },

    city: {
        presence: {
            message: 'Bitte gib eine Stadt für deine Veranstaltung an'
        },
    },

    schedule_begin: {
        presence: {
            message: 'Bitte gib Beginn der Veranstaltung an'
        },
    },

    schedule_end: {
        presence: {
            message: 'Bitte gib Ende der Veranstaltung an'
        },
    },

    zip: {
        presence: {
            message: 'Bitte gib eine PLZ an'
        },
    },

    street: {
        presence: {
            message: 'Bitte gib eine Straße an'
        },
    },

};

export default ClubEventConstraint;