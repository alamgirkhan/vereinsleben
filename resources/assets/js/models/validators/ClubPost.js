const ClubPostConstraint = {
    title: {
        presence: {
            message: 'Bitte gib einen Titel an'
        },
        length: {
            minimum: 3,
            message: 'Bitte mindestens drei Zeichen verwenden'

        }
    },

    content: {
        presence: {
            message: 'Bitte gib den Inhalt deines Beitrages an'
        },
        length: {
            minimum: 5,
            message: 'Bitte mindestens fünf Zeichen verwenden'
        },
    },
    
    club: {
        presence: {
            message: 'Bitte wähle einen Verein aus'
        },
    }

};

export default ClubPostConstraint;