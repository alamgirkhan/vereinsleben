import validate from 'validate.js';

validate.validators.youtube = function (value, options, key, attributes) {
    if (attributes.video === null || /youtube\.?.*(\?v=|\/embed\/)(.{11})/.test(attributes.video)) {
        return null
    }

    return 'Derzeit werden nur youtube-Videos unterstützt'
};

const PostConstraint = {
    content: {
        length: {
            minimum: 5,
            message: 'Bitte mindestens fünf Zeichen verwenden'
        },
    },

    video: {
        youtube: {},
    }


};

export default PostConstraint;