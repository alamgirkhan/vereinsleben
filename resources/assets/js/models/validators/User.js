const UserConstraint = {
    gender: {
        presence: {
            message: 'Bitte wähle eine Anrede'
        },
    },

    firstname: {
        presence: {
            message: 'Bitte gib deinen Vornamen an'
        },
        length: {
            minimum: 3,
            message: 'Bitte mindestens drei Zeichen verwenden'

        }
    },

    lastname: {
        presence: {
            message: 'Bitte gib deinen Nachnamen an'
        },
        length: {
            minimum: 3,
            message: 'Bitte mindestens drei Zeichen verwenden'

        }
    },

    email: {
        presence: {
            message: 'Bitte gib deine E-Mail Adresse an'
        },
        email: {
            message: 'Bitte gib eine gültige E-Mail Adresse an'

        }
    },

    street: {
        presence: {
            message: 'Bitte gib eine Straße an'
        },
        length: {
            minimum: 5,
            message: 'Bitte mindestens fünf Zeichen verwenden'
        },
    },

    house_number: {
        length: {
            tooLong: 'Die Hausnummer ist zu lang',
            maximum: 5
        },
        presence: {
            message: 'Bitte gib eine Hausnummer an'
        },
    },

    city: {
        presence: {
            message: 'Bitte gib eine Stadt an'
        },
    },

    zip: {
        numericality: {
            notValid: 'Die Postleitzahl darf nur aus Zahlen bestehen'
        },
        length: {
            minimum: 5,
            maximum: 6,
            tooShort: 'Bitte mindestens 5 Zeichen angeben',
            tooLong: 'Bitte maximal 6 Zeichen angeben'
        },
        presence: {
            message: 'Bitte gib eine Postleitzahl an'
        },
    },

    phone: {
        presence: {
            message: 'Bitte gib eine Telefonnummer an'
        },
    },

    terms: {
        presence: {
            message: 'Bitte lies und akzeptiere die AGB um fortzufahren'
        },
        equality: {
            attribute: 'terms',
            message: "Bitte lies und akzeptiere die AGB um fortzufahren",
            comparator: function(value) {
                return value === true;
            }
        }
    },

    requirements: {
        presence: {
            message: 'Bitte lies und akzeptiere die Teilnahmebedingungen'
        },
        equality: {
            attribute: 'requirements',
            message: "Bitte lies und akzeptiere die Teilnahmebedingungen",
            comparator: function(value) {
                return value === true;
            }
        }
    },
    privacy     : {
        presence: {
            message: 'Bitte lies und akzeptiere die Datenschutzhinweise'
        },
        equality: {
            attribute : 'privacy',
            message   : "Bitte lies und akzeptiere die Datenschutzhinweise",
            comparator: function (value) {
                return value === true;
            }
        }
    },

};

export default UserConstraint;