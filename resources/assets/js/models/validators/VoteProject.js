const VoteProjectConstraint = {
    title: {
        presence: {
            message: 'Bitte gib einen Titel für das Projekt an'
        },
        length: {
            minimum: 3,
            message: 'Bitte mindestens drei Zeichen verwenden'

        }
    },

    description: {
        presence: {
            message: 'Bitte gib eine Beschreibung für das Projekt an'
        },
        length: {
            minimum: 10,
            tooShort: 'Bitte mindestens zehn Zeichen verwenden',
        }
    },

    start_date: {
        presence: {
            message: 'Bitte gib das Datum zum Projektbeginn an'
        },
    },
};

export default VoteProjectConstraint;