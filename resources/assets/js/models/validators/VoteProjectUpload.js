const VoteProjectUploadConstraint = {
    logo: {
        presence: {
            message: 'Bitte füge euer Vereinslogo an'
        },
    },

    projectImages: {
        presence: {
            message: 'Bitte füge ein oder mehrere Bilder eures Projekts an'
        },
    },

    projectDescription: {
        presence: {
            message: 'Bitte füge eine ausführliche Projektbeschreibung an'
        },
    },
};

export default VoteProjectUploadConstraint;