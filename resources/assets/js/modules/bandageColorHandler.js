const bandageColorHandler = (() => {
    let $toggleHandle = $('[data-bandage-color-published]');
    if ($toggleHandle) {
        let token = $('[data-token]').data('token');
        let slug = $('[data-bandage-slug]').data('bandage-slug');
        $toggleHandle.click(function (evt) {
            let headerColor = $('#header_color').val();
            let titleColor = $('#title_color').val();
            $.ajax('/bandage/' + slug + '/color/update', {
                method : 'PATCH',
                data   : {
                    header_color: headerColor,
                    title_color : titleColor
                },
                success: function (res) {
                    console.log(res);
                },
                error  : function (err) {
                    console.log(err);
                }
            });
        });
    }
})();
export default bandageColorHandler;

