const bandageSubscriberHandler = (() => {
    let $publishBtn = $('[data-bandage-subscribe-button]');
    let buttonActiveClass = 'btn--active';

    if ($publishBtn) {
        // initially set the class
        if ($publishBtn.data('bandageSubscribeStatus') == true) {
            $publishBtn.addClass(buttonActiveClass);
        }

        $publishBtn.on('click', (evt) => {
            evt.preventDefault();
            let $el = $(evt.target);
            $el.blur();

            let status = ($el.data('bandageSubscribeStatus') == true) ? false : true;
            $el.data('bandageSubscribeStatus', status);

            let url = $el.data('bandageSubscribeUrl');
            let token = $el.closest('[data-token]').data('token');
            let data = {
                status: status,
                token : token
            };

            ajaxPublisher(url, data, (res) => {
                let statusMessage = '';
                let $notification = $('body').find('.club-detail__notification');
                if (res.status == true) {
                    statusMessage = 'Du bist jetzt Abonennten des Verbands!';
                    $notification
                        .removeClass('club-detail__notification--info')
                        .addClass('club-detail__notification--success')
                        .text(statusMessage);
                    $el.addClass(buttonActiveClass);
                } else {
                    statusMessage = 'Schade, dass du kein Abonennten mehr bist!';
                    $notification
                        .removeClass('club-detail__notification--success')
                        .addClass('club-detail__notification--info')
                        .text(statusMessage);
                    $el.removeClass(buttonActiveClass);
                }

                $notification.slideDown();

                setTimeout(() => $notification.slideUp(), 2500);
            });
        });
    }

    const ajaxPublisher = (url, data, cb = () => {
    }) => {
        $.ajax(url, {
            method : 'POST',
            data   : data,
            success: function (res) {
                cb(res);
            },
            error  : function (err) {
                alert('Es ist ein Fehler aufgetreten.');
            }
        });
    };

})();

export default bandageSubscriberHandler;