//import $ from 'jquery';
import axios from 'axios';
import 'select2';

export default (() => {
    let $wrapper;
    let sports = [];
    let removeSports = [];
    let config = {
        mode: 'init'
    };

    const http = axios.create({
        headers: {
            'X-CSRF-TOKEN': document.querySelector('[data-token]').dataset.token,
            'X-Requested-With': 'XMLHttpRequest',
        },
    });

    const loader = `<div data-loader class="loader__wrapper loader__wrapper--centered">
                    <div class="loader loader--loading"></div>
                    <div class="loader loader--loading"></div>
                </div>`;

    const loadTemplate = ($wrapper) => {
        let url = $wrapper.data('sportCareer');

        http.get(url).then((res) => {
            $wrapper.replaceWith(res.data.template);

            initializer();
        });
    };

    const handleCancel = (evt) => {
        let $currentWrapper = $(evt.target).closest('[data-sport-career]');
        let addHandle = document.querySelector('[data-sport-career-add]');
        evt.preventDefault();

        sports = [];
        removeSports = [];
        config = {
            mode: 'init'
        };

        if ($currentWrapper.data('sportCareer').length > 0) {
            $currentWrapper.html(loader);
            loadTemplate($currentWrapper);
        } else {
            $currentWrapper.fadeOut(350, () => {
                $currentWrapper.remove();
            });
        }

        config.mode = 'idle';
        addHandle.removeAttribute('disabled');
        addHandle.classList.remove('animation__zoom-in');
    };

    const handleSave = ($wrapper, cb) => {
        let form = $wrapper.find('form');
        let submit = $wrapper.find('[data-sport-career-store]').get(0);
        let url = form.attr('action');
        let addHandle = document.querySelector('[data-sport-career-add]');

        initializeSportAutocomplete(form.find('[data-sport-autocomplete]'));
        initializeClubAutocomplete(form.find('[data-sport-autocomplete]'));

        submit.addEventListener('click', (evt) => {
            evt.preventDefault();

            let formData = new FormData(form.get(0));

            if (sports.length) {
                formData.append('sports', JSON.stringify(sports));
            }

            if (removeSports.length) {
                formData.append('removeSports', JSON.stringify(removeSports));
            }

            http.post(url, formData).then((res) => {
                if (!res.data.success) {
                    $($wrapper[0]).find('.sport-career-js-error').html('')
                    $($wrapper[0]).find('.sport-career-js-error').append(
                      `<div class="flash-message flash-message--error">
                                        <ul>
                                            <li>`+res.data.error+`</li>
                                        </ul>
                                    </div>`
                    );
                    return;
                }
                $wrapper.html(loader);

                if ($wrapper.data('sportCareer').length === 0) {
                    $wrapper.data('sportCareer', res.data.data.url);
                    let url = $wrapper.data('sportCareer');

                    http.get(url).then((res) => {
                        $('#sport-care-list').append(res.data.template);
                        $wrapper.html('');
                        initializer();
                    });
                    config.mode = 'idle';
                    addHandle.removeAttribute('disabled');
                    addHandle.classList.remove('animation__zoom-in');
                } else {
                    config.mode = 'idle';
                    addHandle.removeAttribute('disabled');
                    addHandle.classList.remove('animation__zoom-in');

                    cb(res);
                }

            }).catch((err) => {
                if (err.response.status === 422) {
                    // handle validation errors
                    let error = err.response.data.errors;
                    $($wrapper[0]).find('.sport-career-js-error').html(`<div class="flash-message flash-message--error"><ul></ul></div>`)
                    for (let formKey in error) {
                        if (error.hasOwnProperty(formKey)) {
                            // $wrapper.find($(`#${formKey}`)).after(`<span class="input-notification input-notifcation--error">${error[formKey]}</span>`);
                            $($wrapper[0]).find('.sport-career-js-error .flash-message.flash-message--error ul').append(
                              `<li>${error[formKey]}</li>`
                            );
                        }
                    }
                }
            });
        });
    };

    const handleEdit = ($target) => {
        let editUrl = $target.data('sportCareerEdit');
        $wrapper = $target.closest('[data-sport-career]');
        $wrapper.html(loader);

        // load edit template
        http.get(editUrl).then((res) => {
            $wrapper.html(res.data.template);

            initializeSportDeleteHandles();

            handleSave($wrapper, (res) => {
                loadTemplate($wrapper);
            });

            initializeCancelHandle($wrapper);
        });
    };

    const handleDelete = (evt) => {
        evt.preventDefault();
        let $target = $(evt.currentTarget);
        let deleteUrl = $target.data('sportCareerDelete');
        let addHandle = document.querySelector('[data-sport-career-add]');
        $wrapper = $target.closest('[data-sport-career]');

        let confirm = window.confirm('Möchest du diesen Eintrag wirklich löschen?');
        if (confirm) {
            http.delete(deleteUrl).then((res) => {
                $wrapper.fadeOut(350, () => {
                    $wrapper.remove();
                });

                config.mode = 'idle';
                addHandle.removeAttribute('disabled');
                addHandle.classList.remove('animation__zoom-in');
            });
        }
    };

    const handleSportAutocomplete = () => {
        let $el = $wrapper.find('[data-sport-autocomplete]');
        let $elTags = $wrapper.find('[data-sport-autocomplete-tags]');
        $el.on('select2:select', (evt) => {
            // check if the currently selected id exists
            if (typeof evt.params.data.id === "number" && isFinite(evt.params.data.id) && sports.indexOf(evt.params.data.id) < 0) {
                // store sport
                sports.push(evt.params.data.id);

                // display it as tag
                $elTags.append(`<span data-sport-career-sport-remove class="club-detail__overview-value club-detail__overview-value--highlighted">
                        <i class="fa fa-times"></i>
                        ${evt.params.data.text}
                    </span>`);

                initializeSportDeleteHandles();
            }
            // clear input for further tagging :)
            $el.val('').trigger('change');
        });
    };

    const handleClubAutocomplete = () => {
        let $el = $wrapper.find('[data-club-autocomplete]');

        $el.on('select2:select', handleClubSelection);
    };

    const handleClubSelection = (evt) => {
        let $currentForm = $(evt.currentTarget).closest('form');
        let {avatar, text} = evt.params.data;

        if (isNaN(Number(evt.params.data.id))) {
            avatar = '';
            text = '';
        }

        let autocompleteSelection = $($currentForm).find('[data-club-autocomplete-selection]')[0];
        let autocompleteSelectionImage = $($currentForm).find('[data-club-autocomplete-selection-image]')[0];
        let autocompleteSelectionText = $($currentForm).find('[data-club-autocomplete-selection-text]')[0];
        autocompleteSelection.classList.remove('hidden');
        autocompleteSelectionImage.setAttribute('src', avatar);
        autocompleteSelectionText.innerHTML = text;
    };

    const handleSportDelete = ($target) => {
        let id = $target.data('sportCareerSportRemove');

        if (id) {
            removeSports.push(id);
        }

        $target.fadeOut(350, () => {
            $target.remove();
        });
    };

    const handleEditHandles = (evt) => {
        let $target = $(evt.currentTarget);
        let addHandle = document.querySelector('[data-sport-career-add]');
        addHandle.setAttribute('disabled', 'disabled');
        addHandle.classList.add('animation__zoom-in');
        handleEdit($target);
    };

    const handleDeleteSportHandles = (evt) => {
        let $target = $(evt.currentTarget);
        handleSportDelete($target);
    };

    const handleAdd = (evt) => {
        evt.preventDefault();

        let $target = $(evt.currentTarget);

        if (config.mode !== 'create') {
            $target.get(0).setAttribute('disabled', 'disabled');
            http.get('/render-view/user.profile.partials.sport-career.edit.form').then((res) => {
                config.mode = 'create';
                $target.get(0).classList.add('animation__zoom-in');
                $target.before(`<div data-sport-career>${res.data}</div>`);
                $wrapper = $target.prev('[data-sport-career]');
                $("#suggestedclub").detach().appendTo('#movesuggestedclub');
                $("#suggestedclub").show();
                handleSave($wrapper, (res) => {
                    loadTemplate($wrapper);
                });

                initializeCancelHandle($wrapper);
            });
        }
    };

    const initializeAddHandle = (() => {
        let addHandle = document.querySelector('[data-sport-career-add]');

        if (addHandle !== null) {
            addHandle.addEventListener('click', handleAdd, false);
        }
    })();

    const initializeDeleteHandle = () => {
        let deleteHandles = Array.from(document.querySelectorAll('[data-sport-career-delete]'));
        deleteHandles.map((deleteHandle) => {
            deleteHandle.addEventListener('click', handleDelete, false);
        });
    };

    const initializeEditHandles = () => {
        let addHandle = document.querySelector('[data-sport-career-add]');
        let editHandles = Array.from(document.querySelectorAll('[data-sport-career-edit]'));

        editHandles.map((editHandle) => {
            editHandle.addEventListener('click', handleEditHandles, false);
        });
    };

    const initializeSportDeleteHandles = () => {
        let sportDeleteHandles = Array.from(document.querySelectorAll('[data-sport-career-sport-remove]'));
        sportDeleteHandles.map((sportDeleteHandle) => {
            sportDeleteHandle.addEventListener('click', handleDeleteSportHandles, false);
        });
    };

    const initializeCancelHandle = ($wrapper) => {
        let form = $wrapper.find('form');
        let cancel = form.find('[data-sport-career-cancel]').get(0);

        cancel.addEventListener('click', handleCancel, false);
    };

    const initializeSportAutocomplete = () => {
        handleSportAutocomplete();

        let $el = $wrapper.find('[data-sport-autocomplete]');

        $el.select2({
            language: 'de',
            tags: true,
            minimumInputLength: 3,
            dropdownAutoWidth: false,
            width: 'resolve',
            cache: true,
            placeholder: 'Sportart hinzufügen',
            ajax: {
                url: $el.data('sportAutocomplete'),
                data: function (params) {
                    return {
                        search: params.term
                    };
                },
                dataType: 'json',
                delay: 250,
                processResults: function (data, params) {
                    let results = data['data'].map((element) => {
                        return {
                            id: element.id,
                            text: element.title
                        }
                    });

                    const sorter = function (a, b) {
                        let input_string = params.term;
                        if (input_string.length > 3) {
                            return Levenshtein.get(a.text, input_string) - Levenshtein.get(b.text, input_string);
                        }
                    };

                    results.sort(sorter);

                    return {
                        results: results
                    };
                },
            },
        });
    };

    const initializeClubAutocomplete = () => {
        handleClubAutocomplete();

        let $el = $wrapper.find('[data-club-autocomplete]');

        $el.select2({
            language: 'de',
            tags: true,
            minimumInputLength: 3,
            dropdownAutoWidth: false,
            width: 'resolve',
            cache: true,
            placeholder: {
                id: -1,
                text: 'Verein hinzufügen'
            },
            ajax: {
                url: $el.data('clubAutocomplete'),
                data: function (params) {
                    return {
                        search: params.term
                    };
                },
                dataType: 'json',
                delay: 250,
                processResults: function (data, params) {
                    let results = data['data'].map((element) => {
                        return {
                            id: element.id,
                            text: element.name,
                            avatar: element.images.avatar.startPage
                        }
                    });

                    const sorter = function (a, b) {
                        let input_string = params.term;
                        if (input_string.length > 3) {
                            return Levenshtein.get(a.text, input_string) - Levenshtein.get(b.text, input_string);
                        }
                    };

                    results.sort(sorter);

                    return {
                        results: results
                    };
                },
            },
            templateResult: function (state) {
                let data = {
                    avatar: '/default_images/club/default-avatars-startPage.jpg',
                    text: state.text
                };

                if (state.hasOwnProperty('id') && Number(state.id) > 0 && typeof state.avatar !== 'undefined') {
                    data.avatar = state.avatar;
                }

                if (!isNaN(Number(state.id))) {
                    return $(
                      `<div class="profile-athletic-runway__club">
                            <div class="profile-athletic-runway__club-image-wrapper">
                                <img src="${data.avatar}" class="profile-athletic-runway__club-image" />
                            </div>
                            <span>${data.text}</span>
                        </div>`
                    );
                }
            },
        });
    };

    const initializeAccordion = () => {
        $('.profile-athletic-runway__arrow-container').on('click', function (evt) {
            evt.preventDefault();
            $(this).find('.profile-athletic-runway__arrow').toggleClass('profile-athletic-runway__arrow--reverse');
            $(this).next().stop(false, true).slideToggle();
        });
    };

    let overlayActive = false;

    const initializeProfileOverlay = () => {
        let $content = $('.profile__overlay-content');
        let $overlay = $('.profile__overlay');
        let $btn = $('.profile__overlay-btn');
        let active = false;

        if (!overlayActive && $content.height() >= 550) {
            $content.addClass('profile__overlay-content--overflow-hidden');
            $overlay.addClass('profile__overlay--shown');
            overlayActive = true;
        } else {
            $btn.hide(0);
        }

        $overlay.on('click', function (evt) {
            evt.preventDefault();
            $(this).remove();
            $btn.remove();
            $content.removeClass('profile__overlay-content--overflow-hidden');
        });

        $btn.on('click', function (evt) {
            evt.preventDefault();
            $(this).remove();
            $overlay.remove();
            $content.removeClass('profile__overlay-content--overflow-hidden');
        });
    };

    const initializer = () => {
        sports = [];
        removeSports = [];
        config = {
            mode: 'init'
        };

        initializeEditHandles();
        initializeDeleteHandle();
        initializeAccordion();
        initializeProfileOverlay();
    };

    initializer();

})();