//import $ from 'jquery';
//import 'rangeslider.js';
import 'select2';
import axios from 'axios';
// import Levenshtein from 'fast-levenshtein';

export default (() => {
    const http = axios.create({
        headers: {
            'X-CSRF-TOKEN': document.querySelector('[data-token]').dataset.token,
            'X-Requested-With': 'XMLHttpRequest'
        }
    });

    const editSportInterestHandleEl = document.querySelector('[data-edit-sport-interest]');
    const sportInterestContainer = document.querySelector('[data-sport-interest-wrapper]');
    const loader = `<div data-loader class="loader__wrapper loader__wrapper--centered">
                    <div class="loader loader--loading"></div>
                    <div class="loader loader--loading"></div>
                </div>`;
    const sportInterestTemplateUrl = document.querySelector('[data-sport-interest-template-url]').dataset.sportInterestTemplateUrl;

    //let interestValue = 0;
    let deleteIds = [];
    let deleteSportInterestHandleEls;
    let saveHandle;
    let $sportAutocomplete;
    let sportAutocompleteSelector = '[data-sport-autocomplete]';

    /*const initializeInterestSlider = (el) => {
        el.rangeslider({
            polyfill: false,
            onSlide: function (position, value) {
                interestValue = value;
            },
            onInit: function () {
                interestValue = el.val();
            },
        });
    };*/

    const initializeSportAutocomplete = (el) => {
        el.select2({
            language: 'de',
            ajax: {
                url: el.data('sportAutocomplete'),
                data: function (params) {
                    return {
                        search: params.term
                    };
                },
                dataType: 'json',
                delay: 250,
                processResults: function (data, params) {
                    let results = data['data'].map((element) => {
                        return {
                            id: element.id,
                            text: element.title
                        }
                    });

                    const sorter = function (a, b) {
                        let input_string = params.term;
                        if (input_string.length > 3) {
                            return Levenshtein.get(a.text, input_string) - Levenshtein.get(b.text, input_string);
                        }
                    };

                    results.sort(sorter);

                    return {
                        results: results
                    };
                },
            },
            minimumInputLength: 3,
            dropdownAutoWidth: false,
            width: 'resolve',
            cache: true,
            placeholder: 'nach Sportart suchen...',
        });
    };

    const initializer = () => {
        let $interestSlider = $('[data-interest-slider]');
        $sportAutocomplete = $(sportAutocompleteSelector);

        //initializeInterestSlider($interestSlider);
        initializeSportAutocomplete($sportAutocomplete);
        handleSportAutocomplete($sportAutocomplete);
        handleSportInterestDelete();
        handleSportInterestCancel();
    };

    const showLoader = (el, append = true) => {
        if (append) {
            el.innerHTML += loader;
        } else {
            el.innerHTML = loader;
        }
    };
    const hideLoader = () => {
        document.querySelector('[data-loader]').style.display = 'none';
    };

    const handleSportAutocomplete = (el) => {
        el.on('select2:select', (evt) => {
            $(evt.target).siblings('[data-interest-slider-wrapper]').show();
        });
    };

    const createSportInterest = (cb = () => {
    }) => {
        let interestData = [];
        let reqUrl = saveHandle.dataset.sportInterestSave;

        $.each($(sportAutocompleteSelector), (key, el) => {
            let $el = $(el);
            let interest = $el.siblings('[data-interest-slider-wrapper]').find('[data-interest-slider]').get(0);
            if (typeof interest !== 'undefined' && typeof $el !== 'undefined' && $el.val() !== '' && interest.value !== '') {
                let item = {
                    sport: $el.val(),
                    interest: interest.value
                };
                interestData.push(item);
            }
        });

        if (interestData.length) {
            http.post(reqUrl, {interestData})
                .then((res) => {
                    cb(res.data);
                })
                // @todo handle & notify on error
                .catch((err) => console.log(err));
        }
    };

    const loadSportInterestTemplate = (cb = () => {
    }) => {
        http.get(sportInterestTemplateUrl).then((res) => {
            sportInterestContainer.innerHTML = res.data;
            $('[data-sport-interest-options]').remove();

            cb(sportInterestContainer);
        });
    };

    const deleteSportInterest = (cb = () => {
    }) => {
        http.patch(deleteSportInterestHandleEls[0].dataset.sportInterestDeleteUrl, {deleteIds}).then((res) => {
            createSportInterest();

            cb(res.data);
        });
    };

    const fadeOut = (el, duration = 300) => {
        let elementStyle = el.style;
        let step = 25 / duration;
        elementStyle.opacity = elementStyle.opacity || 1;
        (function fade() {
            (elementStyle.opacity -= step) < 0 ? elementStyle.display = "none" : setTimeout(fade, 25);
        })();
    };

    const handleSportInterestSave = () => {
        saveHandle = document.querySelector('[data-sport-interest-save]');
        saveHandle.addEventListener('click', (evt) => {

            //showLoader(sportInterestContainer, false);

            // check if we have sets to remove
            if (deleteIds.length > 0) {
                deleteSportInterest(() => {
                    loadSportInterestTemplate();
                });
            } else {
                createSportInterest(() => {
                    loadSportInterestTemplate();
                });
            }

            sportInterestContainer.classList.remove('profile-interests__edit');
            editSportInterestHandleEl.classList.remove('animation__zoom-in');
            $('[data-sport-interest-options]').remove();
        });
    };

    const handleSportInterestDelete = () => {
        deleteSportInterestHandleEls = document.querySelectorAll('[data-sport-interest-delete]');
        Array.prototype.slice.call(deleteSportInterestHandleEls).map((el) => {
            el.addEventListener('click', (evt) => {
                deleteIds.push(el.dataset.sportInterestDelete);
                let $container = $(evt.target).closest('[data-sport-interest]').parent();
                fadeOut($container.get(0));
                $container.remove();
            });
        });
    };

    const handleSportInterestCancel = () => {
        let cancelSportInterestHandle = document.querySelector('[data-sport-interest-cancel]');
        cancelSportInterestHandle.addEventListener('click', (evt) => {
            showLoader(sportInterestContainer, false);
            http.get(sportInterestTemplateUrl).then((res) => {
                sportInterestContainer.innerHTML = res.data;
            });
            sportInterestContainer.classList.remove('profile-interests__edit');
            editSportInterestHandleEl.classList.remove('animation__zoom-in');
            $('[data-sport-interest-options]').remove();
        });
    };


    let editSportInterestHandle = (() => {
        if (editSportInterestHandleEl !== null) {
            editSportInterestHandleEl.addEventListener('click', (evt) => {
                showLoader(sportInterestContainer, false);
                // check if we have already have our container and if not append it
                // let childData = Array.prototype.slice.call(sportInterestContainer.children).filter((el) => el.dataset.hasOwnProperty('sportInterestSave'));
                if (!sportInterestContainer.classList.contains('profile-interests__edit')) {
                    sportInterestContainer.classList.add('profile-interests__edit');
                    editSportInterestHandleEl.classList.add('animation__zoom-in');

                    // load container
                    http.get('/render-view/user.profile.partials.edit.sport-interest-handles').then((res) => {
                        $(sportInterestContainer).after(res.data);
                        hideLoader();

                        initializeSportInterestHandler();
                        handleSportInterestSave();

                        // load existing data
                        http.get(sportInterestContainer.dataset.sportInterestEditUrl).then((interests) => {

                            if (interests.data.length) {
                                interests.data.map((html) => {
                                    let partial = document.createElement('div');
                                    partial.innerHTML = html;
                                    sportInterestContainer.appendChild(partial);
                                });

                                initializer();
                            }
                        })
                    });
                }
            });
        }
    })();

    const initializeSportInterestHandler = () => {
        let addSportInterestHandle = document.querySelector('[data-sport-interest-add]');
        addSportInterestHandle.addEventListener('click', (evt) => {
            // fetch server-side rendered partial
            $.get('/render-view/user.profile.partials.edit.sport-interest', (res) => {

                // create element from response to keep event handlers and prevent multiple initializations
                let partial = document.createElement('div');
                partial.innerHTML = res;
                sportInterestContainer.appendChild(partial);

                // initialize plugins as the dom is ready to use
                initializer();
            });
        });
    };
})();

