import $ from 'jquery';
import axios from 'axios';
import autosize from 'autosize';
import slick from 'slick-carousel';

export default (function () {
    const http = axios.create({
        headers: {
            'X-CSRF-TOKEN': document.querySelector('[data-token]').dataset.token,
            'X-Requested-With': 'XMLHttpRequest'
        }
    });

    const loader = `<div data-loader class="loader__wrapper loader__wrapper--centered">
                    <div class="loader loader--loading"></div>
                    <div class="loader loader--loading"></div>
                </div>`;

    const loadTemplate = ($wrapper) => {
        let url = $wrapper.data('userPostHref');

        http.get(url).then((res) => {
            $wrapper = $(res.data.template).replaceAll($wrapper);
            initializeSlider($wrapper);
        });
    };

    const handleSave = ($wrapper, cb) => {
        let form = $wrapper.find('form');
        let submit = form.find('[type="submit"]').get(0);
        let updateUrl = form.attr('action');

        submit.addEventListener('click', (evt) => {
            evt.preventDefault();
            $wrapper.html(loader);

            let formData = new FormData(form.get(0));

            http.post(updateUrl, formData).then((res) => {
                if ($wrapper.data('userPostHref').length === 0) {
                    $wrapper.data('userPostHref', res.data.data.url);
                }
                cb(res);
            });
        });
    };

    const handleEdit = ($target) => {
        let editUrl = $target.data('userPostEdit');
        let $userPostWrapper = $target.closest('[data-user-post]');
        $userPostWrapper.html(loader);

        // load edit template
        http.get(editUrl).then((res) => {
            $userPostWrapper = $(res.data.template).replaceAll($userPostWrapper);
            $userPostWrapper.find('[data-statuspost-media-button="' + $userPostWrapper.data('userPostType') + '"]').click();

            handleSave($userPostWrapper, (res) => {
                loadTemplate($userPostWrapper);
            });
        });
    };

    const handleDelete = ($target) => {
        let deleteUrl = $target.data('userPostDelete');
        let $userPostWrapper = $target.closest('[data-user-post]');

        http.delete(deleteUrl).then((res) => {
            if (res.status == 200 && res.data.success == true) {
                $userPostWrapper.fadeOut();
            }
        });
    };

    const initializeCreateHandle = ($wrapper, cb) => {
        let $container = $('[data-user-post-container]');

        $container.on('click', '[data-statuspost-media-button]', function (evt) {
            evt.preventDefault();
            let $clickedButton = $(this);
            let $type = $clickedButton.data('statuspostMediaButton');
            let $wrapper = $clickedButton.closest('[data-user-post]');
            let $targetBox = $wrapper.find('[data-statuspost-media-content="' + $type + '"]');

            if ($(this).hasClass('btn--icon-switch-active')) {
                $(this).removeClass('btn--icon-switch-active');
                $targetBox.stop(false, true).slideUp(200);
                $wrapper.removeClass(function (index, css) {
                    return (css.match(/(^|\s)profile-statusposts__item--\S+/g) || []).join(' ');
                });
            } else {
                $wrapper.find('[data-statuspost-media-button]').removeClass('btn--icon-switch-active');
                $wrapper.find('[data-statuspost-media-content]').stop(false, true).slideUp(200);
                $clickedButton.addClass('btn--icon-switch-active');
                $targetBox.stop(false, true).slideDown(200);
                $wrapper.removeClass(function (index, css) {
                    return (css.match(/(^|\s)profile-statusposts__item--\S+/g) || []).join(' ');
                });
                $wrapper.addClass('profile-statusposts__item--' + $type);
            }
        });

        autosize($container.find('[data-user-post-content-field]'));

        if ($container.find('[data-user-post-create-form]').length > 0) {
            let $form = $container.find('[data-user-post-create-form]');
            let submit = $form.find('[type="submit"]').get(0);
            let createUrl = $form.attr('action');
            let youtube = $form.find('input[name="video_youtube"]').get(0);
            let youtubePreview = $form.find('[data-video-preview]');
            let endpoint = 'http://www.vimeo.com/api/oembed.json';
            let callback = 'embedVideo';

            youtube.addEventListener('change', (evt) => {
                let youtubeUrl = youtube.value;

                if (youtube.value.length > 0) {
                    let youtubeVideoId = youtubeUrl.match(/youtube\.?.*(\?v=|\/embed\/)(.{11})/);
                    if (youtubeVideoId != null) {
                        youtubeVideoId = youtubeVideoId.pop();

                        if (youtubeVideoId.length == 11) {
                            let videoThumbnail = $('<img class="profile-statusposts__videothumb" src="//img.youtube.com/vi/' + youtubeVideoId + '/0.jpg">');
                            youtubePreview.html(videoThumbnail);
                        }
                    } else {
                        let vimeoVideoId = youtubeUrl.match(/vimeo.com\/([0-9]+)\??/);


                        if (vimeoVideoId != null) {
                            youtubeVideoId = endpoint + '?url=' + vimeoVideoId;

                            let url = youtubeUrl.replace("https://", "https%3A//");
                            let img = youtubeUrl.split('/');

                            let vidurl = img.pop();

                            $.getJSON('http://vimeo.com/api/oembed.json?url=https%3A//vimeo.com/' + vidurl, function (data) {

                                let thumburl = data['thumbnail_url'];

                                let videoThumbnail = $('<img class="profile-statusposts__videothumb" src=' + thumburl + '>');
                                youtubePreview.html(videoThumbnail);
                            });

                            // let thumburl = "https://i.vimeocdn.com/video/" + vidurl + "_640.webp";
                            // let videoThumbnail = $('<img class="profile-statusposts__videothumb" src=' + thumburl + '>');

                            youtubePreview.html(videoThumbnail);

                        }

                        // youtubePreview.html('');
                    }
                } else {
                    youtubePreview.html('');
                }

            });

            $form.on('reset', function(evt) {
                $(this).get(0).reset();
                $(this).find('[data-video-preview]').html('');
                $(this).find('[id^="upload-result"]').html('');
                $(this).find('[data-new-image-text]').addClass('hidden');
                $(this).find('.btn--icon-switch-active[data-statuspost-media-button]').click();
            });

            $container.on('click', '[data-user-post-reset]', function (evt) {
                evt.preventDefault();
                $form.trigger('reset');
            });

            submit.addEventListener('click', (evt) => {
                // do not do anything and do not abort the event here when there is no text to let the HTML5 validation kick in.
                if ($form.find('[name="content"]').val() !== '') {
                    evt.preventDefault();
                    //$wrapper.html(loader);

                    let formData = new FormData($form.get(0));

                    http.post(createUrl, formData).then((res) => {
                        if (res.status == 200 && res.data.success == true) {
                            http.get(res.data.href).then((res) => {
                                $form.closest('[data-user-post]').after(res.data.template);
                                initializeSlider($form.closest('[data-user-post]').next('[data-user-post]'));

                                // reset form
                                $form.trigger('reset');
                            });
                        } else {
                            alert('Es ist ein Fehler aufgetreten.');
                        }
                    });
                }
            });
        }
    };

    const initializeEditHandles = () => {
        $('[data-user-post-container]').on('click', '[data-user-post-edit]', function (evt) {
            evt.preventDefault();
            let $target = $(evt.target);
            handleEdit($target);
        });
    };

    const initializeDeleteHandle = () => {
        $('[data-user-post-container]').on('click', '[data-user-post-delete]', function (evt) {
            evt.preventDefault();
            let $target = $(evt.target);
            let confirm = window.confirm('Möchest du diesen Eintrag wirklich löschen?');
            if (confirm) {
                handleDelete($target);
            }
        });
    };

    const initializeCancelHandle = () => {
        $('[data-user-post-container]').on('click', '[data-user-post-cancel]', function (evt) {
            evt.preventDefault();

            let $target = $(evt.target);
            let $wrapper = $target.closest('[data-user-post]');

            if ($wrapper.data('userPostHref').length > 0) {
                $wrapper.html(loader);
                loadTemplate($wrapper);
            } else {
                $wrapper.fadeOut();
            }
        });
    };

    const initializeSlider = ($wrapper) => {
        if ($wrapper.find('.club-post__image-slider')) {
            $wrapper.find('.club-post__image-slider').not('.slick-initialized').slick({
                prevArrow: '<button type="button" class="slick-prev"></button>',
                nextArrow: '<button type="button" class="slick-next"></button>',
                autoplay: false,
                arrows: true,
                responsive: [
                    {
                        breakpoint: 992,
                        settings: {
                            autoplay: true,
                            arrows: false
                        }
                    }
                ]
            });
        }
    };

    const initializer = () => {
        initializeCreateHandle();
        initializeEditHandles();
        initializeDeleteHandle();
        initializeCancelHandle();
    };

    initializer();

})();