import 'slick-carousel';

const slickCarousel = (() => {
    $('.profile-carousel__navigation').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        asNavFor: '.profile-carousel',
        focusOnSelect: true,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    arrows: true
                }
            }
        ]
    });
    $('.profile-carousel').slick({
        adaptiveHeight: true,
        draggable: false,
        swipe: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        asNavFor: '.profile-carousel__navigation'
    });
})();

export default slickCarousel;