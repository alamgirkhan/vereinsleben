const Campaign = {
    props: ['image', 'campaign'],
    template: `
           <div class="image-ver campaign-image" data-toggle="tooltip" :title="campaign.description">
              <a target="_blank" :href="campaign.link">
                <img :src="host + '/uploaded/campaign/' + campaign.banner_horizontal" alt="">
              </a>
            </div>
    `,
    data() {
        return {
            host: window.location.origin,
        }
    }
};

export default Campaign;