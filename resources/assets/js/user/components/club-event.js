const ClubEvent = {
    props     : ['state'],
    template  : `	
		<div class="club-post__item" :class="modifierClass">
			<div class="row">
				<event-publish-date :date="state.published_at"></event-publish-date>
				<event-content :title="state.title" :avatar="state.club.avatar" :name="state.club.name" :cluburl="state.club.url" :content="state.content" :images="state.images" :event="state"></event-content>
			</div>
		</div>
    `,
    computed  : {
        modifierClass: function () {
            //document.styleSheets[1].insertRule(`.user-stream__item--club-${this.state.club.id}:after { background-image: url(${this.state.club.avatar}) !important }`, 0);
            //document.styleSheets[1].insertRule(`.user-stream__item--club-${this.state.club.id}:before { border-right-color: white !important }`, 0);
            return `user-stream__item--club user-stream__item--club-${this.state.club.id}`;
        }
    },
    components: {
        eventPublishDate: {
            props   : ['date'],
            template: `
				<div class="col-lg-1 col-md-1 club-post__date noborder">
					<span class="date-no">{{ formattedDate.format("DD") }}</span>
					<br>
					<span class="month-no">{{ formattedDate.format("MMMM") }}</span>
					<br>
					<span class="year-no">{{ formattedDate.format("YYYY") }}</span>
				</div>
            `,
            computed: {
                formattedDate: function () {
                    return moment(this.date);
                },
            },
        },
        eventContent    : {
            props     : ['title', 'avatar', 'images', 'content', 'event', 'name', 'cluburl'],
            template  : `
                <div class="col-lg-11 col-sm-11 event-post">
							<div class="event-post-thum">
							    <post-single-image v-if="images && images.length == 1" :images="images"></post-single-image>
								<div class="row post-row-new">								    
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
									    <post-image v-if="images && images.length > 1" :images="images"></post-image>										
										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
												<div class="event-post-thum-arrow"><i class="fa fa-caret-up" aria-hidden="true"></i></div>
												<div class="row post-head">
													<div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 postlogo">
														<post-avatar :avatar="avatar" :cluburl="cluburl"></post-avatar>
													</div>
													<div class="col-lg-9 col-md-9 col-sm-8 col-xs-8 postdetail">
														<div class="postlefttxt">{{ name }}</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<post-images v-if="images && images.length > 1" :images="images"></post-images>
								</div>
							</div>
                            <div class="post-blue">{{ title }}</div>							
                            <div class="row post-head">                               
                                <event-location :street="event.street" :houseNumber="event.house_number" :zip="event.zip" :city="event.city"></event-location>
                                <event-date :scheduleBegin="event.schedule_begin" :scheduleEnd="event.schedule_end"></event-date>
							</div>							
							<p v-html="content"><br><br></p>
						</div>
            `,
            components: {
                eventLocation  : {
                    props   : ['street', 'houseNumber', 'zip', 'city'],
                    template: `
						<div class="col-xs-12 postlocation">
							<i class="fa fa-map-marker" aria-hidden="true"></i>							
							{{ street }}<span>,{{ houseNumber }}</span>
							{{ zip }}, {{ city }}
						</div>
					`,
                },
                eventDate      : {
                    props   : ['scheduleBegin', 'scheduleEnd'],
                    template: `
					    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 postlocation">
								<i class="fa fa-calendar" aria-hidden="true"></i>{{ date.begin }}
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 postlocation">
								<i class="fa fa-calendar" aria-hidden="true"></i>{{ date.end }}
							</div>
						</div>`,
                    computed: {
                        date: function () {
                            let begin = moment(this.scheduleBegin).format('DD.MM.YYYY HH:mm');
                            let end = moment(this.scheduleEnd).format('DD.MM.YYYY HH:mm');
                            return {
                                begin: (begin.length < 18) ? this.scheduleBegin : begin,
                                end  : (end.length < 18) ? this.scheduleEnd : end,
                            }
                        }
                    }
                },
                postAvatar     : {
                    props   : ['avatar', 'cluburl'],
                    template: `<a :href="imageHref" target="_blank"><img :src="imageSrc" /></a>`,
                    computed: {
                        imageSrc : function () {
                            return `${this.avatar}`
                        },
                        imageHref: function () {
                            return `${this.cluburl}`
                        },
                    },
                },
                postImage      : {
                    props   : ['images'],
                    template: `
                            <div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 lgthum">
								    <a :href="imageHref" :data-lightbox="imageableId"><img :src="imageSrc" /></a>
								</div>
							</div>`,
                    computed: {
                        imageSrc   : function () {
                            return `${this.images[0].imageUrl}`
                        },
                        imageHref  : function () {
                            return `${this.images[0].imageUrl}`
                        },
                        imageableId: function () {
                            return `${this.images[0].imageable_id}`
                        },
                    },
                },
                postSingleImage: {
                    props   : ['images'],
                    template: `<a :href="imageHref" :data-lightbox="imageableId"><img :src="imageSrc" /></a>`,
                    computed: {
                        imageSrc   : function () {
                            return `${this.images[0].imageUrl}`
                        },
                        imageHref  : function () {
                            return `${this.images[0].imageUrl}`
                        },
                        imageableId: function () {
                            return `${this.images[0].imageable_id}`
                        },
                    },
                },
                postImages     : {
                    props     : ['images'],
                    template  : `
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 club-post_right-thums">
							<div class="row">
                                <post-image v-for="image in images.slice(1, 4)" :key="image.id" :image="image"></post-image>
							</div>
                            <div v-if="images && images.length > 4" class="event-plus-icon"><a href="javascript:void(0);">
							    <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                            </div>
                            <post-rem-image v-for="image in images.slice(4, 100)" :key="image.id" :image="image"></post-rem-image>							
                        </div>`,
                    components: {
                        postImage   : {
                            props   : ['image'],
                            template: `
								<a :href="imageHref" :data-lightbox="imageableId">
									<div class="col-lg-12 club-post__sml-image" :style="postImage"></div>
								</a>`,
                            computed: {
                                imageHref  : function () {
                                    return `${this.image.imageUrl}`
                                },
                                imageableId: function () {
                                    return `${this.image.imageable_id}`
                                },
                            },
                            data    : function () {
                                return {
                                    postImage: {
                                        background    : `url(${this.image.imageUrl}) no-repeat`,
                                        backgroundSize: `cover`
                                    },
                                }
                            },
                        },
                        postRemImage: {
                            props   : ['image'],
                            template: `
								<a :href="imageHref" :data-lightbox="imageableId"></a>`,
                            computed: {
                                imageHref  : function () {
                                    return `${this.image.imageUrl}`
                                },
                                imageableId: function () {
                                    return `${this.image.imageable_id}`
                                },
                            },
                        },
                    },
                }
            },			
        },
    }
};

export default ClubEvent;
