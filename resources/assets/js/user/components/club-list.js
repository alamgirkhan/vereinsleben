import SidebarNavigation from './sidebar-navigation';

const SidebarClubs = {
    props: ['clubs', 'modifier'],
    template: `
                <div v-if="clubs && clubs.data && clubs.data.length > 0" class="user-stream__club-list col-lg-12 col-xs-12 col-sm-12 col-md-12 no-padding" :class="modifier">
                    <sidebar-navigation :entity="clubs" resource="clubs" label="Vereine"></sidebar-navigation>
                    <club v-for="club in clubs.data" :club="club"></club>
                </div>
            `,
    components: {
        sidebarNavigation: SidebarNavigation,
        club: {
            props: ['club'],
            template: `
                        <a class="user-stream__link" :href="club.url">
                            <div class="user-stream__club-list-item">
                                <img class="user-stream__club-list-item-image" v-if="image" :src="club.imageUrl" :alt="club.name">
                                <div class="user-stream__club-list-item-name">
                                    <span class="user-stream__club-list-title">{{club.name}}</span>
                                </div>
                            </div>
                        </a>
                    `,
            computed: {
                image: function () {
                    let image = '/default_images/club/default-avatars-singleView.jpg';

                    if (this.club.image) {
                        image = `/uploaded/club/${this.club.id}/avatars/${this.club.imageUrl}`;
                    }

                    return image;
                }
            },
        }
    },
};

export default SidebarClubs;