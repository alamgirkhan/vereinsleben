import EventBus from '../utils/EventBus';
import Post from './post';
import ClubPost from './club-post';
import News from './news';
import ClubEvent from './club-event';
import Viewports from '../config/viewports';
import ListHandler from './list-handler';
import Campaign from './campaign';
import BandageEvent from "./bandage-event";
import BandagePost from "./bandage-post";

const ContentHandler = {
    props: ['items', 'events', 'clubs', 'users', 'campaigns', 'bannerSpacingStream'],
    template: `
        <div class="content-handler col-lg-8">
            <transition-group tag="div" class="list" name="user-stream__item--animation" appear>
                <template v-for="(item, index) in items">
                    <component :item="item" :is="item.type" :state="item" :key="item" v-if="item && item.type"></component>
                    <list-handler v-if="viewports.current < viewports.md" :events="events" :clubs="clubs" :users="users" :position="index" listClass="user-stream__item user-stream__item--hollowed" :key="index"></list-handler>
                    <campaign v-if="campaigns && campaigns.length > 0 && ((index + 1) % bannerSpacingStream === 0) && (campaigns[((index + 1) / bannerSpacingStream) - 1])" 
                    :key="index + '_tmp'" :image="campaigns[((index + 1).banner_horizontal / bannerSpacingStream) - 1]" :campaign="campaigns[((index + 1) / bannerSpacingStream) - 1]"></campaign>
                </template>
            </transition-group>
        </div>
    `,
    components: {
        Post,
        ClubPost,
        News,
        ClubEvent,
        ListHandler,
        Campaign,
        BandageEvent,
        BandagePost,
    },

    data() {
        return {
            viewports: Viewports
        }
    },

    created() {
        EventBus.$on('store', (data) => {
            let content = Object.assign({}, data);
            this.items.unshift(content);
        });

        EventBus.$on('fetch-external', () => {
            // axios.get('user/stream').then((res) => {
            //     this.items = res.data;
            // });
        });
    },
};

export default ContentHandler;