import SidebarNavigation from './sidebar-navigation';

const SidebarEvents = {
    props: ['events', 'modifier'],
    template: `
                <div v-if="events && events.data && events.data.length > 0" class="user-stream__event-list col-lg-12 col-xs-12 col-sm-12 col-md-12 no-padding" :class="modifier">
                    <sidebar-navigation :entity="events" resource="events" label="Nächste Events"></sidebar-navigation>
                    <event v-for="event in events.data" :event="event"></event>
                </div>
            `,
    components: {
        sidebarNavigation: SidebarNavigation,
        event: {
            props: ['event'],
            template: `
                        <a class="user-stream__link col-lg-12 no-padding" :href="event.url">
                            <div class="user-stream__event-list-item col-lg-12 no-padding">
                                <div class="user-stream__event-list-item-header col-lg-12 no-padding">
                                    <span class="user-stream__event-list-item-header-club">{{ event.club.name }}</span>
                                    <span class="user-stream__event-list-item-header-title">{{ event.title }}</span>
                                </div>
                                <div class="user-stream__event-date-wrapper col-lg-12 no-padding">
                                    <span class="user-stream__event-label user-stream__event-label--event-list">Datum</span>
                                    <div class="user-stream__event-date user-stream__event-date--event-list">{{ begin }}</div>
                                </div>
                            </div>
                        </a>
                    `,
            computed: {
                begin: function () {
                    return moment(this.event.schedule_begin).format('DD.MM.YYYY HH:mm');
                }
            }
        }
    },
};

export default SidebarEvents;