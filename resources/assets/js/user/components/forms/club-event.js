import axios from 'axios';
import ClubEvent from '../../../models/ClubEvent';
import EventBus from '../../utils/EventBus';
import FormError from './form-error';

const ClubEventForm = {
    props: ['user'],
    created: function () {
        this.$watch('user', (user) => {
            this.clubs = user.clubs;
        });

        EventBus.$on('add-post-image', (image) => {
            this.clubEvent.images.push(image);
        });
    },
    data: function () {
        return {
            clubs: (this.user && this.user.clubs) || [],
            clubEvent: new ClubEvent,
            images: [],
            errors: null
        }
    },
    template: `
        <form id="user-club-event-form" enctype="multipart/form-data">
        <div class="user-stream__item user-stream__item--preceding">
        <div class="club-detail__inline-form user-stream__event">
        <div class="row">
                    <div class="col-xs-12">
                        <label for="title" class="input-label">Verein</label>
                        <div class="input-group input-group--grown" :class="errors && errors.club ? 'input-group--error' : ''">
                            <select v-model="clubEvent.club" class="select" v-if="clubs" name="clubId" id="clubId">
                                <option :value="null">Verein wählen</option>
                                <option v-for="club in clubs" :value="{id: club.id, name: club.name}">{{ club.name }}</option>
                            </select>
                            <form-error :error="errors.club" v-if="errors && errors.club"></form-error>
                        </div>
                    </div>
                </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="title" class="input-label">Titel</label>
                    <div class="input-group input-group--grown" :class="errors && errors.title ? 'input-group--error' : ''">
                        <input v-model="clubEvent.title" class="input" placeholder="Titel der Veranstaltung" required="required" name="title" type="text" id="title">
                        <form-error :error="errors.title" v-if="errors && errors.title"></form-error>
                    </div>
                </div>
    
                <div class="col-md-12">
                    <label for="content_raw" class="input-label">Text</label>
                    <div class="input-group input-group--grown" :class="errors && errors.content ? 'input-group--error' : ''">
                        <textarea v-model="clubEvent.content" class="input" rows="10" placeholder="Beschreibung der Veranstaltung" required="required" name="content_raw" cols="50" id="content_raw"></textarea>
                        <form-error :error="errors.content" v-if="errors && errors.content"></form-error>
                    </div>
                </div>
    
                <div class="col-md-6">
                    <label for="schedule_begin" class="input-label">Beginn der Veranstaltung</label>
                    <div class="input-group input-group--grown" :class="errors && errors.schedule_begin ? 'input-group--error' : ''">
                        <input @blur="setDate($event.target.value, 'schedule_begin')" class="input" placeholder="DD.MM.YY HH:mm" data-datetimepicker-enabled required name="schedule_begin" type="text" id="schedule_begin">
                        <form-error :error="errors.schedule_begin" v-if="errors && errors.schedule_begin"></form-error>
                    </div>
                </div>
    
                <div class="col-md-6">
                    <label for="schedule_end" class="input-label">Ende der Veranstaltung</label>
                    <div class="input-group input-group--grown" :class="errors && errors.schedule_end ? 'input-group--error' : ''">
                        <input @blur="setDate($event.target.value, 'schedule_end')" class="input" placeholder="DD.MM.YY HH:mm" data-datetimepicker-enabled required name="schedule_end" type="text" id="schedule_end">
                        <form-error :error="errors.schedule_end" v-if="errors && errors.schedule_end"></form-error>
                    </div>
                </div>
    
                <div class="col-md-9">
                    <label for="street" class="input-label">Straße</label>
                    <div class="input-group input-group--grown" :class="errors && errors.street ? 'input-group--error' : ''">
                        <input v-model="clubEvent.street" class="input" placeholder="Straße" required="required" name="street" type="text" id="street">
                        <form-error :error="errors.street" v-if="errors && errors.street"></form-error>
                    </div>
                </div>
    
                <div class="col-md-3">
                    <label for="house_number" class="input-label">Hausnummer</label>
                    <div class="input-group input-group--grown">
                        <input v-model="clubEvent.house_number" class="input" placeholder="Nr" name="house_number" type="text" id="house_number">
                    </div>
                </div>
    
                <div class="col-md-4">
                    <label for="zip" class="input-label">Postleitzahl</label>
                    <div class="input-group input-group--grown" :class="errors && errors.zip ? 'input-group--error' : ''">
                        <input v-model="clubEvent.zip" class="input" placeholder="PLZ" required="required" name="zip" type="text" id="zip">
                        <form-error :error="errors.zip" v-if="errors && errors.zip"></form-error>
                    </div>
                </div>
    
                <div class="col-md-8">
                    <label for="city" class="input-label">Stadt</label>
                    <div class="input-group input-group--grown" :class="errors && errors.city ? 'input-group--error' : ''">
                        <input v-model="clubEvent.city" class="input" placeholder="Stadt / Ort" required="required" name="city" type="text" id="city">
                        <form-error :error="errors.city" v-if="errors && errors.city"></form-error>
                    </div>
                </div>
    
                <div class="col-md-6">
                    <label for="published_from" class="input-label">Veröffentlicht von</label>
                    <div class="input-group">
                        <input @blur="setDate($event.target.value, 'published_from')" class="input" placeholder="DD.MM.YY HH:mm" data-datetimepicker-enabled name="published_from" type="text" id="published_from">
                    </div>
                </div>
    
                <div class="col-md-6">
                    <label for="published_until" class="input-label">Veröffentlicht bis</label>
                    <div class="input-group">
                        <input @blur="setDate($event.target.value, 'published_to')" class="input" placeholder="DD.MM.YY HH:mm" data-datetimepicker-enabled name="published_to" type="text" id="published_to">
                    </div>
                </div>
    
                <div class="col-xs-12">
                    <div class="thumbnail__list">
                        <preview-images v-for="image in images" v-if="images && images.length > 0" :image="image"></preview-images>
                    </div>
                </div>
    
                    <div class="col-xs-12">
                        <div class="club-detail__inline-form-submit-wrapper">
                        
                            <label for="image" class="button button--grey button--center button--icon button--full-width">
                                <span class="fa fa-upload"></span>Bilder hochladen
                            </label>
                            <input @change="handleImage($event)" multiple type="file" name="images[]" id="image" class="input-file">
                            
                            <button @click.prevent="save" class="button button--dark button--center button--icon button--full-width">
                                <span class="fa fa-edit"></span>Eintragen
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    `,
    methods: {
        save: function () {
            let domForm = document.getElementById('user-club-event-form');
            let form = new FormData(domForm);

            this.errors = this.clubEvent.validate();

            this.clubEvent.club_id = (this.clubEvent.club && this.clubEvent.club.id) ? this.clubEvent.club.id : null;

            for (let item in this.clubEvent) {
                if (this.clubEvent.hasOwnProperty(item)) {
                    form.append(item, this.clubEvent[item]);
                }
            }

            if (!this.errors) {
                EventBus.$emit('store', this.clubEvent);

                axios.post('/event', form).then((res) => {
                    EventBus.$emit('fetch-external');
                }).catch((err) => {
                    console.log(err);
                });

                this.resetForm(domForm);
            }

        },
        resetForm: function (form) {
            form.reset();
            this.clubEvent = Object.assign({}, new ClubEvent);
            this.images = [];
        },
        setDate: function (val, attribute) {
            this.clubEvent[attribute] = val;
        },
        handleImage: function (evt) {

            this.images = [];

            if (window.File && window.FileList && window.FileReader) {
                const files = Array.prototype.slice.call(evt.target.files);

                files.map((file) => {
                    if (file.type.match('image')) {
                        ((image) => {
                            const fileReader = new FileReader();

                            fileReader.addEventListener("load", (evt) => {

                                const img = {
                                    imageUrl: evt.target.result
                                };

                                this.images.push(img);

                                EventBus.$emit('add-post-image', img);
                            });

                            fileReader.readAsDataURL(image);

                        })(file);

                    }
                });
            }
        }
    },
    components: {
        formError: FormError,
        previewImages: {
            props: ['image'],
            template: `
                <div class="thumbnail__container">
                    <img class="thumbnail__image" :src="image.imageUrl">
                </div>
            `
        }
    }
};

export default ClubEventForm;