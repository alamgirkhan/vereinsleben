import axios from 'axios';
import ClubPost from '../../../models/ClubPost';
import EventBus from '../../utils/EventBus';
import FormError from './form-error';

const ClubPostForm = {
    props: ['user'],
    created: function () {
        this.$watch('user', (user) => {
            this.clubs = user.clubs;
        });

        EventBus.$on('add-post-image', (image) => {
            this.clubPost.images.push(image);
        });
    },
    data: function () {
        return {
            clubs: (this.user && this.user.clubs) || [],
            clubPost: new ClubPost,
            images: [],
            errors: null
        }
    },
    template: `
        <form id="user-club-post-form" enctype="multipart/form-data">
            <div class="user-stream__item user-stream__item--preceding">
                <div class="club-detail__inline-form user-stream__club-post">
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="title" class="input-label">Verein</label>
                            <div class="input-group input-group--grown" :class="errors && errors.club ? 'input-group--error' : ''">
                                <select v-model="clubPost.club" class="select" v-if="clubs" name="clubId" id="clubId">
                                    <option :value="null">Verein wählen</option>
                                    <option v-for="club in clubs" :value="{id: club.id, name: club.name}">{{ club.name }}</option>
                                </select>             
                              <form-error :error="errors.club" v-if="errors && errors.club"></form-error>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="title" class="input-label">Titel</label>
                            <div class="input-group input-group--grown" :class="errors && errors.title ? 'input-group--error' : ''">
                                <input v-model="clubPost.title" class="input" placeholder="Titel der Neuigkeit" required="required" name="title" type="text" id="title">
                                <form-error :error="errors.title" v-if="errors && errors.title"></form-error>
                            </div>
                        </div>
            
                        <div class="col-xs-12">
                            <label for="content_raw" class="input-label">Text</label>
                            <div class="input-group input-group--grown" :class="errors && errors.content ? 'input-group--error' : ''">
                                <textarea v-model="clubPost.content" class="input" rows="10" placeholder="Inhalt der Neuigkeit" required="required" name="content_raw" cols="50" id="content_raw"></textarea>
                                <form-error :error="errors.content" v-if="errors && errors.content"></form-error>
                            </div>
                        </div>
            
                        <div class="col-xs-6">
                            <label for="published_from" class="input-label">Veröffentlicht von</label>
                            <div class="input-group">
                                <input @blur="setPublishedDate($event.target.value, 'from')" class="input" placeholder="DD.MM.YY HH:mm" data-datetimepicker-enabled name="published_from" type="text" id="published_from">
                            </div>
                        </div>
            
                        <div class="col-xs-6">
                            <label for="published_until" class="input-label">Veröffentlicht bis</label>
                            <div class="input-group">
                                <input @blur="setPublishedDate($event.target.value, 'to')" class="input" placeholder="DD.MM.YY HH:mm" data-datetimepicker-enabled name="published_to" type="text" id="published_to">
                            </div>
                        </div>
                        
                        <div class="col-xs-12">
                            <div class="thumbnail__list">
                                <preview-images v-for="image in images" v-if="images && images.length > 0" :image="image"></preview-images>
                            </div>
                        </div>
            
                        <div class="col-xs-12">
                            <div class="club-detail__inline-form-submit-wrapper">
                            
                                <label for="image" class="button button--grey button--center button--icon button--full-width">
                                    <span class="fa fa-upload"></span>Bilder hochladen</label>
                                <input @change="handleImage($event)" multiple type="file" name="images[]" id="image" class="input-file">
                                
                                <button @click.prevent="save" class="button button--dark button--center button--icon button--full-width">
                                    <span class="fa fa-edit"></span> Posten 
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    `,
    methods: {
        save: function () {
            let domForm = document.getElementById('user-club-post-form');
            let form = new FormData(domForm);

            this.errors = this.clubPost.validate();

            this.clubPost.club_id = (this.clubPost.club && this.clubPost.club.id) ? this.clubPost.club.id : null;

            for (let item in this.clubPost) {
                if (this.clubPost.hasOwnProperty(item)) {
                    form.append(item, this.clubPost[item]);
                }
            }

            if(!this.errors) {
                EventBus.$emit('store', this.clubPost);

                axios.post('/post', form).then((res) => {
                    EventBus.$emit('fetch-external');
                }).catch((err) => {
                    console.log(err);
                });

                this.resetForm(domForm);

            }

        },
        resetForm: function (form) {
            form.reset();
            this.clubPost = Object.assign({}, new ClubPost);
            this.images = [];
        },
        setPublishedDate: function (val, scope) {
            this.clubPost[`published_${scope}`] = val;
        },
        handleImage: function (evt) {

            this.images = [];

            if (window.File && window.FileList && window.FileReader) {
                const files = Array.prototype.slice.call(evt.target.files);

                files.map((file) => {
                    if (file.type.match('image')) {
                        ((image) => {
                            const fileReader = new FileReader();

                            fileReader.addEventListener("load", (evt) => {

                                const img = {
                                    imageUrl: evt.target.result
                                };

                                this.images.push(img);

                                EventBus.$emit('add-post-image', img);
                            });

                            fileReader.readAsDataURL(image);

                        })(file);

                    }
                });
            }
        }
    },
    components: {
        formError: FormError,
        previewImages: {
            props: ['image'],
            template: `
                <div class="thumbnail__container">
                    <img class="thumbnail__image" :src="image.imageUrl">
                </div>
            `
        }
    }
};

export default ClubPostForm;