const FormError = {
    props: ['error'],
    template: `
            <span class="input-notification input-notifcation--error">
                {{ error[0] }}
            </span>
    `,
};

export default FormError;