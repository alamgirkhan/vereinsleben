import EventBus from '../../utils/EventBus';
import Post from '../../../models/Post';
import Video from '../../../models/Video';
import FormError from './form-error';
import axios from 'axios';

const PostForm = {
    props: ['state'],
    template: `
        <div class="user-stream__item user-stream__item--preceding">
            <post-date :date="new Date"></post-date>
            <post-form></post-form>
        </div>
    `,
    components: {
        postDate: {
            props: ['date'],
            template: `
                <div class="user-stream__post-date">
                    <div class="user-stream__post-day">
                        {{ formattedDate.format("DD") }}
                    </div>
                    <div class="user-stream__post-month-and-year">
                        <div class="user-stream__post-month">
                            {{ formattedDate.format("MMMM") }}
                        </div>
                        <div class="user-stream__post-year">
                            {{ formattedDate.format("YYYY") }}
                        </div>
                    </div>
                </div>
            `,
            computed: {
                formattedDate: function () {
                    return moment(this.date);
                },
            },
        },
        postForm: {
            data: function () {
                return {
                    formData: new Post,
                    mediaComponent: {
                        image: 'form-image',
                        video: 'form-video'
                    },
                    media: null,
                    errors: null
                }
            },
            created: function () {
                EventBus.$on('toggle-form-media', (component) => {
                    this.media = component;
                });

                EventBus.$on('add-video', (url) => {
                    this.formData.video = url;
                });

                EventBus.$on('add-post-video', (data) => {
                    this.formData.video_youtube = data.url;
                    this.formData.videos.push(data);
                });

                EventBus.$on('reset-post-form', () => {
                    this.formData = new Post;
                    this.media = null;
                    this.errors = null;
                });

                EventBus.$on('add-post-image', (image) => {
                    this.formData.images.push(image);
                });

            },
            template: `
                <form id="user-post-form" enctype="multipart/form-data">
                
                    <div class="profile-statusposts__form">
                
                        <div class="input-group input-group--grown" :class="errors ? '' : 'input-group--no-spacing'">
                            <textarea v-model="formData.content" class="input profile-statusposts__form-input" rows="5" placeholder="Was machst du gerade?" required></textarea>
                            <form-error :error="errors.content ? errors.content : errors.general" v-if="errors && ('content' in errors || 'general' in errors)"></form-error>
                        </div>

                        
                         <div class="profile-statusposts__advanced-options">
                            <button @click.prevent="toggleMedia(mediaComponent.image)" class="profile-statusposts__advanced-options-button btn btn--inline btn--icon btn--icon-big btn--icon-switch">
                                <i class="fa fa-camera" aria-hidden="true"></i>
                            </button>
                            <button @click.prevent="toggleMedia(mediaComponent.video)" class="profile-statusposts__advanced-options-button btn btn--inline btn--icon btn--icon-big btn--icon-switch">
                                <i class="fa fa-youtube-play " aria-hidden="true"></i>
                            </button>
                        </div>
                    
                    </div>
                        
                    
                    <component :is="media" :errors="errors"></component>
                    
                    <div>
                        <button @click.prevent="save" class="profile-statusposts__form-btn btn btn--input btn--inline btn--dark-blue">Posten</button>
                    
                        <button @click.prevent="resetForm" class="profile-statusposts__form-btn--reset btn btn--input btn--inline"><i class="fa fa-close" aria-hidden="true"></i> Zurücksetzen</button>
                    </div>
                </form>
            `,
            methods: {
                save: function () {
                    let form = new FormData(document.getElementById('user-post-form'));
                    let post = Object.assign({}, this.formData);

                    this.errors = this.formData.validate();

                    for (let item in post) {
                        if (post.hasOwnProperty(item) && post[item]) {
                            form.append(item, post[item]);
                        }
                    }

                    if (!this.errors) {

                        EventBus.$emit('store', post);
                        EventBus.$emit('reset-post-form');

                        axios.post('/benutzer/post', form).then((res) => {
                            EventBus.$emit('fetch-external');
                        }).catch((err) => {
                            console.log(err);
                        });
                    }

                },
                resetForm: function () {
                    EventBus.$emit('reset-post-form');
                },
                toggleMedia: function (component) {
                    this.media = (this.media != component) ? component : '';
                    return this.media;
                }
            },
            components: {
                formError: FormError,
                formImage: {
                    data: function () {
                        return {
                            images: []
                        }
                    },
                    template: `
                        <div class="user-stream__post-edit-image">
                            <label for="user-stream__post-image" class="btn btn--grey btn--full-width btn--large">
                            <span class="fa fa-upload"></span> Bilder hochladen</label>
                            <input @change="handleImage($event)" id="user-stream__post-image" multiple type="file" class="input-file" name="images[]">
                            
                            <div class="thumbnail__list">
                                <preview-images v-for="image in images" v-if="images && images.length > 0" :image="image"></preview-images>
                            </div>
                        </div>
                    `,
                    methods: {
                        handleImage: function (evt) {

                            this.images = [];

                            if (window.File && window.FileList && window.FileReader) {
                                const files = Array.prototype.slice.call(evt.target.files);

                                files.map((file) => {
                                    if (file.type.match('image')) {
                                        ((image) => {
                                            const fileReader = new FileReader();

                                            fileReader.addEventListener("load", (evt) => {

                                                const img = {
                                                    imageUrl: evt.target.result
                                                };

                                                this.images.push(img);

                                                EventBus.$emit('add-post-image', img);
                                            });

                                            fileReader.readAsDataURL(image);

                                        })(file);

                                    }
                                });
                            }
                        }
                    },
                    components: {
                        previewImages: {
                            props: ['image'],
                            template: `
                                <div class="thumbnail__container">
                                    <img class="thumbnail__image" :src="image.imageUrl">
                                </div>
                            `
                        }
                    }
                },
                formVideo: {
                    props: ['errors'],
                    data: function () {
                        return {
                            youtubeUrl: ''
                        }
                    },
                    template: `
                        <div class="input-group input-group--grown" :class="(errors && errors.video) ? '' : 'input-group--no-spacing'">
                            <div class="user-stream__post-edit-video">
                                <input ref="input" @input="handleInput($event.target.value)" v-model="youtubeUrl"  type="text" class="input profile-statusposts__form-input" placeholder="YouTube Videolink">
                                <form-error :error="errors.video" v-if="errors && 'video' in errors"></form-error>
                                <video-preview :url="youtubeUrl"></video-preview>
                            </div>
                        </div>
                    `,
                    methods: {
                        handleInput: function (url) {
                            let video = new Video;

                            EventBus.$emit('add-video', url);

                            if (url.match(video.identifier_regex) && url.match(video.identifier_regex)[2]) {
                                video.identifier = url.match(video.identifier_regex)[2];
                                video.url = url;
                                video.type = 'youtube';
                                this.youtubeUrl = video.url;

                                EventBus.$emit('add-post-video', video);
                            }
                        },
                    },
                    components: {
                        formError: FormError,
                        videoPreview: {
                            props: ['url'],
                            template: `
                                <img v-if="imageUrl" class="profile-statusposts__videothumb" :src="imageUrl">
                            `,
                            computed: {
                                videoId: function () {
                                    if (this.url) {
                                        let video = new Video;
                                        return this.url.match(video.identifier_regex);
                                    }
                                },
                                imageUrl: function () {
                                    if (this.videoId) {
                                        return `//img.youtube.com/vi/${this.videoId[2]}/0.jpg`;
                                    }
                                }
                            },
                        }
                    }
                },
            }
        }
    }
};

export default PostForm;