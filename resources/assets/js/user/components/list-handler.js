import EventList from './event-list';
import ClubList from './club-list';
import UserList from './user-list';

const ListHandler = {
    props: ['position', 'listClass', 'events', 'clubs', 'users'],
    components: {
        EventList,
        ClubList,
        UserList
    },
    template: `
        <div class="user-stream__item user-stream__item--news">
            <div class="row">
                <event-list :class="listClass" :events="events" v-if="position == 2"></event-list>
                <club-list :class="listClass" :clubs="clubs" v-if="position == 4"></club-list>
                <user-list :class="listClass" :users="users" v-if="position == 6"></user-list>
            </div>
        </div>
    `,
};

export default ListHandler;