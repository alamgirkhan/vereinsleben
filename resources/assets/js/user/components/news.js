const News = {
    props: ['state'],
    template: `
        <div class="user-stream__item user-stream__item--news">
            <div class="row">
                <news-publish-date :date="state.created_at"></news-publish-date>
                <div class="user-stream__news col-lg-11">
                    <news-header :link="state.link" :category="state.category.title" :parentCategory="state.parentCategory" :image="newsImage"></news-header>
                    <news-content :link="state.link" :text="newsText" :title="state.title"></news-content>
                </div>
            </div>
        </div>
    `,
    data: function () {
        return {
            newsImage: {
                backgroundImage: `url(${this.state.image})`
            }
        }
    },
    computed: {
        newsText: function () {
            let text = this.state.content_teaser;
            return (text.length > 150) ? `${text.substr(0, 150)}...` : text;
        }
    },
    components: {
        newsHeader: {
            props: ['parentCategory', 'category', 'image', 'link'],
            template: `
                <div class="user-stream__news-image-wrapper">
                    <a :href="link" class="user-stream__news-link">
                        <div class="user-stream__news-image" :style="image"></div>
                        <p class="user-stream__news-category-wrapper">{{ parentCategory }}
                            <span class="user-stream__news-category">{{ category }}</span>
                        </p>
                    </a>
                </div>
            `
        },
        newsContent: {
            props: ['text', 'title', 'link'],
            template: `
                <div class="user-stream__news-content">
                    <h3 class="user-stream__news-headline" data-insert-hyphen="true">{{ title }}</h3>
                    <p class="user-stream__news-text">{{ text }}</p>
                    <div class="button__center-wrapper">
                        <a :href="link" class="button button--padded button--condensed button--dark">Mehr lesen</a>
                    </div>
                </div>
            `
        },
        newsPublishDate: {
            props   : ['date'],
            template: `<div class="col-lg-1 col-md-1 stream-news__date noborder">
                            <span class="date-no">{{ formattedDate.format("DD") }}</span>
                            <br>
                            <span class="month-no">{{ formattedDate.format("MMM") }}</span>
                            <br>
                            <span class="year-no">{{ formattedDate.format("YYYY") }}</span>
                       </div>`,
            computed: {
                formattedDate: function () {
                    return moment(this.date);
                },
            },
        }
    }
};

export default News;
