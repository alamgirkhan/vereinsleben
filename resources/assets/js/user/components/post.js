const Post = {
    props: ['state'],
    template: `
            <div class="user-stream__item" :class="modifierClass">
       <div class="row">
                    <post-date :date="state.published_at"></post-date>
                    <post-content :content="state.content" :name="state.name" :avatar="state.avatar" :url="state.userUrl" :videos="state.videos" :images="state.images"></post-content>
    </div>
            </div>`,
    computed: {
        modifierClass: function () {
            let modifierClass = 'user-stream__item--post';

            if (this.state.images && this.state.images.length) {
                modifierClass = 'user-stream__item--photo';
            }

            if (this.state.videos && this.state.videos.length) {
                modifierClass = 'user-stream__item--video';
            }

            return modifierClass;
        }
    },
    components: {
        postDate: {
            props: ['date'],
            template: `
    <div class="col-lg-1 col-md-1 stream-news__date noborder">
     <span class="date-no">{{ formattedDate.format("DD") }}</span>
     <br>
     <span class="month-no">{{ formattedDate.format("MMM") }}</span>
     <br>
     <span class="year-no">{{ formattedDate.format("YYYY") }}</span>
    </div>
            `,
            computed: {
                formattedDate: function () {
                    return moment(this.date);
                },
            },
        },
        postContent: {
            props     : ['content', 'videos', 'images', 'name', 'avatar', 'url'],
            template  : `<div class="col-lg-11 col-sm-11 event-post">
       <div :class="modifierClass">
           <post-videos v-if="videos && videos.length > 0" :videos="videos"></post-videos>
           <post-single-image v-if="images && images.length == 1" :images="images"></post-single-image>
        <div class="row post-row-new">            
         <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
             <post-image v-if="images && images.length > 1" :images="images"></post-image>          
          <div class="row">
           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="event-post-thum-arrow" v-if="images && images.length > 0"><i class="fa fa-caret-up" aria-hidden="true"></i></div>
            <div class="row post-head">
             <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 postlogo">
              <post-avatar :avatar="avatar" :url="url"></post-avatar>
             </div>
             <div class="col-lg-9 col-md-9 col-sm-8 col-xs-8 postdetail">
              <div class="postlefttxt" v-html="name"></div>
             </div>
            </div>
           </div>
          </div>
         </div>
         <post-images v-if="images && images.length > 1" :images="images"></post-images>
        </div>
       </div>
       <p v-html="content"><br><br></p>
      </div>
            `,
            computed  : {
                modifierClass: function () {
                    let modifierClass = 'event-post-thum';

                    if (this.images && this.images.length == 1) {
                        modifierClass = 'event-post-single-thum';
                    }

                    if (this.images && this.images.length > 1) {
                        modifierClass = 'event-post-thum';
                    }

                    return modifierClass;
                }
            },
            components: {
                postVideos     : {
                    props: ['videos'],
                    template: `
                        <div class="profile-statusposts__video">
                                <iframe :src="videoSrc" frameborder="0" allowfullscreen></iframe>
                        </div>
                    `,
                    computed: {
                        videoSrc: function () {
                            if (this.videos[0].type === 'youtube') {
                                return `https://www.youtube.com/embed/${this.videos[0].identifier}`
                            } else {
                                return `https://player.vimeo.com/video/${this.videos[0].identifier}?app_id=122963`
                            }
                        }
                    },
                },
                postAvatar     : {
                    props   : ['avatar', 'url'],
                    template: `<a :href="imageHref" target="_blank"><img :src="imageSrc" /></a>`,
                    computed: {
                        imageSrc : function () {
                            return `${this.avatar}`
                        },
                        imageHref: function () {
                            return `${this.url}`
                        },
                    },
                },
                postImage      : {
                    props   : ['images'],
                    template: `
                            <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 lgthum">
            <a :href="imageHref" :data-lightbox="imageableId"><img :src="imageSrc" /></a>
        </div>
       </div>`,
                    computed: {
                        imageSrc   : function () {
                            return `${this.images[0].imageUrl}`
                        },
                        imageHref  : function () {
                            return `${this.images[0].imageUrl}`
                        },
                        imageableId: function () {
                            return `${this.images[0].imageable_id}`
                        },
                    },
                },
                postSingleImage: {
                    props   : ['images'],
                    template: `<a :href="imageHref" :data-lightbox="imageableId"><img :src="imageSrc" /></a>`,
                    computed: {
                        imageSrc   : function () {
                            return `${this.images[0].imageUrl}`
                        },
                        imageHref  : function () {
                            return `${this.images[0].imageUrl}`
                        },
                        imageableId: function () {
                            return `${this.images[0].imageable_id}`
                        },
                    },
                },
                postImages     : {
                    props: ['images'],
                    template: `
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 club-post_right-thums">
       <div class="row">
                                <post-image v-for="image in images.slice(1, 4)" :key="image.id" :image="image"></post-image>
       </div>
                            <div v-if="images && images.length > 4" class="event-plus-icon"><a href="javascript:void(0);">
           <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                            </div>
                            <post-rem-image v-for="image in images.slice(4, 100)" :key="image.id" :image="image"></post-rem-image>       
                        </div>`,
                    components: {
                        postImage: {
                            props: ['image'],
                            template: `
        <a :href="imageHref" :data-lightbox="imageableId">
         <div class="col-lg-12 club-post__sml-image" :style="postImage"></div>
        </a>`,
                            computed: {
                                imageHref  : function () {
                                    return `${this.image.imageUrl}`
                                },
                                imageableId: function () {
                                    return `${this.image.imageable_id}`
                                },
                            },
                            data: function () {
                                return {
                                    postImage: {
                                        background    : `url(${this.image.imageUrl}) no-repeat`,
                                        backgroundSize: `cover`
                                    },
                                }
                            },
                        },
                        postRemImage: {
                            props   : ['image'],
                            template: `
        <a :href="imageHref" :data-lightbox="imageableId"></a>`,
                            computed: {
                                imageHref  : function () {
                                    return `${this.image.imageUrl}`
                                },
                                imageableId: function () {
                                    return `${this.image.imageable_id}`
                                },
                            },
                        },
                    },
                }
            },
        },
    }
};

export default Post;
