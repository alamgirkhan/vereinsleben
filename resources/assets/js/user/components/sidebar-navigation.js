import EventBus from '../utils/EventBus';

const SidebarNavigation = {
    props: ['label', 'entity', 'resource'],
    created: function () {
        this.$watch('entity', (item) => {
            this.hasPage.next = item.next_page_url ? '' : 'hidden';
            this.hasPage.previous = item.prev_page_url ? '' : 'hidden';
        });
    },
    data: function () {
        return {
            hasPage: {
                next: 'hidden',
                previous: 'hidden'
            },
        }
    },
    template: `
        <div class="user-stream__sidebar-navigation">
            <span :style="{ visibility: hasPage.previous }" @click="switchPage(entity.prev_page_url)" class="user-stream__sidebar-navigation-control fa fa-chevron-left"></span>
            <h2>{{label}}</h2>
            <span :style="{ visibility: hasPage.next }" @click="switchPage(entity.next_page_url)" class="user-stream__sidebar-navigation-control fa fa-chevron-right"></span>
        </div>
    `,
    methods: {
        switchPage: function (page) {
            EventBus.$emit('switch-page', page, this.resource);
        }
    }
};

export default SidebarNavigation;