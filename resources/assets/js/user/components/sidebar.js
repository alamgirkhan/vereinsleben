import ClubList from './club-list';
import UserList from './user-list';
import EventList from './event-list';

const Sidebar = {
    props: ['events', 'clubs', 'users'],
    template: `
        <div class="col-lg-4 stream-sidebar-container">
            <div class="rhs-box col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <events :events="events" modifier="user-stream__event-list--sidebar"></events>
                <clubs :clubs="clubs" modifier="user-stream__club-list--sidebar"></clubs>
            </div>
        </div>
    `,
    components: {
        events: EventList,
        clubs: ClubList,
        users: UserList,
    }
};

export default Sidebar;