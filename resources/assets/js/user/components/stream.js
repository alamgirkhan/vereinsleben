import axios from 'axios';
import EventBus from '../utils/EventBus';
import ContentHandler from './content-handler';
import Sidebar from './sidebar';
import Viewports from '../config/viewports';

const Stream = {	
    template: `
            <div class="user-stream__data_1 row">
                <content-handler :items="stream.data" :events="events" :clubs="clubs" :users="users" :campaigns="campaigns" :bannerSpacingStream="bannerSpacingStream"></content-handler>                
                <sidebar v-if="viewports.current >= viewports.md" :events="events" :clubs="clubs" :users="users"></sidebar>
                <div class="button__center-wrapper col-lg-12 col-xs-12 col-md-12 col-sm-12">
                    <button class="more-streaming-data button button--padded button--dark" @click="loadMore" v-if="stream && stream.next_page_url"> Mehr laden </button>
                </div>
            </div>`,
    components: {
        contentHandler: ContentHandler,
        sidebar: Sidebar,
    },
    methods: {
        loadMore() {
            axios.get(`stream${this.stream.next_page_url}`).then((res) => {
                for (let key in res.data['data']) {
                    if (res.data['data'].hasOwnProperty(key)) {
                        this.stream.next_page_url = res.data.next_page_url;
                        this.stream.data.push(res.data['data'][key]);
                    }
                }
            }).catch(err => console.log(err));
        },
        loadEntities(search_id) {
            axios.all([
                axios.get(`stream`).then(function (response) {
                    console.log(response.data);
                    return response;
                }),
                axios.get(`events`),
                axios.get(`clubs`),
                axios.get(`users`),
            ]).then(axios.spread((stream, events, clubs, users) => {
                this.stream = stream.data;
                this.events = events.data;
                this.clubs = clubs.data;
                this.users = users.data;
            })).catch(err => console.log(err));
        },
        handlePaging(search_id) {
            EventBus.$on('switch-page', (page, entity) => {
                axios.get(`${entity}${page}`).then((res) => {
                    this[entity] = res.data;
                });
            });
        },
        loadRandomCampaign() {
            axios.get(`/random-campaign`).then((res) => {
                this.campaigns = res.data.campaigns;
                this.bannerSpacingStream = +res.data.bannerSpacingStream;
            }).catch(err => console.log(err));
        },
    },
    data() {
        return {
            stream    : [],
            events: [],
            clubs: [],
            users: [],
            entityList: [],
            entities  : ['stream', 'events', 'clubs', 'users'],
            viewports : Viewports,
            campaigns: [],
            bannerSpacingStream: 0,
        }
    },
    created() {
        this.loadEntities(search_id);
        this.handlePaging(search_id);
        this.loadRandomCampaign();
    },

};

export default Stream;