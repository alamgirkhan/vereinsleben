import axios from 'axios';
import TabConfig from '../config/tab';
import EventBus from '../utils/EventBus';

const TabBar = {
    template: `
        <div class="tab-bar">
            <ul class="tabs">
                <tab-label :hasClubs="userHasClubs" v-for="tab in tabConfig.items" :component="tab.component" :label="tab.label" :activeTab="tab.active"></tab-label>
            </ul>
            <tab-content :user="user"></tab-content>
        </div>
    `,
    data: function () {
        return {
            tabConfig: Object.assign({}, TabConfig),
            tabs: TabConfig.items,
            components: TabConfig.components,
            user: null,
            userHasClubs: false
        }
    },
    created: function () {

        // get the current user
        axios.get('user/me').then((res) => {
            this.user = res.data[0];
            this.userHasClubs = (this.user && this.user.clubs.length > 0);
        });

        EventBus.$on('toggle-active-tab', (component) => {
            this.tabConfig.items.map((item) => {
                item.active = (item.component == component);

                return item;
            });
        });
    },
    components: {
        tabLabel: {
            props: ['label', 'component', 'activeTab', 'hasClubs'],
            template: `
                <li v-if="showTab" @click.prevent="selectTab" class="tabs__item tabs__item--spaced" :class="{ active: activeTab }">
                    <a class="tabs__link" href="">{{ label }}</a>
                </li>
            `,
            methods: {
                selectTab: function () {
                    EventBus.$emit('change-component', this.component);
                    EventBus.$emit('toggle-active-tab', this.component);
                }
            },
            data: function () {
                return {
                    isActive: this.activeTab,
                    showTab: true
                }
            },
            computed: {
                showTab: function () {
                    let show = true;

                    if (this.component.indexOf('club') == 0 && !this.hasClubs) {
                        show = false;
                    }

                    return show;
                }
            }
        },
        tabContent: {
            props: ['components', 'user'],
            template: `
                <div class="tab-content">
                    <component :user="user" :is="component"></component>
                </div>
            `,
            components: TabConfig.components,
            data: function () {
                return {
                    component: 'post'
                }
            },
            created: function () {
                EventBus.$on('change-component', (component) => {
                    this.component = component;
                });
            }
        }
    },
};

export default TabBar;