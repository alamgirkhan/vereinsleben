const Tab = {
    props: ['tab'],
    template: `
        <li class="tabs__item tabs__item--spaced">
            <a class="tabs__link" href="">{{ tab.label }}</a>
        </li>
    `
};

export default Tab;