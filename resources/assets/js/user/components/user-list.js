import SidebarNavigation from './sidebar-navigation';

const SidebarUsers = {
    props: ['users', 'modifier'],
    template: `
                <div class="user-stream__user-list col-lg-12 col-xs-12 col-sm-12 col-md-12 no-padding" :class="modifier">
                    <sidebar-navigation :entity="users" resource="users" label="Mitglieder"></sidebar-navigation>
                    <user v-for="user in users.data" :user="user"></user>
                </div>
            `,
    components: {
        sidebarNavigation: SidebarNavigation,
        user: {
            props: ['user'],
            template: `
                        <a :href="user.url" class="member-card__link">
                            <div class="member-card member-card--inverted member-card--user-stream">
                                <div class="member-card__profile-image member-card__profile-image--user-stream"
                                     :style="image"></div>
                                <div class="member-card__profile-informations member-card__profile-information--user-stream">
                                    <div class="member-card__credentials">
                        
                                        <div class="member-card__name">
                                            {{ user.fullname }}
                                        </div>
                                        <div class="member-card__username">
                                            {{ user.username }}
                                        </div>
                        
                                    </div>
                                    
                                    <div class="member-card__club-and-interests member-card__club-and-interests--user-stream">
                                        <club v-if="user.clubs && user.clubs.length > 0" :club="user.clubs[0]"></club>
                                        <interest v-if="user.sports && user.sports.length > 0" :interests="user.sports"></interest>
                                    </div>
                                </div>
                            </div>
                        </a>
                    `,
            computed: {
                image: function () {
                    return `background-image: url('${this.user.imageUrl}')`;
                }
            }
            ,
            components: {
                club: {
                    props: ['club'],
                    template: `
                                <div class="member-card__club">
                                    <a class="member-card__club-link" :href="club.url">
                                        <div class="member-card__club-image-wrapper">
                                            <img class="member-card__club-image member-card__club-image--user-stream" :src="club.imageUrl" :alt="club.name" alt="Vereinsbild">
                                        </div>
                                        <div class="member-card__club-name">{{ club.name }}</div>
                                    </a>
                                </div>
                            `
                },
                interest: {
                    props: ['interests'],
                    data: function () {
                        return {
                            userInterests: Array.prototype.slice.call(this.interests, 0, 3),
                        }
                    },
                    computed: {
                        interestData: function () {
                            return this.userInterests.map((interest) => {
                                return interest.title
                            }).join(', ')
                        }
                    },
                    template: `
                                <div>
                                    <div class="member-card__interests">{{ interestData }}</div>
                                </div>
                            `
                }
            }
        },
    }
};

export default SidebarUsers;