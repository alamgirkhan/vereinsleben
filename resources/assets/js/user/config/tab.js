/**
 * for now this configuration statically serves content for the tabs displayed on users stream
 */

import PostForm from '../components/forms/post';
import ClubPostForm from '../components/forms/club-post';
import ClubEventForm from '../components/forms/club-event';

const TabConfig = {
    items: [
        {
            label: 'Eigener Beitrag',
            component: 'post',
            active: true,
        },
        {
            label: 'Vereinsbeitrag',
            component: 'clubPost',
            active: false,
        },
        {
            label: 'Event',
            component: 'clubEvent',
            active: false,
        },
    ],
    components: {
        post: PostForm,
        clubPost: ClubPostForm,
        clubEvent: ClubEventForm
    }
};


export default TabConfig;