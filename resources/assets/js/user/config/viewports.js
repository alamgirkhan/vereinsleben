export default {
    xs: 520,
    sm: 720,
    md: 992,
    lg: 1200,
    current: window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth,
}