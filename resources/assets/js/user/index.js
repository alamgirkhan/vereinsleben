import Vue from 'vue/dist/vue';
import Stream from './components/stream';
import Sidebar from './components/sidebar';

Vue.component('stream', Stream);
Vue.component('sidebar', Sidebar);

const app = new Vue({
    el: '#user-stream'
});

export default app;