export default class SportHandler {

    constructor($el, url) {
        this.element = $el;
        this.url = url;

        return this.init();
    }

    init() {
        return this.element.select2({
            minimumInputLength: 1,
            language: 'de',
            tags: true,
            ajax: {
                url: this.url,
                dataType: 'json',
                delay: 250,

                data(params) {
                    return {
                        search: params.term
                    };
                },

                processResults(data, params) {
                    let results = $.map(data.data, function (val, i) {
                        return {id: val['id'], text: val['title']};
                    });

                    let sorter = function (a, b) {
                        let input_string = params.term;
                        return Levenshtein.get(a.text, input_string) - Levenshtein.get(b.text, input_string);
                    };

                    results.sort(sorter);

                    return {
                        results: results
                    };
                },

                cache: true
            },
            createTag(params) {
                return {
                    id: params.term,
                    text: params.term
                }
            },
            escapeMarkup(markup) {
                return markup;
            },
        });
    }
}