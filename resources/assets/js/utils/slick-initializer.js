import $ from 'jquery';
import slick from 'slick-carousel';

const SlickInitializer = function () {
    if ($('.slide-show')) {
        $('.slide-show').not('.slick-initialized').slick({
            prevArrow: '<button type="button" class="slick-prev"></button>',
            nextArrow: '<button type="button" class="slick-next"></button>',
            autoplay: false,
            arrows: true,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        autoplay: true,
                        arrows: false
                    }
                }
            ]
        });
    }
};

export default SlickInitializer;