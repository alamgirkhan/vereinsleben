<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'Benutzername/E-Mail oder Passwort ist falsch.',
    'throttle' => 'Zu viele Login Versuche. Versuchen Sie es bitte in :seconds Sekunden.',
    'birthday-day' => 'Es muss ein Tag angegeben werden.',
    'birthday-month' => 'Es muss ein Monat angegeben werden.',
    'birthday-year' => 'Es muss ein Jahr angegeben werden.',

];
