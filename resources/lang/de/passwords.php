<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwörter müssen mindestens 6 Zeichen lang sein und korrekt bestätigt werden.',
    'reset'    => 'Das Passwort wurde zurückgesetzt!',
    'sent'     => 'Wir haben dir eine E-Mail gesendet. Mit einem Klick auf den Link in der E-Mail, kannst du dir ein neues Passwort vergeben!',
    'token'    => 'Der Passwort-Wiederherstellungs-Schlüssel ist ungültig oder abgelaufen.',
    'user'     => 'Es konnte leider kein Nutzer mit dieser E-Mail Adresse gefunden werden.',

];
