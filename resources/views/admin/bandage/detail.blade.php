@extends('layouts.master')

@section('content')
    @include('admin.menu')
    <section class="club-new" data-club-editable="true">

        <div class="club-detail-content">
            <div class="container-flex">
                <h1 class="club-detail__headline">Verband bearbeiten</h1>
                <div class="row">
                    <div class="col-md-10 col-md-offset-2">
                        <div class="club-content__edit-area club-content__box">
                            @include('admin.bandage.edit.base-data', ['bandage' => $bandage, 'bandages' => $bandages])
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <section class="admin__overview">
            <div class="container-flex">
                <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">Verein Admins</h1>
                <div class="row">

                    {!! Form::open(['route' => 'admin.bandage.admin.add', 'method' => 'GET', 'id' => 'search', 'class' => '']) !!}
                    <div class="col-md-3">
                        <div class="input-group">
                            {!! Form::text('user_id', isset($query['user_id']) ? $query['user_id'] : null, ['id' => 'user_id', 'autofocus', 'class' => 'input', 'placeholder' => 'Nutzer-ID']) !!}
                        </div>
                    </div>
                    {{ Form::hidden('bandage_id', $bandage->id, array('id' => $bandage->id)) }}
                    <div class="col-md-3">
                        <div class="input-group">
                            {!! Form::submit('Admin hinzufügen', array('class' => 'button button--light button--light-white button--center button--full-width')) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}

                </div>
                <div class="row">
                    @if(count($admins) > 0)
                        <table id="admin-bandage-admins" class="admin__table" data-token="{{ csrf_token() }}">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Admin-Name</th>
                                <th>Admin-Email</th>
                                <th>Optionen</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($admins as $admin)
                                <tr>
                                    <td><a href="{{route('user.detail', ['username' => $admin->username])}}"
                                           target="_blank">{{ $admin->id }}</a>
                                    </td>
                                    <td>
                                        {{ $admin->firstname}}&nbsp;{{ $admin->lastname }}</td>
                                    <td>{{ $admin->email }}</td>
                                    <td>
                                        <div class="admin__table-option">
                                            <a onclick="return confirm('Vereinsadmin wirklich löschen?')"
                                               href="{{ route('admin.bandage.admin.delete', ['bandage_id' => $bandage->id, 'id' => $admin->id])}}">
                                                <span class="fa fa-trash"></span>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </section>
@endsection

@push('scripts')
    <script>
        $().ready(function(){
            var editClubSelector = '[data-club-editable]';
            var sectionSelector = '[data-section-editable]';
            var editButtonSelector = '.club-content__edit-button';
            var cancelEditButtonSelector = '.club-content__form-cancel-action-button';
            var editAreaSelector = '.club-content__edit-area';
            var viewAreaSelector = '.club-content__view-area';

            var $editClubHandle = $(editClubSelector);
            if ($editClubHandle) {
                $editClubHandle.find(sectionSelector).each(function () {
                    $(this).find(editAreaSelector).hide();
                });

                $(editClubSelector).on('click', editButtonSelector, function (e) {
                    // e.preventDefault();
                    var $section = $(this).closest(sectionSelector);
                    $section.find(viewAreaSelector).hide();
                    $section.find(editButtonSelector).hide();
                    $section.find(editAreaSelector).show();
                });

                $(editClubSelector).on('click', cancelEditButtonSelector, function (e) {
                    e.preventDefault();
                    var $section = $(this).closest(sectionSelector);
                    $section.find(viewAreaSelector).show();
                    $section.find(editButtonSelector).show();
                    $section.find(editAreaSelector).hide();
                });
            }
        });

        $('#admin-bandage-admins').DataTable({
            dom         : 'Blfrtip',
            "lengthMenu": [50, 100, 250],
            buttons     : [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],

            language: {
                url: '/js/vendor/i18n/dataTables.german.json'
            }
        }).on('page.dt', function() {
            $('html, body').animate({
                scrollTop: $(".dataTables_wrapper").offset().top
            }, 'slow');

            $('thead tr th:first-child').focus().blur();
        });
    </script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script src="http://legacy.datatables.net/release-datatables/extras/TableTools/media/js/TableTools.js"></script>
@endpush