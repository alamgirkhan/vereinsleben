@extends('layouts.master')

@section('content')
    {{--@include('components.headerimage-still',
    ['condensed' => true,
     'content' => [
            'icon' =>'newspaper-o',
            'headline' => 'Vereine',
    ]])--}}
    @include('admin.menu')
    <section class="admin__overview" data-token="{{ csrf_token() }}"
             data-mapping-url="{{ app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('api.club.list') }}">
        <div class="container">
            <div class="row admin__section">
                <div class="col-xs-12">
                    <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">VERBÄNDE</h1>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <a href="{{ route('bandage.create') }}"
                           class="button button--icon button--full-width button--center button--light">
                            <span class="fa fa-plus"></span> Lege deinen Verband an
                        </a>
                    </div>
                </div>
            </div>
            @if($bandages)
                <table id="admin-bandage" class="admin__table" data-token="{{ csrf_token() }}">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Sportart</th>
                        <th>Bereich</th>
                        <th>Optionen</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($bandages as $bandage)
                        <tr>
                            <td onclick='window.open("{{ route('bandage.detail', ['bandage' => $bandage->slug]) }}")'>
                                {{ $bandage->id }}
                            </td>
                            <td>{{ $bandage->name}}</td>
                            <td>
                                @if(isset($bandage->sports))
                                    @foreach($bandage->sports as $key => $sport)
                                        <span>
                                            {{$sport->title}}{{$key != (count($bandage->sports) -1) ? ', ' : ''}}
                                        </span>
                                    @endforeach

                                @endif
                            </td>
                            <td>{{ $bandage->region }}</td>
                            <td>
                                <div class="admin__table-option">
                                    <a href="{{ route('bandage.admin.show', ['slug' => $bandage->slug]) }}"
                                       target="_blank" data-toggle="tooltip" data-placement="top"
                                       title="Bearbeiten"><span class="fa fa-edit"></span></a>
                                    @if($bandage->published == 0)
                                        <a href="{{ route('admin.bandage.deactivate', ['id' => $bandage->id])}}"
                                           data-toggle="tooltip" data-placement="top" title="Aktiv/Inaktiv">
                                            <span class="fa fa-globe" style="color:red"></span>
                                        </a>
                                    @endif
                                    @if($bandage->published == 1)
                                        <a href="{{ route('admin.bandage.deactivate', ['id' => $bandage->id])}}"
                                           data-toggle="tooltip" data-placement="top" title="Aktiv/Inaktiv">
                                            <span class="fa fa-globe" style="color:green"></span>
                                        </a>
                                    @endif
                                    @if(is_null($bandage->deleted_at))
                                        {!! Form::model(
                                             $bandage,
                                             array(
                                                 'method' => 'delete',
                                                 'route' => ['bandage.delete', $bandage->slug, '#allgemeine-daten'],
                                                 'class' => 'club-edit__form',
                                                 'files' => 'false'
                                             )
                                         )!!}

                                        <button onclick="return confirm('Möchten Sie diesen Verband wirklich löschen?')" type="submit" class="btn-del">
                                            <i class="fa fa-trash"></i>
                                        </button>

                                        {!!Form::close() !!}
                                    @endif
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </section>
    @push('scripts')
        <script>
            $(document).ready(function () {
              $('#admin-bandage').DataTable({
                    dom         : 'Blfrtip',
                    "lengthMenu": [50, 100, 250],
                    buttons     : [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5'
                    ],

                    language: {
                        url: '/js/vendor/i18n/dataTables.german.json'
                    }
                }).on('page.dt', function() {
                    $('html, body').animate({
                        scrollTop: $(".dataTables_wrapper").offset().top
                    }, 'slow');

                    $('thead tr th:first-child').focus().blur();
                });
            })
        </script>


        <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
        <script src="http://legacy.datatables.net/release-datatables/extras/TableTools/media/js/TableTools.js"></script>

    @endpush
@endsection