@extends('layouts.master')

@section('content')
    @include('admin.menu')
    <section class="club-new">
        <div class="club-detail-content">
            <div class="container">
                <div class="row">
                    <h1 class="club-detail__headline">Verband erstellen</h1>
                    <div class="col-md-10 col-md-offset-2">
                        <div class="club-content__edit-area club-content__box">
                            @include('admin.bandage.edit.base-data', ['bandage' => $bandage])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

