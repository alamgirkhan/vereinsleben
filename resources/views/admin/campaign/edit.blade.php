@extends('layouts.master')

@section('js-additional-vendor')
    <script src="{{ asset(('js/vendor/ckeditor/ckeditor.js')) }}"></script>
@endsection

@section('content')
    <section class="admin__overview">
        <div class="container-flex">
            <a class="" href="{{ url('admin/campaigns') }}">
                <span class="fa fa-caret-left"></span> Zur Übersicht
            </a>
            <form enctype="multipart/form-data" id="frm-kam" method="POST" action="/admin/campaign/{{ isset($campaign->id) ? 'update/'.$campaign->id : 'create' }}">
                @method(isset($campaign->id) ? 'PUT' : 'POST')
                <div class="row">
                    <div class="col-md-10 col-md-offset-2">
                        <h3>Kampagne anlegen oder editieren</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <label for="name">Name der Kampagne {{$campaign->name}}</label>
                                    {!! Form::text('name', isset($campaign->id) ? $campaign->name : null, ['class' => 'input', 'required' => 'required']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <label for="name">Name des Kunden</label>
                                    {!! Form::text('customers_name', isset($campaign->id) ? $campaign->customers_name : null, ['class' => 'input', 'required' => 'required']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <label for="name">Link für Kampagne</label>
                                    {!! Form::url('link', isset($campaign->id) ? $campaign->link : null, ['class' => 'input', 'required' => 'required']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <label for="name">Veröffentlicht Von</label>
                                    {!! Form::text('published_from', isset($campaign->id) ?  date('d.m.Y H:i', strtotime($campaign->published_from)) : null, ['class' => 'input', 'id' => 'published_from', 'data-datetimepicker-enabled', 'required' => 'required', 'autocomplete' => 'off']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <label for="name">Veröffentlicht Bis</label>
                                    {!! Form::text('published_to', isset($campaign->id) ?  date('d.m.Y H:i', strtotime($campaign->published_to)) : null, ['class' => 'input','id' => 'published_to', 'data-datetimepicker-enabled', 'required' => 'required', 'autocomplete' => 'off']) !!}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="input-group">
                                    <label for="name">Beschreibung</label>
                                    {!! Form::textarea('description', isset($campaign->id) ? $campaign->description : null, ['class' => 'input', 'required' => 'required', 'rows' => 10, 'cals' => 50]) !!}
                                </div>
                            </div>
                            <div class="col-md-12 text-center list-adv">
                                <h4>Werbemittel <span class="error-image-preview danger"></span></h4>
                                <div class="col-md-4">
                                    <div class="club-detail__inline-form-submit-wrapper">
                                        <input type="file" class="input-file" id="advertising_material_1" name="banner_horizontal" data-id="1" accept='image/*'>

                                        <input type="hidden" name="banner_horizontal" value="{{isset($campaign->banner_horizontal) ? $campaign->banner_horizontal : ''}}">

                                        <label class="button button--grey button--center button--icon button--full-width btn-add-campaign" for="advertising_material_1">Banner - Horizontal</label>
                                    </div>
                                    <figure class="image-preview">
                                        <img id="output_image_1" alt="" class="img-responsive" src="{{ isset($campaign->id) ? url('/uploaded/campaign/'.$campaign->banner_horizontal) : null }}"/>
                                    </figure>
                                </div>
                                <div class="col-md-4">
                                    <div class="club-detail__inline-form-submit-wrapper">
                                        <input type="file" class="input-file" id="advertising_material_2" name="banner_rechteck" data-id="2" accept='image/*'>

                                        <input type="hidden" name="banner_rechteck" value="{{isset($campaign->banner_rechteck) ? $campaign->banner_rechteck : ''}}">

                                        <label class="button button--grey button--center button--icon button--full-width btn-add-campaign" for="advertising_material_2">Banner - Rechteck</label>
                                    </div>
                                    <figure class="image-preview">
                                        <img id="output_image_2" alt="" class="img-responsive" src="{{ isset($campaign->id) ? url('/uploaded/campaign/'.$campaign->banner_rechteck) : null }}"/>
                                    </figure>
                                </div>
                                <div class="col-md-4">
                                    <div class="club-detail__inline-form-submit-wrapper">
                                        <input type="file" class="input-file" id="advertising_material_3" name="banner_vertikal" data-id="3" accept='image/*'>

                                        <input type="hidden" name="banner_vertikal" value="{{isset($campaign->banner_vertikal) ? $campaign->banner_vertikal : ''}}">

                                        <label class="button button--grey button--center button--icon button--full-width btn-add-campaign" for="advertising_material_3">Banner - Vertikal</label>
                                    </div>
                                    <figure class="image-preview">
                                        <img id="output_image_3" alt="" class="img-responsive" src="{{ isset($campaign->id) ? url('/uploaded/campaign/'.$campaign->banner_vertikal) : null }}"/>
                                    </figure>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <h3>Bundesländer</h3>
                            </div>

                            <div class="col-md-12">
                                <div class="input-group">
                                    {{ Form::checkbox('All', '1', '', ['id' => 'checkAll']) }}
                                    {!! Form::label('Alle auswählen', '', ['class' => 'input-label']) !!}
                                </div>
                            </div>
                            @foreach($federal_states as $state)
                                <div class="col-md-3">
                                    <div class="input-group">
                                        {{ Form::checkbox('federal_states[]',
                                         $state->id,
                                          in_array($state->id, $campaign->states()->pluck('state_id')->all() ) ? true : false
                                        , ['id' => $state->constant]) }}
                                        {!! Form::label($state->constant, $state->name, ['class' => 'input-label']) !!}
                                    </div>
                                </div>
                            @endforeach
                            <div class="col-md-12">
                                <div class="input-group">
                                    <input class="input-checkbox" {{ (isset($campaign->id) && ($campaign->state === \Vereinsleben\Campaign::STATE_HIDDEN)) ? 'checked' : '' }}
                                           type="radio"
                                           name="state"
                                           id="admin-news-draft"
                                           value="HIDDEN">
                                    <label class="input-checkbox__label"
                                           for="admin-news-draft">Entwurf</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="input-group">
                                    <input class="input-checkbox" {{ (isset($campaign->id) && ($campaign->state === \Vereinsleben\Campaign::STATE_PUBLISHED)) ? 'checked' : '' }}
                                           type="radio"
                                           name="state"
                                           id="admin-news-published"
                                           value="PUBLISHED">
                                    <label class="input-checkbox__label"
                                           for="admin-news-published">Veröffentlicht</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button class="button btn-submit button--icon button--full-width button--center button--light" type="submit">Kampagne speichern</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection
@push('scripts')
    <script>
        $().ready(function () {
            $('#published_to').datetimepicker({
                useCurrent: false,
                minDate: moment()
            });
            $('#published_from').datetimepicker().on('dp.change', function (e) {
                var incrementDay = moment(new Date(e.date));
                incrementDay.add(1, 'days');
                $('#published_to').data('DateTimePicker').minDate(incrementDay);
                $(this).data("DateTimePicker").hide();
            });

            $('#published_to').datetimepicker().on('dp.change', function (e) {
                var decrementDay = moment(new Date(e.date));
                decrementDay.subtract(1, 'days');
                $('#published_from').data('DateTimePicker').maxDate(decrementDay);
                $(this).data("DateTimePicker").hide();
            });
            function preview_image(input, id)
            {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#output_image_' + id).attr('src', e.target.result);
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            }
            $('.input-file').change(function () {
                preview_image(this, $(this).attr('data-id'));
            });
        })
    </script>
@endpush