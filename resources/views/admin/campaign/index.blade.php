@extends('layouts.master')

@section('content')
    @include('admin.menu')
    <section class="admin__overview">
        <div class="container">
            <div class="row admin__section">
                <div class="col-xs-12">
                    <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">Kampagne</h1>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <a href="{{ route('admin.campaign.create') }}"
                           class="button button--icon button--full-width button--center button--light">
                            <span class="fa fa-plus"></span> Neue Kampagne anlegen
                        </a>
                    </div>
                </div>
                <div class="col-md-4"></div>
                <div class="col-md-5">
                    <form action="{{route('admin.bannerSpacingStream')}}" method="POST">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-6"><label for="">Bannerabstand im Stream</label></div>
                            <div class="col-md-3"><input value="{{$bannerSpacingStream ? $bannerSpacingStream : 0}}" type="number" class="input" name="banner_spacing_stream"></div>
                            <div class="col-md-3"><button class="button--full-width button button--icon button--center button--light">Speichern</button></div>
                        </div>
                    </form>
                </div>
            </div>
            <table id="admin-table" class="admin__table" data-token="{{ csrf_token() }}">
                <thead>
                <tr>
                    <th style="width:7%">ID&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:35%">Name&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:10%">Kunde&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:15%">Von<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:15%">Bis<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:10%">Publiziert</th>
                    <th style="width:10%">Optionen</th>
                </tr>
                </thead>
                <tbody>
                @foreach($campaigns as $key => $campaign)
                    <tr>
                        <td>{{ $campaign->id }}</td>
                        <td>{{ $campaign->name }}</td>
                        <td>
                            {{ $campaign->customers_name }}
                        </td>
                        <td>
                            {{ date('Y/m/d', strtotime($campaign->published_from)) }}
                        </td>
                        <td>
                            {{ date('Y/m/d', strtotime($campaign->published_to)) }}
                        </td>
                        <td>
                            <div class="onoffswitch">
                                <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox"
                                       id="news-published-{{$key}}" {{ ($campaign->state === \Vereinsleben\Campaign::STATE_PUBLISHED) ? 'checked' : '' }}>
                                <label data-toggle-campaign-published data-campaign-id="{{ $campaign->id }}"
                                       class="onoffswitch-label" for="news-published-{{$key}}"></label>
                            </div>
                        </td>
                        <td>
                            <div class="admin__table-option">
                                <a href="{{ route('admin.campaign.edit', $campaign->id) }}">
                                    <span class="fa fa-edit"></span>
                                </a>
                                <a onclick="return confirm('Möchtest du diese Aufzeichnung wirklich löschen?')"
                                   href="{{ route('admin.campaign.detroy', $campaign->id) }}">
                                    <span class="fa fa-trash"></span>
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </section>
@endsection