@extends('layouts.master')

@section('content')
    @include('admin.menu')
    <section class="admin__overview">
        <div class="container-flex">
            @if($errors->count() > 0)
                @foreach($errors->all() as $error)
                    <p>{{$error}}</p>
                @endforeach
            @endif
            <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">Vereinsdaten bearbeiten</h1>
            <div class="row">
                <div class="col-md-10 col-md-offset-2">

                    @if ($club->id === null)
                        {!! Form::model(
                            $club,
                            array(
                                'method' => 'post',
                                'route' => 'club.store',
                                'class' => 'club-edit__form',
                                'files' => 'true'
                            )
                        )!!}
                    @else
                        {!! Form::model(
                            $club,
                            array(
                                'method' => 'post',
                                'route' => ['club.updatePut'],
                                'class' => 'club-edit__form',
                                'files' => 'true'
                            )
                        )!!}
                        <input type="hidden" name="slug" value="{{$club->slug}}">
                    @endif
                    <div class="row">
                        <div class="col-md-6">
                            {!! Form::label('name', 'Vereinsname *', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('name', null, [
                                        'required',
                                        'class' => 'input',
                                        'title' => 'Vereinsname'
                                    ])
                                !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="club-detail__overview-item">
                                {!! Form::label('bandage', 'Verband', ['class' => 'input-label']) !!}
                                <div class="input-group">
                                    {!! Form::select('bandage', collect(['' => 'Bitte wählen']),$club->bandage===null?'':$club->bandage->slug, [
                                            'class' => 'select',
                                            'title' => 'Mitglieder'
                                        ])
                                    !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            {!! Form::label('street', 'Straße  *', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('street', null, [
                                    'required',
                                    'class' => 'input',
                                    'placeholder' => 'Straße',
                                    'title' => 'Straße'
                                    ])
                                !!}
                            </div>
                        </div>
                        <div class="col-md-3">
                            {!! Form::label('house_number', 'Hausnummer', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('house_number', null, [
                                    'class' => 'input',
                                    'placeholder' => 'Hausnummer',
                                    'title' => 'Hausnummer'
                                    ])
                                !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            {!! Form::label('zip', 'Postleitzahl', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('zip', null, [
                                        'class' => 'input',
                                        'placeholder' => 'PLZ',
                                        'title' => 'PLZ'
                                        ])
                                !!}
                            </div>

                        </div>
                        <div class="col-md-4">
                            {!! Form::label('city', 'Ort *', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('city', null, [
                                        'required',
                                        'class' => 'input',
                                        'placeholder' => 'Ort',
                                        'title' => 'Stadt'
                                        ])
                                !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            {!! Form::label('email', 'E-Mail-Adresse', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('email', null, [
                                        'class' => 'input',
                                        'placeholder' => 'E-Mail-Adresse',
                                        'title' => 'E-Mail-Adresse'
                                        ])
                                !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="club-detail__overview-item">
                                {!! Form::label('founded', 'Gegründet', ['class' => 'input-label']) !!}
                                <div class="input-group">
                                    {!! Form::text('founded', null, [
                                            'class' => 'input',
                                            'placeholder' => 'z.B. '.date('Y'),
                                            'title' => 'Gegründet'
                                        ])
                                    !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="club-detail__overview-item">
                                {!! Form::label('shorthand', 'Kürzel', ['class' => 'input-label']) !!}
                                <div class="input-group">
                                    {!! Form::text('shorthand', null, [
                                            'class' => 'input',
                                            'placeholder' => 'Kürzel',
                                            'title' => 'Kürzel'
                                        ])
                                    !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="club-detail__overview-item">
                                {!! Form::label('member_count', 'Mitglieder', ['class' => 'input-label']) !!}
                                <div class="input-group">
                                    {!! Form::select('member_count', [
                                            '' => 'Anzahl auswählen',
                                            '1-10' => '1-10',
                                            '11-50' => '11-50',
                                            '51-100' => '51-100',
                                            '101-250' => '101-250',
                                            '251-500' => '251-500',
                                            '501-750' => '501-750',
                                            '751-1000' => '751-1000',
                                            '1001-1500' => '1001-1500',
                                            '1500+' => '1500+',
                                    ], $club->member_count, [
                                            'class' => 'select',
                                            'title' => 'Mitglieder'
                                        ])
                                    !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="club-detail__overview-item">
                                {!! Form::label('sports', 'Sportart(en) auswählen *', ['class' => 'input-label']) !!}
                                <div class="input-group">
                                    <select class="club-detail__select-sports" name="sports[]" multiple="multiple"
                                            data-select-sports="true"
                                            data-sports-url="{{ app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('api.sports') }}"
                                            required="required">
                                        @foreach($club->sports as $sport)
                                            <option value="{{ $sport->id }}"
                                                    selected="selected">{{ $sport->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <p class="input-label">
                                * Pflichtfelder
                            </p>
                        </div>
                    </div>

                    <div class="club-content__form-action-wrapper">
                        <a href="{{ route('admin.clubs.list', session('clubListQueryString')) }}"
                           class="club-content__form-cancel-action-button button button--grey button--center button--full-width">
                            Abbrechen
                        </a>
                        {!! Form::submit('Speichern',
                            array(
                                'class' => 'button button--icon button--full-width button--center button--light'
                            ))
                        !!}
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </section>
    <section class="admin__overview">
        <div class="container-flex">
            <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">Verein Admins</h1>
            <div class="row">

                {!! Form::open(['route' => 'admin.club.admin.add', 'method' => 'GET', 'id' => 'search', 'class' => '']) !!}
                <div class="col-md-3">
                    <div class="input-group">
                        {!! Form::text('userid', isset($query['userid']) ? $query['userid'] : null, ['id' => 'userid', 'autofocus', 'class' => 'input', 'placeholder' => 'Nutzer-ID']) !!}
                    </div>
                </div>
                {{ Form::hidden('clubid', $club->id, array('id' => $club->id)) }}
                <div class="col-md-3">
                    <div class="input-group">
                        {!! Form::submit('Admin hinzufügen', array('class' => 'button button--light button--light-white button--center button--full-width')) !!}
                    </div>
                </div>
                {!! Form::close() !!}

            </div>
            <div class="row">
                @if(count($admins) > 0)
                    <table id="admin-club-admins" class="admin__table" data-token="{{ csrf_token() }}">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Admin-Name</th>
                            <th>Admin-Email</th>
                            <th>Optionen</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($admins as $admin)
                            <tr>
                                <td><a href="{{route('user.detail', ['username' => $admin->username])}}"
                                       target="_blank">{{ $admin->id }}</a>
                                </td>
                                <td>
                                    {{ $admin->firstname}}&nbsp;{{ $admin->lastname }}</td>
                                <td>{{ $admin->email }}</td>
                                <td>
                                    <div class="admin__table-option">
                                        <a onclick="return confirm('Vereinsadmin wirklich löschen?')"
                                           href="{{ route('admin.club.admin.delete', ['clubid' => $club->id, 'id' => $admin->id])}}">
                                            <span class="fa fa-trash"></span>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </section>
    @push('scripts')
        <script>
            $(document).ready(function () {
                $('#admin-club-admins').DataTable({
                    dom         : 'Blfrtip',
                    "lengthMenu": [50, 100, 250],
                    buttons     : [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5'
                    ],

                    language: {
                        url: '/js/vendor/i18n/dataTables.german.json'
                    }
                });
            })
        </script>

        <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    @endpush
@endsection