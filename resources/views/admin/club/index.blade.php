@extends('layouts.master')

@section('content')
    {{--@include('components.headerimage-still',
    ['condensed' => true,
     'content' => [
            'icon' =>'newspaper-o',
            'headline' => 'Vereine',
    ]])--}}
    @include('admin.menu')
    <section class="admin__overview" data-token="{{ csrf_token() }}"
             data-mapping-url="{{ app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('api.club.list') }}">
        <div class="container">


            @if($adminRequestedClubs->count() > 0)
                <div class="row">
                    <div class="col-md-12"><h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">Vereine
                            Übernahme</h1></div>
                </div>
                <table class="table table-striped table-bordered">
                    <tr>
                        <th><strong>Datum</strong></th>
                        <th><strong>Vereinsname</strong></th>
                        <th><strong>Username</strong></th>
                        <th><strong>Übernahme</strong></th>
                        <th><strong>Ablehnen</strong></th>
                    </tr>

                    @foreach($adminRequestedClubs as $adminRequestedClub)
                        @if(is_null(\Vereinsleben\Club::find($adminRequestedClub->club_id)))
                            @continue
                        @endif
                        <?php $clubname = Club::find($adminRequestedClub->club_id)->name; ?>
                        <?php $clubSlug = Club::find($adminRequestedClub->club_id)->slug; ?>

                        @if( is_null( \Vereinsleben\User::find($adminRequestedClub->user_id)))
                            @continue
                        @endif

                        <?php $username = \Vereinsleben\User::find($adminRequestedClub->user_id)->username; ?>
                        <?php $userfullname = \Vereinsleben\User::find($adminRequestedClub->user_id)->firstname . ' ' .
                            \Vereinsleben\User::find($adminRequestedClub->user_id)->lastname; ?>


                        @if(isset($clubSlug) && isset($username))
                        <tr>
                            <td>{{ $adminRequestedClub->created_at }}</td>
                            <td><a href="{{route('club.detail', ['slug' => $clubSlug])}}"
                                   class="club-tile-wide__link" target='_blank'>
                                    {{ $clubname }}</a></td>
                            <td><a href="{{route('user.detail', ['username' => $username])}} "
                                   class="club-tile-wide__link" target='_blank'>
                                    @if($userfullname == ' ') Kein Name @else {{ $userfullname }} @endif</a></td>
                            <td>
                                {!! Form::open(
                                [
                                    'route' => 'admin.club.request.accept',
                                    'method' => 'POST',
                                ]
                            )!!}
                                {{ Form::hidden('username', $username) }}
                                {{ Form::hidden('clubSlug', $clubSlug) }}
                                {{ Form::submit('akzeptieren', ['class' => 'button button--center button--light button--full-width']) }}
                                {!! Form::close() !!}
                            </td>
                            <td>
                                {!! Form::open(
                                            [
                                                'route' => 'admin.club.request.decline',
                                                'method' => 'POST',
                                            ]
                                            )!!}
                                {{ Form::hidden('username', $username) }}
                                {{ Form::hidden('clubSlug', $clubSlug) }}
                                {{ Form::submit('ablehnen', ['class' => 'button button--center button--light button--full-width']) }}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        @endif
                    @endforeach
                </table>
            @endif

        </div>
    </section>

    @push('scripts')
        <script src="{{asset('js/admin.js')}}"></script>
        <script></script>
    @endpush
@endsection