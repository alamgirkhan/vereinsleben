@extends('layouts.master')

@section('content')
    @include('admin.menu')
    <section class="admin__overview">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">VEREINE</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    {!! Form::open(['route' => 'admin.clubs.list', 'method' => 'GET', 'id' => 'search', 'class' => '']) !!}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                {!! Form::text('keyword', isset($query['keyword']) ? $query['keyword'] : null, ['id' => 'keyword', 'autofocus', 'class' => 'input', 'placeholder' => 'STICHWORT']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                {!! Form::text('email', isset($query['email']) ? $query['email'] : null, ['id' => 'email', 'autofocus', 'class' => 'input', 'placeholder' => 'E-Mail']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                {!! Form::text('city', isset($query['city']) ? $query['city'] : null, ['id' => 'city', 'autofocus', 'class' => 'input', 'placeholder' => 'ORT/Bundesland']) !!}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                {!! Form::text('von', isset($query['von']) ? $query['von'] : null, ['id' => 'von', 'autofocus', 'class' => 'input', 'placeholder' => 'PLZ / PLZ von']) !!}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                {!! Form::text('bis', isset($query['bis']) ? $query['bis'] : null, ['id' => 'bis', 'autofocus', 'class' => 'input', 'placeholder' => 'PLZ bis']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                {!! Form::select('date_filter', [
                                        '' => 'Datumsoption',
                                        'created_at' => 'übernommen',
                                        'updated_at' => 'Profil aktualisiert',
                                ], isset($query['date_filter']), ['class' => 'select']) !!}
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="input-group">
                                {!! Form::text('anfang', isset($query['anfang']) ? date('Y-m-d h:i:s',strtotime($query['anfang'])) : null, ['id' => 'anfang', 'autofocus', 'class' => 'input', 'placeholder' => 'Anfangsdatum', 'data-datetimepicker-enabled']) !!}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                {!! Form::text('ende', isset($query['ende']) ? date('Y-m-d h:i:s',strtotime($query['ende'])) : null, ['id' => 'ende', 'autofocus', 'class' => 'input', 'placeholder' => 'Enddatum', 'data-datetimepicker-enabled']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            {{--{!! Form::label('sport', 'Nach Sportart filtern', ['class' => 'input-label']) !!}--}}
                            <div class="input-group">
                                {!! Form::text('sport', Request::get('sports'), ['class' => 'input sport-autocomplete', 'data-sports-url' => app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('api.sports'), 'placeholder' => 'SPORTART']) !!}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <a href="{{ route('admin.clubs') }}"
                                   class="button button--icon button--full-width button--center button--light"
                                   target="_blank">
                                    Vereinsübernahme
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <a href="{{ route('club.create') }}"
                                   class="button button--icon button--full-width button--center button--light"
                                   target="_blank">
                                    Verein hinzufügen
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <a href="{{ route('admin.clubs.vita') }}"
                                   class="button button--icon button--full-width button--center button--light"
                                   target="_blank">
                                    Vereinsvorschläge
                                </a>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="input-group">
                                <a href="{{ url('admin/clubs/exportclubdata') }}" style="text-decoration: none;">

                                    <button id="exportclubdata"
                                            class="button button--light button--light-white button--center button--full-width">
                                        Export
                                    </button>
                                </a>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="input-group">
                                @if($exp_club)
                                    <a href="{{route('admin.club.download')}}"
                                       target="_blank"
                                       class="button button--light button--light-white button--center button--full-width"
                                       style="background-color: red;">
                                        Download
                                    </a>
                                @else
                                    <a href="#"
                                       target="_blank"
                                       class="button button--light button--light-white button--center button--full-width">
                                        Download
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group">
                                {!! Form::submit('Finden', array('class' => 'button button--light button--light-white button--center button--full-width')) !!}
                            </div>
                        </div>
                        <div class="col-md-3">
							<span class="undo"><a href="{{ url('admin/clubs/list') }}">Zur&uuml;cksetzen <i
                                            class="fa fa-undo"></i></a></span>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

            @if($clubs)
                <table id="admin-club" class="admin__table" data-token="{{ csrf_token() }}">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Ort</th>
                        <th>Mapping</th>
                        <th>Optionen</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($clubs as $club)
                        <tr>
                            <td onclick='window.open("{{ route('club.detail', ['club' => $club->slug]) }}")'>
                                {{ $club->id }}
                            </td>
                            <td>{{ $club->name}}</td>
                            <td>{{ $club->city }}</td>
                            <td>{!! Form::open(['route' => 'admin.club.map', 'method' => 'GET', 'id' => 'search', 'class' => '']) !!}
                                <div class="input-group">
                                    <input type="hidden" id="federal_state" value="">
                                    {{ Form::hidden('oldclubid', $club->id, array('id' => $club->id)) }}
                                    {!! Form::text('newclub',Request::get('clubs'), ['class' => 'input club-autocomplete', 'data-clubs-url' => app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('api.club.autocomplete'), 'placeholder' => 'Behalten']) !!}
                                </div>
                            </td>
                            <td>
                                <div class="admin__table-option">
                                    <button type="submit" name="" onclick="return confirm('Verein wirklich mappen?')"
                                            class=""
                                            style="border:0; background: none !important; background-color: transparent !important; outline: none !important; padding: 0; vertical-align: baseline; cursor:pointer !important;"
                                            data-toggle="tooltip" data-placement="top" title="Mapping"><i
                                                class="fa fa-chain"></i>
                                    </button>
                                    {!! Form::close() !!}

                                    <a href="{{route('admin.club.edit', ['slug' => $club->slug])}}"
                                       target="_blank" data-toggle="tooltip" data-placement="top"
                                       title="Bearbeiten"><span class="fa fa-edit"></span></a>
                                    @if($club->published == 0)
                                        <a href="{{ route('admin.club.deactivate', ['id' => $club->id])}}"
                                           data-toggle="tooltip" data-placement="top" title="Aktiv/Inaktiv">
                                            <span class="fa fa-globe" style="color:red"></span>
                                        </a>
                                    @endif
                                    @if($club->published == 1)
                                        <a href="{{ route('admin.club.deactivate', ['id' => $club->id])}}"
                                           data-toggle="tooltip" data-placement="top" title="Aktiv/Inaktiv">
                                            <span class="fa fa-globe" style="color:green"></span>
                                        </a>
                                    @endif
                                    <a onclick="return confirm('Verein wirklich zurücksetzen?')"
                                       href="{{ route('admin.club.reset', ['id' => $club->id])}}" data-toggle="tooltip"
                                       data-placement="top" title="Reset">
                                        <span class="fa fa-undo"></span>
                                    </a>
                                    @if(is_null($club->deleted_at))
                                        <a data-toggle-delete data-record-id="{{ $club->id }}"
                                           data-delete-path="{{ route('admin.club.destroy') }}" href="#"
                                           data-toggle="tooltip" data-placement="top" title="Löschen">
                                            <span class="fa fa-trash"></span>
                                        </a>
                                    @endif
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </section>
    @push('scripts')
        <script>
            var clubtab;
            $(document).ready(function () {
                clubtab = $('#admin-club').DataTable({
                    dom         : 'Blfrtip',
                    "lengthMenu": [50, 100, 250],
                    buttons     : [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5'
                    ],

                    language: {
                        url: '/js/vendor/i18n/dataTables.german.json'
                    }
                }).on('page.dt', function() {
                    $('html, body').animate({
                        scrollTop: $(".dataTables_wrapper").offset().top
                    }, 'slow');

                    $('thead tr th:first-child').focus().blur();
                });

                $('#exportclubdata').click(function (event) {

                    // event.preventDefault();

                    var tmp = $.makeArray(clubtab.data().map(function (item) {
                        return item[0]
                    }));

                    var ids = $.makeArray(tmp);
                    ids = JSON.stringify(ids);


                    $.ajax("/admin/clubs/exportclubdata", {
                        method : "POST",
                        data   : {
                            ids: ids
                        },
                        success: function () {
                            location.reload(true);
                        }

                    });
                });

            })
        </script>


        <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
        <script src="http://legacy.datatables.net/release-datatables/extras/TableTools/media/js/TableTools.js"></script>

    @endpush
@endsection