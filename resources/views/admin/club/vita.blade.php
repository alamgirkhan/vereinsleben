@extends('layouts.master')

@section('content')
    @include('admin.menu')

    <section class="admin__overview" data-token="{{ csrf_token() }}"
             data-mapping-url="{{ app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('api.club.list') }}">
        <div class="container">


            @if($suggestedClubs->count() > 0)
                <div class="row">
                    <div class="col-md-12"><h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">Vorgeschlagene
                            Vereine
                        </h1></div>
                </div>
                <table class="table table-striped table-bordered">
                    <tr>
                        <th><strong>Vorgeschlagen von</strong></th>
                        <th><strong>Verein</strong></th>
                        <th><strong>Suche</strong></th>
                        <th colspan="2"><strong>Optionen</strong></th>
                    </tr>

                    @foreach($suggestedClubs as $suggestedClub)

                        @if( is_null( $suggestedClub->suggestedFrom))
                            @continue
                        @endif

                        @if(isset($suggestedClub->slug) && isset($suggestedClub->suggestedFrom->username))
                            <tr>

                                <td>
                                    <a href="{{route('user.detail', ['username' => $suggestedClub->suggestedFrom->username])}} "
                                       class="club-tile-wide__link" target='_blank'>
                                        {{ $suggestedClub->suggestedFrom->firstname }}</a></td>
                                <td><a href="{{route('club.detail', ['slug' => $suggestedClub->slug])}}"
                                       class="club-tile-wide__link" target='_blank'>
                                        {{ $suggestedClub->slug }}</a></td>

                                <td>
                                    <div class="input-group">
                                        {!! Form::open(
                                            [
                                            'route' => 'admin.club.vita.remove',
                                            'method' => 'POST',
                                            ]
                                            )!!}
                                        <input type="hidden" id="federal_state" value="">
                                        <input type="hidden" id="club_id" value="">
                                        {!! Form::text('club','', ['class' => 'input club-autocomplete', 'data-clubs-url' => app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('api.club.autocomplete'), 'placeholder' => 'Vereinsname']) !!}
                                        {{ Form::hidden('firstname', $suggestedClub->suggestedFrom->firstname) }}
                                        {{ Form::hidden('lastname', $suggestedClub->suggestedFrom->lastname) }}
                                        {{ Form::hidden('clubSlug', $suggestedClub->slug) }}

                                    </div>
                                </td>
                                <td>
                                    {{--{{ Form::submit('entfernen', ['class' => 'button button--center button--light button--full-width']) }}--}}
                                    {{ Form::button('<i class="fa fa-trash" aria-hidden="true"></i>', ['class' => 'btn btn-outline-light', 'type' => 'submit', 'onclick' => "return confirm('Vereinsvorschlag wirklich ablehnen?')"]) }}
                                    {!! Form::close() !!}
                                </td>
                                    {!! Form::open(
                                            [
                                            'route' => 'admin.club.vita.publish',
                                            'method' => 'POST',
                                            ]
                                    )!!}
                                <td>
                                    {{ Form::hidden('clubSlug', $suggestedClub->slug) }}
                                    {{--{{ Form::submit('akzeptieren', ['class' => 'button button--center button--light button--full-width']) }}--}}
                                    {{ Form::button('<i class="fa fa-check-square" aria-hidden="true"></i>', ['class' => 'btn btn-outline-light', 'type' => 'submit']) }}
                                    {!! Form::close() !!}
                                </td>

                            </tr>

                        @endif
                    @endforeach
                </table>
            @endif
        </div>

    </section>

@endsection