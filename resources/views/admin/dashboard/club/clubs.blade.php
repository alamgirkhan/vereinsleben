@extends('layouts.master')
@section('content')
    @include('admin.menu')
    <section class="admin__overview">
        <div class="container">
            {{--<ul class="admin__menu">--}}
            {{--<li class="admin__menu-item{{ Request::is('admin/dashboard/user/index') ? ' active' : '' }}">--}}
            {{--<a class="admin__menu-link" href="{{ url('admin/dashboard/user/index') }}">Userstatistik</a>--}}
            {{--</li>--}}
            {{--<li class="admin__menu-item{{ Request::is('admin/dashboard/club/clubs') ? ' active' : '' }}">--}}
            {{--<a class="admin__menu-link" href="{{ url('admin/dashboard/club/clubs') }}">Vereinsstatistik</a>--}}
            {{--</li>--}}
            {{--<li class="admin__menu-item{{ Request::is('admin/fakedomains') ? ' active' : '' }}">--}}
            {{--<a class="admin__menu-link" href="{{ url('admin/fakedomains') }}">Fake-Mails</a>--}}
            {{--</li>--}}
            {{--<li class="admin__menu-item{{ Request::is('admin/fakeusers') ? ' active' : '' }}">--}}
            {{--<a class="admin__menu-link" href="{{ url('admin/fakeusers') }}">Fake-Users</a>--}}
            {{--</li>--}}
            {{--<li class="admin__menu-item{{ Request::is('admin/events/all') ? ' active' : '' }}">--}}
            {{--<a class="admin__menu-link" href="{{ url('admin/events/all') }}">Events</a>--}}
            {{--</li>--}}
            {{--<li class="admin__menu-item{{ Request::is('admin/contest') ? ' active' : '' }}">--}}
            {{--<a class="admin__menu-link" href="{{ url('admin/contest') }}">Teambesprechungsraum</a>--}}
            {{--</li>--}}
            {{--</ul>--}}
            <div class="row">
                <div class="col-lg-9">
                    <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">Vereinsstatistik</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    {!! Form::open(['route' => 'admin.dashboard.club', 'method' => 'GET', 'id' => 'search', 'class' => '']) !!}
                    <div class="row">
                        <div class="col-md-6">
                            {!! Form::label('state', 'Bundesland wählen', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::select('state', array('all' => 'Alle Bundesländer')+$federal_states->pluck('name', 'name')->all(), null, ['class' => 'select', 'id' => 'state']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('sport', 'Nach Sportart filtern', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('sport', Request::get('sport'), ['class' => 'input sport-autocomplete', 'data-sports-url' => app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('api.sports'), 'placeholder' => 'SPORTART']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            {!! Form::label('abschnitt', 'Anzuzeigenden Abschnitt wählen', ['class' => 'input-label']) !!}
                            <div class="input-group">

                                {!! Form::select('abschnitt', $abschnitt, null, ['class' => 'select', 'id' => 'abschnitt']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('subabschnitt', 'Anzuzeigenden Subabschnitt wählen', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {{--<select id="subabschnitt" class="select"></select>--}}
                                {!! Form::select('subabschnitt', [], null, [ 'id' => 'subabschnitt', 'class' => 'select']) !!}

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group">
                                {!! Form::submit('Finden', array('class' => 'button button--light button--light-white button--center button--full-width')) !!}
                            </div>
                        </div>
                        <div class="col-md-3">
							<span class="undo"><a href="{{ url('admin/dashboard/club/clubs') }}">Zur&uuml;cksetzen <i
                                            class="fa fa-undo"></i></a></span>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

            @if(count($statistics) > 0)
                <h2 class="hl hl--upper hl-alternate-ft hl--large-alternate">Vereinsstruktur national (bzw.
                    Bundesland)</h2>
                <table id="club-dash" class="admin__table" data-token="{{ csrf_token() }}">
                    <thead>
                    <tr>
                        <th>Block</th>
                        <th style="width:20%;">Anzahl</th>
                        <th style="width:35%;">Übernommen</th>
                        <th style="width:35%;">Einsparten</th>
                        <th style="width:35%;">Mehrsparten</th>
                        <th style="width:35%;">Anteil (national)</th>
                        <th style="width:35%;">Anteil (ungefiltert)</th>
                    </tr>
                    </thead>
                    <tbody>

                    @if($abschnitt_auswahl == 0)
                        <tr>
                            <td style="text-align: left;">Vereine</td>
                            <td id="clubs_all">{{ $statistics['clubs_all'] }}</td>
                            <td id="clubs_accepted">{{ $statistics['clubs_accepted'] }}
                                @if($statistics['clubs_all'] !== 0)
                                    ({{ number_format(round($statistics['clubs_accepted'] / $statistics['clubs_all'] * 100, 2), 2, ',', '.')}}
                                    %)
                                @else
                                    (0 %)
                                @endif
                            </td>
                            <td id="clubs_1_branch">{{ $statistics['clubs_1_branch'] }}
                                @if($statistics['clubs_all'] !== 0)
                                    ({{ number_format(round($statistics['clubs_1_branch'] / $statistics['clubs_all'] * 100, 2), 2, ',', '.')}}
                                    %)
                                @else
                                    (0 %)
                                @endif
                            </td>
                            <td id="clubs_n_branch">{{ $statistics['clubs_n_branch'] }}
                                @if($statistics['clubs_all'] !== 0)
                                    ({{ number_format(round($statistics['clubs_n_branch'] / $statistics['clubs_all'] * 100, 2), 2, ',', '.')}}
                                    %)
                                @else
                                    (0 %)
                                @endif
                            </td>

                            <td id="clubs_all_fed">

                                @if($statistics['all_clubs'] !== 0 && $statistics['federal'] == 1
                                && $statistics['clubs_all_fed'] < $statistics['all_clubs'] && $statistics['clubs_all_fed'] !== 0)
                                    {{ number_format(round($statistics['clubs_all_fed'] / $statistics['all_clubs'] * 100, 2), 2, ',', '.')}}
                                    %
                                @else
                                    -
                                @endif
                            </td>

                            <td id="clubs_all_sp">

                                @if($statistics['all_clubs'] !== 0 && $statistics['sport'] == 1
                                && $statistics['clubs_all_sp'] < $statistics['all_clubs'] && $statistics['clubs_all_sp'] !== 0)
                                    {{ number_format(round($statistics['clubs_all_sp'] / $statistics['all_clubs'] * 100, 2), 2, ',', '.')}}
                                    %
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                    @endif
                    @if($abschnitt_auswahl == 1)
                        {{--<tr>--}}
                        {{--<td colspan="5" style="text-align: left;"><strong>Aktivität</strong></td>--}}
                        {{--</tr>--}}
                        @if($subabschnitt_auswahl == "24h")
                            <tr>
                                <td style="text-align: left;">Letzte 24h</td>
                                <td id="clubs_24_all">{{ $statistics['clubs_24_all'] }}</td>
                                <td id="clubs_24_all">
                                    {{ $statistics['clubs_24_all'] }}
                                    @if($statistics['clubs_24_all'] !== 0)
                                        ({{ number_format(round($statistics['clubs_24_accepted'] / $statistics['clubs_24_all'] * 100, 2), 2, ',', '.')}}
                                        %)
                                    @else
                                        (0 %)
                                    @endif
                                </td>
                                <td id="clubs_sports_24_1">{{ $statistics['clubs_sports_24_1'] }}
                                    @if($statistics['clubs_24_all'] !== 0)
                                        ({{ number_format(round($statistics['clubs_sports_24_1'] / $statistics['clubs_24_all'] * 100, 2), 2, ',', '.')}}
                                        %)
                                    @else
                                        (0 %)
                                    @endif
                                </td>
                                <td id="clubs_sports_24_n">{{ $statistics['clubs_sports_24_n'] }}
                                    @if($statistics['clubs_24_all'] !== 0)
                                        ({{ number_format(round($statistics['clubs_sports_24_n'] / $statistics['clubs_24_all'] * 100, 2), 2, ',', '.')}}
                                        %)
                                    @else
                                        (0 %)
                                    @endif
                                </td>

                                <td id="clubs_24_all_fed">

                                    @if($statistics['all_clubs'] !== 0 && $statistics['federal'] == 1
                                    && $statistics['clubs_24_all_fed'] < $statistics['all_clubs'] && $statistics['clubs_24_all_fed'] !== 0)
                                        {{ number_format(round($statistics['clubs_24_all_fed'] / $statistics['all_clubs'] * 100, 2), 2, ',', '.') }}
                                        %
                                    @else
                                        -
                                    @endif
                                </td>

                                <td id="clubs_24_all_sp">

                                    @if($statistics['all_clubs'] !== 0 && $statistics['sport'] == 1
                                    && $statistics['clubs_24_all_sp'] < $statistics['all_clubs'] && $statistics['clubs_24_all_sp'] !== 0)
                                        {{ number_format(round($statistics['clubs_24_all_sp'] / $statistics['all_clubs'] * 100, 2), 2, ',', '.') }}
                                        %
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                        @endif
                        @if($subabschnitt_auswahl == "7Tage")
                            <tr>
                                <td style="text-align: left;">Letzte 7 Tage</td>
                                <td id="clubs_7_all">{{ $statistics['clubs_7_all'] }}</td>
                                <td id="clubs_7_accepted">
                                    {{ $statistics['clubs_7_accepted'] }}
                                    @if($statistics['clubs_7_all'] !== 0)
                                        ({{ number_format(round($statistics['clubs_7_accepted'] / $statistics['clubs_7_all'] * 100, 2), 2, ',', '.')}}
                                        %)
                                    @else
                                        (0 %)
                                    @endif
                                </td>
                                <td id="clubs_sports_7_1">{{ $statistics['clubs_sports_7_1'] }}
                                    @if($statistics['clubs_7_all'] !== 0)
                                        ({{ number_format(round($statistics['clubs_sports_7_1'] / $statistics['clubs_7_all'] * 100, 2), 2, ',', '.')}}
                                        %)
                                    @else
                                        (0 %)
                                    @endif
                                </td>
                                <td id="clubs_sports_7_n">{{ $statistics['clubs_sports_7_n'] }}
                                    @if($statistics['clubs_7_all'] !== 0)
                                        ({{ number_format(round($statistics['clubs_sports_7_n'] / $statistics['clubs_7_all'] * 100, 2), 2, ',', '.')}}
                                        %)
                                    @else
                                        (0 %)
                                    @endif
                                </td>

                                <td id="clubs_7_all_fed">

                                    @if($statistics['all_clubs'] !== 0 && $statistics['federal'] == 1
                                    && $statistics['clubs_7_all_fed'] < $statistics['all_clubs'] && $statistics['clubs_7_all_fed'] !== 0)
                                        {{ number_format(round($statistics['clubs_7_all_fed'] / $statistics['all_clubs'] * 100, 2), 2, ',', '.') }}
                                        %
                                    @else
                                        -
                                    @endif
                                </td>

                                <td id="clubs_7_all_sp">

                                    @if($statistics['all_clubs'] !== 0 && $statistics['sport'] == 1
                                    && $statistics['clubs_7_all_sp'] < $statistics['all_clubs'] && $statistics['clubs_7_all_sp'] !== 0)
                                        {{ number_format(round($statistics['clubs_7_all_sp'] / $statistics['all_clubs'] * 100, 2), 2, ',', '.') }}
                                        %
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>

                        @endif
                        @if($subabschnitt_auswahl == "30Tage")

                            <tr>
                                <td style="text-align: left;">Letzte 30 Tage</td>
                                <td id="clubs_30_all">{{ $statistics['clubs_30_all'] }}</td>
                                <td id="clubs_30_accepted">
                                    {{ $statistics['clubs_30_accepted'] }}
                                    @if($statistics['clubs_30_all'] !== 0)
                                        ({{ number_format(round($statistics['clubs_30_accepted'] / $statistics['clubs_30_all'] * 100, 2), 2, ',', '.')}}
                                        %)
                                    @else
                                        (0 %)
                                    @endif
                                </td>
                                <td id="clubs_sports_30_1">{{ $statistics['clubs_sports_30_1'] }}
                                    @if($statistics['clubs_30_all'] !== 0)
                                        ({{ number_format(round($statistics['clubs_sports_30_1'] / $statistics['clubs_30_all'] * 100, 2), 2, ',', '.')}}
                                        %)
                                    @else
                                        (0 %)
                                    @endif
                                </td>
                                <td id="clubs_sports_30_n">{{ $statistics['clubs_sports_30_n'] }}
                                    @if($statistics['clubs_30_all'] !== 0)
                                        ({{ number_format(round($statistics['clubs_sports_30_n'] / $statistics['clubs_30_all'] * 100, 2), 2, ',', '.')}}
                                        %)
                                    @else
                                        (0 %)
                                    @endif
                                </td>

                                <td id="clubs_30_all_fed">

                                    @if($statistics['all_clubs'] !== 0 && $statistics['federal'] == 1
                                    && $statistics['clubs_30_all_fed'] < $statistics['all_clubs'] && $statistics['clubs_30_all_fed'] !== 0)
                                        {{ number_format(round($statistics['clubs_30_all_fed'] / $statistics['all_clubs'] * 100, 2), 2, ',', '.') }}
                                        %
                                    @else
                                        -
                                    @endif
                                </td>

                                <td id="clubs_30_all_sp">

                                    @if($statistics['all_clubs'] !== 0 && $statistics['sport'] == 1
                                    && $statistics['clubs_30_all_sp'] < $statistics['all_clubs'] && $statistics['clubs_30_all_sp'] !== 0)
                                        {{ number_format(round($statistics['clubs_30_all_sp'] / $statistics['all_clubs'] * 100, 2), 2, ',', '.') }}
                                        %
                                    @else
                                        -
                                    @endif
                                </td>

                            </tr>
                        @endif
                        @if($subabschnitt_auswahl == "90Tage")
                            <tr>
                                <td style="text-align: left;">Letzte 90 Tage</td>
                                <td id="clubs_90_all">{{ $statistics['clubs_90_all'] }}</td>
                                <td id="clubs_90_accepted">
                                    {{ $statistics['clubs_90_accepted'] }}
                                    @if($statistics['clubs_90_all'] !== 0)
                                        ({{ number_format(round($statistics['clubs_90_accepted'] / $statistics['clubs_90_all'] * 100, 2), 2, ',', '.')}}
                                        %)
                                    @else
                                        (0 %)
                                    @endif
                                </td>
                                <td id="clubs_sports_90_1">{{ $statistics['clubs_sports_90_1'] }}
                                    @if($statistics['clubs_90_all'] !== 0)
                                        ({{ number_format(round($statistics['clubs_sports_90_1'] / $statistics['clubs_90_all'] * 100, 2), 2, ',', '.')}}
                                        %)
                                    @else
                                        (0 %)
                                    @endif
                                </td>
                                <td id="clubs_sports_90_n">{{ $statistics['clubs_sports_90_n'] }}
                                    @if($statistics['clubs_90_all'] !== 0)
                                        ({{ number_format(round($statistics['clubs_sports_90_n'] / $statistics['clubs_90_all'] * 100, 2), 2, ',', '.')}}
                                        %)
                                    @else
                                        (0 %)
                                    @endif
                                </td>

                                <td id="clubs_90_all_fed">

                                    @if($statistics['all_clubs'] !== 0 && $statistics['federal'] == 1
                                    && $statistics['clubs_90_all_fed'] < $statistics['all_clubs'] && $statistics['clubs_90_all_fed'] !== 0)
                                        {{ number_format(round($statistics['clubs_90_all_fed'] / $statistics['all_clubs'] * 100, 2), 2, ',', '.') }}
                                        %
                                    @else
                                        -
                                    @endif
                                </td>

                                <td id="clubs_90_all_sp">

                                    @if($statistics['all_clubs'] !== 0 && $statistics['sport'] == 1
                                    && $statistics['clubs_90_all_sp'] < $statistics['all_clubs'] && $statistics['clubs_90_all_sp'] !== 0)
                                        {{ number_format(round($statistics['clubs_90_all_sp'] / $statistics['all_clubs'] * 100, 2), 2, ',', '.') }}
                                        %
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                        @endif

                    @endif

                    @if($abschnitt_auswahl == 2)
                        {{--<tr>--}}
                        {{--<td colspan="5" style="text-align: left;"><strong>Intensität</strong></td>--}}
                        {{--</tr>--}}

                        @if($subabschnitt_auswahl == "Logo")
                            <tr>
                                <td style="text-align: left;">Logo</td>
                                <td id="clubs_logo_all">{{ $statistics['clubs_logo_all'] }}</td>
                                <td id="clubs_logo_accepted">
                                    {{ $statistics['clubs_logo_accepted'] }}
                                    @if($statistics['clubs_logo_all'] !== 0)
                                        ({{ number_format(round($statistics['clubs_logo_accepted'] / $statistics['clubs_logo_all'] * 100, 2), 2, ',', '.')}}
                                        %)
                                    @else
                                        (0 %)
                                    @endif
                                </td>
                                <td id="clubsport_logo_1">{{ $statistics['clubsport_logo_1'] }}
                                    @if($statistics['clubs_logo_all'] !== 0)
                                        ({{ number_format(round($statistics['clubsport_logo_1'] / $statistics['clubs_logo_all'] * 100, 2), 2, ',', '.')}}
                                        %)
                                    @else
                                        (0 %)
                                    @endif
                                </td>
                                <td id="clubsport_logo_n">{{ $statistics['clubsport_logo_n'] }}
                                    @if($statistics['clubs_logo_all'] !== 0)
                                        ({{ number_format(round($statistics['clubsport_logo_n'] / $statistics['clubs_logo_all'] * 100, 2), 2, ',', '.')}}
                                        %)
                                    @else
                                        (0 %)
                                    @endif
                                </td>

                                <td id="clubs_logo_all_fed">

                                    @if($statistics['all_clubs'] !== 0 && $statistics['federal'] == 1
                                    && $statistics['clubs_logo_all_fed'] < $statistics['all_clubs'] && $statistics['clubs_logo_all_fed'] !== 0)
                                        {{ number_format(round($statistics['clubs_logo_all_fed'] / $statistics['all_clubs'] * 100, 2), 2, ',', '.') }}
                                        %
                                    @else
                                        -
                                    @endif
                                </td>

                                <td id="clubs_logo_all_sp">

                                    @if($statistics['all_clubs'] !== 0 && $statistics['sport'] == 1
                                    && $statistics['clubs_logo_all_sp'] < $statistics['all_clubs'] && $statistics['clubs_logo_all_sp'] !== 0)
                                        {{ number_format(round($statistics['clubs_logo_all_sp'] / $statistics['all_clubs'] * 100, 2), 2, ',', '.') }}
                                        %
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                        @endif
                        @if($subabschnitt_auswahl == "Header")
                            <tr>
                                <td style="text-align: left;">Header</td>
                                <td id="clubs_header_all">{{ $statistics['clubs_header_all'] }}</td>
                                <td id="clubs_header_accepted">
                                    {{ $statistics['clubs_header_accepted'] }}
                                    @if($statistics['clubs_header_all'] !== 0)
                                        ({{ number_format(round($statistics['clubs_header_accepted'] / $statistics['clubs_header_all'] * 100, 2), 2, ',', '.')}}
                                        %)
                                    @else
                                        (0 %)
                                    @endif
                                </td>
                                <td id="clubsport_header_1">{{ $statistics['clubsport_header_1'] }}
                                    @if($statistics['clubs_header_all'] !== 0)
                                        ({{ number_format(round($statistics['clubsport_header_1'] / $statistics['clubs_header_all'] * 100, 2), 2, ',', '.')}}
                                        %)
                                    @else
                                        (0 %)
                                    @endif
                                </td>
                                <td id="clubsport_header_n">{{ $statistics['clubsport_header_n'] }}
                                    @if($statistics['clubs_header_all'] !== 0)
                                        ({{ number_format(round($statistics['clubsport_header_n'] / $statistics['clubs_header_all'] * 100, 2), 2, ',', '.')}}
                                        %)
                                    @else
                                        (0 %)
                                    @endif
                                </td>

                                <td id="clubs_header_all_fed">

                                    @if($statistics['all_clubs'] !== 0 && $statistics['federal'] == 1
                                    && $statistics['clubs_header_all_fed'] < $statistics['all_clubs'] && $statistics['clubs_header_all_fed'] !== 0)
                                        {{ number_format(round($statistics['clubs_header_all_fed'] / $statistics['all_clubs'] * 100, 2), 2, ',', '.') }}
                                        %
                                    @else
                                        -
                                    @endif
                                </td>

                                <td id="clubs_header_all_sp">

                                    @if($statistics['all_clubs'] !== 0 && $statistics['sport'] == 1
                                    && $statistics['clubs_header_all_sp'] < $statistics['all_clubs'] && $statistics['clubs_header_all_sp'] !== 0)
                                        {{ number_format(round($statistics['clubs_header_all_sp'] / $statistics['all_clubs'] * 100, 2), 2, ',', '.') }}
                                        %
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                        @endif
                        @if($subabschnitt_auswahl == "Infos")
                            <tr>
                                <td style="text-align: left;">Infos</td>
                                <td id="clubs_infos_all">{{ $statistics['clubs_infos_all'] }}</td>
                                <td id="clubs_infos_accepted">
                                    {{ $statistics['clubs_infos_accepted'] }}
                                    @if($statistics['clubs_infos_all'] !== 0)
                                        ({{ number_format(round($statistics['clubs_infos_accepted'] / $statistics['clubs_infos_all'] * 100, 2), 2, ',', '.')}}
                                        %)
                                    @else
                                        (0 %)
                                    @endif
                                </td>
                                <td id="clubsport_infos_1">{{ $statistics['clubsport_infos_1'] }}
                                    @if($statistics['clubs_infos_all'] !== 0)
                                        ({{ number_format(round($statistics['clubsport_infos_1'] / $statistics['clubs_infos_all'] * 100, 2), 2, ',', '.')}}
                                        %)
                                    @else
                                        (0 %)
                                    @endif
                                </td>
                                <td id="clubsport_infos_n">{{ $statistics['clubsport_infos_n'] }}
                                    @if($statistics['clubs_infos_all'] !== 0)
                                        ({{ number_format(round($statistics['clubsport_infos_n'] / $statistics['clubs_infos_all'] * 100, 2), 2, ',', '.')}}
                                        %)
                                    @else
                                        (0 %)
                                    @endif
                                </td>

                                <td id="clubs_infos_all_fed">

                                    @if($statistics['all_clubs'] !== 0 && $statistics['federal'] == 1
                                    && $statistics['clubs_infos_all_fed'] < $statistics['all_clubs'] && $statistics['clubs_infos_all_fed'] !== 0)
                                        {{ number_format(round($statistics['clubs_infos_all_fed'] / $statistics['all_clubs'] * 100, 2), 2, ',', '.') }}
                                        %
                                    @else
                                        -
                                    @endif
                                </td>

                                <td id="clubs_infos_all_sp">

                                    @if($statistics['all_clubs'] !== 0 && $statistics['sport'] == 1
                                    && $statistics['clubs_infos_all_sp'] < $statistics['all_clubs'] && $statistics['clubs_infos_all_sp'] !== 0)
                                        {{ number_format(round($statistics['clubs_infos_all_sp'] / $statistics['all_clubs'] * 100, 2), 2, ',', '.') }}
                                        %
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                        @endif
                        @if($subabschnitt_auswahl == "Teams")
                            <tr>
                                <td style="text-align: left;">Teams</td>
                                <td id="clubs_teams_all">{{ $statistics['clubs_teams_all'] }}</td>
                                <td id="clubs_teams_accepted">
                                    {{ $statistics['clubs_teams_accepted'] }}
                                    @if($statistics['clubs_teams_all'] !== 0)
                                        ({{ number_format(round($statistics['clubs_teams_accepted'] / $statistics['clubs_teams_all'] * 100, 2), 2, ',', '.')}}
                                        %)
                                    @else
                                        (0 %)
                                    @endif
                                </td>
                                <td id="clubsport_teams_1">{{ $statistics['clubsport_teams_1'] }}
                                    @if($statistics['clubs_teams_all'] !== 0)
                                        ({{ number_format(round($statistics['clubsport_teams_1'] / $statistics['clubs_teams_all'] * 100, 2), 2, ',', '.')}}
                                        %)
                                    @else
                                        (0 %)
                                    @endif
                                </td>
                                <td id="clubsport_teams_n">{{ $statistics['clubsport_teams_n'] }}
                                    @if($statistics['clubs_teams_all'] !== 0)
                                        ({{ number_format(round($statistics['clubsport_teams_n'] / $statistics['clubs_teams_all'] * 100, 2), 2, ',', '.')}}
                                        %)
                                    @else
                                        (0 %)
                                    @endif
                                </td>

                                <td id="clubs_teams_all_fed">

                                    @if($statistics['all_clubs'] !== 0 && $statistics['federal'] == 1
                                    && $statistics['clubs_teams_all_fed'] < $statistics['all_clubs'] && $statistics['clubs_teams_all_fed'] !== 0)
                                        {{ number_format(round($statistics['clubs_teams_all_fed'] / $statistics['all_clubs'] * 100, 2), 2, ',', '.') }}
                                        %
                                    @else
                                        -
                                    @endif
                                </td>

                                <td id="clubs_teams_all_sp">

                                    @if($statistics['all_clubs'] !== 0 && $statistics['sport'] == 1
                                    && $statistics['clubs_teams_all_sp'] < $statistics['all_clubs'] && $statistics['clubs_teams_all_sp'] !== 0)
                                        {{ number_format(round($statistics['clubs_teams_all_sp'] / $statistics['all_clubs'] * 100, 2), 2, ',', '.') }}
                                        %
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                        @endif
                        @if($subabschnitt_auswahl == "Weblinks")
                            <tr>
                                <td style="text-align: left;">Weblinks</td>
                                <td id="clubs_web_all">{{ $statistics['clubs_web_all'] }}</td>
                                <td id="clubs_web_accepted">
                                    {{ $statistics['clubs_web_accepted'] }}
                                    @if($statistics['clubs_web_all'] !== 0)
                                        ({{ number_format(round($statistics['clubs_web_accepted'] / $statistics['clubs_web_all'] * 100, 2), 2, ',', '.')}}
                                        %)
                                    @else
                                        (0 %)
                                    @endif
                                </td>
                                <td id="clubsport_web_1">{{ $statistics['clubsport_web_1'] }}
                                    @if($statistics['clubs_web_all'] !== 0)
                                        ({{ number_format(round($statistics['clubsport_web_1'] / $statistics['clubs_web_all'] * 100, 2), 2, ',', '.')}}
                                        %)
                                    @else
                                        (0 %)
                                    @endif
                                </td>
                                <td id="clubsport_web_n">{{ $statistics['clubsport_web_n'] }}
                                    @if($statistics['clubs_web_all'] !== 0)
                                        ({{ number_format(round($statistics['clubsport_web_n'] / $statistics['clubs_web_all'] * 100, 2), 2, ',', '.')}}
                                        %)
                                    @else
                                        (0 %)
                                    @endif
                                </td>

                                <td id="clubs_web_all_fed">

                                    @if($statistics['all_clubs'] !== 0 && $statistics['federal'] == 1
                                    && $statistics['clubs_web_all_fed'] < $statistics['all_clubs'] && $statistics['clubs_web_all_fed'] !== 0)
                                        {{ number_format(round($statistics['clubs_web_all_fed'] / $statistics['all_clubs'] * 100, 2), 2, ',', '.') }}
                                        %
                                    @else
                                        -
                                    @endif
                                </td>

                                <td id="clubs_web_all_sp">

                                    @if($statistics['all_clubs'] !== 0 && $statistics['sport'] == 1
                                    && $statistics['clubs_web_all_sp'] < $statistics['all_clubs'] && $statistics['clubs_web_all_sp'] !== 0)
                                        {{ number_format(round($statistics['clubs_web_all_sp'] / $statistics['all_clubs'] * 100, 2), 2, ',', '.') }}
                                        %
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                        @endif
                        @if($subabschnitt_auswahl == "Erfolge")
                            <tr>
                                <td style="text-align: left;">Erfolge</td>
                                <td id="clubs_success_all">{{ $statistics['clubs_success_all'] }}</td>
                                <td id="clubs_success_accepted">
                                    {{ $statistics['clubs_success_accepted'] }}
                                    @if($statistics['clubs_success_all'] !== 0)
                                        ({{ number_format(round($statistics['clubs_success_accepted'] / $statistics['clubs_success_all'] * 100, 2), 2, ',', '.')}}
                                        %)
                                    @else
                                        (0 %)
                                    @endif
                                </td>
                                <td id="clubsport_success_1">{{ $statistics['clubsport_success_1'] }}
                                    @if($statistics['clubs_success_all'] !== 0)
                                        ({{ number_format(round($statistics['clubsport_success_1'] / $statistics['clubs_success_all'] * 100, 2), 2, ',', '.')}}
                                        %)
                                    @else
                                        (0 %)
                                    @endif
                                </td>
                                <td id="clubsport_success_n">{{ $statistics['clubsport_success_n'] }}
                                    @if($statistics['clubs_success_all'] !== 0)
                                        ({{ number_format(round($statistics['clubsport_success_n'] / $statistics['clubs_success_all'] * 100, 2), 2, ',', '.')}}
                                        %)
                                    @else
                                        (0 %)
                                    @endif
                                </td>

                                <td id="clubs_success_all_fed">

                                    @if($statistics['all_clubs'] !== 0 && $statistics['federal'] == 1
                                    && $statistics['clubs_success_all_fed'] < $statistics['all_clubs'] && $statistics['clubs_success_all_fed'] !== 0)
                                        {{ number_format(round($statistics['clubs_success_all_fed'] / $statistics['all_clubs'] * 100, 2), 2, ',', '.') }}
                                        %
                                    @else
                                        -
                                    @endif
                                </td>

                                <td id="clubs_success_all_sp">

                                    @if($statistics['all_clubs'] !== 0 && $statistics['sport'] == 1
                                    && $statistics['clubs_success_all_sp'] < $statistics['all_clubs'] && $statistics['clubs_success_all_sp'] !== 0)
                                        {{ number_format(round($statistics['clubs_success_all_sp'] / $statistics['all_clubs'] * 100, 2), 2, ',', '.') }}
                                        %
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                        @endif
                        @if($subabschnitt_auswahl == "Posts")
                            <tr>
                                <td style="text-align: left;">Posts</td>
                                <td id="clubs_posts_all">{{ $statistics['clubs_posts_all'] }}</td>
                                <td id="clubs_posts_accepted">
                                    {{ $statistics['clubs_posts_accepted'] }}
                                    @if($statistics['clubs_posts_all'] !== 0)
                                        ({{ number_format(round($statistics['clubs_posts_accepted'] / $statistics['clubs_posts_all'] * 100, 2), 2, ',', '.')}}
                                        %)
                                    @else
                                        (0 %)
                                    @endif
                                </td>
                                <td id="clubsport_posts_1">{{ $statistics['clubsport_posts_1'] }}
                                    @if($statistics['clubs_posts_all'] !== 0)
                                        ({{ number_format(round($statistics['clubsport_posts_1'] / $statistics['clubs_posts_all'] * 100, 2), 2, ',', '.')}}
                                        %)
                                    @else
                                        (0 %)
                                    @endif
                                </td>
                                <td id="clubsport_posts_n">{{ $statistics['clubsport_posts_n'] }}
                                    @if($statistics['clubs_posts_all'] !== 0)
                                        ({{ number_format(round($statistics['clubsport_posts_n'] / $statistics['clubs_posts_all'] * 100, 2), 2, ',', '.')}}
                                        %)
                                    @else
                                        (0 %)
                                    @endif
                                </td>

                                <td id="clubs_posts_all_fed">

                                    @if($statistics['all_clubs'] !== 0 && $statistics['federal'] == 1
                                    && $statistics['clubs_posts_all_fed'] < $statistics['all_clubs'] && $statistics['clubs_posts_all_fed'] !== 0)
                                        {{ number_format(round($statistics['clubs_posts_all_fed'] / $statistics['all_clubs'] * 100, 2), 2, ',', '.') }}
                                        %
                                    @else
                                        -
                                    @endif
                                </td>

                                <td id="clubs_posts_all_sp">

                                    @if($statistics['all_clubs'] !== 0 && $statistics['sport'] == 1
                                    && $statistics['clubs_posts_all_sp'] < $statistics['all_clubs'] && $statistics['clubs_posts_all_sp'] !== 0)
                                        {{ number_format(round($statistics['clubs_posts_all_sp'] / $statistics['all_clubs'] * 100, 2), 2, ',', '.') }}
                                        %
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                        @endif
                    @endif

                    </tbody>
                </table>
            @endif
        </div>
    </section>
    @push('scripts')
        <script>
            $(document).ready(function () {
                $('#club-dash').DataTable({
                    dom         : 'Bfrtip',
                    buttons     : [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5'
                    ],
                    "pageLength": 50,
                    language    : {
                        url: '/js/vendor/i18n/dataTables.german.json'
                    }
                });
            })
        </script>

        <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    @endpush
@endsection

