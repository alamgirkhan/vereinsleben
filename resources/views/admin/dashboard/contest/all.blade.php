@extends('layouts.master')

@section('content')
    @include('admin.menu')
    <section class="admin__overview">
        <div class="container">
            {{--<ul class="admin__menu">--}}
            {{--<li class="admin__menu-item{{ Request::is('admin/dashboard/user/index') ? ' active' : '' }}">--}}
            {{--<a class="admin__menu-link" href="{{ url('admin/dashboard/user/index') }}">Userstatistik</a>--}}
            {{--</li>--}}
            {{--<li class="admin__menu-item{{ Request::is('admin/dashboard/club/clubs') ? ' active' : '' }}">--}}
            {{--<a class="admin__menu-link" href="{{ url('admin/dashboard/club/clubs') }}">Vereinsstatistik</a>--}}
            {{--</li>--}}
            {{--<li class="admin__menu-item{{ Request::is('admin/fakedomains') ? ' active' : '' }}">--}}
            {{--<a class="admin__menu-link" href="{{ url('admin/fakedomains') }}">Fake-Mails</a>--}}
            {{--</li>--}}
            {{--<li class="admin__menu-item{{ Request::is('admin/fakeusers') ? ' active' : '' }}">--}}
            {{--<a class="admin__menu-link" href="{{ url('admin/fakeusers') }}">Fake-Users</a>--}}
            {{--</li>--}}
            {{--<li class="admin__menu-item{{ Request::is('admin/events/all') ? ' active' : '' }}">--}}
            {{--<a class="admin__menu-link" href="{{ url('admin/events/all') }}">Events</a>--}}
            {{--</li>--}}
            {{--<li class="admin__menu-item{{ Request::is('admin/contest') ? ' active' : '' }}">--}}
            {{--<a class="admin__menu-link" href="{{ url('admin/contest') }}">Teambesprechungsraum</a>--}}
            {{--</li>--}}
            {{--</ul>--}}
            <div class="row admin__section">
                <div class="col-xs-12">
                    <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">Teambesprechungsraum Gewinnspiel</h1>
                </div>
            </div>
            <table id="admin-table-contest" class="admin__table" data-token="{{ csrf_token() }}">
                <thead>
                <tr>
                    <th style="width:5%">ID&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:35%">Verein&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:10%">User&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:15%">vorgeschlagen am&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:15%">Optionen</th>
                </tr>
                </thead>
                <tbody>
                @foreach($contest as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>
                            <a href="{{ route('club.detail', $item->clubslug) }}"
                               target="_blank">{{ $item->club_name }}</a>
                        </td>
                        <td>
                            <a href="{{ route('user.detail', $item->username) }}"
                               target="_blank">{{ $item->user_firstname }} {{ $item->user_lastname }}</a>
                        </td>
                        <td>
                            {{ $item->created_at }}
                        </td>
                        <td>
                            <div class="admin__table-option">
                                <a onclick="return confirm('Möchtest du diese Aufzeichnung wirklich löschen?')"
                                   href="{{ route('admin.contest.destroy', $item->id) }}">
                                    <span class="fa fa-trash"></span>
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </section>
    @push('scripts')
        <script>
            $(document).ready(function () {
                $('#admin-table-contest').DataTable({
                    dom         : 'Blfrtip',
                    "lengthMenu": [50, 100, 250],
                    buttons     : [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5'
                    ],
                    fixedHeader: true,
                    stateSave: true,
                    StateDuration: -1,
                    language: {
                        url: '/js/vendor/i18n/dataTables.german.json'
                    }
                }).on('page.dt', function() {
                    $('html, body').animate({
                        scrollTop: $(".dataTables_wrapper").offset().top
                    }, 'slow');

                    $('thead tr th:first-child').focus().blur();
                });

                // var info = table.page.info();
                //
                // console.log(info.page);
            })
        </script>

        <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    @endpush
@endsection