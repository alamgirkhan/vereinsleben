@extends('layouts.master')
@section('content')
    @include('admin.menu')
    <section class="admin__overview">
        <div class="container">
            <ul class="admin__menu">
                <li class="admin__menu-item{{ Request::is('admin/dashboard/user/index') ? ' active' : '' }}">
                    <a class="admin__menu-link" href="{{ url('admin/dashboard/user/index') }}">Userstatistik</a>
                </li>
                <li class="admin__menu-item{{ Request::is('admin/dashboard/club/clubs') ? ' active' : '' }}">
                    <a class="admin__menu-link" href="{{ url('admin/dashboard/club/clubs') }}">Vereinsstatistik</a>
                </li>
                <li class="admin__menu-item{{ Request::is('admin/fakedomains') ? ' active' : '' }}">
                    <a class="admin__menu-link" href="{{ url('admin/fakedomains') }}">Fake-Mails</a>
                </li>
                <li class="admin__menu-item{{ Request::is('admin/fakeusers') ? ' active' : '' }}">
                    <a class="admin__menu-link" href="{{ url('admin/fakeusers') }}">Fake-Users</a>
                </li>
                <li class="admin__menu-item{{ Request::is('admin/events/all') ? ' active' : '' }}">
                    <a class="admin__menu-link" href="{{ url('admin/events/all') }}">Events</a>
                </li>
                <li class="admin__menu-item{{ Request::is('admin/contest') ? ' active' : '' }}">
                    <a class="admin__menu-link" href="{{ url('admin/contest') }}">Teambesprechungsraum</a>
                </li>
            </ul>
        </div>
    </section>
@endsection


