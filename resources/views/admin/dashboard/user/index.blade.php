@extends('layouts.master')
@section('content')
    @include('admin.menu')
    <section class="admin__overview">
        <div class="container">
            {{--<ul class="admin__menu">--}}
            {{--<li class="admin__menu-item{{ Request::is('admin/dashboard/user/index') ? ' active' : '' }}">--}}
            {{--<a class="admin__menu-link" href="{{ url('admin/dashboard/user/index') }}">Userstatistik</a>--}}
            {{--</li>--}}
            {{--<li class="admin__menu-item{{ Request::is('admin/dashboard/club/clubs') ? ' active' : '' }}">--}}
            {{--<a class="admin__menu-link" href="{{ url('admin/dashboard/club/clubs') }}">Vereinsstatistik</a>--}}
            {{--</li>--}}
            {{--<li class="admin__menu-item{{ Request::is('admin/fakedomains') ? ' active' : '' }}">--}}
            {{--<a class="admin__menu-link" href="{{ url('admin/fakedomains') }}">Fake-Mails</a>--}}
            {{--</li>--}}
            {{--<li class="admin__menu-item{{ Request::is('admin/fakeusers') ? ' active' : '' }}">--}}
            {{--<a class="admin__menu-link" href="{{ url('admin/fakeusers') }}">Fake-Users</a>--}}
            {{--</li>--}}
            {{--<li class="admin__menu-item{{ Request::is('admin/events/all') ? ' active' : '' }}">--}}
            {{--<a class="admin__menu-link" href="{{ url('admin/events/all') }}">Events</a>--}}
            {{--</li>--}}
            {{--<li class="admin__menu-item{{ Request::is('admin/contest') ? ' active' : '' }}">--}}
            {{--<a class="admin__menu-link" href="{{ url('admin/contest') }}">Teambesprechungsraum</a>--}}
            {{--</li>--}}
            {{--</ul>--}}
            <div class="row">
                <div class="col-lg-9">
                    <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">Nutzerstatistik</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    {!! Form::open(['route' => 'admin.dashboard.user', 'method' => 'GET', 'id' => 'search', 'class' => '']) !!}
                    <div class="row">
                        <div class="col-md-6">
                            {!! Form::label('state', 'Bundesland wählen', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::select('state', array('all' => 'Alle Bundesländer')+$federal_states->pluck('name', 'name')->all(), null, ['class' => 'select', 'id' => 'state']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('sport', 'Nach Sportart filtern', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('sport', Request::get('sport'), ['class' => 'input sport-autocomplete', 'data-sports-url' => app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('api.sports'), 'placeholder' => 'SPORTART']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            {!! Form::label('abschnitt', 'Anzuzeigenden Abschnitt wählen', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::select('abschnitt', $abschnitt, null, ['class' => 'select', 'default' => 'User']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group">
                                {!! Form::submit('Finden', array('class' => 'button button--light button--light-white button--center button--full-width')) !!}
                            </div>
                        </div>
                        <div class="col-md-3">
							<span class="undo"><a href="{{ url('admin/dashboard/users') }}">Zur&uuml;cksetzen <i
                                            class="fa fa-undo"></i></a></span>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

            @if(count($statistics) > 0)
                <h2 class="hl hl--upper hl-alternate-ft hl--large-alternate">Userstruktur national (bzw.
                    Bundesland)</h2>
                <table id="user-dash" class="admin__table" data-token="{{ csrf_token() }}">
                    <thead>
                    <tr>
                        <th>Block</th>
                        <th style="width:20%;">Anzahl</th>
                        <th style="width:35%;">Bestätigt</th>
                        <th style="width:35%;">Männlich</th>
                        <th style="width:35%;">Weiblich</th>
                        <th style="width:35%;">Anteil (national)</th>
                        <th style="width:35%;">Anteil (ungefiltert)</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($abschnitt_auswahl == 0)
                    <tr>
                        <td style="text-align: left;">Registrierte User</td>
                        <td>{{ $statistics['user_registered'] }}</td>
                        <td>{{ $statistics['user_reg_verified'] }}
                            @if($statistics['user_registered'] !== 0)
                                ({{ number_format(round($statistics['user_reg_verified'] / $statistics['user_registered'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                    (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['user_reg_male'] }}
                            @if($statistics['user_registered'] !== 0)
                                ({{ number_format(round($statistics['user_reg_male'] / $statistics['user_registered'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['user_reg_female'] }}
                            @if($statistics['user_registered'] !== 0)
                                ({{ number_format(round($statistics['user_reg_female'] / $statistics['user_registered'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['federal'] == 1 && $statistics['user_registered_fed'] < $statistics['all_users']
                            && $statistics['user_registered_fed'] !== 0)
                                {{ number_format(round($statistics['user_registered_fed'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['sport'] == 1 && $statistics['user_registered_sp'] < $statistics['all_users']
                            && $statistics['user_registered_sp'] !== 0)
                                {{ number_format(round($statistics['user_registered_sp'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                    @endif
                    @if($abschnitt_auswahl == 1)
                        {{--<tr>--}}
                        {{--<td colspan="5" style="text-align: left;"><strong>Neuregistrierungen</strong></td>--}}
                        {{--</tr>--}}
                    <tr>
                        <td style="text-align: left;">Letzte 24 h</td>
                        <td>{{ $statistics['user_new_24_all'] }}
                            @if($statistics['user_registered'] !== 0)
                                ({{ number_format(round($statistics['user_new_24_all'] / $statistics['user_registered'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['user_new_24_verified'] }}
                            @if($statistics['user_new_24_all'] !== 0)
                                ({{ number_format(round($statistics['user_new_24_verified'] / $statistics['user_new_24_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['user_new_24_male'] }}
                            @if($statistics['user_new_24_all'] !== 0)
                                ({{ number_format(round($statistics['user_new_24_male'] / $statistics['user_new_24_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['user_new_24_female'] }}
                            @if($statistics['user_new_24_all'] !== 0)
                                ({{ number_format(round($statistics['user_new_24_female'] / $statistics['user_new_24_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['federal'] == 1 && $statistics['user_new_24_all_fed'] < $statistics['all_users']
                            && $statistics['user_new_24_all_fed'] !== 0)
                                {{ number_format(round($statistics['user_new_24_all_fed'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['sport'] == 1 && $statistics['user_new_24_all_sp'] < $statistics['all_users']
                            && $statistics['user_new_24_all_sp'] !== 0)
                                {{ number_format(round($statistics['user_new_24_all_sp'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Letzte 7 Tage</td>
                        <td>{{ $statistics['user_new_7d_all'] }}
                            @if($statistics['user_registered'] !== 0)
                                ({{ number_format(round($statistics['user_new_7d_all'] / $statistics['user_registered'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['user_new_7d_verified'] }}
                            @if($statistics['user_new_7d_all'] !== 0)
                                ({{ number_format(round($statistics['user_new_7d_verified'] / $statistics['user_new_7d_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['user_new_7d_male'] }}
                            @if( $statistics['user_new_7d_all'] !== 0)
                                ({{ number_format(round($statistics['user_new_7d_male'] / $statistics['user_new_7d_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['user_new_7d_female'] }}
                            @if( $statistics['user_new_7d_all'] !== 0)
                                ({{ number_format(round($statistics['user_new_7d_female'] / $statistics['user_new_7d_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['federal'] == 1 && $statistics['user_new_7d_all_fed'] < $statistics['all_users']
                            && $statistics['user_new_7d_all_fed'] !== 0)
                                {{ number_format(round($statistics['user_new_7d_all_fed'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['sport'] == 1 && $statistics['user_new_7d_all_sp'] < $statistics['all_users']
                            && $statistics['user_new_7d_all_sp'] !== 0)
                                {{ number_format(round($statistics['user_new_7d_all_sp'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Letzte 30 Tage</td>
                        <td>{{ $statistics['user_new_30d_all'] }}
                            @if($statistics['user_registered'] !== 0)
                                ({{ number_format(round($statistics['user_new_30d_all'] / $statistics['user_registered'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['user_new_30d_verified'] }}
                            @if($statistics['user_new_30d_all'] !== 0)
                                ({{ number_format(round($statistics['user_new_30d_verified'] / $statistics['user_new_30d_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['user_new_30d_male'] }}
                            @if($statistics['user_new_30d_all'] !== 0)
                                ({{ number_format(round($statistics['user_new_30d_male'] / $statistics['user_new_30d_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['user_new_30d_female'] }}
                            @if($statistics['user_new_30d_all'] !== 0)
                                ({{ number_format(round($statistics['user_new_30d_female'] / $statistics['user_new_30d_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['federal'] == 1 && $statistics['user_new_30d_all_fed'] < $statistics['all_users']
                            && $statistics['user_new_30d_all_fed'] !== 0)
                                {{ number_format(round($statistics['user_new_30d_all_fed'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['sport'] == 1 && $statistics['user_new_30d_all_sp'] < $statistics['all_users']
                            && $statistics['user_new_30d_all_sp'] !== 0)
                                {{ number_format(round($statistics['user_new_30d_all_sp'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Letzte 90 Tage</td>
                        <td>{{ $statistics['user_new_90d_all'] }}
                            @if($statistics['user_registered'] !== 0)
                                ({{ number_format(round($statistics['user_new_90d_all'] / $statistics['user_registered'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['user_new_90d_verified'] }}
                            @if($statistics['user_new_90d_all'] !== 0)
                                ({{ number_format(round($statistics['user_new_90d_verified'] / $statistics['user_new_90d_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['user_new_90d_male'] }}
                            @if($statistics['user_new_90d_all'] !== 0)
                                ({{ number_format(round($statistics['user_new_90d_male'] / $statistics['user_new_90d_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['user_new_90d_female'] }}
                            @if($statistics['user_new_90d_all'] !== 0)
                                ({{ number_format(round($statistics['user_new_90d_female'] / $statistics['user_new_90d_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['federal'] == 1 && $statistics['user_new_90d_all_fed'] < $statistics['all_users']
                            && $statistics['user_new_90d_all_fed'] !== 0)
                                {{ number_format(round($statistics['user_new_90d_all_fed'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['sport'] == 1 && $statistics['user_new_90d_all_sp'] < $statistics['all_users']
                            && $statistics['user_new_90d_all_sp'] !== 0)
                                {{ number_format(round($statistics['user_new_90d_all_sp'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                    @endif
                    @if($abschnitt_auswahl == 2)

                        {{--<tr>--}}
                        {{--<td colspan="5" style="text-align: left;"><strong>Altersklassen</strong></td>--}}
                        {{--</tr>--}}

                    <tr>
                        <td style="text-align: left;"><12 Jahre</td>
                        <td>{{ $statistics['12'] }}
                            @if($statistics['user_registered'] !== 0)
                                ({{ number_format(round($statistics['12'] / $statistics['user_registered'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['12_veri'] }}
                            @if($statistics['12'] !== 0)
                                ({{ number_format(round($statistics['12_veri'] / $statistics['12'] * 100, 2), 2, ',', '.') }}
                                %)
                        @else
                            (0 %)
                        @endif
                        <td>{{ $statistics['12_male'] }}
                            @if($statistics['12'] !== 0)
                                ({{ number_format(round($statistics['12_male'] / $statistics['12'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['12_female'] }}
                            @if($statistics['12'] !== 0)
                                ({{ number_format(round($statistics['12_female'] / $statistics['12'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['federal'] == 1 && $statistics['12_fed'] < $statistics['all_users']
                            && $statistics['12_fed'] !== 0)
                                {{ number_format(round($statistics['12_fed'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['sport'] == 1 && $statistics['12_sp'] < $statistics['all_users']
                            && $statistics['12_sp'] !== 0)
                                {{ number_format(round($statistics['12_sp'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;"><13-17 Jahre</td>
                        <td>{{ $statistics['13'] }}
                            @if($statistics['user_registered'] !== 0)
                                ({{ number_format(round($statistics['13'] / $statistics['user_registered'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['13_veri'] }}
                            @if($statistics['13'] !== 0)
                                ({{ number_format(round($statistics['13_veri'] / $statistics['13'] * 100, 2), 2, ',', '.') }}
                                %)
                        @else
                            (0 %)
                        @endif
                        <td>{{ $statistics['13_male'] }}
                            @if($statistics['13'] !== 0)
                                ({{ number_format(round($statistics['13_male'] / $statistics['13'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['13_female'] }}
                            @if($statistics['13'] !== 0)
                                ({{ number_format(round($statistics['13_female'] / $statistics['13'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['federal'] == 1 && $statistics['13_fed'] < $statistics['all_users']
                            && $statistics['13_fed'] !== 0)
                                {{ number_format(round($statistics['13_fed'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['sport'] == 1 && $statistics['13_sp'] < $statistics['all_users']
                            && $statistics['13_sp'] !== 0)
                                {{ number_format(round($statistics['13_sp'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">18-24 Jahre</td>
                        <td>{{ $statistics['18'] }}
                            @if($statistics['user_registered'] !== 0)
                                ({{ number_format(round($statistics['18'] / $statistics['user_registered'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['18_veri'] }}
                            @if($statistics['18'] !== 0)
                                ({{ number_format(round($statistics['18_veri'] / $statistics['18'] * 100, 2), 2, ',', '.') }}
                                %)
                        @else
                            (0 %)
                        @endif
                        <td>{{ $statistics['18_male'] }}
                            @if($statistics['18'] !== 0)
                                ({{ number_format(round($statistics['18_male'] / $statistics['18'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['18_female'] }}
                            @if($statistics['18'] !== 0)
                                ({{ number_format(round($statistics['18_female'] / $statistics['18'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['federal'] == 1 && $statistics['18_fed'] < $statistics['all_users']
                            && $statistics['18_fed'] !== 0)
                                {{ number_format(round($statistics['18_fed'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['sport'] == 1 && $statistics['18_sp'] < $statistics['all_users']
                            && $statistics['18_sp'] !== 0)
                                {{ number_format(round($statistics['18_sp'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">25-34 Jahre</td>
                        <td>{{ $statistics['25'] }}
                            @if($statistics['user_registered'] !== 0)
                                ({{ number_format(round($statistics['25'] / $statistics['user_registered'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['25_veri'] }}
                            @if($statistics['25'] !== 0)
                                ({{ number_format(round($statistics['25_veri'] / $statistics['25'] * 100, 2), 2, ',', '.') }}
                                %)
                        @else
                            (0 %)
                        @endif
                        <td>{{ $statistics['25_male'] }}
                            @if($statistics['25'] !== 0)
                                ({{ number_format(round($statistics['25_male'] / $statistics['25'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['25_female'] }}
                            @if($statistics['25'] !== 0)
                                ({{ number_format(round($statistics['25_female'] / $statistics['25'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['federal'] == 1 && $statistics['25_fed'] < $statistics['all_users']
                            && $statistics['25_fed'] !== 0)
                                {{ number_format(round($statistics['25_fed'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['sport'] == 1 && $statistics['25_sp'] < $statistics['all_users']
                            && $statistics['25_sp'] !== 0)
                                {{ number_format(round($statistics['25_sp'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">35-44 Jahre</td>
                        <td>{{ $statistics['35'] }}
                            @if($statistics['user_registered'] !== 0)
                                ({{ number_format(round($statistics['35'] / $statistics['user_registered'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['35_veri'] }}
                            @if($statistics['35'] !== 0)
                                ({{ number_format(round($statistics['35_veri'] / $statistics['35'] * 100, 2), 2, ',', '.') }}
                                %)
                        @else
                            (0 %)
                        @endif
                        <td>{{ $statistics['35_male'] }}
                            @if($statistics['35'] !== 0)
                                ({{ number_format(round($statistics['35_male'] / $statistics['35'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['35_female'] }}
                            @if($statistics['35'] !== 0)
                                ({{ number_format(round($statistics['35_female'] / $statistics['35'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['federal'] == 1 && $statistics['35_fed'] < $statistics['all_users']
                            && $statistics['35_fed'] !== 0)
                                {{ number_format(round($statistics['35_fed'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['sport'] == 1 && $statistics['35_sp'] < $statistics['all_users']
                            && $statistics['35_sp'] !== 0)
                                {{ number_format(round($statistics['35_sp'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">45-54 Jahre</td>
                        <td>{{ $statistics['45'] }}
                            @if($statistics['user_registered'] !== 0)
                                ({{ number_format(round($statistics['45'] / $statistics['user_registered'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['45_veri'] }}
                            @if($statistics['45'] !== 0)
                                ({{ number_format(round($statistics['45_veri'] / $statistics['45'] * 100, 2), 2, ',', '.') }}
                                %)
                        @else
                            (0 %)
                        @endif
                        <td>{{ $statistics['45_male'] }}
                            @if($statistics['45'] !== 0)
                                ({{ number_format(round($statistics['45_male'] / $statistics['45'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['45_female'] }}
                            @if($statistics['45'] !== 0)
                                ({{ number_format(round($statistics['45_female'] / $statistics['45'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['federal'] == 1 && $statistics['45_fed'] < $statistics['all_users']
                            && $statistics['45_fed'] !== 0)
                                {{ number_format(round($statistics['45_fed'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['sport'] == 1 && $statistics['45_sp'] < $statistics['all_users']
                            && $statistics['45_sp'] !== 0)
                                {{ number_format(round($statistics['45_sp'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">55-64 Jahre</td>
                        <td>{{ $statistics['55'] }}
                            @if($statistics['user_registered'] !== 0)
                                ({{ number_format(round($statistics['55'] / $statistics['user_registered'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['55_veri'] }}
                            @if($statistics['55'] !== 0)
                                ({{ number_format(round($statistics['55_veri'] / $statistics['55'] * 100, 2), 2, ',', '.') }}
                                %)
                        @else
                            (0 %)
                        @endif
                        <td>{{ $statistics['55_male'] }}
                            @if($statistics['55'] !== 0)
                                ({{ number_format(round($statistics['55_male'] / $statistics['55'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['55_female'] }}
                            @if($statistics['55'] !== 0)
                                ({{ number_format(round($statistics['55_female'] / $statistics['55'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['federal'] == 1 && $statistics['55_fed'] < $statistics['all_users']
                            && $statistics['55_fed'] !== 0)
                                {{ number_format(round($statistics['55_fed'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['sport'] == 1 && $statistics['55_sp'] < $statistics['all_users']
                            && $statistics['55_sp'] !== 0)
                                {{ number_format(round($statistics['55_sp'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">> 65 Jahre</td>
                        <td>{{ $statistics['65'] }}
                            @if($statistics['user_registered'] !== 0)
                                ({{ number_format(round($statistics['65'] / $statistics['user_registered'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['65_veri'] }}
                            @if($statistics['65'] !== 0)
                                ({{ number_format(round($statistics['65_veri'] / $statistics['65'] * 100, 2), 2, ',', '.') }}
                                %)
                        @else
                            (0 %)
                        @endif
                        <td>{{ $statistics['65_male'] }}
                            @if($statistics['65'] !== 0)
                                ({{ number_format(round($statistics['65_male'] / $statistics['65'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['65_female'] }}
                            @if($statistics['65'] !== 0)
                                ({{ number_format(round($statistics['65_female'] / $statistics['65'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['federal'] == 1 && $statistics['65_fed'] < $statistics['all_users']
                            && $statistics['65_fed'] !== 0)
                                {{ number_format(round($statistics['65_fed'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['sport'] == 1 && $statistics['65_sp'] < $statistics['all_users']
                            && $statistics['65_sp'] !== 0)
                                {{ number_format(round($statistics['65_sp'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                    @endif
                    @if($abschnitt_auswahl == 3)
                        {{--<tr>--}}
                        {{--<td colspan="5" style="text-align: left;"><strong>Aktivität</strong></td>--}}
                        {{--</tr>--}}

                    <tr>
                        <td style="text-align: left;">Letzte 24 h</td>
                        <td>{{ $statistics['user_act_24_all'] }}
                            @if($statistics['user_registered'] !== 0)
                                ({{ number_format(round($statistics['user_act_24_all'] / $statistics['user_registered'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                    (0 %)
                            @endif
                        </td>
                        <td>-</td>
                        <td>{{ $statistics['user_act_24_male'] }}
                            @if($statistics['user_act_24_all'] !== 0)
                                ({{ number_format(round($statistics['user_act_24_male'] / $statistics['user_act_24_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['user_act_24_female'] }}
                            @if($statistics['user_act_24_all'] !== 0)
                                ({{ number_format(round($statistics['user_act_24_female'] / $statistics['user_act_24_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['federal'] == 1 && $statistics['user_act_24_all_fed'] < $statistics['all_users']
                            && $statistics['user_act_24_all_fed'] !== 0)
                                {{ number_format(round($statistics['user_act_24_all_fed'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['sport'] == 1 && $statistics['user_act_24_all_sp'] < $statistics['all_users']
                            && $statistics['user_act_24_all_sp'] !== 0)
                                {{ number_format(round($statistics['user_act_24_all_sp'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Letzte 7 Tage</td>
                        <td>{{ $statistics['user_act_7d_all'] }}
                            @if($statistics['user_registered'] !== 0)
                                ({{ number_format(round($statistics['user_act_7d_all'] / $statistics['user_registered'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                    (0 %)
                            @endif
                        </td>
                        <td>-</td>
                        <td>{{ $statistics['user_act_7d_male'] }}
                            @if($statistics['user_act_7d_all'] !== 0)
                                ({{ number_format(round($statistics['user_act_7d_male'] / $statistics['user_act_7d_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['user_act_7d_female'] }}
                            @if($statistics['user_act_7d_all'] !== 0)
                                ({{ number_format(round($statistics['user_act_7d_female'] / $statistics['user_act_7d_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['federal'] == 1 && $statistics['user_act_7d_all_fed'] < $statistics['all_users']
                            && $statistics['user_act_7d_all_fed'] !== 0)
                                {{ number_format(round($statistics['user_act_7d_all_fed'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['sport'] == 1 && $statistics['user_act_7d_all_sp'] < $statistics['all_users']
                            && $statistics['user_act_7d_all_sp'] !== 0)
                                {{ number_format(round($statistics['user_act_7d_all_sp'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Letzte 30 Tage</td>
                        <td>{{ $statistics['user_act_30d_all'] }}
                            @if($statistics['user_registered'] !== 0)
                                ({{ number_format(round($statistics['user_act_30d_all'] / $statistics['user_registered'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                    (0 %)
                            @endif
                        </td>
                        <td>-</td>
                        <td>{{ $statistics['user_act_30d_male'] }}
                            @if($statistics['user_act_30d_all'] !== 0)
                                ({{ number_format(round($statistics['user_act_30d_male'] / $statistics['user_act_30d_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['user_act_30d_female'] }}
                            @if($statistics['user_act_30d_all'] !== 0)
                                ({{ number_format(round($statistics['user_act_30d_female'] / $statistics['user_act_30d_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['federal'] == 1 && $statistics['user_act_30d_all_fed'] < $statistics['all_users']
                            && $statistics['user_act_30d_all_fed'] !== 0)
                                {{ number_format(round($statistics['user_act_30d_all_fed'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['sport'] == 1 && $statistics['user_act_30d_all_sp'] < $statistics['all_users']
                            && $statistics['user_act_30d_all_sp'] !== 0)
                                {{ number_format(round($statistics['user_act_30d_all_sp'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Letzte 90 Tage</td>
                        <td>{{ $statistics['user_act_90d_all'] }}
                            @if($statistics['user_registered'] !== 0)
                                ({{ number_format(round($statistics['user_act_90d_all'] / $statistics['user_registered'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                    (0 %)
                            @endif
                        </td>
                        <td>-</td>
                        <td>{{ $statistics['user_act_90d_male'] }}
                            @if($statistics['user_act_90d_all'] !== 0)
                                ({{ number_format(round($statistics['user_act_90d_male'] / $statistics['user_act_90d_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['user_act_90d_female'] }}
                            @if($statistics['user_act_90d_all'] !== 0)
                                ({{ number_format(round($statistics['user_act_90d_female'] / $statistics['user_act_90d_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['federal'] == 1 && $statistics['user_act_90d_all_fed'] < $statistics['all_users']
                            && $statistics['user_act_90d_all_fed'] !== 0)
                                {{ number_format(round($statistics['user_act_90d_all_fed'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['sport'] == 1 && $statistics['user_act_90d_all_sp'] < $statistics['all_users']
                            && $statistics['user_act_90d_all_sp'] !== 0)
                                {{ number_format(round($statistics['user_act_90d_all_sp'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                    @endif
                    @if($abschnitt_auswahl == 4)
                        {{--<tr>--}}
                        {{--<td colspan="5" style="text-align: left;"><strong>Intensität</strong></td>--}}
                        {{--</tr>--}}

                    <tr>
                        <td style="text-align: left;">Pflicht teils</td>
                        <td>{{ $statistics['user_mand_part_all'] }}
                            @if($statistics['user_registered'] !== 0)
                                ({{ number_format(round($statistics['user_mand_part_all'] / $statistics['user_registered'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                    (0 %)
                            @endif
                        </td>
                        <td>-</td>
                        <td>{{ $statistics['user_mand_part_male'] }}
                            @if($statistics['user_mand_part_all'] !== 0)
                                ({{ number_format(round($statistics['user_mand_part_male'] / $statistics['user_mand_part_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['user_mand_part_female'] }}
                            @if($statistics['user_mand_part_all'] !== 0)
                                ({{ number_format(round($statistics['user_mand_part_female'] / $statistics['user_mand_part_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['federal'] == 1 && $statistics['user_mand_part_all_fed'] < $statistics['all_users']
                            && $statistics['user_mand_part_all_fed'] !== 0)
                                {{ number_format(round($statistics['user_mand_part_all_fed'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['sport'] == 1 && $statistics['user_mand_part_all_sp'] < $statistics['all_users']
                            && $statistics['user_mand_part_all_sp'] !== 0)
                                {{ number_format(round($statistics['user_mand_part_all_sp'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Pflicht voll</td>
                        <td>{{ $statistics['user_mand_all'] }}
                            @if($statistics['user_registered'] !== 0)
                                ({{ number_format(round($statistics['user_mand_all'] / $statistics['user_registered'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                    (0 %)
                            @endif
                        </td>
                        <td>-</td>
                        <td>{{ $statistics['user_mand_male'] }}
                            @if($statistics['user_mand_all'] !== 0)
                                ({{ number_format(round($statistics['user_mand_male'] / $statistics['user_mand_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['user_mand_female'] }}
                            @if($statistics['user_mand_all'] !== 0)
                                ({{ number_format(round($statistics['user_mand_female'] / $statistics['user_mand_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['federal'] == 1 && $statistics['user_mand_all_fed'] < $statistics['all_users']
                            && $statistics['user_mand_all_fed'] !== 0)
                                {{ number_format(round($statistics['user_mand_all_fed'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['sport'] == 1 && $statistics['user_mand_all_sp'] < $statistics['all_users']
                            && $statistics['user_mand_all_sp'] !== 0)
                                {{ number_format(round($statistics['user_mand_all_sp'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Sportvita</td>
                        <td>{{ $statistics['user_sportvita_all'] }}
                            @if($statistics['user_registered'] !== 0)
                                ({{ number_format(round($statistics['user_sportvita_all'] / $statistics['user_registered'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                    (0 %)
                            @endif
                        </td>
                        <td>-</td>
                        <td>{{ $statistics['user_sportvita_male'] }}
                            @if($statistics['user_sportvita_all'] !== 0)
                                ({{ number_format(round($statistics['user_sportvita_male'] / $statistics['user_sportvita_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['user_sportvita_female'] }}
                            @if($statistics['user_sportvita_all'] !== 0)
                                ({{ number_format(round($statistics['user_sportvita_female'] / $statistics['user_sportvita_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['federal'] == 1 && $statistics['user_sportvita_all_fed'] < $statistics['all_users']
                            && $statistics['user_sportvita_all_fed'] !== 0)
                                {{ number_format(round($statistics['user_sportvita_all_fed'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['sport'] == 1 && $statistics['user_sportvita_all_sp'] < $statistics['all_users']
                            && $statistics['user_sportvita_all_sp'] !== 0)
                                {{ number_format(round($statistics['user_sportvita_all_sp'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Profilbild</td>
                        <td>{{ $statistics['user_profile_pic_all'] }}
                            @if($statistics['user_registered'] !== 0)
                                ({{ number_format(round($statistics['user_profile_pic_all'] / $statistics['user_registered'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                    (0 %)
                            @endif
                        </td>
                        <td>-</td>
                        <td>{{ $statistics['user_profile_pic_male'] }}
                            @if($statistics['user_profile_pic_all'])
                                ({{ number_format(round($statistics['user_profile_pic_male'] / $statistics['user_profile_pic_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['user_profile_pic_female'] }}
                            @if($statistics['user_profile_pic_all'] !== 0)
                                ({{ number_format(round($statistics['user_profile_pic_female'] / $statistics['user_profile_pic_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['federal'] == 1 && $statistics['user_profile_pic_all_fed'] < $statistics['all_users']
                            && $statistics['user_profile_pic_all_fed'] !== 0)
                                {{ number_format(round($statistics['user_profile_pic_all_fed'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['sport'] == 1 && $statistics['user_profile_pic_all_sp'] < $statistics['all_users']
                            && $statistics['user_profile_pic_all_sp'] !== 0)
                                {{ number_format(round($statistics['user_profile_pic_all_sp'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                    @endif
                    @if($abschnitt_auswahl == 5)
                        {{--<tr>--}}
                        {{--<td colspan="5" style="text-align: left;"><strong>Löschungen</strong></td>--}}
                        {{--</tr>--}}

                    <tr>
                        <td style="text-align: left;">Letzte 24 h</td>
                        <td>{{ $statistics['user_del_24_all'] }}
                            @if($statistics['user_registered'] !== 0)
                                ({{ number_format(round($statistics['user_del_24_all'] / $statistics['user_registered'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                    (0 %)
                            @endif
                        </td>
                        </td>
                        <td>-</td>
                        <td>{{ $statistics['user_del_24_male'] }}
                            @if($statistics['user_del_24_all'] !== 0)
                                ({{ number_format(round($statistics['user_del_24_male'] / $statistics['user_del_24_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['user_del_24_female'] }}
                            @if($statistics['user_del_24_all'] !== 0)
                                ({{ number_format(round($statistics['user_del_24_female'] / $statistics['user_del_24_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['federal'] == 1 && $statistics['user_del_24_all_fed'] < $statistics['all_users']
                            && $statistics['user_del_24_all_fed'] !== 0)
                                {{ number_format(round($statistics['user_del_24_all_fed'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['sport'] == 1 && $statistics['user_del_24_all_sp'] < $statistics['all_users']
                            && $statistics['user_del_24_all_sp'] !== 0)
                                {{ number_format(round($statistics['user_del_24_all_sp'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Letzte 7 Tage</td>
                        <td>{{ $statistics['user_del_7_all'] }}
                            @if($statistics['user_registered'] !== 0)
                                ({{ number_format(round($statistics['user_del_7_all'] / $statistics['user_registered'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                    (0 %)
                            @endif
                        </td>
                        </td>
                        <td>-</td>
                        <td>{{ $statistics['user_del_7_male'] }}
                            @if($statistics['user_del_7_all'] !== 0)
                                ({{ number_format(round($statistics['user_del_7_male'] / $statistics['user_del_7_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['user_del_7_female'] }}
                            @if($statistics['user_del_7_all'] !== 0)
                                ({{ number_format(round($statistics['user_del_7_female'] / $statistics['user_del_7_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['federal'] == 1 && $statistics['user_del_7_all_fed'] < $statistics['all_users']
                            && $statistics['user_del_7_all_fed'] !== 0)
                                {{ number_format(round($statistics['user_del_7_all_fed'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['sport'] == 1 && $statistics['user_del_7_all_sp'] < $statistics['all_users']
                            && $statistics['user_del_7_all_sp'] !== 0)
                                {{ number_format(round($statistics['user_del_7_all_sp'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Letzte 30 Tage</td>
                        <td>{{ $statistics['user_del_30_all'] }}
                            @if($statistics['user_registered'] !== 0)
                                ({{ number_format(round($statistics['user_del_30_all'] / $statistics['user_registered'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                    (0 %)
                            @endif
                        </td>
                        <td>-</td>
                        <td>{{ $statistics['user_del_30_male'] }}
                            @if($statistics['user_del_30_all'] !== 0)
                                ({{ number_format(round($statistics['user_del_30_male'] / $statistics['user_del_30_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['user_del_30_female'] }}
                            @if($statistics['user_del_30_all'] !== 0)
                                ({{ number_format(round($statistics['user_del_30_female'] / $statistics['user_del_30_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['federal'] == 1 && $statistics['user_del_30_all_fed'] < $statistics['all_users']
                            && $statistics['user_del_30_all_fed'] !== 0)
                                {{ number_format(round($statistics['user_del_30_all_fed'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['sport'] == 1 && $statistics['user_del_30_all_sp'] < $statistics['all_users']
                            && $statistics['user_del_30_all_sp'] !== 0)
                                {{ number_format(round($statistics['user_del_30_all_sp'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Letzte 90 Tage</td>
                        <td>{{ $statistics['user_del_90_all'] }}
                            @if($statistics['user_registered'] !== 0)
                                ({{ number_format(round($statistics['user_del_90_all'] / $statistics['user_registered'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                    (0 %)
                            @endif
                        </td>
                        <td>-</td>
                        <td>{{ $statistics['user_del_90_male'] }}
                            @if($statistics['user_del_90_all'] !== 0)
                                ({{ number_format(round($statistics['user_del_90_male'] / $statistics['user_del_90_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>{{ $statistics['user_del_90_female'] }}
                            @if($statistics['user_del_90_all'] !== 0)
                                ({{ number_format(round($statistics['user_del_90_female'] / $statistics['user_del_90_all'] * 100, 2), 2, ',', '.') }}
                                %)
                            @else
                                (0 %)
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['federal'] == 1 && $statistics['user_del_90_all_fed'] < $statistics['all_users']
                            && $statistics['user_del_90_all_fed'] !== 0)
                                {{ number_format(round($statistics['user_del_90_all_fed'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            @if($statistics['all_users'] !== 0 && $statistics['sport'] == 1 && $statistics['user_del_90_all_sp'] < $statistics['all_users']
                            && $statistics['user_del_90_all_sp'] !== 0)
                                {{ number_format(round($statistics['user_del_90_all_sp'] / $statistics['all_users'] * 100, 2), 2, ',', '.') }}
                                %
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                    @endif
                    </tbody>
                </table>
            @endif
        </div>
    </section>
    @push('scripts')
        <script>
            $(document).ready(function () {
                $('#user-dash').DataTable({
                    dom     : 'Bfrtip',
                    buttons : [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5'
                    ],
                    ordering: false,
                    language: {
                        url: '/js/vendor/i18n/dataTables.german.json'
                    }
                });
            })
        </script>

        <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    @endpush
@endsection


