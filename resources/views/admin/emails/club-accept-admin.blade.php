<!DOCTYPE html>
<html lang="de-DE">
<head>
    <meta charset="utf-8">
</head>
<body>

<div>
    <strong>Hallo {{ $user->firstname }},</strong><br>
    <br>
    dein Antrag auf Übernahme des Vereinsprofils vom Vereinsname wurde angenommen. Du kannst Dich gleich mit Deinen
    Benutzerdaten auf vereinsleben.de einloggen und das Vereinsprofil pflegen. Über den Punkt „Meine Vereine“ im
    Profilmenü rechts oben kommst du jederzeit zu Deinem Vereinsprofil.<br><br>

    Wenn Du Probleme oder Fragen hast, kannst Du Dich gerne direkt an unser Team unter der Telefonnummer 0621-59000115
    oder per E-Mail an info@vereinsleben.de wenden.<br><br>

    Wir wünschen Dir viel Spaß auf vereinsleben.de!<br><br>

    Sportliche Grüße,<br>
    Dein vereinsleben.de-Team
</div>
@if(isset($imprint))
    <br/>
    <br/>
    <br/>
    <div>{!! $imprint->content !!}</div>
@endif
</body>
</html>