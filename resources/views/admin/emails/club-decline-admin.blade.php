<!DOCTYPE html>
<html lang="de-DE">
<head>
    <meta charset="utf-8">
</head>
<body>

<div>
    <strong>Hallo {{ $user->firstname }},</strong><br>
    <br>
    dein Antrag auf Übernahme des Vereinsprofils vom Vereinsname wurde leider abgelehnt, weil wir keinen Nachweis für
    eine Tätigkeit im Vereinsvorstand finden konnten.<br><br>

    Wenn Du dennoch offiziell vom Verein hierfür beauftragt und zuständig bist, wende Dich bitte direkt an unser Team
    unter der Telefonnummer 0621-59000115 oder per E-Mail an info@vereinsleben.de.<br><br>

    Sportliche Grüße,<br>
    Dein vereinsleben.de-Team
</div>
@if(isset($imprint))
    <br/>
    <br/>
    <br/>
    <div>{!! $imprint->content !!}</div>
@endif
</body>
</html>