<!DOCTYPE html>
<html lang="de-DE">
<head>
    <meta charset="utf-8">
</head>
<body>

<div>
    <strong>Hallo {{ $user->firstname }},</strong><br>
    <br>
    dein Vereinsvorschlag wurde akzeptiert.<br>
    Der Verein {{ $clubToRemove->name }} wurde auf vereinsleben.de veröffentlicht.
    <br><br>
    Vielen Dank für deinen Vorschlag.
    <br><br>
    Wir wünschen Dir weiterhin viel Spaß auf vereinsleben.de!<br><br>

    Sportliche Grüße,<br>
    Dein vereinsleben.de-Team
</div>

</body>
</html>