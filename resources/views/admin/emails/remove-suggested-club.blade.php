<!DOCTYPE html>
<html lang="de-DE">
<head>
    <meta charset="utf-8">
</head>
<body>

<div>
    <strong>Hallo {{ $user->firstname }},</strong><br>
    <br>
    dein Vereinsvorschlag wurde abgelehnt.<br>
    Wahrscheinlich meintest du mit deinem Vereinsvorschlag den Verein {{ $clubExisting->name }}.
    <br>
    Solltest du einen anderen Verein gemeint haben, kannst Du Dich gerne direkt an unser Team unter der Telefonnummer
    0621-59000115
    oder per E-Mail an info@vereinsleben.de wenden.
    <br><br>
    Wir wünschen Dir weiterhin viel Spaß auf vereinsleben.de!<br><br>

    Sportliche Grüße,<br>
    Dein vereinsleben.de-Team
</div>

</body>
</html>