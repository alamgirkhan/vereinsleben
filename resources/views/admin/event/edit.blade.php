@extends('layouts.master')

@section('js-additional-vendor')
    <script src="{{ asset(('js/vendor/ckeditor/ckeditor.js')) }}"></script>
@endsection

@section('content')
    <section class="admin__overview">
        <div class="container-flex">

            @if($errors->count() > 0)
                @foreach($errors->all() as $error)
                    <p>{{$error}}</p>
                @endforeach
            @endif
            <a class="" href="{{ route('admin.event.list') }}">
                <span class="fa fa-caret-left"></span> Zur Übersicht
            </a>
            <div class="row">
                <div class="col-md-10 col-md-offset-2">
                    @if ($event->id === null)
                        {!! Form::open(
                            [
                                'route' => 'admin.event.store',
                                'enctype' => 'multipart/form-data',
                                'files' => true,
                                'method' => 'POST',
                            ]
                        )!!}
                    @else
                        {!! Form::model(
                            $event,
                            [
                                'route' => ['admin.event.update', $event->id],
                                'enctype' => 'multipart/form-data',
                                'files' => true,
                                'method' => 'PATCH',
                            ]
                        )!!}
                    @endif
                    <h3>Event anlegen oder editieren</h3>

                    <div class="row">
                        <div class="col-md-6">
                            {!! Form::label('Sender', 'Titel', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {{--{!! Form::select('sender', [--}}
                                {{--'0' => 'Bitte wählen',--}}
                                {{--'1' => 'vereinsleben.de',--}}
                                {{--'2' => 'Verein',--}}
                                {{--'3' => 'Verband',--}}
                                {{--], null, ['class' => 'select', 'id' => 'sender']) !!}--}}
                                <select name="sender" id="sender" class="select">
                                    <option value="0">Bitte wählen</option>
                                    <option value="1">vereinsleben.de</option>
                                    <option value="2">Verein</option>
                                    <option value="3">Verband</option>
                                    @isset($event->club_id)
                                        <option value="2" selected="selected">Verein</option>
                                    @endisset
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            {!! Form::label('id', 'Verein oder Verband', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('sender_id', isset($event->club_id) ? $event->club_id : $event->bandage_id, ['class' => 'input', 'placeholder' => 'ID eingeben']) !!}
                            </div>
                        </div>

                        <div class="col-md-12">
                            {!! Form::label('title', 'Titel', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('title', null, ['class' => 'input', 'placeholder' => 'Titel', 'required' => 'required']) !!}
                            </div>
                        </div>

                        <div class="col-md-12">
                            {!! Form::label('content_raw', 'Text', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::textarea('content_raw', null, ['class' => 'input', 'rows' => 10, 'placeholder' => 'Beschreibung der Veranstaltung', 'required' => 'required']) !!}
                            </div>
                        </div>

                        <div class="col-md-6">
                            {!! Form::label('schedule_begin', 'Beginn der Veranstaltung', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('schedule_begin', isset($event->schedule_begin) ? date('d.m.Y H:i', strtotime($event->schedule_begin)) : null, ['class' => 'input', 'placeholder' => 'DD.MM.YY HH:mm', 'data-datetimepicker-enabled', 'required' => 'required']) !!}
                            </div>
                        </div>

                        <div class="col-md-6">
                            {!! Form::label('schedule_end', 'Ende der Veranstaltung', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('schedule_end', isset($event->schedule_end) ? date('d.m.Y H:i', strtotime($event->schedule_end)) : null, ['class' => 'input', 'placeholder' => 'DD.MM.YY HH:mm', 'data-datetimepicker-enabled', 'required' => 'required']) !!}
                            </div>
                        </div>

                        <div class="col-md-9">
                            {!! Form::label('street', 'Straße', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('street', null, ['class' => 'input', 'placeholder' => 'Straße', 'required' => 'required']) !!}
                            </div>
                        </div>

                        <div class="col-md-3">
                            {!! Form::label('house_number', 'Hausnummer', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('house_number', null, ['class' => 'input', 'placeholder' => 'Nr']) !!}
                            </div>
                        </div>

                        <div class="col-md-4">
                            {!! Form::label('zip', 'Postleitzahl', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('zip', null, ['class' => 'input', 'placeholder' => 'PLZ', 'required' => 'required']) !!}
                            </div>
                        </div>

                        <div class="col-md-8">
                            {!! Form::label('city', 'Stadt', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('city', null, ['class' => 'input', 'placeholder' => 'Stadt / Ort', 'required' => 'required']) !!}
                            </div>
                        </div>

                        <div class="col-md-6">
                            {!! Form::label('price', 'Eintrittspreis', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form:: number('price', null, ['class' => 'input', 'placeholder' => 'Eintrittspreis', 'step' => '0.01']) !!}
                            </div>
                        </div>

                        <div class="col-md-6">
                            {!! Form::label('veranstaltungsort', 'Veranstaltungsort', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form:: text('location', null, ['class' => 'input', 'placeholder' => 'Veranstaltungsort - z.B. Sporthalle']) !!}
                            </div>
                        </div>

                        <div class="col-md-12">
                            {!! Form::label('sport_art', 'Sportart', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {{--{!! Form::select('sport', null, ['class' => 'select', 'id' => 'sport', 'data-select-sports' => 'true',--}}
                                {{--'data-sports-url' => "{{ app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('api.sports') }}"]) !!}--}}


                                <select class="club-detail__select-sports" name="sport" data-select-sports="true"
                                        data-sports-url="{{ app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('api.sports') }}"
                                        id="sport">
                                    @isset($sport)
                                        <option value="{{ $sport->id }}"
                                                selected="selected">{{ $sport->title }}</option>
                                    @endisset
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            {!! Form::label('published_from', 'Veröffentlicht von', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('published_from', isset($event->published_from) ? date('d.m.Y H:i', strtotime($event->published_from)) : null, ['class' => 'input', 'placeholder' => 'DD.MM.YY HH:mm', 'data-datetimepicker-enabled']) !!}
                            </div>
                        </div>

                        <div class="col-md-6">
                            {!! Form::label('published_until', 'Veröffentlicht bis', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('published_to', isset($event->published_to) ? date('d.m.Y H:i', strtotime($event->published_to)) : null, ['class' => 'input', 'placeholder' => 'DD.MM.YY HH:mm', 'data-datetimepicker-enabled']) !!}
                            </div>
                        </div>

                        {{--{!! Form::hidden('club_id', $club->id) !!}--}}

                        <div class="col-xs-12">
                            <div id="@if (isset($event)){{ 'upload-result-'. $event->id }}@else{{ 'upload-result' }}@endif "
                                 class="thumbnail__list">
                                @if (isset($event))
                                    @foreach($event->images as $image)
                                        <input type="checkbox" name="delete-image[]" value="{{ $image->id }}"
                                               id="event_thumbnail-{{ $image->id }}" class="thumbnail__switch">
                                        <label for="event_thumbnail-{{ $image->id }}"
                                               class="thumbnail__label thumbnail__container thumbnail__container--deleteable">
                                            <img class="thumbnail__image"
                                                 src="{{asset($image->picture->url('singleView')) }}"/>
                                        </label>
                                    @endforeach
                                @endif
                            </div>
                        </div>

                        <div data-upload-result>
                            <div class="col-xs-12 hidden" data-new-image-text>
                                <span class="input-label">Neu hinzugefügte Bilder</span>
                            </div>
                            <div class="col-xs-12">
                                <div id="@if (isset($post)){{ 'upload-result-'. $post->id }}@else{{ 'upload-result' }}@endif "
                                     class="thumbnail__list">
                                </div>
                            </div>

                            <div class="col-xs-12">
                                <div class="club-detail__inline-form-submit-wrapper">
                                    <label for="@if (isset($event)){{ 'event_image-'. $event->id }}@else{{ 'event_image' }}@endif"
                                           class="button button--grey button--center button--icon button--full-width">
                                        <span class="fa fa-upload"></span>Bilder hochladen</label>
                                    <input multiple type="file" name="images[]"
                                           id="@if (isset($event)){{ 'event_image-'. $event->id }}@else{{ 'event_image' }}@endif"
                                           class="input-file">
                                    <button class="button button--icon button--full-width button--center button--light"
                                            type="submit"><span class="fa fa-edit"></span>
                                        @if ($event->id === null)
                                            Eintragen
                                        @else
                                            Aktualisieren
                                        @endif
                                    </button>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" id="admin-event-csrf" value="{{ csrf_token() }}">
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection