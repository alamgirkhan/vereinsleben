@extends('layouts.master')

@section('content')
    @include('admin.menu')
    <section class="admin__overview">
        <div class="container">
            {{--<ul class="admin__menu">--}}
            {{--<li class="admin__menu-item{{ Request::is('admin/dashboard/user/index') ? ' active' : '' }}">--}}
            {{--<a class="admin__menu-link" href="{{ url('admin/dashboard/user/index') }}">Userstatistik</a>--}}
            {{--</li>--}}
            {{--<li class="admin__menu-item{{ Request::is('admin/dashboard/club/clubs') ? ' active' : '' }}">--}}
            {{--<a class="admin__menu-link" href="{{ url('admin/dashboard/club/clubs') }}">Vereinsstatistik</a>--}}
            {{--</li>--}}
            {{--<li class="admin__menu-item{{ Request::is('admin/fakedomains') ? ' active' : '' }}">--}}
            {{--<a class="admin__menu-link" href="{{ url('admin/fakedomains') }}">Fake-Mails</a>--}}
            {{--</li>--}}
            {{--<li class="admin__menu-item{{ Request::is('admin/fakeusers') ? ' active' : '' }}">--}}
            {{--<a class="admin__menu-link" href="{{ url('admin/fakeusers') }}">Fake-Users</a>--}}
            {{--</li>--}}
            {{--<li class="admin__menu-item{{ Request::is('admin/events/all') ? ' active' : '' }}">--}}
            {{--<a class="admin__menu-link" href="{{ url('admin/events/all') }}">Events</a>--}}
            {{--</li>--}}
            {{--<li class="admin__menu-item{{ Request::is('admin/contest') ? ' active' : '' }}">--}}
            {{--<a class="admin__menu-link" href="{{ url('admin/contest') }}">Teambesprechungsraum</a>--}}
            {{--</li>--}}
            {{--</ul>--}}
            <div class="row admin__section">
                <div class="col-xs-12">
                    <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">Events anlegen und editieren</h1>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <a href="{{ route('admin.event.edit', 0) }}"
                           class="button button--icon button--full-width button--center button--light">
                            <span class="fa fa-plus"></span> Event hinzufügen
                        </a>
                    </div>
                </div>
            </div>
            <table id="admin-table" class="admin__table" data-token="{{ csrf_token() }}">
                <thead>
                <tr>
                    <th style="width:5%">ID&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:35%">Titel&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:10%">Sportart&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:15%">Absender&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:10%">Publiziert</th>
                    <th style="width:10%">Optionen</th>
                </tr>
                </thead>
                <tbody>
                @foreach($events as $event)
                    <tr>
                        <td>{{ $event->id }}</td>
                        <td><a href="{{ route('event.detail', $event->id) }}" target="_blank">{{ $event->title }}</a>
                        </td>
                        <td>
                            @php
                                if(isset($event->sport)){
                                    $sport = \Vereinsleben\Sport::whereId($event->sport)->firstOrFail();
                                    $sport_title = $sport->title;
                                }
                            @endphp
                            @isset($event->sport)
                                {{ $sport_title }}
                            @endisset
                        </td>
                        <td>
                            @if(isset($event->club_id))
                                Verein
                            @elseif(isset($event->bandage_id))
                                Verband
                            @else
                                vereinsleben.de
                            @endif

                        </td>
                        <td>
                            @if($event->published_at)
                                <i class="fa fa-circle" style="color: green;"></i>
                            @else
                                <i class="fa fa-circle" style="color: red;"></i>
                            @endif
                        </td>
                        <td>
                            <div class="admin__table-option">
                                <a href="{{ route('admin.event.edit', $event->id) }}" target="_blank">
                                    <span class="fa fa-edit"></span>
                                </a>
                                {{--@if(is_null($event->deleted_at))--}}
                                {{--<a data-toggle-delete data-record-id="{{ $event->id }}"--}}
                                {{--data-delete-path="{{ route('admin.event.destroy') }}" href="#">--}}
                                {{--<span class="fa fa-trash"></span>--}}
                                {{--</a>--}}
                                {{--@endif--}}
                                <a onclick="return confirm('Möchtest du diese Aufzeichnung wirklich löschen?')"
                                   href="#">
                                    <span class="fa fa-trash"></span>
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </section>
@endsection