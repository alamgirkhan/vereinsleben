@extends('layouts.master')

@section('content')
    {{--@include('components.headerimage-still',
    ['condensed' => true,
     'content' => [
            'icon' =>'newspaper-o',
            'headline' => 'Vereine',
    ]])--}}
    @include('admin.menu')
    <section class="admin__section">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">Exporte</h1>
                </div>
                <div class="col-xs-12 col-md-6">
                    <p>Benutzer</p>
                    <a href="{{ route('admin.user.export') }}"
                       class="btn btn--dark-blue btn--large btn--inline btn--upper">
                        Benutzer Exportieren
                    </a>
                </div>
                <div class="col-xs-12 col-md-6">
                    <p>Vereine</p>
                    <a href="{{ route('admin.club.export') }}"
                       class="btn btn--dark-blue btn--large btn--inline btn--upper">
                        Vereine Exportieren
                    </a>
                </div>
            </div>
            <div class="row admin__section">
                <div class="col-xs-6">
                    <p>Hinweis:</p>
                    <p>Durch die Zusammenfassung von Vereinsbeziehungen (Sportarten, Mitglieder, Mannschaften, ...) kann
                        der Exportvorgang einen kurze Zeit in Anspruch nehmen.</p>
                </div>
            </div>
        </div>
    </section>
@endsection