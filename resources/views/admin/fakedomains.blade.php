@extends('layouts.master')

@section('content')
    @include('admin.menu')
    <section class="admin__overview">
        <div class="container">
            {{--<ul class="admin__menu">--}}
            {{--<li class="admin__menu-item{{ Request::is('admin/dashboard/user/index') ? ' active' : '' }}">--}}
            {{--<a class="admin__menu-link" href="{{ url('admin/dashboard/user/index') }}">Userstatistik</a>--}}
            {{--</li>--}}
            {{--<li class="admin__menu-item{{ Request::is('admin/dashboard/club/clubs') ? ' active' : '' }}">--}}
            {{--<a class="admin__menu-link" href="{{ url('admin/dashboard/club/clubs') }}">Vereinsstatistik</a>--}}
            {{--</li>--}}
            {{--<li class="admin__menu-item{{ Request::is('admin/fakedomains') ? ' active' : '' }}">--}}
            {{--<a class="admin__menu-link" href="{{ url('admin/fakedomains') }}">Fake-Mails</a>--}}
            {{--</li>--}}
            {{--<li class="admin__menu-item{{ Request::is('admin/fakeusers') ? ' active' : '' }}">--}}
            {{--<a class="admin__menu-link" href="{{ url('admin/fakeusers') }}">Fake-Users</a>--}}
            {{--</li>--}}
            {{--<li class="admin__menu-item{{ Request::is('admin/events/all') ? ' active' : '' }}">--}}
            {{--<a class="admin__menu-link" href="{{ url('admin/events/all') }}">Events</a>--}}
            {{--</li>--}}
            {{--<li class="admin__menu-item{{ Request::is('admin/contest') ? ' active' : '' }}">--}}
            {{--<a class="admin__menu-link" href="{{ url('admin/contest') }}">Teambesprechungsraum</a>--}}
            {{--</li>--}}
            {{--</ul>--}}
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">Fake Domains</h1>
                </div>
            </div>
            <div class="row">
                {!! Form::open(['route' => 'admin.fakedomains.add', 'method' => 'GET', 'id' => 'search', 'class' => '']) !!}
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="input-group">
                            {!! Form::text('domain', isset($query['domain']) ? $query['domain'] : null, ['id' => 'city', 'autofocus', 'class' => 'input', 'placeholder' => 'Domain']) !!}
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="input-group">
                            {!! Form::submit('Hinzufügen', array('class' => 'button button--light button--light-white button--center button--full-width')) !!}
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
            <table id="fakemail" class="admin__table" data-token="{{ csrf_token() }}">
                <thead>
                <tr>
                    <th style="width:15%">Domain&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:10%">Optionen</th>
                </tr>
                </thead>
                <tbody>
                @foreach($file as $key => $content)
                    <tr>
                        <td>{{ $content }}</td>
                        <td>
                            <div class="admin__table-option">
                                <a onclick="return confirm('Domain wirklich entfernen?')"
                                   href="{{ route('admin.fakedomains.destroy', $content) }}">
                                    <span class="fa fa-trash"></span></a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </section>
    @push('scripts')
        <script>
            $(document).ready(function () {
                $('#fakemail').DataTable({
                    dom         : 'Blfrtip',
                    "lengthMenu": [50, 100, 250],
                    buttons     : [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5'
                    ],

                    language: {
                        url: '/js/vendor/i18n/dataTables.german.json'
                    }
                });
            })
        </script>

        <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    @endpush
@endsection