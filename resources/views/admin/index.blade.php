@extends('layouts.master')

@section('content')
    @if(Auth::user()->isAdmin())
        @include('admin.menu')
    @endif
    <section class="admin__overview">
        <div class="container">
            <div class="row admin__section">
                <div class="col-xs-12">
                    <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">News</h1>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <a href="{{ route('admin.news.create') }}"
                           class="button button--icon button--full-width button--center button--light">
                            <span class="fa fa-plus"></span> News hinzufügen
                        </a>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="input-group">
                        <a href="{{ route('admin.news.pos') }}"
                           class="button button--icon button--full-width button--center button--light">
                            News Positionen
                        </a>
                    </div>
                </div>

            </div>
            <table id="admin-table" data-order="[[ 1, &quot;desc&quot; ]]" class="admin__table"
                   data-token="{{ csrf_token() }}">
                <thead>
                    <tr>
                        <th style="width:15%">Galerie&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                        <th style="width:5%">ID&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                        <th style="width:35%">Titel&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                        <th style="width:10%">Autor&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                        <th style="width:15%">Erstellt&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                        <th style="width:10%">Publiziert</th>
                        <th style="width:10%">Optionen</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($news as $key => $newsItem)
                        <tr>
                            <td>@if($newsItem->has_gallery == 1)
                                    <span><i class="fa fa-newspaper-o" aria-hidden="true"></i></span>
                                @endif
                            </td>
                            <td>{{ $newsItem->id }}</td>
                            <td>{{ $newsItem->title }}</td>
                            <td>{{$newsItem->user['fullname'] != null || $newsItem->user['fullname'] != "" ? $newsItem->user['fullname'] : $newsItem->user['username'] }}</td>
                            <td>{{ date('Y/m/d', strtotime($newsItem->created_at)) }}</td>
                            <td>
                                <div class="onoffswitch">
                                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox"
                                           id="news-published-{{$key}}" {{ ($newsItem->state === News::STATE_PUBLISHED) ? 'checked' : '' }}>
                                    <label data-toggle-published data-news-id="{{ $newsItem->id }}"
                                           class="onoffswitch-label" for="news-published-{{$key}}"></label>
                                </div>
                            </td>

                            <td>
                                <div class="admin__table-option">
                                    <a href="{{ route('admin.news.edit', $newsItem) }}"><span class="fa fa-edit"></span></a>
                                    @if(is_null($newsItem->deleted_at) && Auth::user()->isAdmin())
                                        <a data-toggle-delete data-record-id="{{ $newsItem->id }}"
                                           data-delete-path="{{ route('admin.news.destroy') }}" href="#">
                                            <span class="fa fa-trash"></span>
                                        </a>
                                    @endif
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </section>
@endsection