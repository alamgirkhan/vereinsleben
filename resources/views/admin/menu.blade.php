{{--<ul class="admin__menu">--}}
{{--<li class="admin__menu-item{{ Request::is('admin/dashboard') ? ' active' : '' }}">--}}
{{--<a class="admin__menu-link" href="{{ url('admin/dashboard') }}">Dashboard</a>--}}
{{--</li>--}}
{{--<li class="admin__menu-item{{ Request::is('admin/slider') ? ' active' : '' }}">--}}
{{--<a class="admin__menu-link" href="{{ url('admin/slider') }}">Startseite</a>--}}
{{--</li>--}}
{{--<li class="admin__menu-item{{ Request::is('admin') ? ' active' : '' }}">--}}
{{--<a class="admin__menu-link" href="{{ route('admin.index') }}">News</a>--}}
{{--</li>--}}
{{--<li class="admin__menu-item{{ Request::is('admin/vote*') ? ' active' : '' }}">--}}
{{--<a class="admin__menu-link" href="{{ url('admin/vote') }}">Verein des Monats</a>--}}
{{--</li>--}}
{{--<li class="admin__menu-item{{ Request::is('admin/sports') ? ' active' : '' }}">--}}
{{--<a class="admin__menu-link" href="{{ url('admin/sports') }}">Sportarten</a>--}}
{{--</li>--}}
{{--<li class="admin__menu-item{{ Request::is('admin/clubs/list') ? ' active' : '' }}">--}}
{{--<a class="admin__menu-link" href="{{ url('admin/clubs/list') }}">Vereine</a>--}}
    {{--<li class="admin__menu-item{{ Request::is('admin/clubs') ? ' active' : '' }}">--}}
    {{--<a class="admin__menu-link" href="{{ url('admin/clubs') }}">Vereine</a>--}}
    {{--</li>--}}
{{--<li class="admin__menu-item{{ Request::is('admin/bandages') ? ' active' : '' }}">--}}
{{--<a class="admin__menu-link" href="{{ url('admin/bandage') }}">Verbände</a>--}}
{{--</li>--}}
{{--<li class="admin__menu-item{{ Request::is('admin/exports') ? ' active' : '' }}">--}}
{{--<a class="admin__menu-link" href="{{ url('admin/exports') }}">Exporte</a>--}}
{{--</li>--}}
{{--<li class="admin__menu-item{{ Request::is('admin/users') ? ' active' : '' }}">--}}
{{--<a class="admin__menu-link" href="{{ url('admin/users') }}">Mitglieder</a>--}}
{{--</li>--}}
{{--<li class="admin__menu-item{{ Request::is('admin/partner') ? ' active' : '' }}">--}}
{{--<a class="admin__menu-link" href="{{ url('admin/partner') }}">Partner</a>--}}
{{--</li>--}}
{{--<li class="admin__menu-item{{ Request::is('admin/content') ? ' active' : '' }}">--}}
{{--<a class="admin__menu-link" href="{{ url('admin/content') }}">Content</a>--}}
{{--</li>--}}
{{--</ul>--}}


{{--<div class="admin__menu">--}}
{{--<div class="adnavbar">--}}
{{--<div class="addropdown">--}}
{{--<button class="addropbtn">Dashboard--}}
{{--<i class="fa fa-caret-down"></i>--}}
{{--</button>--}}
{{--<div class="addropdown-content">--}}
{{--<a href="#">Userstatistik</a>--}}
{{--<a href="#">Vereinsstatistik</a>--}}
{{--</div>--}}
{{--</div>--}}

{{--<div class="addropdown">--}}
{{--<button class="addropbtn">Daten--}}
{{--<i class="fa fa-caret-down"></i>--}}
{{--</button>--}}
{{--<div class="addropdown-content">--}}
{{--<a href="#">User</a>--}}
{{--<a href="#">Vereine</a>--}}
{{--<a href="#">Verbände</a>--}}
{{--<a href="#">Sportarten</a>--}}
{{--</div>--}}
{{--</div>--}}

{{--<div class="addropdown">--}}
{{--<button class="addropbtn">Content--}}
{{--<i class="fa fa-caret-down"></i>--}}
{{--</button>--}}
{{--<div class="addropdown-content">--}}
{{--<a href="#">Artikel</a>--}}
{{--<a href="#">Bildergalerien</a>--}}
{{--<a href="#">Slider</a>--}}
{{--<a href="#">Partner</a>--}}
{{--<a href="#">Infoseiten</a>--}}
{{--</div>--}}
{{--</div>--}}

{{--<div class="addropdown">--}}
{{--<button class="addropbtn">Gewinnspiele--}}
{{--<i class="fa fa-caret-down"></i>--}}
{{--</button>--}}
{{--<div class="addropdown-content">--}}
{{--<a href="#">Verein des Monats</a>--}}
{{--<a href="#">Teambesprechungsraum</a>--}}
{{--</div>--}}
{{--</div>--}}

{{--<a href="#">Banner</a>--}}
{{--<a href="#">Events</a>--}}

{{--<div class="addropdown">--}}
{{--<button class="addropbtn">Spamschutz--}}
{{--<i class="fa fa-caret-down"></i>--}}
{{--</button>--}}
{{--<div class="addropdown-content">--}}
{{--<a href="#">Fake-User</a>--}}
{{--<a href="#">Fake-Domain</a>--}}
{{--</div>--}}
{{--</div>--}}

{{--</div>--}}
{{--</div>--}}


<adminnav class="admin__menu">
    <ul>
        <li><a><strong>Dashboard</strong></a>
            <ul>
                <li><a href="{{ url('admin/dashboard/user/index') }}">Userstatistik</a></li>
                <li><a href="{{ url('admin/dashboard/club/clubs') }}">Vereinsstatistik</a></li>
            </ul>
        </li>
        <li><a><strong>Daten</strong></a>
            <ul>
                <li><a href="{{ url('admin/users') }}">User</a></li>
                <li><a href="{{ url('admin/clubs/list') }}">Vereine</a></li>
                <li><a href="{{ url('admin/verbaende') }}">Verbände</a></li>
                <li><a href="{{ url('admin/sports') }}">Sportarten</a></li>
                <li><a href="{{ url('admin/exports') }}">Exporte</a></li>
                <li><a href="{{ url('admin/bewertungen/list/all') }}">Bewertungen</a></li>
            </ul>
        </li>
        <li><a><strong>Content</strong></a>
            <ul>
                <li><a href="{{ route('admin.index') }}">Artikel</a></li>
                <li><a href="{{ route('admin.index') }}">Bildergalerien</a></li>
                <li><a href="{{ url('admin/slider') }}">Slider</a></li>
                <li><a href="{{ url('admin/partner') }}">Partner</a></li>
                <li><a href="{{ url('admin/content') }}">Infoseiten</a></li>
                <li><a href="{{ url('admin/number-of-week') }}">Zahl der Woche</a></li>
                <li><a href="{{ url('admin/spitzensports') }}">Spitzensport</a></li>
            </ul>
        </li>
        <li><a><strong>Gewinnspiele</strong></a>
            <ul>
                <li><a href="{{ url('admin/vote') }}">Verein des Monats</a></li>
                <li><a href="{{ url('admin/contest') }}">Teambesprechungsraum</a></li>
                <li><a href="{{ route('admin.propose-song.list') }}">Vereinshit der Woche</a></li>
                <li><a href="{{ route('admin.western-rewards.list') }}">Best Western Gold</a></li>
            </ul>
        </li>
        <li><a href="{{ url('admin/campaigns') }}"><strong>Banner</strong></a></li>
        <li><a href="{{ url('admin/events/all') }}"><strong>Events</strong></a></li>
        <li><a><strong>Spamschutz</strong></a>
            <ul>
                <li><a href="{{ url('admin/fakeusers') }}">Fake-User</a></li>
                <li><a href="{{ url('admin/fakedomains') }}">Fake-Domain</a></li>
            </ul>
        </li>
    </ul>
</adminnav>
