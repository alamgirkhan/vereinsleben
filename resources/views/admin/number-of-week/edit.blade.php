@extends('layouts.master')

@section('js-additional-vendor')
    <script src="{{ asset(('js/vendor/ckeditor/ckeditor.js')) }}"></script>
@endsection

@section('content')
    <section class="admin__overview">
        <div class="container-flex">
            <a class="" href="{{ route('number-of-week.index') }}">
                <span class="fa fa-caret-left"></span> Zur Übersicht
            </a>

                @if ($numberOfWeek->id === null)
                    {!! Form::open(
                        [
                            'route' => 'number-of-week.store',
                            'enctype' => 'multipart/form-data',
                            'files' => true,
                            'method' => 'POST',
                        ]
                    )!!}
                @else
                    {!! Form::model(
                        $numberOfWeek,
                        [
                            'route' => ['number-of-week.update', $numberOfWeek->id],
                            'enctype' => 'multipart/form-data',
                            'files' => true,
                            'method' => 'PATCH',
                        ]
                    )!!}
                @endif

                @method(isset($numberOfWeek->id) ? 'PUT' : 'POST')
                <div class="row">
                    <div class="col-md-10 col-md-offset-2">
                        <h3>Bearbeitung - Zahl der Woche</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <label for="name">Zahl</label>
                                    {!! Form::text('number', isset($numberOfWeek->id) ? $numberOfWeek->number : null, ['class' => 'input', 'required' => 'required']) !!}
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="input-group">
                                    <label for="name">Kurzinfo</label>
                                    {!! Form::textarea('short_description', isset($numberOfWeek->id) ? $numberOfWeek->short_description : null, ['class' => 'input', 'required' => 'required']) !!}
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="input-group">
                                    <label for="name">Deckkraft (%)</label>
                                    {!! Form::number('opacity', isset($numberOfWeek->id) ? $numberOfWeek->opacity : null, ['class' => 'input', 'required' => 'required', 'max' => 100]) !!}
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="input-group">
                                    <label for="name">Erscheinungsdatum</label>
                                    {!! Form::text('published_from', isset($numberOfWeek->id) ?  date('d.m.Y H:i', strtotime($numberOfWeek->published_from)) : null, ['class' => 'input', 'id' => 'published_from', 'data-datetimepicker-enabled', 'required' => 'required', 'autocomplete' => 'off']) !!}
                                </div>
                            </div>

                            <div class="col-md-12">
                                {!! Form::label('image', 'Bild', ['class' => 'input-label']) !!}
                                <div data-upload-result>
                                    <div class="col-xs-12 hidden" data-new-image-text>
                                        <span class="input-label">Neu hinzugefügte Bilder</span>
                                    </div>
                                </div>
                                @if ($numberOfWeek->id !== null && $numberOfWeek->image !== null)
                                    <div class="row">
                                        <div class="col-md-12">
                                            <img style="max-width: 100%;" src="{{ asset('/uploaded/number-of-week/'.$numberOfWeek->image) }}">
                                        </div>
                                        <div class="col-md-12">
                                            <label>Anderes Bild hochladen</label>
                                        </div>
                                    </div>
                                @endif

                                <div data-upload-result>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div id="upload-result" class="thumbnail__list"></div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="input-group">
                                                <label for="main_image"
                                                       class="button button--grey button--center button--icon button--full-width">
                                                    <span class="fa fa-upload"></span>Bild hochladen</label>
                                                <input type="file" name="image" id="main_image" class="input-file">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="input-group">
                                    <label for="name">Unterzeile</label>
                                    {!! Form::textarea('sub_line', isset($numberOfWeek->id) ? $numberOfWeek->sub_line : null, ['class' => 'input']) !!}
                                </div>
                            </div>

                            <div class="col-md-12">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button class="button btn-submit button--icon button--full-width button--center button--light" type="submit">Speichern</button>
                            </div>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </section>
@endsection
@push('scripts')
    <script>
        $().ready(function () {
            $('#published_from').datetimepicker();

            // function preview_image(input, id)
            // {
            //     if (input.files && input.files[0]) {
            //         var reader = new FileReader();
            //
            //         reader.onload = function(e) {
            //             $('#output_image_' + id).attr('src', e.target.result);
            //         };
            //
            //         reader.readAsDataURL(input.files[0]);
            //     }
            // }
            // $('.input-file').change(function () {
            //     preview_image(this, $(this).attr('data-id'));
            // });
        })
    </script>
@endpush