@extends('layouts.master')

@section('content')
    @include('admin.menu')
    <section class="admin__overview">
        <div class="container">
            <div class="row admin__section">
                <div class="col-xs-12">
                    <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">Zahl der Woche</h1>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <a href="{{ route('number-of-week.create') }}"
                           class="button button--icon button--full-width button--center button--light">
                            <span class="fa fa-plus"></span> Neu anlegen
                        </a>
                    </div>
                </div>
            </div>
            <table id="admin-table" class="admin__table" data-token="{{ csrf_token() }}">
                <thead>
                <tr>
                    <th style="width:7%">ID&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:35%">Zahl&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:10%">Kurzinfo&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:15%">Startdatum<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:10%">Optionen</th>
                </tr>
                </thead>
                <tbody>
                @foreach($numbers as $number)
                    <tr>
                        <td>{{ $number->id }}</td>
                        <td>{{ $number->number }}</td>
                        <td>
                            {{ $number->short_description }}
                        </td>
                        <td>
                            {{ date('Y/m/d', strtotime($number->published_from)) }}
                        </td>
                        <td>
                            <div class="admin__table-option">
                                <a href="{{ route('number-of-week.edit', $number->id) }}">
                                    <span class="fa fa-edit"></span>
                                </a>
                                <form action="{{route('number-of-week.destroy', $number->id)}}" method="POST">
                                    @method('DELETE')
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button onclick="return confirm('Möchten Sie diese Bewertung wirklich löschen?')" type="submit" class="btn-del">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </section>
@endsection