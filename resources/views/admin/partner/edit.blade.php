@extends('layouts.master')

@section('js-additional-vendor')
    <script src="{{ asset(('js/vendor/ckeditor/ckeditor.js')) }}"></script>
@endsection

@section('content')

    <section class="admin__overview">
        <div class="container-flex">
            @if($errors->count() > 0)
                @foreach($errors->all() as $error)
                    <p>{{$error}}</p>
                @endforeach
            @endif
            <a class="" href="{{ route('admin.partner.index') }}">
                <span class="fa fa-caret-left"></span> Zur Übersicht
            </a>
            <div class="row">
                <div class="col-md-10 col-md-offset-2">
                    @if ($partner->id === null)
                        {!! Form::open(
                            [
                                'route' => 'admin.partner.store',
                                'enctype' => 'multipart/form-data',
                                'files' => true,
                                'method' => 'POST',
                            ]
                        )!!}
                    @else
                        {!! Form::model(
                            $partner,
                            [
                                'route' => ['admin.partner.update', $partner->id],
                                'enctype' => 'multipart/form-data',
                                'files' => true,
                                'method' => 'PATCH',
                            ]
                        )!!}
                    @endif

                    <div class="row">
                        <div class="col-md-12">
                            {!! Form::label('title', 'Titel', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('title', isset($partner->title) ? $partner->title : null, ['class' => 'input', 'placeholder' => 'Titel']) !!}
                            </div>
                        </div>

                        <div class="col-md-12">
                            {!! Form::label('subtitle', 'Untertitel', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('sub_title', isset($partner->sub_title) ? $partner->sub_title : null, ['class' => 'input', 'placeholder' => 'Untertitel']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('image', 'Haupt-Bild', ['class' => 'input-label']) !!}
                            <div data-upload-result>
                                <div class="col-xs-12 hidden" data-new-image-text>
                                    <span class="input-label">Neu hinzugefügte Bilder</span>
                                </div>
                            </div>
                            @if ($partner->id !== null && $partner->main_image->originalFilename() !== null)
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="{{ asset($partner->main_image->url('original')) }}"
                                           data-lightbox="main_image">
                                            <img src="{{ asset($partner->main_image->url('startpage')) }}">
                                        </a>
                                    </div>
                                    <div class="col-md-12">
                                        <label>Anderes Bild hochladen</label>
                                    </div>
                                </div>
                            @endif

                            <div data-upload-result>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div id="upload-result" class="thumbnail__list"></div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="input-group">
                                            <label for="main_image"
                                                   class="button button--grey button--center button--icon button--full-width">
                                                <span class="fa fa-upload"></span>Bild hochladen</label>
                                            <input type="file" name="main_image" id="main_image" class="input-file">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('image', 'Logo-Bild', ['class' => 'input-label']) !!}
                            <div data-upload-result>
                                <div class="col-xs-12 hidden" data-new-image-text>
                                    <span class="input-label">Neu hinzugefügte Bilder</span>
                                </div>
                            </div>
                            @if ($partner->id !== null && $partner->logo->originalFilename() !== null)
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="{{ asset($partner->logo->url('original')) }}"
                                           data-lightbox="logo">
                                            <img src="{{ asset($partner->logo->url('original')) }}">
                                        </a>
                                    </div>
                                    <div class="col-md-12">
                                        <label>Anderes Bild hochladen</label>
                                    </div>
                                </div>
                            @endif

                            <div data-upload-result>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div id="upload-result" class="thumbnail__list"></div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="input-group">
                                            <label for="image"
                                                   class="button button--grey button--center button--icon button--full-width">
                                                <span class="fa fa-upload"></span>Bild hochladen</label>
                                            <input type="file" name="logo" id="image" class="input-file">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            {!! Form::label('slug', 'Verzeichnis', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('slug', isset($partner->slug) ? $partner->slug : null, ['class' => 'input', 'placeholder' => 'Verzeichnis']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            {!! Form::label('pos', 'Position', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::select('pos', [
                                        '1' => 'Position 1',
                                        '2' => 'Position 2',
                                        '3' => 'Position 3',
                                        '4' => 'Position 4',
                                        '5' => 'Position 5',
                                        '6' => 'Position 6',
                                        '7' => 'Position 7',
                                        '8' => 'Position 8',
                                        '9' => 'Position 9',
                                        '10' => 'Position 10',
                                        '11' => 'Position 11',
                                        '12' => 'Position 12',
                                ], $partner->pos, ['class' => 'select']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            {!! Form::label('email', 'Email', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('email', isset($partner->email) ? $partner->email : null, ['class' => 'input', 'placeholder' => 'Email']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            {!! Form::label('phone', 'Telefon', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('phone', isset($partner->phone) ? $partner->phone : null, ['class' => 'input', 'placeholder' => 'Telefon']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            {!! Form::label('address', 'Anschrift', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('address', isset($partner->address) ? $partner->address : null, ['class' => 'input', 'placeholder' => 'Anschrift']) !!}
                            </div>
                        </div>

                        <div class="col-md-12">
                            {!! Form::label('website', 'Webseite', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('website', isset($partner->website) ? $partner->website : null, ['class' => 'input', 'placeholder' => 'Webseite']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            {!! Form::label('content_teaser', 'Text für Startseite', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::textarea('content_teaser', $partner->content_teaser, ['class' => 'input', 'rows' => 5, 'placeholder' => 'Text für Startseite']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            {!! Form::label('content', 'Text', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::textarea('content', $partner->content, ['class' => 'input ckeditor', 'rows' => 10, 'placeholder' => 'Text', 'required' => true]) !!}
                            </div>
                        </div>

                        <div class="col-md-12">
                            {!! Form::label('meta_bild_alt', 'meta-bild-alt', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('meta_bild_alt',
                                    ($partner->id == null || $partner->meta->meta_bild_alt == null) ? null : $partner->meta->meta_bild_alt ,
                                  ['class' => 'input', 'placeholder' => 'meta_bild_alt'])
                                !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h3>Meta-Angaben für Suchmaschine</h3>
                            {!! Form::label('meta_description', 'meta-description', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('meta_description',
                                    ($partner->id == null || $partner->meta->description == null) ? null : $partner->meta->description ,
                                  ['class' => 'input', 'placeholder' => 'meta-description'])
                                !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            {!! Form::label('meta_keywords', 'meta-keywords', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('meta_keywords',
                                    ($partner->id == null || $partner->meta->meta_keywords == null ) ? null : $partner->meta->meta_keywords,
                                    ['class' => 'input', 'placeholder' => 'meta-keywords'])
                                !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            {!! Form::label('meta_author', 'meta-author', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('meta_author',
                                    ($partner->id == null || $partner->meta->meta_author) ? 'vereinsleben.de' : $partner->meta->meta_author ,
                                    ['class' => 'input', 'placeholder' => 'meta-author'])
                                !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            {!! Form::label('meta_language', 'meta-language', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('meta_language',
                                    ($partner->id == null || $partner->meta->meta_language)? 'de' : $partner->meta->meta_language,
                                    ['class' => 'input', 'placeholder' => 'meta-language'])
                                !!}
                            </div>
                        </div>

                        <div class="col-md-12">
                            <h3>Bundesländer</h3>
                        </div>

                        <div class="col-md-12">
                            <div class="input-group">
                                {{ Form::checkbox('All', '1', '', ['id' => 'checkAll']) }}
                                {!! Form::label('Alle auswählen', '', ['class' => 'input-label']) !!}
                            </div>
                        </div>
                        @foreach($federal_states as $state)
                            <div class="col-md-3">
                                <div class="input-group">
                                    {{ Form::checkbox('federal_states[]',
                                     $state->id,
                                      in_array($state->id, $partner->states()->pluck('state_id')->all() ) ? true : false
                                    , ['id' => $state->constant]) }}
                                    {!! Form::label($state->constant, $state->name, ['class' => 'input-label']) !!}
                                </div>
                            </div>
                        @endforeach

                        <div class="col-md-12">
                            <div class="input-group">
                                <input {{ ($partner->published == 1) ? 'checked' : '' }}
                                       class="input-checkbox"
                                       type="checkbox"
                                       name="published"
                                       id="admin-partner-published" value="1">
                                <label class="input-checkbox__label"
                                       for="admin-partner-published">Veröffentlicht</label>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="input-group">
                                {!! Form::submit('Speichern', array('class' => 'button button--light button--light-white button--center button--full-width')) !!}
                            </div>
                        </div>
                    </div>

                    <input type="hidden" id="admin-partner-csrf" value="{{ csrf_token() }}">

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
@endsection