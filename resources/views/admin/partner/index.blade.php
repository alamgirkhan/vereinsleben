@extends('layouts.master')

@section('content')
    @include('admin.menu')
    <section class="admin__overview">
        <div class="container">
            <div class="row admin__section">
                <div class="col-xs-12">
                    <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">Partner</h1>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <a href="{{ route('admin.partner.add') }}"
                           class="button button--icon button--full-width button--center button--light">
                            <span class="fa fa-plus"></span> Partner hinzufügen
                        </a>
                    </div>
                </div>
            </div>
            <table id="admin-table" class="admin__table" data-token="{{ csrf_token() }}">
                <thead>
                <tr>
                    <th>ID&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th>Titel&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th>Bild</th>
                    <th>Publiziert</th>
                    <th>Pos</th>
                    <th>Optionen</th>
                </tr>
                </thead>
                <tbody>
                @foreach($partners as $key => $partner)
                    <tr>
                        <td>{{ $partner->id }}</td>
                        <td>{{ $partner->title }}</td>
                        <td><img width="100" src="{{ asset($partner->main_image->url('original')) }}"></td>
                        <td>@if($partner->published == 1) Publiziert @else Unpubliziert @endif</td>
                        <td>@isset($partner->pos) {{ $partner->pos }} @endisset</td>
                        <td>
                            <div class="admin__table-option">
                                <a href="{{ route('admin.partner.edit', $partner) }}" target="_blank"><span
                                            class="fa fa-edit"></span></a>
                                @if(is_null($partner->deleted_at))
                                    <a onclick="return confirm('Möchtest du diese Aufzeichnung wirklich löschen?')"
                                       href="{{ route('admin.partner.destroy', $partner->id) }}">
                                        <span class="fa fa-trash"></span>
                                    </a>
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </section>
@endsection