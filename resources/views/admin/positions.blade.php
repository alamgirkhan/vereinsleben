@extends('layouts.master')

@section('content')
    @include('admin.menu')
    <section class="admin__overview">
        <div class="container">
            <div class="row admin__section">
                <div class="col-xs-12">
                    <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">News Positionen</h1>
                </div>
            </div>
            <table id="news-pos-table" class="table table-striped table-bordered"
                   data-token="{{ csrf_token() }}">
                <thead>
                <tr>
                    <th style="width:15%">Pos&nbsp;</th>
                    <th style="width:5%">ID&nbsp;</th>
                    <th style="width:35%">Titel&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                {!! Form::open(['route' => 'admin.news.pos.save', 'method' => 'GET', 'id' => 'search', 'class' => '']) !!}

                @if(count($news) > 0)
                    @for($i = 1; $i < 9; $i++)
                        @php
                            $j = strval($i) . '.position';
                            $k = strval($i) . '.id';
                            $l = strval($i) . '.title';
                        @endphp
                        @if(array_get($news, $j) == $i)
                            <tr>
                                <td>
                                    {{ $i }}&nbsp;&nbsp;&nbsp;@if($i == 8) <u>nur</u> Bildergalerie @endif
                                </td>
                                <td>
                                    {{ Form::text('newsid_pos'.$i, array_get($news, $k), array('id' => array_get($news, $k))) }}
                                    {{ Form::hidden('oldnewsid_pos'.$i, array_get($news, $k), array('id' => array_get($news, $k))) }}
                                </td>
                                <td>
                                    {{ array_get($news, $l) }}
                                </td>
                            </tr>
                        @else
                            <tr>
                                <td>
                                    {{ $i }}&nbsp;&nbsp;&nbsp;@if($i == 8) <u>nur</u> Bildergalerie @endif
                                </td>
                                <td>
                                    {{ Form::text('newsid_pos'.$i, '') }}
                                    {{ Form::hidden('oldnewsid_pos'.$i, '') }}
                                </td>
                                <td>-</td>
                            </tr>
                        @endif
                    @endfor

                @else
                    @for($i = 1; $i < 9; $i++)
                        <tr>
                            <td>
                                {{ $i }}&nbsp;&nbsp;&nbsp;@if($i == 8) <u>nur</u> Bildergalerie @endif
                            </td>
                            <td>
                                {{ Form::text('newsid_pos'.$i, '') }}
                                {{ Form::hidden('oldnewsid_pos'.$i, '') }}
                            </td>
                            <td>-</td>
                        </tr>
                    @endfor
                @endif




                {{--@if(count($news) > 0)--}}
                {{--@for($i = 1; $i < 9; $i++)--}}
                {{--@php--}}
                {{--$chunk = $news->shift();--}}
                {{--@endphp--}}
                {{--@if($chunk['position'] == $i)--}}
                {{--<tr>--}}
                {{--<td>{{ $i }}</td>--}}
                {{--<td>--}}
                {{--{{ Form::text('newsid_pos'.$i, $chunk['id'], array('id' => $chunk['id'])) }}--}}
                {{--{{ Form::hidden('oldnewsid_pos'.$i, $chunk['id'], array('id' => $chunk['id'])) }}--}}
                {{--</td>--}}
                {{--<td>--}}
                {{--{{ $chunk['title'] }}--}}
                {{--</td>--}}
                {{--</tr>--}}
                {{--@else--}}
                {{--<tr>--}}
                {{--<td>{{ $i }}</td>--}}
                {{--<td>--}}
                {{--{{ Form::text('newsid_pos'.$i, '') }}--}}
                {{--</td>--}}
                {{--<td>-</td>--}}
                {{--</tr>--}}
                {{--@endif--}}
                {{--@endfor--}}

                {{--@else--}}
                {{--@for($i = 1; $i < 9; $i++)--}}
                {{--<tr>--}}
                {{--<td>{{ $i }}</td>--}}
                {{--<td>--}}
                {{--{{ Form::text('newsid_pos'.$i, '') }}--}}
                {{--</td>--}}
                {{--<td>-</td>--}}
                {{--</tr>--}}
                {{--@endfor--}}
                {{--@endif--}}
                </tbody>
            </table>
            <div class="row">
                <div class="col-md-3">
                    <button type="submit" name="Speichern" onclick="return confirm('Positionen speichern?')"
                            class="button button--light button--light-white button--center button--full-width">
                        Speichern
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </section>
@endsection