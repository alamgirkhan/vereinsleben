@extends('layouts.master')

@section('content')
    <section class="admin__overview">
        <div class="container-flex">
            <a class="" href="{{ route('admin.propose-song.list') }}">
                <span class="fa fa-caret-left"></span> Zur Übersicht
            </a>
            <div class="row">
                <div class="col-md-10 col-md-offset-2 propose-song">
                    <h3>Bearbeiten Empfohlene Lieder</h3>
                    <form method="POST" action="{{route('admin.propose-song.update')}}">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{$editProposeSong->id}}">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <input type="hidden" id="federal_state" value="">
                                    <label for="">Name des Vereins</label>
                                    <input value="{{isset($editProposeSong->club_name) ? $editProposeSong->club_name : null}}" class="input club-autocomplete"
                                           data-clubs-url="{{app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('api.club.autocomplete')}}" placeholder="Name des Vereins" required="" name="club_name" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <label for="">Titel des Songs</label>
                                    <input value="{{isset($editProposeSong->title_song) ? $editProposeSong->title_song : null}}" class="input" placeholder="Titel des Songs" required="required" name="title_song" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <label for="">Interpret des Songs</label>
                                    <input value="{{isset($editProposeSong->artist_name) ? $editProposeSong->artist_name : null}}" class="input" placeholder="Interpret des Songs" required="required" name="artist_name" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <label for="">Name</label>
                                    <input value="{{isset($editProposeSong->first_name) ? $editProposeSong->first_name : null}}" class="input" placeholder="Name" required="required" name="first_name" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <label for="">Vorname</label>
                                    <input value="{{isset($editProposeSong->last_name) ? $editProposeSong->last_name : null}}" class="input" placeholder="Vorname" required="required" name="last_name" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <label for="">E-Mail-Adresse</label>
                                    <input value="{{isset($editProposeSong->email) ? $editProposeSong->email : null}}" class="input" placeholder="E-Mail-Adresse" required="required" name="email" type="email">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <input class="button button--light button--light-white button--center button--full-width" type="submit" value="Aktualisieren">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection