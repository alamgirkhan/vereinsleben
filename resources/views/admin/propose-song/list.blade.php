@extends('layouts.master')

@section('content')
    @include('admin.menu')
    <section class="admin__overview">
        <div class="container">
            <div class="row admin__section">
                <div class="col-xs-12">
                    <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">Vereinshit der Woche</h1>
                </div>
            </div>
            <table id="admin-table-propose-song" class="admin__table" data-token="{{ csrf_token() }}">
                <thead>
                <tr>
                    <th style="width:10%">ID<i class="glyphicon glyphicon-sort"></i></th>
                    <th>Titel<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:15%">Interpret<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:15%">User<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:20%">Verein<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:15%">Erstellt<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:5%">Optionen</th>
                </tr>
                </thead>
                <tbody>
                    @foreach( $proposeSongList as $proposeSong )
                        <tr>
                            <td>{{$proposeSong->id}}</td>
                            <td>{{$proposeSong->title_song}}</td>
                            <td>{{$proposeSong->artist_name}}</td>
                            <td><a href="{{ route('user.detail', $proposeSong->username) }}"
                                  target="_blank">{{$proposeSong->first_name}} {{$proposeSong->last_name}}</a></td>
                            <td>
                                <a href="{{ route('club.detail', $proposeSong->club_slug) }}"
                                   target="_blank">{{ $proposeSong->club_name }}</a>
                            </td>
                            <td>
                                {{ date('Y/m/d', strtotime($proposeSong->created_at)) }}
                            </td>
                            <td>
                                <div class="admin__table-option">
                                    <a href="{{ route('admin.propose-song.view', $proposeSong->id) }}"><span class="fa fa-eye"></span></a>
                                    <a onclick="return confirm('Möchtest du diese Aufzeichnung wirklich löschen?')"
                                       href="{{ route('admin.propose-song.destroy', $proposeSong->id) }}">
                                        <span class="fa fa-trash"></span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </section>
    @push('scripts')
        <script>
            $(document).ready(function () {
                $('#admin-table-propose-song').DataTable({
                    dom         : 'Blfrtip',
                    "lengthMenu": [50, 100, 250],
                    buttons     : [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5'
                    ],
                    fixedHeader: true,
                    stateSave: true,
                    StateDuration: -1,
                    language: {
                        url: '/js/vendor/i18n/dataTables.german.json'
                    }
                }).on('page.dt', function() {
                    $('html, body').animate({
                        scrollTop: $(".dataTables_wrapper").offset().top
                    }, 'slow');

                    $('thead tr th:first-child').focus().blur();
                });
            })
        </script>

        <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    @endpush
@endsection