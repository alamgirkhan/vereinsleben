@extends('layouts.master')

@section('content')
    <section class="admin__overview">
        <div class="container-flex">
            <a class="" href="{{ route('admin.propose-song.list') }}">
                <span class="fa fa-caret-left"></span> Zur Übersicht
            </a>
            <div class="row">
                <div class="col-md-10 col-md-offset-2 propose-song">
                    <h3>Vorgeschlagene Lieddetails</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <label for="">Name des Vereins</label>
                                    <input disabled value="{{isset($proposeSong->club_name) ? $proposeSong->club_name : null}}" class="input">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <label for="">Titel</label>
                                    <input disabled value="{{isset($proposeSong->title_song) ? $proposeSong->title_song : null}}" class="input" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <label for="">Interpret</label>
                                    <input disabled value="{{isset($proposeSong->artist_name) ? $proposeSong->artist_name : null}}" class="input" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <label for="">Name</label>
                                    <input disabled value="{{isset($proposeSong->first_name) ? $proposeSong->first_name : null}}" class="input" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <label for="">Vorname</label>
                                    <input disabled value="{{isset($proposeSong->last_name) ? $proposeSong->last_name : null}}" class="input" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <label for="">E-Mail-Adresse</label>
                                    <input disabled value="{{isset($proposeSong->email) ? $proposeSong->email : null}}" class="input" type="email">
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </section>
@endsection