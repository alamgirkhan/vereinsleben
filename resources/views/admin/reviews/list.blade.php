@extends('layouts.master')

@section('content')
    @include('admin.menu')
    <section class="admin__overview">
        @foreach (['warning', 'error', 'success', 'info'] as $level)
            @if (Session::has('rating-'.$level))
                <div class="flash-message flash-message--{{$level}}">
                    @if(is_array(Session::get('rating-'.$level)))
                        @foreach(Session::get('rating-'.$level) as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    @else
                        {!! Session::get('rating-'.$level) !!}
                    @endif
                </div>
            @endif
        @endforeach
        <div class="container">
            <section class="admin__overview">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">Bewertungen Liste</h1>
                        </div>
                    </div>
                    <div class="sort-by">
                        <a href="{{ route('admin.reviews.list', 'all') }}"  class="{{ ($sort_by == 'all') ? 'activeSortBy' : '' }}">Alle</a>
                        <a href="{{ route('admin.reviews.list', 'report') }}" class="{{ ($sort_by == 'report') ? 'activeSortBy' : '' }}">Gemeldet</a>
                    </div>
                    <table id="review-table" class="admin__table" data-token="{{ csrf_token() }}">
                        <thead>
                        <tr>
                            <th>ID&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                            <th>Rating&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                            <th>User&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                            <th>Club&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                            <th>Comment&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                            <th style="width:10%">Publiziert</th>
                            <th>Optionen&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($reviews as $key => $review)
                            <tr>
                                <td>{{ $review->id }}</td>
                                <td>
                                    <span style="display: none">{{$review->rating}}</span>
                                    @for ($i=1; $i <= 5 ; $i++)
                                        <span class="fa fa-star{{ ($i <= $review->rating) ? '' : '-o'}}"></span>
                                    @endfor

                                </td>
                                <td>
                                    <a target="_blank" href="{{ route('user.detail', $review->user()->first()->username) }}">{{ $review->user()->first()->username }}</a>
                                </td>
                                <td>
                                    <a target="_blank" href="{{ route('club.detail', $review->club()->first()->slug) }}">{{ $review->club()->first()->name }}</a>
                                </td>
                                <td class="short-comment">
                                    <?php echo $review->comment ?>
                                </td>
                                <td>
                                    {{ date('Y/m/d', strtotime($review->created_at)) }}
                                </td>
                                <td>
                                    <div class="admin__table-option">
                                        <form action="{{route('reviews.destroy', $review->id)}}" method="POST">
                                            @method('DELETE')
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button onclick="return confirm('Möchten Sie diese Bewertung wirklich löschen?')" type="submit" class="btn-del">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </section>
    @push('scripts')
        <script>
            $(document).ready(function () {
                $('.short-comment').click(function () {
                    $('.short-comment').removeClass('active-comment');
                    $(this).addClass('active-comment');
                });
                $('#review-table').DataTable( {
                    "order": [[ 5, "desc" ]]
                } );
            });
        </script>
    @endpush
@endsection