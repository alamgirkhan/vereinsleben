@extends('layouts.master')

@section('js-additional-vendor')
    <script src="{{ asset(('js/vendor/ckeditor/ckeditor.js')) }}"></script>
@endsection
@section('content')

    <section class="admin__overview">
        <div class="container-flex">
            @if($errors->count() > 0)
                @foreach($errors->all() as $error)
                    <p>{{$error}}</p>
                @endforeach
            @endif
            <a class="" href="{{ route('admin.content.index') }}">
                <span class="fa fa-caret-left"></span> Zur Übersicht
            </a>
            <div class="row">
                <div class="col-md-10 col-md-offset-2">
                    @if ($content && $content->id)
                        {!! Form::model(
                            $content,
                            [
                                'route' => ['admin.content.update', $content && $content->id],
                                'enctype' => 'multipart/form-data',
                                'files' => true,
                                'method' => 'PATCH',
                            ]
                        )!!}
                    @else
                        {!! Form::open(
                            [
                                'route' => 'admin.content.store',
                                'enctype' => 'multipart/form-data',
                                'files' => true,
                                'method' => 'POST',
                            ]
                        )!!}
                    @endif

                    <div class="row">
                        <div class="col-md-6">
                            <h3>Imprint</h3>
                        </div>
                        <div class="col-md-12">
                            {!! Form::label('content_cont', 'Content', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::textarea('content', $content ? $content->content : '', ['class' => 'input', 'rows' => 10, 'placeholder' => 'content Description']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="input-group">
                                {!! Form::hidden('content_slug', 'imprint', ['class' => 'input']) !!}
                                {!! Form::submit('Save content', array('class' => 'button button--light button--light-white button--center button--full-width')) !!}
                            </div>
                        </div>
                    </div>

                    <input type="hidden" id="admin-news-csrf" value="{{ csrf_token() }}">

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
@endsection