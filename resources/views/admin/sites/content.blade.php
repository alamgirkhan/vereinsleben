@extends('layouts.master')

@section('content')
    @include('admin.menu')
    <section class="admin__overview">
        <div class="container">
            <div class="row admin__section">
                <div class="col-xs-12">
                    <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">Content</h1>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <a href="{{ route('admin.content.add') }}"
                           class="button button--icon button--full-width button--center button--light">
                            <span class="fa fa-plus"></span> Content hinzufügen
                        </a>
                    </div>
                </div>
            </div>
            <table id="admin-table" class="admin__table" data-token="{{ csrf_token() }}">
                <thead>
                <tr>
                    <th>ID&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th>Titel&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th>Slug&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($contents as $key => $content)
                    <tr>
                        <td>{{ $content->id }}</td>
                        <td>{{ $content->title }}</td>
                        <td>{{ $content->slug }}</td>
                        <td>
                            <div class="admin__table-option">
                                <a href="{{ route('admin.content.edit', $content->id) }}"><span
                                            class="fa fa-edit"></span></a>
                                <a onclick="return confirm('Möchtest du diese Aufzeichnung wirklich löschen?')"
                                   href="{{ route('admin.content.destroy', $content->id) }}">
                                    <span class="fa fa-trash"></span>
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </section>
@endsection