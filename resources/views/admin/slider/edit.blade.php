@extends('layouts.master')

@section('content')

    <section class="admin__overview">
        <div class="container-flex">
            @if($errors->count() > 0)
                @foreach($errors->all() as $error)
                    <p>{{$error}}</p>
                @endforeach
            @endif
            <div class="row">
                <div class="col-md-10 col-md-offset-2">
                    @if ($slider->id === null)
                        {!! Form::open(
                            [
                                'route' => 'admin.slider.store',
                                'enctype' => 'multipart/form-data',
                                'files' => true,
                                'method' => 'POST',
                            ]
                        )!!}
                    @else
                        {!! Form::model(
                            $slider,
                            [
                                'route' => ['admin.slider.update', $slider->id],
                                'enctype' => 'multipart/form-data',
                                'files' => true,
                                'method' => 'PATCH',
                            ]
                        )!!}
                    @endif

                    <div class="row">
                        <div class="col-md-6">
                            {!! Form::label('slider_title', 'Titel', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('slider_title', isset($slider->title) ? $slider->title : null, ['class' => 'input', 'placeholder' => 'Slider Title']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('position', 'Position auf der Startseite', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::select('position', [
                                        '1' => 'Position 1',
                                        '2' => 'Position 2',
                                        '3' => 'Position 3',
                                        '4' => 'Position 4',
                                        '5' => 'Position 5',
                                        '6' => 'Position 6',
                                        '7' => 'Position 7',
                                        '8' => 'Position 8',
                                        '9' => 'Position 9',
                                        '10' => 'Position 10',
                                ], $slider->position, ['class' => 'select']) !!}
                            </div>
                        </div>

                        {{--<div class="col-md-12">--}}
                        {{--{!! Form::label('slider_description', 'Beschreibung', ['class' => 'input-label']) !!}--}}
                        {{--<div class="input-group">--}}
                        {{--{!! Form::textarea('slider_description', $slider->description, ['class' => 'input', 'rows' => 10, 'placeholder' => 'Slider Description']) !!}--}}
                        {{--</div>--}}
                        {{--</div>--}}

                        {{--20-06-2018: new fields h2, h3, btn, partner images--}}
                        <div class="col-md-6">
                            {!! Form::label('slider_h2', 'H2', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('slider_h2', $slider->h2, ['class' => 'input', 'placeholder' => 'Slider H2']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('slider_h3', 'H3', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('slider_h3', $slider->h3, ['class' => 'input', 'placeholder' => 'Slider H3']) !!}
                            </div>
                        </div>

                        <div class="col-md-6">
                            {!! Form::label('slider_btn', 'Button', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('slider_btn', $slider->btn, ['class' => 'input', 'placeholder' => 'Slider Button Text']) !!}
                            </div>
                        </div>

                        <div class="col-md-6">
                            {!! Form::label('slider_btn_link', 'Link', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('slider_btn_link', $slider->link, ['class' => 'input', 'placeholder' => 'Slider Button Link']) !!}
                            </div>
                        </div>

                        {{--Logo1--}}
                        <div class="col-md-6">
                            @if ($slider->id !== null && $slider->logo1->originalFilename() !== null)
                                <div class="row">
                                    <div class="col-md-4">
                                        <img width="200" src="{{ asset($slider->logo1->url('original')) }}"
                                             alt="">
                                    </div>
                                    <div class="col-md-12">
                                        <label>Anderes Logo1 hochladen</label>
                                    </div>
                                </div>
                            @endif
                            <div data-upload-result>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div id="upload-result" class="thumbnail__list"></div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="input-group">
                                            <label for="logo1"
                                                   class="button button--grey button--center button--icon button--full-width">
                                                <span class="fa fa-upload"></span>Logo1 hochladen</label>
                                            <input type="file" name="slider_logo1" id="logo1" class="input-file">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{--Logo2--}}
                        <div class="col-md-6">
                            @if ($slider->id !== null && $slider->logo2->originalFilename() !== null)
                                <div class="row">
                                    <div class="col-md-4">
                                        <img width="200" src="{{ asset($slider->logo2->url('original')) }}"
                                             alt="">
                                    </div>
                                    <div class="col-md-12">
                                        <label>Anderes Logo2 hochladen</label>
                                    </div>
                                </div>
                            @endif
                            <div data-upload-result>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div id="upload-result" class="thumbnail__list"></div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="input-group">
                                            <label for="logo2"
                                                   class="button button--grey button--center button--icon button--full-width">
                                                <span class="fa fa-upload"></span>Logo2 hochladen</label>
                                            <input type="file" name="slider_logo2" id="logo2" class="input-file">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{--Logo3--}}
                        <div class="col-md-6">
                            @if ($slider->id !== null && $slider->logo3->originalFilename() !== null)
                                <div class="row">
                                    <div class="col-md-4">
                                        <img width="200" src="{{ asset($slider->logo3->url('original')) }}"
                                             alt="">
                                    </div>
                                    <div class="col-md-12">
                                        <label>Anderes Logo3 hochladen</label>
                                    </div>
                                </div>
                            @endif
                            <div data-upload-result>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div id="upload-result" class="thumbnail__list"></div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="input-group">
                                            <label for="logo3"
                                                   class="button button--grey button--center button--icon button--full-width">
                                                <span class="fa fa-upload"></span>Logo3 hochladen</label>
                                            <input type="file" name="slider_logo3" id="logo3" class="input-file">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{--Logo4--}}
                        <div class="col-md-6">
                            @if ($slider->id !== null && $slider->logo4->originalFilename() !== null)
                                <div class="row">
                                    <div class="col-md-4">
                                        <img width="200" src="{{ asset($slider->logo4->url('original')) }}"
                                             alt="">
                                    </div>
                                    <div class="col-md-12">
                                        <label>Anderes Logo4 hochladen</label>
                                    </div>
                                </div>
                            @endif
                            <div data-upload-result>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div id="upload-result" class="thumbnail__list"></div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="input-group">
                                            <label for="logo4"
                                                   class="button button--grey button--center button--icon button--full-width">
                                                <span class="fa fa-upload"></span>Logo4 hochladen</label>
                                            <input type="file" name="slider_logo4" id="logo4" class="input-file">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{--Logo5--}}
                        <div class="col-md-6">
                            @if ($slider->id !== null && $slider->logo5->originalFilename() !== null)
                                <div class="row">
                                    <div class="col-md-4">
                                        <img width="200" src="{{ asset($slider->logo5->url('original')) }}"
                                             alt="">
                                    </div>
                                    <div class="col-md-12">
                                        <label>Anderes Logo5 hochladen</label>
                                    </div>
                                </div>
                            @endif
                            <div data-upload-result>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div id="upload-result" class="thumbnail__list"></div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="input-group">
                                            <label for="logo5"
                                                   class="button button--grey button--center button--icon button--full-width">
                                                <span class="fa fa-upload"></span>Logo5 hochladen</label>
                                            <input type="file" name="slider_logo5" id="logo5" class="input-file">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{--Logo6--}}
                        <div class="col-md-6">
                            @if ($slider->id !== null && $slider->logo6->originalFilename() !== null)
                                <div class="row">
                                    <div class="col-md-4">
                                        <img width="200" src="{{ asset($slider->logo6->url('original')) }}"
                                             alt="">
                                    </div>
                                    <div class="col-md-12">
                                        <label>Anderes Logo6 hochladen</label>
                                    </div>
                                </div>
                            @endif
                            <div data-upload-result>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div id="upload-result" class="thumbnail__list"></div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="input-group">
                                            <label for="logo6"
                                                   class="button button--grey button--center button--icon button--full-width">
                                                <span class="fa fa-upload"></span>Logo6 hochladen</label>
                                            <input type="file" name="slider_logo6" id="logo6" class="input-file">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-12">
                            @if ($slider->id !== null && $slider->slider_image->originalFilename() !== null)
                                <div class="row">
                                    <div class="col-md-4">
                                        <img width="400" src="{{ asset($slider->slider_image->url('original')) }}"
                                             alt="">
                                    </div>
                                    <div class="col-md-12">
                                        <label>Anderes Bild hochladen</label>
                                    </div>
                                </div>
                            @endif
                            <div data-upload-result>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div id="upload-result" class="thumbnail__list"></div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="input-group">
                                            <label for="main_image"
                                                   class="button button--grey button--center button--icon button--full-width">
                                                <span class="fa fa-upload"></span>Bild hochladen</label>
                                            <input type="file" name="slider_image" id="main_image" class="input-file"
                                                   multiple>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">

                            <!--10-04-2018: Federal State -->
                            <div class="col-md-12">
                                <h3>Bundesländer</h3>
                            </div>

                            <div class="col-md-12">
                                <div class="input-group">
                                    {{ Form::checkbox('All', '1', '', ['id' => 'checkAll']) }}
                                    {!! Form::label('Alle auswählen', '', ['class' => 'input-label']) !!}
                                </div>
                            </div>
                            @foreach($federal_states as $state)
                                <div class="col-md-6">
                                    <div class="input-group">
                                        {{ Form::checkbox('federal_states[]',
                                         $state->id,
                                          in_array($state->id, $slider->states()->pluck('state_id')->all() ) ? true : false
                                        , ['id' => $state->constant]) }}
                                        {!! Form::label($state->constant, $state->name, ['class' => 'input-label']) !!}
                                    </div>
                                </div>
                            @endforeach

                        </div>


                        <div class="col-md-12">
                            <div class="input-group">
                                <input {{ ($slider->published == 1) ? 'checked' : '' }}
                                       class="input-checkbox"
                                       type="checkbox"
                                       name="published"
                                       id="admin-news-published" value="1">
                                <label class="input-checkbox__label"
                                       for="admin-news-published">Veröffentlicht</label>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="input-group">
                                {!! Form::submit('Slider speichern', array('class' => 'button button--light button--light-white button--center button--full-width')) !!}
                            </div>
                        </div>
                    </div>

                    <input type="hidden" id="admin-news-csrf" value="{{ csrf_token() }}">

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
@endsection