@extends('layouts.master')

@section('content')
    @include('admin.menu')
    <section class="admin__overview">
        <div class="container">
            <div class="row admin__section">
                <div class="col-xs-12">
                    <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">Slider</h1>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <a href="{{ route('admin.slider.add') }}"
                           class="button button--icon button--full-width button--center button--light">
                            <span class="fa fa-plus"></span> Slider hinzufügen
                        </a>
                    </div>
                </div>
            </div>
            <table id="admin-table" class="admin__table" data-token="{{ csrf_token() }}">
                <thead>
                <tr>
                    <th>ID&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th>Titel&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th>Image</th>
                    <th>Pos&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th>Veröffentlicht</th>
                    <th>Optionen</th>
                </tr>
                </thead>
                <tbody>
                @foreach($sliders as $key => $sliderItem)
                    <tr>
                        <td>{{ $sliderItem->id }}</td>
                        <td>{{ $sliderItem->title }}</td>
                        <td><img width="100" src="{{ asset($sliderItem->slider_image->url('original')) }}" alt=""></td>
                        <td>{{ $sliderItem->position }}</td>
                        <td>
                            @if($sliderItem->published == 1)
                                <i class="fa fa-circle" style="color: green;"></i>
                            @else
                                <i class="fa fa-circle" style="color: red;"></i>
                            @endif
                        </td>
                        <td>
                            <div class="admin__table-option">
                                <a href="{{ route('admin.slider.edit', $sliderItem->id) }}" target="_blank"><span
                                            class="fa fa-edit"></span></a>
                                <a onclick="return confirm('Möchtest du diese Aufzeichnung wirklich löschen?')"
                                   href="{{ route('admin.slider.destroy', $sliderItem->id) }}">
                                    <span class="fa fa-trash"></span>
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </section>
@endsection