@extends('layouts.master')

@section('content')

    @if(Auth::user()->isAdmin())
        @include('admin.menu')
    @endif
    <?php 
        use Vereinsleben\Spitzensport;
    ?>
    <section class="admin__overview">
        <div class="container">
            <div class="row admin__section">
                <div class="col-xs-12">
                    <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">Spitzensports</h1>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <a href="{{ url('admin/spitzensports/create') }}"
                           class="button button--icon button--full-width button--center button--light">
                            <span class="fa fa-plus"></span> Spitzensport hinzufügen
                        </a>
                    </div>
                </div>

                {{-- removed second link from here --}}

            </div>
            <table id="admin-table" data-order="[[ 1, &quot;desc&quot; ]]" class="admin__table"
                   data-token="{{ csrf_token() }}">
                <thead>
                    <tr>
                        {{-- <th style="width:15%">Galerie&nbsp;<i class="glyphicon glyphicon-sort"></i></th> --}}
                        <th style="width:5%">ID&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                        <th style="width:35%">Full Name&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                        <th style="width:10%">Autor&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                        <th style="width:15%">Erstellt&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                        <th style="width:10%">Publiziert</th>
                        <th style="width:10%">Optionen</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($spitzensports as $key => $spitzensportItem)
                        <tr>
                            {{-- <td>
                                <img src="{{asset($spitzensportItem->main_image->url('startpage'))}}" class="w-100" />
                            </td> --}}
                            <td>{{ $spitzensportItem->id }}</td>
                            <td>{{ $spitzensportItem->full_name }}</td>
                            <td>{{$spitzensportItem->user['fullname'] != null || $spitzensportItem->user['fullname'] != "" ? $spitzensportItem->user['fullname'] : $spitzensportItem->user['username'] }}</td>
                            <td>{{ date('Y/m/d', strtotime($spitzensportItem->created_at)) }}</td>
                            
                            <td>
                                <div class="onoffswitch">
                                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox"
                                           id="spitzensport-published-{{$key}}" {{ ($spitzensportItem->state === Spitzensport::STATE_PUBLISHED) ? 'checked' : '' }}>
                                    <label data-toggle-published-spitzensport data-spitzensport-id="{{ $spitzensportItem->id }}"
                                           class="onoffswitch-label" for="spitzensport-published-{{$key}}"></label>
                                </div>
                            </td>

                            <td>
                                <div class="admin__table-option">
                                    <a href="{{ route('spitzensports.edit', $spitzensportItem) }}"><span class="fa fa-edit"></span></a>
                                    @if(is_null($spitzensportItem->deleted_at) && Auth::user()->isAdmin())
                                        <a data-toggle-delete data-record-id="{{ $spitzensportItem->id }}"
                                           data-delete-path="{{ route('admin.spitzensport.destroy') }}" href="#">
                                            <span class="fa fa-trash"></span>
                                        </a>
                                    @endif
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </section>
@endsection