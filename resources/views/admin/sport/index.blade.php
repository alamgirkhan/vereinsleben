@extends('layouts.master')

@section('content')
    @include('admin.menu')
    <section class="admin__overview" data-token="{{ csrf_token() }}"
             data-mapping-url="{{ app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('api.sports') }}">
        <div class="container">

            <div class="row">
                <div class="col-xs-12">
                    <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">Sportarten</h1>
                </div>
            </div>

            <ul class="club-detail__tabbable-menu" data-tabbable-menu="0">

                <li class="club-detail__tab club-detail__tab--space active">
                    <a class="club-detail__tab-link" href="#new-unowned">Neu</a>
                </li>

                <li class="club-detail__tab club-detail__tab--space">
                    <a class="club-detail__tab-link" href="#existing-unowned">Bestehend</a>
                </li>

            </ul>
            <br><br>
            <div class="row">
                {!! Form::open(['route' => 'admin.sportart.add', 'method' => 'GET', 'id' => 'search', 'class' => '']) !!}
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="input-group">
                            {!! Form::text('newsportart',  null, ['id' => 'newsportart', 'autofocus', 'class' => 'input', 'placeholder' => 'Sportart']) !!}
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="input-group">
                            {!! Form::submit('Hinzufügen', array('class' => 'button button--light button--light-white button--center button--full-width')) !!}
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>

            <div class="row admin__section">
                <div class="col-xs-12">

                    <div id="new-unowned" class="tabbable-menu__page tabbable-menu__page--active">
                        <table data-page-length='100' class="admin__table"
                               data-sport-table="{{ route('admin.sport.list', ['type' => 'new']) }}">
                            <thead>
                                <tr>
                                    <th>Id&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                                    <th>Name&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                                    <th>Mapping</th>
                                    <th>Optionen</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>

                    <div id="existing-unowned" class="tabbable-menu__page tabbable-menu__page--active">
                        <table data-page-length='100' class="admin__table"
                               data-sport-table="{{ route('admin.sport.list', ['type' => 'existing']) }}">
                            <thead>
                                <tr>
                                    <th>Id&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                                    <th>Name&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                                    <th>Mapping</th>
                                    <th>Optionen</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </section>

    @push('scripts')
    <script src="{{asset('js/admin.js')}}"></script>
    @endpush
@endsection