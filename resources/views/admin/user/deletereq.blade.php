@extends('layouts.master')

@section('content')
    @include('admin.menu')
    <section class="admin__overview">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">Benutzeranfrage löschen</h1>
                </div>
            </div>
            <table id="admin-table" class="admin__table" data-token="{{ csrf_token() }}">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Username</th>
                    <th>Vorname Nachname</th>
                    <th>Email</th>
                    <th>Optionen</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td><a href="{{route('user.detail', ['username' => $user->username])}}"
                               target="_blank">
                                {{ $user->username}}</a></td>
                        <td>{{ $user->fullname }}</td>
                        <td>{{ $user->email }}</td>
                        <td>
                            <div class="admin__table-option">
                                <a onclick="return confirm('Soll der User wirklich gelöscht werden?')" data-toggle="tooltip" title="Akzeptieren"
                                   href="{{ route('admin.user.delete.accept', $user) }}"><span
                                            class="fa fa-trash"></span></a>

                                <a onclick="return confirm('Soll die Löschung wirklich abgelehnt werden?')" data-toggle="tooltip" title="Verweigern"
                                   href="{{ route('admin.user.delete.deny', $user) }}">
                                    <span class="fa fa-times"></span>
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </section>

    @push('scripts')
        <script>

        </script>
    @endpush
@endsection

