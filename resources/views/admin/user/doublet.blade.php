@extends('layouts.master')

@section('content')
    <section class="admin__overview">
        <div class="container">
            <div class="col-xs-12">
                <div class="col-md-12">
                    <h1 class="h1 h1--upper h1-alternate-ft h1--large-alternate">Folgende Benutzer wurden als Dublette identifiziert (Gesamt: {{ count($doublet_list) }}):</h1>
                </div>
                <div class="col-md-12">
                    <table>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Benutzer</th>
                                <th>E-Mail</th>
                                <th>Registriert am</th>
                            </tr>
                        </thead>
                        <tbody>
                    @foreach( $doublet_list as $user )
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->username }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->created_at }}</td>
                        </tr>
                    @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection