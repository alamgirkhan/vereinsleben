@extends('layouts.master')

@section('content')
    @include('admin.menu')
    <section class="admin__overview">
        <div class="container-flex">
            @if($errors->count() > 0)
                @foreach($errors->all() as $error)
                    <p>{{$error}}</p>
                @endforeach
            @endif

            <div class="row">
                <div class="col-md-10 col-md-offset-2">
                    {!! Form::model(
                        $user,
                        [
                            'route' => ['admin.user.update', $user->id],
                            'enctype' => 'multipart/form-data',
                            'files' => true,
                            'method' => 'PATCH',
                        ]
                    )!!}
                    <div class="row">
                        <div class="col-md-12">
                            {!! Form::label('gender', 'Anrede', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::select('gender', ['' => 'Bitte wählen', 'male' => 'Herr', 'female' => 'Frau'], $user->gender, ['class' => 'select']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            {!! Form::label('role_id', 'Role', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::select('role_id', $roles->pluck('name', 'id'),$user->role == null ? '' : $user->role->id, [
                                    'class' => 'select',
                                    'title' => 'Mitglieder'
                                    ])
                                !!}
                            </div>
                        </div>

                        <!--06-04-2018: Email soll änderbar sein-->
                        <div class="col-md-12">
                            <div class="row">
                                <div class="input-group">
                                    <div class="col-md-6">
                                        {!! Form::label('email', 'E-Mail', ['class' => 'input-label']) !!}
                                        {!! Form::text('email', $user->email, ['required', 'class' => 'input', 'placeholder' => 'E-Mail', 'name' => 'new_email']) !!}
                                    </div>
                                    <div class="col-md-6">
                                        {!! Form::label('&nbsp;', '&nbsp;', ['class' => 'input-label']) !!}
                                        @if($user->verified)
                                            {!! Form::submit('Bestätigungsmail senden', array('class' => 'button button--light button--light-white button--center button--full-width', 'name' => 'action')) !!}
                                        @else
                                            {!! Form::submit('Registrierungsmail senden', array('class' => 'button button--light button--light-white button--center button--full-width', 'name' => 'action')) !!}
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="row">
                                <div class="input-group"><br>
                                    <div class="col-md-6">
                                        {!! Form::label('firstname', 'Vorname', ['class' => 'input-label']) !!}
                                        {!! Form::text('firstname', $user->firstname, ['class' => 'input', 'placeholder' => 'Vorname']) !!}
                                    </div>
                                    <div class="col-md-6">
                                        {!! Form::label('lastname', 'Nachname', ['class' => 'input-label']) !!}
                                        {!! Form::text('lastname', $user->lastname, ['class' => 'input', 'placeholder' => 'Nachname']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12"><br>
                            {!! Form::label('birthday', 'Geburtsdatum', ['class' => 'input-label']) !!}
                            <div class="row">
                                <div class="input-group geburtsdatum">
                                    <div class="col-md-4">
                                        {!! Form::select('birthday-day',  array('' => 'Tag') + array_combine(range(1, 31), range(1, 31)), isset($user->birthday->day) ? $user->birthday->day : null, ['class' => 'select']) !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::select('birthday-month',  array('' => 'Monat') + array_combine(range(1, 12), range(1, 12)), isset($user->birthday->month) ? $user->birthday->month : null, ['class' => 'select']) !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::select('birthday-year',  array('' => 'Jahr') + array_combine(range(1900, date('Y')), range(1900, date('Y'))), isset($user->birthday->year) ? $user->birthday->year : null, ['class' => 'select']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8"><br>
                            {!! Form::label('street', 'Straße', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('street', $user->street, ['class' => 'input', 'placeholder' => 'Straße']) !!}
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><br>
                            {!! Form::label('street', 'Hausnummer', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::text('house_number', $user->house_number, ['class' => 'input', 'placeholder' => 'Hausnummer']) !!}
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="input-group">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        {!! Form::label('zip', 'Postleitzahl', ['class' => 'input-label']) !!}
                                        {!! Form::text('zip', $user->zip, ['class' => 'input', 'placeholder' => 'Postleitzahl']) !!}
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        {!! Form::label('city', 'Stadt', ['class' => 'input-label']) !!}
                                        {!! Form::text('city', $user->city, ['class' => 'input', 'placeholder' => 'Ort']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><br>
                            {!! Form::label('federal_state', 'Bundesland', ['class' => 'input-label']) !!}
                            <div class="input-group">
                                {!! Form::select('federal_state', array('' => 'Bitte wählen')+$federal_states->pluck('name', 'name')->all(), $user->federal_state, ['class' => 'select', 'id' => 'state']) !!}
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="input-group nwsltr">
                                {!! Form::checkbox('verified', 1, ($user->verified), ['id' => 'verified', 'class' => 'input-checkbox']) !!}
                                {!! Form::label('verified', 'verified', ['class' => 'input-checkbox__label']) !!}
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="input-group">
                                {!! Form::submit('Speichern', array('class' => 'button button--light button--light-white button--center button--full-width', 'name' => 'action')) !!}
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="admin__overview">
        <div class="container-flex">
            <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">Vereine</h1>
            <div class="row">

                {!! Form::open(['route' => 'admin.user.club.add', 'method' => 'GET', 'id' => 'search', 'class' => '']) !!}
                <div class="col-md-3">
                    <div class="input-group">
                        {!! Form::text('clubid', isset($query['clubid']) ? $query['clubid'] : null, ['id' => 'clubid', 'autofocus', 'class' => 'input', 'placeholder' => 'Club-ID']) !!}
                    </div>
                </div>
                {{ Form::hidden('userid', $user->id, array('id' => $user->id)) }}
                <div class="col-md-3">
                    <div class="input-group">
                        {!! Form::submit('Verein hinzufügen', array('class' => 'button button--light button--light-white button--center button--full-width')) !!}
                    </div>
                </div>
                {!! Form::close() !!}

            </div>
            <div class="row">
                @if(count($clubs) > 0)
                    <table id="admin-club-admins" class="admin__table" data-token="{{ csrf_token() }}">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th></th>
                            <th>Optionen</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($clubs as $club)
                            <tr>
                                <td><a href="{{route('club.detail', ['slug' => $club->slug])}}"
                                       target="_blank">{{ $club->id }}</a>
                                </td>
                                <td>
                                    {{ $club->name}}
                                </td>
                                <td>{{ $club->email }}</td>
                                <td>
                                    <div class="admin__table-option">
                                        <a onclick="return confirm('Adminbeziehung zum Verein wirklich löschen?')"
                                           href="{{ route('admin.user.club.delete', ['userid' => $user->id, 'id' => $club->id])}}">
                                            <span class="fa fa-trash"></span>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </section>
    @push('scripts')
        <script>
            $(document).ready(function () {
                $('#admin-club-admins').DataTable({
                    dom         : 'Blfrtip',
                    "lengthMenu": [50, 100, 250],
                    buttons     : [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5'
                    ],

                    language: {
                        url: '/js/vendor/i18n/dataTables.german.json'
                    }
                });
            })
        </script>

        <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    @endpush
@endsection