@extends('layouts.master')

@section('content')
    <section class="admin__overview">
        <div class="container">
            <div class="col-xs-12">
                <div class="col-md-12">
                    <h1 class="h1 h1--upper h1-alternate-ft h1--large-alternate">Folgende Benutzer wurden als falsch identifiziert (Gesamt: {{ count($fake_user_list) }}):</h1>
                    <p>Dateiname: {{ $fake_file }}{{ ($file_exists ? '(vorhanden)' : '(nicht vorhanden)') }}</p>
                </div>
                <div class="col-md-12">
                    <table>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Benutzer</th>
                                <th>E-Mail</th>
                                <th>Registriert am</th>
                            </tr>
                        </thead>
                        <tbody>
                    @foreach( $fake_user_list as $user )
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->username }} ({{ $vote_count[$user->id] }})</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->created_at }}</td>
                        </tr>
                    @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection