@extends('layouts.master')

@section('content')
    @include('admin.menu')
    <section class="admin__overview">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">Mitglieder</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    {!! Form::open(['route' => 'admin.users', 'method' => 'GET', 'id' => 'search', 'class' => '']) !!}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                {!! Form::text('keyword', isset($query['keyword']) ? $query['keyword'] : null, ['id' => 'keyword', 'autofocus', 'class' => 'input', 'placeholder' => 'STICHWORT']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                {!! Form::text('email', isset($query['email']) ? $query['email'] : null, ['id' => 'email', 'autofocus', 'class' => 'input', 'placeholder' => 'E-Mail']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                {!! Form::text('city', isset($query['city']) ? $query['city'] : null, ['id' => 'city', 'autofocus', 'class' => 'input', 'placeholder' => 'ORT/Bundesland']) !!}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                {!! Form::text('von', isset($query['von']) ? $query['von'] : null, ['id' => 'von', 'autofocus', 'class' => 'input', 'placeholder' => 'PLZ von']) !!}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                {!! Form::text('bis', isset($query['bis']) ? $query['bis'] : null, ['id' => 'bis', 'autofocus', 'class' => 'input', 'placeholder' => 'PLZ bis']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                {!! Form::select('date_filter', [
                                        '' => 'Datumsoption',
                                        'created_at' => 'Registriert',
                                        'login_at' => 'Eingeloggt',
                                        'updated_at' => 'Profil aktualisiert',
                                ], isset($query['date_filter']), ['class' => 'select']) !!}
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="input-group">
                                {!! Form::text('anfang', isset($query['anfang']) ? date('Y-m-d h:i:s',strtotime($query['anfang'])) : null, ['id' => 'anfang', 'autofocus', 'class' => 'input', 'placeholder' => 'Anfangsdatum', 'data-datetimepicker-enabled']) !!}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                {!! Form::text('ende', isset($query['ende']) ? date('Y-m-d h:i:s',strtotime($query['ende'])) : null, ['id' => 'ende', 'autofocus', 'class' => 'input', 'placeholder' => 'Enddatum', 'data-datetimepicker-enabled']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group">
                                {!! Form::text('vorname', isset($query['vorname']) ? $query['vorname'] : null, ['id' => 'vorname', 'autofocus', 'class' => 'input', 'placeholder' => 'Vorname']) !!}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                {!! Form::text('nachname', isset($query['nachname']) ? $query['nachname'] : null, ['id' => 'nachname', 'autofocus', 'class' => 'input', 'placeholder' => 'Nachname']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            {{--{!! Form::label('sport', 'Nach Sportart filtern', ['class' => 'input-label']) !!}--}}
                            <div class="input-group">
                                {!! Form::text('sport', Request::get('sport'), ['class' => 'input sport-autocomplete', 'data-sports-url' => app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('api.sports'), 'placeholder' => 'SPORTART']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group">
                                {!! Form::submit('Finden', array('class' => 'button button--light button--light-white button--center button--full-width')) !!}
                            </div>
                        </div>
                        <div class="col-md-3">
							<span class="undo"><a href="{{ url('admin/users') }}">Zur&uuml;cksetzen <i
                                            class="fa fa-undo"></i></a></span>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <a href="{{ url('admin/users/deletereq') }}" style="text-decoration: none;">
                                    {!! Form::button('zu löschende Benutzer anzeigen', array('class' => 'button button--light button--light-white button--center button--full-width')) !!}
                                </a>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="input-group">
                                <a href="{{ url('admin/users/export-data') }}" style="text-decoration: none;">

                                    <button id="exportdata"
                                            class="button button--light button--light-white button--center button--full-width">
                                        Export
                                    </button>
                                </a>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="input-group">
                                @if($exp_user)
                                    <a href="{{route('admin.user.download')}}"
                                       target="_blank"
                                       class="button button--light button--light-white button--center button--full-width"
                                       style="background-color: red;">
                                        Download
                                    </a>
                                @else
                                    <a href="#"
                                       target="_blank"
                                       class="button button--light button--light-white button--center button--full-width">
                                        Download
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

            @if($users)
                <table id="admin-user" class="admin__table" data-token="{{ csrf_token() }}">
                <thead>
                <tr>
                    <th style="width:10%;">ID&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:5%;">Userstatus</th>
                    <th style="width:10%;">Vereinsadmin</th>
                    <th style="width:30%;">Vorname Nachname&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:35%;">Email&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:10%;">Optionen</th>
                </tr>
                </thead>
                <tbody>
                {{--@php--}}
                {{--$i = 0;--}}
                {{--@endphp--}}
                @foreach($users as $user)
                    {{--@php--}}
                    {{--$i++;--}}
                    {{--@endphp--}}
                    <tr>
                        <td onclick='window.open("{{ route('user.detail', ['username' => $user->username]) }}")'
                            style="cursor: pointer;">
                            <u>{{ $user->id }}</u>
                        </td>
                        <td><!-- STATUS -->
                            @if($user->verified == 0)

                                <i class="fa fa-star-o" aria-hidden="true"></i>

                            @elseif($user->verified == 1 && $user->firstname == '' && $user->lastname == '' &&
                            (is_null($user->federal_state) || $user->federal_state == '') && (is_null($user->birthday) || $user->birthday == '')
                            && ($user->zip == '' || is_null($user->zip)) && ($user->city == '' || is_null($user->city)))

                                <i class="fa fa-star-half-o" aria-hidden="true"></i>

                            @elseif($user->verified == 1 &&
                            ($user->firstname == '' || is_null($user->firstname)
                            || $user->lastname == '' || is_null($user->lastname)
                            || is_null($user->federal_state) || $user->federal_state == ''
                            || is_null($user->birthday) || $user->birthday == ''
                            || $user->zip == '' || is_null($user->zip)
                            || $user->city == '' || is_null($user->city)))

                                <i class="fa fa-star" aria-hidden="true"></i>

                            @else
                                <i class="fa fa-star" aria-hidden="true" style="color:green;"></i>

                            @endif
                        </td>
                        <td>
                            @if($user->clubsWhereAdmin()->count() > 0)
                                <i class="fa fa-check-square" aria-hidden="true" style="color:green;"></i>
                            @endif
                        </td>
                        <td>{{ $user->firstname }}&nbsp;{{ $user->lastname }}</td>
                        <td>{{ $user->email }}</td>
                        <td>
                            <div class="admin__table-option">
                                <a href="{{ route('admin.user.edit', $user) }}" target="_blank"><span
                                            class="fa fa-edit"></span></a>
                                @if(is_null($user->deleted_at))
                                    {{--<a data-toggle-delete data-record-id="{{ $user->id }}"--}}
                                       {{--data-delete-path="{{ route('admin.user.destroy') }}" href="#">--}}
                                    <form action="{{route('admin.user.destroy')}}" method="POST">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="id" value="{{$user->id}}">
                                        <button type="submit" class="btn-del">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </form>
                                    </a>
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @endif
        </div>
    </section>
    @push('scripts')
        <script>
            var tab;
            $(document).ready(function () {
                tab = $('#admin-user').DataTable({
                    dom         : 'Blfrtip',
                    "lengthMenu": [50, 100, 250],
                    buttons     : [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5'
                    ],

                    language: {
                        url: '/js/vendor/i18n/dataTables.german.json'
                    }
                });

                $('#exportdata').click(function (event) {

                    // event.preventDefault();

                    var tmp = $.makeArray(tab.data().map(function (item) {
                        return item[0]
                    }));

                    var ids = $.makeArray(tmp);
                    ids = JSON.stringify(ids);


                    $.ajax("/admin/users/exportdata", {
                        method : "POST",
                        data   : {
                            ids: ids
                        },
                        success: function () {
                            location.reload(true);
                        }

                    });
                });

            })
        </script>

        <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
        <script src="http://legacy.datatables.net/release-datatables/extras/TableTools/media/js/TableTools.js"></script>


    @endpush
@endsection


