@extends('layouts.master')

@section('content')
    @include('admin.menu')
    <section class="admin__overview">
        <div class="container">
            <div class="col-xs-12">
                <div class="col-md-12">
                    <h1 class="h1 h1--upper h1-alternate-ft h1--large-alternate">Folgende Angaben sind gegeben:</h1>
                </div>
                <div class="col-md-12">
                    @if( isset($user) )
                        <h1>Der Benutzer wurde erfolgreich verifiziert:</h1>
                        <p>ID: {{ $user->id }}</p>
                        <p>Name: {{ $user->fullname }}</p>
                        <p>E-Mail: {{ $user->email }}</p>
                        <p>Verifiziert: {{ $user->verified }}</p>
                    @else
                        <h1>Der Benutzer konnte nicht gefunden werden.</h1>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection