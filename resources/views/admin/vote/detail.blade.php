@extends('layouts.master')

@section('content')
    {{--@include('components.headerimage-still',
    ['condensed' => true,
     'content' => [
            'icon' =>'edit',
            'headline' => 'Verein des Monats',
    ]])--}}
    @include('admin.menu')
    <section class="admin__overview">
        <div class="container">

            <div class="row">
                <div class="col-xs-12">
                    <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">Verein des Monats</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="row">

                        <div class="col-xs-12">
                            <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate hl--small hl--badged">
                                Optionen</h1>
                        </div>

                        <div class="col-xs-12">
                            <ul class="admin__menu admin__menu--aligned" data-tabbable-menu="0">
                                <li class="admin__menu-item active">
                                    <a class="admin__menu-link admin__menu-link--small"
                                       href="#active-vote">Abstimmung</a>
                                </li>

                                <li class="admin__menu-item">
                                    <a class="admin__menu-link admin__menu-link--small"
                                       href="#pre-vote">Bewerbungsphase</a>
                                </li>
                                <li class="admin__menu-item">
                                    <a class="admin__menu-link admin__menu-link--small"
                                       href="#vote-winner">Gewinner</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    {!! Form::model(
                        isset($vote) ? $vote : null, [
                            'route' => (isset($vote) && is_int($vote->id) ) ? ['admin.vote.update', $vote->id] : 'admin.vote.store',
                            'enctype' => 'multipart/form-data',
                            'files' => true,
                            'method' => (isset($vote) && is_int($vote->id) ) ? 'PATCH' : 'POST',
                        ]
                    )!!}
                    <div class="row admin__section">
                        <div class="col-xs-12">
                            <div id="active-vote" class="tabbable-menu__page tabbable-menu__page--active">

                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        {!! Form::label('period_start', 'Beginn', ['class' => 'input-label']) !!}
                                        <div class="input-group">
                                            {!! Form::text('period_start', null, ['class' => 'input', 'placeholder' => date('Y-m-d')]) !!}
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-md-6">
                                        {!! Form::label('period_end', 'Ende', ['class' => 'input-label']) !!}
                                        <div class="input-group">
                                            {!! Form::text('period_end', null, ['class' => 'input', 'placeholder' => date('Y-m-d')]) !!}
                                        </div>
                                    </div>

                                    <div class="col-xs-12">
                                        {!! Form::label('name', 'Titel der Abstimmung', ['class' => 'input-label']) !!}
                                        <div class="input-group">
                                            {!! Form::text('name', ($vote->name ? $vote->name : ($vote->hasPreviousVote() ? $vote->getPreviousVote()->name : null)), ['class' => 'input', 'placeholder' => 'Titel']) !!}
                                        </div>
                                    </div>

                                    <div class="col-xs-12">
                                        {!! Form::label('description', 'Inhalt der Abstimmung', ['class' => 'input-label']) !!}
                                        <div class="input-group">
                                            {!! Form::textarea('description', ($vote->description ? $vote->description : ($vote->hasPreviousVote() ? $vote->getPreviousVote()->description : null)), ['class' => 'input', 'rows' => 10, 'placeholder' => 'Text', 'required' => true]) !!}
                                        </div>
                                    </div>

                                    <div class="col-xs-12">
                                        {!! Form::label('teaser_headline', 'Anreißer Überschrift', ['class' => 'input-label']) !!}
                                        <div class="input-group">
                                            {!! Form::text('teaser_headline', ($vote->teaser_headline ? $vote->teaser_headline : ($vote->hasPreviousVote() ? $vote->getPreviousVote()->teaser_headline : null)), ['class' => 'input', 'placeholder' => 'Überschrift']) !!}
                                        </div>
                                    </div>

                                    <div class="col-xs-12">
                                        {!! Form::label('teaser', 'Anreißer Inhalt', ['class' => 'input-label']) !!}
                                        <div class="input-group">
                                            {!! Form::textarea('teaser', ($vote->teaser ? $vote->teaser : ($vote->hasPreviousVote() ? $vote->getPreviousVote()->teaser : null)), ['class' => 'input', 'rows' => 10, 'placeholder' => 'Text', 'required' => true]) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="pre-vote" class="tabbable-menu__page">
                                <div class="row">
                                    <div class="col-xs-12">
                                        {!! Form::label('pre_name', 'Titel der Bewerbungsphase', ['class' => 'input-label']) !!}
                                        <div class="input-group">
                                            {!! Form::text('pre_name', ($vote->pre_name ? $vote->pre_name : ($vote->hasPreviousVote() ? $vote->getPreviousVote()->pre_name : null)), ['class' => 'input', 'placeholder' => 'Titel']) !!}
                                        </div>
                                    </div>

                                    <div class="col-xs-12">
                                        {!! Form::label('pre_description', 'Inhalt der Bewerbungsphase', ['class' => 'input-label']) !!}
                                        <div class="input-group">
                                            {!! Form::textarea('pre_description', ($vote->pre_description ? $vote->pre_description : ($vote->hasPreviousVote() ? $vote->getPreviousVote()->pre_description : null)), ['class' => 'input', 'rows' => 10, 'placeholder' => 'Text', 'required' => true]) !!}
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div id="vote-winner" class="tabbable-menu__page">
                                <div class="row">
                                    <div class="col-xs-12">
                                        {!! Form::label('winner_title', 'Titel', ['class' => 'input-label']) !!}
                                        <div class="input-group">
                                            {!! Form::text('winner_title', ($vote->winner_title ? $vote->winner_title : ($vote->hasPreviousVote() ? $vote->getPreviousVote()->winner_title : null)), ['class' => 'input', 'placeholder' => 'Titel']) !!}
                                        </div>
                                    </div>

                                    <div class="col-xs-12">
                                        {!! Form::label('winner_image', 'Bild', ['class' => 'input-label']) !!}
                                        @if ($vote->id !== null && $vote->winner_image->originalFilename() !== null)
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <img class="admin__image--preview" src="{{ asset($vote->winner_image->url('singleview')) }}" alt="Gewinner-Bild-Vorschau">
                                                </div>
                                                <div class="col-md-12">
                                                    <label>Anderes Bild hochladen</label>
                                                </div>
                                            </div>
                                        @endif

                                        <div data-upload-result>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div id="upload-result" class="thumbnail__list"></div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="input-group">
                                                        <label for="winner_image"
                                                               class="button button--grey button--center button--icon button--full-width">
                                                            <span class="fa fa-upload"></span>Bild hochladen</label>
                                                        <input type="file" name="winner_image" id="winner_image" class="input-file" data-previewable-image-upload>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12">
                                        {!! Form::label('winner_description', 'Beschreibung', ['class' => 'input-label']) !!}
                                        <div class="input-group">
                                            {!! Form::textarea('winner_description', ($vote->winner_description ? $vote->winner_description : ($vote->hasPreviousVote() ? $vote->getPreviousVote()->winner_description : null)), ['class' => 'input', 'rows' => 10]) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-md-6">
                    <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate hl--small hl--badged">
                        Teilnehmer</h1>
                    @if(isset($vote) && !is_null($vote->voteNominees))
                        @foreach($vote->voteNominees as $nominee)
                            <div class="admin__list-item" data-winnter="{{$nominee->winner}}">
                                <a href="{{ route('admin.vote.{vote_id}.nominee.{nominee_id}.winner', [$vote->id, $nominee->id]) }}"
                                   class="admin__link--icon admin__link--{{ ($nominee->winner) ? 'active' : 'inactive' }} pull-left">
                                    <span class="fa fa-trophy"></span>
                                </a>
                                {{ str_limit($nominee->voteable->name, 35) }} ({{ $nominee->count_voted }} / {{ $nominee->count_cleared }})
                                <a href="{{route('admin.add.votes.{vote_id}.nominee.{nominee_id}.votes.{votes}', [$vote->id, $nominee->id, 200])}}">
                                    +200 Votes
                                </a>

                                <a href="{{ route('admin.vote.{vote_id}.nominee.{nominee_id}.delete', [$vote->id, $nominee->id]) }}"
                                class="admin__link--icon pull-right">
                                    <span class="fa fa-trash"></span>
                                </a>
                                <a href="{{ route('admin.vote.{vote_id}.nominee.show', [$vote->id, $nominee->id]) }}"
                                   class="admin__link--icon pull-right">
                                    <span class="fa fa-edit"></span>
                                </a>
                            </div>
                        @endforeach
                    @endif
                    @if(isset($vote->id))
                        <a href="{{ route('admin.vote.{vote_id}.nominee.create', $vote->id) }}"
                           class="btn btn--default btn--large btn--spaced"><span class="fa fa-plus"></span>
                            Teilnehmer hinzufügen</a>
                    @else
                        <br><br>
                        <div style="color: red">
                            Bitte die Abstimmung erst speichern, um Teilnehmer hinzufügen zu können!
                        </div>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    {!! Form::submit('Speichern', array('class' => 'btn btn--large btn--default btn--spaced btn--full-width')) !!}
                </div>
            </div>

            {!! Form::close() !!}
        </div>
    </section>

    @push('scripts')
    <script src="{{ asset('js/admin.js') }}"></script>
    <script src="{{ asset(('js/vendor/ckeditor/ckeditor.js')) }}"></script>

    <script>
        CKEDITOR.replace('description');
        CKEDITOR.replace('pre_description');
        CKEDITOR.replace('teaser');
        CKEDITOR.replace('winner_description');
    </script>
    @endpush
@endsection