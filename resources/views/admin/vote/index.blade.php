@extends('layouts.master')

@section('content')
    @include('admin.menu')
    <section class="admin__overview">
        <div class="container">

            <div class="row">
                <div class="col-xs-12">
                    <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">Verein des Monats</h1>
                </div>
            </div>

            <table id="votes-table" class="admin__table">
                <thead>
                    <tr>
                        <th>ID&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                        <th>Titel&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                        <th>Beginn&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                        <th>Ende&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                        <th>Optionen</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($votes as $vote)
                        <tr>
                            <td>{{ $vote->id }}</td>
                            <td>{{ $vote->name }}</td>
                            <td>{{ date('d.m.Y', strtotime($vote->period_start)) }}</td>
                            <td>{{ date('d.m.Y', strtotime($vote->period_end)) }}</td>
                            <td>
                                <div class="admin__table-option">
                                    <a href="{{ url('admin/vote', $vote->id) }}"><span
                                                class="fa fa-edit"></span></a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            
            <div class="row">
                <div class="col-xs-12">
                    <a href="{{ url('admin/vote/create') }}" class="btn btn--large btn--default btn--full-width btn--spaced"><span class="fa fa-plus"></span> Neue Abstimmung</a>
                </div>
            </div>
        </div>
    </section>

    @push('scripts')
    <script src="{{asset('js/admin.js')}}"></script>
    @endpush
@endsection