@extends('layouts.master')

@section('content')
    {{--@include('components.headerimage-still',
    ['condensed' => true,
     'content' => [
            'icon' =>'edit',
            'headline' => 'VdM: Nominierter Verein',
    ]])--}}
    @include('admin.menu')
    <section class="admin__overview" data-token="{{ csrf_token() }}"
             data-mapping-url="{{ app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('api.club.list') }}">
        <div class="container">
            {!! Form::model(
                isset($nominee) ? $nominee : null, [
                    'route' => isset($nominee) ? ['admin.vote.{vote_id}.nominee.update', $vote->id, $nominee->id] : ['admin.vote.{vote_id}.nominee.store', $vote->id],
                    'enctype' => 'multipart/form-data',
                    'files' => true,
                    'method' => isset($nominee) ? 'PATCH' : 'POST',
                ]
            )!!}
            <div class="row">
                <div class="col-xs-12">
                    {!! Form::label('project_clubname', 'Club', ['class' => 'input-label']) !!}
                    @if(isset($nominee))
                        <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">{{ $nominee->voteable->name }}</h1>
                    @else
                        <select id="club_id"
                                       name="club_id"
                                       data-club-mapping>
                        </select>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    {!! Form::label('project', 'Projektname', ['class' => 'input-label']) !!}
                    <div class="input-group">
                        {!! Form::text('project', null, ['class' => 'input', 'placeholder' => 'Titel']) !!}
                    </div>
                </div>
                <div class="col-xs-12">
                    {!! Form::label('project_description', 'Projektbeschreibung', ['class' => 'input-label']) !!}
                    <div class="input-group">
                        {!! Form::textarea('project_description', null, ['class' => 'input', 'rows' => 10, 'placeholder' => 'Text', 'required' => true]) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    {!! Form::submit('Speichern', array('class' => 'btn btn--large btn--default btn--spaced btn--full-width')) !!}
                </div>
            </div>

            {!! Form::close() !!}
        </div>
    </section>

    @push('scripts')
    <script src="{{asset('js/admin.js')}}"></script>
    <script src="{{ asset(('js/vendor/ckeditor/ckeditor.js')) }}"></script>

    <script>
        CKEDITOR.replace('project_description');
    </script>
    @endpush
@endsection