@extends('layouts.master')

@section('content')
    @include('admin.menu')
    <section class="admin__overview">
        <div class="container">
            <div class="row admin__section">
                <div class="col-xs-12">
                    <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">Best Western Gold</h1>
                </div>
            </div>
            <table id="admin-table-western_reward" class="admin__table" data-token="{{ csrf_token() }}">
                <thead>
                <tr>
                    <th>ID<i class="glyphicon glyphicon-sort"></i></th>
                    <th>User ID<i class="glyphicon glyphicon-sort"></i></th>
                    <th>User<i class="glyphicon glyphicon-sort"></i></th>
                    <th>Erstellt<i class="glyphicon glyphicon-sort"></i></th>
                </tr>
                </thead>
                <tbody>
                    @foreach( $western_rewards as $wr )
                        <tr>
                            <td>{{$wr->id}}</td>
                            <td><a href="{{ route('user.detail', $wr->username) }}"
                                   target="_blank">{{$wr->user_id}}</a></td>
                            <td><a href="{{ route('user.detail', $wr->username) }}"
                                  target="_blank">{{$wr->firstname}} {{$wr->lastname}}</a></td>
                            <td>
                                {{ date('d.m.Y - h:i:s', strtotime($wr->created_at)) }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </section>
    @push('scripts')
        <script>
            $(document).ready(function () {
                $('#admin-table-western_reward').DataTable({
                    dom         : 'Blfrtip',
                    "lengthMenu": [50, 100, 250],
                    buttons     : [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5'
                    ],
                    fixedHeader: true,
                    stateSave: true,
                    StateDuration: -1,
                    language: {
                        url: '/js/vendor/i18n/dataTables.german.json'
                    }
                }).on('page.dt', function() {
                    $('html, body').animate({
                        scrollTop: $(".dataTables_wrapper").offset().top
                    }, 'slow');

                    $('thead tr th:first-child').focus().blur();
                });
            })
        </script>

        <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    @endpush
@endsection