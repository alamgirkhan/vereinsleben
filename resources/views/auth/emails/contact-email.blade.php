<!DOCTYPE html>
<html lang="de-DE">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Bestätigen</h2>

<div>
    <strong>Hallo Admin,</strong>
    <br><br>
    Du hast eine Nachricht von erhalten : {{ $name }}
    <br>
    <p>
        Firma: {{ $firma }}
    </p>
    <p>
        Name: {{ $name }}
    </p>
    <p>
        Vorname: {{ $vorname }}
    </p>
    <p>
        Strasse: {{ $strasse }}
    </p>
    <p>
        Ort: {{ $ort }}
    </p>
    <p>
        PLZ: {{ $plz }}
    </p>
    <p>
        Bundesland: {{ $bundesland }}
    </p>
    <p>
        Telefon: {{ $telefon }}
    </p>
    <p>
        Email: {{ $email }}
    </p>
    <p>
        Bitte kontaktieren Sie mich telefonisch: {{ $bksmt }}
    </p>
    <p>
        Anmerkungen: {{ $user_message }}
    </p>
    <br>
    Dein VEREINSLEBEN.DE–Team
</div>
@if(isset($imprint))
    <br/>
    <br/>
    <br/>
    <div>{!! $imprint->content !!}</div>
@endif
</body>
</html>