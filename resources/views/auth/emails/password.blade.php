<?php
$imprint = \Vereinsleben\Content::select('content')->where('slug', 'imprint')->first();
?>
<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <p>
            Bitte hier klicken um Passwort zu erneuern: <br>
            <a href="{{ $link = url('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}"> {{ $link }} </a>
        </p>
        
        <p>
            Liebe Grüße <br>
            Dein VEREINSLEBEN.DE-Team
        </p>
        @if(isset($imprint))
            <br/>
            <br/>
            <br/>
            <div>{!! $imprint->content !!}</div>
        @endif
    </body>
</html>