<!DOCTYPE html>
<html lang="de-DE">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Registrierung Bestätigen</h2>

        <div>
            <p>
                <strong>Hallo {{ $user->firstname }},</strong><br>
            </p>

            <p>
                vielen Dank für deine Registrierung bei vereinsleben.de.<br>
                Bitte klicke auf den unten stehenden Link, um dein Profil zu aktivieren. <br>
                Nach Bestätigen dieses Links und somit der Richtigkeit deiner Daten ist deine Registrierung
                abgeschlossen.
            </p>

            <p>
                Viel Glück und viel Erfolg bei der großen VEREINSLEBEN.DE-Mitgliederkampagne.
            </p>

            <p>
                <a href="{{ URL::to('register/verify/' . $user->register_token) }}">Bitte hier klicken um die
                    Registrierung und damit die automatische Teilnahme am Gewinnspiel abzuschließen.</a>
            </p>

            <p>
                 Wir wünschen dir viel Spaß auf Deutschlands größter Vereins-Plattform im Sport. 
            </p>

            <p>
                Dein VEREINSLEBEN.DE–Team
            </p>

            <a href="{{ URL::to('impressum') }}">Impressum</a>
        </div>

    </body>
</html>