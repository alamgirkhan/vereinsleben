<!DOCTYPE html>
<html lang="de-DE">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Nur noch einen Klick …</h2>
        <div>
            Klicke den folgenden Link, um die Richtigkeit Deiner E-Mail-Adresse {{ $user->email }} zu bestätigen
            <br><br>
            <a href="{{ URL::to('register/verify/' . $user->register_token) }}">{{ URL::to('register/verify/' . $user->register_token) }}</a>
            <br><br>
            Solltest Du den Bestätigungslink nicht anklicken können, kopiere einfach den Link und füge ihn in die
            Adresszeile Deines Browsers ein.
            <br><br>
            Dein vereinsleben.de-Team
            <br><br>
            Hinweis: Solltest Du nicht der richtige Adressat dieser E-Mail sein und kein Interesse an einem
            vereinsleben.de-Profil haben, so siehe diese E-Mail als gegenstandslos an. Sofern Du den in dieser E-Mail
            angezeigte Link nicht bestätigst, wird das Profil nicht aktiviert.
            <br><br><br>
            @if(isset($imprint))
                <div>{!! $imprint->content !!}</div>
            @endif
        </div>
    </body>
</html>