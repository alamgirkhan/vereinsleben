<!DOCTYPE html>
<html lang="de-DE">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Bestätigen</h2>

<div>
    <strong>Hallo {{ $user->firstname }},</strong>
    <br><br>
    Du erhälst diese eMail, weil wir eine Anfrage zur Änderung deiner Email-Addresse
    erhalten haben.
    <br>

    <a href="{{ URL::to('user/email/verify/' . $user->change_email_token) }}">Bitte hier klicken um die Email-Addresse zu bestätigen</a>
    <br><br>
    Wir wünschen dir viel Spaß auf Deutschlands größter Vereins-Plattform im Sport.<br>
    Dein VEREINSLEBEN.DE–Team
    @if(isset($imprint))
        <br/>
        <br/>
        <br/>
        <div>{!! $imprint->content !!}</div>
    @endif
</div>
</body>
</html>