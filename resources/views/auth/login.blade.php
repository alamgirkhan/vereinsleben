@extends('layouts.master')

@section('content')
    @include('components.slider',
    ['condensed' => true,
     'content' => [
            'icon' =>'sign-in',
            'headline' => 'Anmelden',
            'caption' => 'und sofort loslegen ...'
    ]])
    <section class="auth">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <form action="{{ url('/login') }}" method="post" class="mainrgstr">
                        <div class="row">

                            <div class="col-md-12">
                                <label class="input-label" for="login">E-Mail-Adresse</label>
                                <div class="input-group">
                                    <span class="fa fa-sign-in input-icon"></span>
                                    <input placeholder="E-Mail-Adresse" class="input" type="text"
                                           name="email" id="email" value="{{ old('email') }}">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <label class="input-label" for="password">Passwort</label>
                                <div class="input-group">
                                    <span class="fa fa-lock input-icon"></span>
                                    <input placeholder="Passwort" class="input" type="password" name="password"
                                           id="password">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <a class="form-link auth__forgot-password" href="{{ url('/password/reset') }}">Passwort
                                    vergessen?</a>
                            </div>

                            <div class="col-md-12">
                                <div class="input-group">
                                    <input class="input-checkbox" type="checkbox" name="remember" id="remember"
                                           value="1" checked="checked">
                                    <label class="input-checkbox__label" for="remember">Angemeldet bleiben</label>
                                    <input type="hidden" id="prevurl" name="prevurl" value="{{ url()->previous() }}">
                                </div>
                            </div>

                            {{csrf_field()}}

                            <div class="col-md-12">
                                <button type="submit"
                                        class="button button--center button--light button--light-white button--full-width button--icon">
                                    <span class="fa fa-sign-in"></span> Anmelden
                                </button>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection