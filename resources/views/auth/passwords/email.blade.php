@extends('layouts.master')

@section('content')
    @include('components.slider',
    ['condensed' => true,
     'content' => [
            'icon' =>'sign-in',
            'headline' => 'Anmelden',
            'caption' => 'und sofort loslegen ...'
    ]])

    <section class="auth">
        <div class="container">
            @if (Session::has('status'))
                <div class="alert alert-info">{{ Session::get('status') }}</div>
            @else
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <form action="{{ url('/password/email') }}" method="post" class="mainrgstr">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="input-label" for="email">E-Mail</label>
                                    <div class="input-group">
                                        <span class="fa fa-sign-in input-icon"></span>
                                        <input placeholder="E-Mail" class="input" type="text"
                                               name="email" id="email">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button type="submit"
                                            class="button button--center button--light button--light-white button--full-width button--icon">
                                        <i class="fa fa-btn fa-envelope"></i> Neues Passwort anfordern
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            @endif
        </div>
    </section>
@endsection
