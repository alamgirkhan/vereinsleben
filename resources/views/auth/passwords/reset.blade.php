@extends('layouts.master')

@section('content')
    @include('components.slider',
    ['condensed' => true,
     'content' => [
            'icon' =>'refresh',
            'headline' => 'Passwort zurücksetzen'
    ]])
    <section class="auth">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <form class="form-horizontal mainrgstr" role="form" method="POST"
                          action="{{ url('/password/reset') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="row">
                            <div class="col-xs-12">
                                <label class="input-label" for="email">E-Mail-Adresse</label>
                                <div class="input-group">
                                    <span class="fa fa-sign-in input-icon"></span>
                                    <input placeholder="E-Mail-Adresse" class="input" type="text"
                                           name="email" id="email" value="{{ $email or old('email') }}">
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <label class="input-label" for="password">Neues Passwort</label>
                                <div class="input-group">
                                    <span class="fa fa-lock input-icon"></span>
                                    <input id="password" type="password" class="input" name="password">
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <label class="input-label" for="password-confirm">Neues Passwort bestätigen</label>
                                <div class="input-group">
                                    <span class="fa fa-lock input-icon"></span>
                                    <input id="password-confirm" type="password" class="input" name="password_confirmation">
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <button type="submit" class="button button--center button--light button--light-white button--full-width button--icon">
                                    <span class="fa fa-refresh"></span> Passwort ändern
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
