<div data-edit-wrapper>
    <div class="row">
        @if(Auth::check() && Auth::user()->isBandageAdmin($bandage))
            <div class="col-xs-11 edit-del-btn">
                <div class="club-content__options">
                    <a href="#" class="club-content__options-button" title="Bearbeiten"
                       data-edit='{ "bandage_id": "{{ $bandage->id }}",
                       "resource": { "id": "{{ $contact->id }}",
                       "type": "event",
                       "url": "{{ route('contact-person.edit', [$bandage->id, $contact->id]) }}"
                       } }'>
                        <span class="fa fa-pencil club-content__options-button-edit"></span>
                    </a>
                    <a href="#" class="club-content__options-button club-content__options-button--hidden"
                       title="Bearbeiten abbrechen"
                       data-cancel='{ "bandage_id": "{{ $bandage->id }}",
                       "resource": { "id": "{{ $contact->id }}",
                       "type": "event",
                       "url": "{{ route('bandage.contact-person.single', [$bandage->slug, $contact->id]) }}"
                       } }'>
                        <span class="fa fa-times club-content__options-button-cancel"></span>
                    </a>
                    <a href="#" class="club-content__options-button" title="Löschen"
                       data-delete='{ "bandage_id": "{{ $bandage->id }}",
                       "resource": { "id": "{{ $contact->id }}",
                       "type": "event",
                       "url": "{{ route('contact-person.destroy', $contact->id) }}"
                       } }'>
                        <span class="fa fa-trash club-content__options-button-remove"></span>
                    </a>
                </div>
            </div>
        @endif
    </div>

    <div data-edit-content>
        <div class="shaddow-box">
            <ul data-edit-wrapper>
                <li>
                    <h3>{{ $contact->department }}</h3>
                </li>
                @if($contact->name !== null && trim($contact->name) !== "")
                    <li><span class="icon-area"><i
                              class="fa fa-user"
                              aria-hidden="true"></i></span>
                        <span class="txt-area">
                             @if($contact->gender !== null && trim($contact->gender) !== "")
                                {{ $contact->gender }}
                            @endif
                            {{ $contact->name }}
                        </span>
                    </li>
                @endif
                @if($contact->position != '')
                    <li><span style="visibility: hidden" class="icon-area"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                        <span class="txt-area">{{ $contact->position }}</span>
                    </li>
                @endif
                @if($contact->phone !== null && trim($contact->phone) !== "")
                    <li><span class="icon-area"><i
                              class="fa fa-phone"
                              aria-hidden="true"></i></span>
                        <span class="txt-area"><a href="tel:{{ $contact->phone }}"></a>{{ $contact->phone }}</span>
                    </li>
                @endif
                @if($contact->email !== null && trim($contact->email) !== "")
                    <li><span class="icon-area"><i
                              class="fa fa-envelope"
                              aria-hidden="true"></i></span>
                        <span class="txt-area"><a
                              href="mailto:{{ $contact->email }}">{{ $contact->email }}</a></span>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</div>


