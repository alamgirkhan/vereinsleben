{!! Form::model(
    $bandage,
    array(
        'method' => 'patch',
        'route' => ['bandage.update.public', $bandage->slug, '#informationen'],
        'class' => 'club-edit__form',
        'files' => 'true'
    )
)!!}
<div class="club-content__box">
    <h2 class="club-content__box-headline">Über diesen Verband</h2>
    <div class="input-group">
        {!! Form::textarea('about', null, ['class' => 'input', 'placeholder' => 'Informationen zu deinem Verband']) !!}
    </div>

    <h2 class="club-content__box-headline">Geschichte</h2>
    <div class="input-group">
        {!! Form::textarea('description', null, ['class' => 'input', 'placeholder' => 'Geschichte zu deinem Verband']) !!}
    </div>

    <div class="club-content__form-action-wrapper">
        <a href="#"
           class="club-content__form-cancel-action-button button button--grey button--center button--full-width">
            Abbrechen
        </a>
        {!! Form::submit('Speichern',
            array(
                'class' => 'button--save club-content__form-save-action-button button button--edit button--center button--full-width'
            ))
        !!}
    </div>
</div>
{!! Form::close() !!}