@if ($bandage->id === null)
    {!! Form::model(
        $bandage,
        array(
            'method' => 'post',
            'route' => 'bandage.store',
            'class' => 'club-edit__form',
            'files' => 'true'
        )
    )!!}
@else
    {!! Form::model(
        $bandage,
        array(
            'method' => 'patch',
            'route' => ['bandage.update.public', $bandage->slug, '#informationen'],
            'class' => 'club-edit__form',
            'files' => 'true'
        )
    )!!}
@endif
<div class="row">
    <div class="col-md-12">
        {!! Form::label('name', 'Verbandname *', ['class' => 'input-label']) !!}
        <div class="input-group">
            {!! Form::text('name', null, [
                    'required',
                    'class' => 'input',
                    'title' => 'Verbandname'
                ])
            !!}
        </div>
    </div>


    <div class="col-md-6">
        {!! Form::label('type', 'Verbandsart', ['class' => 'input-label']) !!}
        <div class="input-group">
            {!! Form::select('type',
             collect(['' => 'Bitte wählen',
                    'dach' => 'Dachverband',
                    'fach'=>'Fachverband',
                    'weiter' => 'Weitere verbandsähnliche Organisationen'
                   ]),
             $bandage->type ? $bandage->type : '',
             [
                    'class' => 'select',
                    'title' => 'Verbansart'
              ])
            !!}
        </div>
    </div>

    <div class="col-md-6">
        {!! Form::label('region', 'Landkreis', ['class' => 'input-label']) !!}
        <div class="input-group">
            {!! Form::select('region',
             collect(['' => 'Bitte wählen', 'national' => 'National', 'land' => 'Land', 'region' => 'Region']),
             $bandage->region ? $bandage->region : '',
             [
                    'class' => 'select',
                    'title' => 'Landkreis'
              ])
            !!}
        </div>
    </div>
    <div class="col-md-9">
        {!! Form::label('street', 'Straße  *', ['class' => 'input-label']) !!}
        <div class="input-group">
            {!! Form::text('street', null, [
                'required',
                'class' => 'input',
                'placeholder' => 'Straße',
                'title' => 'Straße'
                ])
            !!}
        </div>
    </div>
    <div class="col-md-3">
        {!! Form::label('house_number', 'Hausnummer', ['class' => 'input-label']) !!}
        <div class="input-group">
            {!! Form::text('house_number', null, [
                'class' => 'input',
                'placeholder' => 'Hausnummer',
                'title' => 'Hausnummer'
                ])
            !!}
        </div>
    </div>
    <div class="col-md-3">
        {!! Form::label('zip', 'Postleitzahl', ['class' => 'input-label']) !!}
        <div class="input-group">
            {!! Form::text('zip', null, [
                    'class' => 'input',
                    'placeholder' => 'PLZ',
                    'title' => 'PLZ'
                    ])
            !!}
        </div>

    </div>
    <div class="col-md-4">
        {!! Form::label('city', 'Stadt *', ['class' => 'input-label']) !!}
        <div class="input-group">
            {!! Form::text('city', null, [
                    'required',
                    'class' => 'input',
                    'placeholder' => 'Stadt',
                    'title' => 'Stadt'
                    ])
            !!}
        </div>
    </div>

    <div class="col-md-5">
        {!! Form::label('email', 'E-Mail-Adresse', ['class' => 'input-label']) !!}
        <div class="input-group">
            {!! Form::text('email', null, [
                    'class' => 'input',
                    'placeholder' => 'E-Mail-Adresse',
                    'title' => 'E-Mail-Adresse'
                    ])
            !!}
        </div>
    </div>

    <div class="col-md-6">
        <div class="club-detail__overview-item">
            {!! Form::label('founded', 'Gegründet', ['class' => 'input-label']) !!}
            <div class="input-group">
                {!! Form::text('founded', null, [
                        'class' => 'input',
                        'placeholder' => 'z.B. '.date('Y'),
                        'title' => 'Gegründet'
                    ])
                !!}
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <p class="input-label">
            * Pflichtfelder
        </p>
    </div>
</div>

<div class="club-content__form-action-wrapper">
    <a href="#"
       class="club-content__form-cancel-action-button button button--grey button--center button--full-width">
        Abbrechen
    </a>
    {!! Form::submit('Speichern',
        array(
            'class' => 'button--save club-content__form-save-action-button button button--edit button--center button--full-width'
        ))
    !!}
</div>
{!! Form::close() !!}