<div {{isset($newContact) ? 'id=add-contact-person' : ''}} class="tabbable-menu__page {{isset($newContact) ? '' : 'tabbable-menu__page--active'}}">
    @if ($bandage->id === null || isset($newContact))
        {!! Form::open(
            array(
                'method' => 'post',
                'route' => ['contact-person.store'],
                'files' => 'true'
            )
        )!!}
    @else
        {!! Form::model(
        $contact,
        array(
            'method' => 'patch',
            'route' => ['contact-person.update', $contact->id ],
            'files' => 'true',
            'data-async-form'
        )
    )!!}
    @endif
    <div class="club-detail__inline-form">
        <div class="row">
            <div class="col-md-12">
                {!! Form::label('order_number', 'Reihenfolge', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::number('order_number', null, ['class' => 'input', 'placeholder' => '']) !!}
                </div>
            </div>

            <div class="col-md-12">
                {!! Form::label('department', 'Abteilung', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::text('department', null, ['class' => 'input', 'placeholder' => '', 'required' => 'required']) !!}
                </div>
            </div>

            <div class="col-md-12">
                {!! Form::label('gender', 'Anrede', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::select('gender', ['' => 'Bitte wählen', 'Herr' => 'Herr', 'Frau' => 'Frau'], null, ['required' => 'required', 'class' => 'select']) !!}
                </div>
            </div>

            <div class="col-md-12">
                {!! Form::label('name', 'Name', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::text('name', null, ['class' => 'input', 'rows' => 10, 'placeholder' => '', 'required' => 'required']) !!}
                </div>
            </div>

            <div class="col-md-12">
                {!! Form::label('position', 'Position', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::text('position', null, ['class' => 'input', 'rows' => 10, 'placeholder' => '', 'required' => 'required']) !!}
                </div>
            </div>

            <div class="col-md-12">
                {!! Form::label('phone', 'Telefon', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::text('phone', null, ['class' => 'input phone', 'rows' => 10, 'placeholder' => '', 'required' => 'required']) !!}
                </div>
            </div>

            <div class="col-md-12">
                {!! Form::label('email', 'Email', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::email('email', null, ['class' => 'input', 'rows' => 10, 'placeholder' => '', 'required' => 'required']) !!}
                </div>
            </div>

            {!! Form::hidden('bandage_id', $bandage->id) !!}

            <div class="col-xs-12">
                <button class="button button--dark button--center button--icon button--full-width"
                        type="submit"><span class="fa fa-edit"></span>
                    @if ($bandage->id === null || isset($newContact))
                        Eintragen
                    @else
                        Aktualisieren
                    @endif
                </button>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
</div>