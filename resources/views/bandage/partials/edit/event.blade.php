<div {{isset($newEvent) ? 'id=add-event' : ''}} class="tabbable-menu__page {{isset($newEvent) ? '' : 'tabbable-menu__page--active'}}">
    @if ($bandage->id === null || isset($newEvent))
        {!! Form::open(
            array(
                'method' => 'post',
                'route' => ['bandage.event.store', '#events'],
                'files' => 'true'
            )
        )!!}
    @else
        {!! Form::model(
        $event,
        array(
            'method' => 'patch',
            'route' => ['bandage.event.update', $event->id, '#events'],
            'files' => 'true',
            'data-async-form'
        )
    )!!}
    @endif
    <div class="club-detail__inline-form">
        <div class="row">
            {{-- @todo extend this form --}}
            <div class="col-md-12">
                {!! Form::label('title', 'Titel', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::text('title', null, ['class' => 'input', 'placeholder' => 'Titel der Veranstaltung', 'required' => 'required']) !!}
                </div>
            </div>


            <div class="col-md-12">
                {!! Form::label('content_raw', 'Text', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::textarea('content_raw', null, ['class' => 'input', 'rows' => 10, 'placeholder' => 'Beschreibung der Veranstaltung', 'required' => 'required']) !!}
                </div>
            </div>

            <div class="col-md-6">
                {!! Form::label('schedule_begin', 'Beginn der Veranstaltung', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::text('schedule_begin', isset($event->schedule_begin) ? date('d.m.Y H:i', strtotime($event->schedule_begin)) : null, ['class' => 'input', 'placeholder' => 'DD.MM.YY HH:mm', 'data-datetimepicker-enabled', 'required' => 'required']) !!}
                </div>
            </div>

            <div class="col-md-6">
                {!! Form::label('schedule_end', 'Ende der Veranstaltung', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::text('schedule_end', isset($event->schedule_end) ? date('d.m.Y H:i', strtotime($event->schedule_end)) : null, ['class' => 'input', 'placeholder' => 'DD.MM.YY HH:mm', 'data-datetimepicker-enabled', 'required' => 'required']) !!}
                </div>
            </div>

            <div class="col-md-9">
                {!! Form::label('street', 'Straße', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::text('street', null, ['class' => 'input', 'placeholder' => 'Straße', 'required' => 'required']) !!}
                </div>
            </div>

            <div class="col-md-3">
                {!! Form::label('house_number', 'Hausnummer', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::text('house_number', null, ['class' => 'input', 'placeholder' => 'Nr']) !!}
                </div>
            </div>

            <div class="col-md-4">
                {!! Form::label('zip', 'Postleitzahl', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::text('zip', null, ['class' => 'input', 'placeholder' => 'PLZ', 'required' => 'required']) !!}
                </div>
            </div>

            <div class="col-md-8">
                {!! Form::label('city', 'Stadt', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::text('city', null, ['class' => 'input', 'placeholder' => 'Stadt / Ort', 'required' => 'required']) !!}
                </div>
            </div>

            <div class="col-md-6">
                {!! Form::label('published_from', 'Veröffentlicht von', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::text('published_from', isset($event->published_from) ? date('d.m.Y H:i', strtotime($event->published_from)) : null, ['class' => 'input', 'placeholder' => 'DD.MM.YY HH:mm', 'data-datetimepicker-enabled']) !!}
                </div>
            </div>

            <div class="col-md-6">
                {!! Form::label('published_until', 'Veröffentlicht bis', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::text('published_to', isset($event->published_to) ? date('d.m.Y H:i', strtotime($event->published_to)) : null, ['class' => 'input', 'placeholder' => 'DD.MM.YY HH:mm', 'data-datetimepicker-enabled']) !!}
                </div>
            </div>

            {!! Form::hidden('bandage_id', $bandage->id) !!}

            <div class="col-xs-12">
                <div id="@if (isset($event)){{ 'upload-result-'. $event->id }}@else{{ 'upload-result' }}@endif "
                     class="thumbnail__list">
                    @if (isset($event))
                        @foreach($event->images as $image)
                            <input type="checkbox" name="delete-image[]" value="{{ $image->id }}"
                                   id="event_thumbnail-{{ $image->id }}" class="thumbnail__switch">
                            <label for="event_thumbnail-{{ $image->id }}"
                                   class="thumbnail__label thumbnail__container thumbnail__container--deleteable">
                                <img class="thumbnail__image" src="{{asset($image->picture->url('singleView')) }}"/>
                            </label>
                        @endforeach
                    @endif
                </div>
            </div>

            <div data-upload-result>
                <div class="col-xs-12 hidden" data-new-image-text>
                    <span class="input-label">Neu hinzugefügte Bilder</span>
                </div>
                <div class="col-xs-12">
                    <div id="@if (isset($post)){{ 'upload-result-'. $post->id }}@else{{ 'upload-result' }}@endif "
                         class="thumbnail__list">
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="club-detail__inline-form-submit-wrapper">
                        <label for="@if (isset($event)){{ 'event_image-'. $event->id }}@else{{ 'event_image' }}@endif"
                               class="button button--grey button--center button--icon button--full-width">
                            <span class="fa fa-upload"></span>Bilder hochladen</label>
                        <input multiple type="file" name="images[]"
                               id="@if (isset($event)){{ 'event_image-'. $event->id }}@else{{ 'event_image' }}@endif"
                               class="input-file">
                        <button class="button button--dark button--center button--icon button--full-width"
                                type="submit"><span class="fa fa-edit"></span>
                            @if ($bandage->id === null || isset($newEvent))
                                Eintragen
                            @else
                                Aktualisieren
                            @endif
                        </button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>