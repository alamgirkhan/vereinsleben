<div class="club-post__item" data-edit-wrapper>
    <div class="row">
        @if(Auth::check() && Auth::user()->isBandageAdmin($bandage))
            <div class="col-xs-11 edit-del-btn">
                <div class="club-content__options">
                    <a href="#" class="club-content__options-button" title="Bearbeiten"
                       data-edit='{ "bandage_id": "{{ $bandage->id }}",
                       "resource": { "id": "{{ $post->id }}",
                       "type": "post",
                       "url": "{{ route('bandage.post.edit', $post->id) }}"
                       } }'>
                        <span class="fa fa-pencil club-content__options-button-edit"></span>
                    </a>
                    <a href="#" class="club-content__options-button club-content__options-button--hidden"
                       title="Bearbeiten abbrechen"
                       data-cancel='{ "bandage_id": "{{ $bandage->id }}",
                       "resource": { "id": "{{ $post->id }}",
                       "type": "post",
                       "url": "{{ route('bandage.post.single', [$bandage->slug, $post->id]) }}"
                       } }'>
                        <span class="fa fa-times club-content__options-button-cancel"></span>
                    </a>
                    <a href="#" class="club-content__options-button" title="Löschen"
                       data-delete='{ "bandage_id": "{{ $bandage->id }}",
                       "resource": { "id": "{{ $post->id }}",
                        "type": "post",
                       "url": "{{ route('post.delete', $post->id) }}"
                       } }'>
                        <span class="fa fa-trash club-content__options-button-remove"></span>
                    </a>
                </div>
            </div>
        @endif
    </div>

    <div data-edit-content>
        <div class="row">
            <div class="col-lg-1 club-post__date noborder">
                @if($post->published_from >= \Carbon\Carbon::now())
                    <span class="club-post__date club-post__date--scheduled">
				<i class="fa fa-clock-o" aria-hidden="true"></i>
                        {{ date('d.m.Y / H:i', strtotime($post->published_from)) }}

                        @if(isset($post->published_to) and $post->published_to !== null)
                            - {{ date('d.m.Y / H:i', strtotime($post->published_to)) }}
                        @endif
			</span>
                @else
                    <span class="date-no">{{ date('d', strtotime($post->published_at)) }}.</span><br>
                    <span class="month-no">{{ strtoupper(date('M', strtotime($post->published_at))) }}</span><br>
                    <span class="year-no">{{ date('Y', strtotime($post->published_at)) }}</span>
                @endif
            </div>

            <div class="col-lg-11 col-sm-11 event-post">
                @if($post->images()->count() > 0)
                    <div class="event-post-thum">
                        @if($post->images()->count() == 1)
                            @foreach($post->images as $image)
                                <div class="club-post__image"
                                     style="background-image: url('{{asset($image->picture->url('singleView')) }}')"></div>
                            @endforeach
                        @else

                            <div class="row no-margin">
                                <div class="col-lg-8 col-sm-6 club-post__image"
                                     style="background:url('{{asset($post->images[0]->picture->url('singleView')) }}') no-repeat;background-size:cover;"></div>
                                <div class="col-lg-4 col-sm-6 club-post_right-thums">
                                    <div class="row">
                                        @foreach($post->images as $key => $image)
                                            @if($key !=0)
                                                <div class="col-lg-12 club-post__sml-image"
                                                     style="background:url('{{asset($image->picture->url('singleView')) }}') no-repeat;background-size:cover;"></div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="event-post-thum-arrow"><i class="fa fa-caret-up" aria-hidden="true"></i></div>
                        @if($post->images()->count() > 3)
                            <div class="event-plus-icon"><a href="javascript:void(0);"><i class="fa fa-plus-circle"
                                                                                          aria-hidden="true"></i></a>
                            </div>
                        @endif
                    </div>
                @endif

                @if(isset($post->content))
                    <div class="row post-head">
                        <div class="col-lg-2 col-sm-6 postlogo">
                            <img width="61px" src="{{ asset($bandage->avatar->url('singleView')) }}"/>
                        </div>
                        <div class="col-lg-10 col-sm-6 postdetail">
                            <div class="postlefttxt">{{ $bandage->shorthand }}
                                <div class="post-blue">{{ $post->title }}</div>
                            </div>
                        </div>
                    </div>
                    <p>{!! nl2br($post->content) !!}</p>
                @endif
            </div>
        </div>
    </div>
</div>