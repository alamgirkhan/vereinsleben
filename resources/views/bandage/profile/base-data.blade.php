<div class="row">
    <div class="col-md-12">Verbands-name: {{ $bandage->name }}</div>
    <div class="col-md-12">Verbands-email: {{ $bandage->email }}</div>
    <div class="col-md-12">Verbands-gegründet: {{ $bandage->founded }}</div>
    <div class="col-md-12">status ? : {{ $is_subscribed ? "subscribed"  : "not subscribed" }}</div>
</div>


<div class="row">
    {!! Form::model(
    $bandage,
    array(
        'method' => 'patch',
        'route' => ['bandage.update.subscribe', $bandage->slug],
        'class' => 'club-edit__form',
        'files' => 'false'
    )
)!!}

    {!! Form::submit($is_subscribed ? "unsubscribe" : "subscribe" ,
    array(
        'class' => 'button--save club-content__form-save-action-button button button--edit button--center button--full-width'
    ))
!!}

    {!! Form::close() !!}
</div>