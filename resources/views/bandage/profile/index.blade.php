@extends('layouts.master')

@section('content')
    <section class="club-new">

        <div class="club-detail-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <h1 class="club-detail__headline">Verband bearbeiten</h1>
                        <div class="club-content__edit-area club-content__box">
                            @include('bandage.profile.base-data', ['bandage' => $bandage])
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection