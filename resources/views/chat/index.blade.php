@extends('layouts.master')
@section('content')
    <div class="chatwrapper">
        <div class="content">
            <h2>Messenger</h2>
            @if($clubs)
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-8 col-xs-12">
                        {!! Form::label('clubs', 'Vereine', ['class' => 'input-label']) !!}
                        <div class="input-group">
                            {!! Form::select('clubs', $clubs, '', ['class' => 'select chatselect', 'placeholder' => 'Bitte wählen', 'id' => 'clubs', 'required' => 'required']) !!}
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 ckbox-area">
                        <div class="input-group nwsltr">
                            {!! Form::checkbox('members', '1', '', ['id' => 'members', 'class' => 'input-checkbox']) !!}
                            {!! Form::label('members', 'Mitglieder', ['class' => 'input-checkbox__label']) !!}
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 ckbox-area">
                        <div class="input-group msge">
                            {!! Form::checkbox('fans', '1', '', ['id' => 'fans', 'class' => 'input-checkbox']) !!}
                            {!! Form::label('fans', 'Fans', ['class' => 'input-checkbox__label']) !!}
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 ckbox-area">
                        <div class="input-group msg">
                            {!! Form::checkbox('membfans', '1', '', ['id' => 'membfans', 'class' => 'input-checkbox']) !!}
                            {!! Form::label('memfans', 'Alle', ['class' => 'input-checkbox__label']) !!}
                        </div>
                    </div>
                </div>
            @endif
            <div class="contact-profile">
                @if($chatuser)
                    <a href="{{ route('user.detail', $chatuser->username) }}"><img
                                src="{{ $chatuser->avatar->url('singleView') }}" id="profile-img"></a>
                    <p>{{ $chatuser->fullname }}</p>
                @endif
            </div>
            <div id="chat">
                <div id="messagewindow" class="messages">
                    <ul>
                        <message v-for="message in chatting.messages" :key="message.id">
                            <small v-if="message.user_id == {{$user->id}}" class="reply_date">@{{ message.created_at |
                                formatDate
                                }}
                            </small>
                            <small v-else class="sent_date">@{{ message.created_at | formatDate }}</small>
                            <p v-if="message.user_id == {{$user->id}}" class="replies">@{{ message.message }}</p>
                            <p v-else class="sent">@{{ message.message }}</p>
                        </message>
                    </ul>
                </div>
                <div class="message-input">
                    <div class="wrap">
                        <input type="text" placeholder="Nachricht..." v-model="message" @keyup.enter="send"/>
                    </div>
                </div>
            </div>
        </div>
        <div id="sidepanel">
            <div id="profile">
                <div class="wrap">
                    <a href="{{ route('user.detail', $user->username) }}">
                        <img
                                src="{{ $user->avatar->url('singleView') }}" id="profile-img">
                        <p>{{ $user->fullname }}</p>
                    </a>
                </div>
            </div>
            <div class="chat-rhs">
                <div id="search">
                    <label for=""><i class="fa fa-search" aria-hidden="true"></i></label>
                    <input type="text" id="searchfriends" onkeyup="searchFriends()" placeholder="Suchen..."/>
                </div>
                <div id="contacts">
                    <ul>
                        @foreach($users as $friend)
                            <li class="contact @if($chatuser && $chatuser->username == $friend->username) current @endif">
                                <div class="wrap">
                                    <a href="{{ route('chat', $friend->username) }}">
                                        <img src="{{ $friend->avatar->url('singleView') }}" id="profile-img">
                                        <div class="meta">
                                            <p class="name">{{ $friend->fullname }}</p>
                                            <p class="preview"></p>
                                        </div>
                                        <div style="clear:both"></div>
                                    </a>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
    </div>
    </div>
    <script src="{{ asset('js/chat.js') }}" charset="utf-8"></script>
    <script>
        function searchFriends() {
            var input, filter, ul, li, a, i;
            input = document.getElementById('searchfriends');
            filter = input.value.toUpperCase();
            contacts = document.getElementById("contacts");
            li = contacts.getElementsByTagName('li');

            // Loop through all list items, and hide those who don't match the search query
            for (i = 0; i < li.length; i++) { 
                a = li[i].getElementsByTagName("a")[0];
                if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    li[i].style.display = "";
                } else {
                    li[i].style.display = "none";
                }
            }
        }
    </script>
@endsection