@extends('layouts.master')
@section('title')
    {{ $club->name }} -

    @if($club->sports->first() !== null)
        {{ $club->sports->first()->title }}
    @endif

    in {{ $club->city or '' }}, {{ $club->federal_state or '' }}
@endsection

@section('content')
    {{--@if($club->unowned === 1)--}}
    {{--@include('club.partials.profile.unowned')--}}
    {{--@else--}}

        <section data-token="{{ csrf_token() }}" @can('edit', $club) data-club-editable="true" @endcan>
            <input type="hidden" name="search_id" id="search_id" value="{{ $club->id }}"/>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 profilseite-slide">
                        <div class="club-detail__main-image-wrapper"
                             style="background-image: url('{{ asset($club->header->url('singleView')) }}')"
                             @can('edit', $club) data-section-editable="true" @endcan>

                            <img class="club-detail__main-image" src="{{ asset($club->header->url('singleView')) }}">

                            @if(!$club->isPublished())
                                <div class="club-detail__notification club-detail__notification--warning">
                                    <span>Dein Verein ist noch nicht öffentlich</span>, du kannst ihn
                                    <a data-update-url="{{ route('club.updatePublished', $club->slug) }}"
                                       data-club-published-button
                                       href="#" class="club-detail__notification-link">jetzt
                                        veröffentlichen <span class="fa fa-eye"></span> </a>
                                </div>
                            @else
                                <div class="club-detail__notification club-detail__notification--hidden"></div>
                            @endif

                            @can('edit', $club)
                                @include('club.partials.edit.header-image', ['club' => $club])
                            @endcan
                        </div>
                    </div>
                </div>
            </div>

            <section id="about-strip" @if($club->header_color) style="box-shadow:2px 2px 6px #{{ $club->header_color }}"
                     @endif class="about-strip">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-sm-4"></div>
                        <div class="col-md-9 col-sm-8 col-xs-8 proifle-top-links profile-desk">
                            <div class="row">
                                <div class="col-md-7 col-sm-7 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-3 count-tabs">
                                            Besucher<br><strong>{{ $club->views == null ? 0 : $club->views }}</strong>
                                        </div>
                                        <div class="col-md-3 count-tabs">
                                            Fans<br><strong>{{ $club->fans->count() }}</strong></div>
                                        <div class="col-md-3 count-tabs no-border">
                                            Mitglieder<br><strong>{{ $club->members->count() }}</strong></div>
                                        <div class="col-md-3"></div>
                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-5 col-xs-12">
                                    <div class="row">
                                        <div id="fan-btns" class="col-md-6 col-sm-5 col-xs-6 fan-btns">
                                            @can('updateFanStatus', $club)
                                                @if(Auth::check() && !$me->isProfileCompleted())
                                                    <a href="#" id="bin_fan"
                                                       @if($club->title_color) style="color:#{{ $club->title_color }}"
                                                       @endif
                                                       class="btn btn--inline btn--fan"
                                                       style="{{ Auth::check() && Auth::user()->isClubMember($club) ? 'display: none;' : '' }}"
                                                       data-modal-url="@if(Auth::check() && !$me->isProfileCompleted()) {{ route('user.profile.wizard') }} @else {{ route('team-member', $club->slug) }} @endif"
                                                       data-modal="team-member">{{ Auth::check() && Auth::user()->isClubFan($club) ? 'Fan' : 'Ich bin Fan' }}</a>
                                                @else
                                                    <a href="#" id="bin_fan"
                                                       @if($club->title_color) style="color:#{{ $club->title_color }}"
                                                       @endif class="btn btn--inline btn--fan"
                                                       style="{{ Auth::check() && Auth::user()->isClubMember($club) ? 'display: none;' : '' }}"
                                                       data-club-fan-button
                                                       data-club-fan-url=" {{ route('club.fan.update', $club->slug) }} "
                                                       data-club-fan-status="{{ Auth::check() && Auth::user()->isClubFan($club) ? 'true' : 'false' }}">
                                                        {{ Auth::check() && Auth::user()->isClubFan($club) ? 'Fan' : 'Ich bin Fan' }}</a>
                                                @endif
                                            @endcan
                                        </div>

                                        <div class="col-md-6 col-sm-7 col-xs-6 fan-btns">
                                            @can('updateMemberStatus', $club)
                                                <a href="#" id="bin_member"
                                                   @if($club->title_color) style="color:#{{ $club->title_color }}"
                                                   @endif
                                                   class="btn btn--inline btn--member @if(Auth::check() && Auth::user()->isClubMember($club)) btn--active @endif"
                                                   data-modal-url="@if(Auth::check() && !$me->isProfileCompleted()) {{ route('user.profile.wizard') }} @else {{ route('team-member', $club->slug) }} @endif"
                                                   data-modal-extra="{{ Auth::check() && Auth::user()->isClubMember($club) ? 'true' : 'false' }}"
                                                   data-modal="team-member">{{ Auth::check() && Auth::user()->isClubMember($club) ? 'Mitglied' : 'Ich bin Mitglied' }}</a>
                                            @endcan
                                        </div>

                                        <div class="col-md-8 col-md-offset-4">
                                            @can('updateMemberStatus', $club)
                                                <div class="modal__container" data-club-member-modal>
                                                    <section class="modal">
                                                        <div class="modal__close">
                                                            <i class="fa fa-times" aria-hidden="true"></i>
                                                        </div>
                                                    </section>
                                                </div>
                                            @endcan
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-9 col-xs-8 proifle-top-links profile-mob">
                            <div class="row">
                                <div class="col-md-5 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-5 col-xs-6 fan-btns">
                                            @can('updateFanStatus', $club)
                                                @if(Auth::check() && !$me->isProfileCompleted())
                                                    <a href="#" id="bin_fan"
                                                       @if($club->title_color) style="color:#{{ $club->title_color }}"
                                                       @endif
                                                       class="btn btn--inline btn--fan"
                                                       style="{{ Auth::check() && Auth::user()->isClubMember($club) ? 'display: none;' : '' }}"
                                                       data-modal-url="@if(Auth::check() && !$me->isProfileCompleted()) {{ route('user.profile.wizard') }} @else {{ route('team-member', $club->slug) }} @endif"
                                                       data-modal="team-member">{{ Auth::check() && Auth::user()->isClubFan($club) ? 'Fan' : 'Ich bin Fan' }}</a>
                                                @else
                                                    <a href="#" id="bin_fan"
                                                       @if($club->title_color) style="color:#{{ $club->title_color }}"
                                                       @endif class="btn btn--inline btn--fan"
                                                       style="{{ Auth::check() && Auth::user()->isClubMember($club) ? 'display: none;' : '' }}"
                                                       data-club-fan-button
                                                       data-club-fan-url=" {{ route('club.fan.update', $club->slug) }} "
                                                       data-club-fan-status="{{ Auth::check() && Auth::user()->isClubFan($club) ? 'true' : 'false' }}">
                                                        {{ Auth::check() && Auth::user()->isClubFan($club) ? 'Fan' : 'Ich bin Fan' }}</a>
                                                @endif
                                            @endcan
                                        </div>
                                        <div class="col-md-6 col-sm-7 col-xs-6 fan-btns">
                                            @can('updateMemberStatus', $club)
                                                <a href="#" id="bin_member"
                                                   @if($club->title_color) style="color:#{{ $club->title_color }}"
                                                   @endif
                                                   class="btn btn--inline btn--member @if(Auth::check() && Auth::user()->isClubMember($club)) btn--active @endif"
                                                   data-modal-url="@if(Auth::check() && !$me->isProfileCompleted()) {{ route('user.profile.wizard') }} @else {{ route('team-member', $club->slug) }} @endif"
                                                   data-modal-extra="{{ Auth::check() && Auth::user()->isClubMember($club) ? 'true' : 'false' }}"
                                                   data-modal="team-member">{{ Auth::check() && Auth::user()->isClubMember($club) ? 'Mitglied' : 'Ich bin Mitglied' }}</a>
                                            @endcan
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-7 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-3 count-tabs">
                                            Besucher<br><strong>{{ $club->views == null ? 0 : $club->views }}</strong>
                                        </div>
                                        <div class="col-md-3 count-tabs">
                                            Fans<br><strong>{{ $club->fans->count() }}</strong></div>
                                        <div class="col-md-3 count-tabs no-border">
                                            Mitglieder<br><strong>{{ $club->members->count() }}</strong></div>
                                        <div class="col-md-3"></div>
                                    </div>
                                </div>
                                <div class="col-md-8 col-md-offset-4">
                                    @can('updateMemberStatus', $club)
                                        <div class="modal__container" data-club-member-modal>
                                            <section class="modal">
                                                <div class="modal__close">
                                                    <i class="fa fa-times" aria-hidden="true"></i>
                                                </div>
                                            </section>
                                        </div>
                                    @endcan
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <div class="row club-left proife-lft ">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-3 profile-pic"
                                         @can('edit', $club) data-section-editable="true" @endcan>
                                        <img class=""
                                             src="{{ asset($club->avatar->url('singleView')) }}">
                                        @can('edit', $club)
                                            @include('club.partials.edit.avatar-image', ['club' => $club])
                                        @endcan
                                    </div>
                                </div>
                                <div class="row pro-na-sec">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-3 profile-name">
                                        <a href="#"><img id="profile-pic-icon"
                                                         src="{{asset('static_images/profile-pic-icon.png')}}"/><br>Vereinsprofil</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 profile-tabs">
                                        @include('club.partials.detail-menu', ['club' => $club, 'socialLinks' => $socialLinks, 'socialLinkTypes' => $socialLinkTypes,'sports' => $sports])
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row hidden-xs">
                            <div class="col-lg-12">
                                @if($socialLinks->count() > 0  || (Auth::check() && policy($club)->edit(Auth::user(), $club)))
                                    <section class="club-detail__menu-list-item club-detail__menu-list-item--static"
                                             @can('edit', $club) data-section-editable="true" @endcan>

                                        @can('edit', $club)
                                            <span>
											<a class="club-content__edit-button club-content__edit-button--sociallinks pull-right"
                                               href="#">
												<span class="fa fa-pencil"></span>
											</a>
										</span>
                                        @endcan

                                        <div class="club-content__view-area">
                                            @can('edit', $club)
                                                @if(count($socialLinks) === 0)
                                                    <em>Noch keine Links angelegt.</em>
                                                @endif
                                            @endcan
                                        </div>

                                        @can('edit', $club)
                                            <div class="club-content__edit-area">
                                                @include('club.partials.edit.social-links', ['club' => $club, 'socialLinks' => $socialLinks, 'socialLinkTypes' => $socialLinkTypes])
                                            </div>
                                        @endcan
                                    </section>
                                @endif
                            </div>
                        </div>

                        <div class="row hidden-xs">
                            <div class="col-lg-12 profile-cnt-details">
                                <h2>Mehr vom Verein</h2>
                                <ul>
                                    @if($club->email !== null && trim($club->email) !== "")
                                        <li><a href="mailto:{{ $club->email }}">
                                                <span><i class="fa fa-envelope"
                                                         aria-hidden="true"></i></span>{{ $club->email }}</a>
                                        </li>
                                    @endif
                                    @if($club->phone !== null && trim($club->phone) !== "")
                                        <li><span><i class="fa fa-phone"
                                                     aria-hidden="true"></i></span> {{ $club->phone }}</li>
                                    @endif

                                    @foreach($socialLinks as $socialLink)
                                        @if($socialLink->link_type == 'googleplus')
                                            <li>
                                                <a href="{{ $socialLink->url }}"
                                                   target="_blank">
                                                    <span><i class="fa fa-google-plus" aria-hidden="true"></i></span>
                                                    {{ ucfirst($socialLink->link_type) }}</a>
                                            </li>
                                        @elseif($socialLink->link_type == 'web')
                                            <li>
                                                <a href="{{ $socialLink->url }}"
                                                   target="_blank">
                                                    <span><i class="fa fa-globe" aria-hidden="true"></i></span>
                                                    {{ str_limit($socialLink->url, $limit = 22, $end = '...') }}</a>
                                            </li>
                                        @else
                                            <li>
                                                <a href="{{ $socialLink->url }}"
                                                   target="_blank">
                                                    <span><i class="fa fa-{{ $socialLink->link_type }}"
                                                             aria-hidden="true"></i></span>
                                                    @php
                                                        $link = preg_replace('#^https?://#', '', $socialLink->url);
                                                        echo str_limit($link, $limit = 20, $end = '...');
                                                    @endphp

                                                    {{--{{ ucfirst($socialLink->link_type) }}--}}
                                                </a>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                        </div>

                        <div class="row hidden-xs">
                            <div class="col-lg-12 profile-cnt-details2">
                                @can('edit', $club)
                                    @if(isset($completionProgress))
                                        @include('club.partials.completion-progress', ['completionProgress' => $completionProgress])
                                    @endif
                                @endcan
                            </div>
                        </div>
                        <div class="row hidden-xs">
                            <div class="col-lg-12 profile-cnt-details profile-cnt-details2">
                                <h2>Profil Teilen</h2>
                                <ul class="styled">
                                    <li>
                                        <a href="https://www.facebook.com/sharer/sharer.php?u={{urlencode(route('club.detail', $club->slug))}}"
                                           target="_blank">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://twitter.com/intent/tweet?url={{urlencode(route('club.detail', $club->slug))}}"
                                           target="_blank" class="">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-9 col-sm-8 proifle-post-sec">
                        <div class="row">
                            <div class="col-md-6 col-lg-9">
                                <h2 id="clubname" class="clubname"
                                    @if($club->title_color) style="color:#{{ $club->title_color }}" @endif>{{ $club->name }}</h2>
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="review-average">
                                    @for ($i=1; $i <= 5 ; $i++)
                                      <span class="star {{ ($i <= $reviewAverage) ? '' : 'no-active'}}">★</span>
                                    @endfor
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-md-12">
                                <div @if($club->title_color) style="color:#{{ $club->title_color }}"
                                     @endif id="post-location-links" class="row post-location-links">
                                    @if($club->fullAddress() != '')
                                        <div class="col-lg-8 col-md-8 sm-8 xs-12">
                                            <span class="icon-area"><i class="fa fa-map-marker" aria-hidden="true"></i></span> {{ $club->fullAddress() }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        @if(!is_null($club->allSportsStringsAsArray()))
                            <div class="row">
                                <div class="col-lg-12 profile-post-tabs">
                                    <ul>
                                        <li>Sportarten:</li>
                                        @for($i = 0; $i < count($club->allSportsStringsAsArray()); $i++)

                                            @if($i > 6)
                                                <li class="sporthide" style="display:none;"><a
                                                            @if($club->title_color) style="background-color:#{{ $club->title_color }}"
                                                            @endif class="club_sports">{{ $club->allSportsStringsAsArray()[$i] }}</a>
                                                </li>
                                            @else
                                                <li><a
                                                            @if($club->title_color) style="background-color:#{{ $club->title_color }}"
                                                            @endif class="club_sports">{{ $club->allSportsStringsAsArray()[$i] }}</a>
                                                </li>
                                            @endif
                                        @endfor

                                        @if(count($club->allSportsStringsAsArray()) > 7)
                                            <li>
                                                <a style="background-color: #01262e;" id="sportsmore" onclick="showHiddenSports()">Mehr anzeigen</a>
                                            </li>
                                        @endif

                                        {{--@foreach($club->allSportsStringsAsArray() as $sport)--}}
                                            {{--@if(count($club->allSportsStringsAsArray()) > 8)--}}
                                                {{--<li class="sporthide" style="display:none;"><a--}}
                                                       {{--@if($club->title_color) style="background-color:#{{ $club->title_color }}"--}}
                                                       {{--@endif class="club_sports">{{ $sport }}</a>--}}
                                                {{--</li>--}}
                                            {{--@else--}}
                                                {{--<li><a--}}
                                                   {{--@if($club->title_color) style="background-color:#{{ $club->title_color }}"--}}
                                                   {{--@endif class="club_sports">{{ $sport }}</a>--}}
                                                {{--</li>--}}
                                            {{--@endif--}}
                                        {{--@endforeach--}}
                                        {{--@if(count($club->allSportsStringsAsArray()) > 8)--}}
                                            {{--<li><a--}}
                                                   {{--@if($club->title_color) style="background-color:#{{ $club->title_color }}"--}}
                                                   {{--@endif class="club_sports" id="sportsmore" onclick="showHiddenSports()">Mehr...</a>--}}
                                            {{--</li>--}}
                                        {{--@endif--}}
                                    </ul>
                                </div>
                            </div>
                        @endif
                        @if((Auth::user() && $club->administrators->count() == 0 && !Auth::user()->hasClubAdminRequested($club)) || (!Auth::user() && $club->administrators->count() == 0))
                            <div class="row" id="request-admin">
                                <div class="col-md-12" id="request-description">
                                  <div id="head"><strong>Tipp:</strong> Dieses Vereinsprofil übernehmen und alle Informationen auf den neuesten Stand bringen!</div>
                                    <div id="lightbulb">
                                      <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                    </div>
                                    <div id="des">
                                        Als Administrator eures Vereinsprofils könnt ihr euer Logo und ein Titelbild hinzufügen,<br>
                                        Ansprechpartner, Abteilungen und Mannschaften angeben, Veranstaltungen eintragen, <br>
                                        Neuigkeiten aus eurem Vereinsleben auf eurer Pinnwand veröffentlichen und vieles mehr.<br>
                                    </div>
                                    <div>
                                      Du bist in diesem Verein tätig, z.B. im Vorstand oder als Beauftragter für eure Presse- und Öffentlichkeitsarbeit, Social Media oder die Vereinswebsite zuständig? Dann beantrage jetzt die Übernahme dieses Profils!
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    {!! Form::open(
                                        array(
                                            'method' => 'post',
                                            'route' => ['club.admin.request', $club->slug],
                                            'files' => 'false'
                                        )
                                     )!!}
                                    @if(Auth::user())
                                        {!! Form::hidden('username', Auth::user()->username) !!}
                                    @endif
                                    <div class="input-group" style="padding-top:10px;">
                                        @if(Auth::check() && ($me->firstname == '' || $me->lastname == '' || $me->city == '' || $me->zip == ''))
                                            <a href="#" class="btn btn--inline btn--red"
                                               data-modal-url="{{ route('user.profile.wizard') }}"
                                               data-modal="team-member">Jetzt Profilübernahme beantragen</a>
                                        @else
                                            <button class="btn btn--inline btn--red"
                                                    type="submit">
                                                Jetzt Profilübernahme beantragen
                                            </button>
                                        @endif
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-offset-0 post-lead" style="padding-left:18px;">
                                @if(count($errors) > 0)
                                    <div class="flash-message flash-message--error">
                                        <strong>Es ist ein Fehler aufgetreten:</strong>
                                        <ul>
                                            @foreach($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                @if(Auth::check() && $club->administrators->count() > 0)
                                  <section id="pinnwand" class="pageable-menu__page pageable-menu__page--active">

                                    <h2 class="club-content__box-headline">VEREINS-PINNWAND
                                        @can('edit', $club)
                                            <a class="pull-right"
                                               onClick="$('#add-post').toggleClass('pageable-menu__page--active');"
                                               href="#add-post">
                                                <span><i @if($club->title_color) style="color:#{{ $club->title_color }}"
                                                         @endif class="fa fa-plus-circle" aria-hidden="true"></i></span>
                                            </a>
                                        @endcan
                                    </h2>
                                    @can('edit', $club)
                                        @include('club.partials.edit.post', ['newPost' => true, 'found' => $foundzip])
                                    @endcan

                                    <div class="club-post__wrapper">
                                        @if($posts->count() > 0)
                                            <div data-load-more='{"url": "{{ route('post.latest', $club->slug) }}"}'>
                                                <div data-load-more-html></div>

                                                <div class="col-md-12">
                                                    <div data-loader
                                                         class="loader__wrapper loader__wrapper--centered">
                                                        <div class="loader loader--loading"></div>
                                                        <div class="loader loader--loading"></div>
                                                    </div>
                                                </div>
                                                @if($posts->count() > 9)
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="btn btn-default">
                                                                <a data-load-more-button
                                                                   class=" interessiert-btn">
                                                                    MEHR ANZEIGEN
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        @else
                                            <div class="club-post__item">
                                                <span class="club-post__date noborder">vereinsleben.de</span>
                                                <div class="club-post__content">
                                                    Keine Neuigkeiten vorhanden
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </section>
                                @endif
                                {{-- history & about section --}}
                                @if((trim($club->about) !== '' || trim($club->description) !== '' || Auth::check()) && $club->administrators->count() > 0)
                                    <section id="informationen"
                                             class="pageable-menu__page pageable-menu__page--active club-content club-content--information"
                                             @can('edit', $club) data-section-editable="true" @endcan>

                                        @can('edit', $club)
                                            <a class="club-content__edit-button club-content__edit-button--information button button--edit button--icon button--center button--condensed pull-right"
                                               href="#">
                                                <span class="fa fa-pencil"></span>
                                            </a>
                                        @endcan

                                        <h2 class="club-content__box-headline">Informationen</h2>

                                        @can('edit', $club)
                                            <div class="row clr-theme">
                                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-6 clr1 clr-arrow">
                                                    <span class="lin">Linienfarbe wählen</span>
                                                    <input type="text" name="header_color" id="header_color"
                                                           class="jscolor {valueElement:'header_color', onFineChange:'setHeaderColor(this)'} clr-code"
                                                           value="@if($club->header_color) {{ $club->header_color }} @else 29c7ab @endif">
                                                    <a href="javascript:assignVal(1)"><span class="icon-area"><i
                                                                    class="fa fa-arrow-right"
                                                                    aria-hidden="true"></i></span></a>
                                                </div>

                                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-6 clr1 clr-arrow">
                                                    <span class="sch">Schriftfarbe wählen</span>

                                                    <input type="text" name="title_color" id="title_color"
                                                           class="jscolor {valueElement:'title_color', onFineChange:'setTextColor(this)'} clr-code"
                                                           value="@if($club->title_color) {{ $club->title_color }} @else 29c7ab @endif">
                                                    <a href="javascript:assignVal(2)"><span class="icon-area"><i
                                                                    class="fa fa-arrow-left"
                                                                    aria-hidden="true"></i></span></a>
                                                </div>
                                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 clr3">
                                                    <button @if($club->title_color) style="background:#{{ $club->title_color }}"
                                                            @endif id="color-publish" class="color-publish"
                                                            data-color-published
                                                            data-club-slug='{{ $club->slug }}'>Speichern
                                                    </button>
                                                    <span class="undo"><a href="{{ $club->slug }}/color/reset"><span
                                                                    class="zu">Zurücksetzen</span> <i
                                                                    class="fa fa-undo"></i></a></span>
                                                </div>
                                            </div>
                                        @endcan

                                            @can('edit', $club)
                                            <div class="row">
                                                <div class="club-content__edit-area club-content__box">
                                                    @include('club.partials.edit.base-data', ['club' => $club])
                                                </div>
                                            </div>
                                        @endcan

                                        <div class="row">
                                            <div class="col-md-12">

                                                <div class="tabset">
                                                    <!--<input type="radio" name="tabset" id="tab1"
                                                           aria-controls="satammdaten" checked>
                                                    <label for="tab1">Stammdaten</label>-->
                                                    <input type="radio" name="tabset" id="tab2" checked
                                                           aria-controls="verein">
                                                    <label for="tab2">Über den Verein</label>
                                                    <input type="radio" name="tabset" id="tab3"
                                                           aria-controls="trainingsorte">
                                                    <label for="tab3">Sportstätten</label>

                                                    <div class="tab-panels">
                                                    <!--<section id="satammdaten" class="tab-panel">
                                                            <div class="row">
                                                                <div class="col-lg-6 col-sm-12">
                                                                    <div class="shaddow-box">
                                                                        <h3>Eckdaten</h3>
                                                                        <ul>
                                                                            <li><span class="icon-area"><i
                                                                                            class="fa fa-flag-o"
                                                                                            aria-hidden="true"></i></span>
                                                                                <span class="txt-area">{{ $club->founded }}
                                                            gegründet</span>
                                                    </li>
                                                    <li><span class="icon-area"><i
                                                                    class="fa fa-users"
                                                                    aria-hidden="true"></i></span>
                                                        <span class="txt-area">{{ $club->members->count() }}
                                                            Mitglieder</span></li>
                                                    <li><span class="icon-area"><i
                                                                    class="fa fa-info-circle"
                                                                    aria-hidden="true"></i></span>
                                                        <span class="txt-area">Die MANNschaft e.V. ist ein Verein zur Förderung des Ausdauersports</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="col-lg-6 col-sm-12">
                                            <div class="shaddow-box">
                                                <h3>Kontaktinfos
@can('edit', $club)
                                                        <a class="pull-right"
                                                           onClick="$('#contact').toggleClass('pageable-menu__page--active');"
                                                           href="#contact">
                                                            <span><i class="fa fa-plus-circle"
                                                                     aria-hidden="true"></i></span>
                                                        </a>
@endcan
                                                            </h3>

@can('edit', $club)
                                                        @include('club.partials.user.edit-contact-person', ['club' => $club])
                                                    @endcan

                                                    @foreach($contacts as $contact)
                                                        <ul data-edit-wrapper>
                                                            <li>
                                                                <h3>{{ $contact->firstname }} {{  $contact->lastname }}
                                                        @if(Auth::check() && policy($club)->edit(Auth::user(), $club))
                                                            <a href="#"
                                                               class="club-content__options-button"
                                                               style="float:right"
                                                               title="Löschen"
                                                               data-delete='{ "club_id": "{{ $club->id }}",
																						   "resource": { "id": "{{ $contact->id }}",
																						   "type": "post",
																						   "url": "{{ route('club.delete.contact', [$club->slug, $contact->id]) }}"
																						   } }'>
                                                                                                <span class="fa fa-trash club-content__options-button-remove"></span>
                                                                                            </a>
                                                                                        @endif
                                                                </h3>
                                                            </li>
@if($contact->email !== null && trim($contact->email) !== "")
                                                            <li><span class="icon-area"><i
                                                                            class="fa fa-envelope"
                                                                            aria-hidden="true"></i></span>
                                                                <span class="txt-area"><a
                                                                            href="mailto:{{ $contact->email }}">{{ $contact->email }}</a></span>
                                                                                </li>
                                                                                @endif
                                                        @if($contact->phone !== null && trim($contact->phone) !== "")
                                                            <li><span class="icon-area"><i
                                                                            class="fa fa-phone"
                                                                            aria-hidden="true"></i></span>
                                                                <span class="txt-area">{{ $contact->phone }}</span>
                                                                                </li>
                                                                                @endif
                                                        @if($contact->fullAddress() != '')
                                                            <li><span class="icon-area"><i
                                                                            class="fa fa-map-marker"
                                                                            aria-hidden="true"></i></span>
                                                                <span class="txt-area">{{ $contact->fullAddress() }}</span>
                                                                                </li>
                                                                                @endif
                                                                </ul>
@endforeach
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-lg-6 col-sm-12">
                                                            <div class="shaddow-box">
                                                                <h3>Weitere Infos</h3>
                                                                <ul>
                                                                    <li><span class="icon-area"><i
                                                                                    class="fa fa-info-circle"
                                                                                    aria-hidden="true"></i></span>
                                                                        <span class="txt-area"><strong>Mitglieder</strong></span>
                                                                    </li>
                                                                    <li><span class="icon-area"><i
                                                                                    class="fa fa-info-circle"
                                                                                    aria-hidden="true"></i></span>
                                                                        <span class="txt-area"><strong>Impressum</strong></span>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6 col-sm-12">
                                                            <div class="shaddow-box">
                                                                <h3>Web & Social Media</h3>
                                                                <ul>
@foreach($socialLinks as $socialLink)
                                                        @if($socialLink->link_type == 'googleplus')
                                                            <li>
                                                                <a href="{{ $socialLink->url }}"
                                                                                           target="_blank">
                                                                                            <span><i class="fa fa-google-plus"
                                                                                                     aria-hidden="true"></i></span>
                                                                                            {{ ucfirst($socialLink->link_type) }}
                                                                    </a>
                                                                </li>
@elseif($socialLink->link_type == 'web')
                                                            <li>
                                                                <a href="{{ $socialLink->url }}"
                                                                                           target="_blank">
                                                                                            <span><i class="fa fa-globe"
                                                                                                     aria-hidden="true"></i></span>
                                                                                            {{ $socialLink->url }}</a>
                                                                                    </li>
                                                                                @else
                                                            <li>
                                                                <a href="{{ $socialLink->url }}"
                                                                                           target="_blank">
                                                                                            <span><i class="fa fa-{{ $socialLink->link_type }}"
                                                                                                     aria-hidden="true"></i></span>
                                                                                            {{ ucfirst($socialLink->link_type) }}
                                                                    </a>
                                                                </li>
@endif
                                                    @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>-->
                                                        <section id="verein" class="tab-panel">
                                                            @if(trim($club->about) !== '' || trim($club->description) !== '')
                                                                @if(isset($club->about))
                                                                    <div class="row">
                                                                        <div class="col-lg-12">
                                                                            <div class="shaddow-box">
                                                                                <h3>Über diesen Verein</h3>
                                                                                <p>{!! nl2br($club->about) !!}</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endif

                                                                @if(isset($club->description))
                                                                    <div class="row">
                                                                        <div class="col-lg-12">
                                                                            <div class="shaddow-box">
                                                                                <h3>Geschichte</h3>
                                                                                <p>{!! nl2br($club->description) !!}</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            @elseif(Auth::check() && policy($club)->edit(Auth::user(), $club))
                                                                <div class="club-content__box">
                                                                    Keine Informationen eingetragen
                                                                </div>
                                                            @endif
                                                        </section>

                                                        <section id="trainingsorte" class="tab-panel">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="well">
                                                                        @include('club.partials.trainings-orte-teaser', ['club' => $club])
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </section>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        @can('edit', $club)
                                            <div class="club-content__edit-area">
                                                @include('club.partials.edit.about-history', ['club' => $club])
                                            </div>
                                        @endcan

                                    </section>

                                @endif

                                {{-- achievments :trophy: --}}
                                @if(($achievements->count() > 0 || (Auth::check() && policy($club)->edit(Auth::user(), $club))) && $club->administrators->count() > 0)
                                    <section id="sportliche-erfolge"
                                             class="pageable-menu__page pageable-menu__page--active">
                                        <h2 class="club-content__box-headline">Sportliche Erfolge
                                            @if(Auth::check() && policy($club)->edit(Auth::user(), $club))
                                                <a class="pull-right"
                                                   onClick="$('#add-erfolge').toggleClass('pageable-menu__page--active');"
                                                   href="#add-erfolge">
                                                    <span><i @if($club->title_color) style="color:#{{ $club->title_color }}"
                                                             @endif class="fa fa-plus-circle"
                                                             aria-hidden="true"></i></span>
                                                </a>
                                            @endif
                                        </h2>

                                        @if(Auth::check() && policy($club)->edit(Auth::user(), $club))
                                            @include('club.partials.edit.achievement', ['newAchievement' => true])
                                        @endif

                                        <div class="club-post__wrapper">
                                            @if($achievements->count() > 0)
                                                <div data-load-more='{"url": "{{ route('club.achievement.latest', $club->slug) }}"}'>
                                                    <div data-load-more-html></div>

                                                    <div class="col-md-12">
                                                        <div data-loader
                                                             class="loader__wrapper loader__wrapper--centered">
                                                            <div class="loader loader--loading"></div>
                                                            <div class="loader loader--loading"></div>
                                                        </div>
                                                    </div>
                                                    @if($achievements->count() > 9)
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="button__center-wrapper">
                                                                    <a data-load-more-button
                                                                       class="btn btn-default interessiert-btn">
                                                                        MEHR ANZEIGEN
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                </div>
                                            @elseif(Auth::check() && policy($club)->edit(Auth::user(), $club))
                                                <div class="club-post__wrapper">
                                                    <div class="club-post__item">
                                                        <span class="club-post__date noborder">vereinsleben.de</span>
                                                        <div class="club-post__content">
                                                            Es sind noch keine sportlichen Erfolge für deinen Verein
                                                            eingetragen
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </section>
                                @endif

                                @if(Auth::check() && $club->administrators->count() > 0)
                                  <section id="events" class="pageable-menu__page pageable-menu__page--active">
                                    <h2 class="club-content__box-headline">KOMMENDE EVENTS
                                        @can('edit', $club)
                                            <a class="pull-right"
                                               onClick="$('#add-event').toggleClass('pageable-menu__page--active');"
                                               href="#add-event">
                                                <span><i @if($club->title_color) style="color:#{{ $club->title_color }}"
                                                         @endif class="fa fa-plus-circle" aria-hidden="true"></i></span>
                                            </a>
                                        @endcan
                                    </h2>

                                    @can('edit', $club)
                                        @include('club.partials.edit.event', ['newEvent' => true])
                                    @endcan

                                    <div class="club-post__wrapper">
                                        @if($events->count() > 0)
                                            <div data-load-more='{"url": "{{ route('event.latest', $club->slug) }}"}'>
                                                <div data-load-more-html></div>

                                                <div class="col-md-12">
                                                    <div data-loader
                                                         class="loader__wrapper loader__wrapper--centered">
                                                        <div class="loader loader--loading"></div>
                                                        <div class="loader loader--loading"></div>
                                                    </div>
                                                </div>
                                                @if($events->count() > 9)
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="button__center-wrapper">
                                                                <a data-load-more-button
                                                                   class="btn btn-default interessiert-btn">
                                                                    MEHR ANZEIGEN
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        @else
                                            <div class="club-post__item">
                                                <span class="club-post__date noborder">vereinsleben.de</span>
                                                <div class="club-post__content">
                                                    Keine Events vorhanden
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </section>

                                  <section id="netzwerk" class="pageable-menu__page pageable-menu__page--active">
                                    <h2 class="club-content__box-headline">EUER NETZWERK</h2>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="tabset-mob euer">
                                                <div class="tabset">
                                                    <input type="radio" name="user_type" value="fan" id="tab4"
                                                           aria-controls="fan"
                                                           checked="">
                                                    <label for="tab4" class="fantab">Fans</label>
                                                    <input type="radio" name="user_type" value="member" id="tab5"
                                                           aria-controls="member">
                                                    <label for="tab5" class="mitglieder">Mitglieder</label>
                                                    <input type="radio" name="user_type" value="old_member" id="tab6"
                                                           aria-controls="old_member">
                                                    <label for="tab6" class="ehemalige"> Ehemalige Mitglieder</label>
                                                    <input type="radio" name="user_type" value="vereinen" id="tab7"
                                                           aria-controls="vereinen">
                                                    <label for="tab7" class="vereine">Vereine</label>
                                                    <!--<input type="radio" name="user_type" value="verbände" id="tab8"
                                                           aria-controls="verbände">
                                                        <label for="tab8" class="last">Verbände</label>-->


                                                    <div id="network-search-area" class="row network-search-area">
                                                        <div class="col-lg-5 col-sm-12">
                                                            <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-search"
                                                                                               aria-hidden="true"></i></span>
                                                                <input type="text" name="search" id="search"
                                                                       class="form-control"
                                                                       placeholder="Namen Suchen ...">
                                                            </div>
                                                        </div>
                                                        {{--<div class="col-lg-6 col-sm-6 invite-membrs">--}}
                                                        {{--<a href="#">Neue Mitglieder einladen <i--}}
                                                        {{--class="fa fa-angle-right"--}}
                                                        {{--aria-hidden="true"></i></a>--}}
                                                        {{--</div>--}}
                                                    </div>

                                                    <div class="tab-panels">

                                                        <section id="fan" class="tab-panel">
                                                            @if($fans->count() > 0)
                                                                @include('club.partials.user.fan')
                                                            @endif
                                                        </section>

                                                        <section id="member" class="tab-panel">
                                                            @if($members->count() > 0)
                                                                @include('club.partials.user.member')
                                                            @endif
                                                        </section>

                                                        <section id="old_member" class="tab-panel">
                                                            @if($club->oldMembers->count() > 0)
                                                                @include('club.partials.user.old-member')
                                                            @endif
                                                        </section>

                                                        <section id="vereinen" class="tab-panel">
                                                            @if($club->getFriendsCount() > 0 )
                                                                @include('club.partials.user.vereinen')
                                                            @endif
                                                        </section>

                                                    <!--<section id="verbände" class="tab-panel">
                                                        @if($bandage != null)
                                                        @include('bandage.partials.bandage-card', ['bandage' => $bandage])
                                                    @endif
                                                            </section>-->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </section>

                                  <section id="netzwerk-news" class="pageable-menu__page pageable-menu__page--active">
                                    <h2 class="club-content__box-headline">Netzwerk-News</h2>

                                    @if($networkOverview->count() > 0)
                                        <div data-load-more='{"url": "{{ route('network.post.overview', $club->slug) }}"}'>
                                            <div data-load-more-html></div>

                                            <div class="col-md-12">
                                                <div data-loader class="loader__wrapper loader__wrapper--centered">
                                                    <div class="loader loader--loading"></div>
                                                    <div class="loader loader--loading"></div>
                                                </div>
                                            </div>
                                            @if($networkOverview->count() > 14)
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="button__center-wrapper">
                                                            <a data-load-more-button
                                                               class="btn btn-default interessiert-btn">
                                                                Weitere Einträge laden
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    @else
                                        <div class="club-post__item">
                                            <span class="club-post__date">vereinsleben.de</span>
                                            <div class="club-post__content">
                                                Keine Einträge vorhanden
                                            </div>
                                        </div>
                                    @endif
                                </section>
                                @endif
                                    <section id="bewertungen" class="pageable-menu__page pageable-menu__page--active">
                                        <h2 class="club-content__box-headline">Bewertungen</h2>
                                        <p><strong>Du kennst diesen Verein?</strong> Dann bewerte ihn doch einfach! Mit diesen Informationen hilfst Du anderen Menschen bei der Suche nach passenden Vereinen.</p>
                                        @foreach (['warning', 'error', 'success', 'info'] as $level)
                                            @if (Session::has('rating-'.$level))
                                                <div class="flash-message flash-message--{{$level}}">
                                                    @if(is_array(Session::get('rating-'.$level)))
                                                        @foreach(Session::get('rating-'.$level) as $error)
                                                            <li>{{$error}}</li>
                                                        @endforeach
                                                    @else
                                                        {!! Session::get('rating-'.$level) !!}
                                                    @endif
                                                </div>
                                            @endif
                                        @endforeach
                                        <div class="reviews-form">
                                            <div class="text-right">
                                                @if(Auth::check())
                                                <button <?php echo ((Auth::check() && Auth::user()->isClubAdmin($club))) || !Auth::check() ? 'disable' : '' ?> id="button-show-form"
                                                        class="button-show-form btn btn--inline <?php echo !(Auth::check() && Auth::user()->isClubAdmin($club) || !Auth::check()) ? 'button--light-white' : '' ?>
                                                        <?php echo (Auth::check() && $club->administrators->count() == 0) ? ' hide' : ''?>">Jetzt diesen Verein bewerten</button>
                                                @elseif(!Auth::check())
                                                <form action="{{route('review.ratingNoLogin')}}" method="POST">
                                                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                  <button class="button-show-form btn btn--inline" type="submit">
                                                    Jetzt diesen Verein bewerten
                                                  </button>
                                                </form>
                                                @endif
                                            </div>
                                            @if (Auth::check() && !Auth::user()->isClubAdmin($club) || (Auth::check() && Auth::user()->isAdmin()))
                                                <form action="{{route('reviews.store')}}" method="POST">
                                                    <div class="row form <?php echo !(Auth::check() && $club->administrators->count() == 0) ? 'hide' : ''?>">
                                                        <div class="col-lg-4 stars rate">
                                                            <input <?php echo Session::get('ratingTmp') == 5 ? 'checked' : '' ?> type="radio" id="star5" name="rating" value="5" />
                                                            <label for="star5">5 stars</label>
                                                            <input <?php echo Session::get('ratingTmp') == 4 ? 'checked' : '' ?> type="radio" id="star4" name="rating" value="4" />
                                                            <label for="star4">4 stars</label>
                                                            <input <?php echo Session::get('ratingTmp') == 3 ? 'checked' : '' ?> type="radio" id="star3" name="rating" value="3" />
                                                            <label for="star3">3 stars</label>
                                                            <input <?php echo Session::get('ratingTmp') == 2 ? 'checked' : '' ?> type="radio" id="star2" name="rating" value="2" />
                                                            <label for="star2">2 stars</label>
                                                            <input <?php echo Session::get('ratingTmp') == 1 ? 'checked' : '' ?> type="radio" id="star1" name="rating" value="1" />
                                                            <label for="star1">1 star</label>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <input type="hidden" name="club_id" value="{{$club->id}}">
                                                            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                                                            <input type="hidden" name="parent_id" value="0">
                                                            <textarea cols="30" rows="10" name="comment"><?php echo Session::get('commentTmp') ? Session::get('commentTmp') : '' ?></textarea>
                                                            <div class="text-right">
                                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                <button id="button-show-form" class="btn btn--inline button--light-white">Bewertung absenden</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            @endif
                                        </div>
                                        @if(isset($reviews) && count($reviews) > 0)
                                            <div class="review-list">
                                                <?php
                                                foreach ($reviews as $key => $review) { ?>
                                                <div class="review">
                                                    @if(Auth::check() && Auth::user()->isAdmin())
                                                        <div class="delete-rating">
                                                            <form action="{{route('reviews.destroy', $review->id)}}" method="POST">
                                                                @method('DELETE')
                                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                <button type="submit">
                                                                    <i class="fa fa-trash"></i>
                                                                </button>
                                                            </form>
                                                        </div>
                                                    @endif
                                                    <div class="row">
                                                        <div class="col-lg-9 detail">
                                                            <div class="row">
                                                                <div class="col-lg-5 rating">
                                                                    @for ($i=1; $i <= 5 ; $i++)
                                                                        {{--<span class="fa fa-star{{ ($i <= $review->rating) ? '' : '-o'}}"></span>--}}
                                                                    <span class="star {{ ($i <= $review->rating) ? '' : 'no-active'}}">★</span>
                                                                    @endfor
                                                                </div>
                                                                <div class="col-lg-7 date">
                                                                    <strong>Bewertung</strong><span> vom {{ \Carbon\Carbon::parse($review->created_at)->format('d.m.Y, g:i A')}}</span>
                                                                </div>
                                                            </div>
                                                            <div class="row comment">
                                                                <div class="col-lg-12">
                                                                    <p>{{$review->comment}}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <span class="line"></span>
                                                        <div class="col-lg-3 info">
                                                          <?php
                                                            $filename= asset($review->user()->first()->avatar->url('singleView'));
//                                                            if (!@fopen($filename, "r") || (strpos($filename, 'default-avatars.jpg') !== false)) {
//	                                                            $filename = '/default_images/rating/avatar-default-user-rating.png';
//                                                            }
	                                                        ?>
                                                            <img class="profile-avatar__image" src="{{ $filename }}"
                                                                 data-upload-image/>
                                                            <strong class="name">{{$review->user()->first()->fullname ? $review->user()->first()->fullname :
                                                            $review->user()->first()->firstname . ' ' . $review->user()->first()->lastname}}</strong>
                                                            <span>{{$review->user()->first()->city ? $review->user()->first()->city : ''}}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                @if (Auth::check() && Auth::user()->isClubAdmin($club))
                                                    <div class="action">
                                                        <span class="reply" data-review-id="{{$review->id}}">Auf Kommentar antworten</span> |
                                                        @if ($review->reviewReportByUserId(Auth::user()->id)->first()  == null)
                                                            <form action="{{route('review-reports.store')}}" method="POST">
                                                                <input type="hidden" name="review_id" value="{{$review->id}}">
                                                                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                <button type="submit">
                                                                    Kommentar melden {{--Report--}}
                                                                </button>
                                                            </form>
                                                        @elseif($review->reviewReportByUserId(Auth::user()->id)->first() !== null)
                                                            <form action="{{route('review-reports.destroy', $review->reviewReportByUserId(Auth::user()->id)->first()->id)}}" method="POST">
                                                                @method('DELETE')
                                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                <button type="submit">
                                                                    Meldung abbrechen {{--Cancel Report--}}
                                                                </button>
                                                            </form>
                                                        @endif
                                                    </div>
                                                @elseif(Auth::check() && !Auth::user()->isClubAdmin($club))
                                                    @if(Auth::user()->id != $review->user_id)
                                                        <div class="action">
                                                            @if ($review->reviewReportByUserId(Auth::user()->id)->first()  == null)
                                                                <form action="{{route('review-reports.store')}}" method="POST">
                                                                    <input type="hidden" name="review_id" value="{{$review->id}}">
                                                                    <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                    <button type="submit">
                                                                        Diesen Kommentar als unangemessen melden {{--Report--}}
                                                                    </button>
                                                                </form>
                                                            @elseif($review->reviewReportByUserId(Auth::user()->id)->first() != null)
                                                                <form action="{{route('review-reports.destroy', $review->reviewReportByUserId(Auth::user()->id)->first()->id)}}" method="POST">
                                                                    @method('DELETE')
                                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                    <button type="submit">
                                                                        Meldung abbrechen {{--Cancel Report--}}
                                                                    </button>
                                                                </form>
                                                            @endif
                                                        </div>
                                                    @endif
                                                @endif
                                                @if(isset($review->children) && count($review->children) > 0)
                                                    @foreach($review->children as $childReview)
                                                        <div class="row no-margin">
                                                            <div class="review col-lg-9 child">
                                                                @if(Auth::check() && Auth::user()->isAdmin())
                                                                    <div class="delete-rating">
                                                                        <form action="{{route('reviews.destroy', $childReview->id)}}" method="POST">
                                                                            @method('DELETE')
                                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                            <button type="submit">
                                                                                <i class="fa fa-trash"></i>
                                                                            </button>
                                                                        </form>
                                                                    </div>
                                                                @endif
                                                                <div class="detail">
                                                                    <div class="row">
                                                                        <div class="col-lg-12 date no-padding">
                                                                            <strong>Antwort des Vereins</strong><span> vom {{ \Carbon\Carbon::parse($childReview->created_at)->format('d.m.Y, g:i A')}}</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row comment">
                                                                        <div class="col-lg-12">
                                                                            <p>{{$childReview->comment}}</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endif
                                                <?php }
                                                ?>
                                            </div>
                                            @if (Auth::check() && Auth::user()->isClubAdmin($club))
                                                <form id="answer-form" action="{{route('reviews.store')}}" method="POST">
                                                    <div class="row form hide">
                                                        <div class="col-lg-8">
                                                            <strong>Auf den Kommentar antworten</strong>
                                                            <input type="hidden" name="club_id" value="{{$club->id}}">
                                                            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                                                            <input type="hidden" id="parent_id" name="parent_id" value="{{$reviews[0]->id}}">
                                                            <input type="hidden" name="rating" value="0">
                                                            <textarea cols="30" rows="10" name="comment"></textarea>
                                                            <div class="text-right">
                                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                <button id="button-show-form" class="btn btn--inline button--light-white">Antwort absenden</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            @endif
                                        @endif
                                        @if ((Auth::user() && $club->administrators->count() == 0 && !Auth::user()->hasClubAdminRequested($club)) || !Auth::user() || $club->administrators->count() == 0)
                                            <div class="review-list no-login">
                                                <div class="review col-lg-9 child">
                                                    <div class="row">
                                                        <div class="col-lg-12 detail">
                                                            <strong class="head">Auf Kommentare antworten oder als unangemessen melden?</strong>
                                                            <strong>Kein Problem!</strong> <a href="/register">Einfach ein Nutzerkonto anlegen</a> oder
                                                            <a href="/login">mit Deinen<br>Zugangsdaten anmelden</a> und den Kommentar als unangemessen <br>
                                                            melden. Oder das <a id="scroll-to-request-admin" href="#">Vereinsprofil übernehmen</a> und Kommentare beantworten.
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </section>

                                  @if(Auth::check() && $club->administrators->count() > 0)
                                    <section id="abteilungen"
                                             class="pageable-menu__page pageable-menu__page--active"></section>
                                  @endif

                                    @if($sports->count() > 0 && Auth::check() && $club->administrators->count() > 0)

                                        @foreach($sports as $key => $sport)
                                            <section id="abteilungen{{$sport->id}}"
                                                     class="pageable-menu__page pageable-menu__page--active">
                                                <input type="hidden" name="sportIdArr[]" value="{{ $sport->id }}"/>
                                                @include('club.partials.department', [
												                        'key' => $key,
                                                                        'sport' => $sport,
                                                                        'department' => $sport->department,
                                                                        'club' => $club,
                                                                        'contact' => $sport->contact,
                                                                        'teams' => $sport->teams,
                                                                        'users' => $usersarray,
                                                                        'departmentexist' => $sport->departmentexist,
                                                                    ])
                                            </section>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

    @if(Auth::check())
        @include('user.profile.modal.profile-info-wizard', ['me' => Auth::user(), 'state' => $state])
    @endif

    {{--@endif--}}
    <script src="{{ asset(('js/jscolor.js')) }}"></script>
    <script>

        function showHiddenSports(){

            var sportarray = document.getElementsByClassName('sporthide');

            if(document.getElementById("sportsmore").text == "Weniger anzeigen"){

                console.log('Weniger anzeigen');

                Array.prototype.forEach.call(sportarray, function(el){
                    el.setAttribute("style", "display:none;");
                });

                document.getElementById("sportsmore").text = "Mehr anzeigen";
            }
            else{

                Array.prototype.forEach.call(sportarray, function(el){
                    el.removeAttribute("style", "display");
                });

                document.getElementById("sportsmore").text = "Weniger anzeigen";
            }

            // document.getElementById("sportsmore").style.display = "none";
        }

        function setHeaderColor(picker) {
            document.getElementById('about-strip').style["boxShadow"] = '2px 2px 6px #' + picker.toString();
            document.getElementById('profile-pic-icon').style.background = '#' + picker.toString();
        }

        function setTextColor(picker, colorcode = '') {
            var color = picker.toString();
            if (colorcode) {
                color = colorcode;
            }
            document.getElementById('clubname').style.color = '#' + color;
            document.getElementById('post-location-links').style.color = '#' + color;
            document.getElementById('bin_fan').style.color = '#' + color;
            document.getElementById('bin_member').style.color = '#' + color;
            document.getElementById('color-publish').style.background = '#' + color;
            var clubsports = document.getElementsByClassName("club_sports");
            for (i = 0; i < clubsports.length; i++) {
                clubsports[i].style.backgroundColor = '#' + color;
            }
            var plusicons = document.getElementsByClassName("fa-plus-circle");
            for (i = 0; i < plusicons.length; i++) {
                plusicons[i].style.color = '#' + color;
            }
            var menus = document.getElementsByClassName("club-detail__menu-list-link active");
            for (i = 0; i < menus.length; i++) {
                menus[i].style.color = '#' + color;
                menus[i].style.borderLeft = '3px solid #' + color;
            }
            var css = '.btn--fan:after {background:#' + color + ' url(/static_images/fan.png)}';
            var css = css + '.btn--member:after {background:#' + color + ' url(/static_images/fan.png)}';
            var css = css + '.btn--fan:hover:after {background:#' + color + ' url(/static_images/fan-active.png)}';
            var css = css + '.btn--member:hover:after {background:#' + color + ' url(/static_images/fan-active.png)}';
            var css = css + '.profile-tabs li a:hover{ color: #' + color + ';border-left:3px solid #' + color + '}';
            var style = document.createElement('style');
            if (style.styleSheet) {
                style.styleSheet.cssText = css;
            } else {
                style.appendChild(document.createTextNode(css));
            }
            document.getElementsByTagName('body')[0].appendChild(style);
        }

        function assignVal(val) {
            if (val == 1) {
                titlecolor = document.getElementById('header_color').value;
                document.getElementById('title_color').value = titlecolor;
                document.getElementById('title_color').style.backgroundColor = '#' + titlecolor;
                setTextColor('', titlecolor);
            } else {
                document.getElementById('header_color').value = document.getElementById('title_color').value;
                document.getElementById('header_color').style.backgroundColor = '#' + document.getElementById('title_color').value;
                document.getElementById('about-strip').style["boxShadow"] = '2px 2px 6px #' + document.getElementById('title_color').value;
                document.getElementById('profile-pic-icon').style.background = '#' + document.getElementById('title_color').value;
            }
        }
    </script>
    @if($club->header_color OR $club->title_color)
        <style type="text/css" rel="stylesheet">
            @if($club->title_color)
		    .btn--fan:after {
                background: {{ '#'.$club->title_color}}  url('/static_images/fan.png');
            }

            .btn--fan:hover:after {
                background: {{ '#'.$club->title_color}}  url('/static_images/fan-active.png')
            }

            .btn--member:after {
                background: {{ '#'.$club->title_color}}  url('/static_images/fan.png');
            }

            .btn--member:hover:after {
                background: {{ '#'.$club->title_color}}  url('/static_images/fan-active.png');
            }

            .btn--active.btn--fan:after {
                background: {{ '#'.$club->title_color}}  url('/static_images/fan-active.png');
            }

            .btn--active.btn--member:after, .btn--active.btn--member:focus:after {
                background: {{ '#'.$club->title_color}}  url('/static_images/fan-active.png');
            }

            .btn--active.btn--member:hover:after {
                background: {{ '#'.$club->title_color}}  url('/static_images/fan-active.png');
            }

            .profile-tabs li a.active {
                color: #{{$club->title_color}};
                border-left: 3px solid #{{$club->title_color}};
            }

            .profile-tabs li a:hover {
                color: #{{$club->title_color}};
                border-left: 3px solid #{{$club->title_color}};
            }

            @endif
	        @if($club->title_color)
		    .profile-name img {
                background: #{{$club->header_color}};
            }
            @endif
        </style>
    @endif
    @push('scripts')
        <script>
            $(document).ready(function () {
               $('#button-show-form').click(function(){
                   $(this).addClass('hide');
                   $('.form').removeClass('hide');
               });

               $('.reply').click(function(){
                   $('.reply').removeClass('active');
                   $(this).addClass('active');

                   let review_id = $(this).data('review-id');
                   $('input#parent_id').val(review_id);

                   $('#answer-form .form').removeClass('hide');

                   $([document.documentElement, document.body]).animate({
                       scrollTop: $("#answer-form").offset().top - 500
                   }, 200);
               });

               $('#scroll-to-request-admin').click(function(){
                   $([document.documentElement, document.body]).animate({
                       scrollTop: $("#request-admin").offset().top - 500
                   }, 500);
               });
            })
        </script>
    @endpush
@endsection