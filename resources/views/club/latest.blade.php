@if($clubs->count() > 0)
    <div data-load-more='{"url": "{{ route('club.latest') }}"}'>
        <section class="recent-club-overview">
            <div class="container container--full-width">
                <h2 class="sub-headline sub-headline--bordered headline--centered">Neuste Vereine aus der Community</h2>
                <div class="row">
                    <div data-load-more-html></div>

                    <div class="col-xs-12">
                        @include('components.loader')
                    </div>

                    <div class="col-xs-12">
                        <div class="button__center-wrapper">
                            <button data-load-more-button class="button button--padded button--dark">
                                Weitere Vereine laden
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@else
@endif