@extends('layouts.master')

@section('content')
    <section class="club-new pfl-clb-frm">
        <div class="club-detail__main-image"
             style="background-image: url('{{ asset($club->header->url('singleView')) }}')"
             @can('edit', $club) data-section-editable="true" @endcan>
        </div>
        <div class="club-detail-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <div class="club-detail__avatar club-detail__avatar--create-form">
                            <img class="club-detail__image"
                                 src="{{ asset($club->avatar->url('singleView')) }}"
                                 alt="">
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-md-9 col-xs-12 ">
                        <h1 class="club-detail__headline">Verein erstellen</h1>

                        @if(count($errors) > 0)
                            <div class="flash-message flash-message--error">
                                <strong>Es ist ein Fehler aufgetreten:</strong>
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="club-content__edit-area club-content__box">
                            @include('club.partials.edit.base-data', ['club' => $club])
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection