<div class="club-post__item" data-edit-wrapper>
    <div class="row">
        <div class="col-lg-12 edit-del-btn">
            @if(Auth::check() && policy($club)->edit(Auth::user(), $club))
                <div class="club-content__options">
                    <a href="#" class="club-content__options-button"
                       data-edit='{ "club_id": "{{ $club->id }}",
                       "resource": { "id": "{{ $achievement->id }}",
                       "type": "achievement",
                       "url": "{{ route('club.achievement.edit', [$club->slug, $achievement->id]) }}"
                       } }'>
                        <span class="fa fa-pencil club-content__options-button-edit"></span>
                    </a>
                    <a href="#" class="club-content__options-button"
                       data-delete='{ "club_id": "{{ $club->id }}",
                       "resource": { "id": "{{ $achievement->id }}",
                       "type": "achievement",
                       "url": "{{ route('club.achievement.delete', [$club->slug, $achievement->id]) }}"
                       } }'>
                        <span class="fa fa-trash club-content__options-button-remove"></span>
                    </a>
                </div>
            @endif
        </div>
    </div>
    <div data-edit-content>
        <div class="row">
            <div class="col-lg-1 col-md-1 club-post__date noborder">
                <span class="date-no">{{ date('d', strtotime($achievement->getFormattedDateWithOptionalFields())) }}
                    .</span><br>
                <span class="month-no">{{ strtoupper(date('M', strtotime($achievement->getFormattedDateWithOptionalFields()))) }}</span><br>
                <span class="year-no">{{ date('Y', strtotime($achievement->getFormattedDateWithOptionalFields())) }}</span>
            </div>
            <div class="col-lg-11 col-sm-11 col-xs-12 event-post">
                <div class="event-post-single-thum">
                    @if($achievement->picture->originalFilename() !== null)
                        <a href="{{ asset($achievement->picture->url('singleView')) }}"
                           data-lightbox="achievement"><img
                                    src="{{ asset($achievement->picture->url('singleView')) }}"/></a>
                    @endif
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            @if($achievement->picture->originalFilename() !== null)
                                <div class="event-post-thum-arrow"><i class="fa fa-caret-up"
                                                                      aria-hidden="true"></i></div>
                            @endif
                            <div class="row post-head">
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 postlogo">
                                    <img src="{{ asset($club->avatar->url('singleView')) }}"/>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-8 postdetail">
                                    <div class="postlefttxt">{{ $club->shorthand }}</div>
                                    <div class="post-blue">{{ $achievement->title }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <p>{!! nl2br($achievement->description) !!}</p>
            </div>
        </div>
    </div>
</div>