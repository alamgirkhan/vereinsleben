<div class="col-xs-12 col-sm-6 col-md-6 fanuserlead">
    <div class="fan-box">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 club-result-thum">
                <div class="thum-bg">
                    <a href="{{ route('club.detail', $club->slug) }}"><img
                                src="{{ $club->avatar->url('singleView') }}" alt="">
                    </a>
                </div>
            </div>
            <div class="col-lg-9 col-sm-9 col-xs-8 fanuserlead-detail">
                <div class="fan-box-detail">
                    <h3><a href="{{ route('club.detail', $club->slug) }}" class="member-card__link">
                            {{ $club->name }}
                        </a>
                    </h3>
                </div>
                <div class="fan-lctn"><i class="fa fa-map-marker" aria-hidden="true"></i> {{ $club->city }}</div>
            </div>
        </div>
    </div>
</div>

