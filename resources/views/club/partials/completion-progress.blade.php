<aside class="club-detail__completion-progress-wrapper">
    <div class="meter">
        <span style="width: {{ $completionProgress['progress_percentage'] }}%"></span>
        <label>{{ $completionProgress['progress_percentage'] }}% des Profils komplett</label>
    </div>

    @if(count($completionProgress['unsolved_criteria']) > 0)
        <p class="club-detail__completion-progress-recommendation">Am besten du hinterlegst noch folgendes:</p>

        <ul class="club-detail__completion-progress-recommendation-list">
        @foreach($completionProgress['unsolved_criteria'] as $criteria)
            <li class="club-detail__completion-progress-recommendation-listitem">
                {{ $criteria['title'] }} (<span class="club-detail__completion-progress-pluspercent">+{{ $criteria['weight'] }}%</span>)
            </li>
        @endforeach
        </ul>
    @endif
</aside>