<h3>TEST</h3>
<div data-edit-wrapper>
    @if(Auth::check() && policy($club)->edit(Auth::user(), $club))
        <div class="col-xs-11 edit-del-btn">
            <div class="club-content__options">
                <a href="#" class="club-content__options-button" title="Bearbeiten"
                   data-edit='{ "club_id": "{{ $club->id }}",
			   "resource": { "id": "{{ $department->id }}",
			   "type": "department",
			   "url": "{{ route('department.edit', [$club->slug, $department->id]) }}"
			   } }'>
                    <span class="fa fa-pencil club-content__options-button-edit"></span>
                </a>
                <a href="#" class="club-content__options-button club-content__options-button--hidden"
                   title="Bearbeiten abbrechen"
                   data-cancel='{ "club_id": "{{ $club->id }}",
			   "resource": { "id": "{{ $department->id }}",
			   "type": "department",
			   "url": "{{ route('department.single', [$club->slug, $department->id]) }}"
			   } }'>
                    <span class="fa fa-times club-content__options-button-cancel"></span>
                </a>
                <a href="#" class="club-content__options-button" title="Löschen"
                   data-delete='{ "club_id": "{{ $club->id }}",
			   "resource": { "id": "{{ $department->id }}",
			   "type": "department",
			   "url": "{{ route('department.delete', [$club->slug, $department->id]) }}"
			   } }'>
                    <span class="fa fa-trash club-content__options-button-remove"></span>
                </a>
                <a href="#" class="club-content__options-button" title="Mannschaften hinzufügen"
                   data-edit='{ "club_id": "{{ $club->id }}",
			   "resource": { "id": "{{ $department->id }}",
			   "type": "department",
			   "url": "{{ route('team.add', $department->id) }}"
			   } }'>
                    <span class="fa fa-plus club-content__options-button-edit"></span>
                </a>
            </div>
        </div>
    @endif
    <div data-edit-content>
        <table class="table table-bordered">
            <tr>
                <td><strong>Name: </strong>{{$department->name}}</td>
                <td><strong>Sport: </strong>{{$sport}}</td>
                <td><strong>Contact Person: </strong>{{$contactperson}}</td>
            </tr>
        </table>
        @if($teams->count() > 0)
            @foreach($teams as $team)
                <div class="" data-edit-wrapper>
                    <div class="">
                        @if(Auth::check() && policy($club)->edit(Auth::user(), $club))
                            <div class="col-xs-11 edit-del-btn">
                                <div class="club-content__options">
                                    <a href="#" class="club-content__options-button" title="Bearbeiten"
                                       data-edit='{ "club_id": "{{ $club->id }}",
									   "resource": { "id": "{{ $team->id }}",
									   "type": "team",
									   "url": "{{ route('team.edit', $team->id) }}"
									   } }'>
                                        <span class="fa fa-pencil club-content__options-button-edit"></span>
                                    </a>
                                    <a href="#"
                                       class="club-content__options-button club-content__options-button--hidden"
                                       title="Bearbeiten abbrechen"
                                       data-cancel='{ "club_id": "{{ $club->id }}",
									   "resource": { "id": "{{ $team->id }}",
									   "type": "team",
									   "url": "{{ route('team.single', [$club->slug, $team->id]) }}"
									   } }'>
                                        <span class="fa fa-times club-content__options-button-cancel"></span>
                                    </a>
                                    <a href="#" class="club-content__options-button" title="Löschen"
                                       data-delete='{ "club_id": "{{ $club->id }}",
									   "resource": { "id": "{{ $team->id }}",
									   "type": "team",
									   "url": "{{ route('team.delete', $team->id) }}"
									   } }'>
                                        <span class="fa fa-trash club-content__options-button-remove"></span>
                                    </a>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div data-edit-content class="">
                        <table class="table table-bordered">
                            <?php $teamcontactperson = $team->contacts()->get()->first(); ?>
                            <tr>
                                <td style="width:25%;background: #f4f4f4;"><strong>Team Name: </strong></td>
                                <td>{{$team->name}}</td>
                                <td style="width:25%;background: #f4f4f4;"><strong>Contact Person: </strong></td>
                                <td>{{$teamcontactperson->firstname}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
</div>