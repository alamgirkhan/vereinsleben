<h2 class="club-content__box-headline departhead">ABTEILUNG {{$sport->title}}</h2>
@can('edit', $club)
    @if(!$departmentexist)
        @include('club.partials.edit.department', ['newDepartment' => $departmentexist, 'sport_id' => $sport->id , 'users' => $users ])
    @endif
@endcan

@if($departmentexist)
    <div class="row">
        <div class="col-md-12">
            <div data-edit-wrapper>
                <div data-edit-content class="acdn-cnt">
                    @if(Auth::check() && policy($club)->edit(Auth::user(), $club))
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 edit-del-btn frmedit">
                            <div class="club-content__options">
                                <a href="#" class="club-content__options-button" title="Bearbeiten"
                                   data-edit='{ "club_id": "{{ $club->id }}",
				   "resource": { "id": "{{ $department->id }}",
				   "type": "department",
				   "url": "{{ route('department.edit', [$club->slug, $department->id]) }}"
				   } }'>
                                    <span class="fa fa-pencil club-content__options-button-edit"></span>
                                </a>
                                <a href="#" class="club-content__options-button club-content__options-button--hidden"
                                   title="Bearbeiten abbrechen"
                                   data-cancel='{ "club_id": "{{ $club->id }}",
				   "resource": { "id": "{{ $department->id }}",
				   "type": "department",
				   "url": "{{ route('department.single', [$club->slug, $department->id]) }}"
				   } }'>
                                    <span class="fa fa-times club-content__options-button-cancel"></span>
                                </a>
                            </div>
                        </div>
                    @endif
                    <p>{!! nl2br(e($department->description)) !!}</p>
                    <table cellpadding="10" cellspacing="10" data-token="{{ csrf_token() }}" class="partner-detail">
                        <tr>
                            <td style="padding-bottom:5px;">
                                <strong>Ansprechpartner</strong>
                            </td>
                        </tr>
                        <tr>
                            @if(isset($contact))
                                <td onclick='window.open("{{ route('user.detail', ['username' => $contact->username]) }}")'
                                    style="cursor: pointer;">

                                </td>
                            @endif
                        </tr><p><u>@if(isset($contact) && $contact->gender == 'male')Herr @elseif(isset($contact) && $contact->gender == 'female')
                                    Frau @else Nicht angegeben @endif @if(isset($contact)){{$contact->firstname}} {{$contact->lastname}}@else  @endif</u></p>
                        <tr>
                            <td colspan="2">
                                <p>Telefon:
                                    @if(isset($contact))
                                        {{$contact->phone}}
                                    @else

                                    @endif
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <p>E-Mail: <u><a href="mailto:@if(isset($contact)){{$contact->ap_email}}@endif">@if(isset($contact)){{$contact->ap_email}}@endif</a></u></p>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endif

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 acdn">
        <div data-edit-wrapper>
            <div data-edit-content>
                <h2 class="club-content__box-headline">MANNSCHAFTEN</h2>
                @if($departmentexist)
                    @if(Auth::check() && policy($club)->edit(Auth::user(), $club))
                        @if($teams->count() == 0)
                            <div class="row">
                                <div class="col-md-12 wichtig" style="margin-top:14px;">
                                    Hinweis: Es sind noch keine Mannschaften angelegt!
                                </div>
                            </div>
                        @endif
                    @endif
                @endif
                @if($departmentexist)
                    @if($teams->count() > 0)
                        <div id="accordion_{{ $sport->id }}">
                            @foreach($teams as $team)
                                <h3>{{$team->name}}</h3>
                                <div class="" data-edit-wrapper>
                                    <div class="">
                                        @if(Auth::check() && policy($club)->edit(Auth::user(), $club))
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 edit-del-btn">
                                                <div class="club-content__options">
                                                    <a href="#" class="club-content__options-button" title="Bearbeiten"
                                                       data-edit='{ "club_id": "{{ $club->id }}",
										   "resource": { "id": "{{ $team->id }}",
										   "type": "team",
										   "url": "{{ route('team.edit', $team->id) }}"
										   } }'>
                                                        <span class="fa fa-pencil club-content__options-button-edit"></span>
                                                    </a>
                                                    <a href="#"
                                                       class="club-content__options-button club-content__options-button--hidden"
                                                       title="Bearbeiten abbrechen"
                                                       data-cancel='{ "club_id": "{{ $club->id }}",
										   "resource": { "id": "{{ $team->id }}",
										   "type": "team",
										   "url": "{{ route('team.single', [$club->slug, $team->id]) }}"
										   } }'>
                                                        <span class="fa fa-times club-content__options-button-cancel"></span>
                                                    </a>
                                                <!--<a href="#" class="club-content__options-button" title="Löschen"
													   data-delete='{ "club_id": "{{ $club->id }}",
										   "resource": { "id": "{{ $team->id }}",
										   "type": "team",
										   "url": "{{ route('team.delete', $team->id) }}"
										   } }'>
														<span class="fa fa-trash club-content__options-button-remove"></span>
													</a>-->
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                    <div data-edit-content class="acdn-cnt">
                                        <?php $teamcontactperson = $team->contacts()->get()->first(); ?>
                                        <div class="col-md-12">
                                            <p><strong>Altersklasse:</strong> {{ $team->age_class }}</p>
                                        </div>
                                        <div class="col-md-12">
                                            <p><strong>Liga:</strong> {{ $team->league }}</p>
                                        </div>
                                        @if($team->picture_file_name)
                                            <div class="col-md-12" style="margin:10px 0">
                                                <img class="thumbnail__image"
                                                     src="{{asset($team->picture->url('singleView')) }}"/>
                                            </div>
                                        @endif
                                        <div class="col-md-12 teamdisp">
                                            <p>{!! nl2br(e($team->description)) !!}</p>
                                        </div>
                                        <div class="partner-space">
                                            <div class="col-md-12 ">
                                                <p><strong>Ansprechpartner</strong></p>
                                            </div>
                                            <div class="col-md-12">
                                                <p><u>@if(isset($teamcontactperson) && $teamcontactperson->gender == 'male')Herr @elseif(isset($teamcontactperson) && $teamcontactperson->gender == 'female') Frau @else Nicht angegeben @endif
                                                        @if(isset($teamcontactperson))
                                                            <span onclick='window.open("{{ route('user.detail', ['username' => $teamcontactperson->username]) }}")'
                                                                  style="cursor: pointer;">
                                                                {{$teamcontactperson->firstname}} {{$teamcontactperson->lastname}}
                                                            </span>
                                                        @else

                                                        @endif
                                                    </u>
                                                </p>
                                            </div>
                                            <div class="col-md-12">
                                                <p>Telefon:
                                                    @if(isset($teamcontactperson))
                                                        {{ $teamcontactperson->phone }}
                                                    @else

                                                    @endif
                                                </p>
                                            </div>
                                            <div class="col-md-12">
                                                <p>E-Mail: <u><a href="mailto:@if(isset($teamcontactperson)){{$teamcontactperson->ap_email}}@endif">
                                                            @if(isset($teamcontactperson))
                                                                {{$teamcontactperson->ap_email}}
                                                            @else

                                                            @endif
                                                        </a></u>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif
                @endif
            </div>
        </div>
        @if($departmentexist)
            @if(Auth::check() && policy($club)->edit(Auth::user(), $club))
                <div data-edit-wrapper>
                    <div data-edit-content class="acdn-addgroup">
                        <a href="#" title="Mannschaften hinzufügen"
                           data-edit='{ "club_id": "{{ $club->id }}",
						   "resource": { "id": "{{ $department->id }}",
						   "type": "department",
						   "url": "{{ route('team.add', $department->id) }}"
						   } }'>
                            <i class="fa fa-plus-circle" aria-hidden="true"></i>Mannschaften hinzufügen
                        </a>
                    </div>
                </div>
            @endif
        @endif
    </div>
</div>