@if(Auth::check() and $club->administrators->count() > 0)
    <aside>
        <nav>
            <ul class="club-detail__menu profile-tabs profile-menu-desk" data-pageable-menu>
                <li class="club-detail__menu-list-item active">
                    <a class="club-detail__menu-list-link" href="#pinnwand">
                        <span><i class="fa fa-newspaper-o" aria-hidden="true"></i></span>Pinnwand
                    </a>
                </li>
                @if((isset($club->description) || isset($club->about)) || (Auth::check() && policy($club)->edit(Auth::user(), $club)) || $club->id === null)
                    <li class="club-detail__menu-list-item">
                        <a class="club-detail__menu-list-link" href="#informationen">
                            <span><i class="fa fa-info" aria-hidden="true"></i></span>Informationen
                        </a>
                    </li>
                @endif
                @if($achievements->count() > 0 || (Auth::check() && policy($club)->edit(Auth::user(), $club)) || $club->id === null)
                    <li class="club-detail__menu-list-item">
                        <a class="club-detail__menu-list-link" href="#sportliche-erfolge">
                            <span><i class="fa fa-trophy" aria-hidden="true"></i></span>Erfolge
                        </a>
                    </li>
                @endif

                <li class="club-detail__menu-list-item">
                    <a class="club-detail__menu-list-link" href="#events">
                        <span><i class="fa fa-calendar" aria-hidden="true"></i></span>Events
                    </a>
                </li>

                <li class="club-detail__menu-list-item">
                    <a class="club-detail__menu-list-link" href="#netzwerk">
                        <span><i class="fa fa-users" aria-hidden="true"></i></span>Netzwerk
                    </a>
                </li>

                <li class="club-detail__menu-list-item">
                    <a class="club-detail__menu-list-link" href="#netzwerk-news">
                        <span><i class="fa fa-globe" aria-hidden="true"></i></span>Netzwerk-News
                    </a>
                </li>

                <li class="club-detail__menu-list-item">
                    <a class="club-detail__menu-list-link" href="#bewertungen">
                        <span><i class="fa fa-star" aria-hidden="true"></i></span>Bewertungen
                    </a>
                </li>

                <li class="club-detail__menu-list-item">
                    <a class="club-detail__menu-list-link edit-links" href="#abteilungen">
                        <span><i class="fa fa-tags" aria-hidden="true"></i></span>Abteilungen
                    </a>
                    @if($sports->count() > 0)
                        <ul id="userlinks" style="display:none;" class="club-detail__menu profile-tabs submenu"
                            data-pageable-menu>
                            @foreach($sports as $sport)
                                <li class="club-detail__menu-list-item">
                                    <a class="club-detail__menu-list-link" href="#abteilungen{{$sport->id}}">
                                        <i class="fa fa-angle-right"></i>
                                        {{$sport->title}}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </li>

                <!--<li class="club-detail__menu-list-item">
                    <a class="club-detail__menu-list-link" href="#downloads">
                        <span><i class="fa fa-download" aria-hidden="true"></i></span>Downloads
                    </a>
                </li>-->
            </ul>

            <ul class="club-detail__menu profile-tabs profile-menu-mob" data-pageable-menu>
                <li class="club-detail__menu-list-item active">
                    <a class="club-detail__menu-list-link" href="#pinnwand">
                        <span><i class="fa fa-newspaper-o" aria-hidden="true" href="#pinnwand"></i></span>
                    </a>
                </li>
                @if((isset($club->description) || isset($club->about)) || (Auth::check() && policy($club)->edit(Auth::user(), $club)) || $club->id === null)
                    <li class="club-detail__menu-list-item">
                        <a class="club-detail__menu-list-link" href="#informationen">
                            <span><i class="fa fa-info" aria-hidden="true" href="#informationen"></i></span>
                        </a>
                    </li>
                @endif
                @if($achievements->count() > 0 || (Auth::check() && policy($club)->edit(Auth::user(), $club)) || $club->id === null)
                    <li class="club-detail__menu-list-item">
                        <a class="club-detail__menu-list-link" href="#sportliche-erfolge">
                            <span><i class="fa fa-trophy" aria-hidden="true" href="#sportliche-erfolge"></i></span>
                        </a>
                    </li>
                @endif

                <li class="club-detail__menu-list-item">
                    <a class="club-detail__menu-list-link" href="#events">
                        <span><i class="fa fa-calendar" aria-hidden="true" href="#events"></i></span>
                    </a>
                </li>

                <li class="club-detail__menu-list-item">
                    <a class="club-detail__menu-list-link" href="#netzwerk">
                        <span><i class="fa fa-users" aria-hidden="true" href="#netzwerk"></i></span>
                    </a>
                </li>

                <li class="club-detail__menu-list-item">
                    <a class="club-detail__menu-list-link" href="#netzwerk-news">
                        <span><i class="fa fa-globe" aria-hidden="true" href="#netzwerk-news"></i></span>
                    </a>
                </li>

                <li class="club-detail__menu-list-item">
                    <a class="club-detail__menu-list-link edit-links" href="#abteilungen">
                        <span><i class="fa fa-tags" aria-hidden="true" href="#abteilungen"></i></span>
                    </a>
                    @if($sports->count() > 0)
                        <ul id="userlinks" style="display:none;" class="club-detail__menu profile-tabs"
                            data-pageable-menu>
                            @foreach($sports as $sport)
                                <li class="club-detail__menu-list-item">
                                    <a class="club-detail__menu-list-link" href="#abteilungen{{$sport->id}}">
                                        {{$sport->title}}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </li>

                <!--<li class="club-detail__menu-list-item">
                    <a class="club-detail__menu-list-link" href="#downloads">
                        <span><i class="fa fa-download" aria-hidden="true" href="#downloads"></i></span>
                    </a>
                </li>-->
            </ul>
        </nav>
    </aside>
@else
    <aside style="display:none;">
    <nav>
        <ul class="club-detail__menu profile-tabs profile-menu-desk" data-pageable-menu>
            <li class="club-detail__menu-list-item active">
                <a class="club-detail__menu-list-link" href="#pinnwand">
                    <span><i class="fa fa-newspaper-o" aria-hidden="true"></i></span>Pinnwand
                </a>
            </li>
            @if((isset($club->description) || isset($club->about)) || (Auth::check() && policy($club)->edit(Auth::user(), $club)) || $club->id === null)
                <li class="club-detail__menu-list-item">
                    <a class="club-detail__menu-list-link" href="#informationen">
                        <span><i class="fa fa-info" aria-hidden="true"></i></span>Informationen
                    </a>
                </li>
            @endif
            @if($achievements->count() > 0 || (Auth::check() && policy($club)->edit(Auth::user(), $club)) || $club->id === null)
                <li class="club-detail__menu-list-item">
                    <a class="club-detail__menu-list-link" href="#sportliche-erfolge">
                        <span><i class="fa fa-trophy" aria-hidden="true"></i></span>Sportliche Erfolge
                    </a>
                </li>
            @endif

            <li class="club-detail__menu-list-item">
                <a class="club-detail__menu-list-link" href="#events">
                    <span><i class="fa fa-calendar" aria-hidden="true"></i></span>Events
                </a>
            </li>

            <li class="club-detail__menu-list-item">
                <a class="club-detail__menu-list-link" href="#netzwerk">
                    <span><i class="fa fa-users" aria-hidden="true"></i></span>Netzwerk
                </a>
            </li>

            <li class="club-detail__menu-list-item">
                <a class="club-detail__menu-list-link" href="#netzwerk-news">
                    <span><i class="fa fa-globe" aria-hidden="true"></i></span>Netzwerk-News
                </a>
            </li>

            <li class="club-detail__menu-list-item">
                <a class="club-detail__menu-list-link edit-links" href="#abteilungen">
                    <span><i class="fa fa-tags" aria-hidden="true" href="#abteilungen"></i></span>
                </a>
                @if($sports->count() > 0)
                    <ul id="userlinks" style="display:none;" class="club-detail__menu profile-tabs" data-pageable-menu>
                        @foreach($sports as $sport)
                            <li class="club-detail__menu-list-item">
                                <a class="club-detail__menu-list-link" href="#abteilungen{{$sport->id}}">
                                    {{$sport->title}}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                @endif
            </li>

            <!--<li class="club-detail__menu-list-item">
                <a class="club-detail__menu-list-link" href="#downloads">
                    <span><i class="fa fa-download" aria-hidden="true"></i></span>Downloads
                </a>
            </li>-->
        </ul>

        <ul class="club-detail__menu profile-tabs profile-menu-mob" data-pageable-menu>
            <li class="club-detail__menu-list-item active">
                <a class="club-detail__menu-list-link" href="#pinnwand">
                    <span><i class="fa fa-newspaper-o" aria-hidden="true"></i></span>
                </a>
            </li>
            @if((isset($club->description) || isset($club->about)) || (Auth::check() && policy($club)->edit(Auth::user(), $club)) || $club->id === null)
                <li class="club-detail__menu-list-item">
                    <a class="club-detail__menu-list-link" href="#informationen">
                        <span><i class="fa fa-info" aria-hidden="true"></i></span>
                    </a>
                </li>
            @endif
            @if($achievements->count() > 0 || (Auth::check() && policy($club)->edit(Auth::user(), $club)) || $club->id === null)
                <li class="club-detail__menu-list-item">
                    <a class="club-detail__menu-list-link" href="#sportliche-erfolge">
                        <span><i class="fa fa-trophy" aria-hidden="true"></i></span>
                    </a>
                </li>
            @endif

            <li class="club-detail__menu-list-item">
                <a class="club-detail__menu-list-link" href="#events">
                    <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                </a>
            </li>

            <li class="club-detail__menu-list-item">
                <a class="club-detail__menu-list-link" href="#netzwerk">
                    <span><i class="fa fa-users" aria-hidden="true"></i></span>
                </a>
            </li>

            <li class="club-detail__menu-list-item">
                <a class="club-detail__menu-list-link" href="#netzwerk-news">
                    <span><i class="fa fa-globe" aria-hidden="true"></i></span>
                </a>
            </li>

            <li class="club-detail__menu-list-item">
                <a class="club-detail__menu-list-link edit-links" href="#abteilungen">
                    <span><i class="fa fa-tags" aria-hidden="true" href="#abteilungen"></i></span>
                </a>
                @if($sports->count() > 0)
                    <ul id="userlinks" style="display:none;" class="club-detail__menu profile-tabs" data-pageable-menu>
                        @foreach($sports as $sport)
                            <li class="club-detail__menu-list-item">
                                <a class="club-detail__menu-list-link" href="#abteilungen{{$sport->id}}">
                                    {{$sport->title}}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                @endif
            </li>

            <!--<li class="club-detail__menu-list-item">
                <a class="club-detail__menu-list-link" href="#downloads">
                    <span><i class="fa fa-download" aria-hidden="true"></i></span>
                </a>
            </li>-->
        </ul>
    </nav>
</aside>
@endif