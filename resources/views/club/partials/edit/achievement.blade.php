<div {{isset($newAchievement) ? 'id=add-erfolge' : ''}} class="tabbable-menu__page {{isset($newAchievement) ? '' : 'tabbable-menu__page--active'}}">
    @if ($club->id === null || isset($newAchievement))
        {!! Form::open(
            array(
                'method' => 'post',
                'route' => ['club.achievement.store', $club->slug, '#sportliche-erfolge'],
                'files' => 'true'
            )
        )!!}
    @else
        {!! Form::model(
        $clubAchievement,
        array(
            'method' => 'patch',
            'route' => ['club.achievement.update', $club->slug, $clubAchievement->id, '#sportliche-erfolge'],
            'files' => 'true',
            'data-async-form'
        )
    )!!}
    @endif
    <div class="club-detail__inline-form">
        <div class="row">
            <div class="col-md-4">
                {!! Form::label('day', 'Tag (optional)', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::select('day', array_combine(range(1, 31), range(1, 31)), null, ['class' => 'select', 'placeholder' => 'Tag']) !!}
                </div>
            </div>
            <div class="col-md-4">
                {!! Form::label('month', 'Monat (optional)', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::select('month', array_combine(range(1, 12), range(1, 12)), null, ['class' => 'select', 'placeholder' => 'Monat']) !!}
                </div>
            </div>
            <div class="col-md-4">
                {!! Form::label('year', 'Jahr', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::select('year', array_reverse(array_combine(range(1900, date('Y')), range(1900, date('Y'))), true), null, ['class' => 'select', 'placeholder' => 'Jahr', 'required' => 'required']) !!}
                </div>
            </div>

            <div class="col-md-12">
                {!! Form::label('title', 'Titel', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::text('title', null, ['class' => 'input', 'placeholder' => 'Titel des Erfolges', 'required' => 'required']) !!}
                </div>
            </div>

            <div class="col-md-12">
                {!! Form::label('description', 'Beschreibung', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::textarea('description', null, ['class' => 'input', 'rows' => 10, 'placeholder' => 'Beschreibung']) !!}
                </div>
            </div>

            @if(isset($clubAchievement) && $clubAchievement->picture->originalFilename() !== null)
                <div class="col-xs-12">
                    <div id="@if (isset($clubAchievement)){{ 'upload-result-'. $clubAchievement->id }}@else{{ 'upload-result' }}@endif "
                         class="thumbnail__list">
                        @if (isset($clubAchievement))
                            <input type="checkbox" name="delete-image[]" value="{{ $clubAchievement->id }}"
                                   id="post_thumbnail-{{ $clubAchievement->id }}" class="thumbnail__switch">
                            <label for="post_thumbnail-{{ $clubAchievement->id }}"
                                   class="thumbnail__label thumbnail__container thumbnail__container--deleteable">
                                <img class="thumbnail__image"
                                     src="{{asset($clubAchievement->picture->url('singleView')) }}"/>
                            </label>
                        @endif
                    </div>
                </div>
            @endif

            <div data-upload-result>
                <div class="col-xs-12 hidden" data-new-image-text>
                    <span class="input-label">Neu hinzugefügte Bilder</span>
                </div>
                <div class="col-xs-12">
                    <div id="@if (isset($clubAchievement)){{ 'upload-result-'. $clubAchievement->id }}@else{{ 'upload-result' }}@endif "
                         class="thumbnail__list">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="club-detail__inline-form-submit-wrapper">
                        <label for="@if (isset($clubAchievement)){{ 'achievement_picture-'. $clubAchievement->id }}@else{{ 'achievement_picture' }}@endif"
                               class="button button--grey button--center button--icon button--full-width">
                            <span class="fa fa-upload"></span>Bild hochladen</label>
                        <input type="file"
                               name="@if (isset($clubAchievement)){{ 'achievement_picture-'. $clubAchievement->id }}@else{{ 'achievement_picture' }}@endif"
                               id="@if (isset($clubAchievement)){{ 'achievement_picture-'. $clubAchievement->id }}@else{{ 'achievement_picture' }}@endif"
                               class="input-file">
                        <button class="button button--dark button--center button--icon button--full-width"
                                type="submit"><span class="fa fa-edit"></span>
                            @if ($club->id === null || isset($newAchievement))
                                Eintragen
                            @else
                                Aktualisieren
                            @endif
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>

