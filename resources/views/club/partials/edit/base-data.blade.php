@if ($club->id === null)
    {!! Form::model(
        $club,
        array(
            'method' => 'post',
            'route' => 'club.store',
            'class' => 'club-edit__form',
            'files' => 'true'
        )
    )!!}
@else
    {!! Form::model(
        $club,
        array(
            'method' => 'patch',
            'route' => ['club.update', $club->slug, '#allgemeine-daten'],
            'class' => 'club-edit__form',
            'files' => 'true'
        )
    )!!}
@endif
<div class="row">
    <div class="col-md-6">
        {!! Form::label('name', 'Vereinsname *', ['class' => 'input-label']) !!}
        <div class="input-group">
            {!! Form::text('name', null, [
                    'required',
                    'class' => 'input',
                    'title' => 'Vereinsname',
                    'pattern' => '^(([-. ]+)?([a-zA-ZäöüÄÖÜß.-/]{2,})([/-. ]+)?([0-9]+)?)+'
                ])
            !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="club-detail__overview-item">
            {!! Form::label('bandage', 'Verband', ['class' => 'input-label']) !!}
            <div class="input-group">
                {!! Form::select('bandage', collect(['' => 'Bitte wählen'])->merge($bandages->pluck('name', 'slug')),$club->bandage===null?'':$club->bandage->slug, [
                        'class' => 'select',
                        'title' => 'Mitglieder'
                    ])
                !!}
            </div>
        </div>
    </div>
    <div class="col-md-9">
        {!! Form::label('street', 'Straße  *', ['class' => 'input-label']) !!}
        <div class="input-group">
            {!! Form::text('street', null, [
                'required',
                'class' => 'input',
                'placeholder' => 'Straße',
                'title' => 'Straße',
                'pattern' => '^(([a-zA-ZäöüÄÖÜß]{2,})([-. ]+)?)+'
                ])
            !!}
        </div>
    </div>
    <div class="col-md-3">
        {!! Form::label('house_number', 'Hausnummer', ['class' => 'input-label']) !!}
        <div class="input-group">
            {!! Form::text('house_number', null, [
                'class' => 'input',
                'placeholder' => 'Hausnummer',
                'title' => 'Hausnummer',
                'pattern' => '^[1-9][0-9]?[0-9]?[A-Za-z]{0,2}?'
                ])
            !!}
        </div>
    </div>
    <div class="col-md-4">
        {!! Form::label('zip', 'Postleitzahl', ['class' => 'input-label']) !!}
        <div class="input-group">
            {!! Form::text('zip', null, [
                    'class' => 'input',
                    'placeholder' => 'PLZ',
                    'title' => 'PLZ',
                    'pattern' => '^([0-9]{4,5})$'
                    ])
            !!}
        </div>

    </div>
    <div class="col-md-4">
        {!! Form::label('city', 'Ort *', ['class' => 'input-label']) !!}
        <div class="input-group">
            {!! Form::text('city', null, [
                    'required',
                    'class' => 'input',
                    'placeholder' => 'Ort',
                    'title' => 'Stadt',
                    'pattern' => '^(([a-zA-ZäöüÄÖÜß]{2,})([-. ]+)?)+'
                    ])
            !!}
        </div>
    </div>
    <div class="col-md-4">
        {!! Form::label('email', 'E-Mail-Adresse', ['class' => 'input-label']) !!}
        <div class="input-group">
            {!! Form::email('email', null, [
                    'class' => 'input',
                    'placeholder' => 'E-Mail-Adresse',
                    'title' => 'E-Mail-Adresse'
                    ])
            !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="club-detail__overview-item">
            {!! Form::label('founded', 'Gegründet', ['class' => 'input-label']) !!}
            <div class="input-group">
                {!! Form::text('founded', null, [
                        'class' => 'input',
                        'placeholder' => 'z.B. '.date('Y'),
                        'title' => 'Gegründet'
                    ])
                !!}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="club-detail__overview-item">
            {!! Form::label('shorthand', 'Kürzel', ['class' => 'input-label']) !!}
            <div class="input-group">
                {!! Form::text('shorthand', null, [
                        'class' => 'input',
                        'placeholder' => 'Kürzel',
                        'title' => 'Kürzel'
                    ])
                !!}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="club-detail__overview-item">
            {!! Form::label('member_count', 'Mitglieder', ['class' => 'input-label']) !!}
            <div class="input-group">
                {!! Form::select('member_count', [
                        '' => 'Anzahl auswählen',
                        '1-10' => '1-10',
                        '11-50' => '11-50',
                        '51-100' => '51-100',
                        '101-250' => '101-250',
                        '251-500' => '251-500',
                        '501-750' => '501-750',
                        '751-1000' => '751-1000',
                        '1001-1500' => '1001-1500',
                        '1500+' => '1500+',
                ], $club->member_count, [
                        'class' => 'select',
                        'title' => 'Mitglieder'
                    ])
                !!}
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="club-detail__overview-item">
            {!! Form::label('sports', 'Sportart(en) auswählen *', ['class' => 'input-label']) !!}
            <div class="input-group">
                <select class="club-detail__select-sports" name="sports[]" multiple="multiple" data-select-sports="true"
                        data-sports-url="{{ app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('api.sports') }}"
                        required="required">
                    @foreach($club->sports as $sport)
                        <option value="{{ $sport->id }}" selected="selected">{{ $sport->title }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <p class="input-label">
            * Pflichtfelder
        </p>
    </div>
</div>

<div class="club-content__form-action-wrapper">
    <a href="{{ route('user.dashboard') }}"
       class="club-content__form-cancel-action-button button button--grey button--center button--full-width">
        Abbrechen
    </a>
    {!! Form::submit('Speichern',
        array(
            'class' => 'button--save club-content__form-save-action-button button button--edit button--center button--full-width'
        ))
    !!}
</div>
{!! Form::close() !!}