<div {{isset($newDepartment) ? 'id=edit-department' : 'id=add-department'}} class="{{isset($newDepartment) ? 'tabbable-menu__page--active' : 'tabbable-menu__page'}}">

    @if ($club->id === null || !isset($department))
        {!! Form::open(
            array(
                'method' => 'post',
                'route' => ['department.store', $club->slug, '#abteilungen'.$sport_id],
                'files' => 'true'
            )
        )!!}
    @else
        {!! Form::model(
        $department,
        array(
            'method' => 'patch',
            'route' => ['department.update', $department->id, $club->slug, '#abteilungen'.$sport_id],
            'files' => 'true',
            'data-async-form'
        )
    )!!}
    @endif
    <div>
        <div class="row">
            <div class="col-md-12 wichtig">
                Wichtig: Um diese Abteilung auch für Besucher des Vereinsprofils sichtbar zu machen, muss ein
                Ansprechpartner und/oder eine Beschreibung hinzugefügt werden!
            </div>
        </div>

        <div class="row">
            {!! Form::hidden('sport_id', $sport_id) !!}
            <div class="col-md-12">
                <br><strong>Ansprechpartner </strong><br><br>
                <div class="input-group">
                    <?php $user_id = (isset($department) && $user_id !== null) ? $user_id : null; ?>
                    {!! Form::select('user_id', $users, $user_id, ['class' => 'select', 'id' => 'user_id', 'placeholder' => 'Keiner ausgewählt']) !!}
                    <br><br><strong>Telefonnummer</strong><br><br>
                    {!! Form::text('phone', isset($contact->phone) ? $contact->phone : null, ['id' => 'phone', 'autofocus', 'class' => 'input', 'placeholder' => 'Telefon']) !!}
                    <br><br><strong>E-Mail Adresse</strong><br><br>
                    {!! Form::text('ap_email', isset($contact->ap_email) ? $contact->ap_email : null, ['id' => 'ap_email', 'autofocus', 'class' => 'input', 'placeholder' => 'E-Mail']) !!}
                    <br><br><strong>Kurzbeschreibung</strong><br><br>
                    {!! Form::textarea('description', isset($department->description) ? $department->description : null, ['id' => 'description', 'autofocus', 'class' => 'input', 'placeholder' => 'Kurzbeschreibung']) !!}

                </div>
            </div>
            {!! Form::hidden('club_id', $club->id) !!}
            <div class="col-md-3">
            </div>
            <div class="col-md-3">
            </div>
            <div class="col-md-3">
            </div>
            <div class="col-md-3">
                <div class="club-detail__inline-form-submit-wrapper">

                    {!! Form::submit('Speichern',
                        array(
                            'class' => 'button button--light button--light-white button--center button--full-width'
                        ))
                    !!}
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>