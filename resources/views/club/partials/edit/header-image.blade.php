@if ($club->id === null)
    {!! Form::model(
        $club,
        array(
            'method' => 'post',
            'route' => 'club.store',
            'class' => 'club-edit__form',
            'files' => 'true'
        )
    )!!}
@else
    {!! Form::model(
        $club,
        array(
            'method' => 'patch',
            'route' => ['club.update', $club->slug],
            'class' => 'club-edit__form',
            'files' => 'true'
        )
    )!!}
@endif
<label class="club-content__edit-button club-content__edit-button--headerimage pull-right"
       href="#" for="header">
    <span class="fa fa-pencil"></span>
</label>

<div class="club-content__edit-area club-content__edit-area--headerimage club-content__box">
    <a href="#" class="club-content__icon-button" id="header-zoom-out">
        <span class="fa fa-search-minus"></span>
    </a>
    <a href="#" class="club-content__icon-button" id="header-zoom-in">
        <span class="fa fa-search-plus"></span>
    </a>

    <input type="file" name="header" id="header" class="club-content__hidden-input">
    <input type="hidden" name="crop-props">

    <div class="club-content__form-action-wrapper">
        <a href="#"
           class="club-content__form-cancel-action-button button button--grey button--center button--full-width">
            Abbrechen
        </a>
        {!! Form::submit('Speichern',
            array(
                'class' => 'button--save club-content__form-save-action-button button button--edit button--center button--full-width'
            ))
        !!}
    </div>
</div>
{!! Form::close() !!}