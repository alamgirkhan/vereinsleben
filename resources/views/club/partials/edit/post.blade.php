<div {{isset($newPost) ? 'id=add-post' : ''}} class="tabbable-menu__page {{isset($newPost) ? '' : 'tabbable-menu__page--active'}}">
    @if ($club->id === null || isset($newPost))
        {!! Form::open(
            array(
                'method' => 'post',
                'route' => ['post.store', '#pinnwand'],
                'files' => 'true'
            )
        )!!}
    @else
        {!! Form::model(
        $post,
        array(
            'method' => 'patch',
            'route' => ['post.update', $post->id, '#pinnwand'],
            'files' => 'true',
            'data-async-form'
        )
    )!!}
    @endif
    <div class="club-detail__inline-form">
        <div class="row">
            <div class="col-xs-12">
                {!! Form::label('title', 'Titel', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::text('title', null, ['class' => 'input', 'placeholder' => 'Titel der Neuigkeit', 'required' => 'required']) !!}
                </div>
            </div>

            <div class="col-xs-12">
                {!! Form::label('content_raw', 'Text', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::textarea('content_raw', null, ['class' => 'input', 'rows' => 10, 'placeholder' => 'Inhalt der Neuigkeit', 'required' => 'required']) !!}
                </div>
            </div>

            <div class="col-xs-6">
                {!! Form::label('published_from', 'Veröffentlicht von', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::text('published_from', isset($post->published_from) ? date('d.m.Y H:i', strtotime($post->published_from)) : null, ['class' => 'input', 'placeholder' => 'DD.MM.YY HH:mm', 'data-datetimepicker-enabled']) !!}
                </div>
            </div>

            <div class="col-xs-6">
                {!! Form::label('published_until', 'Veröffentlicht bis', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::text('published_to', isset($post->published_to) ? date('d.m.Y H:i', strtotime($post->published_to)) : null, ['class' => 'input', 'placeholder' => 'DD.MM.YY HH:mm', 'data-datetimepicker-enabled']) !!}
                </div>
            </div>

            {{--@if($found)--}}
                {{--<div class="col-xs-3">--}}
                    {{--<div class="input-group">--}}
                        {{--<img src="{{asset('static_images/vrm_logo_klein.png')}}" alt="vrm_logo"/>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-xs-9">--}}
                    {{--<div class="input-group">--}}
                        {{--Über unseren Partner VRM Lokal kannst Du eure Vereins-News auch für die Veröffentlichung in den Anzeigenblättern und Heimatzeitungen in eurer Umgebung freigeben. <a href="http://www.vrm-lokal.de" target="_blank"> Klicke hier …</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--@endif--}}

            {!! Form::hidden('club_id', $club->id) !!}

            <div class="col-xs-12">
                <div id="@if (isset($post)){{ 'upload-result-'. $post->id }}@else{{ 'upload-result' }}@endif "
                     class="thumbnail__list">
                    @if (isset($post))
                        @foreach($post->images as $image)
                            <input type="checkbox" name="delete-image[]" value="{{ $image->id }}"
                                   id="post_thumbnail-{{ $image->id }}" class="thumbnail__switch">
                            <label for="post_thumbnail-{{ $image->id }}"
                                   class="thumbnail__label thumbnail__container thumbnail__container--deleteable">
                                <img class="thumbnail__image" src="{{asset($image->picture->url('singleView')) }}"/>
                            </label>
                        @endforeach
                    @endif
                </div>
            </div>

            <div data-upload-result>
                <div class="col-xs-12 hidden" data-new-image-text>
                    <span class="input-label">Neu hinzugefügte Bilder</span>
                </div>
                <div class="col-xs-12">
                    <div id="@if (isset($post)){{ 'upload-result-'. $post->id }}@else{{ 'upload-result' }}@endif "
                         class="thumbnail__list">
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="club-detail__inline-form-submit-wrapper">
                        <label for="@if (isset($post)){{ 'image-'. $post->id }}@else{{ 'image' }}@endif"
                               class="button button--grey button--center button--icon button--full-width">
                            <span class="fa fa-upload"></span>Bilder hochladen</label>
                        <input multiple type="file" name="images[]"
                               id="@if (isset($post)){{ 'image-'. $post->id }}@else{{ 'image' }}@endif"
                               class="input-file">
                        <button class="button button--dark button--center button--icon button--full-width"
                                type="submit"><span class="fa fa-edit"></span>
                            @if ($club->id === null || isset($newPost))
                                Posten
                            @else
                                Aktualisieren
                            @endif
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
