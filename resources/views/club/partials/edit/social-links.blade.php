@if ($club->id === null)
    {!! Form::model(
        $club,
        array(
            'method' => 'post',
            'route' => 'club.store',
            'class' => 'club-edit__form',
            'files' => 'true'
        )
    )!!}
@else
    {!! Form::model(
        $club,
        array(
            'method' => 'patch',
            'route' => ['club.update', $club->slug],
            'class' => 'club-edit__form',
            'files' => 'true'
        )
    )!!}
@endif
@foreach($socialLinkTypes as $key => $label)
    <div class="club-social__container">
        <label for="club_edits-ocial_links-{{ $key }}" class="input-label">
            <i class="icon-space fa fa-{{ ($key == 'googleplus') ? 'google-plus' : ($key == 'web' ? 'globe' : $key) }}"></i>
            {{ $label }}
        </label>
        <input type="text" name="social_links[{{ $key }}]"
               id="club_edits-ocial_links-{{ $key }}"
               value="{{ isset($socialLinks[$key]) ? $socialLinks[$key]->url : '' }}"
               class="input">
    </div>
@endforeach

<div class="club-content__form-action-wrapper">
    <a href="#" class="club-content__form-cancel-action-button button button--grey button--center button--full-width">
        Abbrechen
    </a>
    {!! Form::submit('Speichern',
        array(
            'class' => 'button--save club-content__form-save-action-button button button--edit button--center button--full-width'
        ))
    !!}
</div>
{!! Form::close() !!}