@if(Auth::check() && policy($club)->edit(Auth::user(), $club))
@if ($club->id === null || isset($newTeam))
    {!! Form::open(
        array(
            'method' => 'post',
            'route' => ['team.store', '#abteilungen'.$department->sport_id],
            'files' => 'true'
        )
    )!!}
@else
    {!! Form::model(
    $team,
    array(
        'method' => 'patch',
        'route' => ['team.update', $team->id, '#abteilungen'.$department->sport_id],
        'files' => 'true',
        'data-async-form'
    )
)!!}
@endif
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <h3> @if(isset($newTeam))
            Neue Mannschaft
        @else
            Teams bearbeiten
        @endif
    </h3>
    <div class="row">
        <div class="col-md-12">
            <strong>Name der Mannschaft</strong><br><br>
            <div class="input-group">
                {!! Form::text('name', null, ['class' => 'input', 'placeholder' => 'Name', 'required' => 'required']) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <strong>Altersklasse</strong><br><br>
            <div class="input-group">
                {!! Form::text('age_class', null, ['class' => 'input', 'placeholder' => 'Altersklasse']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <strong>Liga</strong> <br/><br/>
            <div class="input-group">
                {!! Form::text('league', null, ['class' => 'input', 'placeholder' => 'Liga']) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <strong>Kurzbeschreibung</strong><br/><br/>
            <div class="input-group">
                {!! Form::textarea('description', null, ['class' => 'input', 'placeholder' => 'Beschreibung']) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <strong>Ansprechpartner</strong><br/><br/>
            <div class="input-group">
                {!! Form::select('user_id', $users, isset($user->id) ? $user->id : null, ['class' => 'select', 'id' => 'user_id', 'onChange' => 'getContact(this.value,'.$dynamicId.')', 'placeholder' => 'Keiner ausgewählt']) !!}
                <br/>
                <strong>Telefon</strong><br/><br/>
                {!! Form::text('phone', isset($user->phone) ? $user->phone : null, ['id' => 'phone'.$dynamicId, 'autofocus', 'class' => 'input', 'placeholder' => 'Telefon']) !!}
                <br/>
                <strong>E-Mail</strong><br/><br/>
                {!! Form::text('ap_email', isset($user->ap_email) ? $user->ap_email : null, ['id' => 'ap_email'.$dynamicId, 'autofocus', 'class' => 'input', 'placeholder' => 'E-Mail']) !!}
            </div>
        </div>
    </div>
    {!! Form::hidden('club_id', $department->club_id) !!}
    {!! Form::hidden('sport_id', $department->sport_id) !!}
    {!! Form::hidden('department_id', $department->id) !!}

    @if(!isset($newTeam) && isset($team) && $team->picture->originalFilename() !== null)
        <div class="row">
            <div class="col-xs-12">
                <div id="@if (isset($team)){{ 'upload-result-'. $team->id }}@else{{ 'upload-result' }}@endif "
                     class="thumbnail__list">
                    <input type="checkbox" name="delete-image[]" value="{{ $team->id }}"
                           id="team_picture_thumbnail-{{ $team->id }}" class="thumbnail__switch">
                    <label for="team_picture_thumbnail-{{ $team->id }}"
                           class="thumbnail__label thumbnail__container thumbnail__container--deleteable">
                        <img class="thumbnail__image" src="{{asset($team->picture->url('singleView')) }}"/>
                    </label>
                </div>
            </div>
        </div>
    @endif

    <div data-upload-result>
        <div class="col-xs-12 hidden" data-new-image-text>
            <span class="input-label">Neu hinzugefügte Bilder</span>
        </div>
        <div class="col-xs-12">
            <div id="@if (!isset($newTeam) && isset($team)){{ 'upload-result-'. $team->id }}@else{{ 'upload-result' }}@endif "
                 class="thumbnail__list">
            </div>
        </div>
        <div class="col-xs-12">

            <div class="col-xs-6">
                <div class="club-detail__inline-form-submit-wrapper">
                    <label for="@if (!isset($newTeam) && isset($team)){{ 'team_picture-'. $team->id }}@else{{ 'team_picture' }}@endif"
                           class="button button--grey button--center button--icon button--full-width">
                        <span class="fa fa-upload"></span>Bild hochladen</label>
                    <input type="file"
                           name="picture"
                           id="@if (!isset($newTeam) && isset($team)){{ 'team_picture-'. $team->id }}@else{{ 'team_picture' }}@endif"
                           class="input-file">
                </div>
            </div>
            <div class="col-xs-6">
                <button class="button button--light button--light-white button--center button--full-width"
                        type="submit">
                    Speichern
                </button>
            </div>

        </div>
    </div>
</div>
{!! Form::close() !!}
@endif