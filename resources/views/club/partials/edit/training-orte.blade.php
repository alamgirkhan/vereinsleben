<div {{isset($newTrainingsort) ? 'id=add-trainingsort' : ''}} class="tabbable-menu__page {{isset($newTrainingsort) ? '' : 'tabbable-menu__page--active'}}">
    @if ($club->id === null || isset($newTrainingsort))
        {!! Form::open(
            array(
                'method' => 'post',
                'route' => ['training_ort.store', '#informationen'],
                'files' => 'true'
            )
        )!!}
    @else
        {!! Form::model(
        $training,
        array(
            'method' => 'patch',
            'route' => ['training_ort.update', $training->id, '#informationen'],
            'files' => 'true',
            'data-async-form'
        )
    )!!}
    @endif
    <div class="club-detail__inline-form">
        <div class="row">
            {{-- @todo extend this form --}}
            {!! Form::open(['route' => 'training_ort.store', 'method' => 'POST']) !!}

            <div class="col-md-12">
                {!! Form::label('name', 'Name', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::text('name', null, ['class' => 'input', 'placeholder' => 'Name der Sportstätte', 'required' => 'required']) !!}
                </div>
            </div>

            <div class="col-md-12">
                {!! Form::label('title', 'Bezeichnung', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::text('title', null, ['class' => 'input', 'placeholder' => 'Ortsbezeichnung', 'required' => 'required']) !!}
                </div>
            </div>


            <div class="col-md-9">
                {!! Form::label('street', 'Straße', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::text('street', null, ['class' => 'input', 'placeholder' => 'Straße', 'required' => 'required']) !!}
                </div>
            </div>

            <div class="col-md-3">
                {!! Form::label('house_number', 'Hausnummer', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::text('house_number', null, ['class' => 'input', 'placeholder' => 'Nr']) !!}
                </div>
            </div>

            <div class="col-md-4">
                {!! Form::label('zip', 'Postleitzahl', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::text('zip', null, ['class' => 'input', 'placeholder' => 'PLZ', 'required' => 'required']) !!}
                </div>
            </div>

            <div class="col-md-8">
                {!! Form::label('city', 'Stadt', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::text('city', null, ['class' => 'input', 'placeholder' => 'Stadt / Ort', 'required' => 'required']) !!}
                </div>
            </div>

            {!! Form::hidden('club_id', $club->id) !!}

            <div class="col-xs-12">
                <div id="@if (isset($event)){{ 'upload-result-'. $event->id }}@else{{ 'upload-result' }}@endif "
                     class="thumbnail__list">
                    @if (isset($event))
                        @foreach($event->images as $image)
                            <input type="checkbox" name="delete-image[]" value="{{ $image->id }}"
                                   id="event_thumbnail-{{ $image->id }}" class="thumbnail__switch">
                            <label for="event_thumbnail-{{ $image->id }}"
                                   class="thumbnail__label thumbnail__container thumbnail__container--deleteable">
                                <img class="thumbnail__image" src="{{asset($image->picture->url('singleView')) }}"/>
                            </label>
                        @endforeach
                    @endif
                </div>
            </div>

            <div data-upload-result>
                <div class="col-xs-12 hidden" data-new-image-text>
                    <span class="input-label">Neu hinzugefügte Bilder</span>
                </div>
                <div class="col-xs-12">
                    <div id="@if (isset($post)){{ 'upload-result-'. $post->id }}@else{{ 'upload-result' }}@endif "
                         class="thumbnail__list">
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="club-detail__inline-form-submit-wrapper">
                        <button class="button button--dark button--center button--icon button--full-width"
                                type="submit"><span class="fa fa-edit"></span>
                            @if ($club->id === null || isset($newTrainingsort))
                                Eintragen
                            @else
                                Aktualisieren
                            @endif
                        </button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>