@if($events->count() > 0)
    <section class="event__upcoming">
        <div class="container">
            <h2 class="sub-headline sub-headline--bordered headline--centered"/>Nächste Events</h2>
            <div class="row">
                @foreach($events as $event)
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="event__upcoming-wrapper">
                            <a class="button--full-width" href="{{ route('club.detail', ['slug' => 'todo']) }}">
                                <div class="event__upcoming-element">
                                    <div class="event__upcoming-image-wrapper">
                                        <div class="event__upcoming-image"
                                            @if($event->image->originalFilename() !== null)
                                                style="background-image: url('{{ $event->image->url('singleView') }}');"
                                            @endif
                                        ></div>

                                        <div class="event__upcoming-caption-wrapper">
                                            <div class="event__upcoming-text">
                                                <span class="event__upcoming-club">{{ $event->club->name }}</span>
                                                <span class="event__upcoming-name">{{ $event->title }}</span>
                                            </div>
                                            <div class="event__upcoming-date-wrapper">
                                                <div class="event__upcoming-date-box">Datum</div>
                                                <div class="event__upcoming-date">{{ date('d.m.Y', strtotime($event->schedule_begin)) }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a class="button button--center button--light button--full-width"
                               href="{{ route('club.detail', ['slug' => $event->club->slug]) }}">Event Details</a>
                        </div>
                    </div>
                @endforeach
                @if($events->count() < 5)
                    <div class="col-xs-12">
                        <div class="button__center-wrapper">
                            <button class="button button--padded button--dark-inverted">Weitere Events laden</button>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endif