<div class="club-post__item" data-edit-wrapper>
    <div class="row">
        @if(Auth::check() && policy($club)->edit(Auth::user(), $club))
            <div class="col-xs-11 edit-del-btn">
                <div class="club-content__options">
                    <a href="#" class="club-content__options-button" title="Bearbeiten"
                       data-edit='{ "club_id": "{{ $club->id }}",
                       "resource": { "id": "{{ $event->id }}",
                       "type": "event",
                       "url": "{{ route('event.edit', $event->id) }}"
                       } }'>
                        <span class="fa fa-pencil club-content__options-button-edit"></span>
                    </a>
                    <a href="#" class="club-content__options-button club-content__options-button--hidden"
                       title="Bearbeiten abbrechen"
                       data-cancel='{ "club_id": "{{ $club->id }}",
                       "resource": { "id": "{{ $event->id }}",
                       "type": "event",
                       "url": "{{ route('event.single', [$club->slug, $event->id]) }}"
                       } }'>
                        <span class="fa fa-times club-content__options-button-cancel"></span>
                    </a>
                    <a href="#" class="club-content__options-button" title="Löschen"
                       data-delete='{ "club_id": "{{ $club->id }}",
                       "resource": { "id": "{{ $event->id }}",
                       "type": "event",
                       "url": "{{ route('event.delete', $event->id) }}"
                       } }'>
                        <span class="fa fa-trash club-content__options-button-remove"></span>
                    </a>
                </div>
            </div>
        @endif
    </div>

    <div data-edit-content class="club-post__container">
        <div class="row">
            <div class="col-lg-1 col-md-1 club-post__date noborder">
                @if($event->published_from >= \Carbon\Carbon::now())
                    <span class="club-post__date club-post__date--scheduled">
                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                        {{ date('d.m.Y / H:i', strtotime($event->published_from)) }}

                        @if(isset($event->published_to) and $event->published_to !== null)
                            - {{ date('d.m.Y / H:i', strtotime($event->published_to)) }}
                        @endif
                </span>
                @else
                    <span class="date-no">{{ date('d', strtotime($event->published_at)) }}.</span><br>
                    <span class="month-no">{{ strtoupper(date('M', strtotime($event->published_at))) }}</span><br>
                    <span class="year-no">{{ date('Y', strtotime($event->published_at)) }}</span>
                @endif
            </div>
            <div class="col-lg-11 col-sm-11 col-xs-12 event-post">
                <div class="@if($event->images()->count() == 1) event-post-single-thum @else event-post-thum @endif">
                        @if($event->images()->count() == 1)
                            @foreach($event->images as $image)
                            <a href="{{ asset($image->picture->url('singleView')) }}"
                               data-lightbox="post-{{ $event->id }}"><img
                                        src="{{ asset($image->picture->url('singleView')) }}"/></a>
                        @endforeach
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="event-post-thum-arrow"><i class="fa fa-caret-up"
                                                                      aria-hidden="true"></i></div>
                                <div class="row post-head">
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 postlogo">
                                        <img src="{{ asset($club->avatar->url('singleView')) }}"/>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-8 postdetail">
                                        <div class="postlefttxt">{{ $club->shorthand }}</div>
                                        <div class="post-blue">{{ $event->title }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="row post-row-new">
                            <div class="@if($event->images()->count() < 2) col-lg-12 col-md-12 col-sm-12 col-xs-12 @else col-lg-8 col-md-8 col-sm-8 col-xs-8 @endif">
                                @if($event->images()->count() > 0)
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 lgthum">
                                            <a href="{{asset($event->images[0]->picture->url('singleView')) }}"
                                               data-lightbox="post-{{ $event->id }}"><img
                                                        src="{{asset($event->images[0]->picture->url('singleView')) }}"/></a>
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        @if($event->images()->count() > 0)
                                            <div class="event-post-thum-arrow"><i class="fa fa-caret-up"
                                                                                  aria-hidden="true"></i></div>
                                        @endif
                                        <div class="row post-head">
                                            <div class="@if($event->images()->count() == 0) col-lg-2 col-md-2 @else col-lg-3 col-md-3 @endif col-sm-2 col-xs-4 postlogo">
                                                <img src="{{ asset($club->avatar->url('singleView')) }}"/>
                                            </div>
                                            <div class="@if($event->images()->count() == 0) col-lg-10 col-md-10 @else col-lg-9 col-md-9 @endif col-sm-10 col-xs-8 postdetail">
                                                <div class="postlefttxt">{{ $club->shorthand }}</div>
                                                @if($event->images()->count() < 2)
                                                    <div class="post-blue">{{ $event->title }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 club-post_right-thums">
                                <div class="row">
                                    @foreach($event->images as $key => $image)
                                        @if($key !=0 && $key < 4)
                                            <a href="{{asset($image->picture->url('singleView')) }}"
                                               data-lightbox="post-{{ $event->id }}">
                                                <div class="col-lg-12 club-post__sml-image"
                                                     style="background:url('{{asset($image->picture->url('singleView')) }}') no-repeat;background-size:cover;"></div>
                                            </a>
                                            @if($key == 3)
                                                @if($event->images()->count() > 4)
                                                    <div class="event-plus-icon"><a href="javascript:void(0);"><i
                                                                    class="fa fa-plus-circle"
                                                                    aria-hidden="true"></i></a>
                                                    </div>
                                                @endif
                                            @endif
                                        @endif

                                        @if($key > 3)
                                            <a href="{{asset($image->picture->url('singleView')) }}"
                                               data-lightbox="post-{{ $event->id }}"></a>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif
                    </div>
                @if($event->images()->count() > 1)
                    <div class="post-blue">{{ $event->title }}</div>
                @endif

                <div class="row post-head">
                    <div class="col-xs-12 postlocation postdetail-mob">
                        <i class="fa fa-map-marker" aria-hidden="true"></i> {{$event->street}}
                        @if(isset($event->house_number))
                            <span>, {{$event->house_number}}, </span>
                        @endif
                        {{$event->zip}}, {{$event->city}}
                    </div>

                    <div class="col-lg-6 col-md-4 col-sm-6 col-xs-12 postlocation">
                        <i class="fa fa-calendar"
                           aria-hidden="true"></i> {{date('d.m.Y H:i', strtotime($event->schedule_begin))}}
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 postlocation">
                        <i class="fa fa-clock-o"
                           aria-hidden="true"></i> {{date('d.m.Y H:i', strtotime($event->schedule_end))}}
                    </div>
                </div>

                @if(isset($event->info))
                    <div class="row event-head postdetail-desk">
                        <div class="col-lg-12 postlocation">
                            <i class="fa fa-eur" aria-hidden="true"></i> {{ $event->info }}
                        </div>
                    </div>

                    <div class="row postdetail-mob">
                        <div class="col-lg-12 postlocation">
                            <i class="fa fa-eur" aria-hidden="true"></i> {{ $event->info }}
                        </div>
                    </div>
                @endif
                <p>{!! nl2br($event->content) !!}</p>
                <!--<div class="row post-head postbottom-desk">
                    <div class="col-lg-9 col-md-9 col-sm-6"><a class="btn btn-default interessiert-btn"
                                                               href="#">Interessiert</a>&nbsp;
                        <a href="#" class="kommentieren"><i class="fa fa-comment-o" aria-hidden="true"></i>
                            Kommentieren <sup>8</sup></a>&nbsp; <a href="#" class="kommentieren"><i
                                    class="fa fa-paper-plane-o" aria-hidden="true"></i> Teilen</a>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 "><a href="#" class="allevents">Alle Events<br>
                            ansehen</a></div>
                </div>

                <div class="row post-head postbottom-mob">
                    <div class="col-xs-12">
                        <a href="#" class="kommentieren"><i class="fa fa-comment-o" aria-hidden="true"></i>
                            Kommentieren <sup>8</sup></a>&nbsp;
                        <a href="#" class="kommentieren"><i class="fa fa-paper-plane-o" aria-hidden="true"></i>
                            Teilen</a>

                    </div>
                    <div class="col-lg-9 col-sm-6 col-xs-6">
                        <a class="btn btn-default interessiert-btn" href="#">Interessiert</a>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-xs-6"><a href="#" class="allevents">Alle Events<br>
                            ansehen</a></div>
                </div>-->
            </div>
        </div>
    </div>
</div>