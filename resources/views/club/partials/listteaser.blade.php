<div class="row club-tile-lead">
    <figure class="club-tile-wide">
        <a href="{{ route('club.detail', ['slug' => $club->slug]) }}" class="club-tile-wide__link">
        <div class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
            <div class="club-tile-wide__image-wrapper">
                <img class="club-tile-wide__image"
                     src="{{ asset($club->avatar->url('startPage')) }}"
                     alt="{{ $club->name }}"/>
            </div>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-8">
            <figcaption class="club-tile-wide__content">
            <p class="club-tile-wide__name">{{ $club->name }}</p>
            <p class="club-tile-wide__location"><i class="fa fa-map-marker"></i> {{ $club->fullAddress() }}</p>
            <p class="club-tile-wide__sports">Sportarten: {{ $club->allSportsString() }}</p>
            </figcaption>
        </div>
        @if (isset($publishedtoggle) && $publishedtoggle === true)
            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 onoffswitch-lead">
                <div class="club-published__button-wrapper">
                <label class="club-published__button-label" for="club-published-{{$club->id}}">Veröffentlicht?</label>
                <div class="onoffswitch">
                    <input data-club-published-checkbox data-club-id="{{ $club->id }}" data-update-url="{{ route('club.updatePublished', $club->slug) }}"
                           type="checkbox" name="onoffswitch" class="club-published__button-checkbox onoffswitch-checkbox"
                           id="club-published-{{$club->id}}" {{ ($club->published == 1) ? 'checked' : '' }}>
                    <label data-toggle-club-published
                           class="club-published__button-toggle onoffswitch-label" for="club-published-{{$club->id}}"></label>
                </div>
                </div>
            </div>
        @endif
        <div class="clear"></div>
        </a>
</figure>
</div>
