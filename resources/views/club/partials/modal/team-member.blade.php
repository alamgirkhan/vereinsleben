@can('updateMemberStatus', $club)
<div id="modal" class="modal__container modal__container--opend" data-club-member-modal>
    <section class="modal aftercheckbox">
        <form data-club-member-form
              data-club-member-url="{{ route('club.member.update', $club->slug) }}"
              data-form-token="{{ csrf_token() }}">
            <div class="modal__content">
                <div class="modal__inner">
                    <div class="club-box-inner">
                        <div class="modal__close" data-modal-close>
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </div>
                        <div class="hl hl--large hl--inverted hl--underlined hl--upper">
                            @if (Auth::user()->isClubMember($club))
                                Du bist Mitglied in diesem Verein
                            @else
                                Bist Du Mitglied in diesem Verein?
                            @endif
                        </div>
                        <div class="modal__info">
                            {{--@if (Auth::user()->isClubMember($club))--}}
                                {{--Dein Verein und die von Dir angegebenen Mannschaften--}}
                                {{--werden in Deinem Profil automatisch gelöscht.--}}
                            {{--@else--}}
                                {{--Dein Verein und die von Dir angegebenen Mannschaften--}}
                                {{--werden in Deinem Profil automatisch ergänzt.--}}
                            {{--@endif--}}
                        </div>
                        <div class="modal__input-group">
                            @if($club->teams()->count() > 0)
                                @foreach($club->teams as $team)
                                    <input data-club-member-form-status-input
                                           class="modal__checkbox-input"
                                           type="checkbox"
                                           name="teams[]"
                                           id="team-{{ $team->id }}"
                                           value="{{ $team->id }}"
                                           @if(Auth::check() && Auth::user()->isTeamMember($team)) checked="checked" @endif>
                                    <label class="modal__checkbox" for="team-{{ $team->id }}">
                                        {{ $team->name }}
                                    </label>
                                @endforeach
                            @endif
                        </div>
                        <br>
                        <div class="modal__btn-grp_sik">
                            @if (Auth::user()->isClubMember($club))
                                <a href="#" class="btn btn-default membership-btn" data-club-exit>
                                    Mitgliedschaft beenden
                                </a>
                                @if ($club->teams()->count() > 0)
                                    <a data-club-member-form-button
                                       class="btn btn-default membership-btn" data-club-save>
                                        Mannschaften angeben
                                    </a>
                                @endif
                            @else
                                @if ($club->teams()->count() > 0)
                                    <a data-club-member-form-button
                                       class="btn btn-default membership-btn" data-club-save>
                                        Mannschaften angeben & Mitglied werden
                                    </a>
                                @else
                                    <a data-club-member-form-button
                                       class="btn btn-default membership-btn" data-club-save>
                                        Mitglied werden
                                    </a>
                            @endif
                        @endif
                        <!--<a class="btn btn-default membership-btn modal__close_2" data-modal-close
                               aria-hidden="true" style="font-size: 14pt">Abbrechen</a>-->
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
</div>
@endcan