<div class="club-post__item" data-edit-wrapper>
    <div class="row">
        @if(Auth::check() && policy($club)->edit(Auth::user(), $club))
            <div class="col-xs-11 edit-del-btn">
                <div class="club-content__options">
                    <a href="#" class="club-content__options-button" title="Bearbeiten"
                       data-edit='{ "club_id": "{{ $club->id }}",
                       "resource": { "id": "{{ $post->id }}",
                       "type": "post",
                       "url": "{{ route('post.edit', $post->id) }}"
                       } }'>
                        <span class="fa fa-pencil club-content__options-button-edit"></span>
                    </a>
                    <a href="#" class="club-content__options-button club-content__options-button--hidden"
                       title="Bearbeiten abbrechen"
                       data-cancel='{ "club_id": "{{ $club->id }}",
                       "resource": { "id": "{{ $post->id }}",
                       "type": "post",
                       "url": "{{ route('post.single', [$club->slug, $post->id]) }}"
                       } }'>
                        <span class="fa fa-times club-content__options-button-cancel"></span>
                    </a>
                    <a href="#" class="club-content__options-button" title="Löschen"
                       data-delete='{ "club_id": "{{ $club->id }}",
                       "resource": { "id": "{{ $post->id }}",
                       "type": "post",
                       "url": "{{ route('post.delete', $post->id) }}"
                       } }'>
                        <span class="fa fa-trash club-content__options-button-remove"></span>
                    </a>
                </div>
            </div>
        @endif
    </div>

    <div data-edit-content>
        <div class="row">
            <div class="col-lg-1 col-md-1 club-post__date noborder">
                @if($post->published_from >= \Carbon\Carbon::now())
                    <span class="club-post__date club-post__date--scheduled">
				<i class="fa fa-clock-o" aria-hidden="true"></i>
                        {{ date('d.m.Y / H:i', strtotime($post->published_from)) }}

                        @if(isset($post->published_to) and $post->published_to !== null)
                            - {{ date('d.m.Y / H:i', strtotime($post->published_to)) }}
                        @endif
			</span>
                @else
                    <span class="date-no">{{ date('d', strtotime($post->published_at)) }}.</span><br>
                    <span class="month-no">{{ strtoupper(date('M', strtotime($post->published_at))) }}</span><br>
                    <span class="year-no">{{ date('Y', strtotime($post->published_at)) }}</span>
                @endif
            </div>

            <div class="col-lg-11 col-sm-11 event-post">
                @if($post->images()->count() > 0)
                    <div class="event-post-thum">
                        @if($post->images()->count() > 1)
                            <div class="club-post__image-slider">
                                @endif

                                @foreach($post->images as $image)
                                    <div class="club-post__image"
                                         style="background-image: url('{{asset($image->picture->url('singleView')) }}')"></div>
                                @endforeach

                                @if($post->images()->count() > 1)
                            </div>
                        @endif

                        <div class="event-post-thum-arrow">
                            <img src="{{asset('static_images/post-thum-arrow.png')}}"/>
                        </div>
                    </div>
                @endif

                @if(isset($post->content))
                    <div class="row post-head">
                        <div class="col-lg-2 col-sm-6 postlogo">
                            <img src="{{ asset($club->avatar->url('singleView')) }}"/>
                        </div>
                        <div class="col-lg-10 col-sm-6 postdetail">
                            <div class="postlefttxt">{{ $club->shorthand }}
                                <div class="post-blue">{{ $post->title }}</div>
                            </div>
                        </div>
                    </div>
                    <p>{!! nl2br($post->content) !!}</p>
                @endif
            </div>
        </div>
    </div>
</div>