<table class="table table-striped table-bordered">
    <tr>
        <th><strong>name</strong></th>
        <th><strong>friendship status</strong></th>
    </tr>
    @foreach($clubs as $friendClub)
        @if(!($club->isBlockedBy($friendClub) || $club->slug == $friendClub->slug))
            <tr>
                <td>
                    <a href="{{ route('club.detail', ['slug' => $friendClub->slug]) }}"
                       class="club-tile-wide__link"> {{ $friendClub->name }}</a>
                </td>
                <td>
                    @if($club->hasSentFriendRequestTo($friendClub))
                        friend request sent
                    @elseif($friendClub->hasSentFriendRequestTo($club))
                        {!! Form::model(
                            $club,
                            array(
                                'method' => 'patch',
                                'route' => ['club.profile.friendship', $club->slug, $friendClub->slug],
                                'class' => '',
                                'files' => 'false'
                            )
                        )!!}
                        {!! Form::submit('accept', array('name'=> 'relation', 'class' => 'button button--light button--light-white button--center ')) !!}
                        {!! Form::submit('deny', array('name'=> 'relation', 'class' => 'button button--light button--light-white button--center ')) !!}
                        {!! Form::close() !!}
                    @elseif($club->isFriendWith($friendClub))
                        {!! Form::model(
                            $club,
                            array(
                                'method' => 'patch',
                                'route' => ['club.profile.edit_friendship', $club->slug, $friendClub->slug],
                                'class' => '',
                                'files' => 'false'
                            )
                        )!!}
                        {!! Form::submit('remove', array('name'=> 'relation', 'class' => 'button button--light button--light-white button--center ')) !!}
                        {!! Form::submit('block', array('name'=> 'relation', 'class' => 'button button--light button--light-white button--center ')) !!}
                        {!! Form::close() !!}
                    @elseif($club->hasBlocked($friendClub))
                        {!! Form::model(
                            $club,
                            array(
                                'method' => 'patch',
                                'route' => ['club.profile.edit_friendship', $club->slug, $friendClub->slug],
                                'class' => '',
                                'files' => 'false'
                            )
                        )!!}
                        {!! Form::submit('unblock', array('name'=> 'relation', 'class' => 'button button--light button--light-white button--center ')) !!}
                        {!! Form::close() !!}
                    @else
                        {!! Form::model(
                            $club,
                            array(
                                'method' => 'patch',
                                'route' => ['club.profile.friendship', $club->slug, $friendClub->slug],
                                'class' => '',
                                'files' => 'false'
                            )
                        )!!}
                        {!! Form::submit('send-friend-request', array('name'=> 'relation', 'class' => 'button button--light button--light-white button--center ')) !!}
                        {!! Form::close() !!}
                    @endif
                </td>
            </tr>
        @endif
    @endforeach
</table>