<section data-token="{{ csrf_token() }}">
    <div class="club-detail__main-image-wrapper">

        <img class="club-detail__main-image club-detail__main-image--unowned"
             src="{{ asset('default_images/club/default-unowned-header.png') }}" alt="Header">
        
        <div class="club-detail__main-image-unowned-content">
            <h1 class="headline-intro headline--centered">Diese Seite wurde automatisch erstellt, weil dieser Verein in der Community genannt wurde.</h1>
            <h2 class="headline-secondary headline-secondary--bordered headline--centered">Dieser Verein kann dir gehören!</h2>
            <h3 class="headline-desc headline--centered">
                Du bist Mitglied in diesem Verein?<br>
                Übernehme das Vereinsprofil und fülle es mit <strong>LEBEN!</strong>
            </h3>
        </div>
    </div>
    <div class="club-detail-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-5 col-md-3">
                    <div class="club-detail__avatar">
                        <img class="club-detail__image"
                             src="{{ asset($club->avatar->url('singleView')) }}"
                             alt="">
                    </div>
                </div>
                <div class="col-sm-7 col-md-8 col-md-offset-1">
                    <section class="club-detail__content-wrapper">
                        <div class="row">
                            <div class="col-xs-12">
                                <h1 class="club-detail__headline">{{ $club->name }}</h1>
                                <a href="{{ route('club.claim', $club->id) }}" data-club-claim="{{-- route --}}" class="btn btn--dark-blue btn--large btn--inline btn--upper">Vereinsprofil übernehmen</a>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</section>