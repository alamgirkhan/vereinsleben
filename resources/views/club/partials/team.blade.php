<div data-edit-wrapper>
    @if(Auth::check() && policy($club)->edit(Auth::user(), $club))
        <div class="club-content__options">
            <a href="#" class="club-content__options-button" title="Bearbeiten"
               data-edit='{ "club_id": "{{ $club->id }}",
								   "resource": { "id": "{{ $team->id }}",
								   "type": "team",
								   "url": "{{ route('team.edit', $team->id) }}"
			   } }'>
                <span class="fa fa-pencil club-content__options-button-edit"></span> </a>
            <a href="#" class="club-content__options-button club-content__options-button--hidden"
               title="Bearbeiten abbrechen"
               data-cancel='{ "club_id": "{{ $club->id }}",
								   "resource": { "id": "{{ $team->id }}",
								   "type": "team",
								   "url": "{{ route('team.single', [$club->slug, $team->id]) }}"
			   } }'>
                <span class="fa fa-times club-content__options-button-cancel"></span> </a>
            <a href="#" class="club-content__options-button" title="Löschen"
               data-delete='{ "club_id": "{{ $club->id }}",
								   "resource": { "id": "{{ $team->id }}",
								   "type": "team",
								   "url": "{{ route('team.delete', $team->id) }}"
			   } }'>
                <span class="fa fa-trash club-content__options-button-remove"></span> </a></div>
        {{--</div>--}}
    @endif
    <div data-edit-content class="club-post__container">
        <?php $teamcontactperson = $team->contacts()->get()->first(); ?>
        <div class="col-md-12">
            <p><strong>Altersklasse:</strong> {{ $team->age_class }}</p>
        </div>
        <div class="col-md-12">
            <p><strong>Liga:</strong> {{ $team->league }}</p>
        </div>
        @if($team->picture_file_name)
            <div class="col-md-12" style="margin:10px 0">
                <img class="thumbnail__image" src="{{asset($team->picture->url('singleView')) }}"/>
            </div>
        @endif
        <div class="col-md-12 teamdisp">
            <p>{!! nl2br(e($team->description)) !!}</p>
        </div>
        <div class="partner-space">
            <div class="col-md-12">
                <p><strong>Ansprechpartner</strong></p>
            </div>
            <div class="col-md-12">
                <p>@if(isset($teamcontactperson) && $teamcontactperson->gender == 'male')
                        <u>Herr @else Frau @endif
                            @if(isset($teamcontactperson))
                                <span
                                    onclick='window.open("{{ route('user.detail', ['username' => $teamcontactperson->username]) }}")'
                                    style="cursor: pointer;">
                                    {{$teamcontactperson->firstname}} {{$teamcontactperson->lastname}}
                                </span>
                            @else

                            @endif
                        </u>
                    </p>
            </div>
            <div class="col-md-12">
                <p>Telefon:@if(isset($teamcontactperson)) {{ $teamcontactperson->phone }}@endif</p>
            </div>
            <div class="col-md-12">
                @if(isset($teamcontactperson))
                    <p>E-Mail: <u><a href="mailto:{{$teamcontactperson->ap_email}}">{{$teamcontactperson->ap_email}}</a></u>
                    </p>
                @else
                    <p>E-Mail:</p>
                @endif
            </div>
        </div>
    </div>
</div>