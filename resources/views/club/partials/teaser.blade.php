<div class="col-md-3 col-sm-6">
    <figure class="club-tile">
        <div class="club-tile__image-wrapper">
            <a href="{{ route('club.detail', ['slug' => $club->slug]) }}">
                <img class="club-tile__image"
                     src="{{ asset($club->avatar->url('startPage')) }}"
                     alt=""/>
            </a>
        </div>
        <figcaption class="club-tile__content">
            <p class="club-tile__name">{{ $club->name }}</p>
        </figcaption>
        <a class="button button--center button--light button--full-width"
           href="{{ route('club.detail', ['slug' => $club->slug]) }}">Zum Vereinsprofil</a>
    </figure>
</div>