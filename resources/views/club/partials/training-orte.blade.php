<div class="" data-edit-wrapper>
    <div class="row">
        @if(Auth::check() && policy($club)->edit(Auth::user(), $club))
            <div class="col-xs-11 edit-del-btn">
                <div class="club-content__options">
                    <a href="#" class="club-content__options-button" title="Bearbeiten"
                       data-edit='{ "club_id": "{{ $club->id }}",
					   "resource": { "id": "{{ $trainingOrt->id }}",
					   "type": "post",
					   "url": "{{ route('training_ort.edit', $trainingOrt->id) }}"
					   } }'>
                        <span class="fa fa-pencil club-content__options-button-edit"></span>
                    </a>
                    <a href="#" class="club-content__options-button club-content__options-button--hidden"
                       title="Bearbeiten abbrechen"
                       data-cancel='{ "club_id": "{{ $club->id }}",
					   "resource": { "id": "{{ $trainingOrt->id }}",
					   "type": "post",
					   "url": "{{ route('post.single', [$club->slug, $trainingOrt->id]) }}"
					   } }'>
                        <span class="fa fa-times club-content__options-button-cancel"></span>
                    </a>
                    <a href="#" class="club-content__options-button" title="Löschen"
                       data-delete='{ "club_id": "{{ $club->id }}",
					   "resource": { "id": "{{ $trainingOrt->id }}",
					   "type": "post",
					   "url": "{{ route('training_ort.delete', $trainingOrt->id) }}"
					   } }'>
                        <span class="fa fa-trash club-content__options-button-remove"></span>
                    </a>
                </div>
            </div>
        @endif
    </div>
    <div data-edit-content class="row">
        <div class="col-lg-12">
            <div class="shaddow-box">
                <h3>{{ $trainingOrt->name }} @if($trainingOrt->title) - {{ $trainingOrt->title }} @endif</h3>
                <p>
                    {{ $trainingOrt->fullAddress() }}
                </p>
            </div>
        </div>
    </div>
</div>