<h2 class="club-content__box-headline">TRAININGS ORTE
    @can('edit', $club)
        <a class="pull-right"
           onClick="$('#add-trainingsort').toggleClass('pageable-menu__page--active');"
           href="#add-trainingsort">
            <span><i class="fa fa-plus-circle" aria-hidden="true"></i></span>
        </a>
    @endcan
</h2>

@can('edit', $club)
    @include('club.partials.edit.training-orte', ['newTrainingsort' => true])
@endcan

@if($trainingOrts->count() > 0)
    @foreach($trainingOrts as $trainingOrt)
        <div class="" data-edit-wrapper>
            <div class="row">
                @if(Auth::check() && policy($club)->edit(Auth::user(), $club))
                    <div class="col-xs-11 edit-del-btn">
                        <div class="club-content__options">
                            <a href="#" class="club-content__options-button" title="Löschen"
                               data-delete='{ "club_id": "{{ $club->id }}",
							   "resource": { "id": "{{ $trainingOrt->id }}",
							   "type": "post",
							   "url": "{{ route('training_ort.delete', [$club->slug, $trainingOrt->id]) }}"
							   } }'>
                                <span class="fa fa-trash club-content__options-button-remove"></span>
                            </a>
                        </div>
                    </div>
                @endif
            </div>
            <div data-edit-content class="row">
                <div class="col-lg-12">
                    <div class="shaddow-box">
                        <h3>{{ $trainingOrt->name }} @if($trainingOrt->title) - {{ $trainingOrt->title }} @endif</h3>
                        <p>
                            {{ $trainingOrt->fullAddress() }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@else
    <div class="club-post__item">
        <span class="club-post__date no-border">vereinsleben.de</span>
        <div class="club-post__content">
            Keine Trainigsorte vorhanden
        </div>
    </div>
@endif
