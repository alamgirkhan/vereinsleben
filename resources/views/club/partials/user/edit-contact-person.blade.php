<div id="contact" class="tabbable-menu__page">
    {!! Form::model(
        $club,
        array(
            'method' => 'post',
            'route' => ['club.add.contact', 'slug' => $club->slug, '#informationen'],
            'class' => '',
            'files' => 'false'
        )
    )!!}

    <div class="row">
        <div class="col-lg-12">
            {!! Form::label('name', 'Add Contact Person', ['class' => 'input-label']) !!}
            <div class="input-group">
                {!! Form::select('user', $users->pluck('username', 'username'),'', [
                        'class' => 'select',
                        'title' => 'contact person'
                    ])
                !!}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            {!! Form::submit('Add Ansprechpartner', array('class' => 'button button--light button--light-white button--center button--full-width')) !!}
        </div>
    </div>

    {!! Form::close() !!}
</div>