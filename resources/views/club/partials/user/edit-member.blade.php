{!! Form::model(
    $club,
    array(
        'method' => 'post',
        'route' => ['club.member.update.relation', 'slug' => $club->slug],
        'class' => '',
        'files' => 'false'
    )
)!!}


<div class="row">
    <div class="col-md-4">
        {!! Form::label('name', 'choose user', ['class' => 'input-label']) !!}
        <div class="input-group">
            {!! Form::select('user', $users->pluck('username', 'username'),'', [
                    'class' => 'select',
                    'title' => 'Mitglieder'
                ])
            !!}
        </div>
    </div>
    <div class="col-md-4">
        {!! Form::label('name', 'choose relation', ['class' => 'input-label']) !!}
        <div class="input-group">
            {!! Form::select('relation', ['1' => 'active member',
                         '2' => 'passive member',
                          '3' => 'old member',
                          '4' => 'trainer',
                           '5' => 'fan',
                           '6' => 'functionary'
                           ], '', [
                    'class' => 'select',
                    'title' => 'Relation'
                ])
            !!}
        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-4">
        {!! Form::submit('Relation speichern', array('class' => 'button button--light button--light-white button--center button--full-width')) !!}
    </div>
</div>