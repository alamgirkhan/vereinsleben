{{-- main template wrapping members and fans --}}

<div id="add-user-relation" class="tabbable-menu__page tabbable-menu__page--active">
    @include('club.partials.user.edit-member')
</div>

<div id="add-user-relation" class="tabbable-menu__page tabbable-menu__page--active">
    @include('club.partials.user.member-list', ['members' => $members])
</div>

<div class="container">
    <ul class="color-tabs" data-tabbable-menu="2">
        @if($members->count() > 0)
            <li class="color-tabs__tab active">
                <a class="color-tabs__link" href="#members">
                    @if($members->count() === 1)
                        {{ $members->count() }} Mitglied
                    @else
                        {{ $members->count() }} Mitglieder
                    @endif
                </a>
            </li>
        @endif
        @if($fans->count() > 0)
            <li class="color-tabs__tab @if($members->count() === 0) active @endif">
                <a class="color-tabs__link" href="#fans">
                    @if($fans->count() === 1)
                        {{ $fans->count() }} Fan
                    @else
                        {{ $fans->count() }} Fan's
                    @endif
                </a>
            </li>
        @endif
    </ul>
</div>

<div id="members" class="tabbable-menu__page tabbable-menu__page--active">
    @if($members->count() > 0)
        @include('club.partials.user.member', ['members' => $members])
    @endif
</div>

<div id="fans" class="tabbable-menu__page tabbable-menu__page--active">
    @if($fans->count() > 0)
        @include('club.partials.user.fan', ['fans' => $fans])
    @endif
</div>

{{-- @todo implement `no user (member/fan)` fallback --}}