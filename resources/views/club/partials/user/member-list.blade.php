<table class="table table-striped table-bordered">
    <tr>
        <th><strong>member</strong></th>
        <th><strong>member type</strong></th>
    </tr>
    @foreach($members as $member)
        <tr>
            <td>{{$member->username}}</td>
            @if($member->pivot->member)
                <td>{{$member->pivot->member->name}}</td>
            @endif
        </tr>
    @endforeach
</table>