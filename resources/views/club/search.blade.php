@extends('layouts.master')
@section('title')
    Finde deinen passenden Verein - vereinsleben.de
@endsection

@section('content')
    <section class="club-search">
        <div class="club-search__results-map">
            {!! $map !!}
        </div>

        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="sr-only">Verein finden</h1>
                    {!! Form::open(['route' => 'club.search', 'method' => 'GET', 'class' => 'club-search__form']) !!}
                        {!! Form::text('city', $query['city'], ['class' => 'club-search__form-input club-search__form-input--city city-autocomplete', 'data-cities-url' => app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('api.cities'), 'placeholder' => 'Ort oder PLZ']) !!}
                        {!! Form::text('keyword', $query['keyword'], ['class' => 'club-search__form-input club-search__form-input--keyword', 'placeholder' => 'Stichwort']) !!}
                        {!! Form::text('sport',   $query['sport'], ['class' => 'club-search__form-input club-search__form-input--sport sport-autocomplete', 'data-sports-url' => app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('api.sports'), 'placeholder' => 'Sportart']) !!}

                        {!! Form::select('distance', ['5' => '5km', '10' => '10km', '15' => '15km', '25' => '25km', '50' => '50km'], $query['distance'], ['class' => 'club-search__form-input club-search__form-distance']) !!}

                        {!! Form::button('Vereine finden', array('type' => 'submit', 'class' => 'club-search__form-submit button button--center button--light')) !!}
                    {!! Form::close() !!}
                </div>
                <div class="col-xs-12">
                    @if(!is_null($results))
                        @if($results->count() > 0)
                            <h2 class="club-search__results-headline"><span class="club-search__results-headline--count">{{ $results->total() }}</span> Vereine für Deine Suche gefunden</h2>

                            @foreach($results as $club)
                                @include('club.partials.listteaser', ['club' => $club])
                            @endforeach

                            {{ $results->links() }}
                        @else
                            <em>Leider keine Treffer!</em>
                        @endif
                    @else
                        <div class="club-search__hint-box">
                            <em>Bitte gib ein oder mehrere Suchkriterien an, um Vereine zu finden.</em>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection

