@if(count($upcomingEvents) >0)
<section class="toptips-section">
    <div class="toptips-section-inner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Top-Tipps</h2>
                    <h3>für Ihre Region</h3>
                </div>
            </div>
            <div class="row toptips-boxes">
                <section class="regular slider">
                    @foreach($upcomingEvents as $upcomingEvent)
                        <div class="col-lg-4 col-sm-6">
                            <div class="row">
                                <div class="col-lg-12 toptips-thum">
                                    <div class="toptips-arrow-top"><img
                                                src="{{asset('static_images/toptips-arrow-top.png')}}" alt=""></div>
                                    <img src="{{asset('static_images/toptips-thum1.jpg')}}" alt="">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="toptips-txt-box">
                                        <div class="toptips-txt-box-inner">
                                            <h4>{{$upcomingEvent->title}}</h4>
                                            <span class="tagtxt">Meisterschaft im Baseball</span>
                                            <ul class="toptips-links">
                                                <li><a href="#"><i class="fa fa-angle-right"
                                                                   aria-hidden="true"></i>{{date("M j, Y",strtotime($upcomingEvent->schedule_begin))}}
                                                        - {{date("M j, Y",strtotime($upcomingEvent->schedule_end))}}</a>
                                                </li>
                                                <li><a href="#"><i class="fa fa-angle-right"
                                                                   aria-hidden="true"></i>{{date("H:i",strtotime($upcomingEvent->schedule_begin))}}
                                                        - {{date("H:i",strtotime($upcomingEvent->schedule_end))}}</a>
                                                </li>
                                                <li><a href="#"><i class="fa fa-angle-right"
                                                                   aria-hidden="true"></i>{{$upcomingEvent->street}} {{$upcomingEvent->house_number}}
                                                    </a></li>
                                                <li><a href="#"><i class="fa fa-angle-right"
                                                                   aria-hidden="true"></i>{{$upcomingEvent->city}}</a>
                                                </li>
                                                <li><a href="#"><i class="fa fa-angle-right"
                                                                   aria-hidden="true"></i>{{$upcomingEvent->info}}</a>
                                                </li>
                                            </ul>
                                            <div class="mehr2">
                                                <a href="#">Mehr</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </section>
                </div>
            <div class="row registrieren">
                <div class="col-lg-12"><a class="btn btn-default registrieren-btn2" href="#">alle Tipps</a></div>
            </div>
        </div>
    </div>
</section>
@endif