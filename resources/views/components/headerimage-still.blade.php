{{-- A slider-compatible, but still-image header currently for the news admin --}}
<div class="headerimage-still__wrapper" style="position: relative;">
      <div class="hero-slider__image{{(isset($condensed)) ? ' hero-slider__image--condensed' : ''}}"
           style="background-image: url('https://s3.eu-central-1.amazonaws.com/vereinsleben/assets/images/vereinsleben.jpg')"></div>
    @include('components.slider-content')
</div>