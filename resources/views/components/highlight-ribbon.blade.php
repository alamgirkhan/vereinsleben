<section class="wirleben-section">
    <div class="container">
        @if(Request::is('suche/benutzer'))
            {!! Form::open(['route' => 'user.search', 'method' => 'GET', 'id' => 'search', 'class' => '']) !!}
        @else
            {!! Form::open(['route' => 'club.search', 'method' => 'GET', 'id' => 'search', 'class' => '']) !!}
        @endif
        <div class="row">
            <div class="col-lg-12">
                <h2>FINDE DEINEN VEREIN</h2>
                <h5 class="text-center">Finde den Verein, in dem du Mitglied bist oder suche nach einem neuen Verein</h5>
            </div>
        </div>
        <div class="row wirleben-section-search-area">
            <div class="col-lg-7 col-md-7 col-sm-6 col-centered">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                    {!! Form::text('keyword', isset($query['keyword']) ? $query['keyword'] : null, ['class' => 'form-control', 'placeholder' => 'STICHWORT']) !!}
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class="row">
        <!--<div class="col-lg-12"><a class="btn btn-default erweiter" href="{{ url('suche/vereine') }}">Erweiter Suche <i class="fa fa-caret-right" aria-hidden="true"></i></a></div>-->
        </div>
        <div class="row finden">
            <div class="col-lg-12">
                <a class="btn btn-default finden-btn" href="javascript:$( '#search' ).submit();">FINDEN</a></div>
        </div>
        {!! Form::close() !!}
    </div>
    <!--<div class="row" width="728px" height="90px" style="margin-left: 60%; position: absolute;">-->
</section>
@include('components.stoeer_adds')
<!--
<div class="absoluty-leaderboard">
    &nbsp;
</div>
<div class="absoluty">
    &nbsp;
</div>
-->
@push('scripts')
<script src="{{ asset(('js/OwlCarousel2/owl.carousel.js')) }}"></script>
@endpush

@include('components.number-of-week', ['numbers' => $numbers])

@if ($spitzensports && $spitzensports->count() > 0)
    @if (Auth::check())
        @if (Auth::user()->desiredState() && Auth::user()->desiredState()->name == 'Rheinland-Pfalz')
        @include('components.spitzensport-slider')
        @endif
    @elseif (isset($sessionState) && ($sessionState->name == 'Rheinland-Pfalz'))
    @include('components.spitzensport-slider')
    @endif
@endif
<?php /*
<section class="satte">
    <div class="container satte-bg">
        <div class="satte-bg-inner">
            <div class="row">
                <div class="col-xs-6 col-sm-3 percentage"><img src="{{asset('static_images/percentage-icon.png')}}" alt=""/></div>
                <div class="col-xs-6 col-sm-3 fortuna"><img src="{{asset('static_images/Fortuna-Kopie.png')}}" alt="" /></div>
            </div>
            <h2>Riesige Auswahl</h2>
            <h3>Über 23.000 Artikel im Online-Shop</h3>
            <div class="row">
                <div class="col-lg-9 col-sm-9 satte-detail">
                    <div class="row">
                        <div class="col-lg-8 col-sm-8 satte-detail-txt">Jetzt vereinsleben.de-Profil erstellen und bei
                            allen zukünftigen Aktionen von fortuna Sportgeräte kräftig sparen!
                        </div>
                        <div class="col-lg-4 col-sm-4 zum-shop"><a class="btn btn-default zum-shop-btn"
                                                                   href="https://www.fortuna-sport.de/?ReferrerID=9"
                                                                   target="_blank">Zum Shop</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wide-skyscraper">
    </div>
</section>
*/ ?>
<section id="vorteile" class="eure-section">
    <div class="eure-section-inner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Eure Vorteile</h2>
                    <h3>ALS VEREIN</h3>
                </div>
            </div>
            <div class="row eure-boxes">
                <div class="col-sm-4 col-md-4">
                    <div class="Vereinsprofil"><span class="secname"><a href="#">Profil übernehmen</a></span></div>
                    <p>Euren Verein suchen, das Profil übernehmen und alle relevanten Informationen für Fans und
                        Mitglieder eintragen.</p>
                </div>
                <div class="col-sm-4 col-md-4">
                    <div class="standort"><span class="secname"><a href="#">Gefunden werden</a></span></div>
                    <p>Ein gut ausgefülltes und informatives Vereinsprofil ist die beste Anlaufstelle für potentielle
                        Mitglieder.</p>
                </div>
                <div class="col-sm-4 col-md-4">
                    <div class="Wachsen"><span class="secname"><a href="#">Wachsen</a></span></div>
                    <p> Gewinnt neue Mitglieder, Interessenten und Sponsoren für euren Verein und werdet bekannt.</p>
                </div>
            </div>
            @if(!Auth::user())
            <div class="row registrieren">
                <div class="col-lg-12"><a class="btn btn-default registrieren-btn2" href="{{ url('/register') }}">Registrieren</a>
                </div>
            </div>
            @endif
        </div>
    </div>
</section>