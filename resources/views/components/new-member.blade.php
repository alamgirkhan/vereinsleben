<section class="section section--grey section--new-member section--member-cards" data-component="new-member">
    <div data-load-more='{"url": "{{ route('user.latest', ['inverted' => true]) }}"}'>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="hl hl--underlined hl--upper hl--large hl--centered">Community Mitglieder</h1>
                </div>

                <div class="col-xs-12">
                    @include('user.partials.member-card-wrapper')
                </div>
            </div>
        </div>
    </div>
</section>
