<div class="number-of-week">
    <div class="owl-carousel number-of-week owl-theme">
        @foreach($numbers as $number)
            <section class="deine-section" style="background-image: url('{{asset('uploaded/number-of-week/' . $number->image)}}')">
                <div class="number-opacity" style="opacity: {{ $number->opacity / 100}}"></div>
                <div class="deine-section-inner">
                    <div class="container">
                        <div class="row">
                            <div class="headline col-lg-12">
                                <h2>DIE ZAHL DER WOCHE</h2>
                                <span>{{date('d.m.', strtotime($number->published_from))}} - {{ date('d.m.Y', strtotime($number->published_from . ' + ' . 6 . ' days' )) }}</span>
                            </div>
                        </div>
                        <div class="deine-boxes">
                            <h1>{{$number->number}}</h1>
                            <p>{{ $number->short_description }}</p>
                        </div>
                    </div>
                </div>
                <div class="subline">
                    {!! $number->sub_line !!}
                </div>
            </section>
        @endforeach
    </div>
</div>

@push('scripts')
<script>
    var numberCount = '<?php echo count($numbers) ?>';
    $().ready(function(){
        var carousel = $(".owl-carousel.number-of-week");
        carousel.owlCarousel({
            nav:true,
            slideSpeed : 300,
            paginationSpeed : 400,
            items : 1,
            itemsDesktop : false,
            itemsDesktopSmall : false,
            itemsTablet: false,
            itemsMobile : false,
            navText: [
                '<img onmouseout="this.src=`{{ asset('static_images/btn_left_normal.png') }}`" onmouseover="this.src=`{{ asset('static_images/btn_left_over.png') }}`" src="{{ asset('static_images/btn_left_normal.png') }}"/>',
                '<img onmouseout="this.src=`{{ asset('static_images/btn_right_normal.png') }}`" onmouseover="this.src=`{{ asset('static_images/btn_right_over.png') }}`" src="{{ asset('static_images/btn_right_normal.png') }}"/>'
            ],
            startPosition: (numberCount - 1),
            autoHeight: false
        });
    });
</script>
@endpush