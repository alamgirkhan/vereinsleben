@if( isset($federalState) && $federalState->name == 'Rheinland-Pfalz')
<section class="partner-section">
    <div class="container">
        <div class="row mob-h2">
            <div class="col-lg-12">
                <h2>Unsere Partner</h2>
            </div>
        </div>
        <div class="row partner-inner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 premuium">
                        <div class="premuium-inner">
                            <h2>Unsere Partner</h2>
                            <p class="lotto-logo">
                                <a target="_blank" href="{{url("partner/lotto")}}">
                                    <img src="{{asset('static_images/lotto.jpg')}}" alt=""/>
                                </a>
                            </p>
                            <span class="parner-logo-cat">Premium</span>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-6 exclusiv-logos">
                        <div class="row partner-row">
                            <div class="col-6 sparda-bank">
                                <a target="_blank" href="{{url("partner/sparda-bank-suedwest")}}">
                                    <img src="{{asset('static_images/sparda-bank-logo.jpg')}}" alt=""/>
                                </a>
                                <span class="parner-logo-cat">Exklusiv</span>
                            </div>
                            <div class="col-6 abc-logo" style="background: #002f68!important; line-height: 400px!important;">
                                <a target="_blank" href="{{ url("partner/best-western") }}">
                                    <img src="{{asset('static_images/BW_Master_Brand_Logo_Negativ_weiss.png')}}" alt=""/>
                                </a>
                                <span class="parner-logo-cat dark">Exklusiv</span>
                            </div>
                            <div class="col-6 rpr1">
                                <a target="_blank" href="{{url("partner/rpr1")}}">
                                    <img src="{{asset('static_images/rpr1-logo.png')}}" alt=""/>
                                </a>
                                <span class="parner-logo-cat">Medien</span>
                            </div>
                            <div class="col-6 bigfum">
                                <a target="_blank" href="{{url("partner/bigfm")}}">
                                    <img src="{{asset('static_images/bigfm-logo.png')}}" alt=""/>
                                </a>
                                <span class="parner-logo-cat dark">Medien</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row sport-row">
                    <div class="col col-lg-1 col-sm-1 sport"><span class="parner-logo-cat">Sport</span></div>
                    <div class="col col-lg-11 col-sm-11">
                        <div class="row partner-row">
                            <div class="col sport-logo">
                                <a target="_blank" href="{{url("partner/landessportbund-rheinland-pfalz")}}">
                                    <img id="lsb-rlp-hoch-logo" src="{{asset('static_images/2017_Logo_LSB-RLP__hoch_c.png')}}" alt=""/>
                                </a>
                            </div>
                            <div class="col sport-logo">
                                <a target="_blank" href="{{url("partner/sportbund-rheinland")}}">
                                    <img src="{{asset('static_images/partner-sportbund-rheinland.png')}}" alt=""/>
                                </a>
                            </div>
                            <div class="col sport-logo">
                                <a target="_blank" href="{{url("partner/sportbund-rheinhessen")}}">
                                    <img src="{{asset('static_images/partner-sportbund-rheinhessen.png')}}" alt=""/>
                                </a>
                            </div>
                            <div class="col sport-logo">
                                <a target="_blank" href="{{url("partner/sportbund-pfalz")}}">
                                    <img src="{{asset('static_images/partner-sportbund-rlp.png')}}" alt=""/>
                                </a>
                            </div>
                            <div class="col sport-logo">
                                <a target="_blank" href="{{url("partner/deutscher-dart-verband")}}">
                                    <img src="{{asset('static_images/partner-ddv.png')}}" alt=""/>
                                </a>
                            </div>
                            <div class="col sport-logo swfv">
                                <a target="_blank" href="{{url("partner/suedwestdeutscher-fussballverband")}}">
                                    <img src="{{asset('static_images/partner_swfv_startseite.gif')}}" alt=""/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="clear"></div>
@elseif(isset($federalState) && $federalState->name == 'Baden-Württemberg')
    <section class="partner-logo">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 partner-logo-in">
                    <ul>
                        <li><a target="_blank" href="{{ url("partner/best-western") }}">
                                    <img src="{{url('/static_images/bw-logo-small.png')}}" width="90%" height="auto" style="margin-top: 5px;"
                                         onmouseover="this.src='{{url('/static_images/bw-logo-small-hover.png')}}'"
                                         onmouseout="this.src='{{url('/static_images/bw-logo-small.png')}}'"/>
                                </a>
                        </li>
                        <li><a target="_blank" href="{{url("partner/bigfm")}}" class="partner-bigfm"></a></li>
                        <li><a target="_blank" href="{{ url("partner/deutscher-dart-verband") }}">
                                    <img src="{{url('/static_images/ddv-logo-small.png')}}" width="70%" height="70%"
                                         onmouseover="this.src='{{url('/static_images/ddv-logo-small-hover.png')}}'"
                                         onmouseout="this.src='{{url('/static_images/ddv-logo-small.png')}}'"/>
                                </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
@elseif(isset($federalState) && $federalState->name == 'Saarland')
    <section class="partner-logo">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 partner-logo-in">
                    <ul>
                        <li><a target="_blank" href="{{url("partner/sparda-bank-suedwest")}}" class="sparda-sw"></a></li>
                        <li><a target="_blank" href="{{ url("partner/best-western") }}">
                                    <img src="{{url('/static_images/bw-logo-small.png')}}" width="90%" height="auto" style="margin-top: 5px;"
                                         onmouseover="this.src='{{url('/static_images/bw-logo-small-hover.png')}}'"
                                         onmouseout="this.src='{{url('/static_images/bw-logo-small.png')}}'"/>
                                </a></li>
                        <li><a target="_blank" href="{{ url("partner/deutscher-dart-verband") }}">
                                    <img src="{{url('/static_images/ddv-logo-small.png')}}" width="70%" height="70%"
                                         onmouseover="this.src='{{url('/static_images/ddv-logo-small-hover.png')}}'"
                                         onmouseout="this.src='{{url('/static_images/ddv-logo-small.png')}}'"/>
                                </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
@else
    <section class="partner-logo">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 partner-logo-in">
                    <ul>
                       <li><a target="_blank" href="{{ url("partner/best-western") }}">
                                    <img src="{{url('/static_images/bw-logo-small.png')}}" width="90%" height="auto" style="margin-top: 5px;"
                                         onmouseover="this.src='{{url('/static_images/bw-logo-small-hover.png')}}'"
                                         onmouseout="this.src='{{url('/static_images/bw-logo-small.png')}}'"/>
                                </a></li>
                        <li><a target="_blank" href="{{ url("partner/deutscher-dart-verband") }}">
                                    <img src="{{url('/static_images/ddv-logo-small.png')}}" width="70%" height="70%"
                                         onmouseover="this.src='{{url('/static_images/ddv-logo-small-hover.png')}}'"
                                         onmouseout="this.src='{{url('/static_images/ddv-logo-small.png')}}'"/>
                                </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
@endif	