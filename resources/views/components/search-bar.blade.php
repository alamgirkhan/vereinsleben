<div class="container">
    <div class="row">
        <div class="col-xs-12">
            {!! Form::open(['route' => 'club.search', 'method' => 'GET']) !!}
            <div class="quick-search">
                {!! Form::text('keyword', null, ['class' => 'input quick-search__input', 'placeholder' => 'Suche']) !!}
                {!! Form::submit('Finden', ['class' => 'btn btn--turquoise btn--full-width quick-search__submit btn--upper']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>