@if(isset($content))
    <div class="hero-slider__content hero-slider__content--centered">
        <h1 class="headline-primary headline--centered headline-primary--inverted">
            @if(isset($content['icon']))<span class="fa fa-{{$content['icon']}}"></span>@endif
            {{ $content['headline'] }}
        </h1>
        @if(isset($content['caption']))
            <h2 class="hero-slider__caption headline--centered">{{ $content['caption'] }}</h2>
        @endif
    </div>
@else
    <div class="container-flex">
        <div class="hero-slider__content">
            <h1 class="headline-primary headline-primary--main headline-primary--inverted">Vereinsleben.de</h1>
            @if(\Carbon\Carbon::now() < '2016-12-30')
                <h2 class="headline-secondary headline-secondary--main headline-secondary--inverted">wünscht sportliche
                    Weihnachten</h2>
            @else
                <h2 class="headline-secondary headline-secondary--main headline-secondary--inverted">Wir. Leben. Sport.</h2>
            @endif
            <div class="hero-slider__box-wrapper">
                @if(!Auth::check())
                    <a href="{{ url('/register') }}" class="hero-slider__big-btn-text">
                        <div class="hero-slider__big-btn">
                            Registriere Dich jetzt und lege Deinen Verein an!
                        </div>
                    </a>
                @else
                    <div class="hero-slider__big-btn-text">
                        <div class="hero-slider__big-btn">
                            Registriere Dich jetzt und lege Deinen Verein an!
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endif