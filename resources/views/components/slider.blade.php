<div class="hero-slider__wrapper {{(isset($condensed)) ? ' hero-slider__wrapper--condensed' : ''}}">
    <div class="hero-slider">
        <div class="hero-slider__image"
             style="background-image: url('{{asset('static_images/vereinsleben_9.jpg')}}')"></div>
        <div class="hero-slider__image"
             style="background-image: url('{{asset('static_images/vereinsleben_7.jpg')}}')"></div>
        <div class="hero-slider__image"
             style="background-image: url('{{asset('static_images/vereinsleben.jpg')}}')"></div>
        <div class="hero-slider__image"
             style="background-image: url('{{asset('static_images/vereinsleben_8.jpg')}}')"></div>
    </div>

    @unless(isset($excludeContent))
        @include('components.slider-content')
    @endunless
</div>