@if($slider->count() > 0)
<div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="roundimg-down"><a href="#"><i class="fa fa-angle-down" aria-hidden="true"></i></a></div>
            </div>
        </div>
    </div>
    <div class="startpage-slider">
        @foreach($slider as $key => $slide)
            {{--<div class="carousel-item @if($key==0) active @endif"--}}
            {{--style="background-image: url({{ asset($slide->slider_image->url('original')) }})">--}}
            {{--{!! $slide->description !!}--}}
            {{--</div>--}}
            <?php
            $slideCount = 0;
            for ($i = 1; $i < 7; $i++) {
                if ($slide["logo{$i}_file_name"]) {
                    $slideCount = $slideCount + 1;
                }
            }
            ?>
            <div class="carousel-item {{$key == 0 ? 'active' : ''}} {{$slideCount > 2 ? 'small small'.$slideCount : ''}}"
                 style="background-image: url({{ asset($slide->slider_image->url('original')) }})">

                <div class="carousel-caption d-none d-md-block">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-7 col-sm-7 slide2">

                                @isset($slide->h2)
                                    <h2>{!! $slide->h2 !!}</h2><br>
                                @endisset

                                @isset($slide->h3)
                                    <h3>{!! $slide->h3 !!}</h3><br>
                                @endisset

                                <span class="slider2-logos">
                                    @isset($slide->logo1_file_name)
                                        <img src="{{ asset($slide->logo1->url('original')) }}" alt=""
                                             style="display: inline-block; margin-right: 7px;"/>
                                    @endisset
                                </span>
                                    <span class="slider2-logos">
                                    @isset($slide->logo2_file_name)
                                        <img src="{{ asset($slide->logo2->url('original')) }}" alt=""
                                             style="display: inline-block; margin-right: 7px;"/>
                                    @endisset
                                </span>
                                    <span class="slider2-logos">
                                    @isset($slide->logo3_file_name)
                                        <img src="{{ asset($slide->logo3->url('original')) }}" alt=""
                                             style="display: inline-block; margin-right: 7px;"/>
                                    @endisset
                                </span>
                                    <span class="slider2-logos">
                                    @isset($slide->logo4_file_name)
                                        <img src="{{ asset($slide->logo4->url('original')) }}" alt=""
                                             style="display: inline-block; margin-right: 7px;"/>
                                    @endisset
                                </span>
                                    <span class="slider2-logos">
                                    @isset($slide->logo5_file_name)
                                        <img src="{{ asset($slide->logo5->url('original')) }}" alt=""
                                             style="display: inline-block; margin-right: 7px;"/>
                                    @endisset
                                </span>
                                    <span class="slider2-logos">
                                    @isset($slide->logo6_file_name)
                                        <img src="{{ asset($slide->logo6->url('original')) }}" alt=""
                                             style="display: inline-block; margin-right: 7px;"/>
                                    @endisset
                                </span>

                                @isset($slide->btn)
                                    <br>
                                    <div class="Jetzt-btn"><a href="{{ $slide->link }}">{{ $slide->btn }}</a></div>
                                @endisset

                            </div>
                        </div>
                    </div>
                </div>


            </div>

        @endforeach
    </div>
</div>
@endif