<div class="section" data-token="{{ csrf_token() }}"
     data-mapping-url="{{ app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('api.club.autocomplete') }}">

    @if(Auth::check())

        @if($voted == 1)

            <div class="container">
                <h2 class="hl hl--large hl--underlined hl--inverted hl--centered hl--upper">
                    Du hast bereits teilgenommen
                </h2>
                <p class="text-block text-block--compressed text-block--thin">
                    Du hast bereits am Gewinnspiel teilgenommen, damit Dein Verein den coolsten Teambesprechungsraum
                    Deutschlands gewinnen kann.
                    Aber um die Gewinnchancen für Deinen Verein zu erhöhen, solltest Du natürlich möglichst vielen
                    Vereinsmitgliedern und Freunden von diesem Gewinnspiel erzählen. Es lohnt sich ...
                </p>
            </div>
            <div class="container">
                <div class="col-md-8 col-md-offset-2">
                    {!! Form::open(['route' => 'contest.invite', 'method' => 'GET', 'id' => 'contest', 'class' => '']) !!}
                    {{ csrf_field() }}
                    <div class="row" id="emailFields">
                        <div class="col-md-12">
                            <div class="input-group">
                                {!! Form::text('email0', '', ['id' => 'email0', 'name' => 'email0', 'class' => 'input', 'required' => 'required', 'placeholder' => 'E-Mail Adresse']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="input-group">
                                {!! Form::text('email1', '', ['id' => 'email1', 'name' => 'email1', 'class' => 'input', 'placeholder' => 'E-Mail Adresse']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="input-group">
                                {!! Form::text('email2', '', ['id' => 'email2', 'name' => 'email2', 'class' => 'input', 'placeholder' => 'E-Mail Adresse']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row col-md-offset-3">
                        <div class="col-md-12">
                            <i class="fa fa-plus-circle" aria-hidden="true" onclick="addField();"
                               style="font-size: 200%; vertical-align: middle; cursor: pointer;" id="plusbutton"></i>&nbsp;&nbsp;&nbsp;
                            <div onclick="addField();" style="cursor: pointer; display: inline-block" id="einladung">
                                Einen
                            weiteren
                                Freund hinzufügen
                            </div>
                            <input type="hidden" id="userid" name="userid" value="{{ $user->id }}">
                            <input type="hidden" id="clubid" name="clubid"
                                   value="{{ isset($contestClub) ? $contestClub : null }}">
                        </div>
                    </div>
                    <br><br><br>
                    <div class="row col-md-offset-3">
                        <div class="col-md-8">
                            <div class="input-group">
                                {!! Form::submit('Einladung(en) versenden', array('class' => 'button button--light button--light-white button--center button--full-width')) !!}
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        @else
            <div class="container">

                <div>
                    <h2 class="hl hl--large hl--underlined hl--inverted hl--centered hl--upper">
                        Ausfüllen, Absenden und Gewinnen
                    </h2>
                    <p class="text-block text-block--compressed text-block--thin">
                        Tragt hier einfach den Verein ein, der unbedingt den coolsten Teambesprechungsraum Deutschlands
                        gewinnen sollte, vervollständige deine Daten und schicke das Formular ab.
                    </p>
                </div>

                <div class="col-md-8 col-md-offset-2">
                    {!! Form::open(['route' => 'contest.request', 'method' => 'GET', 'id' => 'contest', 'class' => '']) !!}
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-12"><input type="hidden" id="federal_state" value="">
                            {!! Form::text('club', isset($suggested->name) ? $suggested->name : null ,['class' => 'input club-autocomplete', 'data-clubs-url' => app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('api.club.autocomplete'), 'placeholder' => 'Name des Vereins', 'required']) !!}
                            <div class="col-xs-6 input-hint pull-right">
                                Nicht gefunden?
                                <a href="" class=""
                                   data-modal-url1="{{ route('user.club.contest.wizard')}}"
                                   data-modal1="team-member" id="clubwizard" data-token="{{ csrf_token() }}">
                                    Verein vorschlagen
                                </a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="input-group">
                                {!! Form::text('name', isset($user->lastname) ? $user->lastname : null, ['id' => 'name', 'class' => 'input', 'placeholder' => 'Name', 'required' => 'required']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="input-group">
                                {!! Form::text('vorname', isset($user->firstname) ? $user->firstname : null, ['id' => 'vorname', 'class' => 'input', 'placeholder' => 'Vorname', 'required' => 'required']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="input-group">
                                {!! Form::select('federal', array('' => 'Bitte wählen')+$federal->pluck('name', 'name')->all(), $user->federal_state, ['class' => 'select', 'id' => 'state']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group">
                                {!! Form::text('plz', isset($user->zip) ? $user->zip : null, ['id' => 'plz', 'class' => 'input', 'placeholder' => 'PLZ', 'required' => 'required']) !!}
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="input-group">
                                {!! Form::text('ort', isset($user->city) ? $user->city : null, ['id' => 'ort', 'class' => 'input', 'placeholder' => 'Wohnort', 'required' => 'required']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="input-group">
                                {!! Form::text('email', isset($user->email) ? $user->email : null, ['id' => 'email', 'class' => 'input', 'disabled', 'required' => 'required']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-group">
                            <div class="col-md-12 rgs-slt-area">
                                <div class="input-group">
                                    <input class="" required="required" type="checkbox" name="terms" id="terms"
                                           value="1">
                                    <label class="" for="terms" id="termserror">Ich habe die
                                        <a class="input-checkbox__label-link"
                                           href="{{ url('/content/teilnahmebedingungen-teambesprechungsraum') }}"
                                           target="_blank">
                                            Teilnahmebedingungen</a> gelesen und bin mit ihnen einverstanden.</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group">
                                {!! Form::submit('Absenden', array('class' => 'button button--light button--light-white button--center button--full-width')) !!}
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        @endif
    @else
        <div class="container">
            <h2 class="hl hl--large hl--underlined hl--inverted hl--centered hl--upper">
                Du möchtest mitmachen?
            </h2>
            <p class="text-block text-block--compressed text-block--thin">
                Natürlich braucht Dein Verein unbedingt den coolsten Teambesprechungsraum Deutschlands, also nix wie
                los.
                Gleich in nur 2 Minuten ein neues Benutzerkonto anlegen oder mit deinen Benutzerdaten anmelden und dann
                das
                Gewinnspielformular abschicken. Die <a class="input-checkbox__label-link"
                                                       href="{{ url('/content/teilnahmebedingungen-teambesprechungsraum') }}"
                                                       target="_blank">Teilnahmebedingungen</a>
                findest du hier.
            </p>
        </div>
        <div class="container" align="center">

            <div class="col-xs-3"></div>
            <div class="col-md-3">
                <a href="/login" class="button button--center button--full-width button--light button--light-white">Anmelden</a>
            </div>
            <div class="col-md-3">
                <a href="/register" class="button button--center button--full-width button--light button--light-white">Benutzerkonto
                    anlegen</a>
            </div>
            <div class="col-xs-3"></div>

        </div>
    @endif
</div>
