<header class="contest-header">
    <div class="container">
        <div class="hero-slider__content hero-slider__content--centered" style="color:white;">
            <h1 class="vote-header__headline" style="text-align:center; color:white;">
                COOLSTER TEAMBESPRECHUNGSRAUM
            </h1>
            <span class="vote-header__subline-contest" style="text-align:center; align:center;">Gewinnt den coolsten Teambesprechungsraum für Euren Verein</span>
        </div>
    </div>
</header>