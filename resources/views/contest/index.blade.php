@extends('layouts.master')
@section('title', 'vereinsleben.de - Euer Teambesprechungsraum - Gewinnt den coolsten Teambesprechungsraum für euren Verein')
@section('meta_description', 'Jetzt mitmachen und gewinnen, gewinnt den coolsten Teambesprechungsraum für euren Verein.')

@section('content')

    <div id="contest-app">

        {{-- contest header --}}
        @include('contest.header')

        @include('contest.reward')

        @include('contest.contest')

    </div>
@endsection
{{--@push('scripts')--}}
    {{--<script>--}}
        {{--$(document).ready(function () {--}}

            {{--var spanis = document.querySelectorAll('span'),--}}
                {{--index;--}}

            {{--for (index = 0; index < spanis.length; index++) {--}}
                {{--spanis[index].setAttribute('style', 'position:relative; display:;');--}}
            {{--}--}}
        {{--});--}}

        {{--var fieldId = 2; // used by the addFile() function to keep track of IDs--}}

        {{--function addField() {--}}
            {{--fieldId++; // increment fileId to get a unique ID for the new element--}}
            {{--if (fieldId > 19) {--}}
                {{--document.getElementById('einladung').style.display = 'none';--}}
                {{--document.getElementById('plusbutton').style.display = 'none';--}}

                {{--return;--}}
            {{--}--}}
            {{--var html = '<input type="text" name="email' + fieldId + '" id="email' + fieldId + '" placeholder="E-Mail Adresse" class="input" />\n';--}}
            {{--addElement('emailFields', html);--}}
        {{--}--}}

        {{--function addElement(parentId, html) {--}}
            {{--// Adds an element to the document--}}
            {{--var p = document.getElementById(parentId);--}}
            {{--var newElement = document.createElement("div");--}}
            {{--var innerElement = document.createElement("div");--}}

            {{--newElement.setAttribute('class', 'col-md-12');--}}
            {{--innerElement.setAttribute('class', 'input-group');--}}
            {{--innerElement.innerHTML = html;--}}
            {{--newElement.appendChild(innerElement);--}}
            {{--p.appendChild(newElement);--}}
        {{--}--}}


    {{--</script>--}}
{{--@endpush--}}