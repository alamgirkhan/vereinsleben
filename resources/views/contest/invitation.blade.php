<!DOCTYPE html>
<html lang="de-DE">
<head>
    <meta charset="utf-8">
</head>
<body>
<p>Hallo,</p>

<div>
    ich habe gerade auf vereinsleben.de an einem tollen Gewinnspiel teilgenommen, bei dem
    der {{ $club }} einen komplett mit tollen Retromöbeln ausgestatteten
    Teambesprechungsraum im Gesamtwert von 20.000 Euro gewinnen kann.<br><br>

    Um die Chancen für den Verein zu verbessern, müssen so viele Personen wie möglich mitmachen. <br>
    Es wäre super, wenn Du auch mitmachen würdest. Die Teilnahme dauert nur ein paar Minuten. Klick dazu einfach auf
    diesen Link:
    <br><br>
    <a href="{{ URL::to('teambesprechungsraum') }}">{{ URL::to('teambesprechungsraum') }}</a>
    <br><br>
    Vielen Dank für Deine Hilfe und viele Grüße,<br>
    {{ $user }}
</div>
@if(isset($imprint))
    <br/>
    <br/>
    <br/>
    <div>{!! $imprint->content !!}</div>
@endif
</body>
</html>