<section>

    <div class="container">

        <h2 class="hl hl--large hl--underlined hl--inverted hl--centered hl--upper">
            Das Gewinnspiel ist beendet
        </h2>
        <p class="text-block text-block--compressed text-block--thin">
            Das Gewinnspiel ist beendet und wir danken allen, die mitgemacht haben, für die rege Teilnahme.
            Die Auslosung findet am Dienstag, den 4. Dezember 2018 um 16 Uhr statt und wird live auf unserer Facebook-Seite übertragen.
            Der Gewinner-Verein wird danach auch hier auf dieser Seite bekanntgegeben und natürlich persönlich von uns benachrichtigt.
        </p>

    </div>


    {{--<div class="container">--}}

        {{--<h2 class="hl hl--large hl--underlined hl--inverted hl--centered hl--upper">--}}
            {{--Darum müsst ihr mitmachen--}}
        {{--</h2>--}}
        {{--<p class="text-block text-block--compressed text-block--thin">--}}
            {{--Kaputte oder ungemütliche Stühle, ein Tisch, der schon wackelt, wenn man ihn nur anschaut und eine winzige--}}
            {{--Tafel, um taktische Vorgaben aufzumalen …--}}
            {{--Das ist die Realität in vielen Besprechungs- und Aufenthaltsräumen von Deutschlands Sportvereinen. Wir--}}
            {{--wollen das ändern! Deswegen „pimpen“ wir Euren--}}
            {{--Besprechungsraum zum coolsten Teambesprechungsraum Deutschlands!--}}
        {{--</p>--}}

    {{--</div>--}}

    {{--<div class="container">--}}
        {{--<h2 class="hl hl--large hl--underlined hl--inverted hl--centered hl--upper">--}}
            {{--Das alles könnt ihr gewinnen--}}
        {{--</h2>--}}
        {{--<p class="text-block text-block--compressed text-block--thin">--}}
            {{--Gemeinsam mit fortuna Sportgeräte verlosen wir vom 1. Oktober bis 30. November 2018 eine Komplettausstattung--}}
            {{--für--}}
            {{--Euren Teambesprechungsraum im Wert von 20.000 Euro.--}}
            {{--Die Gewinne sind dabei natürlich alles andere als „von der Stange“ – aus alten Turngeräten in Handarbeit--}}
            {{--gefertigt verlosen wir exklusiv eine--}}
            {{--große Sitzgruppe aus Turnkästen mit Glastisch, eine Zweisitzer- und eine Dreisitzer-Couch mit Couchtisch,--}}
            {{--drei Turnbänke sowie ein passendes Flipchart!--}}
        {{--</p>--}}

        {{--<div class="row">--}}
            {{--<div class="col-xxs-12 col-md-4">--}}
                {{--<div class="contest-win-teaser">--}}
                    {{--<div class="contest-win-teaser__image--price-1">--}}
                    {{--</div>--}}
                    {{--<p class="text-block text-block--thin vote-win-teaser__text">--}}
                        {{--Die robusten Turnbänke aus massivem Holz kommen direkt aus dem wilden Sporthallenleben und--}}
                        {{--bieten genug Platz für große Teams.--}}

                    {{--</p>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="col-xxs-12 col-md-4">--}}
                {{--<div class="contest-win-teaser">--}}
                    {{--<div class="contest-win-teaser__image--price-2">--}}
                    {{--</div>--}}
                    {{--<p class="text-block text-block--thin vote-win-teaser__text">--}}
                        {{--In dieser gemütlichen Couchecke mit einem Bezug aus Turngeräteleder möchte man gerne mal eine--}}
                        {{--Stunde nachsitzen.--}}
                    {{--</p>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="col-xxs-12 col-md-4">--}}
                {{--<div class="contest-win-teaser">--}}
                    {{--<div class="contest-win-teaser__image--price-3">--}}
                    {{--</div>--}}
                    {{--<p class="text-block text-block--thin vote-win-teaser__text">--}}
                        {{--Für konstruktive Teambesprechungen oder gemütliches Beisammensein: Die Sitzgruppe im angesagten--}}
                        {{--Retro-Look ist ein echter Hingucker.--}}
                    {{--</p>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="col-xxs-12 col-md-12">--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="row">--}}
            {{--<h2 class="hl hl--large hl--underlined hl--inverted hl--centered hl--upper">--}}
                {{--So könnt ihr Eure Chance erhöhen--}}
            {{--</h2>--}}
            {{--<p class="text-block text-block--compressed text-block--thin">--}}
                {{--Um die Chance zu erhöhen, dass Euer Verein den coolsten Teambesprechungsraum Deutschlands gewinnt,--}}
                {{--solltet Ihr diese Aktion unbedingt in Eurem Verein--}}
                {{--und Eurem Freundeskreis weitererzählen! Denn je mehr Leute für Euren Verein an der Aktion teilnehmen,--}}
                {{--desto größer ist natürlich die Chance, dass der Gewinn auch tatsächlich an Euren Verein geht!--}}
            {{--</p>--}}
        {{--</div>--}}
    {{--</div>--}}
</section>