@extends('layouts.master')
@section('title', 'vereinsleben.de - Euer Teambesprechungsraum - Gewinnt den coolsten Teambesprechungsraum für euren Verein')
@section('meta_description', 'Jetzt mitmachen und gewinnen, gewinnt den coolsten Teambesprechungsraum für euren Verein.')

@section('content')

    <div id="contest-app">

        <div class="section section--vote-white vote-winner__content" data-token="{{ csrf_token() }}">

            @if(Auth::check())

                <div class="container">
                    <h2 class="hl hl--large hl--underlined hl--inverted hl--centered hl--upper">
                        Vielen Dank für’s Mitmachen
                    </h2>
                    <p class="text-block text-block--compressed text-block--thin">
                        Vielen Dank für Deine Teilnahme. Wir drücken Dir jetzt natürlich die Daumen, dass Dein Verein
                        den
                        neuen Teambesprechungsraum gewinnt. Aber das hat nicht nur mit Glück zu tun – auch Du kannst die
                        Chancen für Deinen Verein erhöhen.
                    </p>
                </div>

                <div class="container">
                    <h2 class="hl hl--large hl--underlined hl--inverted hl--centered hl--upper">
                        Sag’ Deinen Freunden Bescheid
                    </h2>
                    <p class="text-block text-block--compressed text-block--thin">
                        Mit jeder weiteren Person, die am Gewinnspiel für Deinen Verein teilnimmt, steigen die Chancen
                        für Deinen Verein.
                        Trage hier direkt die E-Mail-Adressen von bis zu 20 Freunden ein, um sie einzuladen, für Deinen
                        Verein am Gewinnspiel teilzunehmen.
                        <br><br>
                        <b>Wichtig:</b> Du brauchst Dir keine Sorgen zu machen. Die hier eingebenen E-Mail-Adressen
                        werden nicht von uns gespeichert
                        und nur für den Versand der Einladungs-E-Mail genutzt.
                    </p>
                </div>

                <div class="container">
                    <div class="col-md-8 col-md-offset-2">
                        {!! Form::open(['route' => 'contest.invite', 'method' => 'GET', 'id' => 'contest', 'class' => '']) !!}
                        {{ csrf_field() }}
                        <div class="row" id="emailFields">
                            <div class="col-md-12">
                                <div class="input-group">
                                    {!! Form::text('email0', '', ['id' => 'email0', 'name' => 'email0', 'class' => 'input', 'required' => 'required', 'placeholder' => 'E-Mail Adresse']) !!}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="input-group">
                                    {!! Form::text('email1', '', ['id' => 'email1', 'name' => 'email1', 'class' => 'input', 'placeholder' => 'E-Mail Adresse']) !!}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="input-group">
                                    {!! Form::text('email2', '', ['id' => 'email2', 'name' => 'email2', 'class' => 'input', 'placeholder' => 'E-Mail Adresse']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row col-md-offset-3">
                            <div class="col-md-12">
                                <i class="fa fa-plus-circle" aria-hidden="true" onclick="addField();"
                                   style="font-size: 200%; vertical-align: middle; cursor: pointer;"
                                   id="plusbutton"></i>&nbsp;&nbsp;&nbsp;
                                <div onclick="addField();" style="cursor: pointer; display: inline-block"
                                     id="einladung">
                                    Einen
                                    weiteren
                                    Freund hinzufügen
                                </div>
                                <input type="hidden" id="userid" name="userid" value="{{ $user->id }}">
                                <input type="hidden" id="clubid" name="clubid" value="{{ $club }}">
                            </div>
                        </div>
                        <br><br><br>
                        <div class="row col-md-offset-3">
                            <div class="col-md-8">
                                <div class="input-group">
                                    {!! Form::submit('Einladung(en) versenden', array('class' => 'button button--light button--light-white button--center button--full-width')) !!}
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            @endif
        </div>

    </div>
@endsection
@push('scripts')
    <script>

        var fieldId = 2; // used by the addFile() function to keep track of IDs

        function addField() {
            fieldId++; // increment fileId to get a unique ID for the new element
            if (fieldId > 19) {
                document.getElementById('einladung').style.display = 'none';
                document.getElementById('plusbutton').style.display = 'none';

                return;
            }
            var html = '<input type="text" name="email' + fieldId + '" id="email' + fieldId + '" placeholder="E-Mail Adresse" class="input" />\n';
            addElement('emailFields', html);
        }

        function addElement(parentId, html) {
            // Adds an element to the document
            var p = document.getElementById(parentId);
            var newElement = document.createElement("div");
            var innerElement = document.createElement("div");

            newElement.setAttribute('class', 'col-md-12');
            innerElement.setAttribute('class', 'input-group');
            innerElement.innerHTML = html;
            newElement.appendChild(innerElement);
            p.appendChild(newElement);
        }
    </script>
@endpush
