@extends('layouts.master')

@section('content')
    <section class="error-page__section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="error-page__headline">Fehler (500): Es ist ein technischer Fehler aufgetreten.</h1>

                    <p>Unser Team wurde benachrichtigt und k&uuml;mmert sich darum! Vielen Dank f&uuml;r Deine Geduld!</p>

                    <p><br><br><a class="button button--center button--light button--full-width" href="{{ route('home') }}"><i class="fa fa-home"></i> Zur Startseite</a></p>
                </div>
            </div>
        </div>
    </section>
@endsection
