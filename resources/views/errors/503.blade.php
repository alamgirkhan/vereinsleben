@extends('layouts.master')

@section('content')
    <section class="error-page__section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="error-page__headline">Wir f&uuml;hren derzeit Wartungsarbeiten durch und sind in K&uuml;rze wieder f&uuml;r Dich da.</h1>
                </div>
            </div>
        </div>
    </section>
@endsection
