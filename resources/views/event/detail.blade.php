@extends('layouts.master')
@section('title')

@endsection

@section('content')
    <section class="news-detail">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-8 col-sm-7 col-xs-12 news">
                    <h2>{{ $event->title }}</h2>
                    <div class="news-lead-img">
                        @if($event->images()->count() == 1)
                            @foreach($event->images as $image)
                                <img src="{{ asset($image->picture->url('singleView')) }}" alt="">
                            @endforeach
                        @else
                            <img src="{{asset('static_images/toptips-thum1.jpg')}}" alt="">
                        @endif
                    </div>
                    <div class="col-md-12 teamdisp">
                        <p>{!! nl2br(e($event->content)) !!}</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="rhs-box">
                                <h2>Event Teilen</h2>
                                <ul class="social-icons">
                                    <li>
                                        <a href="https://www.facebook.com/sharer/sharer.php?u="
                                           target="_blank"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a href="https://twitter.com/intent/tweet?url="
                                           target="_blank"><i class="fa fa-twitter"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="toptips-txt-box">
                                <div class="toptips-txt-box-inner">
                                    <h4>Auf einen Blick</h4>
                                    <span class="tagtxt">{{ $event->title }}</span>
                                    <ul class="toptips-links">
                                        <li><a href="#"><i class="fa fa-angle-right"
                                                           aria-hidden="true"></i>
                                                @php $date_start = \Carbon\Carbon::parse($event->schedule_begin) @endphp
                                                @php $date_start = $date_start->format('d.m.Y') @endphp
                                                @php $date_end = \Carbon\Carbon::parse($event->schedule_end) @endphp
                                                @php $date_end = $date_end->format('d.m.Y') @endphp

                                                {{ $date_start }} - {{ $date_end }}
                                            </a>
                                        </li>
                                        <li><a href="#"><i class="fa fa-angle-right"
                                                           aria-hidden="true"></i>
                                                @php $time = \Carbon\Carbon::parse($event->schedule_begin) @endphp
                                                @php $time = $time->format('H:i') @endphp
                                                {{ $time }} Uhr
                                            </a>
                                        </li>
                                        @if($event->location != null && $event->location != '')
                                            <li><a href="#"><i class="fa fa-angle-right"
                                                               aria-hidden="true"></i>
                                                    {{ $event->location }}
                                                </a>
                                            </li>
                                        @endif
                                        <li><a href="#"><i class="fa fa-angle-right"
                                                           aria-hidden="true"></i>
                                                {{ $event->street }} {{ $event->house_number }}
                                            </a></li>
                                        <li><a href="#"><i class="fa fa-angle-right"
                                                           aria-hidden="true"></i>
                                                {{$event->zip}} {{ $event->city }}
                                            </a>
                                        </li>
                                        @isset($event->price)
                                            <li><a href="#"><i class="fa fa-angle-right"
                                                               aria-hidden="true"></i>
                                                    Eintrittspreis {{$event->price}} €
                                                </a>
                                            </li>
                                        @endisset
                                    </ul>
                                    <span class="tagtxt">Weitere Informationen</span>
                                    <ul class="toptips-links">
                                        <li><i class="fa fa-angle-right"
                                               aria-hidden="true"></i>
                                            @isset($event->club_id)
                                                @php
                                                    $club = \Vereinsleben\Club::whereId($event->club_id)->firstOrFail();
                                                    $clubname = $club->name
                                                @endphp
                                                <a href="{{ route('club.detail', $club->slug) }}"
                                                   target="_blank">{{ $clubname }}</a>
                                            @endisset
                                        </li>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection