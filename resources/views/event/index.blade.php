@extends('layouts.master')
@section('title')
    vereinsleben.de
@endsection

@section('content')
    <section class="news__category-section">
        <div class="container">
            <div class="news__category-wrapper">
                <span class="news__category">EVENTKALENDER</span>
            </div>
            <h1 class="news__headline">ALLE VERANSTALTUNGEN IM ÜBERBLICK</h1>
            {!! Form::open(['route' => 'events.all', 'method' => 'GET', 'id' => 'eventslist', 'class' => '']) !!}
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="input-group">
                        {!! Form::select('federal_state', array('Bundesland wählen') + $federal_states->pluck('name', 'name')->all(), null, ['class' => 'select', 'id' => 'federal_state']) !!}
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="input-group">
                        {!! Form::text('ort_plz', isset($query['ort_plz']) ? $query['ort_plz'] : null, ['id' => 'ort_plz', 'autofocus', 'class' => 'input', 'placeholder' => 'Ort oder PLZ eingeben']) !!}
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="input-group">
                        {!! Form::select('distance', ['5' => '+5km', '10' => '+10km', '15' => '+15km', '25' => '+25km', '50' => '+50km'], '', ['class' => 'select', 'placeholder' => 'Umkreis', 'id' => 'distance']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="input-group">
                        {!! Form::text('searchtext', isset($query['searchtext']) ? $query['searchtext'] : null, ['id' => 'searchtext', 'autofocus', 'class' => 'input', 'placeholder' => 'Suchbegriff eingeben']) !!}
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="input-group">
                        {!! Form::text('beginn', isset($query['beginn']) ? date('Y-m-d h:i:s',strtotime($query['beginn'])) : null, ['id' => 'beginn', 'autofocus', 'class' => 'input', 'placeholder' => 'Datum (Beginn) eingeben', 'data-datetimepicker-enabled']) !!}
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="input-group">
                        {!! Form::text('ende', isset($query['ende']) ? date('Y-m-d h:i:s',strtotime($query['ende'])) : null, ['id' => 'ende', 'autofocus', 'class' => 'input', 'placeholder' => 'Datum (Ende) eingeben', 'data-datetimepicker-enabled']) !!}
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    {!! Form::submit('Auswahl anzeigen', array('class' => 'button button--light button--light-white button--center button--full-width')) !!}
                </div>
            </div>
            {!! Form::close() !!}
            <br>

            <div class="row toptips-boxes">
                @if ($events->count() > 0 )
                    <div id="event_result">
                    @foreach($events as $event)
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="row">
                                <div class="col-lg-12 toptips-thum">
                                    @if(isset($event->sport) && $event->sport != ''  && $event->sport != null)
                                        @php
                                            $spname = "";
                                            try{
                                                $sportname = \Vereinsleben\Sport::whereId($event->sport)->firstOrFail();
                                                if(isset($sportname->title))
                                                {
                                                    $spname = $sportname->title;
                                                }
                                            }catch(Exception $e){

                                            }
                                        @endphp
                                        <div class="toptips-tag">
                                            {{ $spname }}
                                        </div>
                                    @else
                                    @endif
                                    @if($event->images()->count())
                                        <div class="event-thumb"
                                             style="background:url('{{ asset($event->images[0]->picture->url('singleView')) }}') top center no-repeat;background-size: cover;">
                                            <div class="toptips-arrow-top"><img
                                                        src="{{asset('static_images/toptips-arrow-top.png')}}"></div>
                                        </div>
                                    @else
                                        <img src="{{ asset('static_images/toptips-thum1.jpg') }}">
                                        <div class="toptips-arrow-top"><img
                                                    src="{{asset('static_images/toptips-arrow-top.png')}}"></div>
                                    @endif
                                    {{--<div class="toptips-arrow-top">--}}
                                    {{--<img src="{{asset('static_images/toptips-arrow-top.png')}}" alt=""></div>--}}
                                    {{--@if($event->images()->count() == 1)--}}
                                    {{--@foreach($event->images as $image)--}}
                                    {{--<img src="{{ asset($image->picture->url('singleView')) }}" alt="">--}}
                                    {{--@endforeach--}}
                                    {{--@else--}}
                                    {{--<img src="{{ asset('static_images/toptips-thum1.jpg') }}" alt="">--}}
                                    {{--@endif--}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="toptips-txt-box">
                                        <div class="toptips-txt-box-inner">
                                            <a href="{{ route('event.detail', $event->id) }}">
                                                <h4>{{ str_limit($event->title, $limit = 50, $end = '...') }}</h4></a>
                                            {{--<span class="tagtxt">Meisterschaft im Baseball</span>--}}
                                            <ul class="toptips-links">
                                                <li><a href="#"><i class="fa fa-angle-right"
                                                                   aria-hidden="true"></i>
                                                        @php $date_start = \Carbon\Carbon::parse($event->schedule_begin) @endphp
                                                        @php $date_start = $date_start->format('d.m.Y') @endphp
                                                        @php $date_end = \Carbon\Carbon::parse($event->schedule_end) @endphp
                                                        @php $date_end = $date_end->format('d.m.Y') @endphp

                                                        {{ $date_start }} - {{ $date_end }}
                                                    </a>
                                                </li>
                                                <li><a href="#"><i class="fa fa-angle-right"
                                                                   aria-hidden="true"></i>
                                                        @php $time = \Carbon\Carbon::parse($event->schedule_begin) @endphp
                                                        @php $time = $time->format('H:i') @endphp
                                                        {{ $time }} Uhr
                                                    </a>
                                                </li>
                                                @if($event->location != null && $event->location != '')
                                                    <li><a href="#"><i class="fa fa-angle-right"
                                                                       aria-hidden="true"></i>
                                                            {{ $event->location }}
                                                        </a>
                                                    </li>
                                                @endif
                                                <li><a href="#"><i class="fa fa-angle-right"
                                                                   aria-hidden="true"></i>
                                                        {{ $event->street }} {{ $event->house_number }}
                                                    </a></li>
                                                <li><a href="#"><i class="fa fa-angle-right"
                                                                   aria-hidden="true"></i>
                                                        {{$event->zip}} {{ $event->city }}
                                                    </a>
                                                </li>
                                                @isset($event->price)
                                                    <li><a href="#"><i class="fa fa-angle-right"
                                                                       aria-hidden="true"></i>
                                                            Eintrittspreis {{$event->price}} €
                                                        </a>
                                                    </li>
                                                @endisset
                                            </ul>
                                            <div class="mehr2">
                                                <a href="{{ route('event.detail', $event->id) }}"
                                                   target="_blank">Mehr</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>

                    @if($events->total() > 16)
                        <div id="event_more_result" class="col-md-12">
                            <div class="btn btn-default">
                                <a href="javascript:ajaxLoad('{{ $events->nextPageUrl() }}&ajax=1', 'event')"
                                   class="registrieren-btn">
                                    Weitere Events laden
                                </a>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div id="event_loader" style="display:none;" data-loader=""
                                 class="loader__wrapper loader__wrapper--centered">
                                <div class="loader loader--loading"></div>
                                <div class="loader loader--loading"></div>
                            </div>
                        </div>
                    @endif

                @endif
            </div>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
        </div>
    </section>
@endsection