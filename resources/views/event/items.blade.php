<div class="col-xxs-12 col-xs-6 col-md-3">
    <div class="row">
        <div class="col-lg-12 toptips-thum">
            @if($event->sport != ''  && $event->sport != null)
                @php
                    $sportname = \Vereinsleben\Sport::whereId($event->sport)->pluck('name')
                @endphp
                <div class="toptips-tag">
                    {{ $sportname }}
                </div>
            @else
            @endif
            <div class="toptips-arrow-top">
                <img src="{{asset('static_images/toptips-arrow-top.png')}}" alt=""></div>
            @if($event->images()->count() == 1)
                @foreach($event->images as $image)
                    <img src="{{ asset($image->picture->url('singleView')) }}" alt="">
                @endforeach
            @else
                <img src="{{ asset('static_images/toptips-thum1.jpg') }}" alt="">
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="toptips-txt-box">
                <div class="toptips-txt-box-inner">
                    <a href="{{ route('event.detail', $event->id) }}">
                        <h4>{{ str_limit($event->title, $limit = 24, $end = '...') }}</h4></a>
                    <span class="tagtxt">Meisterschaft im Baseball</span>
                    <ul class="toptips-links">
                        <li><a href="#"><i class="fa fa-angle-right"
                                           aria-hidden="true"></i>
                                @php $date = \Carbon\Carbon::parse($event->schedule_begin) @endphp
                                @php $date = $date->format('d.m.Y') @endphp
                                {{ $date }}</a>
                        </li>
                        <li><a href="#"><i class="fa fa-angle-right"
                                           aria-hidden="true"></i>
                                @php $time = \Carbon\Carbon::parse($event->schedule_begin) @endphp
                                @php $time = $time->toTimeString() @endphp
                                {{ $time }}</a>
                        </li>
                        <li><a href="#">
                                <i class="fa fa-angle-right"
                                   aria-hidden="true"></i>
                                {{ $event->street }} {{ $event->house_number }}
                            </a>
                        </li>
                        <li><a href="#"><i class="fa fa-angle-right"
                                           aria-hidden="true"></i>
                                {{ $event->city }}
                            </a>
                        </li>
                        <li><a href="#"><i class="fa fa-angle-right"
                                           aria-hidden="true"></i>Info</a>
                        </li>
                    </ul>
                    <div class="mehr2">
                        <a href="#">Mehr</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if(isset($loadMore) && $news->count() >= 12)
    <div class="col-md-12">
        <div class="button__center-wrapper">
            <button class="button button--padded button--dark">Weitere News laden</button>
        </div>
    </div>
@endif