<div id="{{ $type }}_more_result" class="col-md-12">
    <div class="btn btn-default">
        <a href="javascript:ajaxLoad('{{ $more->nextPageUrl() }}', '{{ $type }}')"
           class="btn btn-default registrieren-btn">
            Weitere Events laden
        </a>
    </div>
</div>