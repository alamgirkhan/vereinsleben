<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="row">
        <div class="col-lg-12 toptips-thum">
            @if($event->sport != ''  && $event->sport != null)
                @php
                    $sportname = \Vereinsleben\Sport::whereId($event->sport)->firstOrFail();
                    $spname = $sportname->title
                @endphp
                <div class="toptips-tag">
                    {{ $spname }}
                </div>
            @else
            @endif
            <div class="toptips-arrow-top">
                <img src="{{asset('static_images/toptips-arrow-top.png')}}" alt=""></div>
            @if($event->images()->count() == 1)
                @foreach($event->images as $image)
                    <img src="{{ asset($image->picture->url('singleView')) }}" alt="">
                @endforeach
            @else
                <img src="{{ asset('static_images/toptips-thum1.jpg') }}" alt="">
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="toptips-txt-box">
                <div class="toptips-txt-box-inner">
                    <a href="{{ route('event.detail', $event->id) }}">
                        <h4>{{ str_limit($event->title, $limit = 50, $end = '...') }}</h4></a>
                    {{--<span class="tagtxt">Meisterschaft im Baseball</span>--}}
                    <ul class="toptips-links">
                        <li><a href="#"><i class="fa fa-angle-right"
                                           aria-hidden="true"></i>
                                @php $date_start = \Carbon\Carbon::parse($event->schedule_begin) @endphp
                                @php $date_start = $date_start->format('d.m.Y') @endphp
                                @php $date_end = \Carbon\Carbon::parse($event->schedule_end) @endphp
                                @php $date_end = $date_end->format('d.m.Y') @endphp

                                {{ $date_start }} - {{ $date_end }}
                            </a>
                        </li>
                        <li><a href="#"><i class="fa fa-angle-right"
                                           aria-hidden="true"></i>
                                @php $time = \Carbon\Carbon::parse($event->schedule_begin) @endphp
                                @php $time = $time->format('H:i') @endphp
                                {{ $time }} Uhr
                            </a>
                        </li>
                        @if($event->location != null && $event->location != '')
                            <li><a href="#"><i class="fa fa-angle-right"
                                               aria-hidden="true"></i>
                                    {{ $event->location }}
                                </a>
                            </li>
                        @endif
                        <li><a href="#"><i class="fa fa-angle-right"
                                           aria-hidden="true"></i>
                                {{ $event->street }} {{ $event->house_number }}
                            </a></li>
                        <li><a href="#"><i class="fa fa-angle-right"
                                           aria-hidden="true"></i>
                                {{$event->zip}} {{ $event->city }}
                            </a>
                        </li>
                        @isset($event->price)
                            <li><a href="#"><i class="fa fa-angle-right"
                                               aria-hidden="true"></i>
                                    Eintrittspreis {{$event->price}} €
                                </a>
                            </li>
                        @endisset
                    </ul>
                    <div class="mehr2">
                        <a href="{{ route('event.detail', $event->id) }}"
                           target="_blank">Mehr</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>