@extends('layouts.master')
@section('title', 'vereinsleben.de - wir. leben. sport.')
@section('meta_description', 'Finde den passenden Sportverein und nutze die Bewertungsfunktion, findet neue Mitglieder und informiert über eurer Sportverein mit Neuigkeiten aus dem Breitensport.')
@section('meta_keywords', 'Sportvereine, bewerten, suchen, Mitglieder, finden, Breitensport')

@section('content')
    @if ($sliderImages && $sliderImages->count())
        @include('components.startpage-slider', [
        'slider' => $sliderImages
        ])
    @endif

    @include('components.highlight-ribbon', ['numbers' => $numbers])
    

    @if ($regionalNews && $regionalNews->count() >= 4)
        @include('news.partials.regional-teaser', [
        'news' => $regionalNews,
        ])
    @endif

    @if ($latestNews && $latestNews->count() >= 4)
        @include('news.partials.full-teaser', [
        'news' => $latestNews,
        'featuredNews' => $featuredNews
        ])
    @endif

    @include('components.feature-section',[
        'upcomingEvents'=>$upcomingEvents,
    ])

    @include('components.partner-section')

@endsection