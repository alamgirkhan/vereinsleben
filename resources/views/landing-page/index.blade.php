<?php
if (isset($data)) {
	$sport = $data['sport'];
	$city = $data['city'];
	$clubs = $data['clubs'];
	$totalCount = $data['totalCount'];
	$perPageList = $data['perPageList'];
	$currentPerPage = $data['currentPerPage'];
	$keyword = $data['keyword'];

	$city = strtoupper(substr($city,0,1)) .  substr($city, 1);

	if ($totalCount == 0)
	  {
	    $anzahlString = "aktuell noch keine";
	    $sportString = $sport."e";
      }
	if ($totalCount == 1)
	  {
	    $anzahlString = "einen";
	    $sportString = $sport."e";
      }
	if ($totalCount > 1)
	  {
	    $anzahlString = $totalCount;
	    $sportString = $sport."e";
      }


}
?>
@extends('layouts.master')
@section('title')
	<?php
	if (isset($data)) {
		echo $sportString . ' in ' . $city . ' | vereinsleben.de';
	}
	?>
@endsection

@section('meta_description')<?php echo 'Finde deinen ' . $sport . ' in ' . $city . ' und andere Mitglieder auf vereinsleben.de. Alle Infos & News rund um die Vereine. Jetzt entdecken!'?>@endsection

@section('content')
  <section class="content-detail" style="padding-top: 50px;">
    <div class="container">
			<?php
			if (isset($data)) { ?>
      <div class="row">

        <div class="col-lg-9 col-md-8 col-sm-7 col-xs-12 news">
          <h1 class="news__headline"><?php echo $sport ?> in <?php echo $city ?></h1>
          <p>In <?php echo $city ?> findest Du <?php echo $anzahlString ?> <?php echo $sportString ?>. Schau einfach unten in
            den
            Suchergebnissen und finde ganz einfach deinen neuen Verein.</p>
          <p>Egal ob Fußball-, Basketball oder einen anderen Sportverein. Bei Vereinsleben.de findest du eine Vielzahl
            von Vereinen und mit
            Sicherheit auch den richtigen für Dich. Solltest Du deinen Verein nicht finden dann melde ihn gerne bei uns
            an. Außerdem gibt es bei uns die aktuellsten
            News rund um die verschiedenen Vereine. Entdecke die verschiedenen Vereine in <?php echo $city ?> und
            werde jetzt Mitglied oder Fan!</p>
          <h2><?php echo $anzahlString ?> <?php echo $sportString ?> in <?php echo $city ?></h2>


          {{--list result--}}
          @if($totalCount > 0)
            <div id="club_result">
              <div id="filter">
                <div class="row">
                  <form>
                    <div class="col-md-2">
                      <select class="select" name="" placeholder="Field 1">
                        <option value="">Field 1</option>
                        <option value="">1</option>
                        <option value="">2</option>
                      </select>
                    </div>
                    <div class="col-md-2">
                      <select class="select" name="" placeholder="Field 2">
                        <option value="">Field 2</option>
                        <option value="">1</option>
                        <option value="">2</option>
                      </select>
                    </div>
                    <div class="col-md-2">
                      <select class="select" name="" placeholder="Field 3">
                        <option value="">Field 3</option>
                        <option value="">1</option>
                        <option value="">2</option>
                      </select>
                    </div>
                    <div class="col-md-3">
                      <input class="input" name="keyword" value="{{$keyword ? $keyword : ''}}" id="keyword" type="text" placeholder="Suchbegriff eingeben">
                    </div>
                    <div class="col-md-3">
                      <input class="button button--light button--light-white button--center button--full-width" type="submit" value="Suchen">
                    </div>
                  </form>
                </div>
              </div>
              <div class="row">
                @foreach( $clubs as $key => $club )
                  <div class="col-xs-12 col-sm-6 col-md-6 fan-box-main">
                    <div class="fan-box">
                      <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 club-result-thum">
                          <div class="thum-bg">
                            <a href="{{ route('club.detail', ['slug' => $club->slug]) }}">
                              <img src="{{ asset($club->avatar->url('startPage')) }}"/>
                            </a>
                          </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 fan-box-detail-lead">
                          <div class="fan-box-detail">
                            <h3><a href="{{ route('club.detail', $club->slug) }}" class="member-card__link">
                                {{ str_limit($club->name, $limit = 25, $end = '...') }}
                              </a>
                            </h3>
                          </div>
                          <div class="fan-lctn"><i class="fa fa-map-marker" aria-hidden="true"></i> {{ $club->city }}
                          </div>
                          <div class="fan-likes">{{ str_limit($club->allSportsString(), $limit = 25, $end = '...') }}</div>
                        </div>
                      </div>
                    </div>
                  </div>
                  @if($key % 2 !== 0)
                    <div class="clear"></div>
                  @endif
                @endforeach
              </div>
              <nav id="footer-list-result">
                <ul class="pagination">
                  {{ $clubs->render() }}
                </ul>
                <div id="doprdown-per-page">
                  <form action="">
                    <select name="per-page" id="per-page" class="select" onchange="this.form.submit()">
                      @foreach($perPageList as $perPage)
                        <option {{$currentPerPage === $perPage ? 'selected' : ''}} value="{{$perPage}}">{{$perPage}}</option>
                      @endforeach
                    </select>
                  </form>
                </div>
              </nav>
            </div>
          @endif
          <h2>Mitglied in einem <?php echo $sport ?> in <?php echo $city ?> werden</h2>
          <p>Sportvereine erfreuen sich einer großen Beliebtheit. Jede Art von Sport ist gesund, und das gesellige
            Vereinsleben gehört für kontaktfreudige Menschen
            zu den beliebtesten Freizeitbeschäftigungen. Möchtest du Mitglied in einem Sportverein werden, ist dies
            jederzeit möglich. Du wendest dich an ein
            Vereinsmitglied oder, bei kleineren Vereinen, direkt an den Vorstand. Dort bekommst du ein
            Beitrittsformular, das du ausfüllen musst. Persönliche
            Angaben sind erforderlich, sie werden aber vertraulich behandelt.</p>
          <p>Die meisten Vereine erheben einen Mitgliedsbeitrag. Du entrichtest ihn erstmalig, wenn du dich als Mitglied
            angemeldet hast.
            Die Satzungen vieler Vereine legen fest, dass die Mitgliedschaft erst dann zustande kommt, wenn der
            Erstbetrag in die Vereinskasse
            eingezahlt wurde. Du kannst den Betrag überweisen, bar in die Kasse einzahlen, oder du entscheidest dich für
            eine Einzugsermächtigung.
            Viele <?php echo $sport ?> ziehen das Geld zweimal jährlich ein. Eine monatliche oder vierteljährliche
            Zahlungsweise wird jedoch ebenso angeboten.
            Dies betrifft vor allem Vereine, deren Mitgliedschaft etwas teurer ist.</p>
          <h2>Fan oder Unterstützer eines Sportvereins werden</h2>
          <p>Möchtest du keinen aktiven Sport betreiben, kannst du deinen <?php echo $sport ?> dennoch unterstützten.
            Mit Spenden oder Taten nimmst du aktiv am Vereinsleben bei.
            Als Unterstützer hast du die Möglichkeit, organisatorische Aufgaben zu übernehmen oder eine Position
            innerhalb des Vereins zu bekleiden.
            Spenden sind freiwillig, doch vor allem kleine lokale Vereine sind darauf angewiesen.</p>
          <h2>Deine Vorteile bei vereinsleben.de</h2>
          <p>Auf vereinsleben.de haben sich verschiedene Vereine aus dem Bundesland Rheinland-Pfalz zusammengeschlossen.
            Du kannst dich kostenlos registrieren und
            dich jederzeit aktiv über das Vereinssportleben in Rheinland-Pfalz registrieren. Auch Vereine profitieren
            von der Registrierung. Sie machen ihre Arbeit
            auf der Plattform bekannt und können schneller wachsen. Dies gilt vor allem für kleinere Vereine, die sich
            erst einen Stamm an Mitgliedern aufbauen möchten.
            Als Verein werdet ihr schneller gefunden und könnt über Events und Neuigkeiten einfach und unkompliziert
            informieren.</p>
          <p>Finde jetzt deinen <?php echo $sport ?> in <?php echo $city ?> auf vereinsleben.de!</p>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <div class="row">
            <div class="col-lg-12">
              <div class="rhs-box">
                <h2>Artikel Teilen</h2>
                <ul class="social-icons">
                  <li>
                    <a
                      href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.vereinsleben.de%2Fneuigkeiten%2F7%2F2018-12-13-olympiasiegerin-im-dschungelcamp-2019"
                      target="_blank"><i class="fa fa-facebook"></i></a>
                  </li>
                  <li>
                    <a
                      href="https://twitter.com/intent/tweet?url=https%3A%2F%2Fwww.vereinsleben.de%2Fneuigkeiten%2F7%2F2018-12-13-olympiasiegerin-im-dschungelcamp-2019"
                      target="_blank"><i class="fa fa-twitter"></i></a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12">
              <div class="rhs-box">
                <h2>Mehr zum Thema</h2>
              </div>
            </div>
          </div>
        </div>
      </div>
			<?php } else { ?>
      <h1>404</h1>
			<?php }
			?>
    </div>
  </section>
@endsection
