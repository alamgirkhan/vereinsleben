@extends('layouts.master')

@section('content')
  <section class="content-detail">
    <div class="container">
	    <?php
	    if (!$keywords->isEmpty()) {
		    ?>
        <div class="row">
          <div class="col-lg-7 col-md-8 col-sm-7 col-xs-12">
            <table class="table">
              <thead>
              <tr>
                <th>Schlüsselwort 1</th>
                <th>Schlüsselwort 2</th>
              </tr>
              </thead>
              <tbody>
					    <?php
					    foreach ($keywords as $keyword ) {
					    $keyword1 = explode(" : ", $keyword->description, 2)[0];
					    $keyword2 = explode(" : ", $keyword->description, 2)[1];
					    ?>
              <tr class="keyword-item" data-sport="<?php echo $keyword->sport ?>" data-location="<?php echo $keyword->location ?>">
                <td><?php echo $keyword1 ?></td>
                <td><?php echo $keyword2 ?></td>
              </tr>
					    <?php
					    }
					    ?>
              </tbody>
            </table>
            <div class="paginate">
              {{ $keywords->links() }}
            </div>
          </div>
          <div class="col-lg-5 col-md-8 col-sm-7 col-xs-12">
            <table class="table">
              <thead>
              <tr>
                <th>URL</th>
              </tr>
              </thead>
              <tbody>
              <tr>
                <td id="keyword-url"></td>
              </tr>
              </tbody>
            </table>
          </div>
        </div>
      <?php
	    } else {
	    	echo '<h2>No Data</h2>';
        }

	    ?>
    </div>
  </section>
  <style>
    tr.keyword-item {
      cursor: pointer;
    }
    tr.keyword-item:hover {
      color: #29c7ab;
    }
    .paginate {
      text-align: center;
    }
    .content-detail {
      padding: 50px 0;
    }
    #keyword-url {
      word-break: break-word;
    }
  </style>
@endsection