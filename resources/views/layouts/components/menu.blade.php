<header class="navi-wrapper">
    @if(isset($federalState) && $federalState->name == 'Rheinland-Pfalz')
        <section class="sponsor-ribbon__wrapper">
            <div class="sponsor-ribbon">
                <div class="container">
                    <div class="sponsor-ribbon__powered-wrapper">
                        <a class="sponsor-ribbon__link" href="{{ url('partner/lotto') }}">
                            <div class="sponsor-ribbon__powered-label">Mit freundlicher Unterstützung von</div>
                            <span class="sponsor-logo"><img src="{{asset('static_images/powered-lotto-rlp.png')}}"
                                                            alt="powered by lotto"></span>
                        </a>
                    </div>
                </div>
            </div>
        </section>
    @endif

    <nav class="navi">
        <a class="navi__logo" href="{{ route('home') }}"> <img class="menu__logo"
                                                               src="{{asset('static_images/logo-vereinsleben.png')}}"
                                                               alt="">
            <span class="logo-caption">
                @if(Auth::check())
                    {{ Auth::user()->desiredState() &&  Auth::user()->desiredState()->name ? Auth::user()->desiredState()->name : '' }}
                @else {{ isset($sessionState) ? $sessionState->name : '' }}
                @endif
            </span>
        </a>

        <div class="roundimg-top"></div>
        <div class="navi__mobile-button" id="mobile-menu"><i class="mobile-button__icon fa fa-bars"
                                                             aria-hidden="true"></i></div>
        @if(isset($federalState) && $federalState->name == 'Rheinland-Pfalz')
            <div class="lotto-mobile"><a href="{{ url('partner/lotto') }}"><img
                            src="{{asset('static_images/lotto-logo.png')}}" alt=""/></a></div>
        @endif
        <div id="dl-menu" class="dl-menuwrapper">
            <button class="dl-trigger">Open Menu</button>
            <ul class="dl-menu">
                <li class="mobile-menu-search">
                    {!! Form::open(['route' => 'club.search', 'method' => 'GET', 'id' => 'search1', 'class' => '']) !!}
                    <div class="input-group">
                        <span class="input-group-addon"><img src="{{asset('static_images/menu-search.png')}}"></span>
                        {!! Form::text('keyword1', isset($query['keyword']) ? $query['keyword'] : null, ['id' => 'keyword1', 'class' => 'form-control', 'placeholder' => 'STICHWORT']) !!}
                        <button type="submit" class="btn btn-default search-suchen">Suchen</button>
                    </div>
                    {!! Form::close() !!}
                </li>
                <li class="dropdown state">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Bundesland wechseln<b class="caret"></b></a>
                    <ul class="dropdown-menu" style="display: none;">
                        @if(isset($federal_states))
                            @foreach($federal_states as $state)
                                <li>
                                    <a href="{{ route('change.federal_state', $state->id)}}">{{ $state->name }}</a>
                                </li>
                            @endforeach
                        @endif
                    </ul>
                </li>
                <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i>@if(Auth::check())MEIN VL @else Anmelden @endif</a>
                        <ul class="dl-submenu">
                        @if(Auth::check())

                            <li>
                                <a class="-link{{ Request::is('user/profile/edit') ? ' -link--active' : '' }}"
                                   href="{{ route('user.detail', Auth::user()->username) }}#streaming"><i
                                            class="fa fa-user"></i> Newsfeed</a>
                            </li>
                            {{--@if((Auth::user()->friends()->count() > 0 || Auth::user()->clubsWhereAdmin()->count() > 0 || Auth::user()->isAdmin()) ||--}}
                            {{--(Auth::user()->isAdmin() && Auth::user()->getFriendsCount() > 0))--}}
                            @if(Auth::user()->clubsWhereAdmin()->count() > 0)
                                <li>
                                    <a class="-link{{ Request::is('chat') ? ' -link--active' : '' }}"
                                       href="{{ url('chat/index') }}"><i class="fa fa-comments-o"
                                                                         aria-hidden="true"></i>
                                        Messenger</a>

                                </li>
                            @else

                                @php
                                    $friend = Auth::user()->getFriends()->first();

                                    if(is_null($friend)){

                                        $friendname = null;
                                    }
                                    else{
                                        $friendname = $friend->username;
                                    }
                                @endphp
                                @isset($friendname)
                                    <li>
                                        <a class="-link{{ Request::is('chat/') ? ' -link--active' : '' }}"
                                           href="{{ route('chat', $friendname) }}"><i class="fa fa-comments-o"
                                                                                      aria-hidden="true"></i>
                                            Messenger</a>

                                    </li>
                                @endisset
                            @endif
                            <li>
                                <a class="-link{{ Request::is('benutzer/'.Auth::user()->username) ? ' -link--active' : '' }}"
                                   href="{{ route('user.detail', Auth::user()->username) }}"><i
                                            class="fa fa-user"></i> Mein Profil</a>
                            </li>
                            @if(Auth::user()->isAdministratorOfClub())
                                <li>
                                    <a class="-link{{ Request::is('user/dashboard') ? ' -link--active' : '' }}"
                                       href="{{ route('user.dashboard') }}"><i
                                                class="fa fa-shield"></i> Vereinsverwaltung</a>
                                </li>
                            @endif
                            @if(Auth::check() && Auth::user()->isAdmin())
                                <li class="{{ Request::is('admin/*') ? ' -link--active' : '' }}">
                                    <a class="-link" href="{{ route('admin.index') }}"><i
                                                class="fa fa-unlock"></i> Administration</a>
                                </li>
                            @endif
                            <li>
                                <a class="-link{{ Request::is('user/profile/edit') ? ' -link--active' : '' }}"
                                   href="{{ route('user.detail', Auth::user()->username) }}#informationen"><i
                                            class="fa fa-gears"></i> Persönliche Daten</a>
                            </li>
                            <li>
                                <a class="-link{{ Request::is('user/profile/edit') ? ' -link--active' : '' }}"
                                   href="{{ route('user.detail', Auth::user()->username) }}#passwort"><i
                                            class="fa fa-cog"></i> Einstellungen</a>
                            </li>
                            <li><a class="-link" href="{{ url('/logout')}}"><i
                                            class="fa fa-sign-out"></i> Abmelden</a>
                    </li>
                @else
                            <li><a href="{{ route('login') }}">Anmelden</a></li>
                            <li><a href="{{ url('/login/facebook') }}">Mit Facebook anmelden</a></li>
                            <li><a href="{{ url('/register') }}">Registrieren</a></li>
                        @endif
                        </ul>
                    </li>
                <li>
                    <a href="{{ url('neuigkeiten') }}"><i class="fa fa-newspaper-o" aria-hidden="true"></i>NEWS</a>
                    {{--<ul class="dl-submenu">--}}
                    {{--<h2>ALLGEMEINE NEWS</h2>--}}
                    {{--<li><a href="{{ url('neuigkeiten/news') }}">ALLE News</a></li>--}}
                    {{--<li><a href="{{ url('/neuigkeiten/fuer-zwischendurch') }}">Für Zwischendurch </a></li>--}}
                    {{--<li><a href="{{ url('neuigkeiten/meine-Meinung') }}">Unsere Meinung</a></li>--}}
                    {{--<li><a href="{{ url('neuigkeiten/so-geschehen') }}">Sportgeschichte</a></li>--}}
                    {{--<li><a href="{{ url('neuigkeiten/wusstet-ihr-schon') }}">Wissenswertes</a></li>--}}
                    {{--<li><a href="{{ url('neuigkeiten/ausblick') }}">Ausblick / Event-Tipps</a></li>--}}
                    {{--<h2>Vereins-News</h2>--}}
                    {{--<li><a href="{{ url('/neuigkeiten/eure-vereine') }}">Eure Vereine</a></li>--}}
                    {{--<li><a href="{{ url('/neuigkeiten/events') }}">Vereins-Events</a></li>--}}
                    {{--<li><a href="{{ url('/neuigkeiten/vereinsleben-de') }}">vereinsleben.de</a></li>--}}
                    {{--<h2>Verbands-news</h2>--}}
                    {{--<li><a href="{{ url('/neuigkeiten/landessportbund-rheinland-pfalz') }}">Landessportbund Rheinland-Pfalz</a></li>--}}
                    {{--<li><a href="{{ url('/neuigkeiten/sportbund-pfalz') }}">Sportbund Pfalz</a></li>--}}
                    {{--<li><a href="{{ url('/neuigkeiten/sportbund-rheinhessen') }}">Sportbund Rheinhessen</a></li>--}}
                    {{--<li><a href="{{ url('/neuigkeiten/sportbund-rheinland') }}">Sportbund Rheinland</a></li>--}}
                    {{--<li><a href="{{ url('/neuigkeiten/sportjugend-rheinland-pfalz') }}">Sportjugend--}}
                    {{--Rheinland-Pfalz</a></li>--}}
                    {{--</ul>--}}

                </li>
                <li>
                    <a href="#"><i class="fa fa-diamond" aria-hidden="true"></i>VORTEILE</a>
                    <ul class="dl-submenu">
                        <h2>Verein des Monats</h2>
                        <li><a href="{{ url('verein-des-monats') }}">Aktuelle Aktion</a></li>
                        <li><a href="{{ url('verein-des-monats/ueber-den-wettbewerb') }}">Über den Wettbewerb</a></li>
                        <li><a href="{{ url('verein-des-monats/teilnehmen') }}">Jetzt bewerben</a></li>
                        <li>
                            <a href="{{ url('neuigkeiten/7/2018-02-19-expertentipps-wie-wird-man-verein-des-monats') }}">Tipps
                                für Sieger</a></li>
                        <li><a href="{{ url('verein-des-monats/teilnahmebedingungen') }}">Teilnahmebedingungen</a></li>

                        {{--<h2>Teambesprechungsraum</h2>--}}
                        {{--<li>--}}
                            {{--<a href="{{ url('teambesprechungsraum') }}">Aktionsseite</a>--}}
                            {{--<a href="{{ url('trainerhero') }}">Mein Trainer - mein Held</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="{{ url('content/teilnahmebedingungen-teambesprechungsraum') }}">Teilnahmebedingungen</a>--}}
                        {{--</li>--}}

                        <h2>SERVICE</h2>
                        <li><a href="{{ url('/best-western-rewards') }}">Best Western Rewards</a></li>
                        <li>
                            <a href="{{ url('neuigkeiten/?searchtext=Sport+mit+Verantwortung&federal_state=0&news_type=') }}">Sport
                                mit Verantwortung</a></li>
                        <li><a href="{{ url('service/stellenboerse') }}">Trainerbörse</a></li>
                    </ul>
                </li>
                <li><a href="/podcast" class="noarrow">
                        <span class="podcast-icon"></span>PODCAST</a>
                </li>
                <li><a href="{{ url('/partner') }}" class="noarrow"><span class="partner-icon"></span>PARTNER</a></li>
            </ul>
        </div>

        <div class="menu-wrapper menu-wrapper--hover" id="menu-wrapper">
            <div class="col-lg-9 col-md-9 col-sm-9">
                <ul class="menu__list @if(isset($federalState) && $federalState->name != 'Rheinland-Pfalz') menu__list__top @endif" id="menu">
                    <li class="menu__item">
                        <a href="{{ url('neuigkeiten') }}" class="menu__item-link menu__item-link--shop">
                            <i class="fa fa-newspaper-o" aria-hidden="true"></i>News
                        </a>
                        {{--<a href="{{ url('neuigkeiten') }}" data-submenu-trigger="true" class="menu__item-link menu__item-link--dropdown"><i--}}
                        {{--class="fa fa-newspaper-o" aria-hidden="true"></i>News</a>--}}
                        {{--<div class="dropdown_4columns">--}}
                        {{--<span class="downarrow"></span>--}}
                        {{--<div class="col_1">--}}
                        {{--<h3>Allgemeine News</h3>--}}
                        {{--<ul>--}}
                        {{--<li><a href="{{ url('/neuigkeiten/fuer-zwischendurch') }}">Für Zwischendurch </a></li>--}}
                        {{--<li><a href="{{ url('neuigkeiten/meine-Meinung') }}">Unsere Meinung</a></li>--}}
                        {{--<li><a href="{{ url('neuigkeiten/so-geschehen') }}">Sportgeschichte</a></li>--}}
                        {{--<li><a href="{{ url('neuigkeiten/wusstet-ihr-schon') }}">Wissenswertes</a></li>--}}
                        {{--<li><a href="{{ url('neuigkeiten/ausblick') }}">Ausblick / Event-Tipps</a></li>--}}
                        {{--</ul>--}}
                        {{--</div>--}}

                        {{--<div class="col_1">--}}
                        {{--<h3>Vereins-News</h3>--}}
                        {{--<ul>--}}
                        {{--<li><a href="{{ url('/neuigkeiten/eure-vereine') }}">Eure Vereine</a></li>--}}
                        {{--<li><a href="{{ url('/neuigkeiten/events') }}">Vereins-Events</a></li>--}}
                        {{--<li><a href="{{ url('/neuigkeiten/vereinsleben-de') }}">vereinsleben.de</a></li>--}}
                        {{--<li><a href="{{ url('neuigkeiten/news') }}">ALLE News</a></li>--}}
                        {{--</ul>--}}
                        {{--</div>--}}
                        {{--<div class="col_1 no-border">--}}
                        {{--<h3>Verbands-news</h3>--}}
                        {{--<ul>--}}
                        {{--<li><a href="{{ url('/neuigkeiten/landessportbund-rheinland-pfalz') }}">Landessportbund Rheinland-Pfalz</a></li>--}}
                        {{--<li><a href="{{ url('/neuigkeiten/sportbund-pfalz') }}">Sportbund Pfalz</a></li>--}}
                        {{--<li><a href="{{ url('/neuigkeiten/sportbund-rheinhessen') }}">Sportbund--}}
                        {{--Rheinhessen</a></li>--}}
                        {{--<li><a href="{{ url('/neuigkeiten/sportbund-rheinland') }}">Sportbund Rheinland</a>--}}
                        {{--</li>--}}
                        {{--<li><a href="{{ url('/neuigkeiten/sportjugend-rheinland-pfalz') }}">Sportjugend--}}
                        {{--Rheinland-Pfalz</a></li>--}}
                        {{--</ul>--}}
                        {{--</div>--}}
                        {{--</div>--}}

                    </li>
                    <li class="menu__item mainsecondmenu">
                        <a href="#" data-submenu-trigger="true" class="menu__item-link menu__item-link--dropdown"><i
                                    class="fa fa-diamond" aria-hidden="true"></i>Vorteile</a>

                        {{--@if($federalState->name == 'Rheinland-Pfalz' or $federalState->name == 'Saarland')--}}
                        <div class="dropdown_customcolumns">
                            {{--@else--}}
                            {{--<div class="dropdown_2columns">--}}
                            {{--@endif--}}
                            <span class="downarrow"></span>

                            {{--@if($federalState->name == 'Rheinland-Pfalz' or $federalState->name == 'Saarland')--}}

                            <div class="col_2">
                                <h3>Verein des Monats</h3>
                                <ul>
                                    <li><a href="{{ url('verein-des-monats') }}">Aktuelle Aktion</a></li>
                                    <li><a href="{{ url('verein-des-monats/ueber-den-wettbewerb') }}">Über den
                                            Wettbewerb</a></li>
                                    <li><a href="{{ url('verein-des-monats/teilnehmen') }}">Jetzt bewerben</a></li>
                                    <li>
                                        <a href="{{ url('neuigkeiten/7/2018-02-19-expertentipps-wie-wird-man-verein-des-monats') }}">Tipps
                                            für Sieger</a></li>
                                    <li><a href="{{ url('verein-des-monats/teilnahmebedingungen') }}">Teilnahmebedingungen</a>
                                    </li>
                                </ul>
                            </div>
                            {{--@endif--}}

                            {{--<div class="@if($federalState->name == 'Rheinland-Pfalz' or $federalState->name == 'Saarland') col_2 @endif no-border">--}}
                            <div class="col_2 no-border">
                                {{--<h3>Teambesprechungsraum</h3>--}}
                                {{--<ul>--}}
                                    {{--<li>--}}
                                        {{--<a href="{{ url('teambesprechungsraum') }}">Aktionsseite</a>--}}
                                    {{--</li>--}}
                                        {{--<a href="{{ url('trainerhero') }}">Mein Trainer - mein Held</a>--}}
                                    {{--<li><a href="{{ url('content/teilnahmebedingungen-teambesprechungsraum') }}">Teilnahmebedingungen</a>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                                <h3>SERVICE</h3>
                                <ul>
                                    <li><a href="{{ url('/best-western-rewards') }}">Best Western Rewards</a></li>
                                    <li>
                                        <a href="{{ url('neuigkeiten/?searchtext=Sport+mit+Verantwortung&federal_state=0&news_type=') }}">Sport
                                            mit Verantwortung</a></li>
                                    <li><a href="{{ url('service/stellenboerse') }}">Trainerbörse</a></li>
                                </ul>
                            </div>
                        </div>

                    </li>
                    <li class="menu__item"><a class="menu__item-link menu__item-link--shop"
                                              href="{{ url('/podcast') }}" >
                            <span class="podcast-icon"></span>Podcast</a></li>
                    <li class="menu__item"><a class="menu__item-link menu__item-link--partner"
                                              href="{{ url('partner') }}"><span
                                    class="partner-icon"></span>Partner</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 second-menu">
                <div class="row second-menu-links">
                    <div class="col-lg-6 col-sm-6 search">
                        <a href="{{ url('suche/vereine') }}"><img src="{{asset('static_images/top-search.png')}}"
                                                                  align="absmiddle"/>Suche</a>
                    </div>
                    <div class="col-lg-6 col-sm-6 exit">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown registration-box">
                                <a href="@if(Auth::check()) {{ route('user.detail', Auth::user()->username) }} @else {{ url('login') }} @endif"
                                   class="dropdown-toggle" data-toggle="dropdown"><img
                                            src="{{asset('static_images/exit.png')}}" alt=""
                                            align="absmiddle"/>@if(Auth::check()) Mein VL @else Anmelden @endif</a>
                                <div class="dropdown-menu rgst-box">
                                    <div class="rgst-arrow"><img src="{{asset('static_images/rgst-arrow.png')}}"/></div>
                                    @if(Auth::check())
                                        <div class="grad-box">
                                            <div class="rgst-box-inner">
                                                <ul>
                                                    <li>
                                                        <a class="-link{{ Request::is('user/profile/edit') ? ' -link--active' : '' }}"
                                                           href="{{ route('user.profile.stream') }}"><i
                                                                    class="fa fa-user"></i> Newsfeed</a>
                                                    </li>
                                                    @if(Auth::user()->clubsWhereAdmin()->count() > 0)
                                                        <li>
                                                            <a class="-link{{ Request::is('chat') ? ' -link--active' : '' }}"
                                                               href="{{ url('chat/index') }}"><i
                                                                        class="fa fa-comments-o"
                                                                        aria-hidden="true"></i>
                                                                Messenger</a>

                                                        </li>
                                                    @else

                                                        @php
                                                            $friend = Auth::user()->getFriends()->first();

                                                            if(is_null($friend)){

                                                                $friendname = null;
                                                            }
                                                            else{
                                                                $friendname = $friend->username;
                                                            }
                                                        @endphp
                                                        @isset($friendname)
                                                            <li>
                                                                <a class="-link{{ Request::is('chat/') ? ' -link--active' : '' }}"
                                                                   href="{{ route('chat', $friendname) }}"><i
                                                                            class="fa fa-comments-o"
                                                                            aria-hidden="true"></i>
                                                                    Messenger</a>

                                                            </li>
                                                        @endisset
                                                    @endif
                                                    <li>
                                                        <a class="-link{{ Request::is('benutzer/'.Auth::user()->username) ? ' -link--active' : '' }}"
                                                           href="{{ route('user.detail', Auth::user()->username) }}"><i
                                                                    class="fa fa-user"></i> Mein Profil</a>
                                                    </li>
                                                    @if(Auth::check() && Auth::user()->isAdministratorOfClub())
                                                        <li>
                                                            <a class="-link{{ Request::is('user/dashboard') ? ' -link--active' : '' }}"
                                                               href="{{ route('user.dashboard') }}"><i
                                                                        class="fa fa-shield"></i> Vereinsverwaltung</a>
                                                        </li>
                                                    @endif
                                                    @if(Auth::check() && Auth::user()->isAdmin())
                                                        <li class="{{ Request::is('admin/*') ? ' -link--active' : '' }}">
                                                            <a class="-link" href="{{ route('admin.index') }}"><i
                                                                        class="fa fa-unlock"></i> Administration</a>
                                                        </li>
                                                    @endif
                                                    <li>
                                                        <a class="-link{{ Request::is('benutzer/'.Auth::user()->username) ? ' -link--active' : '' }}"
                                                           href="{{ route('user.detail', Auth::user()->username) }}#informationen"><i
                                                                    class="fa fa-gears"></i> Persönliche Daten</a>
                                                    </li>
                                                    <li>
                                                        <a class="-link{{ Request::is('user/profile/edit') ? ' -link--active' : '' }}"
                                                           href="{{ route('user.detail', Auth::user()->username) }}#passwort"><i
                                                                    class="fa fa-cog"></i> Einstellungen</a>
                                                    </li>

                                                    <li><a class="-link" href="{{ url('/logout')}}"><i
                                                                    class="fa fa-sign-out"></i> Abmelden</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    @else
                                        <div class="grad-box">
                                            <form action="{{ url('/login') }}" method="post">
                                                {{csrf_field()}}
                                                <div class="rgst-box-inner">
                                                    <h2>Melde dich hier An </h2>
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="input-group">
                                                            <span class="input-group-addon"><img
                                                                        src="{{asset('static_images/email.png')}}"
                                                                        alt=""></span>
                                                                <input type="text" name="email" id="email"
                                                                       value="{{ old('email') }}" class="form-control"
                                                                       placeholder="E-Mail-Adresse">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="input-group">
                                                            <span class="input-group-addon"><img
                                                                        src="{{asset('static_images/password.png')}}"
                                                                        alt=""></span>
                                                                <input type="password" name="password"
                                                                       class="form-control"
                                                                       placeholder="Passwort">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <button type="submit"
                                                            class="button button--center button--light button--light-white button--full-width button--icon">
                                                        <span class="fa fa-sign-in"></span> Anmelden
                                                    </button>
                                                    <p class="noch button button--center"><a

                                                                href="  {{url('/login/facebook') }}">Mit Facebook

                                                            anmelden</a></p>
                                                    <p class="noch">Noch kein Konto?
                                                    </p>
                                                    <p class="jetzt">Jetzt persönliches</p>
                                                    <p class="anmelden profil-anlegen"><a href="{{ url('/register') }}">Profil
                                                            anlegen</a></p>
                                                </div>
                                            </form>
                                        </div>
                                    @endif
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row top-drop-down">
                    <div class="col-lg-12">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown state">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Bundesland wechseln<b
                                            class="caret"></b></a>
                                <ul class="dropdown-menu" style="display: none;">
                                    @if(isset($federal_states))
                                        @foreach($federal_states as $state)
                                            <li>
                                                <a href="{{ route('change.federal_state', $state->id)}}">{{ $state->name }}</a>
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-sm-6 second-menu-afterscroll @if(isset($federalState) && $federalState->name == 'Rheinland-Pfalz') with-strip @endif">
            <div class="row second-menu-links">
                <div class="col-lg-4 col-md-6 col-sm-4 afterscroll-dropdown rlp">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown state">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                Bundesland
                                <b
                                        class="caret"></b></a>
                            <ul class="dropdown-menu" style="display: none;">
                                @if(isset($federal_states))
                                    @foreach($federal_states as $state)
                                        <li>
                                            <a href="{{ route('change.federal_state', $state->id)}}">{{ $state->name }}</a>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-3 col-sm-4 suche"><a href="{{ url('suche/vereine') }}">Suche</a></div>
                <div class="col-lg-4 col-md-3 col-sm-4 profil dropdown">
                    @if(Auth::check())
                        <a href="{{ route('user.detail', Auth::user()->username) }}" class="dropdown-toggle"
                           data-toggle="dropdown">Mein VL</a>
                        <div class="dropdown-menu rgst-box">
                            <div class="rgst-arrow"><img src="{{asset('static_images/rgst-arrow.png')}}"/></div>
                            <div class="grad-box">
                                <div class="rgst-box-inner">
                                    <ul>
                                        <li>
                                            <a class="-link{{ Request::is('user/profile/edit') ? ' -link--active' : '' }}"
                                               href="{{ route('user.profile.stream') }}"><i
                                                        class="fa fa-user"></i> Newsfeed</a>
                                        </li>
                                        @if(Auth::user()->clubsWhereAdmin()->count() > 0)
                                            <li>
                                                <a class="-link{{ Request::is('chat') ? ' -link--active' : '' }}"
                                                   href="{{ url('chat/index') }}"><i class="fa fa-comments-o"
                                                                                     aria-hidden="true"></i>
                                                    Messenger</a>

                                            </li>
                                        @else

                                            @php
                                                $friend = Auth::user()->getFriends()->first();

                                                if(is_null($friend)){

                                                    $friendname = null;
                                                }
                                                else{
                                                    $friendname = $friend->username;
                                                }
                                            @endphp
                                            @isset($friendname)
                                                <li>
                                                    <a class="-link{{ Request::is('chat/') ? ' -link--active' : '' }}"
                                                       href="{{ route('chat', $friendname) }}"><i
                                                                class="fa fa-comments-o"
                                                                aria-hidden="true"></i>
                                                        Messenger</a>

                                                </li>
                                            @endisset
                                        @endif
                                        <li>
                                            <a class="-link{{ Request::is('benutzer/'.Auth::user()->username) ? ' -link--active' : '' }}"
                                               href="{{ route('user.detail', Auth::user()->username) }}"><i
                                                        class="fa fa-user"></i> Mein Profil</a>
                                        </li>
                                        @if(Auth::user()->isAdministratorOfClub())
                                            <li>
                                                <a class="-link{{ Request::is('user/dashboard') ? ' -link--active' : '' }}"
                                                   href="{{ route('user.dashboard') }}"><i
                                                            class="fa fa-shield"></i> Vereinsverwaltung</a>
                                            </li>
                                        @endif
                                        @if(Auth::check() && Auth::user()->isAdmin())
                                            <li class="{{ Request::is('admin/*') ? ' -link--active' : '' }}">
                                                <a class="-link" href="{{ route('admin.index') }}"><i
                                                            class="fa fa-unlock"></i> Administration</a>
                                            </li>
                                        @endif
                                        <li>
                                            <a class="-link{{ Request::is('user/profile/edit') ? ' -link--active' : '' }}"
                                               href="{{ route('user.detail', Auth::user()->username) }}#informationen"><i
                                                        class="fa fa-gears"></i> Persönliche Daten</a>
                                        </li>
                                        <li>
                                            <a class="-link{{ Request::is('user/profile/edit') ? ' -link--active' : '' }}"
                                               href="{{ route('user.detail', Auth::user()->username) }}#passwort"><i
                                                        class="fa fa-cog"></i> Einstellungen</a>
                                        </li>
                                        <li><a class="-link" href="{{ url('/logout')}}"><i
                                                        class="fa fa-sign-out"></i> Abmelden</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @else
                        <a href="{{ route('login') }}">Anmelden</a>
                    @endif
                </div>
            </div>
        </div>
    </nav>
</header>