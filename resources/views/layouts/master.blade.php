<!DOCTYPE html>
<html>
<head>
    <title>@yield('title', 'vereinsleben.de - wir. leben. sport.')</title>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta charset="utf-8">
    <meta name="description" content="@yield('meta_description', '')">
    <meta name="keywords" content="@yield('meta_keywords', '')">
    <meta name="author" content="@yield('meta_author', '')">
    <meta name="language" content="@yield('meta_language', '')">

    <meta property="og:title" content="@yield('title', 'vereinsleben.de - wir. leben. sport.')"/>
    <meta property="og:description" content="@yield('meta_description', '')">

    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <link rel="apple-touch-icon" href="{{ asset('apple-touch-icon.png') }}">
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('apple-touch-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('apple-touch-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('apple-touch-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('apple-touch-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('apple-touch-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('apple-touch-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('apple-touch-icon-152x152.png') }}">


    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta property="fb:app_id"          content="{{ env('FACEBOOK_CLIENT_ID')}}" />
    @yield('fb-meta')

    <script src="{{ asset('js/vendor-head.js') }}"></script>


    <script type="text/javascript">
        window.cookieconsent_options = {
            "message"  :"Cookies helfen uns bei der Bereitstellung unserer Dienste. Durch die Nutzung unserer Webseite erklärst du dich mit dem Einsatz von Cookies einverstanden.",
            "dismiss"  :"Bestätigen",
            "learnMore":"Weitere Informationen.",
            "link"     :"https://www.vereinsleben.de/content/datenschutz/",
            "theme"    : "light-bottom"
        };
    </script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/jcookie.js') }}"></script>
    <script src="https://www.instagram.com/embed.js"></script>

    <link rel="stylesheet" href="{{ asset('js/OwlCarousel2/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/OwlCarousel2/assets/owl.theme.default.min.css') }}">
    @if(app()->environment() === 'production')
        <link rel="stylesheet" href="{{ asset(elixir('css/vendor.css')) }}">
        <link rel="stylesheet" href="{{ asset(elixir('css/app.css')) }}">

    @else
        <link rel="stylesheet" href="{{ asset('css/vendor.css') }}">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    @endif
</head>
<body id="body">
@include('layouts.components.to-top')
@include('layouts.components.menu')
<div class="@if(isset($federalState) && $federalState->name == 'Rheinland-Pfalz') page @else pagetop @endif">
    {{-- Flash messages --}}
    @foreach (['warning', 'error', 'success', 'info'] as $level)
        @if (Session::has('flash-'.$level))
            <div class="flash-message flash-message--{{$level}}">
                @if(is_array(Session::get('flash-'.$level)))
                    @foreach(Session::get('flash-'.$level) as $error)
                        <li>{{$error}}</li>
                    @endforeach
                @else
                    {!! Session::get('flash-'.$level) !!}
                @endif
            </div>
        @endif
    @endforeach
    {{-- Form errors --}}

    @if(isset($errors) && $errors->count() > 0)
        <div class="flash-message flash-message--error">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @yield('content')
    @include('layouts.components.footer')
</div>

@if(app()->environment() === 'production')
    @include('layouts.utils.google.analytics')
    @include('layouts.utils.google.tag-manager-noscript')
    <script src="{{ asset(elixir('js/vendor.js')) }}"></script>
    @yield('js-additional-vendor')
    <script src="{{ asset(elixir('js/main.js')) }}"></script>
@else
    <script src="{{ asset(('js/vendor.js')) }}"></script>
    @yield('js-additional-vendor')
    <script src="{{ asset(('js/main.js')) }}"></script>
@endif
<style>
    .slide {
        position: relative;
    }

    .slick-slide {
        width: 100%;
    }
</style>
<script>
    $(function () {
        $('#dl-menu').dlmenu();
        $("#userlinks").hide();
    });

    $(function () {
        $(".dropdown").hover(
            function () {
                $('.dropdown-menu', this).stop(true, true).fadeIn("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");
            },
            function () {
                $('.dropdown-menu', this).stop(true, true).fadeOut("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");
            });
    });

    function friendshipEvent(senderUser, recipientUser, type, id) {
        var url = '{{ url('/benutzer') }}';
        $.get(url + '/' + senderUser + '/friendship_create/' + recipientUser + '?relation=' + type + '&ajax=1', function (data) {
            if (type == 'accept') {
                $("#acceptuser_" + id).hide();
                $("#denyuser_" + id).hide();
                $("#removeuser_" + id).show();
                $("#bloackuser_" + id).show();
            } else {
                $("#user_" + id).hide();
            }

        });
    }

    function sendRequest(senderUser, recipientUser, type, id) {
        var url = '{{ url('/benutzer') }}';
        $.get(url + '/' + senderUser + '/friendship_create/' + recipientUser + '?relation=' + type + '&ajax=1', function (data) {
            $("#" + type + "_" + id).empty().html(data);
        });
    }

    function Unfriendship(senderUser, recipientUser, type, id) {
        var url = '{{ url('/benutzer') }}';
        $.get(url + '/' + senderUser + '/friendship_edit/' + recipientUser + '?relation=' + type + '&ajax=1', function (data) {
            $("#user_" + id).hide();
        });
    }

    function updateProfile(current, save) {
        $('#save').val(save);
        $('#current').val(current);
        /*var url = '{{ url('/user') }}';*/
        var url = '/user';

        var result = $.post( url + '/profile/wizard/update', $('#form_wizard').serialize());
         result.done(function( data ) {
                    $("#wizard_" + current).hide();
                    $("#wizard_" + data).show();
          });

    }

    function ajaxLoad(url, type) {
        $('#' + type + '_more_result').remove();
        $('#' + type + '_loader').show();
        $.get(url, function (data) {
            $('#' + type + '_loader').hide();
            $('#' + type + '_result').append(data['data']['rendered']);
        });
    }

    function goToByScroll(id) {
        id = id.replace("link", "");
        $('html,body').animate({
                scrollTop: $("#" + id).offset().top
            },
            'slow');
    }

    $('ul.tabs li').click(function () {
        var tab_id = $(this).attr('data-tab');

        $('ul.tabs li').removeClass('current');
        $('.tab-content').removeClass('current');

        $(this).addClass('current');
        $("#" + tab_id).addClass('current');
    })

    $("#checkAll").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
    });

    $("#checkAllSpitzensportStates").change(function () {
        $(".spitzensportStatesSelection input:checkbox").prop('checked', $(this).prop("checked"));
    });

    $(".edit-email").click(function () {
        $("#emailupdate").slideToggle("slow", function () {
        });
    });

    $(".edit-links").click(function () {
        $("#userlinks").slideToggle("slow", function () {
        });
    });


    $('.modal__close').click(function () {
        $("div").remove(".modal__container");
    })

    $("#register").click(function () {
        if($('#terms').is(':checked'))
            $('#termserror').css('color', '');
        else
            $('#termserror').css('color', 'red');
    });

    $("#register").click(function () {
        if ($('#privacy').is(':checked'))
            $('#privacyerror').css('color', '');
        else
            $('#privacyerror').css('color', 'red');
    });

    @if(Auth::check())
        $("#register-btn").hide();

    @endif

    function nextStep(current, save) {

        if (current == 'thankyou') {
            location.reload(true);
            return;
        }
        if (current == 'close') {
            Cookies.set("hideProfilVervollstaendigung","1",{ expires: 1 });
            location.reload(true);
            return;
        }

        if (save == '1' && current == 'state') {
            if ($('#wstate').val() == '') {
                $('#wstate').addClass('error');
                return false;
            }
        }

        if (save == '1' && current == 'anrede') {
            $('#wgender').removeClass('error');
            $('#wfirstname').removeClass('error');
            $('#wlastname').removeClass('error');

            var firstname = $('#wfirstname').val();
            var lastname = $('#wlastname').val();


            if ($('#wgender').val() == '') {
                $('#wgender').addClass('error');
                return false;
            }

            let re_name = new RegExp('^[a-zA-ZäöüÄÖÜß\\-. ]+$');

            if (!re_name.exec(firstname)) {
                $('#wfirstname').addClass('error');
                console.log("Error Vorname");
                return false;
            }
            if (!re_name.exec(lastname)) {
                $('#wlastname').addClass('error');
                console.log("Error Nachname");
                return false;
            }

        }

        if (save == '1' && current == 'dob') {
            $('#wbirthday-day').removeClass('error');
            $('#wbirthday-month').removeClass('error');
            $('#wbirthday-year').removeClass('error');

            if ($('#wbirthday-day').val() == '') {
                $('#wbirthday-day').addClass('error');
                return false;
            }
            if ($('#wbirthday-month').val() == '') {
                $('#wbirthday-month').addClass('error');
                return false;
            }
            if ($('#wbirthday-year').val() == '') {
                $('#wbirthday-year').addClass('error');
                return false;
            }
        }

        if (save == '1' && current == 'postleitzahl') {
            $('#wzip').removeClass('error');
            $('#wcity').removeClass('error');

            let re_zip = new RegExp('^([0-9]{4,5})$');
            let re_city = new RegExp('^[a-zA-ZäöüÄÖÜß\\-. ]+$');


            var zip = $('#wzip').val();
            var city = $('#wcity').val();

            if (!re_zip.exec(zip)) {
                $('#wzip').addClass('error');
                return false;
            }
            if (!re_city.exec(city)) {
                $('#wcity').addClass('error');
                return false;
            }
        }
        updateProfile(current, save);
    }

    function nextStepClub(current, next, save) {

        if (current == 'thankyou') {
            $("#form_clubwizard").submit();
        }
        if (current == 'close') {
            location.reload();
            return;
        }

        var name = $.trim($('#wcname').val());
        var sport = $.trim($('#wcsport').val());
        var timeline_begin_year = $.trim($('#wctimeline_begin_year').val());
        var timeline_end_year = $.trim($('#wctimeline_end_year').val());
        var timeline_begin_month = $.trim($('#wctimeline_begin_month').val());
        var timeline_end_month = $.trim($('#wctimeline_end_month').val());
        var career_type = $.trim($('#wccareer_type').val());
        var street = $.trim($('#wcstreet').val());
        var house_number = $.trim($('#wchouse_number').val());
        var zip = $.trim($('#wczip').val());
        var city = $.trim($('#wccity').val());
        var web = $.trim($('#wcweb').val());


        if (save == '1' && current == 'state') {

            $('#wcname').removeClass('error');
            $('#wcsport').removeClass('error');
            if ($('#wcname').val() == '') {
                $('#wcname').addClass('error');
                return false;
            }
            if ($('#wcsport').val() == '') {
                $('#wcsport').addClass('error');
                return false;
            }
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }

        if (save == '1' && current == 'dob') {

            $('#wctimeline_begin_month').removeClass('error');
            if ($('#wctimeline_begin_month').val() == '') {
                $('#wctimeline_begin_month').addClass('error');
                return false;
            }
            $('#wctimeline_begin_year').removeClass('error');
            if ($('#wctimeline_begin_year').val() == '') {
                $('#wctimeline_begin_year').addClass('error');
                return false;
            }
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();

        }
        else if (save == '0' && current == 'dob') {
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }

        if (save == '1' && current == 'street') {
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }
        else if (save == '0' && current == 'street') {
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }

        if (save == '1' && current == 'postleitzahl') {

            $('#wccity').removeClass('error');
            $('#wczip').removeClass('error');

            if (!$.isNumeric(zip) && zip != '') {
                $('#wczip').addClass('error');
                return false;
            }

            if (zip.length != 5) {
                $('#wczip').addClass('error');
                return false;
            }

            if (!city.match('^[A-Za-z\u00E4\u00F6\u00FC\u00C4\u00D6\u00DC\u00df -]{2,50}$')) {
                $('#wccity').addClass('error');
                return false;
            }
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();

        }
        else if (save == '0' && current == 'postleitzahl') {
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }

        if (save == '1' && current == 'email') {
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }
        else if (save == '0' && current == 'email') {
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }

        if (save == '1' && current == 'photo') {

            /* var url = '{{ url('/user') }}';
            $.post(url + '/club/wizard/store', { name: name, street:street, house_number:house_number,zip:zip,city:city }, function (data) {
                if (data) {
                    $("#wizard_" + current).hide();
                    $("#wizard_" + data).show();
                }
            });*/
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }
        else if (save == '0' && current == 'photo') {
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }

        if (save == '1' && current == 'thanks') {
            $("#form_clubwizard").submit();
        }
    }


    function nextStepClubContest(current, next, save) {

        if (current == 'thankyou') {
            $("#form_clubwizardcontest").submit();
        }
        if (current == 'close') {
            location.reload();
            return;
        }

        var name = $.trim($('#wcname').val());
        var sport = $.trim($('#wcsport').val());
        var street = $.trim($('#wcstreet').val());
        var house_number = $.trim($('#wchouse_number').val());
        var zip = $.trim($('#wczip').val());
        var city = $.trim($('#wccity').val());
        var web = $.trim($('#wcweb').val());


        if (save == '1' && current == 'state') {

            $('#wcname').removeClass('error');
            $('#wcsport').removeClass('error');
            if ($('#wcname').val() == '') {
                $('#wcname').addClass('error');
                return false;
            }
            if ($('#wcsport').val() == '') {
                $('#wcsport').addClass('error');
                return false;
            }
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }

        if (save == '1' && current == 'dob') {

            // $('#wctimeline_begin_month').removeClass('error');
            // if ($('#wctimeline_begin_month').val() == '') {
            //     $('#wctimeline_begin_month').addClass('error');
            //     return false;
            // }
            // $('#wctimeline_begin_year').removeClass('error');
            // if ($('#wctimeline_begin_year').val() == '') {
            //     $('#wctimeline_begin_year').addClass('error');
            //     return false;
            // }
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();

        }
        else if (save == '0' && current == 'dob') {
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }

        if (save == '1' && current == 'street') {
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }
        else if (save == '0' && current == 'street') {
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }

        if (save == '1' && current == 'postleitzahl') {

            $('#wccity').removeClass('error');
            $('#wczip').removeClass('error');

            if (!$.isNumeric(zip) && zip != '') {
                $('#wczip').addClass('error');
                return false;
            }

            if (zip.length != 5) {
                $('#wczip').addClass('error');
                return false;
            }

            if (!city.match('^[A-Za-z\u00E4\u00F6\u00FC\u00C4\u00D6\u00DC\u00df -]{2,50}$')) {
                $('#wccity').addClass('error');
                return false;
            }
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();

        }
        else if (save == '0' && current == 'postleitzahl') {
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }

        if (save == '1' && current == 'email') {
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }
        else if (save == '0' && current == 'email') {
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }

        if (save == '1' && current == 'photo') {

            /* var url = '{{ url('/user') }}';
            $.post(url + '/club/wizard/store', { name: name, street:street, house_number:house_number,zip:zip,city:city }, function (data) {
                if (data) {
                    $("#wizard_" + current).hide();
                    $("#wizard_" + data).show();
                }
            });*/
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }
        else if (save == '0' && current == 'photo') {
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }

        if (save == '1' && current == 'thanks') {
            $("#form_clubwizardcontest").submit();
        }
    }



    $("#sortable").sortable();
    $("#sortable").disableSelection();

    $("input[name^='sportIdArr']").each(function () {
        $("#accordion_" + $(this).val()).accordion();
    });

    function getContact(id, dynamicId) {
        var url = '{{ url('/benutzer') }}';
        $.get(url + '/contact/' + id, function (data) {
            var result = data.split(':');
            $('#phone' + dynamicId).val(result[0]);
            $('#ap_email' + dynamicId).val(result[1]);
        });
    }

    $(document).ready(function () {
        setTimeout(function () {
            $('#messagewindow').animate({
                scrollTop: $('#messagewindow')[0].scrollHeight
            }, 1000)
        }, 2000);
    });

    var rootUrl = '<?php echo (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/'; ?>';

    $(function () {
        $('.keyword-item').click(function(){
          let sport = $(this).data('sport');
          let location = $(this).data('location');
          let url = rootUrl + sport + '/' + location;
            $("#keyword-url").html('');
          $("#keyword-url").append(
            '<a target="_blank" href="' + url + '">' + url + '</a>'
          )
        });
    });

</script>
<!-- Facebook Pixel Code -->
<script>
    $(".cc_banner cc_container cc_container--open").onclick =
    !function (f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function () {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '133008314040080');
    fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=133008314040080&ev=PageView&noscript=1"
    /></noscript>
<!-- End Facebook Pixel Code -->


{{-- use this in a component to append additional js files --}}
@yield('post-js')

{{-- stackable scripts via @push in views --}}
@stack('scripts')

</body>
</html>