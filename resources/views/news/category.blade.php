@extends('layouts.master')
@section('title')
    NEWS - Neuigkeiten bei vereinsleben.de
@endsection

@section('content')
    <section class="news__category-section">
        <div class="container">
            {!! Form::open(['route' => 'news.category', 'method' => 'GET', 'id' => 'newsfilter', 'class' => '']) !!}
            <div class="news__category-wrapper">
                <span class="news__category">NEWS</span>
            </div>
            <h1 class="news__headline">Alle News im Überblick</h1>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="input-group">
                        {!! Form::text('searchtext', isset($searchtext) ? $searchtext : null, ['id' => 'searchtext', 'autofocus', 'class' => 'input', 'placeholder' => 'Suchbegriff']) !!}
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="input-group">
                        {!! Form::select('federal_state', array('Bundesland wählen') + $federal_states->pluck('name', 'name')->all(), isset($federal_state) ? $federal_state : null, ['class' => 'select', 'id' => 'federal_state']) !!}
                    </div>
                </div>
                <div class="col-md-3  col-sm-3">
                    <div class="input-group">
                        {!! Form::select('news_type', [
                        '' => 'Artikeltyp auswählen',
                        '1' => 'Nachrichtenartikel',
                        '2' => 'Bildergalerien',
                        '3' => 'Verbandsinformationen',
                        '4' => 'Vereinsinformationen',
                        ], isset($news_type) ? $news_type : null, ['class' => 'select', 'id' => 'news_type']) !!}
                        {{--{!! Form::select('news_type', ['1' => 'Nachrichtenartikel', '2' => 'Bildergalerien'], '', ['class' => 'select', 'placeholder' => 'Artikeltyp auswählen', 'id' => 'news_type']) !!}--}}
                    </div>
                </div>
                <div class="col-md-3  col-sm-3">
                    {!! Form::submit('Auswahl anzeigen', array('class' => 'button button--light button--light-white button--center button--full-width')) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        <br><br>
        @if ($news->count() > 0)

            <section class="news-overview">
                <div class="container">
                    <div class="row">
                        <div data-load-more='{"url": "{{ route('news.items', ['federal_state' => $federal_state, 'searchtext' => $searchtext, 'news_type' => $news_type]) }}"}'>
                            <div data-load-more-html></div>

                            <div class="col-xs-12 col-md-12">
                                <div data-loader class="loader__wrapper loader__wrapper--centered">
                                    <div class="loader loader--loading"></div>
                                    <div class="loader loader--loading"></div>
                                </div>
                            </div>
                            @if($news->count() > 12)
                                <div class="col-xs-12 col-md-12">
                                    <div class="btn btn-default">
                                        <button data-load-more-button
                                                class=" registrieren-btn">
                                            Weitere News laden
                                        </button>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </section>
        @else
            <div class="container">
                <p>
                    Keine Nachrichten vorhanden
                </p>
            </div>
        @endif
    </section>
@endsection