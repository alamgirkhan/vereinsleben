@extends('layouts.master')
@section('title')
    {{ $news->title }}

    @if(isset($news->sub_title))
        - {{ $news->sub_title }}
    @endif
@endsection
@section('meta_description')@if(isset($news->meta->meta_description )){{ $news->meta->meta_description }}@endif @endsection
@section('meta_language')@if(isset($news->meta->meta_description )){{ $news->meta->meta_language}}@endif @endsection
@section('meta_author')@if(isset($news->meta->meta_description )){{ $news->meta->meta_author }}@endif @endsection
@section('meta_keywords')@if(isset($news->meta->meta_description )){{ $news->meta->meta_keywords }}@endif @endsection

@section('fb-meta')
    <meta property="og:type"            content="article" />
    <meta property="og:url"             content="{{url()->current()}}" />
    <meta property="og:title"           content="{{$news->title ? $news->title : '$news->title'}}" />
    <meta property="og:image"           content="{{isset($news->main_image) && $news->main_image->originalFilename() != null ? asset($news->main_image->url('singleview')) : ''}}" />
    <meta property="og:description"    content="{{$news->meta->meta_description ? $news->meta->meta_description : ''}}" />
@endsection

@section('content')
    <section class="news-detail">
        <div class="container">
            <div class="row">
                {{-- main content --}}
                <div class="col-lg-9 col-md-8 col-sm-7 col-xs-12 news">
                    @can('edit', $news)
                        <a class="pull-right button button--center button--light button--icon"
                           href="{{ route('admin.news.edit', $news->id) }}">
                            &nbsp;<span class="fa fa-edit"></span> News bearbeiten&nbsp;
                        </a>
                    @endcan

                    <h2>{{ $news->title }}</h2>
                    @if(isset($news->sub_title))
                        <h3>{{ $news->sub_title }}</h3>
                    @endif

                    <span class="label label-default sportnews">
					<a class="news-detail__category-link"
                       href="{{ route('news.category', $news->category->slug) }}">
						<span class="news-detail__category">{{ $news->category->parentCategory->title }}</span>
						> <span class="news-detail__subcategory">{{ $news->category->title }}</span>
					</a>
				    </span>

                        <span class="article-time"><i class="fa fa-clock-o" aria-hidden="true"></i>
				      Veröffentlicht am {{ strftime('%A, %d. %B %Y', strtotime($news->published_at)) }}
				    </span>
                        @if($imageRes)
                            <div class="news-lead-img">
                                <img class="news-detail__main-image"
                                     src="{{ asset($imageRes->picture->url('original')) }}"/>
                            </div>
                            <div class="row news-slide-caption">
                                <div class="col-lg-9 col-md-8 col-sm-7 col-xs-7 news-slide news-title-desk">
                                    <h2>{{ $imageRes->title }}</h2>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 news-slide-arrow">
                                    @if($prevImgUrl)
                                        <a href="{{ $prevImgUrl }} ">
                                            @endif
                                            <span class="news-prev"><i class="fa fa-angle-left"
                                                                       aria-hidden="true"></i></span>
                                            @if($prevImgUrl)
                                        </a>
                                    @endif
                                    <span class="pagingInfo">{{ $seq }} von {{ $news->images()->count() }}</span>
                                    @if($nextImgUrl)
                                        <a href="{{ $nextImgUrl }} ">
                                            @endif
                                            <span class="news-next"><i class="fa fa-angle-right"
                                                                       aria-hidden="true"></i></span>
                                            @if($nextImgUrl)
                                        </a>
                                    @endif
                                </div>
                            </div>
                        <div class="row news-slide-caption news-title-mob">
                            <div class="col-sm-12 col-xs-12 news-slide">
                                <h2>{{ $imageRes->title }}</h2>
                            </div>
                        </div>

                        <p>
                            {!! $imageRes->description !!}
                        </p>
                        @else
                            @if ($news->main_image->originalFilename() !== null)
                                <div class="news-lead-img">
                                    <img class="news-detail__main-image"
                                         src="{{ asset($news->main_image->url('singleview')) }}"
                                         alt="@if(isset($news->meta->meta_bild_alt )){{ $news->meta->meta_bild_alt }}@endif">
                                </div>
                                @if ($news->main_image_source !== null && $news->main_image_source !== '')
                                    <p class="news-lead-img-cpsn">
                                        Quelle: {{ $news->main_image_source }}
                                    </p>
                                @endif
                            @endif
                        @endif

                    <h4>{{ $news->content_teaser }}</h4>
                    <p>
                        {!! $news->content !!}
                    </p>

                        @if(count($news->tags) != 0)
                            <div class="row">
                                <div class="col-lg-12 profile-post-tabs">

                                    @foreach($news->tags as $tag)
                                        <li><a href="#">{{ $tag->name }}</a></li>
                                    @endforeach

                                </div>
                            </div>
                        @endif
                    @if(isset($campaign) && isset($campaign->banner_horizontal))
                        <div class="image-hor campaign-image" data-toggle="tooltip" title="{{$campaign->description}}">
                            <a target="_blank" href="{{$campaign->link}}"> <img src="/uploaded/campaign/{{$campaign->banner_horizontal}}" alt=""></a>
                        </div>
                    @endif
                </div>

                {{-- aside --}}

                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="rhs-box">
                                <h2>Artikel Teilen</h2>
                                <ul class="social-icons">
                                    <li>
                                        {{--changes regarding new news layout--}}
                                        {{--<a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(route('news.detail', [$news->category->slug, $news->slug])) }}"--}}
                                        {{--target="_blank"><i class="fa fa-facebook"></i></a>--}}
                                        <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(route('news.detail', [$news->category_id, $news->slug])) }}"
                                           target="_blank"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li>
                                        {{--changes regarding new news layout--}}
                                        {{--<a href="https://twitter.com/intent/tweet?url={{ urlencode(route('news.detail', [$news->category->slug, $news->slug])) }}"--}}
                                        {{--target="_blank"><i class="fa fa-twitter"></i></a>--}}
                                        <a href="https://twitter.com/intent/tweet?url={{ urlencode(route('news.detail', [$news->category_id, $news->slug])) }}"
                                           target="_blank"><i class="fa fa-twitter"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    @if($related->count() > 0)
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="rhs-box">
                                    <h2>Mehr zum Thema</h2>
                                    <aside class="news-detail__aside" data-pager
                                           data-pager-content='{"url": "{{ route('news.related', [$news->category->id, $news->id]) }}", "category_id": "{{ $news->category->id }}"}'
                                           data-pager-url="{{ route('news.related') }}"
                                           data-token="{{ csrf_token() }}">
                                        <div class="news-detail__aside-inner">
                                            <div data-pager-html>
                                                <div data-loader class="loader__wrapper loader__wrapper--centered">
                                                    <div class="loader loader--loading"></div>
                                                    <div class="loader loader--loading"></div>
                                                </div>
                                            </div>

                                            <div class="news-detail__aside-news-pagination">
                                                <span data-pager-previous
                                                      class="pager-control__previous fa fa-chevron-left"></span>
                                                {{--changes regarding new news layout--}}
                                                {{--<a class="news-detail__category-link"--}}
                                                {{--href="{{ route('news.category', $news->category->slug) }}">--}}
                                                <a class="news-detail__category-link"
                                                   href="{{ route('news.category') }}">
                                                    <span class="pager-control__label">Alle Artikel</span>
                                                </a>
                                                <span data-pager-next
                                                      class="pager-control__next fa fa-chevron-right"></span>
                                            </div>
                                        </div>
                                    </aside>
                                </div>
                            </div>
                        </div>
                    @endif
                  @if(isset($campaign) && isset($campaign->banner_rechteck))
                    <div class="image-me campaign-image" data-toggle="tooltip" title="{{$campaign->description}}">
                      <a target="_blank" href="{{$campaign->link}}"> <img src="/uploaded/campaign/{{$campaign->banner_rechteck}}" alt=""></a>
                    </div>
                  @endif
                  @if(isset($campaign) && isset($campaign->banner_vertikal))
                    <div class="tablet-ads-vertical">
                        <div class="image-ver campaign-image" data-toggle="tooltip"
                             title="{{$campaign->description}}">
                            <a target="_blank" href="{{$campaign->link}}"> <img
                                        src="/uploaded/campaign/{{$campaign->banner_vertikal}}" alt=""></a>
                        </div>
                    </div>
                  @endif
                </div>
            </div>
            @if(isset($campaign) && isset($campaign->banner_vertikal))
                <div class="ads-vertical">
                    <div class="image-ver campaign-image" data-toggle="tooltip" title="{{$campaign->description}}">
                        <a target="_blank" href="{{$campaign->link}}"> <img src="/uploaded/campaign/{{$campaign->banner_vertikal}}" alt=""></a>
                    </div>
                </div>
            @endif
        </div>
    </section>
    @if (count($latest) > 0)
        @include('news.partials.latest-teaser', [
        'news' => $latest,
        'start' => 1,
        'end' => 3,
        'inverted' => true,
        ])
    @endif
@endsection