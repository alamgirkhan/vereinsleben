@extends('layouts.master')

@section('content')
    <div class="news">
        <h1>News</h1>

        @if ($news->count() > 0)
            <ol>
                @foreach ($news as $news)
                    <li>
                        @include('news.teaser', ['news' => $news])
                    </li>
                @endforeach
            </ol>
        @else
            <p>
                Keine News vorhanden
            </p>
        @endif
    </div>
@endsection