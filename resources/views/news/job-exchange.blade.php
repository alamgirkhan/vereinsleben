@extends('layouts.master')

@section('title')
    Stellenbörse
@endsection

@section('content')
    <section class="job-exchange">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <section class="job-exchange__main">
                        <h1 class="job-exchange__headline">Stellenbörse</h1>
                        <div class="job-exchange__content">
                            <iframe src="//www.trainersuchportal.de/partnerwidget/NVXYQ/start" width="100%" frameborder="0"
                                    style="margin: 0px; border: 0px; width: 100%; min-height: 710px; overflow: hidden; height: 947px;"
                                    scrolling="no" id="iFrameResizer0">
                                <a title="vereinsleben.de, zu den Stellenangeboten" href="http://www.trainersuchportal.de/suche?cooperation_partner=252">
                                    Ihr Browser unterst&amp;uuml;tzt keine iFrames. Nutzen Sie bitte diesen Link.
                                </a></iframe>
                            <script type="text/javascript" src="//www.trainersuchportal.de/static/website/js/iframeResizer.min.js"></script>
                            <script type="text/javascript">
                                iFrameResize({
                                    log : false,
                                    resizedCallback : function(messageData){},
                                    messageCallback : function(messageData){},
                                    closedCallback : function(id){}
                                });
                            </script>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <ul class="share-list">
                                    <li class="share-list__item share-list__item--horizontal">
                                        <span class="fa fa-facebook share-list__icon share-list__icon--facebook"></span>
                                        <a href="https://www.facebook.com/sharer/sharer.php?u={{ url('/service/stellenboerse') }}"
                                           target="_blank"
                                           class="share-list__link share-list__link--facebook">
                                            Auf Facebook teilen
                                        </a>

                                    </li>
                                    <li class="share-list__item share-list__item--horizontal">
                                        <span class="fa fa-twitter share-list__icon share-list__icon--twitter"></span>
                                        <a href="https://twitter.com/intent/tweet?url={{ url('/service/stellenboerse') }}"
                                           target="_blank"
                                           class="share-list__link share-list__link--twitter">
                                            Auf Twitter teilen
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>
@endsection