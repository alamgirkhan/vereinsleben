@if(count($news) > 0)
    <section class="sportnews-section">
        <div class="skyscraper">
            %nbsp;
        </div>
        <div class="skyscraper-half">
            %nbsp;
        </div>

        <div class="container">

            <div class="row">
                <div class="col-lg-12">
                    <h2>Aus aller Welt</h2>
                    <h3>NEWS & STORIES</h3>
                </div>
            </div>
            <div class="row sportnews-start">

                <div class="col-lg-9 col-md-8 col-sm-6 hnl">
                    <div class="row">
                        @if(isset($news[0]))
                            <div class="col-lg-4 col-sm-6 mobile-news-row1">
                                {{--03-08-2018: changes regarding new news layout--}}
                                {{--<a href="{{ route('news.detail', [$news[0]->category->slug, $news[0]->slug]) }}">--}}
                                <a href="{{ route('news.detail', [$news[0]->category_id, $news[0]->slug]) }}">
                                    <div class="row m-new-l">
                                        <div class="col-lg-12 news-a space-right-remove"
                                             style="background:url('{{ ($news[0]->main_image !== null) ? asset($news[0]->main_image->url('startpage')) : '' }}') top center no-repeat;background-size: cover;">
                                            <div class="news-arrow-top">
                                                <img src="{{asset('static_images/arrow-top.png')}}" alt=""/>
                                            </div>
                                            <div class="news-arrow-left"><img
                                                        src="{{asset('static_images/arrow-left.png')}}" alt=""/></div>
                                        </div>
                                    </div>
                                </a>
                                <div class="row  m-new-r">
                                    <div class="col-lg-12 m-new-row">
                                        <div class="news-box">
                                            <div class="news-box-inner">
                                                <h4>
                                                    {{--03-08-2018: changes regarding new news layout--}}
                                                    {{--<a href="{{ route('news.detail', [$news[0]->category->slug, $news[0]->slug]) }}">{{ $news[0]->title }}</a>--}}
                                                    <a href="{{ route('news.detail', [$news[0]->category_id, $news[0]->slug]) }}">{{ $news[0]->title }}</a>
                                                </h4>
                                                <p> {{ str_limit($news[0]->content_teaser, 150, '...') }}</p>
                                                <div class="mehr">
                                                    {{--03-08-2018: changes regarding new news layout--}}
                                                    {{--<a href="{{ route('news.detail', [$news[0]->category->slug, $news[0]->slug]) }}">Mehr</a>--}}
                                                    <a href="{{ route('news.detail', [$news[0]->category_id, $news[0]->slug]) }}">Mehr</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="col-lg-8 col-sm-6 mobile-news-row2">
                            @if(isset($news[1]))
                                <div class="row news-row-space1">
                                    <div class="col-lg-6 col-sm-6 space-right-remove m-new-l">
                                        <div class="news-box news-box2">
                                            <div class="news-box-inner">
                                                <h4>
                                                    {{--03-08-2018: changes regarding new news layout--}}
                                                    <a href="{{ route('news.detail', [$news[1]->category_id, $news[1]->slug]) }}">{{ $news[1]->title }}</a>
                                                </h4>
                                                <p> {{ str_limit($news[1]->content_teaser, 150, '...') }}</p>
                                                <div class="mehr">
                                                    {{--03-08-2018: changes regarding new news layout--}}
                                                    <a href="{{ route('news.detail', [$news[1]->category_id, $news[1]->slug]) }}">Mehr</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{--03-08-2018: changes regarding new news layout--}}
                                    <a href="{{ route('news.detail', [$news[1]->category_id, $news[1]->slug]) }}">
                                        <div class="col-lg-6 col-sm-6 news-b space-left-remove m-new-r"
                                             style="background:url('{{ ($news[1]->main_image !== null) ? asset($news[1]->main_image->url('startpage')) : '' }}') top center no-repeat;background-size: cover;">
                                            <div class="news-arrow-right"><img
                                                        src="{{asset('static_images/arrow-right.png')}}" alt=""/></div>
                                        </div>
                                    </a>
                                </div>
                            @endif
                            @if(isset($news[2]))
                                <div class="row news-row-space1">
                                    {{--03-08-2018: changes regarding new news layout--}}
                                    <a href="{{ route('news.detail', [$news[2]->category_id, $news[2]->slug]) }}">
                                        <div class="col-lg-6 col-sm-6 news-b space-right-remove m-new-l"
                                             style="background:url('{{ ($news[2]->main_image !== null) ? asset($news[2]->main_image->url('startpage')) : '' }}') top center no-repeat;background-size: cover">
                                            <div class="news-arrow-left"><img src="{{asset('static_images/arrow-left.png')}}" alt="" /></div>
                                        </div>
                                    </a>
                                    <div class="col-lg-6 col-sm-6 space-right-remove m-new-r">
                                        <div class="news-box news-box2">
                                            <div class="news-box-inner">
                                                <h4>
                                                    {{--03-08-2018: changes regarding new news layout--}}
                                                    <a href="{{ route('news.detail', [$news[2]->category_id, $news[2]->slug]) }}">{{ $news[2]->title }}</a>
                                                </h4>
                                                <p> {{ str_limit($news[2]->content_teaser, 150, '...') }}</p>
                                                <div class="mehr">
                                                    {{--03-08-2018: changes regarding new news layout--}}
                                                    <a href="{{ route('news.detail', [$news[2]->category_id, $news[2]->slug]) }}">Mehr</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-8 col-sm-6 mobile-news-row2">
                            @if(isset($news[3]))
                                <div class="row news-row-space1">
                                    {{--03-08-2018: changes regarding new news layout--}}
                                    <a href="{{ route('news.detail', [$news[3]->category_id, $news[3]->slug]) }}">
                                        <div class="col-lg-6 col-sm-6 news-b space-right-remove m-new-l"
                                             style="background:url('{{ ($news[3]->main_image !== null) ? asset($news[3]->main_image->url('startpage')) : '' }}') top center no-repeat;background-size: cover;">
                                        <div class="news-arrow-left"><img
                                                    src="{{asset('static_images/arrow-left.png')}}" alt=""/></div>
                                    </div>
                                    </a>
                                    <div class="col-lg-6 col-sm-6 space-right-remove m-new-r">
                                        <div class="news-box news-box2">
                                            <div class="news-box-inner">
                                                <h4>{{--03-08-2018: changes regarding new news layout--}}
                                                    <a href="{{ route('news.detail', [$news[3]->category_id, $news[3]->slug]) }}">{{ $news[3]->title }}</a>
                                                </h4>
                                                <p> {{ str_limit($news[3]->content_teaser, 150, '...') }}</p>
                                                <div class="mehr">
                                                    {{--03-08-2018: changes regarding new news layout--}}
                                                    <a href="{{ route('news.detail', [$news[3]->category_id, $news[3]->slug]) }}">Mehr</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @if(isset($news[4]))
                                <div class="row news-row-space1">
                                    <div class="col-lg-6 col-sm-6 space-right-remove m-new-l">
                                        <div class="news-box news-box2">
                                            <div class="news-box-inner">
                                                <h4>
                                                    {{--03-08-2018: changes regarding new news layout--}}
                                                    <a href="{{ route('news.detail', [$news[4]->category_id, $news[4]->slug]) }}">{{ $news[4]->title }}</a>
                                                </h4>
                                                <p> {{ str_limit($news[4]->content_teaser, 150, '...') }}</p>
                                                <div class="mehr">
                                                    {{--03-08-2018: changes regarding new news layout--}}
                                                    <a href="{{ route('news.detail', [$news[4]->category_id, $news[4]->slug]) }}">Mehr</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{--03-08-2018: changes regarding new news layout--}}
                                    <a href="{{ route('news.detail', [$news[4]->category_id, $news[4]->slug]) }}">
                                        <div class="col-lg-6 col-sm-6 news-b space-left-remove m-new-r"
                                             style="background:url('{{ ($news[4]->main_image !== null) ? asset($news[4]->main_image->url('startpage')) : '' }}') top center no-repeat;background-size: cover;">
                                            <div class="news-arrow-right"><img
                                                        src="{{asset('static_images/arrow-right.png')}}" alt=""/></div>
                                        </div>
                                    </a>
                                </div>
                            @endif
                        </div>
                        @if(isset($news[5]))
                            <div class="col-lg-4 col-sm-6 mobile-news-row1 right-skining-news">
                                <div class="row m-new-l">
                                    {{--03-08-2018: changes regarding new news layout--}}
                                    <a href="{{ route('news.detail', [$news[5]->category_id, $news[5]->slug]) }}">
                                        <div class="col-lg-12 news-a space-right-remove"
                                             style="background:url('{{ ($news[5]->main_image !== null) ? asset($news[5]->main_image->url('startpage')) : '' }}'); top center no-repeat;background-size: cover;">
                                            <div class="news-arrow-top"><img src="{{asset('static_images/arrow-top.png')}}" alt="" /></div>
                                            <div class="news-arrow-left"><img
                                                        src="{{asset('static_images/arrow-left.png')}}" alt=""/></div>
                                        </div>
                                    </a>
                                </div>
                                <div class="row m-new-r">
                                    <div class="col-lg-12 m-new-row">
                                        <div class="news-box">
                                            <div class="news-box-inner">
                                                <h4>
                                                    {{--03-08-2018: changes regarding new news layout--}}
                                                    <a href="{{ route('news.detail', [$news[5]->category_id, $news[5]->slug]) }}">{{ $news[5]->title }}</a>
                                                </h4>
                                                <p> {{ str_limit($news[5]->content_teaser, 150, '...') }}</p>
                                                <div class="mehr">
                                                    {{--03-08-2018: changes regarding new news layout--}}
                                                    <a href="{{ route('news.detail', [$news[5]->category_id, $news[5]->slug]) }}">Mehr</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>

                    <div class="row">
                        @if(isset($news[6]))
                            <div class="col-lg-4 col-sm-6 mobile-news-row1">
                                {{--03-08-2018: changes regarding new news layout--}}
                                <a href="{{ route('news.detail', [$news[6]->category_id, $news[6]->slug]) }}">
                                    <div class="row m-new-l">
                                        <div class="col-lg-12 news-a space-right-remove"
                                             style="background:url('{{ ($news[6]->main_image !== null) ? asset($news[6]->main_image->url('startpage')) : '' }}'); top center no-repeat;background-size: cover;">
                                            <div class="news-arrow-top"><img src="{{asset('static_images/arrow-top.png')}}" alt="" /></div>
                                            <div class="news-arrow-left"><img
                                                        src="{{asset('static_images/arrow-left.png')}}" alt=""/></div>
                                        </div>
                                    </div>
                                </a>
                                <div class="row m-new-r">
                                    <div class="col-lg-12 m-new-row">
                                        <div class="news-box">
                                            <div class="news-box-inner">
                                                <h4>
                                                    {{--03-08-2018: changes regarding new news layout--}}
                                                    <a href="{{ route('news.detail', [$news[6]->category_id, $news[6]->slug]) }}">{{ $news[6]->title }}</a>
                                                </h4>
                                                <p> {{ str_limit($news[6]->content_teaser, 150, '...') }}</p>
                                                <div class="mehr">
                                                    {{--03-08-2018: changes regarding new news layout--}}
                                                    <a href="{{ route('news.detail', [$news[6]->category_id, $news[6]->slug]) }}">Mehr</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if(isset($featuredNews[0]))
                            <div class="col-lg-8 col-sm-6  featured-news-lead">
                            <div class="row news-row-space1 featured-news">
                                {{--03-08-2018: changes regarding new news layout--}}
                                <a href="{{ route('news.detail', [$featuredNews[0]->category_id, $featuredNews[0]->slug]) }}">

                                    <div class="col-lg-6 col-sm-6 large-news space-right-remove"
                                         style="background:url('{{ ($featuredNews[0]->main_image !== null) ? asset($featuredNews[0]->main_image->url('singleview')) : '' }}') top center no-repeat;background-size: cover;">
                                        <h4>
                                            {{ $featuredNews[0]->title }}
                                            {{--<a href="{{ route('news.detail', [$featuredNews[0]->category->slug, $featuredNews[0]->slug]) }}">{{ $featuredNews[0]->title }}</a>--}}
                                        </h4>
                                        {{--<div class="mehr">--}}
                                        {{--Mehr--}}
                                        {{--<a href="{{ route('news.detail', [$featuredNews[0]->category_id, $featuredNews[0]->slug]) }}">Mehr</a>--}}
                                        {{--</div>--}}
                                    </div>
                                </a>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 col-sm-6 news-ticker-lead">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="news-ticker-section">
                                <h4>Weitere News</h4>
                                <ul class="news-ticker-links">
                                    @for($i = 7; $i < count($news); $i++)
                                        @if(isset($news[$i]))
                                            <li>
                                                {{--03-08-2018: changes regarding new news layout--}}
                                                <a href="{{ route('news.detail', [$news[$i]->category_id, $news[$i]->slug]) }}">{{ $news[$i]->title }}</a>
                                            </li>
                                        @endif
                                    @endfor
                                </ul>
                            </div>
                            <div class="content-ad01">
                                %nbsp;
                            </div>
                            <div class="content-ad01">
                                %nbsp;
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row alle-news">
                <div class="col-lg-12">
                    <a class="btn btn-default registrieren-btn" href="{{ url('/neuigkeiten') }}">
                        alle News
                    </a>
                </div>
            </div>
        </div>
    </section>
@endif