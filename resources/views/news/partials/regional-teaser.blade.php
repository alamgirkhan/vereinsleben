@if(count($news) >= 3)
    <section class="sport-regional">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Sport Regional</h2>
                    <h3>AKTUELLES AUS
                        @if(Auth::check() && Auth::user()->desiredState())
                            {{ Auth::user()->desiredState()->name }}
                        @else
                            {{ $sessionState == null ? '' : $sessionState->name}}
                        @endif
                    </h3>
                </div>
            </div>
            <div class="row sport-regional-start">
                @for($i = 0; $i < 4; $i++)
                    @if(isset($news[$i]))
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 spt-rgnl-box">
                            <a href="{{ route('news.detail', [$news[$i]->category_id, $news[$i]->slug]) }}">
                                <div class="news-overview__image"
                                     style="background:url('{{ ($news[$i]->main_image !== null) ? asset($news[$i]->main_image->url('startpage')) : '' }}') top center no-repeat;background-size: cover;"></div>
                            </a>
                            <div class="spt-rgnl-cnt">
                                <a href="{{ route('news.detail', [$news[$i]->category_id, $news[$i]->slug]) }}">
                                    {{ str_limit($news[$i]->title, 42, '...') }}
                                </a>
                                <br><br>
                                <p> {{ str_limit($news[$i]->content_teaser, 150, '...') }}</p>
                                <br>
                                <div class="mehr">
                                    <a href="{{ route('news.detail', [$news[$i]->category_id, $news[$i]->slug]) }}">Mehr</a>
                                </div>
                            </div>
                        </div>
                    @endif
                @endfor
            </div>
            <div class="row alle-news">
                <div class="col-lg-12">
                    <a id="regnews" class="btn btn-default registrieren-btn"
                       href="{{ url('/neuigkeiten/regional') }}">
                        alle News
                    </a>
                </div>
            </div>
        </div>
    </section>
@endif