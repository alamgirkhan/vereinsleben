<div class="news-detail__aside-news-item">
    <a class="news-detail__aside-link" href="{{ $news->slug }}">
        <span class="news-detail__aside-news-item-date">{{ strftime('%d. %B %Y', strtotime($news->published_at)) }}</span>
        <span class="news-detail__aside-news-item-caption" data-insert-hyphen-async="true">{{ $news->title }}</span>
        <div class="news-detail__aside-news-item-image"
             style="background-image: url('{{ asset($news->main_image->url('startpage')) }}');">
        </div>
    </a>
</div>