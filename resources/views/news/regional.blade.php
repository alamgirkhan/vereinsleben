@extends('layouts.master')
@section('title')
    Regionale NEWS - Neuigkeiten aus deiner Region
@endsection

@section('content')
    <section class="news__category-section">
        <div class="container">
            {!! Form::open(['route' => 'news.regional', 'method' => 'GET', 'id' => 'regionalnewsfilter', 'class' => '']) !!}
            <div class="news__category-wrapper">
                <span class="news__category">Regionale NEWS</span>
            </div>
            <h1 class="news__headline">Alle regionalen News im Überblick</h1>

            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="input-group">
                        {!! Form::text('searchtext', isset($query['searchtext']) ? $query['searchtext'] : null, ['id' => 'searchtext', 'autofocus', 'class' => 'input', 'placeholder' => 'Suchbegriff']) !!}
                    </div>
                </div>
                <div class="col-md-4  col-sm-4">
                    <div class="input-group">
                        {!! Form::select('news_type', [
                        '' => 'Artikeltyp auswählen',
                        '1' => 'Nachrichtenartikel',
                        '2' => 'Bildergalerien',
                        '3' => 'Verbandsinformationen',
                        '4' => 'Vereinsinformationen',
                        ], isset($query['news_type']), ['class' => 'select', 'id' => 'news_type']) !!}
                    </div>
                </div>
                <div class="col-md-4  col-sm-4">
                    {!! Form::submit('Auswahl anzeigen', array('class' => 'button button--light button--light-white button--center button--full-width')) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        @if (count($news) > 0)
            <br><br>
            <section class="news-overview">
                <div class="container">
                    {{--<div class="row">--}}
                    {{--TODO pagination with load more button--}}
                    @for($i = 0; $i < count($news); $i++)
                        <div class="col-xxs-12 col-xs-6 col-md-3">
                            <figure class="news-overview__teaser">
                                <div class="news-overview__image-wrapper">
                                    <a class="news-overview__link"
                                       href="{{ route('news.detail', [$news[$i]->category_id, $news[$i]->slug]) }}">
                                        <div class="news-overview__image"
                                             style="background-image: url('{{ asset($news[$i]->main_image->url('startpage')) }}');">
                                        </div>
                                    </a>
                                </div>
                                <figcaption class="news-overview__item-content news-overview__item-content--plain">
                                    <h3 class="news-overview__item-headline" data-insert-hyphen="true">
                                        <a class="news-overview__link"
                                           href="{{ route('news.detail', [$news[$i]->category_id, $news[$i]->slug]) }}">
                                            {{ str_limit($news[$i]->title, $limit = 46, $end = '...') }}
                                        </a>
                                    </h3>
                                </figcaption>
                            </figure>
                        </div>
                    @endfor


                    {{--<div data-load-more='{"url": "{{ route('news.regionalitems', ['searchtext' => $searchtext, 'news_type' => $news_type]) }}"}'>--}}
                    {{--<div data-load-more-html></div>--}}

                    {{--<div class="col-xs-12 col-md-12">--}}
                    {{--<div data-loader class="loader__wrapper loader__wrapper--centered">--}}
                    {{--<div class="loader loader--loading"></div>--}}
                    {{--<div class="loader loader--loading"></div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--@if($news->count() > 12)--}}
                    {{--<div class="col-xs-12 col-md-12">--}}
                    {{--<div class="btn btn-default">--}}
                    {{--<button data-load-more-button--}}
                    {{--class=" registrieren-btn">--}}
                    {{--Weitere News laden--}}
                    {{--</button>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--@endif--}}
                    {{--</div>--}}
                    {{--</div>--}}
                </div>
            </section>
            {{--@else--}}
            {{--<div class="container">--}}
            {{--<p>--}}
            {{--Keine Nachrichten vorhanden--}}
            {{--</p>--}}
            {{--</div>--}}
        @endif
    </section>
@endsection
















{{--@extends('layouts.master')--}}
{{--@section('title')--}}
{{--Regionale NEWS - Neuigkeiten aus deiner Region--}}
{{--@endsection--}}

{{--@section('content')--}}
{{--<section class="news__category-section">--}}
{{--<div class="container">--}}
{{--{!! Form::open(['route' => 'news.regional', 'method' => 'GET', 'id' => 'regionalnewsfilter', 'class' => '']) !!}--}}
{{--<div class="news__category-wrapper">--}}
{{--<span class="news__category">Regionale NEWS</span>--}}
{{--</div>--}}
{{--<h1 class="news__headline">Alle regionalen News im Überblick</h1>--}}

{{--<div class="row">--}}
{{--<div class="col-md-4 col-sm-4 col-xs-12">--}}
{{--<div class="input-group">--}}
{{--{!! Form::text('searchtext', isset($query['searchtext']) ? $query['searchtext'] : null, ['id' => 'searchtext', 'autofocus', 'class' => 'input', 'placeholder' => 'Suchbegriff']) !!}--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="col-md-4  col-sm-4">--}}
{{--<div class="input-group">--}}
{{--{!! Form::select('news_type', [--}}
{{--'' => 'Artikeltyp auswählen',--}}
{{--'1' => 'Nachrichtenartikel',--}}
{{--'2' => 'Bildergalerien',--}}
{{--'3' => 'Verbandsinformationen',--}}
{{--'4' => 'Vereinsinformationen',--}}
{{--], isset($query['news_type']), ['class' => 'select', 'id' => 'news_type']) !!}--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="col-md-4  col-sm-4">--}}
{{--{!! Form::submit('Auswahl anzeigen', array('class' => 'button button--light button--light-white button--center button--full-width')) !!}--}}
{{--</div>--}}
{{--</div>--}}
{{--{!! Form::close() !!}--}}
{{--</div>--}}
{{--@if (count($news) > 0)--}}
{{--<section class="news-overview">--}}
{{--<div class="container">--}}
                    {{--<div class="row">--}}
                    {{--TODO pagination with load more button--}}
{{--@for($i = 0; $i < count($news); $i++)--}}
{{--<div class="col-xxs-12 col-xs-6 col-md-3">--}}
{{--<figure class="news-overview__teaser">--}}
{{--<div class="news-overview__image-wrapper">--}}
{{--<a class="news-overview__link"--}}
{{--href="{{ route('news.detail', [$news[$i]->category_id, $news[$i]->slug]) }}">--}}
{{--<div class="news-overview__image"--}}
{{--style="background-image: url('{{ asset($news[$i]->main_image->url('startpage')) }}');">--}}
{{--</div>--}}
{{--</a>--}}
{{--</div>--}}
{{--<figcaption class="news-overview__item-content news-overview__item-content--plain">--}}
{{--<h3 class="news-overview__item-headline" data-insert-hyphen="true">--}}
{{--<a class="news-overview__link"--}}
{{--href="{{ route('news.detail', [$news[$i]->category_id, $news[$i]->slug]) }}">--}}
{{--{{ str_limit($news[$i]->title, $limit = 46, $end = '...') }}--}}
{{--</a>--}}
{{--</h3>--}}
{{--</figcaption>--}}
{{--</figure>--}}
{{--</div>--}}
{{--@endfor--}}
{{--</div>--}}
{{--</section>--}}




                    {{--<div data-load-more='{"url": "{{ route('news.regionalitems', ['searchtext' => $searchtext, 'news_type' => $news_type]) }}"}'>--}}
                    {{--<div data-load-more-html></div>--}}

                    {{--<div class="col-xs-12 col-md-12">--}}
                    {{--<div data-loader class="loader__wrapper loader__wrapper--centered">--}}
                    {{--<div class="loader loader--loading"></div>--}}
                    {{--<div class="loader loader--loading"></div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--@if($news->count() > 12)--}}
                    {{--<div class="col-xs-12 col-md-12">--}}
                    {{--<div class="btn btn-default">--}}
                    {{--<button data-load-more-button--}}
                    {{--class=" registrieren-btn">--}}
                    {{--Weitere News laden--}}
                    {{--</button>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--@endif--}}
                    {{--</div>--}}
                    {{--</div>--}}
{{--</div>--}}
{{--</section>--}}
            {{--@else--}}
            {{--<div class="container">--}}
            {{--<p>--}}
            {{--Keine Nachrichten vorhanden--}}
            {{--</p>--}}
            {{--</div>--}}
{{--@endif--}}
{{--</section>--}}
{{--@endsection--}}


