@if($news->count() > 0)
    <section class="news-overview {{!isset($inverted) ?: 'news-overview--inverted'}}">
        <div class="container">
            <div class="row">
                @for($i = $start; $i <= $end; $i++)
                    @if(isset($news[$i-1]))
                        <div class="col-xxs-12 col-xs-6 col-md-4">
                            <figure class="news-overview__teaser">
                                <div class="news-overview__image-wrapper">
                                    <a class="news-overview__link"
                                       href="{{ route('news.detail', [$news[$i-1]->category->slug, $news[$i-1]->slug]) }}">
                                        <div class="news-overview__image"
                                             style="background-image: url('{{ asset($news[$i-1]->main_image->url('startpage')) }}');"></div>
                                        @if(!isset($hideCategory))
                                            <p class="news-overview__item-category-wrapper">{{$news[$i-1]->category->parentCategory->title}}
                                                <span class="news-overview__item-category">{{$news[$i-1]->category->title}}</span>
                                            </p>
                                        @endif
                                    </a>
                                </div>
                                <figcaption
                                        class="news-overview__item-content
                                        @if(isset($inverted)) news-overview__item-content--inverted @endif
                                        @if(isset($hideCategory)) news-overview__item-content--plain @endif
                                                ">
                                    <h3 class="news-overview__item-headline" data-insert-hyphen="true">{{ $news[$i-1]->title }}</h3>
                                    <p class="news-overview__item-text">
                                        {{ str_limit($news[$i-1]->content_teaser, 150, '...') }}
                                    </p>
                                </figcaption>
                                {{--<a class="button button--center button--light button--full-width" ∏--}}
                                {{--href="{{ route('news.detail', [$news[$i-1]->category->slug, $news[$i-1]->slug]) }}">Weiterlesen</a>--}}
                                <a class="button button--center button--light button--full-width" ∏
                                   href="{{ route('news.detail', [$news[$i-1]->category_id, $news[$i-1]->slug]) }}">Weiterlesen</a>
                            </figure>
                        </div>
                    @endif
                @endfor
            </div>
            @if(isset($loadMore) && $news->count() >= 12)
                <div class="col-md-12">
                    <div class="button__center-wrapper">
                        <button class="button button--padded button--dark">Weitere News laden</button>
                    </div>
                </div>
            @endif
        </div>
    </section>
@endif
