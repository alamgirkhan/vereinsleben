@extends('layouts.master')

@section('content')
  <div id="podcast_view">
    <div class="hero-slider__wrapper {{(isset($condensed)) ? ' hero-slider__wrapper--condensed' : ''}}">
        <div class="hero-slider">
            <div class="hero-slider__image"
                 style="background-image: url('{{asset('static_images/vereinsleben_9.jpg')}}')"></div>
            <div class="hero-slider__image"
                 style="background-image: url('{{asset('static_images/vereinsleben_7.jpg')}}')"></div>
            <div class="hero-slider__image"
                 style="background-image: url('{{asset('static_images/vereinsleben.jpg')}}')"></div>
            <div class="hero-slider__image"
                 style="background-image: url('{{asset('static_images/vereinsleben_8.jpg')}}')"></div>
        </div>
            <div class="hero-slider__content hero-slider__content--centered">
                <h1 class="headline-primary headline--centered headline-primary--inverted">PODCAST</h1>

                <!--
                <h2 class="headline-secondary headline-secondary--main headline-secondary--inverted">Wir. Leben. Sport.</h2>

                <div class="hero-slider__box-wrapper">
                        <a href="{{ url('/register') }}" class="hero-slider__big-btn-text">
                            <div class="hero-slider__big-btn">
                                Registriere Dich jetzt und lege Deinen Verein an!
                            </div>
                        </a>
                </div>
                -->
            </div>
    </div>

      <br><br>
      <section class="section1">
        <div class="container">
            <p>
                <iframe frameborder="0" height="950" marginheight="0" marginwidth="0" src="https://meinsportpodcast.de/fussball/?link=iframe&amp;size=xl&amp;id=vereinsleben" width="100%"></iframe>
            </p>

        </div>
      </section>

  </div>
@endsection