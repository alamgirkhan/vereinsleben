@extends('layouts.master')
@section('title')
    {{ $partner->title }}

    @if(isset($partner->sub_title))
        - {{ $partner->sub_title }}
    @endif
@endsection
@section('meta_description')@if(isset($partner->meta->meta_description )){{ $partner->meta->meta_description }}@endif @endsection
@section('meta_language')@if(isset($partner->meta->meta_description )){{ $partner->meta->meta_language}}@endif @endsection
@section('meta_author')@if(isset($partner->meta->meta_description )){{ $partner->meta->meta_author }}@endif @endsection
@section('meta_keywords')@if(isset($partner->meta->meta_description )){{ $partner->meta->meta_keywords }}@endif @endsection

@section('content')
    <section class="news-detail">
        <div class="container">
            <div class="row">
                {{-- main content --}}
                <div class="col-lg-9 col-sm-6 news">
                    <h2>{{ $partner->title }}</h2>
                    @if(isset($partner->sub_title))
                        <h3>{{ $partner->sub_title }}</h3>
                    @endif


                    <p>
                        {!! $partner->content !!}
                    </p>
                </div>

                {{-- aside --}}

                <div class="col-lg-3 col-sm-6">
                    <div class="row">
                        <div class="col-lg-12 profile-cnt-details">
                            <div class="rhs-box partner-box">
                                @if ($partner->logo->originalFilename() !== null)
                                    <img class="news-detail__main-image"
                                         src="{{ asset($partner->logo->url('original')) }}"/>
                                @endif

                                    @if($partner->address !== null && trim($partner->address) !== "")
                                        <div class="row" style="margin-top: 15px;">
                                            <div class="col-sm-2"><span><i class="fa fa-map-marker" aria-hidden="true"></i></span></div>
                                            <div class="col-sm-10">{{ $partner->address }}</div>
                                        </div>
                                    @endif
                                    @if($partner->phone !== null && trim($partner->phone) !== "")
                                        <div class="row" style="margin-top: 15px;">
                                            <div  class="col-sm-2"><span><i class="fa fa-phone" aria-hidden="true"></i></span></div>
                                            <div  class="col-sm-10">{{ $partner->phone }}</div>
                                        </div>
                                    @endif

                                    @if($partner->email !== null && trim($partner->email) !== "")
                                        <div class="row" style="margin-top: 15px;">
                                            <div  class="col-sm-2"><span><i class="fa fa-envelope" aria-hidden="true"></i></span></div>
                                            <div class="col-sm-10"><a href="mailto:{{ $partner->email }}">{{ $partner->email }}</a></div>
                                        </div>
                                    @endif

                                    @if($partner->website !== null && trim($partner->website) !== "")

                                        <div class="row" style="margin-top: 15px;" id="website">
                                            <div  class="col-sm-2"><span><i class="fa fa-globe" aria-hidden="true"></i></span></div>
                                            <div class="col-sm-10"><a href="{{ $partner->website }}" target="_blank">{{ $partner->websiteKurz }}</a></div>
                                        </div>
                                        </a>
                                    @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection