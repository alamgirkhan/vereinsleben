@extends('layouts.master')

@section('content')
    <section class="news__category-section">
        <div class="container">
            <h1 class="news__headline">Partner</h1>
        </div>

        @if ($partner->count() > 0)
            <section class="ptnr-overview">
                <div class="container">
                    <div class="row">
                        <div data-load-more='{"url": "{{ route('partner.single') }}"}'>
                            <div data-load-more-html></div>
                            <div class="col-xs-12 col-md-12">
                                <div data-loader class="loader__wrapper loader__wrapper--centered">
                                    <div class="loader loader--loading"></div>
                                    <div class="loader loader--loading"></div>
                                </div>
                            </div>
                            @if($partner->count() > 12)
                                <div class="col-xs-12 col-md-12">
                                    <div class="btn btn-default">
                                        <button data-load-more-button
                                                class=" registrieren-btn">
                                            Weitere Partner laden
                                        </button>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </section>
        @else
            <div class="container">
                <p>
                    Keine Nachrichten vorhanden
                </p>
            </div>
        @endif
    </section>
@endsection