<div class="col-xxs-12 col-xs-6 col-md-4">
    <figure class="ptnr-overview__teaser">
        <div class="news-overview__image-wrapper">
            <a class="news-overview__link"
               href="{{ route('partner.detail', [$partner->slug]) }}">
                <div class="ptnr-overview__image"
                     style="background-image: url('{{ asset($partner->main_image->url('startpage')) }}');"></div>
            </a>
        </div>
        <figcaption class="ptnr-overview__item-content ptnr-overview__item-content--plain">
            <h3 class="news-overview__item-headline"
                data-insert-hyphen="true">{{ $partner->title }}</h3>
            <p class="news-overview__item-text">
                {{ str_limit($partner->content_teaser, 150, '...') }}
            </p>
        </figcaption>
        <a class="button button--center button--light button--full-width"
           href="{{ route('partner.detail', [$partner->slug]) }}">Weiterlesen</a>
    </figure>
</div>
@if(isset($loadMore) && $partner->count() >= 12)
    <div class="col-md-12">
        <div class="button__center-wrapper">
            <button class="button button--padded button--dark">Weitere Partner laden</button>
        </div>
    </div>
@endif