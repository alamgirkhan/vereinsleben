@extends('layouts.master')
@section('title', 'vereinsleben.de - Euer Teambesprechungsraum - Gewinnt den coolsten Teambesprechungsraum für euren Verein')
@section('meta_description', 'Jetzt mitmachen und gewinnen, gewinnt den coolsten Teambesprechungsraum für euren Verein.')

@section('content')

    <div id="contest-app">
        @include('propose-song.header')
        <section class="padding-top-30px">
            <div class="container">

                <h2 class="hl hl--large hl--underlined hl--inverted hl--centered hl--upper">
                    Welcher Song ist euer Vereinshit?
                </h2>
                <p class="text-block text-block--compressed text-block--thin">
                    Schlagt jetzt einen Song vor, der am nächsten Sonntag zwischen 16 und 18 Uhr in der Sportshow auf RPR1 als euer "Vereinshit der Woche" für euch gespielt werden soll.
                </p>

            </div>
        </section>
        @include('propose-song.propose-song')
    </div>
@endsection