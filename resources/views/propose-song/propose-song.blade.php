<div class="section propose-song">
    @if(Auth::check())
        <div class="container">
            <div>
                <h2 class="hl hl--large hl--underlined hl--inverted hl--centered hl--upper">
                    EUREN VEREINSHIT VORSCHLAGEN!
                </h2>
                <p class="text-block text-block--compressed text-block--thin">
                    Einfach euren Verein sowie Songtitel- und Interpret eintragen und mit etwas Glück wird euer Vorschlag in der nächsten Sendung gespielt.
                </p>
            </div>
            <div class="col-md-8 col-md-offset-2">
                <form method="POST" action="{{route('propose-song.store')}}">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-12"><input type="hidden" id="federal_state" value="">
                            {!! Form::text('club_name', isset($suggested->name) ? $suggested->name : null, ['class' => 'input club-autocomplete', 'data-clubs-url' => app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('api.club.autocomplete'), 'placeholder' => 'Name des Vereins', 'required']) !!}
                            <div class="col-xs-6 input-hint pull-right">
                                Nicht gefunden?
                                <a href="" class=""
                                   data-modal-url1="{{ route('user.club.contest.wizard')}}"
                                   data-modal1="team-member" id="clubwizard" data-token="{{ csrf_token() }}">
                                    Verein vorschlagen
                                </a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="input-group">
                                {!! Form::text('title_song', null, ['class' => 'input', 'placeholder' => 'Titel des Songs', 'required']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="input-group">
                                {!! Form::text('artist_name', null, ['class' => 'input', 'placeholder' => 'Interpret des Songs', 'required']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="input-group">
                                {!! Form::text('first_name', isset($user->firstname) ? $user->firstname : null, ['class' => 'input', isset($user->firstname) ? 'readonly' : '', 'placeholder' => 'Name', 'required']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="input-group">
                                {!! Form::text('last_name', isset($user->lastname) ? $user->lastname : null, ['class' => 'input', isset($user->lastname) ? 'readonly' : '', 'placeholder' => 'Vorname', 'required']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="input-group">
                                {!! Form::email('email', isset($user->email) ? $user->email : null, ['class' => 'input', isset($user->email) ? 'readonly' : '', 'placeholder' => 'E-Mail-Adresse', 'required']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group">
                                <input class="button button--light button--light-white button--center button--full-width" type="submit" value="Absenden">
                            </div>
                        </div>
                    </div>
                </form>
                {!! Form::close() !!}
            </div>
        </div>
    @else
        <div class="container">
            <div class="row">
                <h2 class="hl hl--large hl--underlined hl--inverted hl--centered hl--upper">
                    Du möchtest auch einen Song vorschlagen?
                </h2>
                <p class="text-block text-block--compressed text-block--thin">
                    Einfach in nur 2 Minuten ein neues Benutzerkonto anlegen oder mit deinen Benutzerdaten anmelden und dann das Gewinnspielformular abschicken.
                </p>
            </div>
        </div>
        <div class="container" align="center">

            <div class="col-xs-3"></div>
            <div class="col-md-3">
                <a href="/login" class="button button--center button--full-width button--light button--light-white">Anmelden</a>
            </div>
            <div class="col-md-3">
                <a href="/register" class="button button--center button--full-width button--light button--light-white">Benutzerkonto
                    anlegen</a>
            </div>
            <div class="col-xs-3"></div>
        </div>
    @endif
</div>
