@extends('layouts.master')
@section('title', 'vereinsleben.de - Euer Teambesprechungsraum - Gewinnt den coolsten Teambesprechungsraum für euren Verein')
@section('meta_description', 'Jetzt mitmachen und gewinnen, gewinnt den coolsten Teambesprechungsraum für euren Verein.')

@section('content')
    <div id="contest-app">

        <div class="section section--vote-white vote-winner__content" data-token="{{ csrf_token() }}">

            @if(Auth::check())

                <div class="container">
                    <h2 class="hl hl--large hl--underlined hl--inverted hl--centered hl--upper">
                        Vielen Dank für Deinen Vorschlag
                    </h2>
                    <p class="text-block text-block--compressed text-block--thin">
                        Vielen Dank, dass Du euren Lieblingssong als "{{$club}}" vorgeschlagen hast. Wir drücken Dir jetzt natürlich die Daumen,
                        dass Dein Vorschlag am nächsten Sonntag zwischen 16 und 18 Uhr für euren Verein gespielt wird.
                    </p>
                </div>
            @endif
        </div>

    </div>
@endsection
