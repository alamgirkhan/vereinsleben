@extends('layouts.master')
@section('title')
  Finde Vereine und Benutzer auf vereinsleben.de
@endsection

@section('content')

  <script src="https://cdn.stroeerdigitalgroup.de/metatag/live/OMS_vereinsleben/metaTag.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        SDG.Publisher.setZone('sports');
        SDG.Publisher.registerSlot('banner', 'vlStroeerDivId').load();
        SDG.Publisher.finalizeSlots();
    </script>
  <section class="club-search">
    {!! Form::open(['route' => 'club.search', 'method' => 'GET', 'id' => 'search', 'class' => '']) !!}
    <div class="container-fluid @if(isset($keyword)) search-result-area @else search-no-result @endif">
      <div class="container">
        <div class="row wirleben-section wirleben-section-search-area">
          <div class="col-lg-12">
            <h2>FINDE DEINEN VEREIN</h2>
            <h5 class="text-center">Finde den Verein, in dem du Mitglied bist oder suche nach einem neuen Verein</h5>
          </div>
        </div>
        <div class="row wirleben-section-search-area searchinputarea">
          <div class="col-lg-7 col-md-7 col-sm-6 col-centered">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-search"></i></span>
              {!! Form::text('keyword', isset($keyword) ? $keyword : null, ['id' => 'keyword', 'autofocus', 'class' => 'form-control', 'placeholder' => 'STICHWORT']) !!}
              <div class="clear"></div>
            </div>
          </div>
        </div>
        @if(!isset($keyword))
          <div class="row finden">
            <div class="col-lg-12">
              <a class="btn btn-default finden-btn" href="javascript:$( '#search' ).submit();">FINDEN</a>
            </div>
          </div>
        @endif
      </div>
      <div class="container" style="padding-top: 40px;">
          <div class="image-hor campaign-image" id="vlStroeerDivId" >

          </div>
      </div>
    </div>
    @if(isset($keyword))
      <div class="container search-result-list">
        <div class="row">
          <div class="col-12 state-right-search">
            <h2>SUCHERGEBNISSE</h2>
            <span class="club-search__results-headline">
               @if (count($clubs) > 0)
									<span class="club-search__results-headline--count">{{$clubTotal}}</span> Ergebnisse für <strong class="keyword">'{{$_GET['keyword']}}'</strong> gefunden
               @else
                  Es gibt keine Ergebnisse für die Suche
              @endif
            </span>
          </div>
        </div>
        @if (count($clubs) > 0)
          <div class="row">
            @foreach($clubs as $club)
              <div class="col-md-6">
                <div class="fan-box">
                  <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 club-result-thum">
                      <div class="thum-bg">
                        <a href="{{ route('club.detail', $club->slug) }}">
                          <img src="{{asset($club->avatar->url('startPage'))}}">
                        </a>
                      </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 fan-box-detail-lead">
                      <div class="fan-box-detail">
                        <h3><a href="{{ route('club.detail', $club->slug) }}" class="member-card__link">
                            {{ str_limit($club->name, $limit = 38, $end = '...') }}
                          </a>
                        </h3>
                        <div class="review-average">
                          @for ($i=1; $i <= 5 ; $i++)
                            <span class="star {{ ($i <= $club->average_rating) ? '' : 'no-active'}}">★</span>
                          @endfor

                        </div>
                      </div>
                      @if($club->city)
                        <div class="fan-lctn"><i class="fa fa-map-marker" aria-hidden="true"></i> {{ $club->city }}</div>
                      @endif
                      @if($club->allSportsString())
                        <div class="fan-likes">{{ str_limit($club->allSportsString(), $limit = 25, $end = '...') }}</div>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
        @endif

        @if ($clubs->hasPages())
          <ul class="pagination search-result">
            @if ($clubs->onFirstPage())
              <li class="disabled"><span>Vorherige</span></li>
            @else
              <li><a href="{{ $clubs->previousPageUrl() }}" rel="prev">Vorherige</a></li>
            @endif

            @if($clubs->currentPage() > 3)
              <li class="hidden-xs"><a href="{{ $clubs->url(1) }}">1</a></li>
            @endif
            @if($clubs->currentPage() > 4)
              <li><span>...</span></li>
            @endif
            @foreach(range(1, $clubs->lastPage()) as $i)
              @if($i >= $clubs->currentPage() - 2 && $i <= $clubs->currentPage() + 2)
                @if ($i == $clubs->currentPage())
                  <li class="active"><span>{{ $i }}</span></li>
                @else
                  <li><a href="{{ $clubs->url($i) }}">{{ $i }}</a></li>
                @endif
              @endif
            @endforeach
            @if($clubs->currentPage() < $clubs->lastPage() - 3)
              <li><span>...</span></li>
            @endif
            @if($clubs->currentPage() < $clubs->lastPage() - 2)
              <li class="hidden-xs"><a href="{{ $clubs->url($clubs->lastPage()) }}">{{ $clubs->lastPage() }}</a></li>
            @endif
            @if ($clubs->hasMorePages())
              <li><a href="{{ $clubs->nextPageUrl() }}" rel="next">Nächste</a></li>
            @else
              <li class="disabled"><span>Nächste</span></li>
            @endif
          </ul>
        @endif

      </div>
    @endif
    <script>
        window.setTimeout(function() {
            SDG.Publisher.loadAllSlots(true)
        },60000);
    </script>
    {!! Form::close() !!}
  </section>
@endsection