@extends('layouts.master')
@section('title')
    Finde Vereine und Benutzer auf vereinsleben.de
@endsection

@section('content')
    <section class="club-search">
        {!! Form::open(['route' => 'club.search', 'method' => 'GET', 'id' => 'search', 'class' => '']) !!}
        <div class="container-fluid @if($query['keyword']) search-result-area @else search-no-result @endif">
            <div class="container">
                <div class="row wirleben-section wirleben-section-search-area">
                    <div class="col-lg-12">
                        <h2>wir.leben.sport.</h2>
                        <h3><span>Finde auch</span> deinen Verein <span>und andere</span> Mitglieder
                            <span>oder werde</span> Fan
                        </h3>
                    </div>
                </div>
                <div class="row wirleben-section-search-area searchinputarea">
                    <div class="col-lg-8 col-sm-6 col-md-9 col-centered">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                            {!! Form::text('keyword', isset($query['keyword']) ? $query['keyword'] : null, ['id' => 'keyword', 'autofocus', 'class' => 'form-control', 'placeholder' => 'STICHWORT']) !!}
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
                @if(!$query['keyword'])
                    <div class="row finden">
                        <div class="col-lg-12">
                            <a class="btn btn-default finden-btn" href="javascript:$( '#search' ).submit();">FINDEN</a>
                        </div>
                    </div>
                @endif
            </div>
        </div>
        @if($query['keyword'] || $query['city'] || $query['sport'])
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                    <div class="row wirleben-section-search-area state-left-search">
                        <div class="col-lg-12"><span class="lhsheading">Ergebnisse Filtern</span></div>
                        <div class="col-lg-12">
                            <ul class="search-area-select">
                                <li><label><input type="radio" @if(Request::get('type') == 'personen') checked
                                                  @endif name="type" value="personen"/>Personen</label></li>
                                <li><label><input type="radio" @if(Request::get('type') == 'vereine') checked
                                                  @endif name="type" value="vereine"/>Vereine</label></li>
                                <li><label><input type="radio" @if(Request::get('type') == 'news') checked
                                                  @endif name="type" value="news"/>Neuigkeiten</label></li>
                                <li><label><input type="radio" @if(Request::get('type') == 'events') checked
                                                  @endif name="type" value="events"/>Ereignisse</label></li>
                            <!--<li><label><input type="radio" @if(Request::get('type') == 'verbande') checked @endif name="type" value="verbande" />Verbande</label></li>-->
                            </ul>
                        </div>
                        <div class="col-lg-12">
                            <div class="input-group ort">
								<span class="input-group-addon"><img
                                            src="{{asset('static_images/ort.png')}}"/></span>
                                {!! Form::text('city', Request::get('city'), ['class' => 'form-control', 'data-cities-url' => app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('api.cities'), 'placeholder' => 'ORT oder PLZ']) !!}
                            </div>
                        </div>
                        <div class="col-lg-12 km-select">
                            <div class="input-group">
								<span class="input-group-addon"><img
                                            src="{{asset('static_images/km.png')}}"/></span>
                                {!! Form::select('distance', ['5' => '5km', '10' => '10km', '15' => '15km', '25' => '25km', '50' => '50km'], isset($query['distance']) ? $query['distance'] : null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="input-group sportart">
                                <span class="input-group-addon"><img
                                            src="{{asset('static_images/sportart.png')}}"/></span>
                                {!! Form::text('sport', Request::get('sport'), ['class' => 'sport-autocomplete form-control', 'data-sports-url' => app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('api.sports'), 'placeholder' => 'SPORTART']) !!}
                            </div>
                        </div>
                        <div class="col-lg-12 finden">

                            <button type="submit"
                                    class="button button--center anwenden-btn">
                                <i class="fa fa-check"></i> FILTER ANWENDEN
                            </button>
                            <span class="undo"><a href="{{ url('suche/vereine') }}">Zur&uuml;cksetzen <i
                                            class="fa fa-undo"></i></a></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 state-right-search">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pdg-left">
                            <h2>Suchergebnisse</h2>
                            @if($clubCount || $userCount || $newsCount || $eventsCount || $bandageCount)
                                <span class="club-search__results-headline">
									<span class="club-search__results-headline--count">{{ $clubCount+$userCount+$newsCount+$eventsCount+$bandageCount }}</span> Ergebnisse fur <strong>'{{ $query['keyword'] }}
                                        '</strong> gefunden
							    </span>
                            @else
                                <p>Es gibt keine Ergebnisse für die Suche</p>
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="tabset">
                                @if($clubCount || $userCount || $newsCount || $eventsCount || $bandageCount)
                                    @if(Request::get('type') == 'personen' || Request::get('type') == '')
                                        @if($userCount)
                                            <input type="radio" checked name="user_type" id="tab7">
                                            <label for="tab7" onClick="goToByScroll('member')">Personen</label>
                                        @endif
                                    @endif

                                    @if(Request::get('type') == 'vereine' || Request::get('type') == '')
                                        @if($clubCount)
                                            <input type="radio" @if(Request::get('type') == 'vereine') checked
                                                   @endif name="user_type" id="tab5">
                                            <label for="tab5" onClick="goToByScroll('verein')">Vereine</label>
                                        @endif
                                    @endif

                                    @if(Request::get('type') == 'news' || Request::get('type') == '' && $newsCount != 0)
                                            @if($newsCount)
                                                <input type="radio" @if(Request::get('type') == 'news') checked
                                                       @endif name="user_type" id="tab3">
                                                <label for="tab5" onClick="goToByScroll('search-news-results')">Neuigkeiten</label>
                                            @endif
                                    @endif

                                    @if(Request::get('type') == 'events' || Request::get('type') == '' && $eventsCount != 0)
                                            @if($eventsCount)
                                                <input type="radio" @if(Request::get('type') == 'events') checked
                                                       @endif name="user_type" id="tab1">
                                                <label for="tab5" onClick="goToByScroll('search-events-results')">Events</label>
                                            @endif
                                    @endif

                                    <div class="tab-panels">
                                        @if(Request::get('type') == 'personen' || Request::get('type') == '')
                                            <section id="member">
                                                @if(isset($users))
                                                    @include('search.partials.result.user')
                                                @endif
                                            </section>
                                        @endif

                                        @if(Request::get('type') == 'vereine' || Request::get('type') == '')
                                            <div class="clear"></div>
                                            <section id="verein">
                                                @if(isset($clubs))
                                                    @include('search.partials.result.club')
                                                @endif
                                            </section>
                                        @endif

                                        @if(isset($bandages))
                                            <div class="clear"></div>
                                            <section id="verbande">
                                                @include('search.partials.result.bandage')
                                            </section>
                                        @endif

                                        @if(Request::get('type') == 'news' || Request::get('type') == '' && $newsCount != 0)
                                            <div class="clear"></div>
                                            <section id="search-news-results">
                                                @if(isset($news))
                                                    @include('search.partials.result.news')
                                                @endif
                                            </section>
                                        @endif

                                        @if(Request::get('type') == 'events' || Request::get('type') == '' && $eventsCount != 0)
                                            <div class="clear"></div>
                                            <section id="search-events-results">
                                                @if(isset($events))
                                                    @include('search.partials.result.events')
                                                @endif
                                            </section>
                                        @endif

                                        @if(Request::get('type') == 'verbande' || Request::get('type') == '')
                                            <div class="clear"></div>
                                        @endif
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
        {!! Form::close() !!}
    </section>
    @if (count($latest) > 0)
        @include('news.partials.latest-teaser', [
        'news' => $latest,
        'start' => 1,
        'end' => 3,
        'inverted' => true,
        ])
    @endif
@endsection