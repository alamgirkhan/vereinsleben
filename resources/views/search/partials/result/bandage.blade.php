@if($bandages->count() > 0)
    <h2 class="club-search__results-headline pdg-left">
        {{--<span class="club-search__results-headline--count">{{ $bandages->total() }}</span>--}}
        {{--{{ $bandages->total() === 1 ? 'Bandage' : 'Bandages' }} für Deine Suche gefunden--}}
        VERBÄNDE
    </h2>
    <div id="bandage_result">
        @each('search.partials.result.item.bandage', $bandages, 'bandage')
    </div>
    <div class="clear">
        <div>
            @if($bandages->total() > 6)
                <div id="bandage_more_result" class="col-md-12">
                    <div class="btn btn-default">
                        <a href="javascript:ajaxLoad('{{ $bandages->nextPageUrl() }}&ajax=1', 'bandage')"
                           class="interessiert-btn">
                            MEHR VERBÄNDE
                        </a>
                    </div>
                </div>
                <div class="col-md-12">
                    <div id="club_loader" style="display:none;" data-loader=""
                         class="loader__wrapper loader__wrapper--centered">
                        <div class="loader loader--loading"></div>
                        <div class="loader loader--loading"></div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endif