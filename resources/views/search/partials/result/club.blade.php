@if($clubs->count() > 0)
    <h2 class="club-search__results-headline pdg-left">VEREINE</h2>
    <div id="club_result">
        @each('search.partials.result.item.club', $clubs, 'club')
    </div>
    <div class="clear">
        <div>
            @if($clubs->total() > 6)
                <div id="club_more_result" class="col-md-12">
                    <div class="btn btn-default">
                        <a href="javascript:ajaxLoad('{{ $clubs->nextPageUrl() }}&ajax=1', 'club')" class="interessiert-btn">
                            MEHR VEREINE
                        </a>
                    </div>
                </div>
                <div class="col-md-12">
                    <div id="club_loader" style="display:none;" data-loader=""
                         class="loader__wrapper loader__wrapper--centered">
                        <div class="loader loader--loading"></div>
                        <div class="loader loader--loading"></div>
                    </div>
                </div>
        </div>
    </div>
@endif
@endif