@if($events->count() > 0)
    <h2 class="events-search__results-headline pdg-left">EVENTS</h2>
    <div id="events_result" class="col-lg-12 row row-eq-height" >
        @each('search.partials.result.item.event', $events, 'event')
    </div>
    <div class="clear">
        <div>
            @if($events->total() > 6)
                <div id="events_more_result" class="col-md-12">
                    <div class="btn btn-default">
                        <a href="javascript:ajaxLoad('{{ $events->nextPageUrl() }}&ajax=1', 'events')" class="interessiert-btn">
                            MEHR EVENTS
                        </a>
                    </div>
                </div>
                <div class="col-md-12">
                    <div id="events_loader" style="display:none;" data-loader=""
                         class="loader__wrapper loader__wrapper--centered">
                        <div class="loader loader--loading"></div>
                        <div class="loader loader--loading"></div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endif