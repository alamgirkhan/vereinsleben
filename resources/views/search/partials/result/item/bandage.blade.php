<div class="col-sm-6 col-md-6 col-xs-12 ">
    <div class="fan-box">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 club-result-thum">
                <div class="thum-bg">
                    <a href="{{ route('bandage.detail', ['slug' => $bandage->slug]) }}">
                        <img class="club-tile-wide__image"
                             src="{{ asset($bandage->avatar->url('startPage')) }}"
                             alt="{{ $bandage->name }}"/>
                    </a>
                </div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 fan-box-detail-lead">
                <div class="fan-box-detail">
                    <h3><a href="{{ route('bandage.detail', $bandage->slug) }}" class="member-card__link">
                            {{ $bandage->name }}
                        </a>
                    </h3>
                </div>
                <div class="fan-lctn"><i class="fa fa-map-marker" aria-hidden="true"></i> {{ $bandage->city }}</div>
                <div class="fan-likes">{{$bandage->sports()->first()->title}}</div>
            </div>
        </div>
    </div>
</div>