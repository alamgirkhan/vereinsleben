<div class="col-xs-12 col-sm-12 col-md-12 fan-box-main">
    <div class="fan-box news-box-search">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4 news-result-thum">
                <div class="thum-bg col-xs-12 col-lg-12 col-md-12 col-sm-12">
                    <a href="{{ route('news.detail', ['id' => $single_news->id, 'slug' => $single_news->slug]) }}">
                        @if ($single_news->main_image->originalFilename() !== null)
                                <img class="col-xs-12 col-lg-12 col-md-12 col-sm-12 no-padding"
                                     src="{{ asset($single_news->main_image->url('singleview')) }}"
                                     alt="@if(isset($single_news->meta->meta_bild_alt )){{ $single_news->meta->meta_bild_alt }}@endif">
                        @endif
                    </a>
                </div>
            </div>
            <div class="col-lg-9 col-md-8 col-sm-8 col-xs-8 fan-box-detail">
                <h3 class="col-xs-12 col-lg-12 col-md-12 col-sm-12">
                    <a href="{{ route('news.detail', ['id' => $single_news->id, 'slug' => $single_news->slug]) }}" class="member-card__link">
                        {{ str_limit($single_news->title, $limit = 25, $end = '...') }}
                    </a>
                </h3>
                <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 news-item-teaser no-padding">
                    {{ str_limit($single_news->content_teaser, $limit = 500, $end = '...') }}
                </div>
{{--                <div class="fan-likes">{{ str_limit($single_news->allSportsString(), $limit = 25, $end = '...') }}</div>--}}
            </div>
        </div>
    </div>
</div>