<div class="col-xs-12 col-sm-6 col-md-6 fan-box-main">
    <div class="fan-box">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 club-result-thum">
                <div class="thum-bg">
                    <a href="{{ route('user.detail', $user->username) }}">
                        <img src="{{ $user->avatar->url('singleView') }}"/>
                    </a>
                </div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 fan-box-detail-lead">
                <div class="fan-box-detail">
                    <h3><a href="{{ route('user.detail', $user->username) }}" class="member-card__link">
                            {{ str_limit($user->fullName(), $limit = 25, $end = '...') }}                           
                        </a>
                    </h3>
                </div>

                @if(isset($user->city) && trim($user->city) !== '')
                    <div class="fan-lctn"><i class="fa fa-map-marker" aria-hidden="true"></i> {{ $user->city }}</div>
                @endif

                @if($user->sports->count() > 0)
                    <div class="fan-likes">
                        {{ str_limit($user->sports->take(10)->implode('title', ', '), $limit = 22, $end = '...') }}
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>