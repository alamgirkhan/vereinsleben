<div id="{{ $type }}_more_result" class="col-md-12">
    <div class="button__center-wrapper">
        <a href="javascript:ajaxLoad('{{ $more->nextPageUrl() }}', '{{ $type }}')"
           class="btn btn-default interessiert-btn">
            MEHR {{ $name }}
        </a>
    </div>
</div>