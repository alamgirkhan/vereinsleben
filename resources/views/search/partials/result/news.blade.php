@if($news->count() > 0)
    <h2 class="news-search__results-headline pdg-left">NEUIGKEITEN</h2>
    <div id="news_result" class="col-lg-12 row row-eq-height" >
        @each('search.partials.result.item.news', $news, 'single_news')
    </div>
    <div class="clear">
        <div>
            @if($news->total() > 6)
                <div id="news_more_result" class="col-md-12">
                    <div class="btn btn-default">
                        <a href="javascript:ajaxLoad('{{ $news->nextPageUrl() }}&ajax=1', 'news')" class="interessiert-btn">
                            MEHR NEUIGKEITEN
                        </a>
                    </div>
                </div>
                <div class="col-md-12">
                    <div id="news_loader" style="display:none;" data-loader=""
                         class="loader__wrapper loader__wrapper--centered">
                        <div class="loader loader--loading"></div>
                        <div class="loader loader--loading"></div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endif