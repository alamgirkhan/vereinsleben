@if($users->total() > 0)
    <h2 class="club-search__results-headline pdg-left">PERSONEN</h2>
    <div id="user_result">
        @each('search.partials.result.item.user', $users, 'user')
    </div>
    <div class="clear">
        <div>
            @if($users->total() > 6)
                <div id="user_more_result" class="col-md-12">
                    <div class="btn btn-default">
                        <a href="javascript:ajaxLoad('{{ $users->nextPageUrl() }}&ajax=1', 'user')" class="interessiert-btn">
                            MEHR PERSONEN
                        </a>
                    </div>
                </div>
                <div class="col-md-12">
                    <div id="user_loader" style="display:none;" data-loader=""
                         class="loader__wrapper loader__wrapper--centered">
                        <div class="loader loader--loading"></div>
                        <div class="loader loader--loading"></div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endif