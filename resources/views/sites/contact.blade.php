@extends('layouts.master')
@section('title')
    Kontakt - vereinsleben.de
@endsection

@section('content')
    @include('components.slider',
    ['condensed' => true,
     'content' => [
            'headline' => 'Kontakt',
    ]])
    <?php
        $email = auth() && auth()->user() ? auth()->user()->email : '';
        $street = auth() && auth()->user() ? auth()->user()->street : '';
        $firstname = auth() && auth()->user() ? auth()->user()->firstname : '';
        $lastname = auth() && auth()->user() ? auth()->user()->lastname : '';
        $phone = auth() && auth()->user() ? auth()->user()->phone : '';
        $city = auth() && auth()->user() ? auth()->user()->city : '';
        $zip = auth() && auth()->user() ? auth()->user()->zip : '';
    ?>
    <div class="container section__content">
        <div class="row">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                {!! Form::open(
                    [
                        'route' => 'contact',
                        'enctype' => 'multipart/form-data',
                        'files' => true,
                        'method' => 'POST',
                        'class' => 'form-horizontal'
                    ]
                )!!}
                <div class="col-sm-12">
                    <p><b>Du hast eine Frage oder Anregung zu vereinsleben.de? </b><br>
                        Dann schicke uns einfach eine Nachricht über das Kontaktformular, eine E-Mail direkt an <a
                                href="mailto:info@vereinsleben.de">info@vereinsleben
                        .de</a>
                        oder rufe unter der 0621-59000-115 bei uns an.</p><br/>
                </div>
                <div class="col-sm-12">
                    {!! Form::label('firma', 'Firma', ['class' => 'control-label col-sm-3']) !!}
                    <div class="input-group col-sm-9">
                        {!! Form::text('firma', null, ['class' => 'input', 'placeholder' => 'Firma']) !!}
                    </div>
                </div>

                <div class="col-sm-12">
                    {!! Form::label('name', 'Name *', ['class' => 'control-label col-sm-3']) !!}
                    <div class="input-group col-sm-9">
                        {!! Form::text('name', $lastname, ['class' => 'input', 'placeholder' => 'Name', 'required']) !!}
                    </div>
                </div>

                <div class="col-sm-12">
                    {!! Form::label('vorname', 'Vorname *', ['class' => 'control-label col-sm-3']) !!}
                    <div class="input-group col-sm-9">
                        {!! Form::text('vorname', $firstname, ['class' => 'input', 'placeholder' => 'Vorname', 'required']) !!}
                    </div>
                </div>

                <div class="col-sm-12">
                    {!! Form::label('Strasse', 'Straße', ['class' => 'control-label col-sm-3']) !!}
                    <div class="input-group col-sm-9">
                        {!! Form::text('strasse', $street, ['class' => 'input', 'placeholder' => 'Strasse']) !!}
                    </div>
                </div>

                <div class="col-sm-12">
                    {!! Form::label('Ort', 'Ort', ['class' => 'control-label col-sm-3']) !!}
                    <div class="input-group col-sm-9">
                        {!! Form::text('ort', $city, ['class' => 'input', 'placeholder' => 'Ort']) !!}
                    </div>
                </div>

                <div class="col-sm-12">
                    {!! Form::label('plz', 'PLZ', ['class' => 'control-label col-sm-3']) !!}
                    <div class="input-group col-sm-9">
                        {!! Form::text('plz', $zip, ['class' => 'input', 'placeholder' => 'PLZ']) !!}
                    </div>
                </div>

                <div class="col-sm-12">
                    {!! Form::label('bundesland', 'Bundesland', ['class' => 'control-label col-sm-3']) !!}
                    <div class="input-group col-sm-9">
                        {!! Form::select('bundesland', array('' => 'Bitte wählen')+$states, $state_id, ['required', 'class' => 'select', 'id' => 'wstate']) !!}
                    </div>
                </div>

                <div class="col-sm-12">
                    {!! Form::label('Telefon', 'Telefon', ['class' => 'control-label col-sm-3']) !!}
                    <div class="input-group col-sm-9">
                        {!! Form::text('telefon', $phone, ['class' => 'input', 'placeholder' => 'Telefon']) !!}
                    </div>
                </div>

                <div class="col-sm-12">
                    {!! Form::label('email', 'Email *', ['class' => 'control-label col-sm-3']) !!}
                    <div class="input-group col-sm-9">
                        {!! Form::text('email', $email, ['class' => 'input', 'placeholder' => 'Email', 'required']) !!}
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="input-group col-sm-3"></div>
                    {{--<div class="input-group col-sm-9">--}}
                    {{--{{ Form::checkbox('bksmt','Yes') }}--}}
                    {{--{!! Form::label('bksmt', 'Bitte kontaktieren Sie mich telefonisch') !!}--}}
                    {{--</div>--}}
                </div>

                <div class="col-sm-12">
                    {!! Form::label('kommentare', 'Ihre Nachricht *', ['class' => 'control-label col-sm-3']) !!}
                    <div class="input-group col-sm-9">
                        {{ Form::textarea('message',null,['class' => 'input', 'placeholder' => 'Ihre Nachricht', 'required']) }}
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="input-group">
                        {!! Recaptcha::render() !!}
                    </div>
                </div>

                <div class="col-sm-12">
                    <p>Mit Absendung Ihrer Daten erklären Sie Ihre Einwilligung darin, dass wir diese in dem in unseren
                        <a class="input-checkbox__label-link" href="{{ url('/content/datenschutz') }}">Datenschutzhinweisen</a>
                        genannten Umfang zum Zweck der Bearbeitung Ihrer Anfrage verarbeiten und
                        Sie auf demselben Kommunikationsweg kontaktieren dürfen.
                        Diese Daten werden aufgrund berechtigten Interesses verarbeitet und dann
                        gelöscht, wenn Ihre Anfrage erkennbar endgültig erledigt ist, die Einwilligung
                        widerrufen wird bzw.
                        Sie wirksam der weiteren Verarbeitung Ihrer Daten durch uns widersprechen.
                        Weitere Informationen dazu erhalten Sie in unseren
                        <a class="input-checkbox__label-link" href="{{ url('/content/datenschutz') }}">Datenschutzhinweisen</a>.
                    </p><br/>
                </div>


                {!! Form::submit('Absenden', array('class' => 'button button--light button--light-white button--center button--full-width')) !!}

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
