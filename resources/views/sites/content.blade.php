@extends('layouts.master')
@section('title')
    {{ $content->title }} - vereinsleben.de
@endsection

@section('content')
    @include('components.slider',
    ['condensed' => true,
     'content' => [
            'headline' => $content->title,
    ]])
    <div class="container section__content">
        <div class="row">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                {!! $content->content !!}
            </div>
        </div>
    </div>
@endsection