@extends('layouts.master')
@section('title')
    FAQ - Häufig gestellte Fragen - vereinsleben.de
@endsection

@section('content')
    @include('components.slider',
    ['condensed' => true,
     'content' => [
            'headline' => 'FAQ',
    ]])
    <div class="container section__content">
        <div class="row">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <p>
                    <strong>1. Wie registriere ich mich als Mitglied bei vereinsleben.de und wie melde ich meinen Verein
                        an?</strong>
                </p>
                <p class="section__content-text">
                    Um dich zu registrieren, klicke oben rechts in der Leiste auf „Registrieren“. Fülle dann Schritt für
                    Schritt
                    die
                    Felder aus. Wenn du dich erfolgreich registriert hast, hast du die Möglichkeit entweder deinen
                    Verein zu
                    anzulegen oder nach deinem Verein zu suchen. Wenn du bereits registriert bist, klicke einfach oben
                    rechts in
                    der
                    Leiste auf „Anmelden“. Möchtest du einen neuen Verein erstellen, klicke oben rechts in der Leiste
                    auf
                    deinen
                    Benutzernamen und dann auf „Meine Vereine“. Dort hast du dann die Möglichkeit einen neuen Verein
                    anzulegen.

                    WICHITG: Bitte achte darauf, dass du die Vereinsinformationen vollständig ausfüllst. Speziell die
                    Geodaten
                    (Straße, PLZ und Ort) sowie die Sportarten, die dein Verein anbietet, sollten ausgefüllt sein. Denn
                    nur
                    so
                    können wir sicherstellen, dass dein Verein auch von allen gefunden wird.
                </p>

                <p class="section__content-text">
                    <strong>2. Muss ich mich zuerst als Mitglied registrieren, bevor ich meinen Verein anmelden
                        kann?</strong>
                </p>
                <p class="section__content-text">
                    Ja. Gehe die Registrierung Schritt für Schritt durch. Nach der Registrierung hast du die Möglichkeit
                    entweder
                    deinen Verein zu erstellen oder nach deinem Verein zu suchen, falls dieser schon angelegt ist.
                </p>

                <p class="section__content-text">
                    <strong>3. Ich habe mein Passwort vergessen. Was nun?</strong>
                </p>

                <p class="section__content-text">
                    Sende uns einfach eine E-Mail an info@vereinsleben.de mit Angabe deines Benutzernamen und deiner
                    hinterlegten
                    E-Mail-Adresse
                </p>

                <p class="section__content-text">
                    <strong>4. Wie bearbeite ich mein Vereinsprofil?</strong>
                </p>

                <p class="section__content-text">
                    Logge Dich bitte auf der Startseite rechts oben ein. Klicke dann auf deinen Username und gehe auf
                    "Meine
                    Vereine". Dort kannst du nun den Verein, den du bearbeiten möchtest auswählen. Danach gelangst du
                    auf
                    deine
                    Vereinsprofil-Seite. Dort findest du den Button „Bearbeiten“. Klicke einfach darauf und du bekommst
                    angezeigt,
                    was du alles Bearbeiten kannst.
                </p>

                <p class="section__content-text">
                    <strong>5. Kann ich meinen Benutzernamen ändern?</strong>
                </p>

                <p class="section__content-text">
                    Nein. Der Benutzername kann nicht geändert werden, da dieser für die Registrierung notwendig und mit
                    dieser
                    gekoppelt ist.
                </p>

                <p class="section__content-text">
                    <strong>6. Kann ich meine E-Mail Adresse ändern?</strong>
                </p>

                <p class="section__content-text">
                    Nein. Die E-Mail Adresse kann nicht geändert werden, da diese für die Registrierung notwendig ist
                    und
                    ebenfalls
                    mit dieser gekoppelt ist.
                </p>

                <p class="section__content-text">
                    <strong>7. Wie logge ich mich aus?</strong>
                </p>

                <p class="section__content-text">
                    Klicke dazu einfach rechts oben auf deinen Benutzername und dann auf „Abmelden“.
                </p>

                <p class="section__content-text">
                    <strong>8. Warum kann ich meinen Verein nicht über die Suche finden?</strong>
                </p>

                <p class="section__content-text">
                    Bitte stelle sicher, dass in Deinem Vereinsprofil die Anschrift des Vereins (PLZ und Ort) sowie die
                    Sportart
                    eingetragen ist. Die PLZ sowie die Kategorie (Sportart) sind für die Suche notwendig.
                </p>
            </div>
        </div>
    </div>
@endsection