@extends('layouts.master')
@section('title')
    Impressum - vereinsleben.de
@endsection

@section('content')
    @include('components.slider',
    ['condensed' => true,
     'content' => [
            'headline' => 'Impressum',
    ]])
{{--    <div class="container section__content">--}}
{{--        <div class="row">--}}
{{--            <div class="col-xs-12 col-md-8 col-md-offset-2">--}}
{{--                <p class="section__content-text"><strong>Rheinland-Pfälzische Rundfunk GmbH & Co. KG</strong><br/>Turmstraße 10<br/>67059 Ludwigshafen</p>--}}
{{--                <p class="section__content-text">Telefon: +49 (0) 621 / 59000 - 0<br/>Telefax: +49 (0) 621 / 622750</p>--}}
{{--                <p class="section__content-text">E-Mail: <a href="mailto:info@rpr1.de">info@rpr1.de</a></p>--}}
{{--                <p class="section__content-text">Registergericht Ludwigshafen, HRA 3373<br/>USt-ID: DE 149 116 004</p>--}}
{{--                <p class="section__content-text">Vertretungsberechtigung und persönlich haftende Gesellschafterin:<br/>--}}
{{--                    Rheinland-Pfälzische Rundfunkbetriebsgesellschaft mbH<br/>--}}
{{--                    Geschäftsführer: Tobias Heger, Kristian Kropp<br/>--}}
{{--                    Beiratsvorsitzender: Dr. Oliver C. Dubber<br/>--}}
{{--                    Registergericht Ludwigshafen, HRB 3082<br/>--}}
{{--                    <a href="mailto:info@rpr1.de">info@rpr1.de</a>--}}
{{--                </p>--}}
{{--                <p class="section__content-text">Vertretungsberechtigter gemäß §5 Abs. 1 TMG sowie Verantwortlicher nach §55 Abs. 2 RStV:<br/>Kristian--}}
{{--                    Kropp</p>--}}
{{--                <p class="section__content-text">Weitere Verantwortliche nach §55 Abs. 2 RStV:<br/>Das Internet-Angebot von RPR1. wird durch die--}}
{{--                    Internet-Redaktion gestaltet. Diese ist für den aktuellen themenbezogenen Teil des Angebots--}}
{{--                    zuständig.</p>--}}
{{--                <p class="section__content-text">Leitung: Aleksandar Rustemovski</p>--}}
{{--                <p class="section__content-text">Information zur Verbraucherstreitbeilegung<br/>--}}
{{--                    Die Europäische Kommission stellt eine Plattform zur Online-Streitbeilegung (OS) bereit, die Sie unter <a href="https://webgate.ec.europa.eu/odr/" target="_blank">https://webgate.ec.europa.eu/odr/</a> finden. Zur Teilnahme an einem Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle ist die Rheinland-Pfälzische Rundfunk GmbH & Co. KG nicht verpflichtet und nicht bereit.</p>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

    @if(isset($imprint))
        <div>{!! $imprint->content !!}</div>
    @endif
@endsection
