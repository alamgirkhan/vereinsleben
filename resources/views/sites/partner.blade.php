@extends('layouts.master')
@section('title')
    Partner - vereinsleben.de
@endsection

@section('content')
    @include('components.slider',
    ['condensed' => true,
     'content' => [
            'headline' => 'Partner',
    ]])
    <div class="container section__content">
        <div class="row">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <h3>Partner</h3>

                <h3>1.1 Partner und Sponsoren</h3>
                <p class="section__content-text">
                    Für jedes Bundesland sind unterschiedliche Partner und Sponsoren möglich. Welche Partner
                    angezeigt werden, hängt vom Bundesland ab. Dies wird entweder entweder über IP-Adresse
                    ausgelesen, durch die Angaben im User-Profil bestimmt (siehe 2.1.3.1) oder durch einen evtl.
                    gesetzten Cookie (siehe 1.2.1.2.).
                </p>

                <h4>1.1.1 Typen von Partnern und Platzierung</h4>
                <p class="section__content-text">
                    Der jeweilige Premiumpartner soll im Headerbereich (wie jetzt) dargestellt werden.
                    Die weiteren Partner sollen im Footerbereich eingebunden werden.
                </p>

                <h4>1.1.2 Unterseite für Partner</h4>
                <p class="section__content-text">
                    Zusätzlich gibt es eine Seite „Partner und Sponsoren“, auf der sämtliche Partner und
                    Sponsoren geordnet nach folgenden Rubriken dargestellt sind:
                </p>

                <p>&rarr; Premiumpartner</p>
                <p>&rarr; Exklusivpartner</p>
                <p>&rarr; Medienpartner</p>
                <p>&rarr; Sportpartner</p>
                <p>&rarr; Servicepartner</p>
                <br/>
                <p class="section__content-text">
                    Jeder Partner hat eine eigene Unterseite, erreichbar über Klick auf das Logo, egal ob im
                    Header, im Footer oder auf der Seite „Partner und Sponsoren“. Die Unterseite ist prinzipiell
                    wie ein normaler Artikel aufgebaut, kann Headline, Anleser, Text, Bild, Video enthalten.
                    In der rechten Spalte lassen sich das Kontaktdaten, sowie Bild und Informationen zum
                    Ansprechpartner unterbringen. Trotz des einem Artikel ähnlichen Aufbaus, sollte sich das
                    Partnerprofil von einem Artikel optisch unterscheiden und wichtig ist, dass es einen
                    speziellen Platz für das Firmenlogo gibt.
                </p>

                <p class="section__content-text">
                    Die Verwaltung der Partner erfolgt über den Administrationsbereich. Hier können Partner
                    hinzugefügt, bearbeitet (Name, Logos usw.) oder gelöscht werden sowie den Bundesländern und
                    Kategorien zugeordnet werden. Alle für die Partner erforderlichen Logoversionen
                    (klein, mittel, groß) können hier hinterlegt werden.
                </p>
            </div>
        </div>
    </div>
@endsection