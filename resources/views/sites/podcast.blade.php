@extends('layouts.master')
@section('title')
    Podcast - vereinsleben.de
@endsection

@section('content')
    @include('components.slider',
    ['condensed' => true,
     'content' => [
            'headline' => 'Podcast',
    ]])
    <div class="container section__content">
        <div class="row">
            <iframe src="https://meinsportpodcast.de/fussball/?link=iframe&size=xl&id=vereinsleben" width="847" height="950" frameborder="0" marginwidth="0" marginheight="0"></iframe>
        </div>
    </div>
@endsection
