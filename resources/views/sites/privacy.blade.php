@extends('layouts.master')
@section('title')
    Datenschutz - vereinsleben.de
@endsection

@section('content')
    @include('components.slider',
    ['condensed' => true,
     'content' => [
            'headline' => 'Datenschutz',
    ]])
    <div class="container section__content">
        <div class="row">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <h3>Datenschutzbeauftragter</h3>
                <p class="section__content-text">

                </p>
                <h3>Cookiehinweis</h3>
                <p class="section__content-text">

                </p>
                <h3>Speicherung</h3>
                <p class="section__content-text">

                </p>
            </div>
        </div>
    </div>
@endsection