@extends('layouts.master')
@section('title')
    Allgemeine Geschäftsbedingungen - AGBs - vereinsleben.de
@endsection

@section('content')
    @include('components.slider',
    ['condensed' => true,
     'content' => [
            'headline' => 'Allgemeine Geschäftsbedingungen',
    ]])
    <div class="container section__content">
        <div class="row">
            <div class="col-xs-12 col-md-8 col-md-offset-2">

                <h3>Allgemeine Geschäftsbedingungen</h3>
                <h4>Allgemeine Geschäftsbedingungen und Nutzungsbestimmungen des Webportals www.vereinsleben.de</h4>

                <p class="section__content-text">Die Rheinland-Pf&auml;lzische Rundfunk GmbH &amp; Co. KG (nachfolgend
                    RPR1.
                    genannt) mit Sitz in
                    Ludwigshafen, Turmstra&szlig;e 10, bietet unter dem Namen <a
                            title="vereinsleben.de - wir. leben. sport"
                            href="//www.vereinsleben.de">www.vereinsleben.de</a>
                    ein Netzwerk an, um &nbsp;Vereinen aus Rheinland Pfalz die M&ouml;glichkeit zu er&ouml;ffnen, Ihren
                    Verein, seine Aktivit&auml;ten und Veranstaltungen kostenlos einer breiten &Ouml;ffentlichkeit
                    vorzustellen. In den nachfolgenden Allgemeinen Gesch&auml;ftsbedingungen wird die Nutzung dieses
                    Netzwerkes geregelt.</p>
                <h3>1 Geltungsbereich</h3>
                <p class="section__content-text"><strong>1.1</strong> RPR1. erbringt alle Leistungen im Rahmen des von
                    ihr
                    angebotenen Webportals
                    www.vereinsleben.de ausschlie&szlig;lich auf Grundlage dieser AGB. Abweichende, entgegenstehende
                    und/oder erg&auml;nzende AGB werden, selbst bei Kenntnis von RPR1., nicht Vertragsbestandteil, es
                    sei
                    denn RPR1. hat ihrer Geltung ausdr&uuml;cklich schriftlich zugestimmt.</p>
                <p class="section__content-text">RPR1. ist berechtigt, diese AGB mit einer Ank&uuml;ndigungsfrist von
                    vier
                    Wochen zu &auml;ndern und/oder
                    zu erg&auml;nzen. Die Ank&uuml;ndigung erfolgt ausschlie&szlig;lich durch Ver&ouml;ffentlichung im
                    Internet unter www.vereinsleben.de. Der Nutzer hat die M&ouml;glichkeit, den ge&auml;nderten
                    und/oder
                    erg&auml;nzten AGB innerhalb von vier Wochen nach deren Ver&ouml;ffentlichung im Internet
                    zuzustimmen.
                    Die sich ergebenden &Auml;nderungen und/oder Erg&auml;nzungen treten mit dem Datum der Zustimmung,
                    sp&auml;testens
                    jedoch vier Wochen nach Ver&ouml;ffentlichung im Internet, in Kraft. Bleibt die Zustimmung innerhalb
                    dieser vier Wochen aus, endet das Vertragsverh&auml;ltnis mit sofortiger Wirkung mit Ablauf der vier
                    Wochen.</p>
                <h3>2 Vertragsgegenstand</h3>
                <p class="section__content-text">Mit der Nutzung der Dienste des Webportals www.vereinsleben.de erh&auml;lt
                    der Nutzer die M&ouml;glichkeit,
                    folgende Inhalte zu ver&ouml;ffentlichen, hoch zu laden, zu speichern und zum Abruf bereit zu
                    halten:
                    <br/> <strong>1.</strong> Textbeitr&auml;ge (allgemeine Informationen zum Verein, Aktuelles,
                    Vereinsnachrichten etc.)
                    <br/> <strong>2.</strong> Informationen zu Veranstaltungen (Konzerte, Parties usw.) mit Terminen,
                    Veranstaltungsorten, Beschreibungstexten etc.
                    <br/> <strong>3.</strong> Bilddaten (digitale Fotos, Grafiken etc.) und das Verkn&uuml;pfen der
                    Bilder
                    mit den o.g. Informationen </p>
                <h3>3. Nutzerkreis</h3>
                <p class="section__content-text">Die kostenlosen Dienste des Portals <a
                            title="vereinsleben.de - wir. leben. sport"
                            href="//www.vereinsleben.de">www.vereinsleben.de</a>
                    stehen jeder nat&uuml;rlichen Person ab Vollendung des 14. Lebensjahres nach entsprechender
                    Anmeldung
                    und Registrierung zur Verf&uuml;gung. Auch juristische Personen k&ouml;nnen nach Anmeldung und
                    Registrierung die Dienste des Webportals nutzen. RPR1. hat das Recht, einzelne nat&uuml;rliche oder
                    juristische Personen von der Nutzung auszuschlie&szlig;en. Von diesem Recht wird RPR1. insbesondere
                    dann
                    Gebrauch machen, wenn bereits im Rahmen des Nutzerantrags oder der sich anschlie&szlig;enden
                    Registrierung Gr&uuml;nde erkennbar sind, die ernsthaft bef&uuml;rchten lassen, dass der Nutzer die
                    von
                    RPR1. angebotenen Leistungen nicht vertragsgem&auml;&szlig; im Sinne dieser AGB nutzen wird. Dies
                    ist
                    insbesondere dann der Fall, wenn mit dem jeweiligen Nutzer bereits in der Vergangenheit ein
                    Vertragsverh&auml;ltnis
                    mit RPR1. bestand, das jedoch aufgrund von Vertragsverletzungen und/oder Regelversto&szlig; au&szlig;erordentlich
                    durch RPR1. gek&uuml;ndigt wurde.</p>
                <h3>4. Zustandekommen des Vertrags/Anmeldung und Registrierung</h3>
                <p class="section__content-text"><strong>4.1</strong> Voraussetzungen f&uuml;r die vollumf&auml;ngliche
                    Nutzung der Dienste des Webportals
                    von <a title="vereinsleben.de - wir. leben. sport" href="http://www.vereinsleben.de">www.vereinsleben.de'}</a>
                    sind die Anmeldung und Registrierung des Nutzers.</p>
                <p class="section__content-text">4.2 Die Anmeldung erfolgt durch elektronische Erkl&auml;rung des
                    Nutzers
                    (Nutzerantrag) auf der
                    Startseite unter <a title="vereinsleben.de - wir. leben. sport" href="http://www.vereinsleben.de">www.vereinsleben.de</a>.
                    Im Rahmen der Anmeldung w&auml;hlt der Nutzer einen Benutzernamen sowie ein geeignetes Passwort aus,
                    unter dem der k&uuml;nftige Zugang zu den Diensten des Webportals <a
                            title="vereinsleben.de - wir. leben. sport"
                            href="//www.vereinsleben.de">www.vereinsleben.de</a>
                    erfolgen soll. Der Zugang zur Anmeldung wird dem Nutzer unverz&uuml;glich auf elektronischem Weg
                    best&auml;tigt.
                    Die Zugangsbest&auml;tigung stellt zugleich die Annahme des Nuterzantrags dar. Diese erfolgt auf
                    elektronischem Weg. Gleichzeitig ergeht die Aufforderung an den Nutzer zur abschlie&szlig;enden
                    Registrierung mit dem von ihm gew&auml;hlten Benutzernamen/Passwort. Ein Anspruch des Nutzers auf
                    Zuteilung eines bestimmten Benutzernamens, insbesondere seines nat&uuml;rlichen Namens, besteht
                    nicht.
                    RPR1. ist bem&uuml;ht, den vom Nutzer ausgew&auml;hlten Benutzernamen an diesen zu vergeben, kann
                    dieses
                    jedoch ausdr&uuml;cklich nicht garantieren. Bei &Uuml;berschneidungen mit dem Benutzernamen anderer
                    Nutzer gilt das Priorit&auml;tsprinzip, wobei der Zeitpunkt des Zugangs des Nutzerantrags bei RPR1.
                    ma&szlig;geblich
                    ist. Anspr&uuml;che des Nutzers gegen RPR1. wegen der Nichtvergabe eines nat&uuml;rlichen Namens als
                    Benutzername, insbesondere wegen etwaiger Verletzungen des Namensrechts im Sinne des B&uuml;rgerlichen
                    Gesetzbuches bestehen nicht.</p>
                <p class="section__content-text"><strong>4.3</strong> Der registrierte Benutzername darf mit Form,
                    Inhalt
                    und Zweck nicht gegen
                    gesetzliche Verbote, insbesondere Rechte Dritter, versto&szlig;en. Erlangt RPR1. Kenntnis von einem
                    Versto&szlig;, so ist RPR1. berechtigt den Benutzernamen zu sperren und gespeicherte Inhalte &ndash;
                    nach vorheriger Mitteilung an den Nutzer &ndash; zu l&ouml;schen. Ferner ist RPR1. dann berechtigt,
                    die
                    vertraglichen Leistungen &ndash; auch &ndash; sofort einzustellen und das Nutzungsverh&auml;ltnis au&szlig;erordentlich
                    zu k&uuml;ndigen (s.u. Punkt 10.3).</p>
                <p class="section__content-text"><strong>4.4</strong> Der Nutzer sichert zu, dass die von ihm im Rahmen
                    des
                    Nutzerantrags und/oder der
                    Registrierung gemachten Angaben &uuml;ber seine Person bzw. das Unternehmen/die Institution bei
                    juristischen Personen und sonstige vertragsrelevante Umst&auml;nde vollst&auml;ndig und richtig
                    sind.&nbsp;
                </p>
                <h3>5. Widerrufsrecht</h3>
                <p class="section__content-text"><strong>5.1</strong> Der Nutzer hat das Recht, seine auf Abschluss des
                    Vertrages gerichtete Willenserkl&auml;rung
                    (Nutzerantrag) zu widerrufen. Die Widerrufsfrist betr&auml;gt zwei Wochen. Sie beginnt mit Erhalt
                    einer
                    Belehrung &uuml;ber das Widerrufsrecht und Erf&uuml;llung der gesetzlichen Informationspflichten.
                </p>
                <p class="section__content-text"><strong>5.2</strong> Zur Fristwahrung gen&uuml;gt die rechtzeitige
                    Absendung der Widerrufserkl&auml;rung.
                    Die Erkl&auml;rung bedarf keiner Begr&uuml;ndung. Sie kann gegen&uuml;ber RPR1. elektronisch
                    abgegeben
                    werden an folgende E-Mail-Adresse: info@vereinsleben.de .</p>
                <h3>6. Rechte und Pflichten des Nutzers</h3>
                <p class="section__content-text"><strong>6.1</strong> Der Nutzer tr&auml;gt die alleinige Verantwortung
                    f&uuml;r
                    s&auml;mtliche von ihm
                    eingestellten Inhalte und/oder Aktivit&auml;ten im Rahmen der Nutzung der Dienste von <a
                            title="vereinsleben.de - wir. leben. sport"
                            href="//www.vereinsleben.de">www.vereinsleben.de</a>.
                    Er darf die Dienste von <a title="vereinsleben.de - wir. leben. sport"
                                               href="//www.vereinsleben.de">www.vereinsleben.de</a>
                    nicht missbr&auml;uchlich nutzen, insbesondere nicht vors&auml;tzlich oder fahrl&auml;ssig zur
                    rechtswidrigen &Uuml;bermittlung und/ oder Verbreitung von Mitteilungen, Daten und Inhalten
                    beitragen
                    und/oder diese nicht bewusst betreiben.</p>
                <p class="section__content-text"><strong>6.2</strong> Der Nutzer verpflichtet sich, die Rechtsordnung
                    der
                    Bundesrepublik Deutschland zu
                    beachten und einzuhalten. Er verpflichtet sich insbesondere, die von ihm in Anspruch genommenen
                    Dienste
                    von RPR1.</p>
                <ul class="section__content-list">
                    <li class="section__content-list__item--terms">nicht zum gezielten und/ oder planm&auml;&szlig;igen Empfang</li>
                    <li class="section__content-list__item--terms">nicht zur gezielten und/ oder planm&auml;&szlig;igen Versendung und/oder Ver&ouml;ffentlichung
                    </li>
                    <li class="section__content-list__item--terms">nicht zum Abruf</li>
                    <li class="section__content-list__item--terms">nicht zur Speicherung</li>
                    <li class="section__content-list__item--terms">nicht zur Weiterleitung</li>
                    <li class="section__content-list__item--terms">nicht zum Hochladen</li>
                    <li class="section__content-list__item--terms">nicht zur Ver&ouml;ffentlichung</li>
                </ul>
                <p class="section__content-text">jugendgef&auml;hrdender, pornographischer Inhalte (z. B. Nacktbilder,
                    pornographische Texte, Schriften
                    und &Auml;u&szlig;erungen etc.), gewaltverherrlichender und/oder volksverhetzender und/oder
                    diffamierender sowie sonstiger sitten-/gesetzeswidriger Inhalte und/oder Schriften zu nutzen und/
                    oder
                    die Dienste hierf&uuml;r Dritten zur Verf&uuml;gung zu stellen.</p>
                <p class="section__content-text"><strong>Er verpflichtet sich weiter, keine Inhalte</strong>
                </p>
                <ul class="section__content-list">
                    <li class="section__content-list__item--terms">gezielt und/ oder planm&auml;&szlig;ig zu empfangen</li>
                    <li class="section__content-list__item--terms">gezielt und/ oder planm&auml;&szlig;ig zu versenden und/oder zu verbreiten</li>
                    <li class="section__content-list__item--terms">zum Abruf bereit zu halten&nbsp;</li>
                    <li class="section__content-list__item--terms">zu speichern</li>
                    <li class="section__content-list__item--terms">weiterzuleiten</li>
                    <li class="section__content-list__item--terms">hoch zuladen</li>
                    <li>zu ver&ouml;ffentlichen</li>
                </ul>
                <p class="section__content-text">in denen zu Straftaten aufgerufen und/oder Anleitungen hierf&uuml;r
                    dargestellt werden und/ oder
                    derartige Leistungen anzubieten und/oder anbieten zu lassen.</p>
                <p class="section__content-text">Dar&uuml;ber hinaus verpflichtet sich der Nutzer, keine Inhalte zu ver&ouml;ffentlichen,
                    die anderen
                    einen Zugang oder die M&ouml;glichkeit zum Abruf jugendgef&auml;hrdender, pornographischer,
                    gewaltverherrlichender, volksverhetzender, diffamierender und/oder sonstiger
                    sitten-/gesetzeswidriger
                    Inhalte erlauben.</p>
                <p class="section__content-text">Die vorstehenden Aufz&auml;hlungen sind nicht abschlie&szlig;end.</p>
                <p class="section__content-text"><strong>6.3</strong> Die Dienste sind ausschlie&szlig;lich zur
                    privaten,
                    nicht-kommerziellen Nutzung
                    vorgesehen. Der Nutzer verpflichtet sich, keine kommerziellen Aktivit&auml;ten und/oder
                    Verk&auml;ufe &uuml;ber die Dienste auszu&uuml;ben bzw. anzubahnen und/oder keine kommerziellen
                    Angebote
                    zu verbreiten und/oder hierf&uuml;r zu werben.</p>
                <p class="section__content-text"><strong>6.4</strong> Erlangt RPR1. Kenntnis davon, dass das der
                    Benutzername und/oder die durch ihn im
                    Rahmen der Nutzung verbreiteten Inhalte, Texte oder Schriften mit Form, Inhalt und/ oder verfolgtem
                    Zweck gegen gesetzliche Verbote/ Gebote, insbesondere gegen Rechte Dritter verst&ouml;&szlig;t, ist
                    RPR1. berechtigt, die entsprechenden Inhalte zu entfernen und/ oder den Zugang zu diesen &ndash;
                    auch
                    dauerhaft &ndash; zu sperren und das Nutzungsverh&auml;ltnis au&szlig;erordentlich fristlos zu k&uuml;ndigen
                    (Punkt 10.3).</p>
                <p class="section__content-text"><strong>6.5</strong> Verst&ouml;&szlig;t der Nutzers und/oder dessen
                    Benutzername und/oder die durch ihn
                    im Rahmen der Nutzung verbreiteten Inhalte, Texte oder Schriften gegen gesetzliche Verbote/Gebote,
                    insbesondere Rechte Dritter, oder gegen diese AGB und hat der Nutzer dieses zu vertreten, haftet der
                    Nutzer gegen&uuml;ber RPR1. auf Ersatz aller hieraus entstehenden Sch&auml;den. Der Nutzer stellt
                    RPR1.
                    bereits heute im Innenverh&auml;ltnis von etwaigen aus diesen Verst&ouml;&szlig;en resultierenden
                    Anspr&uuml;chen
                    Dritter frei. Die Geltendmachung eines weiteren Schadenersatzes durch RPR1. &ndash; gegen
                    entsprechenden
                    Nachweis &ndash; ist hierdurch nicht ausgeschlossen. Zudem kann eine zivil- und/oder strafrechtliche
                    Verfolgung erfolgen.</p>
                <p class="section__content-text"><strong>6.6</strong> Aktivit&auml;ten des Nutzers, die darauf abzielen,
                    die
                    RPR1.-Dienste des Webportals
                    <a title="vereinsleben.de - wir. leben. sport" href="//www.vereinsleben.de">www.vereinsleben.de</a>
                    funktionsuntauglich zu machen und/oder deren Funktionstauglichkeit zumindest &ndash; sei es auch nur
                    vor&uuml;bergehend &ndash;
                    zu beeintr&auml;chtigen und/ oder zu erschweren, berechtigen RPR1. zur au&szlig;erordentlichen
                    fristlosen K&uuml;ndigung des Nutzungsverh&auml;ltnisses. Zudem kann eine zivil- und/oder
                    strafrechtliche Verfolgung erfolgen.</p>
                <p class="section__content-text"><strong>6.7</strong> Nach einem Zeitraum von sechs Monaten vollst&auml;ndiger
                    Inaktivit&auml;t (kein
                    Login &uuml;ber Webbrowser) ist RPR1. berechtigt, den Account bzw. Zugang des Nutzers und die im
                    Profil
                    des Nutzers gespeicherten Inhalte ohne Vorank&uuml;ndigung und R&uuml;ckfrage zu l&ouml;schen.</p>
                <p class="section__content-text">Mit der L&ouml;schung geht ein v&ouml;lliger Verlust der Zugangsm&ouml;glichkeiten
                    zu www.vereinsleben.de
                    einher. Das Vertragsverh&auml;ltnis endet damit. Ein neues Vertragsverh&auml;ltnis kann jeder Zeit
                    begr&uuml;ndet
                    werden.</p>
                <p class="section__content-text">Nach einem weiteren Zeitraum von sechs Monaten ab L&ouml;schung ist
                    RPR1.
                    zudem berechtigt, den
                    Benutzernamen des Nutzers freizugeben und anderen Nutzern zur Verf&uuml;gung zu stellen.</p>
                <h3>7 Urheberrechte an eingestellten Inhalten</h3>
                <p class="section__content-text">RPR1. weist die Nutzer auf Folgendes hin:</p>
                <p class="section__content-text"><strong>7.1</strong> Bereits ein Up- oder Download urheberrechtlich
                    gesch&uuml;tzter
                    Inhalte z.B. als
                    Text, Foto, Audiosignal oder Video, kann Verwertungsrechte des Rechteinhabers verletzen,
                    insbesondere
                    das allein dem Urheber zustehende Vervielf&auml;ltigungsrecht und das Recht der &ouml;ffentlichen
                    Wiedergabe.</p>
                <p class="section__content-text">Ein legaler Upload urheberrechtlich gesch&uuml;tzter Inhalte im
                    Internet
                    bedarf der vorherigen
                    Genehmigung (Lizenz) des Rechteinhabers bzw. Urhebers, wobei insbesondere auch die Genehmigung
                    (Lizenz)
                    zur Erstellung von Kopien und zur Verwendung durch Dritte durch den Urheber erteilt werden muss.
                    Macht
                    der Nutzer durch Upload urheberrechtlich gesch&uuml;tzte Inhalte ohne vorherige Genehmigung (Lizenz)
                    der &Ouml;ffentlichkeit zug&auml;nglich, verst&ouml;&szlig;t der Upload gegen die Rechte des
                    Urhebers/des sonst wie Berechtigten.</p>
                <p class="section__content-text"><strong>7.2</strong> Im Falle des nicht genehmigten Uploads kann der
                    Rechteinhaber vom Nutzer die L&ouml;schung
                    der gesch&uuml;tzten Inhalte und Unterlassung und Ersatz des entstandenen Schadens verlangen.
                    Daneben k&ouml;nnen
                    auf den Nutzer erhebliche Rechtsverfolgungskosten zukommen.</p>
                <p class="section__content-text"><strong>7.3</strong> Der Nutzer sichert bereits mit dem Upload der
                    Inhalte
                    auf die ihm zur Verf&uuml;gung
                    gestellten Speicherpl&auml;tze, insbesondere auf sein pers&ouml;nliches Profil, zu, Inhaber s&auml;mtlicher
                    Rechte, insbesondere der Marken-, Patent-, Urheber- und/ oder Lizenzrechte und/ oder sonstiger
                    Rechte
                    und/oder vergleichbarer Rechtspositionen an den jeweiligen Inhalten, insbesondere an Texten, Fotos,
                    Audiosignalen (insbesondere MP3&acute;s) und Videos zu sein, wobei die vorstehende Aufz&auml;hlung
                    nicht
                    abschlie&szlig;end ist. Ferner sichert der Nutzer zu, dass Dritten keine anderweitigen Rechte,
                    insbesondere Wahrnehmungsrechte (z.B. GEMA), einger&auml;umt wurden, die geeignet w&auml;ren, die
                    erforderliche Rechte&uuml;bertragung zu behindern oder unm&ouml;glich zu machen.</p>
                <p class="section__content-text"><strong>7.4</strong> Durch den Upload der Inhalte auf das Webportal <a
                            title="vereinsleben.de - wir. leben. sport"
                            href="//www.vereinsleben.de">www.vereinsleben.de</a>
                    gew&auml;hrt der Nutzer RPR1. eine zeitlich, r&auml;umlich und inhaltlich unbeschr&auml;nkte,
                    unwiderrufliche sowie &uuml;bertragbare Lizenz zum Kopieren, Verbreiten, Ver&auml;ndern, Vervielf&auml;ltigen
                    und Ver&ouml;ffentlichen der jeweiligen Inhalte. Diese Lizenz umfasst insbesondere die Berechtigung,
                    die
                    Inhalte und/oder die entstandenen Bearbeitungen bzw. K&uuml;rzungen ganz und/oder in Teilen in ihren
                    visuellen und/oder audiovisuellen und/oder auditiven Erscheinungsformen weltweit</p>
                <p class="section__content-text"><strong>7.5</strong> Wenn Nutzer Bilder und / oder Videos auf die
                    Plattform
                    www.vereinsleben.de
                    hochladen, werden die Nutzungsrechte an RPR1. zeitlich und r&auml;umlich unbegrenzt einger&auml;umt.
                    Die
                    Nutzer, die die Bilder und/oder Videos hochladen tragen dabei die Verantwortung daf&uuml;r, dass die
                    Bilder frei von Rechten Dritter sind.</p>
                <ul class="section__content-list">
                    <li class="section__content-list__item--terms">zu verbreiten</li>
                    <li class="section__content-list__item--terms">der &Ouml;ffentlichkeit zug&auml;nglich zu machen</li>
                    <li class="section__content-list__item--terms">zur Ausstrahlung zu bringen</li>
                    <li class="section__content-list__item--terms">zu senden</li>
                    <li class="section__content-list__item--terms">&ouml;ffentlich aufzuf&uuml;hren.</li>
                </ul>
                <p class="section__content-text">Als Verbreitung bzw. Ausstrahlung im Sinne dieser AGB gilt insbesondere
                    die
                    Ausstrahlung des auditiven
                    Materials &uuml;ber die von RPR1. produzierten H&ouml;rfunkprogramme ("on air") sowie die
                    Verbreitung
                    der auditiven und/oder visuellen Inhalte via mobiler und Internet-Technologien, insbesondere via
                    Streaming und/oder Podcasting im Internet.</p>
                <p class="section__content-text"><strong>7.6</strong> Der Nutzer gew&auml;hrt dar&uuml;ber hinaus jedem
                    registrierten und angemeldeten
                    Nutzer des Portals <a title="vereinsleben.de - wir. leben. sport" href="//www.vereinsleben.de">www.vereinsleben.de</a>
                    eine nicht exklusive Lizenz zum Zugriff auf die hochgeladenen Inhalte. Im Rahmen dieser nicht
                    exklusiven
                    Lizenz darf jeder angemeldete und registrierte Nutzer die hochgeladenen Inhalte abrufen.</p>
                <p class="section__content-text">Ausdr&uuml;cklich nicht erfasst von der nicht exklusiven Lizenz ist</p>
                <ul class="section__content-list">
                    <li class="section__content-list__item--terms">jegliche kommerzielle Nutzung</li>
                    <li class="section__content-list__item--terms">sowie jegliche Verwendung im Rahmen von Diensten au&szlig;erhalb des Portals von RPR1.</li>
                </ul>
                <p class="section__content-text">Diese nicht exklusive Lizenz erlischt mit Entfernung der Inhalte durch
                    den
                    Nutzer aus dem Webportal <a
                            title="vereinsleben.de - wir. leben. sport"
                            href="//www.vereinsleben.de">www.vereinsleben.de</a>
                    .</p>
                <p class="section__content-text"><strong>7.7</strong> Erlangt RPR1. Kenntnis von einem Versto&szlig;
                    gegen
                    Dritten zustehende Marken-,
                    Patent-, Urheber- und/ oder Lizenzrechte und/ oder sonstiger Rechte und/ oder vergleichbarer
                    Rechtspositionen, wird RPR1. die entsprechenden Inhalte ohne Vorank&uuml;ndigung l&ouml;schen. RPR1.
                    beh&auml;lt
                    sich vor, in solchen F&auml;llen eine Bearbeitungsgeb&uuml;hr von pauschal EUR 100.- zu erheben und
                    ggf.
                    im Wiederholungsfall den Nutzer durch einen Rechtsanwalt abzumahnen. Dar&uuml;ber hinaus hat RPR1.
                    das
                    Recht, den Nutzer dauerhaft und mit sofortiger Wirkung von der Nutzung des Portals auszuschlie&szlig;en.
                </p>
                <p class="section__content-text"><strong>7.8</strong> Der Nutzer stellt RPR1. bereits heute von etwaigen
                    Anspr&uuml;chen Dritter frei, die
                    im Zusammenhang mit der Verletzung der diesen zustehenden Marken-, Patent-, Urheber- und/ oder
                    Lizenzrechten und/ oder sonstiger Rechte und/ oder vergleichbarer Rechtspositionen stehen. Die
                    vorstehende Aufz&auml;hlung ist nicht abschlie&szlig;end. Davon unber&uuml;hrt bleibt das Recht von
                    RPR1., den Nutzer auf Schadensersatz in Anspruch zu nehmen.</p>
                <h3>8. Rechte und Pflichten von RPR1.</h3>
                <p class="section__content-text">
                    <strong>8.1</strong> Die von RPR1. im Rahmen der Dienste des Webportals <a
                            title="vereinsleben.de - wir. leben. sport"
                            href="//www.vereinsleben.de">www.vereinsleben.de</a>
                    zu erbringenden Leistungen beschr&auml;nken sich auf die oben (s.o. Punkt 2) genannten Leistungen.
                </p>
                <p class="section__content-text">
                    <strong>8.2</strong> RPR1. &uuml;bernimmt keine Gew&auml;hr f&uuml;r die
                    Weiterleitung von Nachrichten an
                    andere Nutzer, sowie f&uuml;r die tats&auml;chliche Ver&ouml;ffentlichung der durch den Nutzer
                    vorgesehenen Inhalte, Texte und Schriften. Es obliegt dem Nutzer, Originale seiner vorgesehenen
                    Inhalte,
                    Texte und Schriften sicher an einem anderen Ort zu verwahren.</p>
                <p class="section__content-text">
                    <strong>8.3</strong> RPR1. trifft alle nach dem Stand der Technik m&ouml;glichen
                    Vorkehrungen, eine Verf&uuml;gbarkeit
                    der Dienste von 90 Prozent im Jahresmittel zu erreichen. Ausgenommen sind Zeiten, in denen die
                    Dienste
                    aufgrund von technischen und/ oder sonstigen Problemen, die nicht im Einflussbereich von RPR1.
                    liegen (h&ouml;here
                    Gewalt, Verschulden Dritter usw.) nicht verf&uuml;gbar sind.</p>
                <p class="section__content-text">Gleiches gilt f&uuml;r Zeiten, in denen Wartungsarbeiten durch RPR1.
                    oder
                    damit betraute Dritte
                    vorgenommen werden. Dieses geschieht in der Regel jedoch nicht ohne vorherige Ank&uuml;ndigung.</p>
                <p class="section__content-text">RPR1. kann den Zugang zu den Leistungen beschr&auml;nken, sofern die
                    Sicherheit des Netzbetriebes, die
                    Aufrechterhaltung der Netzintegrit&auml;t, insbesondere die Vermeidung schwerwiegender St&ouml;rungen
                    des Netzes, der Software und/oder gespeicherter Daten dies erfordern.</p>
                <p class="section__content-text">
                    <strong>8.4</strong> Anhand der weiteren, durch den Nutzer hinterlegten
                    freiwilligen Daten wird RPR1. f&uuml;r
                    den Nutzer passende Werbeeinblendungen ausw&auml;hlen. Der Datenschutz richtet sich nach der
                    geltenden
                    Datenschutzbestimmungen.</p>
                <p class="section__content-text">
                    <strong>8.5</strong> RPR1. beh&auml;lt sich das Recht vor, die angebotenen
                    Dienste und Leistungen im
                    Rahmen des technischen Fortschritts zu verbessern.</p>
                <h3>9. Freistellung</h3>
                <p class="section__content-text">
                    <strong>9.1</strong> Der Nutzer stellt RPR1. von s&auml;mtlichen Anspr&uuml;chen
                    Dritter frei, die diesen
                    infolge von Rechtsverletzungen im Zusammenhang mit der Nutzung der Dienste von RPR1. durch den
                    Nutzer
                    unmittelbar und/ oder mittelbar entstehen.</p>
                <p class="section__content-text"><strong>9.2</strong> Dieses gilt auch und insbesondere in F&auml;llen,
                    in
                    denen Dritten ein Schaden durch
                    die unbefugte Nutzung der Zugangsdaten (Benutzername und Passwort) und/ oder des Benutzernamens des
                    Nutzers entsteht.</p>
                <h3>10. Ende des Nutzungsrechts</h3>
                <p class="section__content-text">
                    <strong>10.1</strong> Der Nutzer hat das Recht, das Nutzungsverh&auml;ltnis
                    jederzeit fristlos zu k&uuml;ndigen.
                    Die K&uuml;ndigung erfolgt durch elektronische Erkl&auml;rung gegen&uuml;ber RPR1. durch Auswahl der
                    Option "Mitgliedschaft k&uuml;ndigen".</p>
                <p class="section__content-text">Der Angabe von Gr&uuml;nden bedarf es zur Wirksamkeit der K&uuml;ndigung
                    nicht.</p>
                <p class="section__content-text">
                    <strong>10.2</strong> RPR1. hat das Recht, das Nutzungsverh&auml;ltnis mit
                    einer Frist von vier Wochen zu
                    k&uuml;ndigen. Die K&uuml;ndigung erfolgt durch eine entsprechende elektronische Erkl&auml;rung an
                    die
                    im Rahmen der Registrierung angegebene E-Mail-Adresse des Nutzers.</p>
                <p class="section__content-text">Der Angabe von Gr&uuml;nden bedarf es zur Wirksamkeit der K&uuml;ndigung
                    nicht.</p>
                <p class="section__content-text">
                    <strong>10.3</strong> Davon unber&uuml;hrt bleibt das Recht von RPR1. zur
                    au&szlig;erordentlichen K&uuml;ndigung
                    aus wichtigem Grunde.</p>
                <p class="section__content-text">
                    <strong>10.4</strong> Ferner endet das Nutzungsverh&auml;ltnis, ohne dass
                    es einer gesonderten Erkl&auml;rung
                    bedarf, wenn RPR1. die angebotenen Dienste nicht nur vor&uuml;bergehend einstellt.</p>
                <p class="section__content-text">
                    <strong>10.5</strong> Wird das Vertragsverh&auml;ltnis beendigt, d.h. durch
                    den Nutzer oder durch RPR1.
                    ordentlich gek&uuml;ndigt oder nach Ablauf einer zeitlichen Befristung oder durch RPR1. au&szlig;erordentlich
                    gek&uuml;ndigt, verpflichtet sich RPR1 die in dem Profil des Nutzers gespeicherten Daten, nach den
                    gesetzlich vorgeschriebenen Aufbewahrungsfristen, r&uuml;ckbehaltlos und dauerhaft zu l&ouml;schen.
                </p>
                <h3>11. Gew&auml;hrleistung</h3>
                <p class="section__content-text">
                    <strong>11.1</strong> Eine bestimmte Verf&uuml;gbarkeit der angebotenen
                    Dienste wird nicht gew&auml;hrleistet.
                </p>
                <p class="section__content-text">
                    <strong>11.2</strong> Es wird darauf hingewiesen, dass die Verantwortung f&uuml;r
                    die technischen
                    Einrichtungen des Nutzers und/ oder deren Eignung f&uuml;r die Inanspruchnahme der von RPR1.
                    angebotenen
                    Dienste ausschlie&szlig;lich im Verantwortungsbereich des Nutzers liegt.</p>
                <p class="section__content-text">RPR1. haftet nicht f&uuml;r Sch&auml;den, die dem Nutzer an seinen
                    technischen Einrichtungen (Computer
                    etc.) im Rahmen der Nutzung der von RPR1. angebotenen Dienste entstehen.</p>
                <p class="section__content-text">
                    <strong>11.3</strong> RPR1. haftet nur f&uuml;r solche Sch&auml;den, die
                    auf vors&auml;tzlichen und/ oder
                    grob fahrl&auml;ssigen Vertragsverletzungen durch RPR1., deren gesetzliche Vertreter, und/oder Erf&uuml;llungsgehilfen
                    beruhen.</p>
                <p class="section__content-text">
                    <strong>11.4</strong> F&uuml;r die Haftung von RPR1. in dem Fall, dass
                    Telekommunikationsdienstleistungen
                    betroffen sind, gelten im Falle von Verm&ouml;genssch&auml;den die Vorschrift des &sect; 7 TKV und
                    die
                    dort vorgesehenen H&ouml;chstbetr&auml;ge.</p>
                <p class="section__content-text">
                    <strong>11.5</strong> RPR1. kann nicht verantwortlich gemacht werden f&uuml;r
                    St&ouml;rungen und/oder Sch&auml;den,
                    die im Allgemeinen beim Betrieb von Computeranlagen entstehen k&ouml;nnen und/oder durch mangelnde
                    und/oder unzureichende Kenntnis im Umgang mit Computersystemen und Internetanwendungen entstehen k&ouml;nnen.
                </p>
                <p class="section__content-text">
                    <strong>11.6</strong> RPR1. weist darauf hin, dass es sich bei den durch
                    die Nutzer &uuml;bermittelten,
                    hochgeladenen, zum Abruf bereit gehaltenen und/oder ver&ouml;ffentlichten Inhalte, Texte und/oder
                    Schriften um fremde Inhalte handelt und damit nicht die Meinung von RPR1. wiedergegeben wird.</p>
                <h3>12. Schlussbestimmungen</h3>
                <p class="section__content-text">F&uuml;r die von RPR1. auf der Grundlage dieser AGB abgeschlossenen
                    Vertr&auml;ge
                    und f&uuml;r aus ihnen
                    folgende Anspr&uuml;che gleich welcher Art gilt ausschlie&szlig;lich das Recht der Bundesrepublik
                    Deutschland unter Ausschluss der Bestimmungen zum einheitlichen UN-Kaufrecht &uuml;ber den Kauf
                    beweglicher Sachen und Ausschluss des Deutschen Internationalen Privatrechts.</p>
                <p class="section__content-text">Sollten einzelne Bestimmungen dieser allgemeinen Gesch&auml;ftsbedingungen
                    und/ oder des Vertrages
                    unwirksam sein oder werden, so ber&uuml;hrt dies die Wirksamkeit der &uuml;brigen Bestimmungen
                    nicht</p>
            </div>
        </div>
    </div>
@endsection