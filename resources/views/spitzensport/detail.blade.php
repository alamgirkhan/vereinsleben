@extends('layouts.master')
@section('title')
    {{ $spitzensport->title }}

    @if(isset($spitzensport->sub_title))
        - {{ $spitzensport->sub_title }}
    @endif
@endsection
@section('meta_description')@if(isset($spitzensport->meta->meta_description )){{ $spitzensport->meta->meta_description }}@endif @endsection
@section('meta_language')@if(isset($spitzensport->meta->meta_description )){{ $spitzensport->meta->meta_language}}@endif @endsection
@section('meta_author')@if(isset($spitzensport->meta->meta_description )){{ $spitzensport->meta->meta_author }}@endif @endsection
@section('meta_keywords')@if(isset($spitzensport->meta->meta_description )){{ $spitzensport->meta->meta_keywords }}@endif @endsection

@section('fb-meta')
    <meta property="og:type"            content="article" />
    <meta property="og:url"             content="{{url()->current()}}" />
    <meta property="og:title"           content="{{$spitzensport->title ? $spitzensport->title : '$spitzensport->title'}}" />
    <meta property="og:image"           content="{{isset($spitzensport->main_image) && $spitzensport->main_image->originalFilename() != null ? asset($spitzensport->main_image->url('singleview')) : ''}}" />
    <meta property="og:description"    content="{{$spitzensport->meta->meta_description ? $spitzensport->meta->meta_description : ''}}" />
@endsection

@section('content')
    <section class="news-detail">
        <div class="container">
            <div class="row">
                {{-- main content --}}
                <div class="col-lg-9 col-md-8 col-sm-7 col-xs-12 news">
                    @can('edit', $spitzensport)
                        <a class="pull-right button button--center button--light button--icon"
                           href="{{ route('admin.news.edit', $spitzensport->id) }}">
                            &nbsp;<span class="fa fa-edit"></span> News bearbeiten&nbsp;
                        </a>
                    @endcan

                    <h2>{{ $spitzensport->title }}</h2>
                    @if(isset($spitzensport->sub_title))
                        <h3>{{ $spitzensport->sub_title }}</h3>
                    @endif

                    <span class="label label-default sportnews">
					<a class="news-detail__category-link"
                       href="{{ route('news.category', $spitzensport->category->slug) }}">
                       {{-- FIXME: below is not working properly will see in the next phase  --}}
                       <span class="news-detail__subcategory">Spitzensport in Rheinland Pfalz</span>
						{{-- <span class="news-detail__subcategory">{{ $spitzensport->category->title }} in {{ isset($sessionState) ? $sessionState->name : 'Rheinland-Pfalz' }}</span> --}}
					</a>
				    </span>

                        <span class="article-time"><i class="fa fa-clock-o" aria-hidden="true"></i>
				      Veröffentlicht am {{ strftime('%A, %d. %B %Y', strtotime($spitzensport->published_at)) }}
				    </span>
                        @if($imageRes)
                            <div class="news-lead-img">
                                <img class="news-detail__main-image"
                                     src="{{ asset($imageRes->picture->url('original')) }}"/>
                            </div>
                            <div class="row news-slide-caption">
                                <div class="col-lg-9 col-md-8 col-sm-7 col-xs-7 news-slide news-title-desk">
                                    <h2>{{ $imageRes->title }}</h2>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 news-slide-arrow">
                                    @if($prevImgUrl)
                                        <a href="{{ $prevImgUrl }} ">
                                            @endif
                                            <span class="news-prev"><i class="fa fa-angle-left"
                                                                       aria-hidden="true"></i></span>
                                            @if($prevImgUrl)
                                        </a>
                                    @endif
                                    <span class="pagingInfo">{{ $seq }} von {{ $spitzensport->images()->count() }}</span>
                                    @if($nextImgUrl)
                                        <a href="{{ $nextImgUrl }} ">
                                            @endif
                                            <span class="news-next"><i class="fa fa-angle-right"
                                                                       aria-hidden="true"></i></span>
                                            @if($nextImgUrl)
                                        </a>
                                    @endif
                                </div>
                            </div>
                        <div class="row news-slide-caption news-title-mob">
                            <div class="col-sm-12 col-xs-12 news-slide">
                                <h2>{{ $imageRes->title }}</h2>
                            </div>
                        </div>

                        <p>
                            {!! $imageRes->description !!}
                        </p>
                        @else
                            @if ($spitzensport->main_image->originalFilename() !== null)
                                <div class="news-lead-img">
                                    <img class="news-detail__main-image"
                                         src="{{ asset($spitzensport->main_image->url('singleview')) }}"
                                         alt="@if(isset($spitzensport->meta->meta_bild_alt )){{ $spitzensport->meta->meta_bild_alt }}@endif">
                                </div>
                                @if ($spitzensport->main_image_source !== null && $spitzensport->main_image_source !== '')
                                    <p class="news-lead-img-cpsn">
                                        Quelle: {{ $spitzensport->main_image_source }}
                                    </p>
                                @endif
                            @endif
                        @endif

                    <h4>{{ $spitzensport->content_teaser }}</h4>
                    <p>
                        {!! $spitzensport->content !!}
                    </p>

                        @if(count($spitzensport->tags) != 0)
                            <div class="row">
                                <div class="col-lg-12 profile-post-tabs">

                                    @foreach($spitzensport->tags as $tag)
                                        <li><a href="#">{{ $tag->name }}</a></li>
                                    @endforeach

                                </div>
                            </div>
                        @endif
                    @if(isset($campaign) && isset($campaign->banner_horizontal))
                        <div class="image-hor campaign-image" data-toggle="tooltip" title="{{$campaign->description}}">
                            <a target="_blank" href="{{$campaign->link}}"> <img src="/uploaded/campaign/{{$campaign->banner_horizontal}}" alt=""></a>
                        </div>
                    @endif
                </div>

                {{-- aside --}}

                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="rhs-box">
                                <h2>Artikel Teilen</h2>
                                <ul class="social-icons">
                                    <li>
                                        {{--changes regarding new news layout--}}
                                        {{--<a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(route('news.detail', [$spitzensport->category->slug, $spitzensport->slug])) }}"--}}
                                        {{--target="_blank"><i class="fa fa-facebook"></i></a>--}}
                                        <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(route('news.detail', [$spitzensport->category_id, $spitzensport->slug])) }}"
                                           target="_blank"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li>
                                        {{--changes regarding new news layout--}}
                                        {{--<a href="https://twitter.com/intent/tweet?url={{ urlencode(route('news.detail', [$spitzensport->category->slug, $spitzensport->slug])) }}"--}}
                                        {{--target="_blank"><i class="fa fa-twitter"></i></a>--}}
                                        <a href="https://twitter.com/intent/tweet?url={{ urlencode(route('news.detail', [$spitzensport->category_id, $spitzensport->slug])) }}"
                                           target="_blank"><i class="fa fa-twitter"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    {{-- @if($related->count() > 0)
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="rhs-box">
                                    <h2>Mehr zum Thema</h2>
                                    <aside class="news-detail__aside" data-pager
                                           data-pager-content='{"url": "{{ route('news.related', [$spitzensport->category->id, $spitzensport->id]) }}", "category_id": "{{ $spitzensport->category->id }}"}'
                                           data-pager-url="{{ route('news.related') }}"
                                           data-token="{{ csrf_token() }}">
                                        <div class="news-detail__aside-inner">
                                            <div data-pager-html>
                                                <div data-loader class="loader__wrapper loader__wrapper--centered">
                                                    <div class="loader loader--loading"></div>
                                                    <div class="loader loader--loading"></div>
                                                </div>
                                            </div>

                                            <div class="news-detail__aside-news-pagination">
                                                <span data-pager-previous
                                                      class="pager-control__previous fa fa-chevron-left"></span>
                                                <a class="news-detail__category-link"
                                                   href="{{ route('news.category') }}">
                                                    <span class="pager-control__label">Alle Artikel</span>
                                                </a>
                                                <span data-pager-next
                                                      class="pager-control__next fa fa-chevron-right"></span>
                                            </div>
                                        </div>
                                    </aside>
                                </div>
                            </div>
                        </div>
                    @endif --}}
                
                    {{-- steckbrief --}}
                    @if(strlen($spitzensport->steckbrief) > 10)
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="rhs-box">
                                <h2>steckbrief</h2>
                                {!! $spitzensport->steckbrief !!}
                            </div>
                        </div>
                    </div>
                    @endif


                  @if(isset($campaign) && isset($campaign->banner_rechteck))
                    <div class="image-me campaign-image" data-toggle="tooltip" title="{{$campaign->description}}">
                      <a target="_blank" href="{{$campaign->link}}"> <img src="/uploaded/campaign/{{$campaign->banner_rechteck}}" alt=""></a>
                    </div>
                  @endif
                  @if(isset($campaign) && isset($campaign->banner_vertikal))
                    <div class="tablet-ads-vertical">
                        <div class="image-ver campaign-image" data-toggle="tooltip"
                             title="{{$campaign->description}}">
                            <a target="_blank" href="{{$campaign->link}}"> <img
                                        src="/uploaded/campaign/{{$campaign->banner_vertikal}}" alt=""></a>
                        </div>
                    </div>
                  @endif
                </div>

                
            </div>
            @if(isset($campaign) && isset($campaign->banner_vertikal))
                <div class="ads-vertical">
                    <div class="image-ver campaign-image" data-toggle="tooltip" title="{{$campaign->description}}">
                        <a target="_blank" href="{{$campaign->link}}"> <img src="/uploaded/campaign/{{$campaign->banner_vertikal}}" alt=""></a>
                    </div>
                </div>
            @endif
        </div>
    </section>
    @if (count($latest) > 0)
        @include('news.partials.latest-teaser', [
        'news' => $latest,
        'start' => 1,
        'end' => 3,
        'inverted' => true,
        ])
    @endif
@endsection