<!DOCTYPE html>
<html lang="de-DE">
<head>
    <meta charset="utf-8">
</head>
<body>


<div>
    <strong>Hallo {{ $user->firstname }},</strong>
    <br><br>
    es ist sehr schade, dass du dein Konto auf vereinsleben.de löschen möchtest. <br>
    Klicke bitte auf den unten stehenden Link, um dein Konto endgültig zu löschen. <br>
    Nach bestätigen dieses Links und somit der Richtigkeit deiner Daten wird eine entsprechende E-Mail an uns gesandt.
    <br><br>
    <a href="{{ URL::to('/benutzer/delete/' . $deleted_token) }}">Bitte hier klicken um dein Konto endgültig zu löschen!</a>
    <br><br>
    Nach Bestätigung des Links wird dein Konto innerhalb von 48 Stunden gelöscht.<br>
    Danach kannst du dich jederzeit wieder mit deiner E-Mail Adresse bei uns registrieren.
    <br><br>
    Dein VEREINSLEBEN.DE–Team
    <br><br><br>
    @if(isset($imprint))
        <div>{!! $imprint->content !!}</div>
    @endif
</div>
</body>
</html>