<!DOCTYPE html>
<html lang="de-DE">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Benutzerkonto löschen</h2>

<div>
    <br><br>
    ID: {{ $user->user_id }}<br>
    Username: {{ $user->username }}<br>
    E-Mail: {{ $user->email }}
    <br><br>
    Diese E-Mail wurde automatisch generiert.<br>
</div>

@if(isset($imprint))
    <br/>
    <br/>
    <br/>
    <div>{!! $imprint->content !!}</div>
@endif
</body>
</html>