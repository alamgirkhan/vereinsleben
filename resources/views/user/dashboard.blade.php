@extends('layouts.master')

@section('content')
    <section class="dashboard__section">
        <div class="container">
            <div class="row">
                {{--<div class="col-md-4">--}}
                    {{--@include('user.partials.sidebar')--}}
                {{--</div>--}}
                <div class="col-lg-offset-2 col-md-8" data-token="{{ csrf_token() }}">
                    <h1 class="user-dashboard__headline">Mein Vereinsleben</h1>
                    <h2 class="user-dashboard__subheadline">Meine Vereine</h2>

                    @foreach($clubs as $club)
                        @include('club.partials.listteaser', ['club' => $club, 'publishedtoggle' => true])
                    @endforeach

                    {{--<a href="{{ route('club.create') }}"--}}
                       {{--class="button button--center button--light button--icon button--full-width">--}}
                        {{--<i class="fa fa-plus"></i> Lege deinen Verein an</a>--}}
                </div>
            </div>
        </div>
    </section>
@endsection
