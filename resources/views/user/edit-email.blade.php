<div class="row" id="emailupdate" style="display:none;">
    <div class="col-lg-12 col-md-12 col-sn-12 col-xs-12 konto">
        @if ($errors->count() > 0)
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif
        <div class="row">
            {!! Form::model($user, ['route' => 'user.email.update', 'method' => 'POST']) !!}

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                {!! Form::label('updateEmail', 'E-Mail', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::text('new_email', null, ['required', 'class' => 'input', 'placeholder' => 'E-Mail']) !!}
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="input-group">
                    {!! Form::submit('Speichern', array('class' => 'button button--light button--light-white button--center button--full-width')) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>