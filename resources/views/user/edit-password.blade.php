<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 passwort">
        @if ($errors->count() > 0)
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        <div class="row">
            {!! Form::open(['route' => 'user.password.update', 'method' => 'POST']) !!}
            {{-- @todo Benutzername, E-Mail ändern (mit erneuter Validierung) --}}

            <div class="col-md-12">
                {!! Form::label('current-password', 'Aktuelles Passwort', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::password('current-password', ['class' => 'input', 'placeholder' => 'Aktuelles Passwort', 'required' => 'required']) !!}                          </div>
            </div>
            <div class="col-md-12">
                {!! Form::label('new-password', 'Neues Passwort', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::password('new-password', ['class' => 'input', 'placeholder' => 'Neues Passwort', 'required' => 'required']) !!}                          </div>
            </div>
            <div class="col-md-12">
                {!! Form::label('new-password-confirmation', 'Neues Passwort bestätigen', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::password('new-password-confirmation', ['class' => 'input', 'placeholder' => 'Neues Passwort bestätigen', 'required' => 'required']) !!}                          </div>
            </div>
            <div class="col-md-12">
                <div class="input-group">
                    {!! Form::submit('Passwort ändern', array('class' => 'button button--light button--light-white button--center button--full-width')) !!}                          </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
