<div class="row">
    <div class="col-lg-12 col-md-12 col-sn-12 col-xs-12 konto">
        <h2 class="user-dashboard__subheadline">Konto einstellungen</h2>
        @if ($errors->count() > 0)
            <ul>
                @foreach($errors->all() as $error)
                    <li style="color:red;">{{ $error }}</li>
                @endforeach
            </ul>
        @endif
        <div class="row">
            {!! Form::model($user, ['route' => 'user.profile.update', 'method' => 'POST']) !!}


            <div class="col-md-12">
                {!! Form::label('gender', 'Anrede', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::select('gender', ['' => 'Bitte wählen', 'male' => 'Herr', 'female' => 'Frau'], null, ['required' => 'required', 'class' => 'select']) !!}
                </div>
            </div>

            <div class="col-md-12">
                <div class="row">
                    <div class="input-group">
                        <div class="col-md-4">
                            {!! Form::label('firstname', 'Vorname(n)', ['class' => 'input-label']) !!}
                            {!! Form::text('firstname', null, ['required' => 'required', 'class' => 'input', 'placeholder' => 'Vorname(n)', 'pattern' => '^[a-zA-ZäöüÄÖÜß\-. ]+$']) !!}
                        </div>
                        <div class="col-md-4">
                            {!! Form::label('lastname', 'Nachname', ['class' => 'input-label']) !!}
                            {!! Form::text('lastname', null, ['required' => 'required', 'class' => 'input', 'placeholder' => 'Nachname', 'pattern' => '^^[a-zA-ZäöüÄÖÜß\-. ]+$']) !!}
                            {{--{!! Form::text('lastname', null, ['required' => 'required', 'class' => 'input', 'placeholder' => 'Nachname', 'pattern' => '^(([a-zA-ZäöüÄÖÜß]{2,})([- ]+)?)+']) !!}--}}
                        </div>
                        <div class="col-md-4">
                            {!! Form::label('nickname', 'Spitzname', ['class' => 'input-label']) !!}
                            {!! Form::text('nickname', null, ['class' => 'input', 'placeholder' => 'Spitzname']) !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                {!! Form::label('birthday', 'Geburtsdatum', ['class' => 'input-label']) !!}
                <div class="row">
                    <div class="input-group geburtsdatum">
                        <div class="col-md-4">
                            {!! Form::select('birthday-day',  (array('' => 'Tag') + array_combine(range(1, 31), range(1, 31))), isset($user->birthday->day) ? $user->birthday->day : null, ['required' => 'required', 'class' => 'select']) !!}
                        </div>
                        <div class="col-md-4">
                            {!! Form::select('birthday-month',  (array('' => 'Monat') + array_combine(range(1, 12), range(1, 12))), isset($user->birthday->month) ? $user->birthday->month : null, ['required' => 'required', 'class' => 'select']) !!}
                        </div>
                        <div class="col-md-4">
                            {!! Form::select('birthday-year',  (array('' => 'Jahr') + array_combine(range(1900, date('Y')), range(1900, date('Y')))), isset($user->birthday->year) ? $user->birthday->year : null, ['required' => 'required', 'class' => 'select']) !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                {!! Form::label('street', 'Straße', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::text('street', null, ['class' => 'input', 'placeholder' => 'Straße', 'pattern' => '^(([a-zA-ZäöüÄÖÜß]{2,})([-. ]+)?)+']) !!}
                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                {!! Form::label('street', 'Hausnummer', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::text('house_number', null, ['class' => 'input', 'placeholder' => 'Hausnummer', 'id' => 'hnr', 'pattern' => '^[1-9][0-9]?[0-9]?[A-Za-z]{0,2}?']) !!}
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="input-group">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            {!! Form::label('zip', 'Postleitzahl', ['class' => 'input-label']) !!}
                            {!! Form::text('zip', null, ['class' => 'input', 'placeholder' => 'Postleitzahl', 'pattern' => '^([0-9]{4,5})$']) !!}
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                            {!! Form::label('city', 'Ort', ['class' => 'input-label']) !!}
                            {!! Form::text('city', null, ['class' => 'input', 'placeholder' => 'Ort', 'pattern' => '^(([a-zA-ZäöüÄÖÜß]{2,})([-. ]+)?)+']) !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                {!! Form::label('federal_state', 'Bundesland', ['class' => 'input-label']) !!}
                <div class="input-group">
                    {!! Form::select('federal_state', array('' => 'Bitte wählen')+$federal_states->pluck('name', 'name')->all(), null, ['class' => 'select', 'id' => 'state']) !!}
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="input-group nwsltr">
                    {!! Form::checkbox('newsletter', '1', ($user->newsletter == '1'), ['id' => 'newsletter', 'class' => 'input-checkbox']) !!}
                    {!! Form::label('newsletter', 'Ich möchte kostenfrei den Newsletter erhalten', ['class' => 'input-checkbox__label']) !!}
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="input-group msge">
                    {!! Form::checkbox('messages', '1', ($user->messages == '1'), ['id' => 'messages', 'class' => 'input-checkbox']) !!}
                    {!! Form::label('messages', 'Ich möchte Systembenachrichtigungen erhalten', ['class' => 'input-checkbox__label']) !!}
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="input-group">
                    {!! Form::submit('Speichern', array('class' => 'button button--light button--light-white button--center button--full-width')) !!}
                </div>
            </div>

            {!! Form::close() !!}
            {{-- @todo Felder DB-Schema--}}
        </div>
    </div>
</div>


<style>

    /*input:invalid{*/
        /*border: 1px solid red!important;*/
    /*}*/

    input:invalid:not(:focus):not(:placeholder-shown) {

        border: 1px solid red!important;
    }

</style>