<!DOCTYPE html>
<html lang="de-DE">
<head>
    <meta charset="utf-8">
</head>
<body>

<div>
    <strong>Hallo {{ $recipient->firstname }},</strong>
    <br><br>
    du hast eine Freundschaftsanfrage erhalten von {{ $user->firstname }} {{ $user->lastname }}. <br>
    Bitte besuche dein Profil auf <a href="https://www.vereinsleben.de">vereinsleben.de um die Anfrage zu
        überprüfen.</a>
    <br><br>
    Dein VEREINSLEBEN.DE–Team
    @if(isset($imprint))
        <br/>
        <br/>s
        <br/>
        <div>{!! $imprint->content !!}</div>
    @endif
</div>
</body>
</html>