@extends('layouts.master')

@section('content')
    @include('user.index.header')

    @include('user.index.stream')
@endsection