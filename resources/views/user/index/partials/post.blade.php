<?php
$classes = [];
$type = 'default';
if (isset($post) && $post->images()->count() > 0) {
    $classes[] = 'profile-statusposts__item--photo';
    $type = 'photo';
} elseif (isset($post) && $post->videos()->count() > 0) {
    $classes[] = 'profile-statusposts__item--video';
    $type = 'video';
}
$date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $post->published_at);
?>

<div class="user-stream__item {{ implode(' ', $classes) }}" data-user-post data-user-post-type="{{ $type }}"
     data-user-post-href="{{ route('user.post.template', [$user->username, $post->id]) }}">

    <div class="profile-statusposts__actions">
        @can('edit', $post)
            <button data-user-post-edit="{{ route('user.post.edit', [$user->username, $post->id]) }}"
                    class="btn btn--inline btn--icon">
                <i class="fa fa-pencil" aria-hidden="true"></i>
            </button>
        @endcan
        @can('destroy', $post)
            <button data-user-post-delete="{{ route('user.post.destroy', [$user->username, $post->id]) }}"
                    class="btn btn--inline btn--icon">
                <i class="fa fa-trash" aria-hidden="true"></i>
            </button>
        @endcan
    </div>

    <div class="profile-statusposts__date">
        <div class="profile-statusposts__day">
            {{ $date->day }}
        </div>
        <div class="profile-statusposts__month-and-year">
            <div class="profile-statusposts__month">
                {{ $date->formatLocalized('%B') }}
            </div>
            <div class="profile-statusposts__year">
                {{ $date->year }}
            </div>
        </div>
    </div>
    <div class="profile-statusposts__content">
        {{ $post->content }}
    </div>

    @if($post->images()->count() > 0)
        @if($post->images()->count() > 1)
            <div class="club-post__image-slider">
                @endif

                @foreach($post->images as $image)
                    <div class="profile-statusposts__image"
                         style="background-image: url('{{asset($image->picture->url('singleView')) }}')"></div>
                @endforeach

                @if($post->images()->count() > 1)
            </div>
        @endif
    @endif

    @foreach ($post->videos as $video)
        <div class="profile-statusposts__video">
            @if ($video->type === \Vereinsleben\Video::TYPE_YOUTUBE)
                <iframe src="https://www.youtube.com/embed/{{ $video->identifier }}" frameborder="0" allowfullscreen></iframe>
            @endif
        </div>
    @endforeach
</div>
