<div data-load-more='{"url": "{{ route('user.friend.list', ['user_id' => $user->id]) }}"}'>
    @include('user.partials.member-card-wrapper')
</div>
