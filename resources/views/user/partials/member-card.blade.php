<div class="col-xs-12 col-sm-6 col-md-6 fanuserlead" id="user_{{ $user->id }}">
    <div class="fan-box">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 club-result-thum">
                <div class="thum-bg">
                    <a href="{{ route('user.detail', $user->username) }}"><img
                                src="{{ $user->avatar->url('singleView') }}" alt=""></a>
                </div>
            </div>
            <div class="col-lg-9 col-sm-9 col-xs-8 fanuserlead-detail">
                <div class="fan-box-detail">
                    <h3><a href="{{ route('user.detail', $user->username) }}" class="member-card__link">
                            @if(!$user->firstname && !$user->lastname)
                                {{ $user->username }}
                            @else
                                {{ $user->firstname or '' }}
                                {{ $user->lastname or '' }}
                            @endif
                        </a>
                    </h3>
                </div>
                <div class="fan-lctn"><i class="fa fa-map-marker" aria-hidden="true"></i> {{ $user->city }}</div>
            <!--<div class="fan-likes">
                    @if($user->sports->count() > 0)
                        {{ $user->sports->take(4)->implode('title', ', ') }}
                    @endif
                    </div>-->
                @if($me)
                    <div class="folgen-btn">
                        @if($me->hasSentFriendRequestTo($user))
                            <div class="friend-btn">
                                <i class="fa fa-check-square-o" aria-hidden="true"></i>Anfrage gesendet
                            </div>
                        @elseif($me->isFriendWith($user))
                            @if(isset($profile) && ($me->id == $profile->id))
                            <a href="javascript:Unfriendship('{{ $username }}', '{{ $user->username }}', 'remove', '{{ $user->id }}');">
                                <i class="fa fa-check-square-o" aria-hidden="true"></i><span
                                        id="remove_{{ $user->id }}">entfernen</span>
                            </a>
                            <a href="javascript:Unfriendship('{{ $username }}', '{{ $user->username }}', 'block', '{{ $user->id }}');">
                                <i class="fa fa-check-square-o" aria-hidden="true"></i><span id="block_{{ $user->id }}">blockieren</span>
                            </a>
                            @endif
                        @elseif($me->hasFriendRequestFrom($user))

                            <a id="acceptuser_{{ $user->id }}"
                               href="javascript:friendshipEvent('{{ $username }}', '{{ $user->username }}', 'accept', '{{ $user->id }}');"><i
                                        class="fa fa-check-square-o" aria-hidden="true"></i><span
                                        id="accept_{{ $user->id }}">akzeptieren<span></a>
                            <a id="denyuser_{{ $user->id }}"
                               href="javascript:friendshipEvent('{{ $username }}', '{{ $user->username }}', 'deny', '{{ $user->id }}');"><i
                                        class="fa fa-times-circle-o" aria-hidden="true"></i><span
                                        id="deny_{{ $user->id }}">ablehnen<span></a>
                            <a id="removeuser_{{ $user->id }}" style="display:none;"
                               href="javascript:Unfriendship('{{ $username }}', '{{ $user->username }}', 'remove', '{{ $user->id }}');">
                                <i class="fa fa-check-square-o" aria-hidden="true"></i><span
                                        id="remove_{{ $user->id }}">entfernen</span>
                            </a>
                            <a id="bloackuser_{{ $user->id }}" style="display:none;"
                               href="javascript:Unfriendship('{{ $username }}', '{{ $user->username }}', 'block', '{{ $user->id }}');">
                                <i class="fa fa-check-square-o" aria-hidden="true"></i><span id="block_{{ $user->id }}">blockieren</span>
                            </a>
                        @elseif($me->id == $user->id)
                            
						
                        @elseif($me->hasBlocked($user))
                            <a href="javascript:void(0);">
                                <i class="fa fa-ban" aria-hidden="true"></i>Blocked
                            </a>
                        @else
                            <a href="javascript:sendRequest('{{ $username }}', '{{ $user->username }}', 'send-friend-request', '{{ $user->id }}');">
                                <i class="fa fa-plus-circle" aria-hidden="true"></i><span
                                        id="send-friend-request_{{ $user->id }}">Anfrage senden</span>
                            </a>
                        @endif
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>

