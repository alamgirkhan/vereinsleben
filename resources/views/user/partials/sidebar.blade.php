<aside>
    <ul class="club-detail__menu">
        <li class="club-detail__menu-list-item">
            <a href="{{ route('user.detail', Auth::user()->username) }}"
               class="club-detail__menu-list-link">
                <i class="fa fa-user"></i> Mein Profil
            </a>
        </li>

        <li class="club-detail__menu-list-item">
            <a href="{{ route('user.dashboard') }}"
               class="club-detail__menu-list-link{{ Request::is('user/dashboard') ? ' club-detail__menu-list-link--active' : '' }}">
                <i class="fa fa-shield"></i> Meine Vereine
            </a>
        </li>

        <li class="club-detail__menu-list-item">
            <a href="{{ route('user.profile.edit') }}"
               class="club-detail__menu-list-link{{ Request::is('user/profile/edit') ? ' club-detail__menu-list-link--active' : '' }}">
                <i class="fa fa-gears"></i> Kontoeinstellungen
            </a>
        </li>
        <li class="club-detail__menu-list-item">
            <a href="{{ route('user.password.edit') }}"
               class="club-detail__menu-list-link{{ Request::is('user/password/edit') ? ' club-detail__menu-list-link--active' : '' }}"><i class="fa fa-unlock-alt"></i> Passwort ändern
            </a>
        </li>
    </ul>
</aside>