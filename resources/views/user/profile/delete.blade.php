<div id="modal" class="modal__container modal__container--opend rgst-box-inner">
    <section class="modal">
        <div class="modal__close" data-modal-close>
            <i class="fa fa-times" aria-hidden="true"></i>
        </div>
        <div class="modal__content">
            <div class="modal__inner">
                <h2>Konto löschen?</h2>
                <a class="btn btn-default finden-btn" href="/benutzer/{username}/delete">Bestätigen</a>
                <a class="btn btn-default finden-btn">Abbrechen</a>
            </div>
        </div>
    </section>
</div>


