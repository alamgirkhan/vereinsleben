@extends('layouts.master')
@section('content')
    <section data-token="{{ csrf_token() }}">
        <input type="hidden" name="search_id" id="search_id" value="{{ $user->id }}"/>
        <div class="container-fluid">
            <div class="row">
                @include('user.profile.partials.header')
            </div>
        </div>
        <section class="about-strip userprofile-main">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4"></div>
                    <div class="col-md-9 col-sm-8 col-xs-8 proifle-top-links profile-desk ">
                        <div class="row">
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-3 count-tabs">Besucher<br><strong>{{ $user->views == null ? 0 : $user->views }}</strong>
                                    </div>
                                    <div class="col-md-4 count-tabs no-border">
                                        Freunde<br><strong>{{ $user->getFriendsCount() }}</strong></div>
                                </div>
                            </div>
                            @if($me && !$me->hasSentFriendRequestTo($user) && !$me->isFriendWith($user) && ($me->id != $user->id))
                                <div class="col-md-5">
                                    <div class="folgen-btn folgen-padding">
                                        @if(Auth::check() && !$me->isProfileCompleted())
                                            <a href="#"
                                               data-modal-url="{{ route('user.profile.wizard') }}"
                                               data-modal="team-member">FREUNDSCHAFTSANFRAGE SENDEN</a>

                                        @else
                                        <a href="javascript:sendRequest('{{ $me->username }}', '{{ $user->username }}', 'send-friend-request', '{{ $user->id }}');">
                                            <i class="fa fa-plus-circle" aria-hidden="true"></i><span
                                                    id="send-friend-request_{{ $user->id }}">FREUNDSCHAFTSANFRAGE SENDEN</span>
                                        </a>
                                        @endif
                                    </div>
                                </div>
                            @endif
                            @if($me && $me->hasSentFriendRequestTo($user))
                                <div class="col-md-5">
                                    <div class="friend-btn">Anfrage gesendet</div>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-9 col-xs-8 proifle-top-links profile-mob">
                        <div class="row">

                            @if($me && !$me->hasSentFriendRequestTo($user) && !$me->isFriendWith($user) && ($me->id != $user->id))
                                <div class="col-md-5 col-xs-12">
                                    <div class="folgen-btn folgen-padding">
                                        @if(Auth::check() && !$me->isProfileCompleted())
                                            <a href="#"
                                               data-modal-url="{{ route('user.profile.wizard') }}"
                                               data-modal="team-member">FREUNDSCHAFTSANFRAGE SENDEN</a>

                                        @else
                                            <a href="javascript:sendRequest('{{ $me->username }}', '{{ $user->username }}', 'send-friend-request', '{{ $user->id }}');">
                                            <i class="fa fa-plus-circle" aria-hidden="true"></i><span
                                                    id="send-friend-request_{{ $user->id }}">FREUNDSCHAFTSANFRAGE SENDEN</span>
                                        </a>
                                        @endif
                                    </div>
                                </div>
                            @endif
                            @if($me && $me->hasSentFriendRequestTo($user))
                                <div class="col-md-5">
                                    <div class="friend-btn">Anfrage gesendet</div>
                                </div>
                            @endif
                            <div class="col-md-7 col-xs-12">
                                <div class="row">
                                    <div class="col-md-3 count-tabs">Besucher<br><strong>{{ $user->views == null ? 0 : $user->views }}</strong>
                                    </div>
                                    <div class="col-md-4 count-tabs no-border">
                                        Freunde<br><strong>{{ $user->getFriendsCount() }}</strong></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <div class="row proife-lft">
                        <div class="col-lg-12">
                            <div class="row">
                                @include('user.profile.partials.avatar')
                            </div>
                            @if(Auth::user() && policy($me)->showProfile($me, $user))
                                <div class="row">
                                    <div class="col-lg-12 profile-tabs userprofile-nav">
                                        <aside>
                                            <nav>
                                                <ul class="club-detail__menu profile-tabs profile-menu-desk"
                                                    data-pageable-menu>
                                                    <li class="club-detail__menu-list-item active">
                                                        <a class="club-detail__menu-list-link active" href="#pinnwand">
                                                        <span><i class="fa fa-newspaper-o"
                                                                 aria-hidden="true"></i></span> Pinnwand
                                                        </a>
                                                    </li>
                                                    @can('edit', $user)
                                                        <li class="club-detail__menu-list-item">
                                                            <a class="club-detail__menu-list-link"
                                                               href="#informationen">
                                                                <span><i class="fa fa-info"
                                                                         aria-hidden="true"></i></span>
                                                                Informationen
                                                            </a>
                                                        </li>
                                                    @endcan
                                                    @can('edit', $user)
                                                        <li class="club-detail__menu-list-item">
                                                            <a class="club-detail__menu-list-link" href="#passwort">
                                                            <span><i class="fa fa-unlock-alt"
                                                                     aria-hidden="true"></i></span>
                                                                Passwort ändern
                                                            </a>
                                                        </li>
                                                    @endcan
                                                    <li class="club-detail__menu-list-item">
                                                        <a class="club-detail__menu-list-link" href="#sportvita">
                                                            <span class="sportvita-icon"></span>&nbsp;Sport-Vita
                                                        </a>
                                                    </li>

                                                    <li class="club-detail__menu-list-item">
                                                        <a class="club-detail__menu-list-link" href="#netzwerk">
                                                            <span><i class="fa fa-users" aria-hidden="true"></i></span>
                                                            Netzwerk
                                                            @if($me->id == $user->id)
                                                                @if( count($user->getFriendRequests()) > 0 )
                                                                    &nbsp;
                                                                    <div class="friend-request-circle">
                                                                        {{ count($user->getFriendRequests()) }}
                                                                    </div>
                                                                @endif
                                                            @endif
                                                        </a>
                                                    </li>

                                                    <li class="club-detail__menu-list-item">
                                                        <a class="club-detail__menu-list-link" href="#fotos">
                                                            <span><i class="fa fa-camera" aria-hidden="true"></i></span>
                                                            Fotos
                                                        </a>
                                                    </li>
                                                    {{--@if($me->id == 2074 || $me->id == 439 || $me->id == 27000 || $me->id == 28068 || $me->id == 1951 || $me->id == 2674 || $me->id == 26590 || $me->id == 22871)--}}
                                                        {{--<li class="club-detail__menu-list-item">--}}
                                                            {{--<a class="club-detail__menu-list-link" href="#streaming">--}}
                                                                {{--<span><i class="fa fa-bars"--}}
                                                                         {{--aria-hidden="true"></i></span>--}}
                                                                {{--Streaming--}}
                                                            {{--</a>--}}
                                                        {{--</li>--}}
                                                    {{--@endif--}}
                                                </ul>

                                                <ul class="club-detail__menu profile-tabs profile-menu-mob"
                                                    data-pageable-menu>
                                                    <li class="club-detail__menu-list-item active">
                                                        <a class="club-detail__menu-list-link active" href="#pinnwand">
                                                        <span><i class="fa fa-newspaper-o"
                                                                 aria-hidden="true" href="#pinnwand"></i></span>
                                                        </a>
                                                    </li>
                                                    @can('edit', $user)
                                                        <li class="club-detail__menu-list-item">
                                                            <a class="club-detail__menu-list-link"
                                                               href="#informationen">
                                                        <span><i class="fa fa-info" aria-hidden="true"
                                                                 href="#informationen"></i></span>
                                                            </a>
                                                        </li>
                                                    @endcan
                                                    @can('edit', $user)
                                                        <li class="club-detail__menu-list-item">
                                                            <a class="club-detail__menu-list-link" href="#passwort">
                                                            <span><i class="fa fa-unlock-alt"
                                                                     aria-hidden="true" href="#passwort"></i></span>
                                                            </a>
                                                        </li>
                                                    @endcan
                                                    <li class="club-detail__menu-list-item">
                                                        <a class="club-detail__menu-list-link" href="#sportvita">
                                                            <!--<span><i class="fa fa-spotify" aria-hidden="true"
                                                                     href="#sportvita"></i></span>-->
                                                            <span class="sportvita-icon" href="#sportvita"></span>
                                                        </a>
                                                    </li>

                                                    <li class="club-detail__menu-list-item">
                                                        <a class="club-detail__menu-list-link" href="#netzwerk">
                                                        <span><i class="fa fa-users" aria-hidden="true"
                                                                 href="#netzwerk"></i></span>
                                                        </a>
                                                    </li>

                                                    <li class="club-detail__menu-list-item">
                                                        <a class="club-detail__menu-list-link" href="#fotos">
                                                        <span><i class="fa fa-camera" aria-hidden="true"
                                                                 href="#fotos"></i></span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </nav>
                                        </aside>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="row hidden-xs">
                        <div class="col-lg-12">
                            @if($socialLinks->count() > 0  || Auth::user() && policy($me)->showProfile($me, $user))
                                <section class="club-detail__menu-list-item club-detail__menu-list-item--static"
                                         @can('edit', $user) data-section-editable="true" @endcan>

                                    @can('edit', $user)
                                        <span>
											<a class="club-content__edit-button club-content__edit-button--sociallinks pull-right edit-links"
                                               href="#">
												<span class="fa fa-pencil"></span>
											</a>
										</span>
                                    @endcan

                                    @can('edit', $user)
                                        <div id="userlinks" class="club-content__edit-area">
                                            @include('user.profile.partials.edit.social-links', ['club' => $user, 'socialLinks' => $socialLinks, 'socialLinkTypes' => $socialLinkTypes])
                                        </div>
                                    @endcan
                                    <div class="club-content__view-area">
                                        @can('edit', $user)
                                            @if(count($socialLinks) === 0)
                                                <em>Noch keine Links angelegt.</em>
                                            @else
                                                <em>Links bearbeiten</em>
                                            @endif
                                        @endcan

                                    </div>
                                </section>


                                <div class="row hidden-xs">
                                    <div class="col-lg-12 profile-cnt-details">
                                        <h2>Mehr vom User</h2>
                                        <ul>

                                            @foreach($socialLinks as $socialLink)
                                                @if($socialLink->link_type == 'googleplus')
                                                    <li>
                                                        <a href="{{ $socialLink->url }}"
                                                           target="_blank">
                                                            <span><i class="fa fa-google-plus"
                                                                     aria-hidden="true"></i></span>
                                                            {{ ucfirst($socialLink->link_type) }}</a>
                                                    </li>
                                                @elseif($socialLink->link_type == 'web')
                                                    <li>
                                                        <a href="{{ $socialLink->url }}"
                                                           target="_blank">
                                                            <span><i class="fa fa-globe" aria-hidden="true"></i></span>
                                                            {{ str_limit($socialLink->url, $limit = 22, $end = '...') }}
                                                        </a>
                                                    </li>
                                                @else
                                                    <li>
                                                        <a href="{{ $socialLink->url }}"
                                                           target="_blank">
                                                    <span><i class="fa fa-{{ $socialLink->link_type }}"
                                                             aria-hidden="true"></i></span>
                                                            {{ ucfirst($socialLink->link_type) }}</a>
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>

                </div>

                <div class="col-md-9 col-sm-8 col-xs-12 proifle-post-sec">
                    @if($me && ($me->isFriendWith($user) || ($me->id == $user->id || $me->role_id == 2)))
                        <div class="row">
                            <div class="col-md-offset-0 pfl-info">
                                @if(count($errors) > 0)
                                    <div class="flash-message flash-message--error">
                                        <strong>Es ist ein Fehler aufgetreten:</strong>
                                        <ul>
                                            @foreach($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                <section id="pinnwand" class="pageable-menu__page pageable-menu__page--active">
                                    <h2 class="clubname">{{ $user->fullName() }}</h2>
                                    <h2 class="club-content__box-headline">PINNWAND</h2>
                                    <div class="club-post__wrapper">
                                        @include('user.profile.partials.post.list', ['user' => $user, 'posts' => $posts])
                                    </div>
                                </section>

                                @can('edit', $user)
                                    <section id="informationen" class="pageable-menu__page pageable-menu__page--active">
                                        <h2>{{ $user->fullName() }}</h2>
                                        <h2 class="club-content__box-headline">Informationen</h2>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                    <div class="col-lg-6 col-md-6 col-sm-6  col-xs-12">
                                                        <div class="shaddow-box">
                                                            <ul>
                                                                <li><span class="icon-area"><i class="fa fa-envelope"
                                                                                               aria-hidden="true"></i></span>
                                                                    <span class="txt-area"><a
                                                                                href="mailto:{{ $user->email }}">{{ $user->email }}</a></span>
                                                                    @can('edit', $user)
                                                                        <button class="btn btn--inline btn--icon animation pull-right edit-email">
                                                                            <i class="fa fa-pencil"
                                                                               aria-hidden="true"></i>
                                                                        </button><br>
                                                                        <!-- skhan: User hat die Möglichkeit sein Konto zu löschen -->
                                                                        <a href="#"
                                                                           class="btn btn--red"
                                                                           data-modal-url=" {{ route('account-delete-request', $user->username)}}"
                                                                           data-modal="team-member"
                                                                           style="color: white;">Konto
                                                                            löschen</a>
                                                                    @endcan
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @can('edit', $user)
                                            @include('user.edit-email', ['user' => $user] )
                                        @endcan
                                        @can('edit', $user)
                                            @include('user.edit-profile', ['user' => $user] )
                                        @endcan
                                    </section>
                                @endcan

                                @can('edit', $user)
                                    <section id="passwort" class="pageable-menu__page pageable-menu__page--active">
                                        <h2>{{ $user->fullName() }}</h2>
                                        <h2 class="club-content__box-headline">Passwort ändern</h2>
                                        @include('user.edit-password', ['user' => $user] )
                                    </section>
                                @endcan

                                <section id="sportvita" class="pageable-menu__page pageable-menu__page--active">
                                    <h2>{{ $user->fullName() }}</h2>
                                    <h2 class="club-content__box-headline">Sport-Vita</h2>
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="tabset">
                                                <input type="radio" name="tabset" checked id="tab2"
                                                       aria-controls="erfolge">
                                                <label for="tab2">Vereine</label>

                                                <input type="radio" name="tabset" id="tab3" aria-controls="interessen">
                                                <label for="tab3">Interessen</label>

                                                <div class="tab-panels">
                                                    <section id="erfolge" class="tab-panel">
                                                        @can('edit', $user)
                                                            <div class="row" id="suggestedclub" style="display:none;">
                                                                <div class="col-xs-12 input-hint pull-right">
                                                                    Nicht gefunden?
                                                                    <a href="#" class=""
                                                                       data-modal-url1="{{ route('user.club.wizard')}}"
                                                                       data-modal1="team-member" id="clubwizard">
                                                                        Verein vorschlagen
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><a
                                                                            class="pull-right profile-plus-icon"
                                                                            data-sport-career-add>
                                                                        <i class="fa fa-plus-circle"
                                                                           aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        @endcan
                                                        @if($sportCareers->count() > 0)
                                                            <div class="row">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="well">
                                                                        @include('user.profile.partials.sport-career.list')
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @else
                                                            <div class="row">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    {{--@include('user.profile.partials.sport-career.edit.form')--}}
                                                                </div>
                                                            </div>
                                                            @endif
                                                    </section>

                                                    <section id="interessen" class="tab-panel">
                                                        <div class="row">
                                                            <div class="col-lg-12 col-md-12 col-sn-12 col-xs-12">
                                                                <div class="well">
                                                                    <div id="sport-interest" class="profile-interests"
                                                                         data-sport-interest-edit-url="{{ route('user.sport.edit', $user->id) }}"
                                                                         data-interest-url="{{ route('user.sport', $user->id) }}">

                                                                        @can('edit', $user)
                                                                            <button data-edit-sport-interest
                                                                                    class="btn btn--inline btn--icon animation pull-right sport-edit">
                                                                                <i class="fa fa-pencil"
                                                                                   aria-hidden="true"></i>
                                                                            </button>
                                                                        @endcan
                                                                        <div data-sport-interest-wrapper
                                                                             data-sport-interest-template-url="{{ route('user.sport.template', $user->id) }}"
                                                                             data-sport-interest-edit-url="{{ route('user.sport.edit', $user->id) }}">
                                                                            @include('user.profile.partials.sport-interest')
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>

                                <section id="netzwerk" class="pageable-menu__page pageable-menu__page--active">
                                    <h2>{{ $user->fullName() }}</h2>
                                    <h2 class="club-content__box-headline">Netzwerk</h2>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="tabset">
                                                <input type="radio" name="user_type" value="friend" id="tab4"
                                                       aria-controls="friend" checked>
                                                <label for="tab4">Freunde</label>
                                                <input type="radio" name="user_type" value="u_vereine" id="tab5"
                                                       aria-controls="vereine">
                                                <label for="tab5">Vereine</label>

                                                <div class="row network-search-area">
                                                    <div class="col-lg-5 col-sm-6">
                                                        <div class="input-group">
													<span class="input-group-addon">
													   <i class="fa fa-search" aria-hidden="true"></i>
													</span>
                                                            <input type="text" id="search" name="search"
                                                                   class="form-control"
                                                                   placeholder="in meinem Netzwerk finden">
                                                        </div>
                                                    </div>
                                                    {{--<div class="col-lg-6 col-sm-6 invite-membrs">--}}
                                                    {{--<a href="{{ url('suche/vereine') }}">Neue Mitglieder einladen <i--}}
                                                    {{--class="fa fa-angle-right"--}}
                                                    {{--aria-hidden="true"></i></a>--}}
                                                    {{--</div>--}}
                                                </div>

                                                <div class="tab-panels">
                                                    <section id="friend" class="tab-panel">
                                                        @include('user.partials.friend')
                                                    </section>

                                                    <section id="u_vereine" class="tab-panel">
                                                        @include('user.partials.vereine')
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>

                                <section id="fotos" class="pageable-menu__page pageable-menu__page--active">
                                    <h2>{{ $user->fullName() }}</h2>
                                    <h2 class="club-content__box-headline">Fotos</h2>
                                    @include('user.profile.partials.post.image', ['user' => $user, 'posts' => $posts])
                                </section>
                            </div>
                        </div>
                    @else
                        <h2>{{ $user->fullName() }}</h2>
                    @endif
                </div>
            </div>
        </div>

    </section>

    @if(Auth::check())
        @include('user.profile.modal.profile-info-wizard', ['me' => $me, 'state' => $state])
    @endif
@endsection
@section('post-js')
    <script src="{{ asset(('js/user-profile.js')) }}"></script>
    {{--<script src="{{ asset(('js/user-stream.js')) }}"></script>--}}
@endsection