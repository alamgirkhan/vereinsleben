<div id="modal" class="modal__container modal__container--opend">
    <section class="modal">
        <div class="modal__content">
            <div class="modal__inner">
                <div id="wizard_info" class="rgst-box-inner">
                    <h2>Möchtest du dein Konto wirklich löschen?</h2>
                    <div class="modal__btns">
                        <a class="btn btn-default finden-btn" href="/benutzer/{username}/delete">Konto löschen</a>
                        <a class="btn btn-default finden-btn modal__close_2" data-modal-close
                           aria-hidden="true">Abbrechen</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>