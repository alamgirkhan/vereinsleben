<div id="model" class="modal__container  modal__container--opend">
    <section class="modal">
        <div class="modal__content">
            {!! Form::model($me, ['route' => 'sport.career.club.wizard', 'files' => true, 'id' => 'form_clubwizard', 'class' => 'mainrgstr', 'method' => 'POST']) !!}
            <div class="modal__inner">
                <div id="wizard_state" class="rgst-box-inner wizardbox">
                    <h2>Dein Verein fehlt?</h2>
                    <p>Der Verein, den Du suchst, ist möglicherweise noch nicht bei uns eingetragen. Wenn Du uns ein
                        paar Informationen zu dem Verein gibst, können wir das ändern.</p>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="input-label wlabel" for="firstname">Name des Vereins *</label>
                                    <div class="input-group wgroup">
                                        <input placeholder="Name des Vereins" class="input wizardinput" type="text"
                                               name="wcclub_id" id="wcname" value="" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="input-label wlabel" for="Sportart">Sportart(en) *</label>
                                    <div class="input-group wgroup">
                                        <div class="input-group wgroup">
                                            {!! Form::select('wcsports', $sports, '', ['class' => 'select', 'placeholder' => 'Bitte wählen', 'id' => 'wcsport', 'required' => 'required']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal__btns">
                        <a class="btn btn-default finden-btn" href="javascript:void(0);" onClick="location.reload();">Abbrechen</a>
                        <a class="btn btn-default finden-btn" href="javascript:void(0);"
                           onClick="nextStepClub('state', 'postleitzahl','1')">Weiter</a>
                    </div>
                </div>

                <div id="wizard_postleitzahl" class="rgst-box-inner wizardbox" style="display:none;">
                    <h2>An welchem Ort ist der Verein?</h2>
                    <p>Wenn Du uns die Postleitzahl und den Ort nennen kannst, können andere Benutzer den Verein
                        leichter bei uns finden.</p>
                    <br>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                            <label class="input-label wlabel" for="zip">Postleitzahl *</label>
                            <div class="input-group wgroup">
                                <input placeholder="Postleitzahl" class="input wizardinput" type="text" name="wczip"
                                       id="wczip" required="required" value="">
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                            <label class="input-label wlabel" for="city">Ort *</label>
                            <div class="input-group wgroup">
                                <input placeholder="Ort" class="input wizardinput" type="text" name="wccity" id="wccity"
                                       required="required" value="">
                            </div>
                        </div>
                    </div>
                    <div class="modal__btns">
                        <a class="btn btn-default finden-btn" href="javascript:void(0);" onClick="location.reload();">Beenden</a>
                        <a class="btn btn-default finden-btn" href="javascript:void(0);"
                           onClick="nextStepClub('postleitzahl', 'street','0');">Überspringen</a>
                        <a class="btn btn-default finden-btn" href="javascript:void(0);"
                           onClick="nextStepClub('postleitzahl', 'street','1');">Weiter</a>
                    </div>
                </div>

                <div id="wizard_street" class="rgst-box-inner wizardbox" style="display: none;">
                    <h2>In welcher Straße ist der Verein?</h2>
                    <p>Wenn Du die Straße und Hausnummer kennst und hier einträgst, wäre die Anschrift komplett.</p>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                                    <label class="input-label wlabel" for="street">Straße</label>
                                    <div class="input-group wgroup">
                                        <input placeholder="Straße" class="input wizardinput " type="text"
                                               name="wcstreet" id="wcstreet" value="">
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                                    <label class="input-label wlabel" for="house_number">Hausnummer</label>
                                    <div class="input-group wgroup">
                                        <input placeholder="Hausnummer" class="input wizardinput" type="text"
                                               name="wchouse_number" id="wchouse_number" value="">
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal__btns">
                        <a class="btn btn-default finden-btn" href="javascript:void(0);" onClick="location.reload();">Beenden</a>
                        <a class="btn btn-default finden-btn" href="javascript:void(0);"
                           onClick="nextStepClub('street', 'email','0');">Überspringen</a>
                        <a class="btn btn-default finden-btn" href="javascript:void(0);"
                           onClick="nextStepClub('street', 'email','1');">Weiter</a>
                    </div>
                </div>

                <div id="wizard_email" class="rgst-box-inner wizardbox" style="display: none;">
                    <h2>Website des Vereins</h2>
                    <p>Wenn wir auf dem Vereinsprofil einen Link zur Vereinswebsite hätten, können Interessierte dort
                        weitere Informationen bekommen.</p>
                    <br>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label class="input-label wlabel" for="city">Website</label>
                            <div class="input-group wgroup">
                                <input placeholder="Website" class="input wizardinput" type="text" name="wcweb"
                                       id="wcweb" value="">
                            </div>
                        </div>
                    </div>

                    <div class="modal__btns">
                        <a class="btn btn-default finden-btn" href="javascript:void(0);" onClick="location.reload();">Beenden</a>
                        <a class="btn btn-default finden-btn" href="javascript:void(0);"
                           onClick="nextStepClub('email', 'photo','0');">Überspringen</a>
                        <a class="btn btn-default finden-btn" href="javascript:void(0);"
                           onClick="nextStepClub('email', 'photo','1')">Weiter</a>
                    </div>
                </div>

                <div id="wizard_photo" class="rgst-box-inner" style="display: none;">
                    <h2>Logo oder Wappen des Vereins</h2>
                    <p>Mit einem Vereinslogo oder -wappen sieht das Vereinsprofil natürlich viel ansprechender aus.</p>
                    <br>
                    <div class="row">
                        <div class="col-xs-12">
                            <div data-upload-result>
                                <label for="club_logo"
                                       class="button button--grey button--center button--icon button--full-width">
                                    <span class="fa fa-upload"></span>Bild hochladen</label>
                                <div class="input-group">
                                    <input type="file" name="wcimage" id="club_logo" class="input-file"
                                           data-preview-image>
                                </div>
                                <div id="upload-result" class="thumbnail__list"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal__btns">
                        <a class="btn btn-default finden-btn" href="javascript:void(0);" onClick="location.reload();">Beenden</a>
                        <a class="btn btn-default finden-btn" href="javascript:void(0);"
                           onClick="nextStepClub('photo', 'dob','0');">Überspringen</a>
                        <a class="btn btn-default finden-btn" href="javascript:void(0);"
                           onClick="nextStepClub('photo', 'dob','1')">Weiter</a>
                    </div>
                </div>

                <div id="wizard_dob" class="rgst-box-inner wizardbox" style="display: none;">
                    <h2>Wann warst Du in dem Verein?</h2>
                    <p>Um diesen Verein in Deiner Sport-Vita zu hinterlegen, kannst du hier einfach noch den Zeitraum
                        deiner Mitgliedschaft eintragen.</p>
                    <br>
                    <div class="row von">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            {!! Form::label('wctimeline_begin', 'Von *', ['class' => 'input-label']) !!}
                            <div class="input-group row">
                                <div class="col-md-6">
                                    {!! Form::select('wctimeline_begin_month',  array('' => 'Monat') + array_combine(range(1, 12), range(1, 12)), null, ['required', 'class' => 'select','id' => 'wctimeline_begin_month']) !!}
                                </div>
                                <div class="col-md-6">
                                    {!! Form::select('wctimeline_begin_year',  array('' => 'Jahr') + array_combine(range(1900, date('Y')), range(1900, date('Y'))), null, ['required', 'class' => 'select','id' => 'wctimeline_begin_year']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            {!! Form::label('wctimeline_end', 'Bis', ['class' => 'input-label']) !!}
                            <div class="input-group input-group--no-bottom-spacing row">
                                <div class="col-md-6">
                                    {!! Form::select('wctimeline_end_month',  array('' => 'Monat') + array_combine(range(1, 12), range(1, 12)), null, ['class' => 'select','id' => 'wctimeline_end_month']) !!}
                                </div>
                                <div class="col-md-6">
                                    {!! Form::select('wctimeline_end_year',  array('' => 'Jahr') + array_combine(range(1900, date('Y')), range(1900, date('Y'))), null, ['class' => 'select','id' => 'wctimeline_end_year']) !!}
                                </div>
                            </div>
                            <span class="input-hint" style="display:inline-block">
                                    Aktuell noch aktiv im Verein? Dann einfach leer lassen.
                                </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            {!! Form::label('career_type', 'Funktion', ['class' => 'input-label wlabel']) !!}
                            <div class="wgroup input-group input-group--no-spacing">
                                {!! Form::select('wccareer_type', ['Spieler' => 'Spieler','Trainer' => 'Trainer','Funktionär' => 'Funktionär',], '', ['class' => 'select', 'placeholder' => 'Bitte wählen', 'id' => 'wccareer_type', 'required' => 'required']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="modal__btns">
                        <a class="btn btn-default finden-btn" href="javascript:void(0);" onClick="location.reload();">Abbrechen</a>
                        <a class="btn btn-default finden-btn" href="javascript:void(0);"
                           onClick="nextStepClub('dob', 'thanks','1');">Fertigstellen</a>
                    </div>
                </div>

                <div id="wizard_thanks" class="rgst-box-inner" style="display:none;">
                    <h2>Vielen Dank für Deine Hilfe!</h2>
                    <p>Mit Deiner Hilfe können wir vereinsleben.de nun ein Stückchen besser machen und dafür sagen wir
                        Danke!</p>
                    <div class="modal__btns">
                        <a href="javascript:void('0');" onClick="nextStepClub('thanks','dob','1')"
                           class="btn btn-default finden-btn">Schließen</a>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </section>
</div>
<style>
    .error {
        border: 1px solid red;
    }

    .wizardinput {
        padding: 8px 8px 8px 10px !important;
        padding-left: 20px !important;
    }

    .wgroup {
        margin: 5px 0 !important;
        border: none !important;
    }

    .wlabel {
        padding-left: 0px !important;
    }

    .wizardbox {
        padding: 10% 7% 0 7% !important;
    }
</style>