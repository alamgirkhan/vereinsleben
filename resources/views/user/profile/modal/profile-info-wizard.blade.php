@if(!$me->isProfileCompleted())
    <?php
            if (isset($_COOKIE["hideProfilVervollstaendigung"] ))
            {
                $hideCookie = $_COOKIE["hideProfilVervollstaendigung"] ;
            }else{
                $hideCookie = 0;
            }

            /*@if ($me->federal_state == null or strlen($me->federal_state)<1 or $hideCookie != "1")*/
            /*
             * $thisIsVotingPage ist gestetzt in VoteController.php overview()!
             * */
    ?>
    @if ($me->federal_state == null or strlen($me->federal_state)<1 or (isset($thisIsVotingPage) and $thisIsVotingPage == 1) or $hideCookie != "1")

        <div id="modal"
             class="modal__container @if(isset($show)) modal__container--closed @else modal__container--opend @endif">
            <section class="modal">
                <div class="modal__content">
                    {!! Form::model($me, ['route' => 'user.wizard.profile.update', 'id' => 'form_wizard', 'class' => 'mainrgstr', 'method' => 'POST']) !!}
                    <input type="hidden" id="save" name="save" value="0"/>
                    <input type="hidden" id="current" name="current" value=""/>
                    <div class="modal__inner">
                        @if($me->federal_state == null)
                            <div id="wizard_state" class="rgst-box-inner">
                                <h2>In welchem Bundesland lebst Du?</h2>
                                <p>Damit wir Dir die passenden News und Infos anzeigen können, müssen wir wissen, in welchem
                                    Bundesland Du wohnst, bzw. aus welchem Bundesland Du die Informationen möchtest.
                                    Diese Auswahl kannst Du jederzeit in deinen Profileinstellungen ändern.</p>
                                <br/>
                                <div class="col-md-12">
                                    {!! Form::label('state', 'Bundesland', ['class' => 'input-label']) !!}
                                    <div class="input-group">
                                        {!! Form::select('state', array('' => 'Bitte wählen')+$state, null, ['required', 'class' => 'select', 'id' => 'wstate']) !!}
                                    </div>
                                </div>
                                <div class="modal__btns"><a class="btn btn-default finden-btn"
                                                            onClick="javascript:nextStep('state','1');">Weiter</a></div>

                            </div>
                        @endif

                        @if($me->gender == null || $me->firstname == null || $me->lastname == null || $me->birthday == null || $me->city == null)

                            <div id="wizard_info" class="rgst-box-inner"
                                 style="@if($me->federal_state == null) display:none; @endif">
                                <h2>Bitte vervollständige Dein Profil</h2>
                                <p>Um vereinsleben.de optimal auf Deine Bedürfnisse anzupassen und damit Du alle Vorteile
                                    und
                                    Funktionen nutzen kannst, benötigen wir noch ein paar Informationen von Dir. Alle
                                    Angaben
                                    kannst Du natürlich jederzeit in deinen Profileinstellungen ändern.</p>
                                <br/>
                                <div class="modal__btns">
                                    <a class="btn btn-default finden-btn" onClick="javascript:nextStep('close',0);">Später</a>
                                    <a class="btn btn-default finden-btn" onClick="javascript:nextStep('info','1');">Jetzt ausfüllen</a>
                                </div>
                            </div>

                        @endif

                        @if($me->gender == null || $me->firstname == null || $me->lastname == null)
                            <div id="wizard_anrede" class="rgst-box-inner" style="display:none;">
                                <h2>Wie heisst Du?</h2>
                                <p>Deine Freunde und Vereinskameraden finden Dich natürlich nur, wenn Du Deinen Namen
                                    angibst
                                    und auch wir würden Dich sehr gerne persönlich begrüßen.</p>
                                <div class="row">
                                    <div class="col-md-12">
                                        <form action="{{ url('/register') }}" method="POST" id="form_register"
                                              class="mainrgstr">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    {!! Form::label('gender', 'Anrede', ['class' => 'input-label']) !!}
                                                    <div class="input-group">
                                                        {!! Form::select('gender', ['' => 'Bitte wählen', 'male' => 'Herr', 'female' => 'Frau'], null, ['required' => 'required', 'id' => 'wgender', 'class' => 'select', 'pattern' => '^[male|female]']) !!}
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <label class="input-label" for="firstname">Vorname(n)</label>
                                                    <div class="input-group">
                                                        <span class="fa fa-user input-icon"></span>
                                                        <input placeholder="Vorname" class="input" type="text"
                                                               name="firstname"
                                                                minlength="2"
                                                               id="wfirstname" value="{{ $me->firstname }}" required="required" pattern="^[a-zA-ZäöüÄÖÜß\-. ]+$">

                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <label class="input-label" for="lastname">Nachname</label>
                                                    <div class="input-group">
                                                        <span class="fa fa-user input-icon"></span>
                                                        <input placeholder="Nachname" class="input" type="text"
                                                               name="lastname"
                                                                minlength="2"
                                                               id="wlastname" value="{{ $me->lastname }}" required="required" pattern="^[a-zA-ZäöüÄÖÜß\-. ]+$">
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="modal__btns">
                                    <a class="btn btn-default finden-btn"
                                       onClick="javascript:nextStep('close',0);">Beenden</a>
                                    <a class="btn btn-default finden-btn" onClick="javascript:nextStep('anrede',0);">Überspringen</a>
                                    <a class="btn btn-default finden-btn" onClick="javascript:nextStep('anrede','1');">Weiter</a>
                                </div>
                            </div>
                        @endif

                        @if($me->birthday == null)
                            <div id="wizard_dob" class="rgst-box-inner" style="display:none;">
                                <h2>Wie alt bist Du?</h2>
                                <p>Natürlich gratulieren wir Dir auch gerne zum Geburtstag, aber vor allem benötigen wir
                                    Dein Geburtstdatum, um Dein Alter für die Teilnahme an Aktionen und Gewinnspielen zu
                                    verifizieren.</p>
                                <div class="row">
                                    <label class="input-label" for="birthday">Geburtsdatum</label>
                                    <div class="input-group">
                                        <div class="col-md-4">
                                            {!! Form::select('birthday-day', array('' => 'Tag') + array_combine(range(1, 31), range(1, 31)), isset($me->birthday->day) ? $me->birthday->day : null, ['required', 'class' => 'select', 'id' => 'wbirthday-day']) !!}
                                        </div>
                                        <div class="col-md-4">
                                            {!! Form::select('birthday-month', array('' => 'Monat') + array_combine(range(1, 12), range(1, 12)), isset($me->birthday->month) ? $me->birthday->month : null, ['required', 'class' => 'select', 'id' => 'wbirthday-month']) !!}
                                        </div>
                                        <div class="col-md-4">
                                            {!! Form::select('birthday-year', array('' => 'Jahr') + array_combine(range(1900, date('Y')), range(1900, date('Y'))), isset($me->birthday->year) ? $me->birthday->year : null, ['required', 'class' => 'select', 'id' => 'wbirthday-year']) !!}
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                <div class="modal__btns">
                                    <a class="btn btn-default finden-btn"
                                       onClick="javascript:nextStep('close',0);">Beenden</a>
                                    <a class="btn btn-default finden-btn" onClick="javascript:nextStep('dob',0);">Überspringen</a>
                                    <a class="btn btn-default finden-btn"
                                       onClick="javascript:nextStep('dob','1');">Weiter</a>
                                </div>

                            </div>
                        @endif

                        @if($me->zip == null || $me->city == null)
                            <div id="wizard_postleitzahl" class="rgst-box-inner" style="display:none;">
                                <h2>Wo wohnst Du?</h2>
                                <p>Diese Information ist wichtig, damit wir Dir z.B. Sportveranstaltungen und -events in
                                    Deiner Nähe vorschlagen können, die für Dich interessant sein könnten.</p>
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                                        <label class="input-label" for="zip">Postleitzahl</label>
                                        <div class="input-group">
                                            <input placeholder="Postleitzahl" class="input" type="text" name="zip"
                                                   id="wzip" required="required" value="" pattern="^([0-9]{4,5})$">
                                        </div>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                                        <label class="input-label" for="city">Ort</label>
                                        <div class="input-group">
                                            <input placeholder="Ort" class="input" type="text" name="city"
                                                   id="wcity" required="required" value="" pattern="^(([a-zA-ZäöüÄÖÜß]{2,})([-. ]+)?)+">
                                            {{--<input placeholder="Ort" class="input" type="text" name="city"--}}
                                                   {{--id="wcity" required="required" value="" pattern="^[A-Za-zöÖäÄüÜß.]+[- ]?[A-Za-zöÖäÄüÜß.]+[- ]?[A-Za-zöÖäÄüÜß.]+?">--}}
                                        </div>
                                    </div>
                                </div>


                                <div class="modal__btns">
                                    <a class="btn btn-default finden-btn"
                                       onClick="javascript:nextStep('close',0);">Beenden</a>
                                    <a class="btn btn-default finden-btn" onClick="javascript:nextStep('postleitzahl',0);">Überspringen</a>
                                    <a class="btn btn-default finden-btn"
                                       onClick="javascript:nextStep('postleitzahl','1');">Weiter</a>
                                </div>

                            </div>
                        @endif

                        <div id="wizard_thanks" class="rgst-box-inner" style="display:none;">
                            <h2>Dein Profil wurde erfolgreich aktualisiert. Vielen Dank!</h2>
                            <br/>
                            <div class="modal__btns">
                                <a onClick="javascript:nextStep('thankyou',0);" class="btn btn-default finden-btn"
                                   aria-hidden="true">Schließen</a>
                            </div>
                        </div>

                        <div id="wizard_final" class="rgst-box-inner" style="display:none;">
                            <h2></h2>
                            <p>Du hast noch nicht alle notwendigen Angaben gemacht, um alle Vorteile und Funktionen von
                                vereinsleben.de nutzen zu können. Möchtest Du die fehlenden Informationen jetzt
                                eintragen?</p>
                            <div class="modal__btns">
                                <a class="btn btn-default finden-btn" onClick="javascript:nextStep('close',0);">Später</a>
                                <a class="btn btn-default finden-btn" onClick="javascript:nextStep('final', 0);">Jetzt</a>
                            </div>
                        </div>

                    </div>
                </div>
                </form>
            </section>
        </div>


        <style>
            .error {
                border: 1px solid red;
            }
        </style>
    @endif
@endif