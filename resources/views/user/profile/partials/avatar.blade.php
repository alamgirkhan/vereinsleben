<section class="profile-section profile-section--avatar">
    <div class="profile-avatar">
        <div class="profile-avatar__image-container">
            @can('edit', $user)
                <div class="btn btn--icon profile__edit-button animation animation--scale" data-call-popover="avatar">
                    <i class="fa fa-upload" aria-hidden="true"></i>
                </div>
            @endcan
            <div class="profile-avatar__image-wrapper col-lg-12 col-md-12 col-sm-12 col-xs-3 profile-pic">
                <img class="profile-avatar__image" src="{{ asset($user->avatar->url('singleView')) }}"
                     data-upload-image/>
            </div>
        </div>

        @can('edit', $user)
            @include('user.profile.partials.edit.avatar', ['user' => $user])
        @endcan
    </div>
</section>