<div class="popover popover--bottom-center popover--avatar" data-popover="avatar">
    {!! Form::model(
        $user,
        array(
            'method' => 'patch',
            'route' => ['user.profile.patch', $user->username],
            'class' => 'profile-avatar-edit__form',
            'files' => 'true'
        )
    )!!}
    <div class="popover__inner">
        <div class="popover__controlls tooltip">
            <label for="avatar" class="btn btn--icon-switch">
                <i class="fa fa-upload" aria-hidden="true"></i>
            </label>
            {{ csrf_field() }}
            <div class="popover__controlls popover__controlls--hidden popover__controlls--avatar">
                <a href="#" class="btn btn--icon-switch" id="avatar-zoom-out">
                    <span class="fa fa-search-minus"></span>
                </a>
                <a href="#" class="btn btn--icon-switch" id="avatar-zoom-in">
                    <span class="fa fa-search-plus"></span>
                </a>
            </div>

            <input type="file" name="avatar" id="avatar" class="popover__upload hidden-input"
                   data-image="profile-avatar__image"
                   data-width="293"
                   data-height="293"
                   data-zoom-in="avatar-zoom-in"
                   data-zoom-out="avatar-zoom-out"
                   data-crop-props="avatar-crop-props"
            >
            <input type="hidden" name="crop-props" id="avatar-crop-props">
        </div>
    </div>
    <div class="profile-avatar-edit__form-action-wrapper">
        <a href="#"
           id="profile-avatar-edit__form-cancel-action-button"
           class="btn btn--full-width btn--grey btn--upper btn--large">
            Abbrechen
        </a>
        {!! Form::submit('Speichern',
            array(
                'class' => 'btn btn--full-width btn--red btn--upper btn--large'
            ))
        !!}
    </div>
    {!! Form::close() !!}
</div>