<div class="popover popover--bottom-right popover--header" data-popover="header">
    {!! Form::model(
        $user,
        array(
            'method' => 'patch',
            'route' => ['user.profile.patch', $user->username],
            'class' => 'profile-header-edit__form',
            'files' => 'true'
        )
    )!!}
    <div class="popover__inner">
        <div class="popover__controlls tooltip">
            <label for="header" class="btn btn--icon-switch">
                <i class="fa fa-upload" aria-hidden="true"></i>
            </label>

            <div class="popover__controlls popover__controlls--hidden popover__controlls--header">
                <a href="#" class="btn btn--icon-switch" id="header-zoom-out">
                    <span class="fa fa-search-minus"></span>
                </a>
                <a href="#" class="btn btn--icon-switch" id="header-zoom-in">
                    <span class="fa fa-search-plus"></span>
                </a>
            </div>

            <input type="file" name="header" id="header" class="popover__upload hidden-input"
                   data-image="profile-header__image"
                   data-width="1920"
                   data-height="492"
                   data-zoom-in="header-zoom-in"
                   data-zoom-out="header-zoom-out"
                   data-crop-props="header-crop-props"
            >
            <input type="hidden" name="crop-props" id="header-crop-props">
        </div>
    </div>
    <div class="profile-header-edit__form-action-wrapper">
        <a href="#"
           id="profile-header-edit__form-cancel-action-button"
           class="btn btn--full-width btn--grey btn--upper btn--large">
            Abbrechen
        </a>
        {!! Form::submit('Speichern',
            array(
                'class' => 'btn btn--full-width btn--red btn--upper btn--large'
            ))
        !!}
    </div>
    {!! Form::close() !!}
</div>
