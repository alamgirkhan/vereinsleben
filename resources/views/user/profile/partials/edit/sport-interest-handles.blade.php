<div data-sport-interest-options class="profile-interests__button-group">
    <div class="row">
        <div class="col-xs-12">
            <button class="pull-left profile-plus-icon interessen-add" data-sport-interest-add>
                <span><i class="fa fa-plus-circle" aria-hidden="true"></i></span>
            </button>
        </div>
        <div class="col-xs-12">
            <br>
            <button class="btn btn--inline btn--grey btn--large btn--upper" data-sport-interest-cancel>
                <i class="fa fa-close" aria-hidden="true"></i> Abbrechen
            </button>
            <button class="btn btn--inline btn--red btn--large btn--upper" data-sport-interest-save="{{ route('user.sport.store') }}">
                <i class="fa fa-save" aria-hidden="true"></i> Speichern
            </button>
        </div>
    </div>
</div>