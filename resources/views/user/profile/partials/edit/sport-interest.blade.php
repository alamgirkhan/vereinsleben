<div data-sport-interest>
    <div class="profile-rangeslider__container">
        <div class="profile-rangeslider__inner slidecontainer">
            <select data-sport-autocomplete="{{ app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('api.sports') }}">
                @if(isset($userSport))
                    <option value="{{ $userSport->sport->id }}">{{ $userSport->sport->title }}</option>
                @endif
            </select>
            <div class="" data-interest-slider-wrapper>
                <input
                        data-interest-slider
                        type="range"
                        min="0"
                        max="100"
                        step="1"
                        value="{{ $userSport->interest or 30 }}"
                        data-orientation="horizontal"
                        style="width:100%;"						
                >
            </div>
            <div class="profile-rangeslider__scala">
                <div class="profile-rangeslider__scala-middle"></div>
                <div class="profile-rangeslider__scala-desc inter">interessiert</div>
                <div class="profile-rangeslider__scala-desc bege">begeistert</div>
                <div class="clear"></div>
            </div>

            <button class="btn btn--full-width sport-delete"
                    data-sport-interest-delete-url="{{ route('user.sport.delete') }}"
                data-sport-interest-delete="{{ $userSport->id or '' }}"><i class="fa fa-trash" aria-hidden="true"></i> Löschen
            </button>
        </div>
    </div>
</div>