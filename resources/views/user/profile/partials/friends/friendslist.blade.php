<table class="table table-striped table-bordered">
    <tr>
        <th><strong>username</strong></th>
        <th><strong>firstname</strong></th>
        <th><strong>lastname</strong></th>
        <th></th>
    </tr>
    @foreach($friends as $friend)
        @if(!$user->isBlockedBy($friend))
            <tr>
                <td>{{ $friend->username}}</td>
                <td>{{ $friend->firstname }}</td>
                <td>{{ $friend->lastname }}</td>
                <td>
                    @if($user->hasSentFriendRequestTo($friend))
                        friend request sent
                    @elseif($friend->hasSentFriendRequestTo($user))

                        {!! Form::model(
                            $user,
                            array(
                                'method' => 'patch',
                                'route' => ['user.profile.friendship', $user->username, $friend->username],
                                'class' => '',
                                'files' => 'false'
                            )
                        )!!}
                        {!! Form::submit('accept', array('name'=> 'relation', 'class' => 'button button--light button--light-white button--center')) !!}
                        {!! Form::submit('refuse', array('name'=> 'relation', 'class' => 'button button--light button--light-white button--center')) !!}
                        {!! Form::close() !!}

                    @elseif($user->isFriendWith($friend))

                        {!! Form::model(
                            $user,
                            array(
                                'method' => 'patch',
                                'route' => ['user.profile.edit_friendship', $user->username, $friend->username],
                                'class' => '',
                                'files' => 'false'
                            )
                        )!!}
                        {!! Form::submit('remove', array('name'=> 'relation', 'class' => 'button button--light button--light-white button--center')) !!}
                        {!! Form::submit('block', array('name'=> 'relation', 'class' => 'button button--light button--light-white button--center')) !!}
                        {!! Form::close() !!}

                    @elseif($user->hasBlocked($friend))
                        {!! Form::model(
                            $user,
                            array(
                                'method' => 'patch',
                                'route' => ['user.profile.edit_friendship', $user->username, $friend->username],
                                'class' => '',
                                'files' => 'false'
                            )
                        )!!}
                        {!! Form::submit('unblock', array('name'=> 'relation', 'class' => 'button button--light button--light-white button--center')) !!}
                        {!! Form::close() !!}
                    @else
                        {!! Form::model(
                            $user,
                            array(
                                'method' => 'patch',
                                'route' => ['user.profile.friendship', $user->username, $friend->username],
                                'class' => '',
                                'files' => 'false'
                            )
                        )!!}
                        {!! Form::submit('send-friend-request', array('name'=> 'relation', 'class' => 'button button--light button--light-white button--center')) !!}
                        {!! Form::close() !!}
                    @endif
                </td>
            </tr>
        @endif
    @endforeach
</table>