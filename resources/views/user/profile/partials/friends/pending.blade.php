<div class="col-xs-12 col-sm-6 col-md-6">
    <div class="fan-box">
        <div class="row">
            <div class="col-lg-3 col-sm-6">
                <div class="thum-bg">
                    <a href="{{ route('user.detail', $user->username) }}"><img
                                src="{{ $user->avatar->url('singleView') }}" alt=""></a>
                </div>
            </div>
            <div class="col-lg-9 col-sm-6">
                <div class="fan-box-detail">
                    <h3><a href="{{ route('user.detail', $user->username) }}" class="member-card__link">
                            @if(!$user->firstname && !$user->lastname)
                                {{ $user->username }}
                            @else
                                {{ $user->firstname or '' }}
                                {{ $user->lastname or '' }}
                            @endif
                        </a>
                    </h3>
                </div>
                <div class="fan-lctn"><i class="fa fa-map-marker" aria-hidden="true"></i> {{ $user->city }}</div>
                <div class="folgen-btn">
                    <a href="javascript:friendshipEvent('{{ $username }}', '{{ $user->username }}', 'accept', '{{ $user->id }}');"><i
                                class="fa fa-check-square-o" aria-hidden="true"></i><span id="accept_{{ $user->id }}">Accept<span></a>
                    <a href="javascript:friendshipEvent('{{ $username }}', '{{ $user->username }}', 'deny', '{{ $user->id }}');"><i
                                class="fa fa-times-circle-o" aria-hidden="true"></i><span id="deny_{{ $user->id }}">Deny<span></a>
                </div>
            </div>
        </div>
    </div>
</div>