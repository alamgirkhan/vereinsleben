<div data-load-more='{"url": "{{ route('user.profile.friends.pending', ['username' => $user->username]) }}"}'>
    @include('user.partials.member-card-wrapper')
</div>