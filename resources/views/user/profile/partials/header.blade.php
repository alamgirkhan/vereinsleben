<section class="profile-section profile-section--header">
    <div class="profile-header">
        <div class="profile-header__image-container" style="background-image: url('{{ asset($user->header->url('singleView')) }}')">
            @can('edit', $user)
                <div class="btn btn--icon profile__edit-button animation profile__edit-button--header animation--scale"
                     data-call-popover="header">
                    <i class="fa fa-upload" aria-hidden="true"></i>
                </div>
            @endcan
            <img class="profile-header__image" src="{{ asset($user->header->url('singleView')) }}" alt=""/>
        </div>
        @can('edit', $user)
            @include('user.profile.partials.edit.header', ['user' => $user])
        @endcan
    </div>
</section>