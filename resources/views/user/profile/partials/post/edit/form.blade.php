<?php
$classes = [];
$type = 'default';
if (isset($post) && $post->images()->count() > 0) {
    $classes[] = 'profile-statusposts__item--photo';
    $type = 'photo';
} elseif (isset($post) && $post->videos()->count() > 0) {
    $classes[] = 'profile-statusposts__item--video';
    $type = 'video';
}
?>
<div class="profile-statusposts__item {{ implode(' ', $classes) }}" data-user-post data-user-post-type="{{ $type }}"
     @if (isset($post) && $post->id !== null) data-user-post-href="{{ route('user.post.template', [$user->username, $post->id]) }}" @endif>
    @if (isset($post) && $post->id !== null)
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 statusposts-lead">
                <div class="profile-statusposts__actions">
            <button class="btn btn--inline" data-user-post-cancel>
                <i class="fa fa-close" aria-hidden="true"></i> Bearbeiten abbrechen
            </button>
                </div>
            </div>
        </div>
    @endif

    @if (!isset($post) || $post->id === null)
        <?php
        $newPost = new \Vereinsleben\Post;
        if (Auth::check()) {
            $newPost->user_id = Auth::user()->id;
        }
        $newPost->published_at = date('Y-m-d H:i:s');
        $publishedDate = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $newPost->published_at);
        ?>
        {!! Form::model(
            $newPost,
            [
                'route' => ['user.post.store', $user->username],
                'enctype' => 'multipart/form-data',
                'files' => true,
                'method' => 'POST',
                'data-user-post-create-form' => '',
            ]
        )!!}
    @else
        {!! Form::model(
            $post,
            [
                'route' => ['user.post.update', $user->username, $post->id],
                'enctype' => 'multipart/form-data',
                'files' => true,
                'method' => 'POST',
                'data-user-post-edit-form' => '',
            ]
        )!!}
        <?php
        $publishedDate = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $post->published_at);
        ?>
    @endif

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-3 col-xs-4">
            <div class="profile-statusposts__date">
        <div class="profile-statusposts__day">
            {{ $publishedDate->day }}
        </div>
        <div class="profile-statusposts__month-and-year">
            <div class="profile-statusposts__month">
                {{ $publishedDate->formatLocalized('%B') }}
            </div>
            <div class="profile-statusposts__year">
                {{ $publishedDate->year }}
            </div>
        </div>
            </div>
        </div>
        <div class="col-lg-11 col-md-11 col-sm-9 col-xs-8 profile-statusposts-lead">
            <div class="profile-statusposts__form">
        {!! Form::textarea('content', null, ['class' => 'input profile-statusposts__form-input', 'rows' => 5,
            'placeholder' => 'Was machst du gerade?', 'required' => true, 'data-user-post-content-field' => true]) !!}

        <div class="profile-statusposts__advanced-options">
            <button class="profile-statusposts__advanced-options-button btn btn--inline btn--icon btn--icon-big btn--icon-switch" data-statuspost-media-button="photo">
                <i class="fa fa-camera" aria-hidden="true"></i>
            </button>
            <button class="profile-statusposts__advanced-options-button btn btn--inline btn--icon btn--icon-big btn--icon-switch" data-statuspost-media-button="video">
                <i class="fa fa-youtube-play " aria-hidden="true"></i>
            </button>
        </div>
                <div class="clear"></div>

            </div>

    <div class="profile-statusposts__edit-photo" data-statuspost-media-content="photo">
        <div id="@if (isset($post)){{ 'upload-result-'. $post->id }}@else{{ 'upload-result' }}@endif "
             class="thumbnail__list">
            @if (isset($post))
                @foreach($post->images as $image)
                    <input type="checkbox" name="delete-image[]" value="{{ $image->id }}"
                           id="event_thumbnail-{{ $image->id }}" class="thumbnail__switch">
                    <label for="event_thumbnail-{{ $image->id }}"
                           class="thumbnail__label thumbnail__container thumbnail__container--deleteable">
                        <img class="thumbnail__image" src="{{asset($image->picture->url('singleView')) }}"/>
                    </label>
                @endforeach
            @endif
        </div>

        <div data-upload-result>
            <div class="hidden" data-new-image-text>
                <span class="input-label">Neu hinzugefügte Bilder</span>
            </div>
            <div>
                <div id="@if (isset($post)){{ 'upload-result-'. $post->id }}@else{{ 'upload-result' }}@endif "
                     class="thumbnail__list">
                </div>
            </div>
            <div>
                <div>
                    <label for="@if (isset($post)){{ 'user_post_image-'. $post->id }}@else{{ 'user_post_image' }}@endif"
                           class="btn btn--grey btn--full-width btn--large">
                        <span class="fa fa-upload"></span> Bilder hochladen</label>
                    <input multiple type="file" name="images[]"
                           id="@if (isset($post)){{ 'user_post_image-'. $post->id }}@else{{ 'user_post_image' }}@endif"
                           class="input-file">
                </div>
            </div>
        </div>
    </div>

    <div class="profile-statusposts__edit-video" data-statuspost-media-content="video">
        <div>
            @if (isset($post) && $post->videos()->count() > 0 && ($video = $post->videos->first()) !== null)
                {!! Form::text('video_youtube', $video->url, ['class' => 'input profile-statusposts__form-input', 'placeholder' => 'YouTube/Vimeo Videolink']) !!}
            @else
                {!! Form::text('video_youtube', null, ['class' => 'input profile-statusposts__form-input', 'placeholder' => 'YouTube/Vimeo Videolink']) !!}
            @endif
        </div>
        <div data-video-preview></div>
    </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-3 col-xs-4"></div>
        <div class="col-lg-11 col-md-11 col-sm-9 col-xs-8">
            <button type="submit" class="profile-statusposts__form-btn btn btn--input btn--inline btn--dark-blue">
                Posten
            </button>
            @if (!isset($post) || $post->id === null)
                <button class="profile-statusposts__form-btn--reset btn btn--input btn--inline" data-user-post-reset>
                    <i class="fa fa-close" aria-hidden="true"></i> Zurücksetzen
                </button>
            @endif
        </div>
    </div>

    {{ Form::close() }}
</div>
