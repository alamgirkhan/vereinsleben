<div class="row">
    @foreach($posts as $post)
        @if($post->images()->count() > 0)
            @foreach($post->images as $key => $image)
                <div class="col-md-3 pfl-fts">
                    <a href="{{ asset($image->picture->url('singleView')) }}"
                       data-lightbox="gallery">
                        <img class="responsive-image" src="{{ asset($image->picture->url('detail')) }}"/>
                    </a>
                </div>
            @endforeach
        @endif
    @endforeach
    @if(!$image)
        <div class="col-md-12">Es sind noch keine Bilder in dieser Pinnwand gepostet!</div>
    @endif
</div>