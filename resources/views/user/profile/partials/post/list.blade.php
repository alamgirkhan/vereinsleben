<div class="profile-statusposts">
    <div class="profile-statusposts__container" data-user-post-container>
        <?php
        $newPost = new \Vereinsleben\Post;
        if (Auth::check()){
            $newPost->user_id = $user->id;
        }
        ?>
        @can('store', $newPost)
            @include('user.profile.partials.post.edit.form', ['user' => $user])
        @endcan

        @forelse($posts as $post)
            @include('user.profile.partials.post.single', ['user' => $user, 'post' => $post])
        @empty
            @include('user.profile.partials.post.empty')
        @endforelse
    </div>
</div>