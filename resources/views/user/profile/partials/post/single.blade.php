<?php
$classes = [];
$type = 'default';
if (isset($post) && $post->images()->count() > 0) {
    $classes[] = 'profile-statusposts__item--photo';
    $type = 'photo';
} elseif (isset($post) && $post->videos()->count() > 0) {
    $classes[] = 'profile-statusposts__item--video';
    $type = 'video';
}
$date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $post->published_at);
?>

<div class="club-post__item" data-user-post data-user-post-type="{{ $type }}"
     data-user-post-href="{{ route('user.post.template', [$user->username, $post->id]) }}">
    <div class="row">
        <div class="col-xs-11 edit-del-btn">
            <div class="club-content__options">
                @can('edit', $post)
                    <button data-user-post-edit="{{ route('user.post.edit', [$user->username, $post->id]) }}"
                            class="btn btn--inline btn--icon">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                    </button>
                @endcan
                @can('destroy', $post)
                    <button data-user-post-delete="{{ route('user.post.destroy', [$user->username, $post->id]) }}"
                            class="btn btn--inline btn--icon">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                    </button>
                @endcan
            </div>
        </div>
    </div>

    <div data-edit-content>
        <div class="row">
            <div class="col-lg-1 col-md-1 club-post__date noborder">
                <span class="date-no">{{ $date->day }}</span><br>
                <span class="month-no">{{ $date->formatLocalized('%b') }}</span><br>
                <span class="year-no">{{ $date->year }}</span>
            </div>

            <div class="col-lg-11 col-sm-11 event-post">
                @if($post->videos()->count() > 0)
                    <div class="event-post-thum">
                        @foreach ($post->videos as $video)
                            <div class="profile-statusposts__video">
                                @if ($video->type === \Vereinsleben\Video::TYPE_YOUTUBE)
                                    <iframe src="https://www.youtube.com/embed/{{ $video->identifier }}" frameborder="0"
                                            allowfullscreen></iframe>

                                @elseif($video->type === \Vereinsleben\Video::TYPE_VIMEO)
                                    <iframe src="https://player.vimeo.com/video/{{ $video->identifier }}?app_id=122963"
                                            frameborder="0"
                                            allowfullscreen></iframe>
                                @endif
                            </div>
                        @endforeach
                    </div>
                @endif

                <div class="@if($post->videos()->count() == 0) @if($post->images()->count() == 1) event-post-single-thum @else event-post-thum @endif @else post-videos-images @endif">
                    @if($post->images()->count() == 1)
                        @foreach($post->images as $image)
                            <a href="{{ asset($image->picture->url('singleView')) }}"
                               data-lightbox="post-{{ $post->id }}"><img
                                        src="{{ asset($image->picture->url('singleView')) }}"/></a>
                        @endforeach
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-8">
                                <div class="event-post-thum-arrow"><i class="fa fa-caret-up"
                                                                      aria-hidden="true"></i></div>
                                <div class="row post-head">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 postlogo">
                                        <img src="{{ asset($user->avatar->url('singleView')) }}"/>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-8 postdetail">
                                        <div class="postlefttxt">{{ $user->firstname }} {{ $user->lastname }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="row post-row-new">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                @if($post->images()->count() > 0)
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 lgthum">
                                            <a href="{{asset($post->images[0]->picture->url('singleView')) }}"
                                               data-lightbox="post-{{ $post->id }}"><img
                                                        src="{{asset($post->images[0]->picture->url('singleView')) }}"/></a>
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        @if($post->images()->count() > 0)
                                            <div class="event-post-thum-arrow"><i class="fa fa-caret-up"
                                                                                  aria-hidden="true"></i></div>
                                        @endif
                                        <div class="row post-head">
                                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 postlogo">
                                                <img src="{{ asset($user->avatar->url('singleView')) }}"/>
                                            </div>
                                            <div class="col-lg-9 col-md-9 col-sm-8 col-xs-8 postdetail">
                                                <div class="postlefttxt">{{ $user->firstname }} {{ $user->lastname }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if($post->images()->count() > 1)
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 club-post_right-thums">
                                <div class="row">
                                    @foreach($post->images as $key => $image)
                                        @if($key !=0 && $key < 4)
                                            <a href="{{asset($image->picture->url('singleView')) }}"
                                               data-lightbox="post-{{ $post->id }}">
                                                <div class="col-lg-12 club-post__sml-image"
                                                     style="background:url('{{asset($image->picture->url('singleView')) }}') no-repeat;background-size:cover;"></div>
                                            </a>
                                            @if($key == 3)
                                                @if($post->images()->count() > 4)
                                                    <div class="event-plus-icon"><a href="javascript:void(0);"><i
                                                                    class="fa fa-plus-circle"
                                                                    aria-hidden="true"></i></a>
                                                    </div>
                                                @endif
                                            @endif
                                        @endif

                                        @if($key > 3)
                                            <a href="{{asset($image->picture->url('singleView')) }}"
                                               data-lightbox="post-{{ $post->id }}"></a>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                            @endif
                        </div>
                    @endif
                </div>
                @if(isset($post->content))
                    <p>{!! nl2br($post->content) !!}</p>
                @endif
            </div>
        </div>
    </div>
</div>