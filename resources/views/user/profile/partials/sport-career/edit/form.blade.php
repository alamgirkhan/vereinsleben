<div class="sport-career-js-error"></div>
@if (!isset($sportCareer) || $sportCareer->id === null)
    {!! Form::open(
        [
            'route' => 'sport.career.store',
            'files' => true,
            'method' => 'POST',
        ]
    )!!}
@else
    {!! Form::model(
        $sportCareer,
            [
                'route' => ['sport.career.update', $sportCareer->id],
                'files' => true,
                'method' => 'POST',
            ]
    )!!}
    {{ Form::hidden('old_club_id', $sportCareer->club->id) }}
@endif
<div class="profile-athletic-runway__edit">

    <div class="row">
        <div class="col-xs-12">
            {!! Form::label('club_id', 'Verein', ['class' => 'input-label']) !!}
            <div class="input-group">
                <select id="club_id"
                        name="club_id"
                        data-club-autocomplete="{{ app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('api.club.autocomplete') }}">
                    @if(isset($sportCareer->club))
                        <option value="{{ $sportCareer->club->id }}">{{ $sportCareer->club->name }}</option>
                    @endif
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="{{ isset($sportCareer->club) ? '' : 'hidden'}} " data-club-autocomplete-selection>
                <div class="profile-athletic-runway__club profile-athletic-runway__club--edit">
                    <div class="profile-athletic-runway__club-image-wrapper">
                        <img data-club-autocomplete-selection-image
                             src="{{ isset($sportCareer->club->avatar) ? $sportCareer->club->avatar->url('singleView') : '' }}"
                             class="profile-athletic-runway__club-image"/>
                    </div>
                    <span data-club-autocomplete-selection-text>{{ $sportCareer->club->name or '' }}</span>
                </div>
            </div>
        </div>
    </div>
    <div id="movesuggestedclub"></div>
    <div class="row">
        <?php
        $begin_timeline = isset($sportCareer) && trim($sportCareer->timeline_begin) !== '' ? $sportCareer->timeline_begin : null;
        if ($begin_timeline != null){
            $beginarr = explode("-", $begin_timeline);
            $beginmonth = (int)$beginarr[1];
            $beginyear = $beginarr[0];
        } else{
            $beginmonth = null;
            $beginyear = null;
        }
        $end_timeline = isset($sportCareer) && trim($sportCareer->timeline_end) !== '' ? $sportCareer->timeline_end : null;
        if ($end_timeline != null){
            $endarr = explode("-", $end_timeline);
            $endmonth = (int)$endarr[1];
            $endyear = $endarr[0];
        } else{
            $endmonth = null;
            $endyear = null;
        }

        ?>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            {!! Form::label('timeline_begin', 'Von:', ['class' => 'input-label']) !!}
            <div class="input-group row">
                <div class="col-md-6">
                    {!! Form::select('timeline_begin_month',  array('' => 'Monat') + array_combine(range(01, 12), range(01, 12)), $beginmonth, ['required', 'class' => 'select','id' => 'timeline_begin_month']) !!}
                </div>
                <div class="col-md-6">
                    {!! Form::select('timeline_begin_year',  array('' => 'Jahr') + array_combine(range(1900, date('Y')), range(1900, date('Y'))), $beginyear, ['required', 'class' => 'select','id' => 'timeline_begin_year']) !!}
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            {!! Form::label('timeline_end', 'Bis:', ['class' => 'input-label']) !!}
            <div class="input-group input-group--no-bottom-spacing row">
                <div class="col-md-6">
                    {!! Form::select('timeline_end_month',  array('' => 'Monat') + array_combine(range(01, 12), range(01, 12)), $endmonth, ['class' => 'select','id' => 'timeline_end_month']) !!}
                </div>
                <div class="col-md-6">
                    {!! Form::select('timeline_end_year',  array('' => 'Jahr') + array_combine(range(1900, date('Y')), range(1900, date('Y'))), $endyear, ['class' => 'select','id' => 'timeline_end_year']) !!}
                </div>
            </div>

        </div>
        <span class="input-hint" style="display:inline-block">
                Aktuell noch aktiv im Verein? Dann einfach leer lassen.
        </span>
    </div>

    <div class="row">
        <div class="col-xs-12">
            {!! Form::label('function', 'Funktion', ['class' => 'input-label']) !!}
            <div class="input-group input-group--no-spacing">
                <?php $career = (isset($sportCareer) && $sportCareer->career_type !== null) ? $sportCareer->career_type : null; ?>
                {!! Form::select('career_type', ['Spieler' => 'Spieler','Trainer' => 'Trainer','Funktionär' => 'Funktionär',], $career, ['class' => 'select select2-container', 'placeholder' => 'Bitte wählen', 'id' => 'career_type', 'required' => 'required']) !!}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            {!! Form::label('sports', 'Sportarten', ['class' => 'input-label']) !!}
            <div class="input-group input-group--no-spacing">
                <select id="sports"
                        name="sports[]" multiple="multiple"
                        data-sport-autocomplete="{{ app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('api.sports') }}">
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div data-sport-autocomplete-tags class="input-group input-group--different-spacing">
                @if(isset($sportCareer) && $sportCareer->sports->count() > 0)
                    @foreach($sportCareer->sports as $sport)
                        <span data-sport-career-sport-remove="{{ $sport->id }}"
                              class="club-detail__overview-value club-detail__overview-value--highlighted input-checkbox">
                        <i class="fa fa-trash"></i>
                            {{ $sport->title }}
                    </span>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div data-upload-result>
                <label for="image"
                       class="button button--grey button--center button--icon button--full-width">
                    <span class="fa fa-upload"></span>Bild hochladen</label>
                <div class="input-group">
                    <input type="file" name="image"
                           id="@if (isset($post)){{ 'image-'. $post->id }}@else{{ 'image' }}@endif"
                           class="input-file" data-preview-image>
                </div>
                <div id="upload-result " class="thumbnail__list">
                    @if(isset($sportCareer) && $sportCareer->images->count() > 0)
                        @foreach($sportCareer->images as $image)
                            <input type="checkbox" name="delete-image[]" value="{{ $image->id }}"
                                   id="post_thumbnail-{{ $image->id }}" class="thumbnail__switch">
                            <label for="post_thumbnail-{{ $image->id }}"
                                   class="thumbnail__label thumbnail__container thumbnail__container--deleteable">
                                <img class="thumbnail__image" src="{{asset($image->picture->url('detail')) }}"/>
                            </label>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="">
                <button class="btn btn--inline btn--grey btn--large btn--upper" data-sport-career-cancel>
                    <i class="fa fa-close" aria-hidden="true"></i> Abbrechen
                </button>

                {!! Form::submit('Speichern', ['class' => 'btn btn--inline btn--red btn--large btn--upper', 'data-sport-career-store']) !!}
            </div>
        </div>
    </div>
    </div>

</div>
{{ Form::close() }}