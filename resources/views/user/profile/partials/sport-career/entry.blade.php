<div data-sport-career="{{ route('sport.career.template', $sportCareer->id) }}"
     class="row profile-athletic-runway @can('edit', $user) profile-athletic-runway--editable @endcan">
    {{-- Date --}}
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 runway-date">
        <div class="profile-athletic-runway__date">
        <div class="profile-athletic-runway__date-inner">
            <span class="profile-athletic-runway__granular-date">{{ $sportCareer->getFormattedDate()  }}</span>
            @if($sportCareer->getTimePeriod() !== null)
                <span class="profile-athletic-runway__duration">{{ $sportCareer->getTimePeriod() }}</span>
            @endif
        </div>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 runway-club"> @include('user.profile.partials.sport-career.layouts.default')</div>

    @can('edit', $user)
        <div class="col-lg-2 col-md-2 col-sm-3 col-xs-2 erfolge-edit-icons">
            <div class="profile-athletic-runway__button-group pull-right">
        <button data-sport-career-edit="{{ route('sport.career.edit', $sportCareer->id) }}"
                class="btn btn--inline btn--icon">
            <i class="fa fa-pencil" aria-hidden="true"></i>
        </button>
        <button data-sport-career-delete="{{ route('sport.career.destroy', $sportCareer->id) }}"
                class="btn btn--inline btn--icon">
            <i class="fa fa-trash" aria-hidden="true"></i>
        </button>
            </div>
    </div>
    @endcan
</div>