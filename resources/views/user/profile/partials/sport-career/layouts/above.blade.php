{{-- Layout: image above --}}


{{-- Image --}}
<div class="col-xs-12">
    @if($sportCareer->images->count() > 0)
        @foreach($sportCareer->images as $image)
            <img src="{{ asset($image->picture->url('detail')) }}" alt=""
                 class="profile-athletic-runway__image">
        @endforeach
    @endif
</div>

<div class="col-xs-12">

    @if($sportCareer->club !== null)
        <a href="{{ route('club.detail', $sportCareer->club->slug) }}" class="profile-athletic-runway__link">
            <div class="profile-athletic-runway__club">
                <div class="profile-athletic-runway__club-image-wrapper">
                    {{-- Club --}}
                    <img class="profile-athletic-runway__club-image"
                         src="{{ $sportCareer->club->avatar->url('singleView') }}"
                         alt="">
                </div>
                <div>
                    <span class="profile-athletic-runway__club-name">{{ $sportCareer->club->name }}</span>
                    {{-- Sports --}}
                    @if($sportCareer->sports->count() > 0)
                        @foreach($sportCareer->sports as $sport)
                            <span class="club-detail__overview-value club-detail__overview-value--highlighted">{{ $sport->title }}</span>
                        @endforeach
                    @endif
                </div>
            </div>
        </a>
    @endif

    {{-- Headline --}}
    @if($sportCareer->title !== null)
        <h2 class="hl hl-alternate-ft hl--inverted">{{ $sportCareer->title }}</h2>
    @endif

    {{-- Content --}}
    @if(isset($sportCareer->content) && $sportCareer->content !== '')
        <p class="profile-athletic-runway__text">
            {{ $sportCareer->content }}
        </p>
    @endif
</div>