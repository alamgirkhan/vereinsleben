{{-- Layout: default --}}

<div class="profile-athletic-runway__club-container">
    @if(isset($sportCareer) && $sportCareer->club !== null)
        <a href="{{ route('club.detail', $sportCareer->club->slug) }}" class="profile-athletic-runway__link">
            @endif
            <div class="profile-athletic-runway__club">
                <div class="row">
                    @if($sportCareer->club !== null)
                        <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3 club-thum-lead">
                            <div class="profile-athletic-runway__club-image-wrapper">
                                {{-- Club --}}
                                <img class="profile-athletic-runway__club-image"
                                     src="{{ $sportCareer->club->avatar->url('singleView') }}"
                                     alt="">
                            </div>
                        </div>
                    @endif
                    <div class="col-lg-10 col-md-10 col-sm-9 col-xs-9 club-name-lead">
                        @if($sportCareer->club !== null)
                            <span class="profile-athletic-runway__club-name">{{ $sportCareer->club->name }}</span>
                        @endif
                        {{-- Sports --}}
                        @if(isset($sportCareer) && $sportCareer->sports->count() > 0)
                            @foreach($sportCareer->sports as $sport)
                                <span class="club-detail__overview-value club-detail__overview-value--highlighted">{{ $sport->title }}</span>
                            @endforeach
                        @endif
                </div>
                </div>
            </div>

            @if($sportCareer->club !== null)
        </a>
    @endif
</div>

@if(isset($sportCareer) && $sportCareer->images->count() > 0 OR isset($sportCareer->content) AND $sportCareer->content !== '')
    <div class="profile-athletic-runway__arrow-container">
        <i class="fa fa-chevron-down profile-athletic-runway__arrow" aria-hidden="true"></i>
    </div>
@endif

<div class="profile-athletic-runway__content-container">
    <div class="profile-athletic-runway__content-container-inner">
        <p class="profile-athletic-runway__text">
            @if(isset($sportCareer) && $sportCareer->images->count() > 0)
                @foreach($sportCareer->images as $image)
                    <img src="{{ asset($image->picture->url('detail')) }}" alt=""
                         class="profile-athletic-runway__image">
                @endforeach
            @endif
            @if(isset($sportCareer->content) && $sportCareer->content !== '')
                {{ $sportCareer->content }}
            @endif
        </p>
    </div>
</div>