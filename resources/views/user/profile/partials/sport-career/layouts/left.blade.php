{{-- Layout: image left --}}


{{-- Image --}}
<div class="col-xs-12 col-sm-4">
    @if($sportCareer->images->count() > 0)
        @foreach($sportCareer->images as $image)
            <img src="{{ asset($image->picture->url('detail')) }}" alt=""
                 class="profile-athletic-runway__image">
        @endforeach
    @endif

</div>

<div class="col-xs-12 col-sm-8">

    @if($sportCareer->club !== null && $sportCareer->club->isPublished())
        <a href="{{ route('club.detail', $sportCareer->club->slug) }}" class="profile-athletic-runway__link">
            @endif
            <div class="profile-athletic-runway__club">
                @if($sportCareer->club !== null && $sportCareer->club->isPublished())
                    <div class="profile-athletic-runway__club-image-wrapper">
                        {{-- Club --}}
                        <img class="profile-athletic-runway__club-image"
                             src="{{ $sportCareer->club->avatar->url('singleView') }}"
                             alt="">
                    </div>
                @endif
                <div>
                    @if($sportCareer->club !== null && $sportCareer->club->isPublished())
                        <span class="profile-athletic-runway__club-name">{{ $sportCareer->club->name }}</span>
                    @endif
                    {{-- Sports --}}
                    @if($sportCareer->sports->count() > 0)
                        @foreach($sportCareer->sports as $sport)
                            <span class="club-detail__overview-value club-detail__overview-value--highlighted">{{ $sport->title }}</span>
                        @endforeach
                    @endif
                </div>
            </div>

            @if($sportCareer->club !== null && $sportCareer->club->isPublished())
        </a>
    @endif

    {{-- Headline --}}
    @if($sportCareer->title !== null)
        <h2 class="hl hl-alternate-ft hl--inverted">{{ $sportCareer->title }}</h2>
    @endif

    {{-- Content --}}
    @if(isset($sportCareer->content) && $sportCareer->content !== '')
        <p class="profile-athletic-runway__text">
            {{ $sportCareer->content }}
        </p>
    @endif
</div>