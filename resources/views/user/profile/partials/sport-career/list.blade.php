<section class="profile-section profile-section--athletic-runway" id="sport-care-list">
    {{-- content --}}
    @if($sportCareers->count() > 0 )
        @foreach($sportCareers as $sportCareer)
            @include('user.profile.partials.sport-career.entry', [
                'sportCareer' => $sportCareer
            ])
        @endforeach
    @else
        @cannot('edit', $user)
            <p><em>Dieser Benutzer hat seine Laufbahn noch nicht bearbeitet.</em></p>
        @endcannot
    @endif

    @can('edit', $user)

    @endcan
</section>