@if($userSports->count() > 0)
    @foreach($userSports as $userSport)
        @if($userSport->sport)
        <h4 class="hl hl--small hl--inverted">{{ ($userSport->sport) ? $userSport->sport->title : null }}</h4>
        <div class="profile-rangeslider">
            <div class="profile-rangeslider__fill" style="width: {{ $userSport->interest }}%"></div>
        </div>
        <div class="profile-rangeslider__scala">
            <div class="profile-rangeslider__scala-middle"></div>
            <div class="profile-rangeslider__scala-desc inter">interessiert</div>
            <div class="profile-rangeslider__scala-desc bege">begeistert</div>
            <div class="clear"></div>
        </div>
        @endif
    @endforeach
@else
    <p><em>Dieser Benutzer hat noch keine Interessen angegeben.</em></p>
@endif