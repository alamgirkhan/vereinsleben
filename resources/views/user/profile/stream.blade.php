@extends('layouts.master')
@section('content')
    {{--@if($me->id == 2074 || $me->id == 439 || $me->id == 27000 || $me->id == 28068 || $me->id == 1951 || $me->id == 2674 || $me->id == 26590 || $me->id == 22871)--}}
    <div class="container">
        <input type="hidden" name="search_id" id="search_id" value="{{ Auth::user()->id }}"/>
        <section id="streaming" class="pageable-menu__page pageable-menu__page--active">
            {{--<h2>{{ Auth::user()->fullName() }}</h2>--}}
            <h1 class="club-content__box-headline">Dein persönlicher Stream</h1>
            @include('user.index.stream', ['user' => Auth::user()->username])
        </section>
    </div>
    {{--@endif--}}
@endsection
@section('post-js')
    <script src="{{ asset(('js/user-stream.js')) }}"></script>
@endsection