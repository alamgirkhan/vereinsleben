@extends('layouts.master')

@section('content')
    @php
        $friends =  $suggestedFriends
    @endphp
    <section id="user-profile" data-token="{{ csrf_token() }}">
        @include('user.profile.partials.header')
        @include('user.profile.partials.avatar')
        <a href="{{route('user.profile.friends', $user->username)}}">friends</a>
        <div class="container profile__container">
            <div class="profile__overlay-content">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        @include('user.profile.partials.friends.friendslist')
                    </div>

                </div>
                <div class="profile__overlay"></div>
            </div>

        </div>

    </section>
@endsection

