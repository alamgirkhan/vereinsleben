@extends('layouts.master')
@section('content')
    <section data-token="{{ csrf_token() }}">
        <div class="club-post__wrapper" id="user-stream">
            <stream></stream>
        </div>
    </section>
@endsection
@section('post-js')
    <script src="{{ asset(('js/user-stream.js')) }}"></script>
@endsection