@extends('layouts.master')

@section('content')


    <header class="vote-detail-header header-blank">
        <div class="container">
            <div class="vote-detail-header__caption">
                <h1 class="vote-detail-header__headline">
                    Über „Verein des Monats“
                </h1>
                <span class="vote-detail-header__subline">
                    Informationen zum Wettbewerb
                </span>
            </div>
        </div>
    </header>

    <div class="section section--vote-white section--vote-detail-bordered no-before">
        <div class="container">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <section class="news-detail__main">

                    <div class="news-detail__content">
                        <h2 class="news-detail__headline">
                            Über den Wettbewerb
                        </h2>
                        <p>
                            Was Sportvereine jeden Tag leisten, kann man gar nicht genug würdigen, selbst wenn es sich dabei „nur“ um das „normale“ Tagesgeschäft, die „normale“ Vereinsarbeit handelt.
                            Sportvereine sind sinnstiftend und führen unzählige Kinder und Jugendliche, Erwachsene und Senioren an den Sport heran. zum ersten Mal,  oder auch aufs Neue.
                            Sie übernehmen eine wichtige Funktion in unserer Gesellschaft und bieten vielen Menschen eine zweite Heimat im Sport.
                            Ämter im Verein sind häufig Ehrenämter, neben dem eigenen Beruf, dem Alltag, dem Leben. Alleine das ist schon guter Grund,  Danke zu sagen.

                        </p>

                        <h2 class="news-detail__headline">
                            Dienst nach Vorschrift? – Fehlanzeige!
                        </h2>
                        <p>
                            Aber damit nicht genug: Viele Vereine leisten Herausragendes für uns, für die Gesellschaft,
                            für ihre Mitmenschen. Dienst nach Vorschrift? Fehlanzeige! Viele Vereine übernehmen in
                            Eigeninitiative zusätzlich soziale und gesellschaftspolitische Aufgaben, z.B. in den
                            Bereichen Inklusion, Gleichstellung, Integration, Gesundheit, Prävention oder auch Bildung
                            und bilden somit eine Art Sicherheitsnetz für das Funktionieren unserer Gesellschaft. Viele
                            Vereine erledigen diese Aufgaben sehr still und ohne viel Aufheben. Andere sind zwar
                            bekannt, aber oft nur innerhalb der eigenen Kommune, der eigenen Region.
                        </p>
                        <h2 class="news-detail__headline">
                            Vereine wie Leuchttürme
                        </h2>
                        <p>
                            Diese Vereine verbindet, dass sie Leuchttürme sind. Leuchttürme für die Bedeutung der
                            Sportvereine.
                            Mit dem Wettbewerb „Verein des Monats“ wollen wir diesen Leuchtturm-Vereinen in
                            Rheinland-Pfalz und dem Saarland
                            ein Forum bieten, sich und ihre Arbeit zu präsentieren. Aber auch monatlich einen ganz besonderen Verein zum „Verein des Monats“ küren, der 10.000 Euro gewinnt.
                            Die Besonderheit bei diesem Wettbewerb besteht darin, dass die Wahl zum „Verein des Monats“
                            per Abstimmung entschieden wird.
                            Also liegt es auch ein Stück weit in der Hand der Vereine. Je mehr Mitglieder, Fans und
                            Freunde ein Verein
                            zur Abstimmung aktivieren kann, desto größer ist seine Chance.


                        </p>
                        <h2 class="news-detail__headline">
                            Jetzt bewerben und mitmachen
                        </h2>

                        <p>
                            Ihr seid ein Sportverein aus Rheinland-Pfalz oder dem Saarland? Dann bewerbt euch jetzt bei uns. Aktiviert eure Mitglieder, Fans und Freunde, für euch abzustimmen und gewinnt jeden Monat * 10.000 Euro vom Gewinnsparverein der Sparda-Bank Südwest. Aber damit nicht genug: Die Vereine, die bei der Abstimmung die Plätze eins, zwei und drei belegen, erhalten außerdem jeweils einen Gutschein über 30 Tickets für ein Heimspiel des FSV Mainz 05. Weiterhin vergeben wir im Anschluss an den Aktionszeitraum * unter allen Monatssiegern weitere Geldpreise im Gesamtwert von 5.000 Euro.
                        </p>


                    </div>

                    @include ('vote.partials.btn-group')

                    <div class="vote-footnote">
                        <p>
                            * Jeden Monat im Aktionszeitraum März bis November 2019
                        </p>

                    </div>





                </section>
            </div>
        </div>
    </div>

@endsection
