@extends('layouts.master')

@section('content')


    <header class="vote-detail-header">
        <div class="container">
            <div class="vote-detail-header__caption">
                <h1 class="vote-detail-header__headline">Teilnahmebedingungen
                </h1>
                <span class="vote-detail-header__subline">
                    am Wettbewerb „Verein des Monats“
                </span>
            </div>
        </div>
    </header>

    <div class="section section--vote-white section--vote-detail-bordered">
        <div class="container">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <section class="news-detail__main">

                    <div class="news-detail__content">
                        <h2 class="news-detail__headline">1. Allgemeine Bedingungen</h2>
                        <p>
                            1.1. Das Onlineportal vereinsleben.de, das von der Rheinland-Pfälzische Rundfunk GmbH & Co. KG, Turmstraße 10, 67059 Ludwigshafen betrieben wird, veranstaltet, unterstützt vom Landessportbund Rheinland-Pfalz, im Aktionszeitraum vom 1. März bis 30. November 2019 auf www.vereinsleben.de den Wettbewerb „Verein des Monats“. vereinsleben.de ist berechtigt, sich zur Abwicklung des Wettbewerbs Dritter zu bedienen.
                        </p>
                        <p>
                            1.2. Beim Wettbewerb „Verein des Monats“ sollen Sportvereine in den Fokus gerückt und belohnt werden, die sich durch besonderes Engagement, z.B. in den Bereichen Kinder- und Jugendarbeit, Integration, Inklusion, Gesundheit, Prävention, Bildung u.v.m. auszeichnen. Ob dieses Engagement im Rahmen spezieller Projekte stattfindet oder durch die normale Vereinsarbeit abgedeckt wird, ist dabei unerheblich. Bereits abgeschlossene Projekte dürfen dabei nicht länger als 24 Monate vor der Bewerbung beendet worden sein.
                        </p>
                        <p>
                            1.3. Teilnahmeberechtigt sind alle Sportvereine aus Rheinland-Pfalz und dem Saarland, deren Gemeinnützigkeit vom jeweiligen, zuständigen Finanzamt anerkannt und bestätigt ist. Vereine, bei denen Mitarbeiter von vereinsleben.de/RPR1. im Vorstand sind, sind nicht teilnahmeberechtigt.
                        </p>
                        <p>
                            1.4. Für die Teilnahme am Wettbewerb „Verein des Monats“ können sich teilnahmeberechtigte Sportvereine mittels eines auf www.vereinsleben.de hinterlegten Online-Bewerbungsformulars bewerben.
                        </p>
                        <p>
                            1.5. Mit dem Absenden des Bewerbungsformulars akzeptieren der Verein und die den Verein
                            anmeldende Person diese Teilnahmebedingungen.

                        </p>
                        <p>
                            1.6. Teilnehmen können außerdem alle auf vereinsleben.de registrierten Nutzer im Sinne einer
                            Beteiligung am Abstimmungsverfahren, die zum Zeitpunkt der Stimmabgabe über 18 Jahre alt
                            sind. Für die Registrierung auf vereinsleben.de gelten die Allgemeinen Geschäftsbedingungen,
                            abrufbar unter <a href="https://www.vereinsleben.de/content/agbs" target="_blank">www.vereinsleben.de/agbs</a>.

                        </p>
                        <p>
                            1.7. Die Teilnehmer am Abstimmungsverfahren akzeptieren die Teilnahmebedingungen durch eine mindestens einmalige Stimmvergabe.
                        </p>

                        <h2 class="news-detail__headline">2. Ablauf des Wettbewerbs</h2>
                        <p>
                            2.1. Aus allen im gesamten Aktionszeitraum eingehenden Bewerbungen werden pro Kalendermonat
                            im Aktionszeitraum (Siehe Punkt 1.1.) fünf Vereine als Kandidaten ausgewählt und auf www.vereinsleben.de vorgestellt. Unter diesen
                            Vereinen wird per Abstimmungsverfahren jeden Monat ein „Verein des Monats“ ermittelt.

                        </p>
                        <p>
                            2.2. In jedem Kalendermonat im Aktionszeitraum (Siehe Punkt 1.1.) kann vom jeweils ersten bis zum jeweils letzten Tag für die vorgestellten Vereine abgestimmt werden.
                        </p>
                        <p>
                            2.3. Stimmberechtigt sind alle auf www.vereinsleben.de registrierten Nutzer, die zum
                            Zeitpunkt der Stimmabgabe über 18 Jahre alt sind.
                        </p>
                        <p>
                            2.4. Jede stimmberechtigte Person kann während der Abstimmungsphase pro Tag eine Stimme vergeben. Die Stimmen müssen nicht auf denselben Verein vereint werden, sondern können auch für verschiedene Vereine vergeben werden.

                        </p>
                        <p>
                            2.5. Derjenige Verein, der am Ende des jeweiligen Kalendermonats im Aktionszeitraum die meisten Stimmen auf sich vereinen kann, wird „Verein des Monats“.
                        </p>

                        <h2 class="news-detail__headline">
                            3. Gewinn und Gewinnbenachrichtigung
                        </h2>
                        <p>
                            3.1. Jeder Verein-des-Monats-Gewinner erhält 10.000 Euro (per Überweisung, keine Barauszahlung möglich) als Spende gegen eine Spendenquittung vom Gewinnsparverein der Sparda-Bank Südwest eG, Robert-Koch-Straße 45, 55129 Mainz. Die Auszahlung des Gewinns erfolgt zeitnah durch den Gewinnsparverein der Sparda-Bank Südwest. Die Gewinnauszahlung für die Sieger der Monate September 2019, Oktober 2019 und November 2019 erfolgt jedoch erst im Januar 2020.
                        </p>
                        <p>
                            3.2. Die Vereine, die bei der Abstimmung die Plätze eins, zwei und drei belegen, erhalten jeweils einen Gutschein für 30 Tickets für ein Heimspiel des FSV Mainz 05 der Saison 2018/2019, bzw. der Saison 2019/2020 vom 1. FSV Mainz 05 e. V., Isaac-Fulda-Allee 5, 55124 Mainz.
                        </p>
                        <p>
                            3.3 Die Gewinnübergabe für die Verein-des-Monats-Gewinner findet in der ersten Hälfte des darauffolgenden Monats in der Sendezentrale von RPR1. in der Turmstraße 10 in 67059 Ludwigshafen statt. Die Anwesenheit mindestens eines Vereinsvertreters – idealerweise aus dem Vereinsvorstand – ist hierfür erforderlich. Gerne können weitere Vereinsvertreter (maximal fünf Personen) ebenfalls teilnehmen, alle Teilnehmer müssen jedoch aus organisatorischen Gründen vorab angemeldet werden. Der jeweilige Termin für die Gewinnübergabe wird den Gewinner-Vereinen rechtzeitig mitgeteilt.
                        </p>
                        <p>
                            3.4. Die anderen vier teilnehmenden Vereine erhalten die detaillierten Informationen zu ihren jeweiligen Gewinnen (Siehe Punkt 3.2.) im Nachgang per Telefon oder E-Mail.
                        </p>
                        <p>
                            3.5. Im Anschluss an den Aktionszeitraum (Siehe Punkt 1.1.) werden im Rahmen einen Sonderpreisverleihung weitere Geldpreise im Gesamtwert von 5.000 Euro an alle Monatssieger (Siehe Punkt 2.5.) vergeben. Die Reihenfolge ergibt aus einem Vergleich der Abstimmergebnisse der jeweiligen Monatssieger. Der Verein mit den meisten Stimmen (Platz 1) erhält 2.000 Euro, der Verein mit den zweitmeisten Stimmen (Platz 2) erhält 1.000 Euro, der Verein mit den drittmeisten Stimmen (Platz 3) erhält 500 Euro. Die Vereine auf den Plätzen 4 bis 9 erhalten jeweils 250 Euro.
                        </p>
                        <h2 class="news-detail__headline">
                            4. Einverständniserklärung
                        </h2>
                        <p>
                            4.1. Übermittlung von Fotos/Bildern: Die Gewinner ermächtigt den Veranstalter eingesandte Fotos im Rahmen oder in Folge des Gewinnspiels bzw. zur Berichterstattung über dieses Gewinnspiel zu verwenden (auf www.vereinsleben.de, auf www.rpr1.de und/oder auf den Facebookseite von vereinsleben.de und RPR1.). Insofern überträgt der Teilnehmer die mit dem Foto verbundenen Nutzungsrechte zeitlich und räumlich uneingeschränkt an die Rheinland-Pfälzische Rundfunk GmbH & Co. KG und sichert zu, sämtliche notwendige Rechte, auch vom Urheber (dem Fotografen) des Bildes und von evtl. abgebildeten Personen/Gegenständen innezuhaben. Der Veranstalter nimmt die Übertragung hiermit an. Ein Honoraranspruch wird hierdurch weder von dem Teilnehmer noch von den auf den ggfs. auf dem Foto zu erkennenden Personen begründet.
                        </p>
                        <p>
                            4.2. Aufzeichnung und Ausstrahlung Gespräche: Der Teilnehmer erklärt sich durch seine
                            Teilnahme unentgeltlich und unwiderruflich damit einverstanden, dass seine Äußerungen
                            aufgezeichnet werden. Der Veranstalter erhält mit der Teilnahme das Recht, die Aufzeichnung
                            zu kürzen und zum Zwecke der inhaltlichen Gestaltung ganz oder in Teilen im Programm von
                            RPR1. auszustrahlen und zu wiederholen. Telefongespräche mit Teilnehmern und Gewinnern
                            werden während und ggfs. nach dem Gewinnspielzeitraum im Programm von RPR1. gesendet.
                        </p>
                        <p>
                            4.3. Veröffentlichung von Gewinnern: Die Gewinner können – bei Personen die an der
                            Abstimmung teilgenommen haben unter Angabe ihres Vornamens und dem ersten Buchstaben des
                            Nachnamens sowie des Wohnorts – online auf der Aktionsseite auf www.vereinsleben.de,
                            www.rpr1.de und/oder auf den Facebookseite von vereinsleben.de und RPR1. sowie im Programm
                            von RPR1. bekannt gegeben werden.
                            Bei allen Gewinnen behält sich die Rheinland-Pfälzische Rundfunk GmbH & Co. KG vor,
                            aufmerksamkeitsstarke und medienwirksame Gewinnübergaben durchzuführen und über Gewinner und
                            Gewinn auf www.vereinsleben.de, im Programm von RPR1., auf www.rpr1.de und/oder auf den
                            Facebookseite von vereinsleben.de und RPR1. in Wort und/oder Bild zu berichten.
                            Der Gewinner erklärt mit der Teilnahme seine grundsätzliche Bereitschaft, auf
                            Interviewanfragen auch anderer Medien im Zusammenhang mit seinem Gewinn einzugehen.
                        </p>

                        <h2 class="news-detail__headline">
                            5. Besondere Datenschutzhinweise
                        </h2>
                        <p>
                            5.1. Durch die Teilnahme am Wettbewerb „Verein des Monats“ erklären sich die teilnehmenden
                            Vereine, die den Verein anmeldenden Personen und die sich am Abstimmungsverfahrung
                            beteiligenden Personen ausdrücklich damit einverstanden, dass die Rheinland-Pfälzische
                            Rundfunk GmbH & Co. KG die für Durchführung und Abwicklung des Wettbewerbs erforderlichen
                            personenbezogenen Daten verarbeitet (also erhebt, speichert und nutzt).
                        </p>
                        <p>
                            5.2. Die Rheinland-Pfälzische Rundfunk GmbH & Co. KG nimmt den Umgang mit Ihren
                            personenbezogenen Daten sehr ernst. Alle weiteren Informationen zur Datenverarbeitung im
                            Rahmen der Teilnahme an diesem Gewinnspiel können Sie unseren Datenschutzhinweisen
                            entnehmen, die Sie jederzeit unter <a href="https://www.vereinsleben.de/content/datenschutz"
                                                                  target="_blank">https://www.vereinsleben.de/content/datenschutz</a>
                            einsehen können.
                        </p>
                        <p>
                            5.3. Soweit die persönlichen Daten aller teilnehmenden Beteiligten zum Zwecke des Wettbewerb
                            und dessen Betreuung verwendet werden, werden die Daten nur an die Kooperationspartner des
                            Wettbewerbs weitergegeben, soweit dies zur Abwicklung im Rahmen des Wettkampfs und zur
                            Gewinnausschüttung, bzw. dem Versand des Gewinns erforderlich ist.
                        </p>
                        <p>
                            5.4. Durch die Bewerbung willigen die teilnehmenden Vereine ein, dass die den Verein
                            betreffenden Daten genutzt werden, um ein Vereinsprofil auf www.vereinsleben.de zu
                            erstellen, sofern ein solches nicht bereits besteht. Ein Vereinsprofil ist aus technischen
                            Gründen für die Teilnahme am Wettbewerb unbedingt erforderlich. Für das Vereinsprofil gelten
                            die Allgemeinen Geschäftsbedingungen, abrufbar unter <a
                                    href="https://www.vereinsleben.de/content/agbs" target="_blank">www.vereinsleben.de/content/agbs</a>.
                        </p>
                        <p>
                            5.5. Es steht allen teilnehmenden Beteiligten jederzeit frei, die Einwilligung zur Erhebung,
                            Speicherung und Nutzung seiner Daten den Wettbewerb betreffend zu widerrufen und somit von
                            der Teilnahme zurückzutreten. Der Widerruf bedarf der Textform und ist zu richten an:
                        </p>
                        <p>
                            vereinsleben.de <br>
                            c/o Rheinland-Pfälzische Rundfunk GmbH & Co. KG<br>
                            Turmstraße 10<br>
                            67059 Ludwigshafen<br>
                            <a href="mailto:info@vereinsleben.de">info@vereinsleben.de</a>.
                        </p>

                        <h2 class="news-detail__headline">
                            6. Weitere Bestimmungen
                        </h2>
                        <p>
                            6.1. Die Rheinland-Pfälzische Rundfunk GmbH & Co. KG ist berechtigt, Personen und/oder
                            Vereine, bei denen der Verdacht besteht, dass sie sich bei der Teilnahme am Wettbewerb
                            unerlaubter Hilfsmittel bedienen oder anderweitig versuchen, sich oder Dritten durch
                            Manipulation oder unter Vorspiegelung falscher Tatsachen Vorteile zu verschaffen oder in
                            sonstiger Weise den Wettbewerb negativ oder entgegen den Teilnahmebedingungen zu
                            beeinflussen, von der Teilnahme auszuschließen bzw. Gewinner nachträglich zu
                            disqualifizieren.
                            Ein Ausschluss von der Teilnahme bzw. eine nachträgliche Disqualifikation behält sich die
                            Rheinland-Pfälzische Rundfunk GmbH & Co. KG auch insbesondere in folgenden Fällen vor:
                            Beeinflussung des Teilnahmevorgangs, Störung des Ablaufs, Belästigung oder Bedrohung von
                            Mitarbeitern oder anderen Teilnehmern, (Versuch der) Verbreitung von rechts- oder
                            sittenwidrigem Gedankengut.
                            Liegen die Voraussetzungen für einen Teilnahmeausschluss vor, wird der Veranstalter in
                            diesem Fall keinen Gewinn auszahlen bzw. ausschütten; bei bereits gewährten Gewinnen ist der
                            Veranstalter berechtigt, diese zurückzufordern.
                        <p>
                            6.2. Weiterhin behält sich die Rheinland-Pfälzische Rundfunk GmbH & Co. KG vor, den
                            Wettkampf auch ohne Angabe von Gründen jederzeit zu verlängern oder vorzeitig zu beenden,
                            insbesondere bei Vorliegen wichtiger Gründe, wie z. B. dem Verdacht auf Manipulation, bei
                            höherer Gewalt und/oder wenn das Gewinnspiel aus organisatorischen, technischen oder
                            rechtlichen Gründen nicht durchgeführt bzw. fortgesetzt werden kann. Wird das Gewinnspiel
                            aus diesen Gründen abgebrochen oder beendet, haben die Teilnehmer keine Ansprüche gegen den
                            Veranstalter.
                        </p>
                        <p>
                            6.3. Die Rheinland-Pfälzische Rundfunk GmbH & Co. KG übernimmt keine Gewähr für entgangene
                            Gewinnchancen durch technisch bedingte Verbindungsstörungen o.Ä.
                            Der Veranstalter haftet jedoch nicht für Fehlaussagen oder technisch bedingte Fehler im
                            Zusammenhang mit der Gewinnentscheidung.
                        </p>
                        <p>
                            6.4. Gewinnansprüche sind nicht auf andere Vereine übertragbar. Der teilweise Warenwert des
                            Gewinns kann nicht in Bargeld ausgezahlt werden.
                        </p>
                        <p>
                            6.5. Die Person, welche eine Bewerbung für einen Verein einreicht, kann per Checkbox und
                            anschließendem Double-Opt-In (über die angegebene E-Mail-Adresse) einen kostenlosten
                            Newsletter von vereinsleben.de abonnieren. Dieser Vorgang ist jederzeit widerrufbar
                            (<a href="mailto:info@vereinsleben.de">info@vereinsleben.de</a>). Näheres können Sie unseren allgemeinen Datenschutzhinweisen <br />unter <a href="https://www.vereinsleben.de/content/datenschutz" target="_blank">https://www.vereinsleben.de/content/datenschutz</a>
                            entnehmen.
                        </p>
                        <p>
                            6.6. Durch die Teilnahme an dem Wettkampf werden die Teilnahmebedingungen anerkannt. Die
                            Rheinland-Pfälzische Rundfunk GmbH & Co. KG behält sich vor, jederzeit die
                            Teilnahmebedingungen zu ändern.
                        </p>
                        <p>
                            6.7. Sofern eine oder mehrere Regelungen ganz oder teilweise nichtig, unwirksam oder
                            undurchführbar sind oder werden, bleiben die übrigen Regelungen in ihrer Wirksamkeit
                            unberührt. An die Stelle der unwirksamen Regelung tritt eine rechtlich haltbare Klausel.
                            Gleiches gilt bei einer Regelungslücke hinsichtlich ihrer Ergänzung.
                        </p>
                        <p>
                            6.8. Der Rechtsweg ist für die Durchführung des Wettbewerbs, die Gewinnentscheidung und auch
                            die Gewinnauszahlung bzw. -ausschüttung ausgeschlossen.
                        </p>
                        <p>Ludwigshafen, im März 2019</p>
                    </div>
                    @include ('vote.partials.btn-group')
                </section>
            </div>
        </div>
    </div>

@endsection
