@extends('layouts.master')

@section('content')
    <header class="vote-sponsor-header--lsb header-blank">
        <div class="container">
            <div class="row pos-rel">
                <img class="vote-sponsor-logo-detail" src="/default_images/vote/sponsores/logo-detail/bigfm-logo-detail.png"
                     alt="Logo Landessportbund Rheinland-Pfalz">
            </div>
            <div class="vote-detail-header__caption">
                <h1 class="vote-detail-header__headline">
                    BIGFM
                </h1>
                <span class="vote-detail-header__subline">
                    DEUTSCHLAND BIGGSTE BEATS
                </span>
            </div>
        </div>
    </header>

    <div class="section section--vote-white section--vote-detail-bordered no-before">
        <div class="container">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <section class="news-detail__main">
                    <div class="news-detail__content">
                        <p>Das bigFM Sendegebiet erstreckt sich über fünf Bundesländer im Südwesten Deutschlands. Über 9,4 Millionen Menschen in Deutschland kennen bigFM und über 2,4 Millionen Hörer pro Tag schalten den Sender täglich ein. Damit ist bigFM die erfolgreichste Radiostation für junge Erwachsene.</p>
                        <h2>Breit aufgestellt</h2>
                        <p>Ob Event, zielgerichtetes Hörermarketing, Cross- und Multichannel-Kampagnen, digitale Beiträge oder innovatives Radio und Streaming – bigFM erzeugen Emotionen, immer und überall.</p>

                        <h2>Weitere Informationen</h2>
                        <p>Mehr über den BigFM und seine Aufgaben findet ihr im Internet unter <a href="http://www.big-fm.de/" target="_blank">www.big-fm.de</a></p>
                    </div>
                    @include ('vote.partials.btn-group')
                </section>
            </div>
        </div>
    </div>

@endsection
