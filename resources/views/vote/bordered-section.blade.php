<section class="section section--white section--vote-bordered no-before no-after">

    @if($previous && ($previous->getWinner() !== false && !$previous->isCurrent()) && !($vote instanceof \Vereinsleben\Vote))

        @if(!($vote instanceof \Vereinsleben\Vote))
            @include('vote.intro', ['vote' => $previous])
        @endif
    @elseif($previous && ($previous->getWinner() === false && !$previous->isCurrent()) && !($vote instanceof \Vereinsleben\Vote))
        {{-- break between voting phases --}}
        @include('vote.break-between')
    @endif

    {{--  new year - teaser for new votings --}}
    {{-- vote nominees --}}
    @if(!$previous && !($vote instanceof \Vereinsleben\Vote))
        @include('vote.new-year-teaser')
    @endif

    {{-- introduction --}}
    @if($vote instanceof \Vereinsleben\Vote)
        @include('vote.intro')
    @endif

    {{-- vote nominees --}}
    @if($vote && $vote->isCurrent() )
        @include('vote.nominee-list')
    @endif

    {{-- teaser content --}}
    @if($vote )
        @include('vote.partials.teaser-content')
    @endif
</section>