<div class="section section--vote-white">
    <div class="container">
        <h2 class="hl hl--large hl--underlined hl--inverted hl--centered hl--upper">
            Kurze Pause
        </h2>

        <div class="text-block text-block--compressed text-block--thin text-block--smaller-spacing">
            <p>
                Die Abstimmung ist beendet. Vielen Dank für eure Teilnahme. Die Ergebnisse werden gerade ausgewertet und in Kürze wird der Gewinner hier bekanntgegeben. Vielen Dank für euere Geduld!
            </p>
        </div>

        <p class="text-block text-block--compressed text-block--align-center">
            <a href="/verein-des-monats/teilnehmen" class="btn btn--default btn--inline btn--large btn--upper">
                Jetzt als "Verein des Monats" bewerben</a>
        </p>
    </div>
</div>