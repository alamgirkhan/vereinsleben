@extends('layouts.master')

@section('content')
    {{-- initialize app --}}
    <div id="vote-app">
        <header class="vote-detail-header">
            <div class="container">
                <div class="vote-detail-header__caption">
                    <h1 class="vote-detail-header__headline">{{ $club->name }}</h1>
                    <span class="vote-detail-header__subline">
                    Wähle deinen Verein des Monats
                </span>
                </div>
            </div>
        </header>

        <div class="section section--vote-white section--vote-detail-bordered">
            <div class="container">
                <div class="col-xs-12 col-md-8">
                    <section class="news-detail__main">
                        <div class="news__category-wrapper">
                            <a class="news-detail__category-link" href="{{ route('vote.index') }}">
                                <span class="news-detail__category">Wahl zum Verein des Monats</span>
                            </a>
                            <a class="news-detail__category-link" href="{{ route('club.detail', $club->slug) }}">
                                &raquo; <span class="news-detail__subcategory">{{ $club->name }}</span>
                            </a>
                        </div>
                        <div class="vote-detail-headline-wrapper">
                            <h1 class="news-detail__headline">{{ $nominee->project }}</h1>
                        </div>
                        <div class="news-detail__content">
                            {!! $nominee->project_description !!}
                        </div>

                    </section>
                </div>
                <div class="vote-detail-sidebar-wrapper col-xxs-12 col-xs-offset-0 col-xs-6 col-md-3 col-md-offset-1">
                    <div class="vote-detail-sidebar">
                        <img src="{{ asset($club->avatar->url('singleView')) }}" alt="Avatar"
                             class="vote-detail-sidebar__img">

                        {{-- vote button --}}
                        @if($nominee->vote->isCurrent())
                        <vote-button label="Abstimmen" :id="{{ $nominee->vote_id }}"
                                     :authenticated="{{ (int)Auth::check() }}"
                                     nominee="{{ $nominee->id }}"
                                     voted="{{ Auth::check() && $hasVoted }}"></vote-button>
                        @endif
                        <div class="vote-detail-sidebar__box">
                        <span class="tiny-headline">
                            Über den Verein
                        </span>
                            <div class="vote-detail-sidebar__meta">
                                <span class="vote-detail-sidebar__meta-item">Kürzel: {{ $club->shorthand or '' }}</span>
                                <span class="vote-detail-sidebar__meta-item">Gegründet: {{ $club->founded or '' }}</span>
                                <span class="vote-detail-sidebar__meta-item">
                                Sportart(en): {{ $club->sports->take(3)->pluck('title')->implode(', ') }}
                            </span>
                                <span class="vote-detail-sidebar__meta-item">Mitglieder: {{ $club->member_count }}</span>
                                <span class="vote-detail-sidebar__meta-item vote-detail-sidebar__meta-item--geo">
                                {{ $club->fullAddress() }}
                            </span>
                            </div>
                        </div>
                        <a target="_blank" href="{{ route('club.detail', $club->slug) }}"
                           class="btn btn--default btn--upper btn--full-width">Zum Vereinsprofil</a>
                    </div>
                </div>
            </div>
            <div class="container">
                @include ('vote.partials.btn-group')
            </div>
        </div>
        {{--profile wizard--}}
        @if(Auth::check())
            @include('user.profile.modal.profile-info-wizard', ['me' => $me, 'state' => $state, 'show' => 1])
        @endif
        @include('vote.addthis')
        {{-- info popup --}}
        @include('vote.partials.popup')
    </div>

@endsection

@push('scripts')
<script src="{{ asset(('js/vote.js')) }}"></script>
@endpush