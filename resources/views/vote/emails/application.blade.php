<!doctype html>
<html lang="de">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Verein des Monats Bewerbung</title>
    </head>
    <body>
        <h1>Neue Bewerbung für einen Verein des Monats</h1>

        <h2>Informationen über den Verein</h2>

        <table class="table">
            <tr>
                <td>Vereinsname</td>
                <td>{{ $club['name'] or '' }}</td>
            </tr>
            <tr>
                <td>Adresse</td>
                <td>{{ $club['street'] or '' }} {{ $club['house_number'] or '' }}
                    , {{ $club['zip'] or '' }} {{ $club['city'] or '' }}</td>
            </tr>
            <tr>
                <td>Gründungsjahr</td>
                <td>{{ $club['founded'] or '' }}</td>
            </tr>
            <tr>
                <td>Kürzel</td>
                <td>{{ $club['shorthand'] or '' }}</td>
            </tr>
            <tr>
                <td>Mitgliederanzahl</td>
                <td>{{ $club['member_count'] or '' }}</td>
            </tr>
            <tr>
                <td>Sportarten</td>
                <td>{{ join(', ', $club['sports']) }}</td>
            </tr>
        </table>

        <h2>Informationen zum Ansprechpartner</h2>

        <table class="table">
            <tr>
                <td>Anrede</td>
                <td>{{ ($user['gender'] == 'female') ? 'Frau' : 'Herr' }}</td>
            </tr>
            <tr>
                <td>Name</td>
                <td>{{ $user['firstname'] or '' }} {{ $user['lastname'] or '' }}</td>
            </tr>
            <tr>
                <td>Adresse</td>
                <td>{{ $user['street'] or '' }} {{ $user['house_number'] or '' }}, {{ $user['zip'] or '' }} {{ $user['city'] or '' }}</td>
            </tr>
            <tr>
                <td>E-Mail</td>
                <td>{{ $user['email'] or '' }}</td>
            </tr>
            <tr>
                <td>Telefon</td>
                <td>{{ $user['phone'] or '' }}</td>
            </tr>
            <tr>
                <td>Newsletter</td>
                <td>{{ $user['newsletter'] ? 'Ja' : 'Nein' }}</td>
            </tr>
            <tr>
                <td>Datenübermittlung</td>
                <td>{{ $user['privacy'] ? 'Ja' : 'Nein' }}</td>
            </tr>
        </table>

        <h2>Informationen & Daten zum Projekt</h2>

        <table class="table">
            <tr>
                <td>Titel</td>
                <td>{{ $voteproject['title'] or '' }}</td>
            </tr>
            <tr>
                <td>Beschreibung</td>
                <td>{{ $voteproject['description'] or '' }}</td>
            </tr>
            <tr>
                <td>Zeitraum</td>
                <td>{{ $voteproject['start_date'] or '' }} - {{ $voteproject['end_date'] or '' }}</td>
            </tr>
        </table>
        @if (isset($data['imprint']))
            <br/>
            <br/>
            <br/>
            <div>{!! $data['imprint'] !!}</div>
        @endif
    </body>
</html>