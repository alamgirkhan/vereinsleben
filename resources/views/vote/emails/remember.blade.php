<!DOCTYPE html>
<html lang="de-DE">
<head>
    <meta charset="utf-8">
</head>
<body>

<div>
    <strong>Hallo {{ $user->firstname }},</strong><br>
    <br>
    Regelmäßigkeit macht Sieger. Zumindest beim „Verein des Monats“. Deswegen erinnern wir Dich gerne daran, heute für
    Deinen Verein abzustimmen und ihm damit eine Stimme näher ans Siegertreppchen zu bringen.<br><br>

    Jetzt abstimmen auf www.vereinsleben.de/verein-des-monats<br><br>

    Wir drücken Dir und Deinem Favoriten die Daumen!<br>
    Dein vereinsleben.de-Team<br><br>
    <a href="{{ URL::to('vote/notremember/' . $deleted_token) }}">Klicke hier, um keine weiteren Erinnerungen zu
        erhalten</a>


</div>
@if(isset($imprint))
    <br/>
    <br/>
    <br/>
    <div>{!! $imprint->content !!}</div>
@endif
</body>
</html>