<header class="vote-header vote-header-new">
    <div class="container">
        <div class="vote-header__caption">
            <h1 class="vote-header__headline">
                Verein des Monats
            </h1>
            <span class="vote-header__subline">Der Wettbewerb für herausragende Vereinsarbeit</span>
            <div class="vote-header__sponsors-container">
                <a href="/verein-des-monats/sponsoren/sparda-bank" class="vote-header__logo-link">
                    <img src="/default_images/vote/sponsores/vdm_partnerlogo_klein_sparda.png" alt="Sparda-Bank">
                </a>
                <a href="/verein-des-monats/sponsoren/landessportbund-rheinland-pfalz" class="vote-header__logo-link">
                    <img src="/default_images/vote/sponsores/vdm_partnerlogo_klein_lsb.png" alt="Landessportbund
                    Rheinland-Pfalz">
                </a>
            </div>
            <div class="vote-header__sponsors-container">

                <a href="/verein-des-monats/sponsoren/rpr1" class="vote-header__logo-link">
                    <img src="/default_images/vote/sponsores/vdm_partnerlogo_klein_rpr1.png" alt="RPR1.">
                </a>
                <a href="/verein-des-monats/sponsoren/bigfm" class="vote-header__logo-link">
                    <img src="/default_images/vote/sponsores/vdm_partnerlogo_klein_bigfm.png" alt="abc EUROPE - Werbeagentur">
                </a>
            </div>
        </div>
    </div>
</header>