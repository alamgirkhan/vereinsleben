@extends('layouts.master')
@section('title', 'vereinsleben.de - Verein des Monats - Der Wettbewerb für herausragende Vereinsarbeit')
@section('meta_description', 'Jetzt abstimmen und eurem Favoriten helfen, das Preispaket im Gesamtwert von 10.000 Euro zu gewinnen; oder bewerbt euch selbst als Verein des Monats auf www.vereinsleben.de.')

@section('content')

    <div id="vote-app">

        {{-- vote header --}}
        @include('vote.header')

         {{--bordered section--}}
        @include('vote.bordered-section')

        {{-- reward section --}}
        @include('vote.reward')

        {{-- info popup --}}
        @include('vote.partials.popup')

        {{--ranking--}}
        @include('vote.ranking')
    </div>
@endsection
@push('scripts')
<style>
    .partner-logo {
        margin-top: 0!important;
        background: none;
    }
    .section--vote-bg-new {
        margin-bottom: -75px;
        margin-top: -70px;
        position: relative;
    }
    @media (max-width: 1090px) {
        .section--vote-bg-new {
            margin-top: -50px;
        }
    }
    @media (max-width: 768px) {
        .section--vote-bg-new {
            margin-bottom: -95px;
            margin-top: -40px;
        }
    }
</style>
<script src="{{ asset(('js/vote.js')) }}"></script>
<script src="{{ asset(('js/OwlCarousel2/owl.carousel.js')) }}"></script>

<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58a427b07de68834"></script>

@endpush