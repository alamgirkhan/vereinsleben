<header class="vote-header">
    <div class="container">
        <div class="vote-header__caption">
            <h1 class="vote-header__headline">
                Wählt euren <br>Verein des Monats
            </h1>
            <span class="vote-header__subline">Eure Auszeichnung für besonderes soziales Engagement</span>
            <div class="vote-header__sponsors-container">
                <a href="#" class="vote-header__logo-link">
                    <img src="http://placehold.it/160x60" alt="">
                </a>
                <a href="#" class="vote-header__logo-link">
                    <img src="http://placehold.it/160x60" alt="">
                </a>
                <a href="#" class="vote-header__logo-link">
                    <img src="http://placehold.it/160x60" alt="">
                </a>
            </div>
        </div>
    </div>
</header>

<div class="section section--vote-white section--vote-bordered">
    <div class="container">
        <h2 class="hl hl--large hl--underlined hl--inverted hl--centered hl--upper">
            Eure Vereinshelden
        </h2>
        <p class="text-block text-block--compressed">
            Lorem ipsum dolor sit amet, consetetur sadipscing magna aliquyam erat, sed diam voluptua. At vero eos et
            accusam <a href="#" class="link link--underlined">über den Wettbewerb</a> rebum. Stet clita kasd gubergren,
            no sea takimata sanctus
            dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
            invidunt ut labore et dolore magna aliquyam erat, <a href="#" class="link link--underlined">Teilnahmebedingungen</a>
            scusam et justo duo
            dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
        </p>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="vote-teaser">
                    <a href="#" class="vote-teaser__img"
                       style="background-image: url('http://lorempixel.com/270/260?=a')">
                        <div class="vote-teaser__badge">Details</div>
                    </a>
                    <div class="vote-teaser__content">
                        <div class="vote-teaser__project-name">
                            Beispiel für Projektname
                        </div>
                        <div class="vote-teaser__club-details">
                            Muster Vereinsname Musterstadt
                        </div>
                    </div>
                    <a href="#" class="btn btn--default btn--upper btn--full-width">
                        Abstimmen
                    </a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="vote-teaser">
                    <a href="#" class="vote-teaser__img"
                       style="background-image: url('http://lorempixel.com/270/260?=b')">
                        <div class="vote-teaser__badge">Details</div>
                    </a>
                    <div class="vote-teaser__content">
                        <div class="vote-teaser__project-name">
                            Beispiel für Projektname
                        </div>
                        <div class="vote-teaser__club-details">
                            Muster Vereinsname Musterstadt
                        </div>
                    </div>
                    <a href="#" class="btn btn--default btn--upper btn--full-width">
                        Abstimmen
                    </a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="vote-teaser">
                    <a href="#" class="vote-teaser__img"
                       style="background-image: url('http://lorempixel.com/270/260?=c')">
                        <div class="vote-teaser__badge">Details</div>
                    </a>
                    <div class="vote-teaser__content">
                        <div class="vote-teaser__project-name">
                            Beispiel für Projektname
                        </div>
                        <div class="vote-teaser__club-details">
                            Muster Vereinsname Musterstadt
                        </div>
                    </div>
                    <a href="#" class="btn btn--default btn--upper btn--full-width">
                        Abstimmen
                    </a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="vote-teaser">
                    <a href="#" class="vote-teaser__img"
                       style="background-image: url('http://lorempixel.com/270/260?=d')">
                        <div class="vote-teaser__badge">Details</div>
                    </a>
                    <div class="vote-teaser__content">
                        <div class="vote-teaser__project-name">
                            Beispiel für Projektname
                        </div>
                        <div class="vote-teaser__club-details">
                            Muster Vereinsname Musterstadt
                        </div>
                    </div>
                    <a href="#" class="btn btn--default btn--upper btn--full-width">
                        Abstimmen
                    </a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="vote-teaser">
                    <a href="#" class="vote-teaser__img"
                       style="background-image: url('http://lorempixel.com/270/260?=e')">
                        <div class="vote-teaser__badge">Details</div>
                    </a>
                    <div class="vote-teaser__content">
                        <div class="vote-teaser__project-name">
                            Beispiel für Projektname
                        </div>
                        <div class="vote-teaser__club-details">
                            Muster Vereinsname Musterstadt
                        </div>
                    </div>
                    <a href="#" class="btn btn--default btn--upper btn--full-width">
                        Abstimmen
                    </a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="vote-teaser">
                    <a href="#" class="vote-teaser__img"
                       style="background-image: url('http://lorempixel.com/270/260?=f')">
                        <div class="vote-teaser__badge">Details</div>
                    </a>
                    <div class="vote-teaser__content">
                        <div class="vote-teaser__project-name">
                            Beispiel für Projektname
                        </div>
                        <div class="vote-teaser__club-details">
                            Muster Vereinsname Musterstadt
                        </div>
                    </div>
                    <a href="#" class="btn btn--default btn--upper btn--full-width">
                        Abstimmen
                    </a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="vote-teaser">
                    <a href="#" class="vote-teaser__img"
                       style="background-image: url('http://lorempixel.com/270/260?=g')">
                        <div class="vote-teaser__badge">Details</div>
                    </a>
                    <div class="vote-teaser__content">
                        <div class="vote-teaser__project-name">
                            Beispiel für Projektname
                        </div>
                        <div class="vote-teaser__club-details">
                            Muster Vereinsname Musterstadt
                        </div>
                    </div>
                    <a href="#" class="btn btn--default btn--upper btn--full-width">
                        Abstimmen
                    </a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="vote-teaser">
                    <a href="#" class="vote-teaser__img"
                       style="background-image: url('http://lorempixel.com/270/260?=h')">
                        <div class="vote-teaser__badge">Details</div>
                    </a>
                    <div class="vote-teaser__content">
                        <div class="vote-teaser__project-name">
                            Beispiel für Projektname
                        </div>
                        <div class="vote-teaser__club-details">
                            Muster Vereinsname Musterstadt
                        </div>
                    </div>
                    <a href="#" class="btn btn--default btn--upper btn--full-width">
                        Abstimmen
                    </a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-md-offset-3">
                <div class="vote-teaser">
                    <a href="#" class="vote-teaser__img"
                       style="background-image: url('http://lorempixel.com/270/260?=i')">
                        <div class="vote-teaser__badge">Details</div>
                    </a>
                    <div class="vote-teaser__content">
                        <div class="vote-teaser__project-name">
                            Beispiel für Projektname
                        </div>
                        <div class="vote-teaser__club-details">
                            Muster Vereinsname Musterstadt
                        </div>
                    </div>
                    <a href="#" class="btn btn--default btn--upper btn--full-width">
                        Abstimmen
                    </a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="vote-teaser">
                    <a href="#" class="vote-teaser__img"
                       style="background-image: url('http://lorempixel.com/270/260?=j')">
                        <div class="vote-teaser__badge">Details</div>
                    </a>
                    <div class="vote-teaser__content">
                        <div class="vote-teaser__project-name">
                            Beispiel für Projektname
                        </div>
                        <div class="vote-teaser__club-details">
                            Muster Vereinsname Musterstadt
                        </div>
                    </div>
                    <a href="#" class="btn btn--default btn--upper btn--full-width">
                        Abstimmen
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section section--vote-bg">
    <div class="container">
        <h2 class="hl hl--large hl--underlined hl--inverted hl--centered hl--upper">
            Mach mit und werde selbst zum Gewinner
        </h2>
        <p class="text-block text-block--compressed">
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
            nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et
            accusam et justo duo dolores et ea rebum.
        </p>
    </div>
</div>

<div class="section section--vote-dark">
    <div class="container">
        <h2 class="hl hl--large hl--underlined hl--light hl--centered hl--upper">
            Aktuelle Platzierung
        </h2>
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <div class="vote-placement">
                    <div class="vote-placement__num">1.</div>
                    <img src="http://lorempixel.com/68/68?=a" class="vote-placement__img">
                    <div class="vote-placement__club">Vereinsname e.V.</div>
                    <a href="#" class="btn btn--inline btn--default btn--upper">Abstimmen</a>
                </div>

                <div class="vote-placement">
                    <div class="vote-placement__num">2.</div>
                    <img src="http://lorempixel.com/68/68?=b" class="vote-placement__img">
                    <div class="vote-placement__club">Vereinsname e.V.</div>
                    <a href="#" class="btn btn--inline btn--default btn--upper">Abstimmen</a>
                </div>

                <div class="vote-placement">
                    <div class="vote-placement__num">3.</div>
                    <img src="http://lorempixel.com/68/68?=c" class="vote-placement__img">
                    <div class="vote-placement__club">Vereinsname e.V.</div>
                    <a href="#" class="btn btn--inline btn--default btn--upper">Abstimmen</a>
                </div>

                <div class="vote-placement">
                    <div class="vote-placement__num">4.</div>
                    <img src="http://lorempixel.com/68/68?=d" class="vote-placement__img">
                    <div class="vote-placement__club">Vereinsname e.V.</div>
                    <a href="#" class="btn btn--inline btn--default btn--upper">Abstimmen</a>
                </div>

                <div class="vote-placement">
                    <div class="vote-placement__num">5.</div>
                    <img src="http://lorempixel.com/68/68?=e" class="vote-placement__img">
                    <div class="vote-placement__club">Vereinsname e.V.</div>
                    <a href="#" class="btn btn--inline btn--default btn--upper">Abstimmen</a>
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="vote-placement">
                    <div class="vote-placement__num">6.</div>
                    <img src="http://lorempixel.com/68/68?=f" class="vote-placement__img">
                    <div class="vote-placement__club">Vereinsname e.V.</div>
                    <a href="#" class="btn btn--inline btn--default btn--upper">Abstimmen</a>
                </div>

                <div class="vote-placement">
                    <div class="vote-placement__num">7.</div>
                    <img src="http://lorempixel.com/68/68?=g" class="vote-placement__img">
                    <div class="vote-placement__club">Vereinsname e.V.</div>
                    <a href="#" class="btn btn--inline btn--default btn--upper">Abstimmen</a>
                </div>

                <div class="vote-placement">
                    <div class="vote-placement__num">8.</div>
                    <img src="http://lorempixel.com/68/68?=h" class="vote-placement__img">
                    <div class="vote-placement__club">Vereinsname e.V.</div>
                    <a href="#" class="btn btn--inline btn--default btn--upper">Abstimmen</a>
                </div>

                <div class="vote-placement">
                    <div class="vote-placement__num">9.</div>
                    <img src="http://lorempixel.com/68/68?=i" class="vote-placement__img">
                    <div class="vote-placement__club">Vereinsname e.V.</div>
                    <a href="#" class="btn btn--inline btn--default btn--upper">Abstimmen</a>
                </div>

                <div class="vote-placement">
                    <div class="vote-placement__num">10.</div>
                    <img src="http://lorempixel.com/68/68?=j" class="vote-placement__img">
                    <div class="vote-placement__club">Vereinsname e.V.</div>
                    <a href="#" class="btn btn--inline btn--default btn--upper">Abstimmen</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section section--vote-white">
    <div class="container">
        <h2 class="hl hl--large hl--underlined hl--inverted hl--centered hl--upper">
            Gewinner des Monats Februar
        </h2>
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <img class="responsive-image" src="http://lorempixel.com/550/380?=a" alt="">
            </div>
            <div class="col-xs-12 col-md-6">
                <p class="text-block">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
                    labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores
                    et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
                    labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores
                    et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
                    labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores
                    et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
                </p>
                <a href="#" class="btn btn--inline btn--default btn--upper">Zur Vereinsseite</a>
            </div>
        </div>
    </div>
</div>