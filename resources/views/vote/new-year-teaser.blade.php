<div class="new-year-teaser">
    <div class="container"><h2 class="hl hl--large hl--underlined hl--inverted hl--centered hl--upper">
            WIR SUCHEN ENGAGIERTE VEREINE!
        </h2>
        <div class="text-block text-block--compressed text-block--thin text-block--smaller-spacing">
            <p>
                Ihr seid mit eurem Verein in Rheinland-Pfalz und im Saarland auch ganz vorne mit dabei, wenn es um soziales Engagement, z.B. in den Bereichen Integration, Inklusion, Bildung, Nachwuchsarbeit, Seniorensport usw. geht? Euer Verein ist in eurer Stadt einfach unentbehrlich, denn ihr übernehmt  über die normale Vereinsarbeit hinausgehende Aufgaben? Oder ihr habt ein anspruchsvolles Projekt mit eurem Verein komplett ehrenamtlich auf die Beine gestellt?
            </p>
            <p>
                Ihr startet mit eurer Vereinsarbeit richtig durch, bewirkt viel Gutes und die Menschen in eurer Stadt und der Umgebung kenne euch als Macher und gute Vorbilder?  Ihr macht also mehr aus eurem Verein als andere, dann sind wir auf der Suche nach Euch. Denn euer Verein verdient dafür noch mehr Aufmerksamkeit als bisher, auch Wertschätzung in Form eines attraktiven Preispakets.
            </p>
            <p>
                Bewerbt euch jetzt bei uns, aktiviert eure Mitglieder, Fans und Freunde, für euch abzustimmen und gewinnt jeden Monat * 10.000 Euro vom Gewinnsparverein der Sparda-Bank Südwest. Aber damit nicht genug: Die Vereine, die bei der Abstimmung die Plätze eins, zwei und drei belegen, erhalten außerdem jeweils einen Gutschein über 30 Tickets für ein Heimspiel des FSV Mainz 05. Weiterhin vergeben wir im Anschluss an den Aktionszeitraum * unter allen Monatssiegern weitere Geldpreise im Gesamtwert von 5.000 Euro. <a href="/verein-des-monats/ueber-den-wettbewerb" target="_blank" class="text-block__link">Weitere Information über den Wettbewerb findet ihr hier</a> und <a href="/verein-des-monats/teilnahmebedingungen" target="_blank" class="text-block__link">hier geht es zu den Teilnahmebedingungen</a>.
            </p>
            <p style="text-align: right;">
                * Jeden Monat im Aktionszeitraum März bis November 2019
            </p>
            <p class="text-block text-block--compressed text-block--align-center">
                <a href="/verein-des-monats/teilnehmen" class="btn btn--default btn--inline btn--large btn--upper">
                    Jetzt als "Verein des Monats" bewerben
                </a>
            </p>
        </div>
    </div>
</div>
