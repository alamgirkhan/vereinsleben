<div class="container">
    <div class="pd-0-mg-200 mt-0 mb-0">
        <div class="row " data-vote="{{ $vote->id }}">
            @foreach($vote->voteNominees as $index => $nominee)
                @include('vote.nominee-teaser', [
                    'voteId' => $vote->id,
                    'club' => $nominee->voteable,
                    'nominee' => $nominee,
                    'index' => $index
                ])
            @endforeach
        </div>
    </div>

    {{-- winner --}}
    @include('vote.previous-winner')
    @if(Auth::check())
        @include('user.profile.modal.profile-info-wizard', ['me' => $me, 'state' => $state, 'show' => 1])
    @endif


</div>