<div class="col-xxs-10 col-xxs-offset-1 col-xs-6 col-sm-4 col-md-4 @if($index == 3 ) col-md-offset-2 @endif @if($index > 100 && $index < 10) col-sm-offset-4 col-md-offset-2 @endif">
    <div class="vote-teaser">
        <a href="{{ route('club.vote.nominee', $club->slug) }}" class="vote-teaser__img"
           style="background-image: url('{{ asset($club->avatar->url('startPage')) }}')">
            <div class="vote-teaser__badge">Details</div>
        </a>
        <div class="vote-teaser__content-wrapper">
            <div class="vote-teaser__content">
                <div class="vote-teaser__project-name">
                    {{ $nominee->project }}
                </div>
                <a href="{{ route('club.vote.nominee', $club->slug) }}" class="vote-teaser__club-details">
                    {{ $club->name }}
                </a>
            </div>
        </div>
        <vote-button label="Abstimmen" :id="{{ $nominee->vote_id }}"
                     :authenticated="{{ (int)Auth::check() }}"
                     nominee="{{ $nominee->id }}"
                     voted="{{ Auth::check() && $hasVoted }}"></vote-button>
    </div>
</div>