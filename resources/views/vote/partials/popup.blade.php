<popup>
    <div class="modal__content">
        <div class="modal__inner">
            <popup-vote-content authorized="{{ Auth::check() }}"
                                hasvoted="{{ $hasVoted ? true : false }}"
                                remember="{{$remember == 0 ? false : true}}"></popup-vote-content>
        </div>
    </div>
</popup>