@extends('layouts.master')

@section('content')
    <header class="vote-header">
        <div class="container">
            <div class="vote-header__caption">
                <h1 class="vote-header__headline">
                    Jetzt bewerben
                </h1>
                <span class="vote-header__subline">Jetzt bewerben und Verein des Monats werden</span>
                <div class="vote-header__sponsors-container">
                    <a href="/verein-des-monats/sponsoren/sparda-bank" class="vote-header__logo-link">
                        <img src="/default_images/vote/sponsores/vdm_partnerlogo_klein_sparda.png" alt="Sparda-Bank">
                    </a>
                    <a href="/verein-des-monats/sponsoren/landessportbund-rheinland-pfalz" class="vote-header__logo-link">
                        <img src="/default_images/vote/sponsores/vdm_partnerlogo_klein_lsb.png" alt="Landessportbund
                        Rheinland-Pfalz">
                    </a>
                </div>
                <div class="vote-header__sponsors-container">

                    <a href="/verein-des-monats/sponsoren/rpr1" class="vote-header__logo-link">
                        <img src="/default_images/vote/sponsores/vdm_partnerlogo_klein_rpr1.png" alt="RPR1.">
                    </a>
                    <a href="/partner/bigfm" class="vote-header__logo-link">
                        <img src="/default_images/vote/sponsores/vdm_partnerlogo_klein_bigfm.png" alt="abc EUROPE - Werbeagentur">
                    </a>
                </div>
            </div>
        </div>
    </header>
    <div class="section section--vote-white section--vote-detail-bordered section--vote-wizard">
        <p class="text-block text-block--compressed text-block--align-center">
            <a href="/verein-des-monats/teilnehmen" class="btn btn--default btn--inline btn--large btn--upper">
                Verein nicht gefunden?
            </a>
        </p>

        <form action="/verein-des-monats/bewerben" method="POST">
            {{csrf_field()}}
            <input type="hidden" name="club_id" id="club_id" value=""/>
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <label class="input-label" for="state">Bundesland wechseln</label>
                        <div class="input-group">
                            <select name="federal_state" id="federal_state" class="input">
                                @foreach($federal_states as $state)
                                    <option value="{{ $state->name }}">{{ $state->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <label class="input-label" for="club">Titel des Projekts *</label>
                        <div class="input-group">
                            {!! Form::text('club','', ['class' => 'input club-autocomplete', 'data-clubs-url' => app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('api.club.autocomplete'), 'placeholder' => 'Titel des Projekts']) !!}
                        </div>
                    </div>

                    <div class="col-md-12">
                        <label class="input-label" for="description">Kurzbeschreibung *</label>
                        <div class="input-group">
                            <textarea cols="30" placeholder="Kurzbeschreibung"
                                      rows="10" class="input" name="description" id="description"
                                      required="required"></textarea>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <label class="input-label" for="start_date">Beginn des Projekts *</label>
                        <div class="input-group">
                            <input
                                   placeholder="Beginn des Projekts" class="input" type="text" name="start_date"
                                   id="start_date"
                                   required="required">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <label class="input-label" for="end_date">Ende des Projekts</label>
                        <div class="input-group">
                            <input placeholder="Ende des Projekts" class="input" type="text" name="end_date"
                                   id="end_date"
                                   required="required">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="input-group" data-upload-result>
                        <label for="club_logo" class="btn btn--grey btn--full-width btn--large"><span
                                    class="fa fa-upload"></span> Vereinslogo oder -wappen *</label>
                        <input class="input-file" type="file"
                               name="logo" id="club_logo" required="required">
                        <div id="upload-result" class="thumbnail__list"></div>
                    </div>

                    <div class="input-group" data-upload-result>
                        <label for="project_image" class="btn btn--grey btn--full-width btn--large"><span
                                    class="fa fa-upload"></span> Bilder vom Projekt *</label>
                        <input multiple type="file"
                               name="projectImages" id="project_image" class="input-file">
                        <div id="upload-result" class="thumbnail__list"></div>
                    </div>

                    <div class="input-group" data-upload-result>
                        <label for="projectdesc_image" class="btn btn--grey btn--full-width btn--large"><span
                                    class="fa fa-upload"></span> Ausführliche Projektbeschreibung *</label>
                        <input type="file"
                               name="projectDescription" id="projectdesc_image" class="input-file">
                        <div id="upload-result" class="thumbnail__list"></div>
                    </div>

                    <div class="input-group" data-upload-result>
                        <label for="club_image" class="btn btn--grey btn--full-width btn--large"><span
                                    class="fa fa-upload"></span> Bild vom Verein</label>
                        <input type="file" multiple name="clubImages[]"
                               id="club_image" class="input-file">
                        <div id="upload-result" class="thumbnail__list"></div>
                    </div>

                    <div class="col-md-12 section__content--centered">
                        <button class="btn btn--default btn--inline btn--large btn--upper btn--fixed-width">Bewerben
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection