@extends('layouts.master')

@section('content')
    <header class="vote-detail-header header-blank">
        <div class="container">
            <div class="vote-header__caption">
                <h1 class="vote-header__headline">
                    Jetzt bewerben
                </h1>
                <span class="vote-header__subline">Jetzt bewerben und Verein des Monats werden</span>
                <div class="vote-header__sponsors-container">
                    <a href="/verein-des-monats/sponsoren/sparda-bank" class="vote-header__logo-link">
                        <img src="/default_images/vote/sponsores/vdm_partnerlogo_klein_sparda.png" alt="Sparda-Bank">
                    </a>
                    <a href="/verein-des-monats/sponsoren/landessportbund-rheinland-pfalz" class="vote-header__logo-link">
                        <img src="/default_images/vote/sponsores/vdm_partnerlogo_klein_lsb.png" alt="Landessportbund
                        Rheinland-Pfalz">
                    </a>
                </div>
                <div class="vote-header__sponsors-container">

                    <a href="/verein-des-monats/sponsoren/rpr1" class="vote-header__logo-link">
                        <img src="/default_images/vote/sponsores/vdm_partnerlogo_klein_rpr1.png" alt="RPR1.">
                    </a>
                    <a href="/verein-des-monats/sponsoren/bigfm" class="vote-header__logo-link">
                        <img src="/default_images/vote/sponsores/vdm_partnerlogo_klein_bigfm.png" alt="abc EUROPE - Werbeagentur">
                    </a>
                </div>
            </div>
        </div>
    </header>


    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt=""
             src="//www.googleadservices.com/pagead/conversion/869385028/?label=DUanCLC9-4IBEMSGx54D&amp;guid=ON&amp;script=0"/>
    </div>

    <div class="section section--vote-white section--vote-detail-bordered section--vote-wizard no-before">
        <div id="form-app">
            <form-wizard></form-wizard>
        </div>
    </div>

    @push('scripts')
        <!-- Google Code for Verein des Monats Conversion Page -->
        <script type="text/javascript">
            / <![CDATA[ /
            var google_conversion_id = 869385028;
            var google_conversion_label = "DUanCLC9-4IBEMSGx54D";
            var google_remarketing_only = false;
            / ]]> /
        </script>
        <script type="text/javascript"
                src="//www.googleadservices.com/pagead/conversion.js">
        </script>


        <script src="{{ asset('js/form.js') }}"></script>
    @endpush

@endsection