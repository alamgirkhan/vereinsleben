@foreach($rankings as $rank => $ranking)
    @if($rank >= ($from - 1) && $rank < ($to - 1))
        <div class="vote-placement">
            <div class="vote-placement__content">
                <div class="vote-placement__num">{{ $rank + 1 }}.</div>
                <a href="{{ route('club.vote.nominee', $ranking->voteable->slug) }}">
                    <img src="{{ asset($ranking->voteable->avatar->url('startPage')) }}" class="vote-placement__img">
                </a>
                <a href="{{ route('club.vote.nominee', $ranking->voteable->slug) }}"
                class="vote-placement__club">{{ $ranking->voteable->name }}</a>
            </div>
            <vote-button label="Abstimmen" :id="{{ $vote->id }}"
                         :authenticated="{{ (int)Auth::check() }}"
                         nominee="{{ $ranking->id }}"
                         voted="{{ Auth::check() && $hasVoted }}"></vote-button>
        </div>
    @endif
@endforeach