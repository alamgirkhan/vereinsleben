<div id="vote-ranking" class="section section--vote-dark">
    <div class="container">
        <h2 class="hl hl--large hl--underlined hl--light hl--centered hl--upper">
            Aktuelle Platzierung
        </h2>
        <div class="row">
            <div class="col-xs-12 col-md-6 col-md-offset-3">
                @include('vote.ranking-data', [
                    'rankings' => $rankings,
                    'from' => 1,
                    'to' => 6
                ])
            </div>
        </div>
    </div>
</div>