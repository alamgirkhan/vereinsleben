@extends('layouts.master')

@section('content')


    <header class="vote-sponsor-header--fortuna">
        <div class="container">
            <div class="row pos-rel">
                <img class="vote-sponsor-logo-detail" src="/default_images/vote/sponsores/logo-detail/fortuna-logo-detail.png"
                     alt="Fortuna - Turn- und Sportgeräte">
            </div>
            <div class="vote-detail-header__caption">
                <h1 class="vote-detail-header__headline">
                    fortuna-Sportgeräte
                </h1>
                <span class="vote-detail-header__subline">
                    Sportgeräte made in Germany
                </span>
            </div>
        </div>
    </header>

    <div class="section section--vote-white section--vote-detail-bordered">
        <div class="container">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <section class="news-detail__main">

                    <div class="news-detail__content">

                        <h2>Das Unternehmen</h2>

                        <p>
                            fortuna-Sportgeräte steht für Tradition, Qualität und Sicherheit. Seit über 50 Jahren versorgt das Unternehmen aus dem Westerwald Sporthallen und Sportanlagen mit Geräten aller Art. Ob Springbock, Holzbank oder Basketballanlage. Ob Federball, Ballettspiegel oder Magnesiumständer - das Sortiment ist nahezu grenzenlos.
                        </p>
                        <img src="/default_images/vote/sponsores/fortuna-halle-3.jpg" alt="Fortuna - Turn- und
                        Sportgeräte">
                        <p>
                            Ein weiterer großer Bereich des Angebotes von fortuna-Sportgeräte ist die Inspektion und Wartung. Egal ob ein Gutachten erstellt werden muss, ein ganzes Sanierungskonzept erarbeitet und geplant werden soll oder einfach nur ein Gerät repariert werden muss – fortuna-Sportgeräte ist der richtige Ansprechpartner.

                        </p>

                        <h2>
                            Unser Anspruch
                        </h2>
                        <p>
                            Wir bewegen Deutschland – Mehr Sport in deutschen Sporthallen und Bewegung mit Spaß und Freude.
                            Deutschland bewegt sich mit fortuna – Unfallsichere Ausstattung durch fortuna-Sportgeräte.
                        </p>

                        <h2>
                            Weitere Informationen
                        </h2>
                        <p>
                            Mehr über fortuna-Sportgeräte, das Unternehmen und die Produkte findet ihr im Internet unter <a href="https://www.fortuna-sport.de/" target="_blank">www.fortuna-sport.de</a>
                        </p>

                    </div>
                    @include ('vote.partials.btn-group')



                </section>
            </div>
        </div>
    </div>

@endsection
