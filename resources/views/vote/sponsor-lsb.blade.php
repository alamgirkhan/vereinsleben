@extends('layouts.master')

@section('content')


    <header class="vote-sponsor-header--lsb header-blank">
        <div class="container">
            <div class="row pos-rel">
                <img class="vote-sponsor-logo-detail" src="/default_images/vote/sponsores/logo-detail/lsp-logo-detail.png"
                     alt="Logo Landessportbund Rheinland-Pfalz">
            </div>

            <div class="vote-detail-header__caption">
                <h1 class="vote-detail-header__headline">
                    Landessportbund Rheinland-Pfalz
                </h1>
                <span class="vote-detail-header__subline">
                    Das Dach des Sports in Rheinland-Pfalz
                </span>
            </div>
        </div>
    </header>

    <div class="section section--vote-white section--vote-detail-bordered no-before">
        <div class="container">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <section class="news-detail__main">

                    <div class="news-detail__content">

                        <h2>Der Landessportbund</h2>
                        <p>
                            Der Landessportbund Rheinland-Pfalz (LSB) ist die Dachorganisation des Sports in Rheinland-Pfalz. Mehr als 6.300 Vereine und über 1,5 Millionen Mitglieder machen ihn zur größten Personenvereinigung des Landes. Damit ist jeder dritte Bürger unseres Landes Mitglied in einem Sportverein!
                        </p>
                        <img src="/default_images/vote/sponsores/lsb_content_bild.jpg" alt="Fortuna - Turn-
                         und
                        Sportgeräte">
                        <p>
                            Die Sportbünde Rheinland (Koblenz), Pfalz (Kaiserslautern) und Rheinhessen (Mainz), die Fachverbände und die Vereine sind Träger des Sports in Rheinland-Pfalz.
                        </p>

                        <h2>
                            Unsere Aufgaben
                        </h2>
                        <ul class="vote-sponsor-ul">
                            <li>Wahrnehmung der Interessen gegenüber dem Deutschen Olympischen Sportbund, den staatlichen und kommunalen Stellen und gesellschaftlichen Gruppierungen in Rheinland-Pfalz;</li>
                            <li>Information und Beratung der Mitglieder;</li>
                            <li>Entwicklung neuer Programme und Konzeptionen im Breitensport;</li>
                            <li>Förderung des Leistungssports, insbesondere der Nachwuchsathleten sowie die sportwissenschaftliche Begleitung;</li>
                            <li>Unterstützung der internationalen Arbeit von Vereinen und Verbänden;</li>
                            <li>Durchführung von landesweiten Wettbewerben;</li>
                            <li>Ausbildung von Führungskräften in Vereinen und Verbänden;</li>
                            <li>Förderung des Frauensports;</li>
                            <li>Unterstützung der Jugendarbeit.</li>
                        </ul>


                        <h2>
                            Weitere Informationen
                        </h2>
                        <p>
                            Mehr über den Landessportbund und seine Aufgaben findet ihr im Internet unter <a href="http://www.lsb-rlp.de/" target="_blank">www.lsb-rlp.de</a>
                        </p>

                    </div>
                    @include ('vote.partials.btn-group')



                </section>
            </div>
        </div>
    </div>

@endsection
