@extends('layouts.master')

@section('content')


    <header class="vote-sponsor-header--rpr1 header-blank">
        <div class="container">
            <div class="row pos-rel">
                <img class="vote-sponsor-logo-detail" src="/default_images/vote/sponsores/logo-detail/rpr1-logo-detail.png"
                     alt="RPR1.">
            </div>

            <div class="vote-detail-header__caption">
                <h1 class="vote-detail-header__headline">
                    RPR1.
                </h1>
                <span class="vote-detail-header__subline">
                    „WIR LIEBEN, WAS WIR TUN!“
                </span>
            </div>
        </div>
    </header>

    <div class="section section--vote-white section--vote-detail-bordered no-before">
        <div class="container">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <section class="news-detail__main">

                    <div class="news-detail__content">
                        <p>
                            <strong>„WIR LIEBEN, WAS WIR TUN!“</strong> – Das ist das Motto der RPR Unternehmensgruppe, die mit RPR1. Und bigFM die erfolgreichsten privaten Radiosender im Südwesten Deutschlands umfasst. Ob Event, zielgerichtetes Hörermarketing, Cross- und Multichannel-Kampagnen, digitale Beiträge oder innovatives Radio und Streaming – Wir erzeugen Emotionen, immer und überall.
                        </p>
                        <img src="/default_images/vote/sponsores/rpr1-content.jpg" alt="Fortuna - Turn- und
                        Sportgeräte">

                        <h2>
                            Begeisterung seit 30 Jahren
                        </h2>
                        <p>
                            Mit speziellen Aktionen wie dem „Feelgood Freitag“, bei dem wir uns jeden Freitag von 09 – 16 Uhr auf eine Zeitreise mit den besten Hits aus den 90ern begeben oder unserer aktuellen Glück-sucht-Bringer-Kampagne begeistern wir Menschen seit über 30 Jahren.
                        </p>

                        <h2>
                            Weitere Informationen
                        </h2>
                        <p>
                            Mehr über RPR1. und die aktuellen Kampagnen findet ihr im Internet unter <a
                                    href="http://www.rpr1.de/" target="_blank">www.rpr1.de</a>
                        </p>

                    </div>
                    @include ('vote.partials.btn-group')



                </section>
            </div>
        </div>
    </div>

@endsection
