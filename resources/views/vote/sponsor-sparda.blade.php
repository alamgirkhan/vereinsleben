@extends('layouts.master')

@section('content')


    <header class="vote-sponsor-header--sparda">

        <div class="container">
            <div class="row pos-rel">
                <img class="vote-sponsor-logo-detail" src="/default_images/vote/sponsores/logo-detail/sparda-logo-detail.png"
                     alt="Logo Landessportbund Rheinland-Pfalz">
            </div>
            <div class="vote-detail-header__caption">
                <h1 class="vote-detail-header__headline">
                    Sparda-Bank Südwest
                </h1>
                <span class="vote-detail-header__subline">
                    Unsere Philosophie: freundlich & fair
                </span>
            </div>
        </div>
    </header>

    <div class="section section--vote-white section--vote-detail-bordered">
        <div class="container">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <section class="news-detail__main">

                    <div class="news-detail__content">

                        <h2>Das Unternehmen</h2>
                        <img src="/default_images/vote/sponsores/sparda_content_bild.jpeg" alt="Sparda-Bank Südwest">

                        <p>
                            Die Zufriedenheit unserer Kunden ist die Basis unseres Geschäftserfolges. Denn für uns steht der Mensch wirklich im Mittelpunkt. Freundlich & fair bedeutet aber auch, dass wir Ihnen ein Preis-Leistungs-Verhältnis bieten, das auf dem deutschen Bankenmarkt nahezu einmalig ist.
                        </p>

                        <h2>
                            Die Bank mit den zufriedensten Kunden
                        </h2>
                        <p>
                            Die Sparda-Banken landeten bei der Kundenzufriedenheits-Studie „Kundenmonitor Deutschland“ auch im Jahr 2016 wieder im Bereich Filialbanken auf Platz 1. Und das zum 24. Mal in Folge.
                        </p>


                        <h2>
                            Das bessere Girokonto und die clevere Finanzierung

                        </h2>
                        <p>
                            Das clevere Konto speziell für die Bedürfnisse von Privatkunden. Sie entscheiden, ob Sie persönlich zu uns in die Filiale kommen, uns anrufen, das Online-Banking oder die mobilen Apps nutzen. Und wenn es um die Erfüllung Ihrer Träume geht, sprechen Sie einfach mit.
                            Vom Wohntraum bis zum Traumauto - die Sparda-Bank ist der Finanzierungsspezialist für
                            den Privatkunden. Wir sind für Sie da: Freundlich & fair!

                        </p>

                        <h2>
                            Wir kümmern uns nur um Privatkunden
                        </h2>
                        <p>
                            Unsere Geschichte hat uns geprägt. Gegründet als Bank nur für Eisenbahner, haben wir uns immer auf das Privatkundengeschäft beschränkt. Deshalb kennen wir Ihre Bedürfnisse ganz genau. Heute genauso wie vor über 100 Jahren.

                        </p>
                        <h2>
                            Warum kann die Sparda-Bank so etwas?
                        </h2>
                        <p>
                            Die Sparda-Bank ist eine Regional- und Spezialbank für Arbeitnehmer. Wir betreiben kein risikobelastetes Firmengeschäft und können mit einer auf Privatkunden zugeschnittenen Produktpalette hocheffizient arbeiten. Das bringt Ihnen Vorteile in Euro und Prozent! Wir sind eben die Bank für Privatkunden!

                        </p>
                        <h2>
                            Weitere Informationen
                        </h2>
                        <p>
                            Mehr über die Sparda-Bank und ihre Angebote findet ihr im Internet unter www.sparda-sw.de
                            <a href="http://www.sparda-sw.de/" target="_blank">www.sparda-sw.de</a>
                        </p>

                    </div>
                    @include ('vote.partials.btn-group')



                </section>
            </div>
        </div>
    </div>

@endsection
