<a href="{{ route('vote.toggle') }}"
   class="btn btn--default btn--full-width">Abstimmungsrunde {{ $vote->isCurrent() ? 'aktiv' : 'nicht aktiv' }}</a>
<a href="{{ route('vote.reset') }}"
   class="btn btn--red btn--full-width">Stimmen zurücksetzen</a>