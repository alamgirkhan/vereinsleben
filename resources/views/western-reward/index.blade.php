@extends('layouts.master')

@section('content')
  <div id="western-reward">
    <header class="western-reward-header">
      <div class="container">
        <div class="hero-slider__content hero-slider__content--centered" style="color:white;">
          <h1 class="vote-header__headline" style="text-align:center; color:white;">
            EXKLUSIVE BEST WESTERN
            REWARDS GOLD-MITGLIEDSCHAFT
          </h1>
        </div>
      </div>
    </header>
      <section class="section1">
        <div class="container">
          <h2 class="hl hl--large hl--underlined hl--inverted hl--centered hl--upper">
            DEINE VORTEILE
          </h2>
          <p class="text-block text-block--compressed text-block--thin">
            Mit weltweit über 4.500 Hotels in mehr als 100 Ländern ist unser Exklusiv-Partner Best Western Hotels & Resorts eine der größten Hotelmarken der Welt. Und diese Partnerschaft bringt natürlich auch Vorteile für alle registrierten vereinsleben.de-User. Das solltest Du Dir nicht entgehen lassen.
            <br><br>
            <strong>Deine Vorteile mit einer exklusiven Best Western Rewards Gold-Mitgliedschaft</strong>
          </p>
          <ul class="text-block text-block--compressed text-block--thin">
            <li>Exklusive Reservierungshotline</li>
            <li>Zugriff auf exklusive Best Western Rewards Mitgliedsraten mit bis zu 10% Rabatt</li>
            <li>Best Western Rewards Gold-Mitgliedschaft mit weiteren Vorteilen wie Zimmer-Upgrades auf
              Anfrage und Verfügbarkeit und 10% Bonuspunkte auf die regulär gesammelten Best
              Western Rewards Basispunkte</li>
          </ul>
        </div>
      </section>
      <section class="logos section2">
        <div class="wrap">
          <div class="logo">
            <img src="{{asset('default_images/western-reward/BWR_Member_Cards_Basic_FNL_2-Gold.png')}}" alt="">
          </div>
          <div class="logo">
            <img src="{{asset('default_images/western-reward/BW_REWARDS_Logo_CMYK.jpg')}}" alt="">
          </div>
        </div>
      </section>

    <div class="container">
      <section class="padding-top-30px section3">
        <h2 class="hl hl--large hl--underlined hl--inverted hl--centered hl--upper">
          SO FUNKTIONIERT ES
        </h2>
        <p class="text-block text-block--compressed text-block--thin">
          1. Klickt auf den Button und meldet Euch direkt an zu Best Western Rewards. <br>
          2. Deine neue Best Western Rewards Mitgliedsnummer sowie Zugriff auf die exklusiven
          Mitgliedsraten erhältst Du sofort. <br>
          3. Deinen Gold Status und die damit verbundenen Vorteile erhältst Du ca. 4 Wochen nach
          Anmeldung.
        </p>
      </section>
    </div>

    @if(Auth::check())
      <div class="container">
        <section class="padding-top-30px section4">
          <form id="memberGoldForm" method="POST" action="{{route('western-rewards.store')}}">
            {{ csrf_field() }}
            <h2 class="hl hl--large hl--underlined hl--inverted hl--centered hl--upper">
              NUR EIN KLICK ZUM GOLD-STATUS
            </h2>
            <p class="text-block text-block--compressed text-block--thin">
              Um bares Geld zu sparen und Dir weitere Vorteile zu sichern, musst Du einfach den Button klicken und Dich für eine Best Western Rewards Mitgliedschaft anmelden …
            </p>
            <button id="becomeMemberGold" class="border button button--center button--full-width button--light button--light-white">Klicken und Best Western Rewards Gold-Mitglied werden</button>
          </form>
        </section>
      </div>
      @else
        <div class="container padding-top-30px no-login">
          <div class="row">
            <h2 class="hl hl--large hl--underlined hl--inverted hl--centered hl--upper">
              NUR NOCH WENIGE KLICKS <br>
              BIS ZUM GOLD-STATUS
            </h2>
            <p class="text-block text-block--compressed text-block--thin">
              Der exklusive Vorteil der Gold-Mitgliedschaft bei unserem Partner gilt für alle registrierten vereinsleben.de-User. Also gleich
              <a href="#">mit Deinen Benutzerdaten anmelden</a> oder <a href="">ein neues Benutzerkonto anlegen</a> damit Du Dir den Gold-Status sichern kannst …
            </p>
          </div>
        </div>
        <div class="container" align="center">
          <div class="col-xs-3"></div>
          <div class="col-md-3">
            <a href="/login" class="button button--center button--full-width button--light button--light-white border">Anmelden</a>
          </div>
          <div class="col-md-3">
            <a href="/register" class="button button--center button--full-width button--light button--light-white border">Benutzerkonto
              anlegen</a>
          </div>
          <div class="col-xs-3"></div>
        </div>
      @endif
      <section class="section5">
        <div class="container">
          <div class="row">
            <div class="col-md-4">
              <div class="image">
                <img src="{{asset('/default_images/western-reward/95293_bgg_h_20160823-043339.jpg')}}" alt="">
              </div>
              <div class="content">
                <p>Die Marke Best Western steht weltweit für komfortable Hotels im Drei- bis Vier-Sterne-Bereich, die sich durch individuellen Charakter auszeichnen.</p>
              </div>
            </div>
            <div class="col-md-4">
              <div class="image">
                <img src="{{asset('/default_images/western-reward/95503_bgg_z_20170327-095159.jpg')}}" alt="">
              </div>
              <div class="content">
                <p>Vom Boutiquehotel in der Stadt über moderne Tagungshotels bis hin zum exklusiven Resorthotel ist für jede Nachfrage die passende Unterkunft dabei.</p>
              </div>
            </div>
            <div class="col-md-4">
              <div class="image">
                <img src="{{asset('/default_images/western-reward/95508_bgg_r_20180531-022116.jpg')}}" alt="">
                <div class="content">
                  <p>In mehr als 100 Ländern der Welt haben alle Best Western Hotels eines gemeinsam: individuellen Charme und herzlichen Service.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    <div class="container">
      <div class="section6">
        <h2 class="hl hl--large hl--underlined hl--inverted hl--centered hl--upper">
          BEST WESTERN REWARDS MITGLIEDERSERVICE
        </h2>
        <p class="text-block text-block--compressed text-block--thin">
          Du hast Fragen zu einer Punkte-Transaktion, Deinem Konto oder einer Prämie? Dann wende Dich doch einfach an unseren freundlichen Best Western Rewards Mitgliederservice! Wir sind montags bis freitags von 8:00 bis 20:00 Uhr und samstags von 9:00 bis 13:00 Uhr persönlich für Dich da:
          <br><br>
          Hotline: 0800 - 21 25 888 (Kostenfrei in Deutschland) <br>
          E-Mail: <a href="mailto:bwr.service@bestwestern.com">bwr.service@bestwestern.com</a>
        </p>
      </div>
    </div>
  </div>
  @push('scripts')
    <script>
      $(document).ready(function () {
       $('#becomeMemberGold').click(function(e){
         e.preventDefault();
         $.ajax({
           type: 'POST',
           url: $("form#memberGoldForm").attr("action"),
           data: $("form#memberGoldForm").serialize(),
           success: function(response) {
             /*
             if (JSON.parse(response).status === true) {
               window.open('https://www.bestwestern.de/bwr-registrierung.html?etcc_med=Partner&etcc_cmp=LM&etcc_par=vereinsleben&etcc_var=goldreg&etcc_plc=partnersite&iata=00171640&programcode=VEREIN19');
             }*/
           },
         });
         //causes Errors by Alex - Link doesnt work for him
         window.open('https://www.bestwestern.de/bwr-registrierung.html?etcc_med=Partner&etcc_cmp=LM&etcc_par=vereinsleben&etcc_var=goldreg&etcc_plc=partnersite&iata=00171640&programcode=VEREIN19');

       });
      })
    </script>
  @endpush
@endsection