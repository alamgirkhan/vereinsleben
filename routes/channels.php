<?php


use Illuminate\Support\Facades\Broadcast;
use Nahid\Talk\Conversations\Conversation;


Broadcast::routes();

/*
 * Authenticate the user's personal channel...
 */
Broadcast::channel('user.{user}', function($user, $userId){
    return (int)$user->id === (int)$userId;
});


Broadcast::channel('chat.{conversatioId}', function($user, $conversationId){
    $conversation = Conversation::where('id', $conversationId)->firstOrFail();
    return ($conversation->user_one == $user->id || $conversation->user_two == $user->id);
});
