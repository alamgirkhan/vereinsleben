<?php
	// SG
	/*
	|--------------------------------------------------------------------------
	| Application Routes
	|--------------------------------------------------------------------------
	|
	| Here is where you can register all of the routes for an application.
	| It's a breeze. Simply tell Laravel the URIs it should respond to
	| and give it the controller to call when that URI is requested.
	|
	*/
	
	use Vereinsleben\State;

# news
	Route::group(['prefix' => 'neuigkeiten'], function () {
		Route::get('/', function () {
			return abort(404);
		});
		Route::get('/related/category/{categoryId?}/news/{newsId?}', 'NewsController@related')->name('news.related');
		Route::get('/categoryItems/{categoryId}', 'NewsController@categoryItems')->name('news.category.items');
//    Route::get('/{categoryId}/{newsSlug?}', 'NewsController@show')->name('news.detail');
		Route::get('/{categorySlug}/{newsSlug}', 'NewsController@show')->name('news.detail');
		Route::get('/', 'NewsController@selectedNews')->name('news.category');
		Route::get('/regional', 'NewsController@regionalNews')->name('news.regional');
		Route::get('/newsItems', 'NewsController@newsItems')->name('news.items');
//    Route::get('/regionalnewsItems', 'NewsController@regionalItems')->name('news.regionalitems');
	});

	// spitzensport routes
	Route::group(['prefix' => 'spitzensport'], function () {
		Route::get('/{categorySlug}/{spitzensportSlug}', 'SpitzensportController@show')->name('spitzensport.detail');
	});
	

# partner
	Route::group(['prefix' => 'partner'], function () {
		Route::get('/', 'PartnerController@partnerIndex')->name('partner.list');
		Route::get('/partnerList', 'PartnerController@partnerList')->name('partner.single');
		Route::get('/{partnerSlug?}', 'PartnerController@show')->name('partner.detail');
	});
	
	Route::group(['prefix' => 'service'], function () {
//    Route::get('/stellenboerse', function(){
//        return view('news.job-exchange');
//    });
		
		Route::get('/stellenboerse', 'ContentController@stellenBoerse');
	});


# verband
	Route::group(['prefix' => 'verband'], function () {
		Route::get('subscribers-list', 'BandageController@subscribersList')->name('verband.subscribers.list');
		Route::get('vereinen-list', 'BandageController@vereinenList')->name('verband.vereinen.list');
	});

# bandage detail, edit
	Route::group(['prefix' => 'verband/{slug}'], function () {
		Route::get('/', [
			'as' => 'bandage.detail',
			'uses' => 'BandageController@show'
		]);
		Route::post('/update-subscriber-status', [
			'as' => 'bandage.update.subscribe',
			'uses' => 'BandageController@updateSubscribe'
		]);
		Route::post('/add-contact-person', 'BandageController@addContactPerson')->name('bandage.add.contact');
		Route::delete('/delete/{id}', 'BandageController@removeContactPerson')->name('bandage.delete.contact');
	});

//bandage post
	Route::group(['prefix' => 'verband/post'], function () {
		Route::get('/{id}/template', 'BandagePostController@showRendered')->name('bandage.post.template');
		Route::post('/', 'BandagePostController@store')->name('bandage.post.store');
		Route::get('/{id}/edit', 'BandagePostController@edit')->name('bandage.post.edit');
		Route::post('/{id}', 'BandagePostController@update')->name('bandage.post.update');
		Route::delete('/delete/{id}', 'BandagePostController@delete')->name('bandage.post.delete');
		
	});

//bandage event
	Route::group(['prefix' => 'verband/event'], function () {
		Route::get('/{id}/template', 'BandageEventController@showRendered')->name('bandage.event.template');
		Route::post('/', 'BandageEventController@store')->name('bandage.event.store');
		Route::get('/{id}/edit', 'BandageEventController@edit')->name('bandage.event.edit');
		//Route::post('/{id}', 'BandageEventController@update')->name('bandage.event.update');
		Route::patch('/update/{id}', 'EventController@update')->name('bandage.event.update');
		Route::delete('/delete/{id}', 'BandageEventController@delete')->name('bandage.event.delete');
		
	});

# club detail, edit
	Route::group(['prefix' => '/vereine/{slug}'], function () {
		Route::get('/', [
			'as' => 'club.detail',
			'uses' => 'ClubController@show'
		]);
		Route::patch('updatePublished', [
			'as' => 'club.updatePublished',
			'uses' => 'ClubController@updatePublished'
		]);
		Route::patch('/', [
			'as' => 'club.update',
			'uses' => 'ClubController@updatePatch'
		]);
		Route::patch('/friendship_create/{recipientClub}', [
			'as' => 'club.profile.friendship',
			'uses' => 'ClubController@createFriendship'
		]);
		Route::patch('/friendship_edit/{friend}', [
			'as' => 'club.profile.edit_friendship',
			'uses' => 'ClubController@editFriendship'
		]);
		
		Route::post('/request/admin', [
			'as' => 'club.admin.request',
			'uses' => 'ClubController@requestAdmin'
		]);
		
		Route::patch('/color/update', [
			'as' => 'club.color.update',
			'uses' => 'ClubController@updateColor'
		]);
		
		Route::get('/color/reset', [
			'as' => 'club.color.reset',
			'uses' => 'ClubController@resetColor'
		]);
		
		# club achievements
		Route::group(['prefix' => 'achievement'], function () {
			Route::post('/', 'ClubAchievementController@store')->name('club.achievement.store');
			Route::get('/edit/{id}', 'ClubAchievementController@edit')->name('club.achievement.edit');
			Route::get('/latest', 'ClubAchievementController@latest')->name('club.achievement.latest');
			Route::patch('/{id}', 'ClubAchievementController@update')->name('club.achievement.update');
			Route::delete('/{id}', 'ClubAchievementController@delete')->name('club.achievement.delete');
		});
		
		# departments
		Route::group(['prefix' => 'department'], function () {
			Route::post('/', 'DepartmentController@store')->name('department.store');
			Route::get('/edit/{id}', 'DepartmentController@edit')->name('department.edit');
			Route::patch('/update/{id}', 'DepartmentController@update')->name('department.update');
			Route::delete('/delete/{id}', 'DepartmentController@delete')->name('department.delete');
			Route::get('/template/{id}', 'DepartmentController@showRendered')->name('department.template');
			//10-07-2018: edit phone and mobile for department contact user
			Route::post('/contact/edit', 'DepartmentController@editUser')->name('department.user.edit');
		});
		
		# posts
		Route::group(['prefix' => 'post'], function () {
			Route::get('/latest', 'ClubPostController@latest')->name('post.latest');
			Route::get('/single/{id}', 'ClubPostController@single')->name('post.single');
		});
		
		# events
		Route::group(['prefix' => 'event'], function () {
			Route::get('/latest', 'EventController@latest')->name('event.latest');
			Route::get('/single/{id}', 'EventController@single')->name('event.single');
		});
		
		# departments
		Route::group(['prefix' => 'department'], function () {
			Route::get('/latest/{id}', 'DepartmentController@latest')->name('department.latest');
			Route::get('/single/{id}', 'DepartmentController@single')->name('department.single');
		});
		
		# team
		Route::group(['prefix' => 'team'], function () {
			Route::get('/latest', 'TeamController@latest')->name('team.latest');
			Route::get('/single/{id}', 'TeamController@single')->name('team.single');
		});
		
		# post / event overview
		Route::get('/event-post-overview', 'ClubController@eventPostOverview')->name('event.post.overview');
		Route::get('/network-post-overview', 'ClubController@networkPostOverview')->name('network.post.overview');
		
		# become fan or member or un-fanize/un-memberize
		Route::post('/update-fan-status', 'ClubController@updateFanStatus')->name('club.fan.update');
		Route::post('/update-member-status', 'ClubController@updateMemberStatus')->name('club.member.update');
		Route::post('/update-member-relation', 'ClubController@updateMember')->name('club.member.update.relation');
		
		# contact person
		Route::post('/add-contact-person', 'ClubController@addContactPerson')->name('club.add.contact');
		Route::delete('/delete/{id}', 'ClubController@removeContactPerson')->name('club.delete.contact');
		
	});

# club creation
	Route::get('/verein-anlegen', [
		'as' => 'club.create',
		'uses' => 'ClubController@create'
	]);
	Route::post('/vereine', [
		'as' => 'club.store',
		'uses' => 'ClubController@store'
	]);
	Route::post('/admin/clubs/update', [
		'as' => 'club.updatePut',
		'uses' => 'ClubController@update'
	]);
	Route::get('/vereine', function () {
		return redirect()->route('club.search');
	});

# club
	Route::group(['prefix' => 'club'], function () {
		Route::get('latest', 'ClubController@latest')->name('club.latest');
		Route::get('user-list', 'ClubController@userList')->name('club.user.list');
		Route::get('friend-list', 'ClubController@friendList')->name('club.friend.list');
		Route::get('claim/{id}', 'ClubController@claim')->name('club.claim');
	});


# chat
	Route::get('/chat/{username}', 'ChatController@index')->name('chat')->middleware('auth');
	Route::group(['prefix' => 'chat/{username}'], function () {
//    Route::get('/', 'ChatController@index')->name('chat')->middleware('auth');
		Route::get('/messages', 'ChatController@messages')->name('chat.messages')->middleware('auth');
		Route::post('/store', 'ChatController@store')->name('chat.store')->middleware('auth');
	});
	
	
	Auth::routes();
# authentication, registration
	Route::auth();
	Route::get('/logout', 'Auth\LoginController@logout');
	Route::get('/register/verify/{token}', [
		'as' => 'register_verify',
		'uses' => 'Auth\AuthController@registerVerify'
	]);
	Route::get('login/facebook', 'Auth\LoginController@redirectToProvider');
	Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallback');
	Route::get('/change/federal_state/{stateiD}', 'UserController@changeFederalState')->name('change.federal_state');


# user, dashboard, profile
	
	Route::group(['prefix' => 'user'], function () {
		Route::get('dashboard', [
			'as' => 'user.dashboard',
			'uses' => 'UserController@dashboard',
			'middleware' => 'auth',
		]);
		
		Route::get('profile/edit', [
			'as' => 'user.profile.edit',
			'uses' => 'UserController@editProfile',
			'middleware' => 'auth',
		]);
		
		Route::post('profile/update', [
			'as' => 'user.profile.update',
			'uses' => 'UserController@updateProfile',
			'middleware' => 'auth',
		]);
		
		Route::get('profile/wizard', [
			'as' => 'user.profile.wizard',
			'uses' => 'UserController@profileWizard',
			'middleware' => 'auth',
		]);
		
		Route::post('profile/wizard/update', [
			'as' => 'user.wizard.profile.update',
			'uses' => 'UserController@updateWizardProfile',
			'middleware' => 'auth',
		]);
		
		Route::get('password/edit', [
			'as' => 'user.password.edit',
			'uses' => 'UserController@editPassword',
			'middleware' => 'auth',
		]);
		
		Route::post('password/update', [
			'as' => 'user.password.update',
			'uses' => 'UserController@updatePassword',
			'middleware' => 'auth',
		]);
		
		Route::post('email/update', [
			'as' => 'user.email.update',
			'uses' => 'UserController@updateEmail',
			'middleware' => 'auth',
		]);
		
		Route::get('/email/verify/{token}', [
			'as' => 'email.verify',
			'uses' => 'UserController@verifyEmail',
		]);
		
		Route::get('club/wizard', [
			'as' => 'user.club.wizard',
			'uses' => 'UserController@clubWizard',
		]);

    Route::get('club/wizard/teambesprechungsraum', [
        'as'   => 'user.club.contest.wizard',
        'uses' => 'UserController@clubWizardContest',
    ]);
		
		Route::post('/club/wizard/store', [
			'as' => 'user.wizard.club.store',
			'uses' => 'UserController@storeWizardclub',
		]);
		
		Route::get('latest', 'UserController@latest')->name('user.latest');
		Route::get('friend-list', 'UserController@friendList')->name('user.friend.list');
		Route::get('verbaende-list', 'UserController@bandageList')->name('user.bandage.list');
		Route::get('vereine-list', 'UserController@vereineList')->name('user.vereine.list');
	});

// API - prefix: /api
	$api = app('Dingo\Api\Routing\Router');
	$api->version('v1', function ($api) {
		$api->group(['namespace' => 'Vereinsleben\Http\Controllers\Api'], function ($api) {
			$api->get('cities', 'CityController@search')->name('api.cities');
			$api->get('sports', 'SportController@search')->name('api.sports');
			$api->group(['prefix' => 'users'], function ($user) use ($api) {
				$user->get('autocomplete', 'UserController@autocomplete')->name('api.user.autocomplete');
			});
			$api->group(['prefix' => 'clubs'], function ($club) use ($api) {
				$club->get('/', 'ClubController@index')->name('api.clubs');
				$club->get('search', 'ClubController@search');
				$club->get('autocomplete', 'ClubController@autocomplete')->name('api.club.autocomplete');
				$club->get('list', 'ClubController@clubList')->name('api.club.list');
			});
		});
	});

	Route::get('/verbaende/{bandage}', 'BandageController@show')->name('bandage.show');

	Route::group(['prefix' => 'admin', 'middleware' => 'contentManager'], function () {
		Route::get('/', 'AdminController@index')->name('admin.index');
		Route::get('/edit/{id}', 'AdminController@edit')->name('admin.news.edit');
		Route::patch('/update/{id}', 'AdminController@update')->name('admin.news.update');
		Route::get('/create', 'AdminController@create')->name('admin.news.create');
		Route::post('/store', 'AdminController@store')->name('admin.news.store');
		
		
		// news toggle-visibilty
		Route::patch('/news/toggle-visibility', [
			'uses' => 'AdminController@toggleVisibility',
			'as' => 'admin.news.toggle-visibility',
		]);
		
		Route::get('/pos/save', 'AdminController@posNewsSave')->name('admin.news.pos.save');
		Route::get('/pos', 'AdminController@posNews')->name('admin.news.pos');



		// administration
		Route::group(['middleware' => 'admin'], function () {
			Route::get('/campaigns', 'CampaignController@list')->name('admin.campaigns');
			Route::post('/campaign/create', 'CampaignController@store');
			Route::get('/campaign/edit/{id}', 'CampaignController@edit')->name('admin.campaign.edit');
			Route::get('/campaign/create', 'CampaignController@create')->name('admin.campaign.create');
			Route::put('/campaign/update/{id}', 'CampaignController@update');
			Route::get('/campaign/detroy/{id}', 'CampaignController@remove')->name('admin.campaign.detroy');
			Route::post('/updateBannerSpacingStream', 'CampaignController@updateBannerSpacingStream')->name('admin.bannerSpacingStream');
			
			
			Route::post('/upload-news-image', 'AdminController@uploadNewsImage');
			//30-05-2018: update position for news
			Route::post('/verbaende', 'BandageController@store')->name('bandage.store');
			Route::get('/verbaende', 'BandageController@index')->name('bandage.index');
			Route::get('/verbaende/create', 'BandageController@create')->name('bandage.create');
			Route::get('/verbaende/{bandage}', 'BandageController@adminShow')->name('bandage.admin.show');

			Route::patch('/verbaende/{bandage}', 'BandageController@update')->name('bandage.update');
			Route::get('/verbaende/deactivate/{id}', 'BandageController@deactivate')->name('admin.bandage.deactivate');
			Route::get('/admin/verbaende/addadmin', 'BandageController@addAdmin')->name('admin.bandage.admin.add');
			Route::get('/verbaende/deladmin/{bandage_id}/{id}', 'BandageController@deleteAdmin')->name('admin.bandage.admin.delete');
			Route::get('clubs', 'AdminController@adminClubs')->name('admin.clubs');
			Route::get('clubs/vita', 'AdminController@adminClubsVita')->name('admin.clubs.vita');
			Route::post('club/request/accept', 'AdminController@adminRequestAccept')->name('admin.club.request.accept');
			Route::post('club/request/decline', 'AdminController@adminRequestDecline')->name('admin.club.request.decline');
			Route::post('clubs/vita/publish', 'AdminController@adminSuggestedPublish')->name('admin.club.vita.publish');
			Route::post('clubs/vita/remove', 'AdminController@adminSuggestedRemove')->name('admin.club.vita.remove');
			
			Route::post('/clubs/exportclubdata', [
				'as' => 'admin.clubs.exportdata',
				'uses' => 'AdminClubController@exportClubsData'
			]);
			
			Route::get('/clubs/download', 'AdminClubController@downloadClubFile')->name('admin.club.download');
			
			Route::get('/users', 'AdminUserController@index')->name('admin.users');
			Route::get('/user/edit/{id}', 'AdminUserController@edit')->name('admin.user.edit');
			Route::patch('/user/update/{id}', 'AdminUserController@update')->name('admin.user.update');
			Route::post('/user/destroy', 'AdminUserController@destroy')->name('admin.user.destroy');
			
			Route::get('/users/deletereq', 'AdminUserController@showDelReq')->name('admin.users.deletereq');
			Route::get('/user/delete/accept/{id}', 'AdminUserController@hardDeleteUser')->name('admin.user.delete.accept');
			Route::get('/user/delete/deny/{id}', 'AdminUserController@denyDeleteUser')->name('admin.user.delete.deny');
			//08-10-2018: add club to user as admin
			Route::get('/user/addclub', 'AdminUserController@addClub')->name('admin.user.club.add');
			//08-10-2018: delete relation as clubAdmin from User
			Route::get('/user/delclub/{userid}/{id}', 'AdminUserController@deleteClub')->name('admin.user.club.delete');
			
			//11-06-2018: export data from datatable
			Route::post('/users/exportdata', [
				'as' => 'admin.users.exportdata',
				'uses' => 'AdminUserController@exportUsersData'
			]);
			
			//31-07-2018: events
			Route::group(['prefix' => 'events'], function () {
				Route::get('all', 'AdminController@getEventsList')->name('admin.event.list');
				Route::get('edit/{id}', 'AdminController@editEvent')->name('admin.event.edit');
				Route::patch('update/{id}', 'AdminController@updateEvent')->name('admin.event.update');
				Route::post('store', 'AdminController@storeEvent')->name('admin.event.store');

				//        Route::patch('{id}/delete', 'SportController@delete')->name('admin.sport.delete');
			});
			
			Route::get('/users/download', 'AdminUserController@downloadUserFile')->name('admin.user.download');
			Route::get('fakedomains', 'AdminController@getFakeDomains')->name('admin.get.fakedomains');
			Route::get('fakedomains/add', 'AdminController@addFakeDomain')->name('admin.fakedomains.add');
			Route::get('sportart/add', 'AdminController@addSportArt')->name('admin.sportart.add');
			Route::get('fakedomains/delete/{domain}', 'AdminController@deleteFakeDomain')->name('admin.fakedomains.destroy');
			Route::get('fakeusers', 'AdminController@getFakeUsers')->name('admin.get.fakeusers');
			Route::get('fakeusers/add', 'AdminController@addFakeUser')->name('admin.fakeusers.add');
			Route::get('fakeusers/delete/{user}', 'AdminController@deleteFakeUser')->name('admin.fakeusers.destroy');
			
			Route::get('contest', 'AdminController@getContest')->name('admin.get.contest');
			Route::get('contest/delete/{id}', 'AdminController@deleteContest')->name('admin.contest.destroy');
			
			
			Route::get('/clubs/list', 'AdminClubController@clubList')->name('admin.clubs.list');
			Route::patch('/clubs/destroy', 'AdminClubController@destroy')->name('admin.club.destroy');
			//22-05-2018: club edit as in new spec
			Route::get('/clubs/edit/{slug}', 'AdminClubController@edit')->name('admin.club.edit');
			//23-05-2018: deactivate function for a club
			Route::get('/clubs/deactivate/{id}', 'AdminClubController@deactivate')->name('admin.club.deactivate');
			//24-05-2018: reset function for club
			Route::get('/clubs/reset/{id}', 'AdminClubController@reset')->name('admin.club.reset');
			//25-05-2018: add admin to club
			Route::get('/clubs/addadmin', 'AdminClubController@addAdmin')->name('admin.club.admin.add');
			//28-05-2018: delete admin from club
			Route::get('/clubs/deladmin/{clubid}/{id}', 'AdminClubController@deleteAdmin')->name('admin.club.admin.delete');
			//29-05-2018: mapping function for club
			Route::get('/clubs/map', 'AdminClubController@mappy')->name('admin.club.map');
			
			Route::get('/bewertungen/list/{sort_by}', 'ReviewController@index')->name('admin.reviews.list');
			Route::get('/review/destroy/{id}', 'ReviewController@destroy')->name('admin.review.destroy');
			Route::get('/review/destroy/{id}', 'ReviewReportController@destroy')->name('admin.report.destroy');
			
			Route::patch('/news/destroy', [
				'uses' => 'AdminController@destroy',
				'as' => 'admin.news.destroy',
			]);
			
			Route::patch('/campaign/toggle-visibility', 'CampaignController@toggleVisibility');
			
			# sports
			Route::get('sports', 'AdminController@showSports')->name('admin.show.sports');
			//    Route::get('sports', function(){
			//        return view('admin.sport.index');
			//    });
			
			# clubs
			//    Route::get('clubs', function(){
			//        return view('admin.club.index');
			//    });
			
			# exports
			Route::get('exports', 'AdminController@showExports')->name('admin.show.exports');
			//    Route::get('exports', function(){
			//        return view('admin.export.index');
			//    });
			
			
			Route::group(['prefix' => 'club'], function () {
				# review club
				Route::patch('review', 'AdminController@reviewClub')->name('admin.club.review');
				
				# export
				Route::get('export', function () {
					dispatch(new Vereinsleben\Jobs\ClubExport);
					
					return redirect()->back()->with('flash-success', 'Der Export von Vereinen wurde angestoßen. Du wirst per E-Mail informiert, sobald dieser zum Download steht.');
				})->name('admin.club.export');
				
				# retrieve clubs by type (new-unowned, new-owned, ...)
				Route::get('{type}', 'AdminController@clubList')->name('admin.club.list');
				
				# this is an update call to flag the resource as deleted, so no DELETE method and /delete
				Route::patch('{id}/delete', 'ClubController@delete')->name('admin.club.delete');
				
				
				Route::post('/clubwizardcontest', 'ClubController@storeClubContest')->name('club.store.wizard.contest');
			});
			
			Route::group(['prefix' => 'user'], function () {
				# export
				Route::get('export', function () {
					dispatch(new Vereinsleben\Jobs\UserExport);
					
					return redirect()->back()->with('flash-success', 'Der Export von Benutzern wurde angestoßen. Du wirst per E-Mail informiert, sobald dieser zum Download steht.');
				})->name('admin.user.export');
			});
			
			Route::group(['prefix' => 'sport'], function () {
				Route::patch('{id}/review', 'AdminController@reviewSport')->name('admin.sport.review');
				Route::get('{type}', 'AdminController@sportList')->name('admin.sport.list');
				Route::patch('{id}/delete', 'SportController@delete')->name('admin.sport.delete');
			});
			
			Route::group(['namespace' => 'Admin'], function () {
				Route::resource('vote', 'VoteController');
				
				Route::get('vote/create', 'VoteController@create');
				Route::post('vote/store', 'VoteController@store')->name('admin.vote.store');
				Route::patch('vote/{vote_id}/update', 'VoteController@update')->name('admin.vote.update');
				
				
				Route::group(['prefix' => 'vote/{vote_id}'], function () {
				//            Route::resource('nominee', 'VoteNomineeController');
					
					Route::get('nominee/create', 'VoteNomineeController@create')->name('admin.vote.{vote_id}.nominee.create');
					Route::get('nominee/{id}/show', 'VoteNomineeController@show')->name('admin.vote.{vote_id}.nominee.show');
					//            Route::get('nominee/{id}/show', 'VoteNomineeController@show')->name('admin.vote.{vote_id}.nominee.{nominee_id}.show');
					Route::post('nominee/store', 'VoteNomineeController@store')->name('admin.vote.{vote_id}.nominee.store');
					Route::patch('nominee/{id}/update', 'VoteNomineeController@update')->name('admin.vote.{vote_id}.nominee.update');
					Route::get('nominee/{id}/delete', 'VoteNomineeController@delete')->name('admin.vote.{vote_id}.nominee.{nominee_id}.delete');
					Route::get('nominee/{fid}/winner', 'VoteNomineeController@setWinner')->name('admin.vote.{vote_id}.nominee.{nominee_id}.winner');
				});
				
				Route::get('checking/fake', 'CheckingController@fake');
				Route::get('checking/doublets', 'CheckingController@proofDoublet');
				Route::get('checking/nominee-add-votes/{vote_id}/{nominee_id}/votes/{votes}', 'CheckingController@addVotesToIdNominee')
					->name('admin.add.votes.{vote_id}.nominee.{nominee_id}.votes.{votes}');
				
				//Route::get('deletion/user/{value}', 'CheckingController@deleteUser');
				Route::get('deletion/user/{data}/club/{deleteRelatedClubs}', 'CheckingController@deleteUser');
				Route::get('deletion/fake', 'CheckingController@deleteFake');
				
				Route::get('verification/user/{email}', 'CheckingController@verifyUser');
			});
			
			Route::group(['prefix' => 'slider'], function(){
				Route::get('/', 'SliderController@index')->name('admin.slider.index');
				Route::get('/add', 'SliderController@addSlider')->name('admin.slider.add');
				Route::post('/store', 'SliderController@store')->name('admin.slider.store');
				Route::get('/edit/{id}', 'SliderController@edit')->name('admin.slider.edit');
				Route::patch('/update/{id}', 'SliderController@update')->name('admin.slider.update');
				
				Route::get('/destroy/{id}', [
					'uses' => 'SliderController@destroy',
					'as'   => 'admin.slider.destroy',
				]);
			});
			
			Route::group(['prefix' => 'content'], function(){
				Route::get('/', 'ContentController@index')->name('admin.content.index');
				Route::get('/add', 'ContentController@addContent')->name('admin.content.add');
				Route::post('/store', 'ContentController@store')->name('admin.content.store');
				Route::get('/edit/{id}', 'ContentController@edit')->name('admin.content.edit');
				Route::patch('/update/{id}', 'ContentController@update')->name('admin.content.update');
				
				Route::get('/destroy/{id}', [
					'uses' => 'ContentController@destroy',
					'as'   => 'admin.content.destroy',
				]);
			});
			
			Route::group(['prefix' => 'partner'], function(){
				Route::get('/', 'PartnerController@index')->name('admin.partner.index');
				Route::get('/add', 'PartnerController@add')->name('admin.partner.add');
				Route::post('/store', 'PartnerController@store')->name('admin.partner.store');
				Route::get('/edit/{id}', 'PartnerController@edit')->name('admin.partner.edit');
				Route::patch('/update/{id}', 'PartnerController@update')->name('admin.partner.update');
				Route::get('/destroy/{id}', [
					'uses' => 'PartnerController@destroy',
					'as'   => 'admin.partner.destroy',
				]);
			});
			
			//new admin dashboard
			/**
			 * Route::group(['prefix' => 'dashboard'], function(){
			 * Route::get('/', function(){
			 * return view('admin.dashboard.dashboard');
			 * });
			 */
			Route::get('/dashboard', function(){
				return view('admin.dashboard.dashboard');
			});
			Route::get('/dashboard/user/index', 'AdminUserController@userStatistics')->name('admin.dashboard.user');
			Route::get('/dashboard/club/clubs', 'AdminUserController@clubStatistics')->name('admin.dashboard.club');
			//});
			
			Route::get('/propose-songs', 'ProposeSongController@list')->name('admin.propose-song.list');
			Route::get('propose-song/destroy/{id}', 'ProposeSongController@destroy')->name('admin.propose-song.destroy');
			Route::get('propose-song/edit/{id}', 'ProposeSongController@edit')->name('admin.propose-song.edit');
			Route::get('propose-song/view/{id}', 'ProposeSongController@view')->name('admin.propose-song.view');
			Route::POST('propose-song/update', 'ProposeSongController@update')->name('admin.propose-song.update');
			
			// number of week
			Route::resource('number-of-week', 'NumberOfWeekController');

			// Spitzensport
			Route::patch('/spitzensports/destroy', [
				'uses' => 'SpitzensportController@destroy',
				'as' => 'admin.spitzensport.destroy',
			]);
			Route::patch('/spitzensport/toggle-visibility', [
				'uses' => 'SpitzensportController@toggleVisibility',
				'as' => 'admin.spitzensport.toggle-visibility',
			]);
			Route::resource('spitzensports', 'SpitzensportController');
			// Route::get('/edit/{id}', 'AdminController@edit')->name('admin.news.edit');
			// Route::patch('/update/{id}', 'AdminController@update')->name('admin.news.update');

			//best western rewards
			Route::get('/best-western-rewards', 'WesternRewardController@list')->name('admin.western-rewards.list');

		});
	});
	
Route::get('/verbaende/{slug}', [
    'as'         => 'bandage.detail',
    'uses'       => 'BandageController@show',
    'middleware' => 'auth'
]);
	
	Route::patch('/verbaende/{slug}/color/update', 'BandageController@updateColor')->name('bandage.color.update');

	Route::get('/admin/user/{username}', [
		'as' => 'admin.user.detail',
		'uses' => 'AdminUserController@show',
		'middleware' => 'admin'
	]);
	
	Route::patch('/admin/user/{username}/updatefederalstate', [
		'as' => 'admin.user.update.state',
		'uses' => 'AdminUserController@updateState',
		'middleware' => 'admin'
	]);
	
	Route::patch('/verbaende/{id}', [
		'as' => 'bandage.update.public',
		'uses' => 'BandageController@updateAtPublic',
		'middleware' => 'auth'
	]);
	
	Route::delete('/verbaende/{slug}', [
		'as' => 'bandage.delete',
		'uses' => 'BandageController@destroy',
		'middleware' => 'auth'
	]);
//Route::get('/latest', 'BandagePostController@latest')->name('bandage.post.latest');
//Route::get('/single/{id}', 'ClubPostController@single')->name('post.single');
	Route::get('/verbaende/{slug}/post/latest', [
		'as' => 'bandage.post.latest',
		'uses' => 'BandagePostController@latest',
		'middelware' => 'auth'
	]);
	
	Route::get('/verbaende/{slug}/post/single/{id}', [
		'as' => 'bandage.post.single',
		'uses' => 'BandagePostController@single',
		'middelware' => 'auth'
	]);


// posts
	Route::group(['prefix' => 'post'], function () {
		Route::post('/', 'ClubPostController@store')->name('post.store');
		Route::get('/edit/{id}', 'ClubPostController@edit')->name('post.edit');
		Route::patch('/update/{id}', 'ClubPostController@update')->name('post.update');
		Route::delete('/delete/{id}', 'ClubPostController@delete')->name('post.delete');
	});

// events
	Route::get('/verbaende/{slug}/event/latest', [
		'as' => 'bandage.event.latest',
		'uses' => 'BandageEventController@latest',
		'middelware' => 'auth'
	]);
	
	Route::get('/verbaende/{slug}/event/single/{id}', [
		'as' => 'bandage.event.single',
		'uses' => 'BandageEventController@single',
		'middelware' => 'auth'
	]);
	
	Route::group(['prefix' => 'event'], function () {
		Route::post('/', 'EventController@store')->name('event.store');
		Route::get('/edit/{id}', 'EventController@edit')->name('event.edit');
		Route::patch('/update/{id}', 'EventController@update')->name('event.update');
		Route::delete('/delete/{id}', 'EventController@delete')->name('event.delete');
		Route::get('/all', 'EventController@eventList')->name('events.all');
		Route::get('/detail/{id}', 'EventController@detail')->name('event.detail');
	});


# Team
	Route::group(['prefix' => 'team'], function () {
		Route::get('/add/{id}', 'TeamController@add')->name('team.add');
		Route::post('/', 'TeamController@store')->name('team.store');
		Route::get('/edit/{id}', 'TeamController@edit')->name('team.edit');
		Route::patch('/update/{id}', 'TeamController@update')->name('team.update');
		Route::delete('/delete/{id}', 'TeamController@delete')->name('team.delete');
	});
	
	Route::group(['prefix' => 'training_ort'], function () {
		Route::post('/', 'TrainingOrtController@store')->name('training_ort.store');
		Route::get('/edit/{id}', 'TrainingOrtController@edit')->name('training_ort.edit');
		//Route::patch('/update/{id}', 'TrainingOrtController@update')->name('training_ort.update');
		Route::delete('/delete/{slug}/{id}', 'TrainingOrtController@delete')->name('training_ort.delete');
	});
	
	Route::get('/vereine/{slug}/training_ort/single/{id}', [
		'as' => 'training_ort.single',
		'uses' => 'TrainingOrtController@single',
		'middelware' => 'auth'
	]);


// static / content pages
	Route::group([], function () {
		Route::get('/', 'IndexController@index')->name('home');

//    Route::get('/kontakt', function(){
//        return view('sites.contact');
//    });
		
		Route::get('/kontakt', 'ContentController@kontakt');
		
		Route::get('/facebook_login', function () {
			return view('sites.facebook');
		});
		
		Route::post('/contact', 'ContentController@contactUS')->name('contact');
		
		Route::get('/content/{slug}', 'ContentController@view');
	});

# club voting
	Route::get('verein-des-monats', 'VoteController@overview')->name('vote.index');
	
	Route::get('verein-des-monats/teilnehmen', 'VoteController@showTeilnehmen')->name('vote.show.teilnehmen');
//Route::get('verein-des-monats/teilnehmen', function(){
//    return view('vote.participate');
//});
	
	Route::get('verein-des-monats/teilnahme', function () {
		//return view('vote.participate-general', ['federal_states' => State::all()]);
		return view('vote.participate');
	});
	
	Route::post('verein-des-monats/bewerben', 'VoteController@bewerben')->name('vote.bewerben');
	
	Route::get('verein-des-monats/teilnahmebedingungen', 'VoteController@showTeilnahmebedingungen')->name('vote.show.teilnahmebedinungen');
//Route::get('verein-des-monats/teilnahmebedingungen', function(){
//    return view('vote.agreement');
//});
	
	Route::get('verein-des-monats/ueber-den-wettbewerb', 'VoteController@showUeberDenWettbewerb')->name('vote.show.ueberdenwettbewerb');
//Route::get('verein-des-monats/ueber-den-wettbewerb', function(){
//    return view('vote.about');
//});
	
	Route::get('verein-des-monats/sponsoren/fortuna-sportgeraete', 'VoteController@showSponsorFortuna')->name('vote.show.fortuna');
//Route::get('verein-des-monats/sponsoren/fortuna-sportgeraete', function(){
//    return view('vote.sponsor-fortuna');
//});
	
	Route::get('verein-des-monats/sponsoren/landessportbund-rheinland-pfalz', 'VoteController@showSponsorLSB')->name('vote.show.lsb');
//Route::get('verein-des-monats/sponsoren/landessportbund-rheinland-pfalz', function(){
//    return view('vote.sponsor-lsb');
//});
	
	Route::get('verein-des-monats/sponsoren/sparda-bank', 'VoteController@showSponsorSparda')->name('vote.show.sparda');
//Route::get('verein-des-monats/sponsoren/sparda-bank', function(){
//    return view('vote.sponsor-sparda');
//});
	
	Route::get('verein-des-monats/sponsoren/rpr1', 'VoteController@showSponsorRPR1')->name('vote.show.rpr1');
//Route::get('verein-des-monats/sponsoren/rpr1', function(){
//    return view('vote.sponsor-rpr1');
//});
	
	Route::get('verein-des-monats/sponsoren/bigfm', 'VoteController@showSponsorBigfm')->name('vote.show.bigfm');
	
	Route::get('verein-des-monats/sponsoren/abc-werbeagentur', 'VoteController@showSponsorABC')->name('vote.show.abc');
	
	Route::post('verein-des-monats/teilnehmen', 'VoteController@participate')->name('club.vote.participate');
	
	
	Route::get('verein-des-monats/{clubSlug}', 'VoteController@nominee')->name('club.vote.nominee');
	
	Route::group(['prefix' => 'vote'], function () {
		Route::post('{voteId}/nominee/{nomineeId}', 'VoteController@voteFor')->name('vote.for.nominee');
		Route::post('/remember', 'VoteController@remember')->name('vote.remember');
		Route::post('/notremember', 'VoteController@notRemember')->name('vote.not.remember');
		Route::get('/notremember/{token}', 'VoteController@notRememberAnymore')->name('vote.not.remember.more');
		
	});


# redirect deprecated urls
	Route::get('/news-{slug}', function ($slug) {
		$news = \Vereinsleben\News::where('slug', $slug)->firstOrFail();
		
		//03-08-2018: changes regarding new news layout
//    return redirect()->route('news.detail', [$news->category->slug, $news->slug], 301);
		return redirect()->route('news.detail', [$news->category_id, $news->slug], 301);
	});
	
	Route::get('vereine-finden', function () {
		return redirect()->route('club.search');
	});
	
	Route::get('/verein-{slug}', function ($slug) {
		$club = \Vereinsleben\Club::where('slug', $slug)->firstOrFail();
		
		return redirect()->route('club.detail', $club->slug, 301);
	});

// XML sitemap
	Route::get('sitemap.xml', function () {
		
		$sitemap = App::make("sitemap");
		
		// check if there is cached sitemap and build new only if is not
		if (!$sitemap->isCached()) {
			
			// add item to the sitemap (url, date, priority, freq)
			$sitemap->add(URL::to('/'), date('c', time()), '1.0', 'daily');
			$sitemap->add(URL::to('/kontakt'), date('c', time()), '0.5', 'monthly');
			$sitemap->add(URL::to('/faq'), date('c', time()), '0.5', 'monthly');
			$sitemap->add(URL::to('/agbs'), date('c', time()), '0.5', 'monthly');
			$sitemap->add(URL::to('/impressum'), date('c', time()), '0.5', 'monthly');
			
			$sitemap->add(URL::to('/verein-finden'), date('c', time()), '1.0', 'weekly');
			
			$categories = \Vereinsleben\NewsCategory::unarchived()->withAnyParent()->get();
			foreach ($categories as $category) {
				$sitemap->add(URL::route('news.category', $category->slug), date('c', time()), '1.0', 'daily');
			}
			
			$newses = \Vereinsleben\News::published()->public()->get();
			foreach ($newses as $news) {
				//03-08-2018: changes regarding new news layout
//            $sitemap->add(URL::route('news.detail', [($news->category !== null ? $news->category->slug : null), $news->slug]), date('c', is_null($news->updated_at) ? strtotime($news->created_at) : strtotime($news->updated_at)), '1.0', 'weekly');
				$sitemap->add(URL::route('news.detail', [($news->category !== null ? $news->category_id : null), $news->slug]), date('c', is_null($news->updated_at) ? strtotime($news->created_at) : strtotime($news->updated_at)), '1.0', 'weekly');
			}
			
			$clubs = \Vereinsleben\Club::published()->get();
			foreach ($clubs as $club) {
				$sitemap->add(URL::route('club.detail', $club->slug), date('c', is_null($club->updated_at) ? strtotime($club->created_at) : strtotime($club->updated_at)), '1.0', 'daily');
			}
			
		}
		
		// show your sitemap (options: 'xml' (default), 'html', 'txt', 'ror-rss', 'ror-rdf')
		return $sitemap->render('xml');
		
	});

Route::get('/teambesprechungsraum', 'ContestController@index')->name('contest.index');
//
Route::get('/teambesprechungsraum/request', 'ContestController@contestReq')->name('contest.request');
//
Route::get('/teambesprechungsraum/invite', 'ContestController@sendInvitation')->name('contest.invite');
//
//Route::get('/trainerhero', 'HeroController@index')->name('hero.index');
	
	
	Route::get('/benutzer/delete/{token}', 'UserController@deletion')->name('user.profile.delete.req');
	
	Route::get('/vereinshit', 'ProposeSongController@index');
	Route::post('/vereinshit/request', 'ProposeSongController@store')->name('propose-song.store');
	
	Route::get('/best-western-rewards', 'WesternRewardController@index');
	Route::post('/best-western-rewards/request', 'WesternRewardController@store')->name('western-rewards.store');

	//stand alone Pages
    Route::get('/podcast', 'PagesController@podcast');


# user profile
	Route::group(['prefix' => '/benutzer'], function () {
		Route::get('/{username}', 'UserController@show')->name('user.detail');
		Route::patch('/{username}/update', 'UserController@updatePatch')->name('user.profile.patch');
		Route::get('/{username}/friends', 'UserController@showFriends')->name('user.profile.friends');
		Route::get('/{username}/get_friend_requests', 'UserController@getFriendRequests')->name('user.profile.friends.pending');
		Route::get('/{username}/search_friends', 'UserController@searchFriends')->name('user.profile.search.friends');
		Route::get('/{senderUser}/friendship_create/{recipientUser}', 'UserController@createFriendship')->name('user.profile.friendship');
		Route::get('/{user}/friendship_edit/{friend}', 'UserController@editFriendship')->name('user.profile.edit_friendship');
		Route::patch('/{username}/social/update', 'UserController@updateSocial')->name('user.social.update');
		Route::get('/contact/{id}', 'UserController@getContact')->name('user.contact');
		
		//skhan 20.03.2018: User kann sein eigenes Konto löschen
		Route::get('/{username}/delete', 'UserController@ReqDelete')->name('user.profile.delete');
		//skhan 21.03.2018: Konto löschen verifizieren
		Route::get('/delete/{token}', 'UserController@deletion')->name('user.profile.delete.req');
		//skhan 23.03.2018: call modal
		Route::get('/{username}/delete/request', function ($username) {
			return view('user.profile.modal.account-delete');
		})->name('account-delete-request');
		
		
		// user posts
		Route::group(['prefix' => '/{username?}/post'], function () {
			Route::get('/{id}/template', 'UserPostController@showRendered')->name('user.post.template');
			Route::post('/', 'UserPostController@store')->name('user.post.store');
			Route::get('/{id}/edit', 'UserPostController@edit')->name('user.post.edit');
			Route::post('/{id}', 'UserPostController@update')->name('user.post.update');
			Route::delete('/{id}', 'UserPostController@destroy')->name('user.post.destroy');
		});
		
		// @todo see routes above and change them to be rest-like
		Route::post('post', 'UserPostController@store')->name('user.post.save');
		
		Route::get('/{user}/profile/modal', 'UserController@profileInfoModal')->name('user.profile.modal');
	});

# user sport
	Route::group(['prefix' => '/user-sport'], function () {
		Route::post('/', 'UserSportController@store')->name('user.sport.store');
		Route::get('/{userId}', 'UserSportController@show')->name('user.sport');
		Route::get('/edit/{userId}', 'UserSportController@edit')->name('user.sport.edit');
		Route::get('/template/{userId}', 'UserSportController@showRendered')->name('user.sport.template');
		Route::patch('/delete', 'UserSportController@delete')->name('user.sport.delete');
	});

# user sport career
	Route::group(['prefix' => '/sport-career'], function () {
		Route::post('/', 'SportCareerController@store')->name('sport.career.store');
		Route::post('/clubwizard', 'SportCareerController@storeWizard')->name('sport.career.club.wizard');
		Route::get('/edit/{id}', 'SportCareerController@edit')->name('sport.career.edit');
		Route::post('/{id}', 'SportCareerController@update')->name('sport.career.update');
		Route::delete('/{id}', 'SportCareerController@destroy')->name('sport.career.destroy');
		Route::get('/template/{id}', 'SportCareerController@showRendered')->name('sport.career.template');
	});

# dynamic view rendering
	Route::get('render-view/{viewName}', function ($viewName) {
		return view($viewName);
	});
	
	use Vereinsleben\Club;

# dynamic view rendering
	Route::get('team-member/{slug}', function ($slug) {
		$club = Club::where('slug', $slug)->firstOrFail();
		
		return view('club.partials.modal.team-member', ['club' => $club]);
	})->name('team-member');

# search
	Route::group(['prefix' => 'suche'], function () {
		Route::get('/', 'SearchController@search')->name('search');
//		Route::get('/vereine', 'SearchController@clubSearch')->name('club.search');
		Route::get('/vereine', 'SearchController@clubSearchNew')->name('club.search');
		Route::get('/benutzer', 'SearchController@userSearch')->name('user.search');
		Route::get('/verbaende', 'SearchController@bandageSearch')->name('bandage.search');
	});

# user stream
// it's me, mario! ...returns the current user :)
	Route::get('benutzer/user/me', 'UserController@me')->name('user.me');
// index
	Route::get('user/stream', 'User\StreamController@index')->name('user.stream');
// events
	Route::get('user/events', 'User\StreamController@events')->name('user.stream.events');
// clubs
	Route::get('user/clubs', 'User\StreamController@clubs')->name('user.stream.clubs');
// users
	Route::get('user/users', 'User\StreamController@users')->name('user.stream.users');
// streaming
Route::get('user/streaming', 'User\StreamController@streaming')->name('user.profile.stream');

# landing page test
Route::get('/{sport}/{city}', 'LandingPageController@index');

# table excels db
Route::get('/keyword-table', 'LandingPageController@keyWordTable');

// random campaign
	Route::get('random-campaign', 'RandomCampaignController@index');
	
// reviews
	Route::resource('reviews', 'ReviewController');
	Route::post('reviews/ratingNoLogin', 'ReviewController@ratingNoLogin')->name('review.ratingNoLogin');
	Route::resource('review-reports', 'ReviewReportController');
	
// contact person
	Route::resource('contact-person', 'ContactPersonController');
	Route::get('/verbaende/{slug}/contact-person/list', 'ContactPersonController@list')->name('contact-person.list');
	Route::get('/verbaende/{slug}/contact-person/edit/{id}', 'ContactPersonController@edit')->name('contact-person.edit');
	
	Route::get('/verbaende/{slug}/contact-person/single/{id}', [
		'as' => 'bandage.contact-person.single',
		'uses' => 'ContactPersonController@single',
		'middelware' => 'auth'
	]);