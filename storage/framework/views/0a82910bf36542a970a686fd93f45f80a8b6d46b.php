

















    
    
    


















































































<adminnav class="admin__menu">
    <ul>
        <li><a><strong>Dashboard</strong></a>
            <ul>
                <li><a href="<?php echo e(url('admin/dashboard/user/index')); ?>">Userstatistik</a></li>
                <li><a href="<?php echo e(url('admin/dashboard/club/clubs')); ?>">Vereinsstatistik</a></li>
            </ul>
        </li>
        <li><a><strong>Daten</strong></a>
            <ul>
                <li><a href="<?php echo e(url('admin/users')); ?>">User</a></li>
                <li><a href="<?php echo e(url('admin/clubs/list')); ?>">Vereine</a></li>
                <li><a href="<?php echo e(url('admin/verbaende')); ?>">Verbände</a></li>
                <li><a href="<?php echo e(url('admin/sports')); ?>">Sportarten</a></li>
                <li><a href="<?php echo e(url('admin/exports')); ?>">Exporte</a></li>
                <li><a href="<?php echo e(url('admin/bewertungen/list/all')); ?>">Bewertungen</a></li>
            </ul>
        </li>
        <li><a><strong>Content</strong></a>
            <ul>
                <li><a href="<?php echo e(route('admin.index')); ?>">Artikel</a></li>
                <li><a href="<?php echo e(route('admin.index')); ?>">Bildergalerien</a></li>
                <li><a href="<?php echo e(url('admin/slider')); ?>">Slider</a></li>
                <li><a href="<?php echo e(url('admin/partner')); ?>">Partner</a></li>
                <li><a href="<?php echo e(url('admin/content')); ?>">Infoseiten</a></li>
                <li><a href="<?php echo e(url('admin/number-of-week')); ?>">Zahl der Woche</a></li>
                <li><a href="<?php echo e(url('admin/spitzensports')); ?>">Spitzensport</a></li>
            </ul>
        </li>
        <li><a><strong>Gewinnspiele</strong></a>
            <ul>
                <li><a href="<?php echo e(url('admin/vote')); ?>">Verein des Monats</a></li>
                <li><a href="<?php echo e(url('admin/contest')); ?>">Teambesprechungsraum</a></li>
                <li><a href="<?php echo e(route('admin.propose-song.list')); ?>">Vereinshit der Woche</a></li>
                <li><a href="<?php echo e(route('admin.western-rewards.list')); ?>">Best Western Gold</a></li>
            </ul>
        </li>
        <li><a href="<?php echo e(url('admin/campaigns')); ?>"><strong>Banner</strong></a></li>
        <li><a href="<?php echo e(url('admin/events/all')); ?>"><strong>Events</strong></a></li>
        <li><a><strong>Spamschutz</strong></a>
            <ul>
                <li><a href="<?php echo e(url('admin/fakeusers')); ?>">Fake-User</a></li>
                <li><a href="<?php echo e(url('admin/fakedomains')); ?>">Fake-Domain</a></li>
            </ul>
        </li>
    </ul>
</adminnav>
