<?php $__env->startSection('js-additional-vendor'); ?>
    <script src="<?php echo e(asset(('js/vendor/ckeditor/ckeditor.js'))); ?>"></script>
<?php $__env->stopSection(); ?>


    
    

    
        
            
        
    


<?php $__env->startSection('content'); ?> 
    
    <section class="admin__overview">
        <div class="container-flex">

            <?php if($errors->count() > 0): ?>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <p><?php echo e($error); ?></p>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
            <a class="" href="<?php echo e(route('admin.index')); ?>">
                <span class="fa fa-caret-left"></span> Zur Übersicht
            </a>
            <div class="row">
                <div class="col-md-10 col-md-offset-2">
                    <?php if($spitzensport->id === null): ?>
                        <?php echo Form::open(
                            [
                                'route' => 'spitzensports.store',
                                'enctype' => 'multipart/form-data',
                                'files' => true,
                                'method' => 'POST',
                            ]
                        ); ?>

                    <?php else: ?>
                        <?php echo Form::model(
                            $spitzensport,
                            [
                                'route' => ['spitzensports.update', $spitzensport->id],
                                'enctype' => 'multipart/form-data',
                                'files' => true,
                                'method' => 'PATCH',
                            ]
                        ); ?>

                    <?php endif; ?>
                    <h3>Spitzensport erfassen und editieren</h3>

                    <ul class="tabs">
                        <li class="tab-link current" data-tab="tab-1">Basis-Artikel</li>
                        <li class="tab-link" data-tab="tab-2">Multi-Page-Artikel(Will be removed)</li>
                    </ul>
                    <div id="tab-1" class="tab-content current">
                        <div class="row">
                            <div class="col-md-6">
                                <?php echo Form::label('published_from', 'Veröffentlicht von', ['class' => 'input-label']); ?>

                                <div class="input-group">
                                    <span class="fa fa-calendar input-icon"></span>
                                    <?php echo Form::text('published_from', isset($spitzensport->published_from) ? date('d.m.Y H:i', strtotime($spitzensport->published_from)) : null, ['class' => 'input', 'placeholder' => 'DD.MM.YY HH:mm', 'data-datetimepicker-enabled']); ?>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <?php echo Form::label('published_until', 'Veröffentlicht bis', ['class' => 'input-label']); ?>

                                <div class="input-group">
                                    <span class="fa fa-calendar input-icon"></span>
                                    <?php echo Form::text('published_until', isset($spitzensport->published_until) ? date('d.m.Y H:i', strtotime($spitzensport->published_until)) : null, ['class' => 'input', 'placeholder' => 'DD.MM.YY HH:mm', 'data-datetimepicker-enabled']); ?>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <?php echo Form::label('category_id', 'Kategorie', ['class' => 'input-label']); ?>

                                <div class="input-group">
                                    <?php echo Form::select('category_id', (['' => ''] + $newsCategories), $spitzensport->category_id, ['class' => 'select', 'required' => true]); ?>

                                </div>
                            </div>
                            <div class="col-md-8">
                                <?php if($spitzensport->id === null): ?>
                                    <label class="input-label" for="admin-news-title">Titel*</label>
                                    <div class="input-group">
                                        <input name="title" id="admin-news-title" class="input" type="text"
                                               placeholder="Titel">
                                    </div>
                                <?php else: ?>
                                    <?php echo Form::label('title', 'Titel', ['class' => 'input-label']); ?>

                                    <div class="input-group">
                                        <?php echo Form::text('title', null, ['class' => 'input', 'placeholder' => 'Titel', 'required' => true]); ?>

                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="col-md-4">
                                <?php if($spitzensport->id === null): ?>
                                    <label class="input-label" for="admin-news-slug">URL* (Ergänzung nach
                                        Titeleingabe)</label>
                                    <div class="input-group">
                                        <input style="display: none" name="slug" id="admin-news-slug"
                                               class="input" type="text"
                                               placeholder="URL">
                                        <input type="hidden" name="slug_date" id="admin-news-slug_date"
                                               value="<?php echo e(date('Y-m-d')); ?>">
                                    </div>
                                <?php else: ?>
                                    <?php echo Form::label('slug', 'URL', ['class' => 'input-label']); ?>

                                    <div class="input-group">
                                        <?php echo Form::text('slug', null, ['class' => 'input', 'placeholder' => 'URL']); ?>

                                    </div>
                                <?php endif; ?>
                            </div>

                            <div class="col-md-12">
                                <?php echo Form::label('sub_title', 'Untertitel', ['class' => 'input-label']); ?>

                                <div class="input-group">
                                    <?php echo Form::text('sub_title', null, ['class' => 'input', 'placeholder' => 'Untertitel', 'required' => true]); ?>

                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <?php echo Form::label('full_name', 'Full Name', ['class' => 'input-label']); ?>

                                <div class="input-group">
                                    <?php echo Form::text('full_name', null, ['class' => 'input', 'placeholder' => 'Full Name', 'required' => true]); ?>

                                </div>
                            </div>
                            <div class="col-md-12">
                                <?php echo Form::label('profession', 'Profession', ['class' => 'input-label']); ?>

                                <div class="input-group">
                                    <?php echo Form::text('profession', null, ['class' => 'input', 'placeholder' => 'Profession', 'required' => true]); ?>

                                </div>
                            </div>

                            <div class="col-md-6">
                                <?php echo Form::label('image', 'Haupt-Bild', ['class' => 'input-label']); ?>

                                <div data-upload-result>
                                    <div class="col-xs-12 hidden" data-new-image-text>
                                        <span class="input-label">Neu hinzugefügte Bilder</span>
                                    </div>
                                </div>
                                <?php if($spitzensport->id !== null && $spitzensport->main_image->originalFilename() !== null): ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <a href="<?php echo e(asset($spitzensport->main_image->url('original'))); ?>"
                                               data-lightbox="main_image">
                                                <img src="<?php echo e(asset($spitzensport->main_image->url('startpage'))); ?>">
                                            </a>
                                        </div>
                                        <div class="col-md-12">
                                            <label>Anderes Bild hochladen</label>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <div data-upload-result>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div id="upload-result" class="thumbnail__list"></div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="input-group">
                                                <label for="main_image"
                                                       class="button button--grey button--center button--icon button--full-width">
                                                    <span class="fa fa-upload"></span>Bild hochladen</label>
                                                <input type="file" name="main_image" id="main_image" class="input-file">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <?php echo Form::label('image_source', 'Bildquelle', ['class' => 'input-label']); ?>

                                <div class="input-group">
                                    <?php echo Form::text('main_image_source', null, ['class' => 'input', 'placeholder' => 'Bildquelle']); ?>

                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <?php echo Form::label('meta_bild_alt', 'meta-bild-alt', ['class' => 'input-label']); ?>

                                <div class="input-group">
                                    <?php echo Form::text('meta_bild_alt',
                                        ($spitzensport->id == null || $spitzensport->meta->meta_bild_alt == null) ? null : $spitzensport->meta->meta_bild_alt ,
                                        ['class' => 'input', 'placeholder' => 'meta-bild-alt']); ?>

                                </div>
                            </div>

                            <div class="col-md-12">
                                <h3>Meta-Angaben für Suchmaschine</h3>
                                <?php echo Form::label('meta_description', 'meta-description', ['class' => 'input-label']); ?>

                                <div class="input-group">
                                    <?php echo Form::text('meta_description',
                                        ($spitzensport->id == null || $spitzensport->meta->meta_description == null) ? null : $spitzensport->meta->meta_description ,
                                      ['class' => 'input', 'placeholder' => 'meta-description']); ?>

                                </div>
                            </div>


                            <div class="col-md-12">
                                <?php echo Form::label('meta_keywords', 'meta-keywords', ['class' => 'input-label']); ?>

                                <div class="input-group">
                                    <?php echo Form::text('meta_keywords',
                                        ($spitzensport->id == null || $spitzensport->meta->meta_keywords == null ) ? null : $spitzensport->meta->meta_keywords,
                                        ['class' => 'input', 'placeholder' => 'meta-keywords']); ?>

                                </div>
                            </div>

                            <div class="col-md-12">
                                <?php echo Form::label('meta_author', 'meta-author', ['class' => 'input-label']); ?>

                                <div class="input-group">
                                    <?php echo Form::text('meta_author',
                                        ($spitzensport->id == null || $spitzensport->meta->meta_author) ? 'vereinsleben.de' : $spitzensport->meta->meta_author ,
                                        ['class' => 'input', 'placeholder' => 'meta-author']); ?>

                                </div>
                            </div>

                            <div class="col-md-12">
                                <?php echo Form::label('meta_language', 'meta-language', ['class' => 'input-label']); ?>

                                <div class="input-group">
                                    <?php echo Form::text('meta_language',
                                        ($spitzensport->id == null || $spitzensport->meta->meta_language)? 'de' : $spitzensport->meta->meta_language,
                                        ['class' => 'input', 'placeholder' => 'meta-language']); ?>

                                </div>
                            </div>
                            <div class="col-md-12">
                                <h3>Eingabe von Texten</h3>
                            </div>

                            <div class="col-md-12">
                                <?php echo Form::label('content_teaser', 'Anleser', ['class' => 'input-label']); ?>

                                <div class="input-group">
                                    <?php echo Form::textarea('content_teaser', null, ['class' => 'input', 'rows' => 10, 'placeholder' => 'Anleser']); ?>

                                </div>
                            </div>

                            <div class="col-md-12">
                                <?php echo Form::label('content', 'Text', ['class' => 'input-label']); ?>

                                <div class="input-group">
                                    <?php echo Form::textarea('content', $spitzensport->content, ['class' => 'input', 'rows' => 10, 'placeholder' => 'Text']); ?>

                                </div>
                            </div>

                            <div class="col-md-12">
                                <?php echo Form::label('steckbrief', 'Steckbrief', ['class' => 'input-label']); ?>

                                <div class="input-group">
                                    <?php echo Form::textarea('steckbrief', $spitzensport->steckbrief, ['class' => 'input', 'rows' => 10, 'placeholder' => 'Steckbrief']); ?>

                                </div>
                            </div>

                            <div class="col-md-12">
                                <h3>Tags für die Suche</h3>
                                <div class="input-group">
                                    <?php echo Form::text('tag',
                                        ($spitzensport->id == null || $spitzensport->tags == null) ? null : implode(" ", $spitzensport->tags()->pluck('name')->all()) ,
                                      ['class' => 'input', 'placeholder' => 'tags']); ?>

                                </div>
                            </div>
                            <div class="col-md-12">
                                <h3>Bundesländer</h3>
                            </div>

                            <div class="col-md-12">
                                <div class="input-group">
                                    <?php echo e(Form::checkbox('All', '1', '', ['id' => 'checkAllSpitzensportStates'])); ?>

                                    <?php echo Form::label('Alle auswählen', '', ['class' => 'input-label']); ?>

                                </div>
                            </div>

                            <?php $__currentLoopData = $federal_states; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $state): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="col-md-3">
                                    <div class="input-group spitzensportStatesSelection">
                                        <?php echo e(Form::checkbox('federal_states[]',
                                         $state->id,
                                          in_array($state->id, $spitzensport->states()->pluck('state_id')->all() ) ? true : false
                                        , ['id' => $state->constant])); ?>

                                        <?php echo Form::label($state->constant, $state->name, ['class' => 'input-label']); ?>

                                    </div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <div class="col-md-6">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <input <?php echo e(($spitzensport->exclusive == 1 || $spitzensport->exclusive === 1) ? 'checked' : ''); ?>

                                               class="input-checkbox"
                                               type="checkbox"
                                               name="exclusive"
                                               id="admin-news-exclusive" value="<?php echo e(($spitzensport->exclusive == 1 || $spitzensport->exclusive === 1) ? 1 : 0); ?>">
                                        <label class="input-checkbox__label"
                                               for="admin-news-exclusive">Bundeslandexclusiv</label>
                                    </div>
                                </div>
                                <?php 
                                    use Vereinsleben\Spitzensport;
                                ?>
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <input <?php echo e(($spitzensport->state === Spitzensport::STATE_DRAFT) ? 'checked' : ''); ?>

                                               class="input-checkbox"
                                               type="radio"
                                               name="state"
                                               id="admin-news-draft" value="<?php echo e(Spitzensport::STATE_DRAFT); ?>">
                                        <label class="input-checkbox__label"
                                               for="admin-news-draft">Entwurf</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <input <?php echo e(($spitzensport->state === Spitzensport::STATE_PUBLISHED) ? 'checked' : ''); ?>

                                               class="input-checkbox"
                                               type="radio"
                                               name="state"
                                               id="admin-news-published" value="<?php echo e(Spitzensport::STATE_PUBLISHED); ?>">
                                        <label class="input-checkbox__label"
                                               for="admin-news-published">Veröffentlicht</label>
                                    </div>
                                </div>
                            </div>
                            <?php
                                use Vereinsleben\Role;
                                $role = new Role();
                                $adminRoleId  = $role->getByConstant(Role::ADMIN)->id;
                            ?>
                              <div class="col-md-6">
                                  <label for="contentManagerId" class="input-label">Autor</label>
                                  <div class="input-group" id="dropwdown-cotent-manager">
                                      <select name="contentManagerId" id="contentManagerId" class="select">
                                          <option value="0"></option>
                                          <?php $__currentLoopData = $usersContentManager; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                              <option <?php echo e(Auth::user()->isContentManager() && Auth::user()->id != $user->id ? 'disabled' : ''); ?> <?php echo e($spitzensport->user_id == $user->id ? 'selected' : ''); ?> value="<?php echo e($user->id); ?>">
                                                <?php echo e($user->role_id == $adminRoleId ? 'Administrator' : 'Contentmanager'); ?> -
                                                <?php echo e($user->fullName() != " " ? $user->fullName() : $user->username); ?>

                                              </option>
                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                      </select>
                                  </div>
                              </div>
                        </div>
                    </div>

                    <div id="tab-2" class="tab-content">
                        <div id="multipleImages" class="tab-pane fade">
                            <div class="col-xs-12" style="padding-top:30px;">
                                <?php if(isset($spitzensportImages) && count($spitzensportImages)>0): ?>
                                    <div class="thumbnail__list" id="sortable">
                                        <?php $__currentLoopData = $spitzensportImages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $spitzensportImage): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <input type="checkbox" name="delete-image[]"
                                                           value="<?php echo e($spitzensportImage->id); ?>"
                                                           id="news_thumbnail-<?php echo e($spitzensportImage->id); ?>"
                                                           class="thumbnail__switch">
                                                    <label for="news_thumbnail-<?php echo e($spitzensportImage->id); ?>"
                                                           class="thumbnail__label thumbnail__container thumbnail__container--deleteable">
                                                        <img alt="<?php echo e($spitzensportImage->title); ?>"
                                                             title="<?php echo e($spitzensportImage->title); ?>"
                                                             class="thumbnail__image"
                                                             src="<?php echo e(asset($spitzensportImage->picture->url('singleView'))); ?>"/>
                                                    </label>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <input type="text" name="imgUpdateTitle[<?php echo e($spitzensportImage->id); ?>]"
                                                               class="input" value="<?php echo e($spitzensportImage->title); ?>">
                                                    </div>
                                                    <div class="form-group" style="padding-top:20px;">
                                                        <textarea name="imgUpdateDesc[<?php echo e($spitzensportImage->id); ?>]"
                                                                  class="input"><?php echo e($spitzensportImage->description); ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="col-md-12">
                                <div class="input-group" data-upload-result>
                                    <label for="news_image" class="btn btn--grey btn--full-width btn--large"><span
                                                class="fa fa-upload"></span>Bild hochladen</label>
                                    <input type="file" multiple name="spitzensportImages[]"
                                           id="news_image" class="input-file">
                                    <div id="sortable" style="padding-top:20px;"
                                         class="upload-result thumbnail__list"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="input-group">
                            <?php echo Form::submit('News speichern', array('class' => 'button button--light button--light-white button--center button--full-width')); ?>

                        </div>
                    </div>

                    <input type="hidden" id="admin-news-csrf" value="<?php echo e(csrf_token()); ?>">

                    <?php echo Form::close(); ?>

                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>