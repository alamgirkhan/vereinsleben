<?php if(isset($content)): ?>
    <div class="hero-slider__content hero-slider__content--centered">
        <h1 class="headline-primary headline--centered headline-primary--inverted">
            <?php if(isset($content['icon'])): ?><span class="fa fa-<?php echo e($content['icon']); ?>"></span><?php endif; ?>
            <?php echo e($content['headline']); ?>

        </h1>
        <?php if(isset($content['caption'])): ?>
            <h2 class="hero-slider__caption headline--centered"><?php echo e($content['caption']); ?></h2>
        <?php endif; ?>
    </div>
<?php else: ?>
    <div class="container-flex">
        <div class="hero-slider__content">
            <h1 class="headline-primary headline-primary--main headline-primary--inverted">Vereinsleben.de</h1>
            <?php if(\Carbon\Carbon::now() < '2016-12-30'): ?>
                <h2 class="headline-secondary headline-secondary--main headline-secondary--inverted">wünscht sportliche
                    Weihnachten</h2>
            <?php else: ?>
                <h2 class="headline-secondary headline-secondary--main headline-secondary--inverted">Wir. Leben. Sport.</h2>
            <?php endif; ?>
            <div class="hero-slider__box-wrapper">
                <?php if(!Auth::check()): ?>
                    <a href="<?php echo e(url('/register')); ?>" class="hero-slider__big-btn-text">
                        <div class="hero-slider__big-btn">
                            Registriere Dich jetzt und lege Deinen Verein an!
                        </div>
                    </a>
                <?php else: ?>
                    <div class="hero-slider__big-btn-text">
                        <div class="hero-slider__big-btn">
                            Registriere Dich jetzt und lege Deinen Verein an!
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php endif; ?>