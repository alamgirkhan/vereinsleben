<div <?php echo e(isset($newContact) ? 'id=add-contact-person' : ''); ?> class="tabbable-menu__page <?php echo e(isset($newContact) ? '' : 'tabbable-menu__page--active'); ?>">
    <?php if($bandage->id === null || isset($newContact)): ?>
        <?php echo Form::open(
            array(
                'method' => 'post',
                'route' => ['contact-person.store'],
                'files' => 'true'
            )
        ); ?>

    <?php else: ?>
        <?php echo Form::model(
        $contact,
        array(
            'method' => 'patch',
            'route' => ['contact-person.update', $contact->id ],
            'files' => 'true',
            'data-async-form'
        )
    ); ?>

    <?php endif; ?>
    <div class="club-detail__inline-form">
        <div class="row">
            <div class="col-md-12">
                <?php echo Form::label('order_number', 'Reihenfolge', ['class' => 'input-label']); ?>

                <div class="input-group">
                    <?php echo Form::number('order_number', null, ['class' => 'input', 'placeholder' => '']); ?>

                </div>
            </div>

            <div class="col-md-12">
                <?php echo Form::label('department', 'Abteilung', ['class' => 'input-label']); ?>

                <div class="input-group">
                    <?php echo Form::text('department', null, ['class' => 'input', 'placeholder' => '', 'required' => 'required']); ?>

                </div>
            </div>

            <div class="col-md-12">
                <?php echo Form::label('gender', 'Anrede', ['class' => 'input-label']); ?>

                <div class="input-group">
                    <?php echo Form::select('gender', ['' => 'Bitte wählen', 'Herr' => 'Herr', 'Frau' => 'Frau'], null, ['required' => 'required', 'class' => 'select']); ?>

                </div>
            </div>

            <div class="col-md-12">
                <?php echo Form::label('name', 'Name', ['class' => 'input-label']); ?>

                <div class="input-group">
                    <?php echo Form::text('name', null, ['class' => 'input', 'rows' => 10, 'placeholder' => '', 'required' => 'required']); ?>

                </div>
            </div>

            <div class="col-md-12">
                <?php echo Form::label('position', 'Position', ['class' => 'input-label']); ?>

                <div class="input-group">
                    <?php echo Form::text('position', null, ['class' => 'input', 'rows' => 10, 'placeholder' => '', 'required' => 'required']); ?>

                </div>
            </div>

            <div class="col-md-12">
                <?php echo Form::label('phone', 'Telefon', ['class' => 'input-label']); ?>

                <div class="input-group">
                    <?php echo Form::text('phone', null, ['class' => 'input phone', 'rows' => 10, 'placeholder' => '', 'required' => 'required']); ?>

                </div>
            </div>

            <div class="col-md-12">
                <?php echo Form::label('email', 'Email', ['class' => 'input-label']); ?>

                <div class="input-group">
                    <?php echo Form::email('email', null, ['class' => 'input', 'rows' => 10, 'placeholder' => '', 'required' => 'required']); ?>

                </div>
            </div>

            <?php echo Form::hidden('bandage_id', $bandage->id); ?>


            <div class="col-xs-12">
                <button class="button button--dark button--center button--icon button--full-width"
                        type="submit"><span class="fa fa-edit"></span>
                    <?php if($bandage->id === null || isset($newContact)): ?>
                        Eintragen
                    <?php else: ?>
                        Aktualisieren
                    <?php endif; ?>
                </button>
            </div>

            <?php echo Form::close(); ?>

        </div>
    </div>
</div>