<section class="profile-section profile-section--header">
    <div class="profile-header">
        <div class="profile-header__image-container" style="background-image: url('<?php echo e(asset($user->header->url('singleView'))); ?>')">
            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $user)): ?>
                <div class="btn btn--icon profile__edit-button animation profile__edit-button--header animation--scale"
                     data-call-popover="header">
                    <i class="fa fa-upload" aria-hidden="true"></i>
                </div>
            <?php endif; ?>
            <img class="profile-header__image" src="<?php echo e(asset($user->header->url('singleView'))); ?>" alt=""/>
        </div>
        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $user)): ?>
            <?php echo $__env->make('user.profile.partials.edit.header', ['user' => $user], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php endif; ?>
    </div>
</section>