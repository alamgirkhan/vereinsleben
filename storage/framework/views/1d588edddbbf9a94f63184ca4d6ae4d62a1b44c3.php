<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('admin.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <section class="admin__overview">
        <div class="container">
            <div class="row admin__section">
                <div class="col-xs-12">
                    <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">News Positionen</h1>
                </div>
            </div>
            <table id="news-pos-table" class="table table-striped table-bordered"
                   data-token="<?php echo e(csrf_token()); ?>">
                <thead>
                <tr>
                    <th style="width:15%">Pos&nbsp;</th>
                    <th style="width:5%">ID&nbsp;</th>
                    <th style="width:35%">Titel&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                <?php echo Form::open(['route' => 'admin.news.pos.save', 'method' => 'GET', 'id' => 'search', 'class' => '']); ?>


                <?php if(count($news) > 0): ?>
                    <?php for($i = 1; $i < 9; $i++): ?>
                        <?php
                            $j = strval($i) . '.position';
                            $k = strval($i) . '.id';
                            $l = strval($i) . '.title';
                        ?>
                        <?php if(array_get($news, $j) == $i): ?>
                            <tr>
                                <td>
                                    <?php echo e($i); ?>&nbsp;&nbsp;&nbsp;<?php if($i == 8): ?> <u>nur</u> Bildergalerie <?php endif; ?>
                                </td>
                                <td>
                                    <?php echo e(Form::text('newsid_pos'.$i, array_get($news, $k), array('id' => array_get($news, $k)))); ?>

                                    <?php echo e(Form::hidden('oldnewsid_pos'.$i, array_get($news, $k), array('id' => array_get($news, $k)))); ?>

                                </td>
                                <td>
                                    <?php echo e(array_get($news, $l)); ?>

                                </td>
                            </tr>
                        <?php else: ?>
                            <tr>
                                <td>
                                    <?php echo e($i); ?>&nbsp;&nbsp;&nbsp;<?php if($i == 8): ?> <u>nur</u> Bildergalerie <?php endif; ?>
                                </td>
                                <td>
                                    <?php echo e(Form::text('newsid_pos'.$i, '')); ?>

                                    <?php echo e(Form::hidden('oldnewsid_pos'.$i, '')); ?>

                                </td>
                                <td>-</td>
                            </tr>
                        <?php endif; ?>
                    <?php endfor; ?>

                <?php else: ?>
                    <?php for($i = 1; $i < 9; $i++): ?>
                        <tr>
                            <td>
                                <?php echo e($i); ?>&nbsp;&nbsp;&nbsp;<?php if($i == 8): ?> <u>nur</u> Bildergalerie <?php endif; ?>
                            </td>
                            <td>
                                <?php echo e(Form::text('newsid_pos'.$i, '')); ?>

                                <?php echo e(Form::hidden('oldnewsid_pos'.$i, '')); ?>

                            </td>
                            <td>-</td>
                        </tr>
                    <?php endfor; ?>
                <?php endif; ?>




                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                

                
                
                
                
                
                
                
                
                
                
                
                </tbody>
            </table>
            <div class="row">
                <div class="col-md-3">
                    <button type="submit" name="Speichern" onclick="return confirm('Positionen speichern?')"
                            class="button button--light button--light-white button--center button--full-width">
                        Speichern
                    </button>
                </div>
            </div>
            <?php echo Form::close(); ?>

        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>