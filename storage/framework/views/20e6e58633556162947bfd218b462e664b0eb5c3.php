<?php $__env->startSection('title'); ?>
    <?php echo e($content->title); ?> - vereinsleben.de
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('components.slider',
    ['condensed' => true,
     'content' => [
            'headline' => $content->title,
    ]], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="container section__content">
        <div class="row">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <?php echo $content->content; ?>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>