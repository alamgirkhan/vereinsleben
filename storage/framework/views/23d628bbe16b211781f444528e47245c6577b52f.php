<div class="col-xxs-10 col-xxs-offset-1 col-xs-6 col-sm-4 col-md-4 <?php if($index == 3 ): ?> col-md-offset-2 <?php endif; ?> <?php if($index > 100 && $index < 10): ?> col-sm-offset-4 col-md-offset-2 <?php endif; ?>">
    <div class="vote-teaser">
        <a href="<?php echo e(route('club.vote.nominee', $club->slug)); ?>" class="vote-teaser__img"
           style="background-image: url('<?php echo e(asset($club->avatar->url('startPage'))); ?>')">
            <div class="vote-teaser__badge">Details</div>
        </a>
        <div class="vote-teaser__content-wrapper">
            <div class="vote-teaser__content">
                <div class="vote-teaser__project-name">
                    <?php echo e($nominee->project); ?>

                </div>
                <a href="<?php echo e(route('club.vote.nominee', $club->slug)); ?>" class="vote-teaser__club-details">
                    <?php echo e($club->name); ?>

                </a>
            </div>
        </div>
        <vote-button label="Abstimmen" :id="<?php echo e($nominee->vote_id); ?>"
                     :authenticated="<?php echo e((int)Auth::check()); ?>"
                     nominee="<?php echo e($nominee->id); ?>"
                     voted="<?php echo e(Auth::check() && $hasVoted); ?>"></vote-button>
    </div>
</div>