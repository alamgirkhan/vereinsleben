<div <?php echo e(isset($newEvent) ? 'id=add-event' : ''); ?> class="tabbable-menu__page <?php echo e(isset($newEvent) ? '' : 'tabbable-menu__page--active'); ?>">
    <?php if($bandage->id === null || isset($newEvent)): ?>
        <?php echo Form::open(
            array(
                'method' => 'post',
                'route' => ['bandage.event.store', '#events'],
                'files' => 'true'
            )
        ); ?>

    <?php else: ?>
        <?php echo Form::model(
        $event,
        array(
            'method' => 'patch',
            'route' => ['bandage.event.update', $event->id, '#events'],
            'files' => 'true',
            'data-async-form'
        )
    ); ?>

    <?php endif; ?>
    <div class="club-detail__inline-form">
        <div class="row">
            
            <div class="col-md-12">
                <?php echo Form::label('title', 'Titel', ['class' => 'input-label']); ?>

                <div class="input-group">
                    <?php echo Form::text('title', null, ['class' => 'input', 'placeholder' => 'Titel der Veranstaltung', 'required' => 'required']); ?>

                </div>
            </div>


            <div class="col-md-12">
                <?php echo Form::label('content_raw', 'Text', ['class' => 'input-label']); ?>

                <div class="input-group">
                    <?php echo Form::textarea('content_raw', null, ['class' => 'input', 'rows' => 10, 'placeholder' => 'Beschreibung der Veranstaltung', 'required' => 'required']); ?>

                </div>
            </div>

            <div class="col-md-6">
                <?php echo Form::label('schedule_begin', 'Beginn der Veranstaltung', ['class' => 'input-label']); ?>

                <div class="input-group">
                    <?php echo Form::text('schedule_begin', isset($event->schedule_begin) ? date('d.m.Y H:i', strtotime($event->schedule_begin)) : null, ['class' => 'input', 'placeholder' => 'DD.MM.YY HH:mm', 'data-datetimepicker-enabled', 'required' => 'required']); ?>

                </div>
            </div>

            <div class="col-md-6">
                <?php echo Form::label('schedule_end', 'Ende der Veranstaltung', ['class' => 'input-label']); ?>

                <div class="input-group">
                    <?php echo Form::text('schedule_end', isset($event->schedule_end) ? date('d.m.Y H:i', strtotime($event->schedule_end)) : null, ['class' => 'input', 'placeholder' => 'DD.MM.YY HH:mm', 'data-datetimepicker-enabled', 'required' => 'required']); ?>

                </div>
            </div>

            <div class="col-md-9">
                <?php echo Form::label('street', 'Straße', ['class' => 'input-label']); ?>

                <div class="input-group">
                    <?php echo Form::text('street', null, ['class' => 'input', 'placeholder' => 'Straße', 'required' => 'required']); ?>

                </div>
            </div>

            <div class="col-md-3">
                <?php echo Form::label('house_number', 'Hausnummer', ['class' => 'input-label']); ?>

                <div class="input-group">
                    <?php echo Form::text('house_number', null, ['class' => 'input', 'placeholder' => 'Nr']); ?>

                </div>
            </div>

            <div class="col-md-4">
                <?php echo Form::label('zip', 'Postleitzahl', ['class' => 'input-label']); ?>

                <div class="input-group">
                    <?php echo Form::text('zip', null, ['class' => 'input', 'placeholder' => 'PLZ', 'required' => 'required']); ?>

                </div>
            </div>

            <div class="col-md-8">
                <?php echo Form::label('city', 'Stadt', ['class' => 'input-label']); ?>

                <div class="input-group">
                    <?php echo Form::text('city', null, ['class' => 'input', 'placeholder' => 'Stadt / Ort', 'required' => 'required']); ?>

                </div>
            </div>

            <div class="col-md-6">
                <?php echo Form::label('published_from', 'Veröffentlicht von', ['class' => 'input-label']); ?>

                <div class="input-group">
                    <?php echo Form::text('published_from', isset($event->published_from) ? date('d.m.Y H:i', strtotime($event->published_from)) : null, ['class' => 'input', 'placeholder' => 'DD.MM.YY HH:mm', 'data-datetimepicker-enabled']); ?>

                </div>
            </div>

            <div class="col-md-6">
                <?php echo Form::label('published_until', 'Veröffentlicht bis', ['class' => 'input-label']); ?>

                <div class="input-group">
                    <?php echo Form::text('published_to', isset($event->published_to) ? date('d.m.Y H:i', strtotime($event->published_to)) : null, ['class' => 'input', 'placeholder' => 'DD.MM.YY HH:mm', 'data-datetimepicker-enabled']); ?>

                </div>
            </div>

            <?php echo Form::hidden('bandage_id', $bandage->id); ?>


            <div class="col-xs-12">
                <div id="<?php if(isset($event)): ?><?php echo e('upload-result-'. $event->id); ?><?php else: ?><?php echo e('upload-result'); ?><?php endif; ?> "
                     class="thumbnail__list">
                    <?php if(isset($event)): ?>
                        <?php $__currentLoopData = $event->images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <input type="checkbox" name="delete-image[]" value="<?php echo e($image->id); ?>"
                                   id="event_thumbnail-<?php echo e($image->id); ?>" class="thumbnail__switch">
                            <label for="event_thumbnail-<?php echo e($image->id); ?>"
                                   class="thumbnail__label thumbnail__container thumbnail__container--deleteable">
                                <img class="thumbnail__image" src="<?php echo e(asset($image->picture->url('singleView'))); ?>"/>
                            </label>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </div>
            </div>

            <div data-upload-result>
                <div class="col-xs-12 hidden" data-new-image-text>
                    <span class="input-label">Neu hinzugefügte Bilder</span>
                </div>
                <div class="col-xs-12">
                    <div id="<?php if(isset($post)): ?><?php echo e('upload-result-'. $post->id); ?><?php else: ?><?php echo e('upload-result'); ?><?php endif; ?> "
                         class="thumbnail__list">
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="club-detail__inline-form-submit-wrapper">
                        <label for="<?php if(isset($event)): ?><?php echo e('event_image-'. $event->id); ?><?php else: ?><?php echo e('event_image'); ?><?php endif; ?>"
                               class="button button--grey button--center button--icon button--full-width">
                            <span class="fa fa-upload"></span>Bilder hochladen</label>
                        <input multiple type="file" name="images[]"
                               id="<?php if(isset($event)): ?><?php echo e('event_image-'. $event->id); ?><?php else: ?><?php echo e('event_image'); ?><?php endif; ?>"
                               class="input-file">
                        <button class="button button--dark button--center button--icon button--full-width"
                                type="submit"><span class="fa fa-edit"></span>
                            <?php if($bandage->id === null || isset($newEvent)): ?>
                                Eintragen
                            <?php else: ?>
                                Aktualisieren
                            <?php endif; ?>
                        </button>
                    </div>
                </div>
            </div>
            <?php echo Form::close(); ?>

        </div>
    </div>
</div>