<div class="news-detail__aside-news-item">
    <a class="news-detail__aside-link" href="<?php echo e($news->slug); ?>">
        <span class="news-detail__aside-news-item-date"><?php echo e(strftime('%d. %B %Y', strtotime($news->published_at))); ?></span>
        <span class="news-detail__aside-news-item-caption" data-insert-hyphen-async="true"><?php echo e($news->title); ?></span>
        <div class="news-detail__aside-news-item-image"
             style="background-image: url('<?php echo e(asset($news->main_image->url('startpage'))); ?>');">
        </div>
    </a>
</div>