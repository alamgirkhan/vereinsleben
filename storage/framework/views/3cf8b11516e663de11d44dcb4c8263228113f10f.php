<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('admin.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <section class="admin__overview">
        <div class="container">
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            <div class="row admin__section">
                <div class="col-xs-12">
                    <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">Events anlegen und editieren</h1>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <a href="<?php echo e(route('admin.event.edit', 0)); ?>"
                           class="button button--icon button--full-width button--center button--light">
                            <span class="fa fa-plus"></span> Event hinzufügen
                        </a>
                    </div>
                </div>
            </div>
            <table id="admin-table" class="admin__table" data-token="<?php echo e(csrf_token()); ?>">
                <thead>
                <tr>
                    <th style="width:5%">ID&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:35%">Titel&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:10%">Sportart&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:15%">Absender&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:10%">Publiziert</th>
                    <th style="width:10%">Optionen</th>
                </tr>
                </thead>
                <tbody>
                <?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($event->id); ?></td>
                        <td><a href="<?php echo e(route('event.detail', $event->id)); ?>" target="_blank"><?php echo e($event->title); ?></a>
                        </td>
                        <td>
                            <?php
                                if(isset($event->sport)){
                                    $sport = \Vereinsleben\Sport::whereId($event->sport)->firstOrFail();
                                    $sport_title = $sport->title;
                                }
                            ?>
                            <?php if(isset($event->sport)): ?>
                                <?php echo e($sport_title); ?>

                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if(isset($event->club_id)): ?>
                                Verein
                            <?php elseif(isset($event->bandage_id)): ?>
                                Verband
                            <?php else: ?>
                                vereinsleben.de
                            <?php endif; ?>

                        </td>
                        <td>
                            <?php if($event->published_at): ?>
                                <i class="fa fa-circle" style="color: green;"></i>
                            <?php else: ?>
                                <i class="fa fa-circle" style="color: red;"></i>
                            <?php endif; ?>
                        </td>
                        <td>
                            <div class="admin__table-option">
                                <a href="<?php echo e(route('admin.event.edit', $event->id)); ?>" target="_blank">
                                    <span class="fa fa-edit"></span>
                                </a>
                                
                                
                                
                                
                                
                                
                                <a onclick="return confirm('Möchtest du diese Aufzeichnung wirklich löschen?')"
                                   href="#">
                                    <span class="fa fa-trash"></span>
                                </a>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>