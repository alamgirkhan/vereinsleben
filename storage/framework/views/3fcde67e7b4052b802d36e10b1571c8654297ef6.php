<div class="container">
    <h2 class="hl hl--large hl--underlined hl--inverted hl--centered hl--upper">
        <?php echo e($vote->isCurrent() ? $vote->name : $vote->pre_name); ?>

    </h2>

    <div class="text-block text-block--compressed text-block--thin text-block--smaller-spacing">
        <?php echo $vote->isCurrent() ? $vote->description : $vote->pre_description; ?>

    </div>

    <?php if (! ($vote->isCurrent())): ?>
        <p class="text-block text-block--compressed text-block--align-center">
            <a href="/verein-des-monats/teilnahme" class="btn btn--default btn--inline btn--large btn--upper">
                Jetzt als "Verein des Monats" bewerben
            </a>
        </p>
    <?php endif; ?>
</div>