<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('admin.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <section class="admin__overview">
        <div class="container">
            <div class="row admin__section">
                <div class="col-xs-12">
                    <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">Zahl der Woche</h1>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <a href="<?php echo e(route('number-of-week.create')); ?>"
                           class="button button--icon button--full-width button--center button--light">
                            <span class="fa fa-plus"></span> Neu anlegen
                        </a>
                    </div>
                </div>
            </div>
            <table id="admin-table" class="admin__table" data-token="<?php echo e(csrf_token()); ?>">
                <thead>
                <tr>
                    <th style="width:7%">ID&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:35%">Zahl&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:10%">Kurzinfo&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:15%">Startdatum<i class="glyphicon glyphicon-sort"></i></th>
                    <th style="width:10%">Optionen</th>
                </tr>
                </thead>
                <tbody>
                <?php $__currentLoopData = $numbers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $number): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($number->id); ?></td>
                        <td><?php echo e($number->number); ?></td>
                        <td>
                            <?php echo e($number->short_description); ?>

                        </td>
                        <td>
                            <?php echo e(date('Y/m/d', strtotime($number->published_from))); ?>

                        </td>
                        <td>
                            <div class="admin__table-option">
                                <a href="<?php echo e(route('number-of-week.edit', $number->id)); ?>">
                                    <span class="fa fa-edit"></span>
                                </a>
                                <form action="<?php echo e(route('number-of-week.destroy', $number->id)); ?>" method="POST">
                                    <?php echo method_field('DELETE'); ?>
                                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                    <button onclick="return confirm('Möchten Sie diese Bewertung wirklich löschen?')" type="submit" class="btn-del">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </form>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>