<div class="row">
    <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php if($post->images()->count() > 0): ?>
            <?php $__currentLoopData = $post->images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-md-3 pfl-fts">
                    <a href="<?php echo e(asset($image->picture->url('singleView'))); ?>"
                       data-lightbox="gallery">
                        <img class="responsive-image" src="<?php echo e(asset($image->picture->url('detail'))); ?>"/>
                    </a>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php if(!$image): ?>
        <div class="col-md-12">Es sind noch keine Bilder in dieser Pinnwand gepostet!</div>
    <?php endif; ?>
</div>