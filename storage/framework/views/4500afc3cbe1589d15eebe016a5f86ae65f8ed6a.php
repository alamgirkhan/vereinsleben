<?php $__env->startSection('content'); ?>
    <section class="error-page__section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="error-page__headline">Fehler (404): Seite nicht gefunden</h1>

                    <p><a class="button button--center button--light button--full-width" href="<?php echo e(route('home')); ?>"><i class="fa fa-home"></i> Zur Startseite</a></p>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>