<?php $__env->startSection('js-additional-vendor'); ?>
    <script src="<?php echo e(asset(('js/vendor/ckeditor/ckeditor.js'))); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <section class="admin__overview">
        <div class="container-flex">
            <?php if($errors->count() > 0): ?>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <p><?php echo e($error); ?></p>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
            <a class="" href="<?php echo e(route('admin.content.index')); ?>">
                <span class="fa fa-caret-left"></span> Zur Übersicht
            </a>
            <div class="row">
                <div class="col-md-10 col-md-offset-2">
                    <?php if($content->id === null): ?>
                        <?php echo Form::open(
                            [
                                'route' => 'admin.content.store',
                                'enctype' => 'multipart/form-data',
                                'files' => true,
                                'method' => 'POST',
                            ]
                        ); ?>

                    <?php else: ?>
                        <?php echo Form::model(
                            $content,
                            [
                                'route' => ['admin.content.update', $content->id],
                                'enctype' => 'multipart/form-data',
                                'files' => true,
                                'method' => 'PATCH',
                            ]
                        ); ?>

                    <?php endif; ?>

                    <div class="row">
                        <div class="col-md-6">
                            <?php echo Form::label('content_title', 'Titel', ['class' => 'input-label']); ?>

                            <div class="input-group">
                                <?php echo Form::text('content_title', isset($content->title) ? $content->title : null, ['class' => 'input', 'placeholder' => 'content Title']); ?>

                            </div>
                        </div>

                        <div class="col-md-12">
                            <?php echo Form::label('content_cont', 'Content', ['class' => 'input-label']); ?>

                            <div class="input-group">
                                <?php echo Form::textarea('content', $content->content, ['class' => 'input', 'rows' => 10, 'placeholder' => 'content Description']); ?>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <?php echo Form::label('content_slug', 'Slug', ['class' => 'input-label']); ?>

                            <div class="input-group">
                                <?php echo Form::text('content_slug', isset($content->slug) ? $content->slug : null, ['class' => 'input', 'placeholder' => 'content slug']); ?>

                            </div>
                        </div>

                        <div class="col-md-12">
                            <h3>Meta-Angaben für Suchmaschine</h3>
                            <?php echo Form::label('meta_description', 'meta-description', ['class' => 'input-label']); ?>

                            <div class="input-group">
                                <?php echo Form::text('meta_description', isset($content->meta->meta_description) ? $content->meta->meta_description : null, ['class' => 'input', 'placeholder' => 'meta-description']); ?>

                            </div>
                        </div>


                        <div class="col-md-12">
                            <?php echo Form::label('meta_keywords', 'meta-keywords', ['class' => 'input-label']); ?>

                            <div class="input-group">
                                <?php echo Form::text('meta_keywords', isset($content->meta->meta_keywords) ? $content->meta->meta_keywords : null, ['class' => 'input', 'placeholder' => 'meta-keywords']); ?>

                            </div>
                        </div>

                        <div class="col-md-12">
                            <?php echo Form::label('meta_author', 'meta-author', ['class' => 'input-label']); ?>

                            <div class="input-group">
                                <?php echo Form::text('meta_author', isset($content->meta->meta_author) ? $content->meta->meta_author : 'vereinsleben.de', ['class' => 'input', 'placeholder' => 'meta-author']); ?>

                            </div>
                        </div>

                        <div class="col-md-12">
                            <?php echo Form::label('meta_language', 'meta-language', ['class' => 'input-label']); ?>

                            <div class="input-group">
                                <?php echo Form::text('meta_language', isset($content->meta->meta_langrage) ? $content->meta->meta_langrage : 'deutschland', ['class' => 'input', 'placeholder' => 'meta-language']); ?>

                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="input-group">
                                <?php echo Form::submit('Save content', array('class' => 'button button--light button--light-white button--center button--full-width')); ?>

                            </div>
                        </div>
                    </div>

                    <input type="hidden" id="admin-news-csrf" value="<?php echo e(csrf_token()); ?>">

                    <?php echo Form::close(); ?>

                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>