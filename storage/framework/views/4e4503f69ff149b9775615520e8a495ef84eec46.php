<?php $__env->startSection('title', 'vereinsleben.de - Verein des Monats - Der Wettbewerb für herausragende Vereinsarbeit'); ?>
<?php $__env->startSection('meta_description', 'Jetzt abstimmen und eurem Favoriten helfen, das Preispaket im Gesamtwert von 10.000 Euro zu gewinnen; oder bewerbt euch selbst als Verein des Monats auf www.vereinsleben.de.'); ?>

<?php $__env->startSection('content'); ?>

    <div id="vote-app">

        
        <?php echo $__env->make('vote.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

         
        <?php echo $__env->make('vote.bordered-section', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        
        <?php echo $__env->make('vote.reward', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        
        <?php echo $__env->make('vote.partials.popup', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        
        <?php echo $__env->make('vote.ranking', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('scripts'); ?>
<style>
    .partner-logo {
        margin-top: 0!important;
        background: none;
    }
    .section--vote-bg-new {
        margin-bottom: -75px;
        margin-top: -70px;
        position: relative;
    }
    @media (max-width: 1090px) {
        .section--vote-bg-new {
            margin-top: -50px;
        }
    }
    @media (max-width: 768px) {
        .section--vote-bg-new {
            margin-bottom: -95px;
            margin-top: -40px;
        }
    }
</style>
<script src="<?php echo e(asset(('js/vote.js'))); ?>"></script>
<script src="<?php echo e(asset(('js/OwlCarousel2/owl.carousel.js'))); ?>"></script>

<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58a427b07de68834"></script>

<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>