<?php $__env->startSection('content'); ?>

    <?php if(Auth::user()->isAdmin()): ?>
        <?php echo $__env->make('admin.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>
    <?php 
        use Vereinsleben\Spitzensport;
    ?>
    <section class="admin__overview">
        <div class="container">
            <div class="row admin__section">
                <div class="col-xs-12">
                    <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">Spitzensports</h1>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <a href="<?php echo e(url('admin/spitzensports/create')); ?>"
                           class="button button--icon button--full-width button--center button--light">
                            <span class="fa fa-plus"></span> Spitzensport hinzufügen
                        </a>
                    </div>
                </div>

                

            </div>
            <table id="admin-table" data-order="[[ 1, &quot;desc&quot; ]]" class="admin__table"
                   data-token="<?php echo e(csrf_token()); ?>">
                <thead>
                    <tr>
                        
                        <th style="width:5%">ID&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                        <th style="width:35%">Full Name&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                        <th style="width:10%">Autor&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                        <th style="width:15%">Erstellt&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                        <th style="width:10%">Publiziert</th>
                        <th style="width:10%">Optionen</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $spitzensports; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $spitzensportItem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            
                            <td><?php echo e($spitzensportItem->id); ?></td>
                            <td><?php echo e($spitzensportItem->full_name); ?></td>
                            <td><?php echo e($spitzensportItem->user['fullname'] != null || $spitzensportItem->user['fullname'] != "" ? $spitzensportItem->user['fullname'] : $spitzensportItem->user['username']); ?></td>
                            <td><?php echo e(date('Y/m/d', strtotime($spitzensportItem->created_at))); ?></td>
                            
                            <td>
                                <div class="onoffswitch">
                                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox"
                                           id="spitzensport-published-<?php echo e($key); ?>" <?php echo e(($spitzensportItem->state === Spitzensport::STATE_PUBLISHED) ? 'checked' : ''); ?>>
                                    <label data-toggle-published-spitzensport data-spitzensport-id="<?php echo e($spitzensportItem->id); ?>"
                                           class="onoffswitch-label" for="spitzensport-published-<?php echo e($key); ?>"></label>
                                </div>
                            </td>

                            <td>
                                <div class="admin__table-option">
                                    <a href="<?php echo e(route('spitzensports.edit', $spitzensportItem)); ?>"><span class="fa fa-edit"></span></a>
                                    <?php if(is_null($spitzensportItem->deleted_at) && Auth::user()->isAdmin()): ?>
                                        <a data-toggle-delete data-record-id="<?php echo e($spitzensportItem->id); ?>"
                                           data-delete-path="<?php echo e(route('admin.spitzensport.destroy')); ?>" href="#">
                                            <span class="fa fa-trash"></span>
                                        </a>
                                    <?php endif; ?>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>