<?php echo Form::model(
    $user,
    array(
        'method' => 'patch',
        'route' => ['user.social.update', $user->username],
        'class' => 'club-edit__form',
        'files' => 'true'
    )
); ?>

<?php $__currentLoopData = $socialLinkTypes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $label): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <div class="club-social__container">
        <label for="club_edits-ocial_links-<?php echo e($key); ?>" class="input-label">
            <i class="icon-space fa fa-<?php echo e(($key == 'googleplus') ? 'google-plus' : ($key == 'web' ? 'globe' : $key)); ?>"></i>
            <?php echo e($label); ?>

        </label>
        <input type="text" name="social_links[<?php echo e($key); ?>]"
               id="club_edits-ocial_links-<?php echo e($key); ?>"
               value="<?php echo e(isset($socialLinks[$key]) ? $socialLinks[$key]->url : ''); ?>"
               class="input">
    </div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

<div class="club-content__form-action-wrapper">
    <a href="#"
       class="club-content__form-cancel-action-button button button--grey button--center button--full-width edit-links">
        Abbrechen
    </a>
    <?php echo Form::submit('Speichern',
        array(
            'class' => 'button--save club-content__form-save-action-button button button--edit button--center button--full-width'
        )); ?>

</div>
<?php echo Form::close(); ?>