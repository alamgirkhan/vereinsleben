<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('admin.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <section class="admin__overview">
        <div class="container">
            <div class="row admin__section">
                <div class="col-xs-12">
                    <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">Content</h1>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <a href="<?php echo e(route('admin.content.add')); ?>"
                           class="button button--icon button--full-width button--center button--light">
                            <span class="fa fa-plus"></span> Content hinzufügen
                        </a>
                    </div>
                </div>
            </div>
            <table id="admin-table" class="admin__table" data-token="<?php echo e(csrf_token()); ?>">
                <thead>
                <tr>
                    <th>ID&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th>Titel&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th>Slug&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php $__currentLoopData = $contents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $content): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($content->id); ?></td>
                        <td><?php echo e($content->title); ?></td>
                        <td><?php echo e($content->slug); ?></td>
                        <td>
                            <div class="admin__table-option">
                                <a href="<?php echo e(route('admin.content.edit', $content->id)); ?>"><span
                                            class="fa fa-edit"></span></a>
                                <a onclick="return confirm('Möchtest du diese Aufzeichnung wirklich löschen?')"
                                   href="<?php echo e(route('admin.content.destroy', $content->id)); ?>">
                                    <span class="fa fa-trash"></span>
                                </a>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>