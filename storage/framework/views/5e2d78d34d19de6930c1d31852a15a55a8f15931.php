<aside>
    <nav>
        <ul class="club-detail__menu profile-tabs profile-menu-desk" data-pageable-menu>
            <li class="club-detail__menu-list-item active">
                <a class="club-detail__menu-list-link" href="#pinnwand">
                    <span><i class="fa fa-newspaper-o" aria-hidden="true"></i></span>Pinnwand
                </a>
            </li>

            <li class="club-detail__menu-list-item">
                <a class="club-detail__menu-list-link" href="#informationen">
                    <span><i class="fa fa-info" aria-hidden="true"></i></span>Informationen
                </a>
            </li>

            <li class="club-detail__menu-list-item">
                <a class="club-detail__menu-list-link" href="#ansprechpartner">
                    <span><i class="fa fa-user" aria-hidden="true"></i></span>Ansprechpartner
                </a>
            </li>

            <li class="club-detail__menu-list-item">
                <a class="club-detail__menu-list-link" href="#events">
                    <span><i class="fa fa-calendar" aria-hidden="true"></i></span>Events
                </a>
            </li>

            <li class="club-detail__menu-list-item">
                <a class="club-detail__menu-list-link" href="#netzwerk">
                    <span><i class="fa fa-users" aria-hidden="true"></i></span>Netzwerk
                </a>
            </li>

            
                
                    
                
            
        </ul>
        <ul class="club-detail__menu profile-tabs profile-menu-mob" data-pageable-menu>
            <li class="club-detail__menu-list-item active">
                <a class="club-detail__menu-list-link" href="#pinnwand">
                    <span><i class="fa fa-newspaper-o" aria-hidden="true" href="#pinnwand"></i></span>
                </a>
            </li>

            <li class="club-detail__menu-list-item">
                <a class="club-detail__menu-list-link" href="#informationen">
                    <span><i class="fa fa-info" aria-hidden="true" href="#informationen"></i></span>
                </a>
            </li>

            <li class="club-detail__menu-list-item">
                <a class="club-detail__menu-list-link" href="#ansprechpartner">
                    <span><i class="fa fa-user" aria-hidden="true" href="#ansprechpartner"></i></span>
                </a>
            </li>

            <li class="club-detail__menu-list-item">
                <a class="club-detail__menu-list-link" href="#events">
                    <span><i class="fa fa-calendar" aria-hidden="true" href="#events"></i></span>
                </a>
            </li>

            <li class="club-detail__menu-list-item">
                <a class="club-detail__menu-list-link" href="#netzwerk">
                    <span><i class="fa fa-users" aria-hidden="true" href="#netzwerk"></i></span>
                </a>
            </li>

            
                
                    
                
            
        </ul>
    </nav>
</aside>	