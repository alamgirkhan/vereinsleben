<?php $__env->startSection('title'); ?>
    <?php echo e($spitzensport->title); ?>


    <?php if(isset($spitzensport->sub_title)): ?>
        - <?php echo e($spitzensport->sub_title); ?>

    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('meta_description'); ?><?php if(isset($spitzensport->meta->meta_description )): ?><?php echo e($spitzensport->meta->meta_description); ?><?php endif; ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection('meta_language'); ?><?php if(isset($spitzensport->meta->meta_description )): ?><?php echo e($spitzensport->meta->meta_language); ?><?php endif; ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection('meta_author'); ?><?php if(isset($spitzensport->meta->meta_description )): ?><?php echo e($spitzensport->meta->meta_author); ?><?php endif; ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection('meta_keywords'); ?><?php if(isset($spitzensport->meta->meta_description )): ?><?php echo e($spitzensport->meta->meta_keywords); ?><?php endif; ?> <?php $__env->stopSection(); ?>

<?php $__env->startSection('fb-meta'); ?>
    <meta property="og:type"            content="article" />
    <meta property="og:url"             content="<?php echo e(url()->current()); ?>" />
    <meta property="og:title"           content="<?php echo e($spitzensport->title ? $spitzensport->title : '$spitzensport->title'); ?>" />
    <meta property="og:image"           content="<?php echo e(isset($spitzensport->main_image) && $spitzensport->main_image->originalFilename() != null ? asset($spitzensport->main_image->url('singleview')) : ''); ?>" />
    <meta property="og:description"    content="<?php echo e($spitzensport->meta->meta_description ? $spitzensport->meta->meta_description : ''); ?>" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <section class="news-detail">
        <div class="container">
            <div class="row">
                
                <div class="col-lg-9 col-md-8 col-sm-7 col-xs-12 news">
                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $spitzensport)): ?>
                        <a class="pull-right button button--center button--light button--icon"
                           href="<?php echo e(route('admin.news.edit', $spitzensport->id)); ?>">
                            &nbsp;<span class="fa fa-edit"></span> News bearbeiten&nbsp;
                        </a>
                    <?php endif; ?>

                    <h2><?php echo e($spitzensport->title); ?></h2>
                    <?php if(isset($spitzensport->sub_title)): ?>
                        <h3><?php echo e($spitzensport->sub_title); ?></h3>
                    <?php endif; ?>

                    <span class="label label-default sportnews">
					<a class="news-detail__category-link"
                       href="<?php echo e(route('news.category', $spitzensport->category->slug)); ?>">
                       
                       <span class="news-detail__subcategory">Spitzensport in Rheinland Pfalz</span>
						
					</a>
				    </span>

                        <span class="article-time"><i class="fa fa-clock-o" aria-hidden="true"></i>
				      Veröffentlicht am <?php echo e(strftime('%A, %d. %B %Y', strtotime($spitzensport->published_at))); ?>

				    </span>
                        <?php if($imageRes): ?>
                            <div class="news-lead-img">
                                <img class="news-detail__main-image"
                                     src="<?php echo e(asset($imageRes->picture->url('original'))); ?>"/>
                            </div>
                            <div class="row news-slide-caption">
                                <div class="col-lg-9 col-md-8 col-sm-7 col-xs-7 news-slide news-title-desk">
                                    <h2><?php echo e($imageRes->title); ?></h2>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 news-slide-arrow">
                                    <?php if($prevImgUrl): ?>
                                        <a href="<?php echo e($prevImgUrl); ?> ">
                                            <?php endif; ?>
                                            <span class="news-prev"><i class="fa fa-angle-left"
                                                                       aria-hidden="true"></i></span>
                                            <?php if($prevImgUrl): ?>
                                        </a>
                                    <?php endif; ?>
                                    <span class="pagingInfo"><?php echo e($seq); ?> von <?php echo e($spitzensport->images()->count()); ?></span>
                                    <?php if($nextImgUrl): ?>
                                        <a href="<?php echo e($nextImgUrl); ?> ">
                                            <?php endif; ?>
                                            <span class="news-next"><i class="fa fa-angle-right"
                                                                       aria-hidden="true"></i></span>
                                            <?php if($nextImgUrl): ?>
                                        </a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <div class="row news-slide-caption news-title-mob">
                            <div class="col-sm-12 col-xs-12 news-slide">
                                <h2><?php echo e($imageRes->title); ?></h2>
                            </div>
                        </div>

                        <p>
                            <?php echo $imageRes->description; ?>

                        </p>
                        <?php else: ?>
                            <?php if($spitzensport->main_image->originalFilename() !== null): ?>
                                <div class="news-lead-img">
                                    <img class="news-detail__main-image"
                                         src="<?php echo e(asset($spitzensport->main_image->url('singleview'))); ?>"
                                         alt="<?php if(isset($spitzensport->meta->meta_bild_alt )): ?><?php echo e($spitzensport->meta->meta_bild_alt); ?><?php endif; ?>">
                                </div>
                                <?php if($spitzensport->main_image_source !== null && $spitzensport->main_image_source !== ''): ?>
                                    <p class="news-lead-img-cpsn">
                                        Quelle: <?php echo e($spitzensport->main_image_source); ?>

                                    </p>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endif; ?>

                    <h4><?php echo e($spitzensport->content_teaser); ?></h4>
                    <p>
                        <?php echo $spitzensport->content; ?>

                    </p>

                        <?php if(count($spitzensport->tags) != 0): ?>
                            <div class="row">
                                <div class="col-lg-12 profile-post-tabs">

                                    <?php $__currentLoopData = $spitzensport->tags; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><a href="#"><?php echo e($tag->name); ?></a></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                </div>
                            </div>
                        <?php endif; ?>
                    <?php if(isset($campaign) && isset($campaign->banner_horizontal)): ?>
                        <div class="image-hor campaign-image" data-toggle="tooltip" title="<?php echo e($campaign->description); ?>">
                            <a target="_blank" href="<?php echo e($campaign->link); ?>"> <img src="/uploaded/campaign/<?php echo e($campaign->banner_horizontal); ?>" alt=""></a>
                        </div>
                    <?php endif; ?>
                </div>

                

                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="rhs-box">
                                <h2>Artikel Teilen</h2>
                                <ul class="social-icons">
                                    <li>
                                        
                                        
                                        
                                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo e(urlencode(route('news.detail', [$spitzensport->category_id, $spitzensport->slug]))); ?>"
                                           target="_blank"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li>
                                        
                                        
                                        
                                        <a href="https://twitter.com/intent/tweet?url=<?php echo e(urlencode(route('news.detail', [$spitzensport->category_id, $spitzensport->slug]))); ?>"
                                           target="_blank"><i class="fa fa-twitter"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    
                
                    
                    <?php if(strlen($spitzensport->steckbrief) > 10): ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="rhs-box">
                                <h2>steckbrief</h2>
                                <?php echo $spitzensport->steckbrief; ?>

                            </div>
                        </div>
                    </div>
                    <?php endif; ?>


                  <?php if(isset($campaign) && isset($campaign->banner_rechteck)): ?>
                    <div class="image-me campaign-image" data-toggle="tooltip" title="<?php echo e($campaign->description); ?>">
                      <a target="_blank" href="<?php echo e($campaign->link); ?>"> <img src="/uploaded/campaign/<?php echo e($campaign->banner_rechteck); ?>" alt=""></a>
                    </div>
                  <?php endif; ?>
                  <?php if(isset($campaign) && isset($campaign->banner_vertikal)): ?>
                    <div class="tablet-ads-vertical">
                        <div class="image-ver campaign-image" data-toggle="tooltip"
                             title="<?php echo e($campaign->description); ?>">
                            <a target="_blank" href="<?php echo e($campaign->link); ?>"> <img
                                        src="/uploaded/campaign/<?php echo e($campaign->banner_vertikal); ?>" alt=""></a>
                        </div>
                    </div>
                  <?php endif; ?>
                </div>

                
            </div>
            <?php if(isset($campaign) && isset($campaign->banner_vertikal)): ?>
                <div class="ads-vertical">
                    <div class="image-ver campaign-image" data-toggle="tooltip" title="<?php echo e($campaign->description); ?>">
                        <a target="_blank" href="<?php echo e($campaign->link); ?>"> <img src="/uploaded/campaign/<?php echo e($campaign->banner_vertikal); ?>" alt=""></a>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </section>
    <?php if(count($latest) > 0): ?>
        <?php echo $__env->make('news.partials.latest-teaser', [
        'news' => $latest,
        'start' => 1,
        'end' => 3,
        'inverted' => true,
        ], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>