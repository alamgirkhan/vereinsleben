<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 passwort">
        <?php if($errors->count() > 0): ?>
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        <?php endif; ?>

        <div class="row">
            <?php echo Form::open(['route' => 'user.password.update', 'method' => 'POST']); ?>

            

            <div class="col-md-12">
                <?php echo Form::label('current-password', 'Aktuelles Passwort', ['class' => 'input-label']); ?>

                <div class="input-group">
                    <?php echo Form::password('current-password', ['class' => 'input', 'placeholder' => 'Aktuelles Passwort', 'required' => 'required']); ?>                          </div>
            </div>
            <div class="col-md-12">
                <?php echo Form::label('new-password', 'Neues Passwort', ['class' => 'input-label']); ?>

                <div class="input-group">
                    <?php echo Form::password('new-password', ['class' => 'input', 'placeholder' => 'Neues Passwort', 'required' => 'required']); ?>                          </div>
            </div>
            <div class="col-md-12">
                <?php echo Form::label('new-password-confirmation', 'Neues Passwort bestätigen', ['class' => 'input-label']); ?>

                <div class="input-group">
                    <?php echo Form::password('new-password-confirmation', ['class' => 'input', 'placeholder' => 'Neues Passwort bestätigen', 'required' => 'required']); ?>                          </div>
            </div>
            <div class="col-md-12">
                <div class="input-group">
                    <?php echo Form::submit('Passwort ändern', array('class' => 'button button--light button--light-white button--center button--full-width')); ?>                          </div>
            </div>
            <?php echo Form::close(); ?>

        </div>
    </div>
</div>
