<?php $__env->startSection('title'); ?>
    <?php echo e($news->title); ?>


    <?php if(isset($news->sub_title)): ?>
        - <?php echo e($news->sub_title); ?>

    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('meta_description'); ?><?php if(isset($news->meta->meta_description )): ?><?php echo e($news->meta->meta_description); ?><?php endif; ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection('meta_language'); ?><?php if(isset($news->meta->meta_description )): ?><?php echo e($news->meta->meta_language); ?><?php endif; ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection('meta_author'); ?><?php if(isset($news->meta->meta_description )): ?><?php echo e($news->meta->meta_author); ?><?php endif; ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection('meta_keywords'); ?><?php if(isset($news->meta->meta_description )): ?><?php echo e($news->meta->meta_keywords); ?><?php endif; ?> <?php $__env->stopSection(); ?>

<?php $__env->startSection('fb-meta'); ?>
    <meta property="og:type"            content="article" />
    <meta property="og:url"             content="<?php echo e(url()->current()); ?>" />
    <meta property="og:title"           content="<?php echo e($news->title ? $news->title : '$news->title'); ?>" />
    <meta property="og:image"           content="<?php echo e(isset($news->main_image) && $news->main_image->originalFilename() != null ? asset($news->main_image->url('singleview')) : ''); ?>" />
    <meta property="og:description"    content="<?php echo e($news->meta->meta_description ? $news->meta->meta_description : ''); ?>" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <section class="news-detail">
        <div class="container">
            <div class="row">
                
                <div class="col-lg-9 col-md-8 col-sm-7 col-xs-12 news">
                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $news)): ?>
                        <a class="pull-right button button--center button--light button--icon"
                           href="<?php echo e(route('admin.news.edit', $news->id)); ?>">
                            &nbsp;<span class="fa fa-edit"></span> News bearbeiten&nbsp;
                        </a>
                    <?php endif; ?>

                    <h2><?php echo e($news->title); ?></h2>
                    <?php if(isset($news->sub_title)): ?>
                        <h3><?php echo e($news->sub_title); ?></h3>
                    <?php endif; ?>

                    <span class="label label-default sportnews">
					<a class="news-detail__category-link"
                       href="<?php echo e(route('news.category', $news->category->slug)); ?>">
						<span class="news-detail__category"><?php echo e($news->category->parentCategory->title); ?></span>
						> <span class="news-detail__subcategory"><?php echo e($news->category->title); ?></span>
					</a>
				    </span>

                        <span class="article-time"><i class="fa fa-clock-o" aria-hidden="true"></i>
				      Veröffentlicht am <?php echo e(strftime('%A, %d. %B %Y', strtotime($news->published_at))); ?>

				    </span>
                        <?php if($imageRes): ?>
                            <div class="news-lead-img">
                                <img class="news-detail__main-image"
                                     src="<?php echo e(asset($imageRes->picture->url('original'))); ?>"/>
                            </div>
                            <div class="row news-slide-caption">
                                <div class="col-lg-9 col-md-8 col-sm-7 col-xs-7 news-slide news-title-desk">
                                    <h2><?php echo e($imageRes->title); ?></h2>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 news-slide-arrow">
                                    <?php if($prevImgUrl): ?>
                                        <a href="<?php echo e($prevImgUrl); ?> ">
                                            <?php endif; ?>
                                            <span class="news-prev"><i class="fa fa-angle-left"
                                                                       aria-hidden="true"></i></span>
                                            <?php if($prevImgUrl): ?>
                                        </a>
                                    <?php endif; ?>
                                    <span class="pagingInfo"><?php echo e($seq); ?> von <?php echo e($news->images()->count()); ?></span>
                                    <?php if($nextImgUrl): ?>
                                        <a href="<?php echo e($nextImgUrl); ?> ">
                                            <?php endif; ?>
                                            <span class="news-next"><i class="fa fa-angle-right"
                                                                       aria-hidden="true"></i></span>
                                            <?php if($nextImgUrl): ?>
                                        </a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <div class="row news-slide-caption news-title-mob">
                            <div class="col-sm-12 col-xs-12 news-slide">
                                <h2><?php echo e($imageRes->title); ?></h2>
                            </div>
                        </div>

                        <p>
                            <?php echo $imageRes->description; ?>

                        </p>
                        <?php else: ?>
                            <?php if($news->main_image->originalFilename() !== null): ?>
                                <div class="news-lead-img">
                                    <img class="news-detail__main-image"
                                         src="<?php echo e(asset($news->main_image->url('singleview'))); ?>"
                                         alt="<?php if(isset($news->meta->meta_bild_alt )): ?><?php echo e($news->meta->meta_bild_alt); ?><?php endif; ?>">
                                </div>
                                <?php if($news->main_image_source !== null && $news->main_image_source !== ''): ?>
                                    <p class="news-lead-img-cpsn">
                                        Quelle: <?php echo e($news->main_image_source); ?>

                                    </p>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endif; ?>

                    <h4><?php echo e($news->content_teaser); ?></h4>
                    <p>
                        <?php echo $news->content; ?>

                    </p>

                        <?php if(count($news->tags) != 0): ?>
                            <div class="row">
                                <div class="col-lg-12 profile-post-tabs">

                                    <?php $__currentLoopData = $news->tags; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><a href="#"><?php echo e($tag->name); ?></a></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                </div>
                            </div>
                        <?php endif; ?>
                    <?php if(isset($campaign) && isset($campaign->banner_horizontal)): ?>
                        <div class="image-hor campaign-image" data-toggle="tooltip" title="<?php echo e($campaign->description); ?>">
                            <a target="_blank" href="<?php echo e($campaign->link); ?>"> <img src="/uploaded/campaign/<?php echo e($campaign->banner_horizontal); ?>" alt=""></a>
                        </div>
                    <?php endif; ?>
                </div>

                

                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="rhs-box">
                                <h2>Artikel Teilen</h2>
                                <ul class="social-icons">
                                    <li>
                                        
                                        
                                        
                                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo e(urlencode(route('news.detail', [$news->category_id, $news->slug]))); ?>"
                                           target="_blank"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li>
                                        
                                        
                                        
                                        <a href="https://twitter.com/intent/tweet?url=<?php echo e(urlencode(route('news.detail', [$news->category_id, $news->slug]))); ?>"
                                           target="_blank"><i class="fa fa-twitter"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <?php if($related->count() > 0): ?>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="rhs-box">
                                    <h2>Mehr zum Thema</h2>
                                    <aside class="news-detail__aside" data-pager
                                           data-pager-content='{"url": "<?php echo e(route('news.related', [$news->category->id, $news->id])); ?>", "category_id": "<?php echo e($news->category->id); ?>"}'
                                           data-pager-url="<?php echo e(route('news.related')); ?>"
                                           data-token="<?php echo e(csrf_token()); ?>">
                                        <div class="news-detail__aside-inner">
                                            <div data-pager-html>
                                                <div data-loader class="loader__wrapper loader__wrapper--centered">
                                                    <div class="loader loader--loading"></div>
                                                    <div class="loader loader--loading"></div>
                                                </div>
                                            </div>

                                            <div class="news-detail__aside-news-pagination">
                                                <span data-pager-previous
                                                      class="pager-control__previous fa fa-chevron-left"></span>
                                                
                                                
                                                
                                                <a class="news-detail__category-link"
                                                   href="<?php echo e(route('news.category')); ?>">
                                                    <span class="pager-control__label">Alle Artikel</span>
                                                </a>
                                                <span data-pager-next
                                                      class="pager-control__next fa fa-chevron-right"></span>
                                            </div>
                                        </div>
                                    </aside>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                  <?php if(isset($campaign) && isset($campaign->banner_rechteck)): ?>
                    <div class="image-me campaign-image" data-toggle="tooltip" title="<?php echo e($campaign->description); ?>">
                      <a target="_blank" href="<?php echo e($campaign->link); ?>"> <img src="/uploaded/campaign/<?php echo e($campaign->banner_rechteck); ?>" alt=""></a>
                    </div>
                  <?php endif; ?>
                  <?php if(isset($campaign) && isset($campaign->banner_vertikal)): ?>
                    <div class="tablet-ads-vertical">
                        <div class="image-ver campaign-image" data-toggle="tooltip"
                             title="<?php echo e($campaign->description); ?>">
                            <a target="_blank" href="<?php echo e($campaign->link); ?>"> <img
                                        src="/uploaded/campaign/<?php echo e($campaign->banner_vertikal); ?>" alt=""></a>
                        </div>
                    </div>
                  <?php endif; ?>
                </div>
            </div>
            <?php if(isset($campaign) && isset($campaign->banner_vertikal)): ?>
                <div class="ads-vertical">
                    <div class="image-ver campaign-image" data-toggle="tooltip" title="<?php echo e($campaign->description); ?>">
                        <a target="_blank" href="<?php echo e($campaign->link); ?>"> <img src="/uploaded/campaign/<?php echo e($campaign->banner_vertikal); ?>" alt=""></a>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </section>
    <?php if(count($latest) > 0): ?>
        <?php echo $__env->make('news.partials.latest-teaser', [
        'news' => $latest,
        'start' => 1,
        'end' => 3,
        'inverted' => true,
        ], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>