<popup>
    <div class="modal__content">
        <div class="modal__inner">
            <popup-vote-content authorized="<?php echo e(Auth::check()); ?>"
                                hasvoted="<?php echo e($hasVoted ? true : false); ?>"
                                remember="<?php echo e($remember == 0 ? false : true); ?>"></popup-vote-content>
        </div>
    </div>
</popup>