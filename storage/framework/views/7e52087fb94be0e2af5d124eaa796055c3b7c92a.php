<div class="hero-slider__wrapper <?php echo e((isset($condensed)) ? ' hero-slider__wrapper--condensed' : ''); ?>">
    <div class="hero-slider">
        <div class="hero-slider__image"
             style="background-image: url('<?php echo e(asset('static_images/vereinsleben_9.jpg')); ?>')"></div>
        <div class="hero-slider__image"
             style="background-image: url('<?php echo e(asset('static_images/vereinsleben_7.jpg')); ?>')"></div>
        <div class="hero-slider__image"
             style="background-image: url('<?php echo e(asset('static_images/vereinsleben.jpg')); ?>')"></div>
        <div class="hero-slider__image"
             style="background-image: url('<?php echo e(asset('static_images/vereinsleben_8.jpg')); ?>')"></div>
    </div>

    <?php if (! (isset($excludeContent))): ?>
        <?php echo $__env->make('components.slider-content', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>
</div>