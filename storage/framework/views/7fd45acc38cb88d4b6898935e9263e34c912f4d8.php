<div class="section section--vote-white vote-winner__content pd-0-mg-200 first-space">
    <div class="container">
        <?php if(count($previous_winners)): ?>
            <h2 class="hl hl--large hl--underlined hl--inverted hl--centered hl--upper">DIE GEWINNER</h2>
            <div class="owl-carousel owl-theme">
                <?php if(count($previous_winners)>0): ?>
                    <?php $__currentLoopData = $previous_winners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($item->voteNominees->first() && $item->voteNominees->first() != null): ?>
                        <div class="item section--white">
                            <div class="row">
                                <div class="col-sm-12 col-xs-12 col-lg-3 col-md-3">
                                    <div class="avatar">
                                      <img class="responsive-image" src="<?php echo e(asset($item->voteNominees->first()->club->avatar->url('singleView'))); ?>"
                                           onerror="this.src='<?php echo e(asset('default_images/club/default-avatars-singleView.jpg')); ?>'"
                                           alt="Verein des Monats">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-xs-12 col-lg-6 col-md-6">
                                    <div class="info">
                                      <h2 class="">Verein des Monats <?php echo e($month[DateTime::createFromFormat('Y-m-d', $item->period_start)->format('n')]); ?></h2>
                                      <h3 class="vote-winner__headline vote-winner__teamname"><?php echo e($item->voteNominees->first()->club->name); ?></h3>
                                      <p class="clearfix"><?php echo str_limit($item->voteNominees->first()->project_description, 400); ?></p>
                                    </div>
                                    <a href="<?php echo e(route('club.vote.nominee', ['clubSlug' => $item->voteNominees->first()->club->slug])); ?>"
                                     class="btn btn--default btn--inline btn--large btn--upper">Mehr über das Projekt</a>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
            </div>
            <div id="FirstWinnerOfYearTeaser" style="display:none;">
                <div class="text-block text-block--compressed text-block--thin text-block--smaller-spacing" style="text-align: center;">
                    <p style="text-align: center; font-family: Montserrat, sans-serif;">
                    Der Verein des Monats März wird am 01. April um 12:00 Uhr hier bekannt gegeben.
                </p></div>
            </div>
        <?php else: ?>
            <div id="FirstWinnerOfYearTeaser" style="display:none;">
                 <h2 class="hl hl--large hl--underlined hl--inverted hl--centered hl--upper">DIE GEWINNER</h2>
                <div class="text-block text-block--compressed text-block--thin text-block--smaller-spacing" style="text-align: center;">
                    <p style="text-align: center; font-family: Montserrat, sans-serif;">
                    Der Verein des Monats März wird am 01. April um 12:00 Uhr hier bekannt gegeben.
                </p></div>
            </div>
        <?php endif; ?>
    </div>
</div>

<?php $__env->startPush('scripts'); ?>
<script>
  $().ready(function(){
    var carousel = $(".owl-carousel");
    carousel.owlCarousel({
      loop:false,
      margin:10,
      nav:true,
      dot: false,
      navText: [
        '<img src="<?php echo e(asset('js/OwlCarousel2/assets/sort-left.png')); ?>" alt="Previous"/>',
        '<img src="<?php echo e(asset('js/OwlCarousel2/assets/sort-right.png')); ?>" alt="Next"/>'
      ],
      responsive:{
        0:{
          items:1
        },
        600:{
          items:1
        },
        1000:{
          items:1
        }
      }
    });
    carousel.trigger('to.owl.carousel', <?php echo e(count($previous_winners) - 1); ?>);
    var d1 = new Date().getTime();
    var d2 = new Date(2019, 3, 1, 12,00,00,0).getTime();
    if (d2 > d1)
    {
        $("#FirstWinnerOfYearTeaser").show();
    }

  });
</script>
<?php $__env->stopPush(); ?>