<header class="navi-wrapper">
    <?php if(isset($federalState) && $federalState->name == 'Rheinland-Pfalz'): ?>
        <section class="sponsor-ribbon__wrapper">
            <div class="sponsor-ribbon">
                <div class="container">
                    <div class="sponsor-ribbon__powered-wrapper">
                        <a class="sponsor-ribbon__link" href="<?php echo e(url('partner/lotto')); ?>">
                            <div class="sponsor-ribbon__powered-label">Mit freundlicher Unterstützung von</div>
                            <span class="sponsor-logo"><img src="<?php echo e(asset('static_images/powered-lotto-rlp.png')); ?>"
                                                            alt="powered by lotto"></span>
                        </a>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>

    <nav class="navi">
        <a class="navi__logo" href="<?php echo e(route('home')); ?>"> <img class="menu__logo"
                                                               src="<?php echo e(asset('static_images/logo-vereinsleben.png')); ?>"
                                                               alt="">
            <span class="logo-caption">
                <?php if(Auth::check()): ?>
                    <?php echo e(Auth::user()->desiredState() &&  Auth::user()->desiredState()->name ? Auth::user()->desiredState()->name : ''); ?>

                <?php else: ?> <?php echo e(isset($sessionState) ? $sessionState->name : ''); ?>

                <?php endif; ?>
            </span>
        </a>

        <div class="roundimg-top"></div>
        <div class="navi__mobile-button" id="mobile-menu"><i class="mobile-button__icon fa fa-bars"
                                                             aria-hidden="true"></i></div>
        <?php if(isset($federalState) && $federalState->name == 'Rheinland-Pfalz'): ?>
            <div class="lotto-mobile"><a href="<?php echo e(url('partner/lotto')); ?>"><img
                            src="<?php echo e(asset('static_images/lotto-logo.png')); ?>" alt=""/></a></div>
        <?php endif; ?>
        <div id="dl-menu" class="dl-menuwrapper">
            <button class="dl-trigger">Open Menu</button>
            <ul class="dl-menu">
                <li class="mobile-menu-search">
                    <?php echo Form::open(['route' => 'club.search', 'method' => 'GET', 'id' => 'search1', 'class' => '']); ?>

                    <div class="input-group">
                        <span class="input-group-addon"><img src="<?php echo e(asset('static_images/menu-search.png')); ?>"></span>
                        <?php echo Form::text('keyword1', isset($query['keyword']) ? $query['keyword'] : null, ['id' => 'keyword1', 'class' => 'form-control', 'placeholder' => 'STICHWORT']); ?>

                        <button type="submit" class="btn btn-default search-suchen">Suchen</button>
                    </div>
                    <?php echo Form::close(); ?>

                </li>
                <li class="dropdown state">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Bundesland wechseln<b class="caret"></b></a>
                    <ul class="dropdown-menu" style="display: none;">
                        <?php if(isset($federal_states)): ?>
                            <?php $__currentLoopData = $federal_states; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $state): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li>
                                    <a href="<?php echo e(route('change.federal_state', $state->id)); ?>"><?php echo e($state->name); ?></a>
                                </li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                    </ul>
                </li>
                <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i><?php if(Auth::check()): ?>MEIN VL <?php else: ?> Anmelden <?php endif; ?></a>
                        <ul class="dl-submenu">
                        <?php if(Auth::check()): ?>

                            <li>
                                <a class="-link<?php echo e(Request::is('user/profile/edit') ? ' -link--active' : ''); ?>"
                                   href="<?php echo e(route('user.detail', Auth::user()->username)); ?>#streaming"><i
                                            class="fa fa-user"></i> Newsfeed</a>
                            </li>
                            
                            
                            <?php if(Auth::user()->clubsWhereAdmin()->count() > 0): ?>
                                <li>
                                    <a class="-link<?php echo e(Request::is('chat') ? ' -link--active' : ''); ?>"
                                       href="<?php echo e(url('chat/index')); ?>"><i class="fa fa-comments-o"
                                                                         aria-hidden="true"></i>
                                        Messenger</a>

                                </li>
                            <?php else: ?>

                                <?php
                                    $friend = Auth::user()->getFriends()->first();

                                    if(is_null($friend)){

                                        $friendname = null;
                                    }
                                    else{
                                        $friendname = $friend->username;
                                    }
                                ?>
                                <?php if(isset($friendname)): ?>
                                    <li>
                                        <a class="-link<?php echo e(Request::is('chat/') ? ' -link--active' : ''); ?>"
                                           href="<?php echo e(route('chat', $friendname)); ?>"><i class="fa fa-comments-o"
                                                                                      aria-hidden="true"></i>
                                            Messenger</a>

                                    </li>
                                <?php endif; ?>
                            <?php endif; ?>
                            <li>
                                <a class="-link<?php echo e(Request::is('benutzer/'.Auth::user()->username) ? ' -link--active' : ''); ?>"
                                   href="<?php echo e(route('user.detail', Auth::user()->username)); ?>"><i
                                            class="fa fa-user"></i> Mein Profil</a>
                            </li>
                            <?php if(Auth::user()->isAdministratorOfClub()): ?>
                                <li>
                                    <a class="-link<?php echo e(Request::is('user/dashboard') ? ' -link--active' : ''); ?>"
                                       href="<?php echo e(route('user.dashboard')); ?>"><i
                                                class="fa fa-shield"></i> Vereinsverwaltung</a>
                                </li>
                            <?php endif; ?>
                            <?php if(Auth::check() && Auth::user()->isAdmin()): ?>
                                <li class="<?php echo e(Request::is('admin/*') ? ' -link--active' : ''); ?>">
                                    <a class="-link" href="<?php echo e(route('admin.index')); ?>"><i
                                                class="fa fa-unlock"></i> Administration</a>
                                </li>
                            <?php endif; ?>
                            <li>
                                <a class="-link<?php echo e(Request::is('user/profile/edit') ? ' -link--active' : ''); ?>"
                                   href="<?php echo e(route('user.detail', Auth::user()->username)); ?>#informationen"><i
                                            class="fa fa-gears"></i> Persönliche Daten</a>
                            </li>
                            <li>
                                <a class="-link<?php echo e(Request::is('user/profile/edit') ? ' -link--active' : ''); ?>"
                                   href="<?php echo e(route('user.detail', Auth::user()->username)); ?>#passwort"><i
                                            class="fa fa-cog"></i> Einstellungen</a>
                            </li>
                            <li><a class="-link" href="<?php echo e(url('/logout')); ?>"><i
                                            class="fa fa-sign-out"></i> Abmelden</a>
                    </li>
                <?php else: ?>
                            <li><a href="<?php echo e(route('login')); ?>">Anmelden</a></li>
                            <li><a href="<?php echo e(url('/login/facebook')); ?>">Mit Facebook anmelden</a></li>
                            <li><a href="<?php echo e(url('/register')); ?>">Registrieren</a></li>
                        <?php endif; ?>
                        </ul>
                    </li>
                <li>
                    <a href="<?php echo e(url('neuigkeiten')); ?>"><i class="fa fa-newspaper-o" aria-hidden="true"></i>NEWS</a>
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    

                </li>
                <li>
                    <a href="#"><i class="fa fa-diamond" aria-hidden="true"></i>VORTEILE</a>
                    <ul class="dl-submenu">
                        <h2>Verein des Monats</h2>
                        <li><a href="<?php echo e(url('verein-des-monats')); ?>">Aktuelle Aktion</a></li>
                        <li><a href="<?php echo e(url('verein-des-monats/ueber-den-wettbewerb')); ?>">Über den Wettbewerb</a></li>
                        <li><a href="<?php echo e(url('verein-des-monats/teilnehmen')); ?>">Jetzt bewerben</a></li>
                        <li>
                            <a href="<?php echo e(url('neuigkeiten/7/2018-02-19-expertentipps-wie-wird-man-verein-des-monats')); ?>">Tipps
                                für Sieger</a></li>
                        <li><a href="<?php echo e(url('verein-des-monats/teilnahmebedingungen')); ?>">Teilnahmebedingungen</a></li>

                        
                        
                            
                            
                        
                        
                            
                        

                        <h2>SERVICE</h2>
                        <li><a href="<?php echo e(url('/best-western-rewards')); ?>">Best Western Rewards</a></li>
                        <li>
                            <a href="<?php echo e(url('neuigkeiten/?searchtext=Sport+mit+Verantwortung&federal_state=0&news_type=')); ?>">Sport
                                mit Verantwortung</a></li>
                        <li><a href="<?php echo e(url('service/stellenboerse')); ?>">Trainerbörse</a></li>
                    </ul>
                </li>
                <li><a href="/podcast" class="noarrow">
                        <span class="podcast-icon"></span>PODCAST</a>
                </li>
                <li><a href="<?php echo e(url('/partner')); ?>" class="noarrow"><span class="partner-icon"></span>PARTNER</a></li>
            </ul>
        </div>

        <div class="menu-wrapper menu-wrapper--hover" id="menu-wrapper">
            <div class="col-lg-9 col-md-9 col-sm-9">
                <ul class="menu__list <?php if(isset($federalState) && $federalState->name != 'Rheinland-Pfalz'): ?> menu__list__top <?php endif; ?>" id="menu">
                    <li class="menu__item">
                        <a href="<?php echo e(url('neuigkeiten')); ?>" class="menu__item-link menu__item-link--shop">
                            <i class="fa fa-newspaper-o" aria-hidden="true"></i>News
                        </a>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        

                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        

                    </li>
                    <li class="menu__item mainsecondmenu">
                        <a href="#" data-submenu-trigger="true" class="menu__item-link menu__item-link--dropdown"><i
                                    class="fa fa-diamond" aria-hidden="true"></i>Vorteile</a>

                        
                        <div class="dropdown_customcolumns">
                            
                            
                            
                            <span class="downarrow"></span>

                            

                            <div class="col_2">
                                <h3>Verein des Monats</h3>
                                <ul>
                                    <li><a href="<?php echo e(url('verein-des-monats')); ?>">Aktuelle Aktion</a></li>
                                    <li><a href="<?php echo e(url('verein-des-monats/ueber-den-wettbewerb')); ?>">Über den
                                            Wettbewerb</a></li>
                                    <li><a href="<?php echo e(url('verein-des-monats/teilnehmen')); ?>">Jetzt bewerben</a></li>
                                    <li>
                                        <a href="<?php echo e(url('neuigkeiten/7/2018-02-19-expertentipps-wie-wird-man-verein-des-monats')); ?>">Tipps
                                            für Sieger</a></li>
                                    <li><a href="<?php echo e(url('verein-des-monats/teilnahmebedingungen')); ?>">Teilnahmebedingungen</a>
                                    </li>
                                </ul>
                            </div>
                            

                            
                            <div class="col_2 no-border">
                                
                                
                                    
                                        
                                    
                                        
                                    
                                    
                                
                                <h3>SERVICE</h3>
                                <ul>
                                    <li><a href="<?php echo e(url('/best-western-rewards')); ?>">Best Western Rewards</a></li>
                                    <li>
                                        <a href="<?php echo e(url('neuigkeiten/?searchtext=Sport+mit+Verantwortung&federal_state=0&news_type=')); ?>">Sport
                                            mit Verantwortung</a></li>
                                    <li><a href="<?php echo e(url('service/stellenboerse')); ?>">Trainerbörse</a></li>
                                </ul>
                            </div>
                        </div>

                    </li>
                    <li class="menu__item"><a class="menu__item-link menu__item-link--shop"
                                              href="<?php echo e(url('/podcast')); ?>" >
                            <span class="podcast-icon"></span>Podcast</a></li>
                    <li class="menu__item"><a class="menu__item-link menu__item-link--partner"
                                              href="<?php echo e(url('partner')); ?>"><span
                                    class="partner-icon"></span>Partner</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 second-menu">
                <div class="row second-menu-links">
                    <div class="col-lg-6 col-sm-6 search">
                        <a href="<?php echo e(url('suche/vereine')); ?>"><img src="<?php echo e(asset('static_images/top-search.png')); ?>"
                                                                  align="absmiddle"/>Suche</a>
                    </div>
                    <div class="col-lg-6 col-sm-6 exit">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown registration-box">
                                <a href="<?php if(Auth::check()): ?> <?php echo e(route('user.detail', Auth::user()->username)); ?> <?php else: ?> <?php echo e(url('login')); ?> <?php endif; ?>"
                                   class="dropdown-toggle" data-toggle="dropdown"><img
                                            src="<?php echo e(asset('static_images/exit.png')); ?>" alt=""
                                            align="absmiddle"/><?php if(Auth::check()): ?> Mein VL <?php else: ?> Anmelden <?php endif; ?></a>
                                <div class="dropdown-menu rgst-box">
                                    <div class="rgst-arrow"><img src="<?php echo e(asset('static_images/rgst-arrow.png')); ?>"/></div>
                                    <?php if(Auth::check()): ?>
                                        <div class="grad-box">
                                            <div class="rgst-box-inner">
                                                <ul>
                                                    <li>
                                                        <a class="-link<?php echo e(Request::is('user/profile/edit') ? ' -link--active' : ''); ?>"
                                                           href="<?php echo e(route('user.profile.stream')); ?>"><i
                                                                    class="fa fa-user"></i> Newsfeed</a>
                                                    </li>
                                                    <?php if(Auth::user()->clubsWhereAdmin()->count() > 0): ?>
                                                        <li>
                                                            <a class="-link<?php echo e(Request::is('chat') ? ' -link--active' : ''); ?>"
                                                               href="<?php echo e(url('chat/index')); ?>"><i
                                                                        class="fa fa-comments-o"
                                                                        aria-hidden="true"></i>
                                                                Messenger</a>

                                                        </li>
                                                    <?php else: ?>

                                                        <?php
                                                            $friend = Auth::user()->getFriends()->first();

                                                            if(is_null($friend)){

                                                                $friendname = null;
                                                            }
                                                            else{
                                                                $friendname = $friend->username;
                                                            }
                                                        ?>
                                                        <?php if(isset($friendname)): ?>
                                                            <li>
                                                                <a class="-link<?php echo e(Request::is('chat/') ? ' -link--active' : ''); ?>"
                                                                   href="<?php echo e(route('chat', $friendname)); ?>"><i
                                                                            class="fa fa-comments-o"
                                                                            aria-hidden="true"></i>
                                                                    Messenger</a>

                                                            </li>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                    <li>
                                                        <a class="-link<?php echo e(Request::is('benutzer/'.Auth::user()->username) ? ' -link--active' : ''); ?>"
                                                           href="<?php echo e(route('user.detail', Auth::user()->username)); ?>"><i
                                                                    class="fa fa-user"></i> Mein Profil</a>
                                                    </li>
                                                    <?php if(Auth::check() && Auth::user()->isAdministratorOfClub()): ?>
                                                        <li>
                                                            <a class="-link<?php echo e(Request::is('user/dashboard') ? ' -link--active' : ''); ?>"
                                                               href="<?php echo e(route('user.dashboard')); ?>"><i
                                                                        class="fa fa-shield"></i> Vereinsverwaltung</a>
                                                        </li>
                                                    <?php endif; ?>
                                                    <?php if(Auth::check() && Auth::user()->isAdmin()): ?>
                                                        <li class="<?php echo e(Request::is('admin/*') ? ' -link--active' : ''); ?>">
                                                            <a class="-link" href="<?php echo e(route('admin.index')); ?>"><i
                                                                        class="fa fa-unlock"></i> Administration</a>
                                                        </li>
                                                    <?php endif; ?>
                                                    <li>
                                                        <a class="-link<?php echo e(Request::is('benutzer/'.Auth::user()->username) ? ' -link--active' : ''); ?>"
                                                           href="<?php echo e(route('user.detail', Auth::user()->username)); ?>#informationen"><i
                                                                    class="fa fa-gears"></i> Persönliche Daten</a>
                                                    </li>
                                                    <li>
                                                        <a class="-link<?php echo e(Request::is('user/profile/edit') ? ' -link--active' : ''); ?>"
                                                           href="<?php echo e(route('user.detail', Auth::user()->username)); ?>#passwort"><i
                                                                    class="fa fa-cog"></i> Einstellungen</a>
                                                    </li>

                                                    <li><a class="-link" href="<?php echo e(url('/logout')); ?>"><i
                                                                    class="fa fa-sign-out"></i> Abmelden</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    <?php else: ?>
                                        <div class="grad-box">
                                            <form action="<?php echo e(url('/login')); ?>" method="post">
                                                <?php echo e(csrf_field()); ?>

                                                <div class="rgst-box-inner">
                                                    <h2>Melde dich hier An </h2>
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="input-group">
                                                            <span class="input-group-addon"><img
                                                                        src="<?php echo e(asset('static_images/email.png')); ?>"
                                                                        alt=""></span>
                                                                <input type="text" name="email" id="email"
                                                                       value="<?php echo e(old('email')); ?>" class="form-control"
                                                                       placeholder="E-Mail-Adresse">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="input-group">
                                                            <span class="input-group-addon"><img
                                                                        src="<?php echo e(asset('static_images/password.png')); ?>"
                                                                        alt=""></span>
                                                                <input type="password" name="password"
                                                                       class="form-control"
                                                                       placeholder="Passwort">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <button type="submit"
                                                            class="button button--center button--light button--light-white button--full-width button--icon">
                                                        <span class="fa fa-sign-in"></span> Anmelden
                                                    </button>
                                                    <p class="noch button button--center"><a

                                                                href="  <?php echo e(url('/login/facebook')); ?>">Mit Facebook

                                                            anmelden</a></p>
                                                    <p class="noch">Noch kein Konto?
                                                    </p>
                                                    <p class="jetzt">Jetzt persönliches</p>
                                                    <p class="anmelden profil-anlegen"><a href="<?php echo e(url('/register')); ?>">Profil
                                                            anlegen</a></p>
                                                </div>
                                            </form>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row top-drop-down">
                    <div class="col-lg-12">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown state">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Bundesland wechseln<b
                                            class="caret"></b></a>
                                <ul class="dropdown-menu" style="display: none;">
                                    <?php if(isset($federal_states)): ?>
                                        <?php $__currentLoopData = $federal_states; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $state): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li>
                                                <a href="<?php echo e(route('change.federal_state', $state->id)); ?>"><?php echo e($state->name); ?></a>
                                            </li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-sm-6 second-menu-afterscroll <?php if(isset($federalState) && $federalState->name == 'Rheinland-Pfalz'): ?> with-strip <?php endif; ?>">
            <div class="row second-menu-links">
                <div class="col-lg-4 col-md-6 col-sm-4 afterscroll-dropdown rlp">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown state">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                Bundesland
                                <b
                                        class="caret"></b></a>
                            <ul class="dropdown-menu" style="display: none;">
                                <?php if(isset($federal_states)): ?>
                                    <?php $__currentLoopData = $federal_states; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $state): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li>
                                            <a href="<?php echo e(route('change.federal_state', $state->id)); ?>"><?php echo e($state->name); ?></a>
                                        </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-3 col-sm-4 suche"><a href="<?php echo e(url('suche/vereine')); ?>">Suche</a></div>
                <div class="col-lg-4 col-md-3 col-sm-4 profil dropdown">
                    <?php if(Auth::check()): ?>
                        <a href="<?php echo e(route('user.detail', Auth::user()->username)); ?>" class="dropdown-toggle"
                           data-toggle="dropdown">Mein VL</a>
                        <div class="dropdown-menu rgst-box">
                            <div class="rgst-arrow"><img src="<?php echo e(asset('static_images/rgst-arrow.png')); ?>"/></div>
                            <div class="grad-box">
                                <div class="rgst-box-inner">
                                    <ul>
                                        <li>
                                            <a class="-link<?php echo e(Request::is('user/profile/edit') ? ' -link--active' : ''); ?>"
                                               href="<?php echo e(route('user.profile.stream')); ?>"><i
                                                        class="fa fa-user"></i> Newsfeed</a>
                                        </li>
                                        <?php if(Auth::user()->clubsWhereAdmin()->count() > 0): ?>
                                            <li>
                                                <a class="-link<?php echo e(Request::is('chat') ? ' -link--active' : ''); ?>"
                                                   href="<?php echo e(url('chat/index')); ?>"><i class="fa fa-comments-o"
                                                                                     aria-hidden="true"></i>
                                                    Messenger</a>

                                            </li>
                                        <?php else: ?>

                                            <?php
                                                $friend = Auth::user()->getFriends()->first();

                                                if(is_null($friend)){

                                                    $friendname = null;
                                                }
                                                else{
                                                    $friendname = $friend->username;
                                                }
                                            ?>
                                            <?php if(isset($friendname)): ?>
                                                <li>
                                                    <a class="-link<?php echo e(Request::is('chat/') ? ' -link--active' : ''); ?>"
                                                       href="<?php echo e(route('chat', $friendname)); ?>"><i
                                                                class="fa fa-comments-o"
                                                                aria-hidden="true"></i>
                                                        Messenger</a>

                                                </li>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                        <li>
                                            <a class="-link<?php echo e(Request::is('benutzer/'.Auth::user()->username) ? ' -link--active' : ''); ?>"
                                               href="<?php echo e(route('user.detail', Auth::user()->username)); ?>"><i
                                                        class="fa fa-user"></i> Mein Profil</a>
                                        </li>
                                        <?php if(Auth::user()->isAdministratorOfClub()): ?>
                                            <li>
                                                <a class="-link<?php echo e(Request::is('user/dashboard') ? ' -link--active' : ''); ?>"
                                                   href="<?php echo e(route('user.dashboard')); ?>"><i
                                                            class="fa fa-shield"></i> Vereinsverwaltung</a>
                                            </li>
                                        <?php endif; ?>
                                        <?php if(Auth::check() && Auth::user()->isAdmin()): ?>
                                            <li class="<?php echo e(Request::is('admin/*') ? ' -link--active' : ''); ?>">
                                                <a class="-link" href="<?php echo e(route('admin.index')); ?>"><i
                                                            class="fa fa-unlock"></i> Administration</a>
                                            </li>
                                        <?php endif; ?>
                                        <li>
                                            <a class="-link<?php echo e(Request::is('user/profile/edit') ? ' -link--active' : ''); ?>"
                                               href="<?php echo e(route('user.detail', Auth::user()->username)); ?>#informationen"><i
                                                        class="fa fa-gears"></i> Persönliche Daten</a>
                                        </li>
                                        <li>
                                            <a class="-link<?php echo e(Request::is('user/profile/edit') ? ' -link--active' : ''); ?>"
                                               href="<?php echo e(route('user.detail', Auth::user()->username)); ?>#passwort"><i
                                                        class="fa fa-cog"></i> Einstellungen</a>
                                        </li>
                                        <li><a class="-link" href="<?php echo e(url('/logout')); ?>"><i
                                                        class="fa fa-sign-out"></i> Abmelden</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    <?php else: ?>
                        <a href="<?php echo e(route('login')); ?>">Anmelden</a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </nav>
</header>