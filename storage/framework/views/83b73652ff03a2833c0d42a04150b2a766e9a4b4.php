<?php if($userSports->count() > 0): ?>
    <?php $__currentLoopData = $userSports; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $userSport): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php if($userSport->sport): ?>
        <h4 class="hl hl--small hl--inverted"><?php echo e(($userSport->sport) ? $userSport->sport->title : null); ?></h4>
        <div class="profile-rangeslider">
            <div class="profile-rangeslider__fill" style="width: <?php echo e($userSport->interest); ?>%"></div>
        </div>
        <div class="profile-rangeslider__scala">
            <div class="profile-rangeslider__scala-middle"></div>
            <div class="profile-rangeslider__scala-desc inter">interessiert</div>
            <div class="profile-rangeslider__scala-desc bege">begeistert</div>
            <div class="clear"></div>
        </div>
        <?php endif; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php else: ?>
    <p><em>Dieser Benutzer hat noch keine Interessen angegeben.</em></p>
<?php endif; ?>