<?php if($bandage->id === null): ?>
    <?php echo Form::model(
        $bandage,
        array(
            'method' => 'post',
            'route' => 'bandage.store',
            'class' => 'club-edit__form',
            'files' => 'true'
        )
    ); ?>

<?php else: ?>
    <?php echo Form::model(
        $bandage,
        array(
            'method' => 'patch',
            'route' => ['bandage.update', $bandage->slug, '#allgemeine-daten'],
            'class' => 'club-edit__form',
            'files' => 'true'
        )
    ); ?>

<?php endif; ?>
<div class="row">
    <div class="col-md-12">
        <?php echo Form::label('name', 'Verbandname *', ['class' => 'input-label']); ?>

        <div class="input-group">
            <?php echo Form::text('name', null, [
                    'required',
                    'class' => 'input',
                    'title' => 'Verbandname'
                ]); ?>

        </div>
    </div>


    <div class="col-md-6">
        <?php echo Form::label('type', 'Verbandsart', ['class' => 'input-label']); ?>

        <div class="input-group">
            <?php echo Form::select('type',
             collect(['' => 'Bitte wählen',
                    'dach' => 'Dachverband',
                    'fach'=>'Fachverband',
                    'weiter' => 'Weitere verbandsähnliche Organisationen'
                   ]),
             $bandage->type ? $bandage->type : '',
             [
                    'class' => 'select',
                    'title' => 'Verbansart'
              ]); ?>

        </div>
    </div>

    <div class="col-md-6">
        <?php echo Form::label('region', 'Landkreis', ['class' => 'input-label']); ?>

        <div class="input-group">
            <?php echo Form::select('region',
             collect(['' => 'Bitte wählen', 'national' => 'National', 'land' => 'Land', 'region' => 'Region']),
             $bandage->region ? $bandage->region : '',
             [
                    'class' => 'select',
                    'title' => 'Landkreis'
              ]); ?>

        </div>
    </div>
    <div class="col-md-9">
        <?php echo Form::label('street', 'Straße  *', ['class' => 'input-label']); ?>

        <div class="input-group">
            <?php echo Form::text('street', null, [
                'required',
                'class' => 'input',
                'placeholder' => 'Straße',
                'title' => 'Straße'
                ]); ?>

        </div>
    </div>
    <div class="col-md-3">
        <?php echo Form::label('house_number', 'Hausnummer', ['class' => 'input-label']); ?>

        <div class="input-group">
            <?php echo Form::text('house_number', null, [
                'class' => 'input',
                'placeholder' => 'Hausnummer',
                'title' => 'Hausnummer'
                ]); ?>

        </div>
    </div>
    <div class="col-md-3">
        <?php echo Form::label('zip', 'Postleitzahl', ['class' => 'input-label']); ?>

        <div class="input-group">
            <?php echo Form::text('zip', null, [
                    'class' => 'input',
                    'placeholder' => 'PLZ',
                    'title' => 'PLZ'
                    ]); ?>

        </div>

    </div>
    <div class="col-md-4">
        <?php echo Form::label('city', 'Stadt *', ['class' => 'input-label']); ?>

        <div class="input-group">
            <?php echo Form::text('city', null, [
                    'required',
                    'class' => 'input',
                    'placeholder' => 'Stadt',
                    'title' => 'Stadt'
                    ]); ?>

        </div>
    </div>

    <div class="col-md-5">
        <?php echo Form::label('email', 'E-Mail-Adresse', ['class' => 'input-label']); ?>

        <div class="input-group">
            <?php echo Form::text('email', null, [
                    'class' => 'input',
                    'placeholder' => 'E-Mail-Adresse',
                    'title' => 'E-Mail-Adresse'
                    ]); ?>

        </div>
    </div>
    <div class="col-md-6">
        <div class="club-detail__overview-item">
            <?php echo Form::label('parent-bandage', 'Übergeordneter Verband', ['class' => 'input-label']); ?>

            <div class="input-group">
                <?php echo Form::select('parent_bandage',
                 collect(['' => 'Bitte wählen'])->merge($bandages->pluck('name', 'slug')),
                 $bandage->parent ? $bandage->parent->slug : '',
                 [
                        'class' => 'select',
                        'title' => 'Vater'
                  ]); ?>

            </div>
        </div>
    </div>


    <div class="col-md-6">
        <div class="club-detail__overview-item">
            <?php echo Form::label('founded', 'Gegründet', ['class' => 'input-label']); ?>

            <div class="input-group">
                <?php echo Form::text('founded', null, [
                        'class' => 'input',
                        'placeholder' => 'z.B. '.date('Y'),
                        'title' => 'Gegründet'
                    ]); ?>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="club-detail__overview-item">
            <?php echo Form::label('sports', 'Sportart(en) auswählen *', ['class' => 'input-label']); ?>

            <div class="input-group">
                <select class="club-detail__select-sports" name="sports[]" multiple="multiple"
                        data-select-sports="true"
                        data-sports-url="<?php echo e(app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('api.sports')); ?>"
                        required="required">
                    <?php if(isset($bandage->sports)): ?>

                        <?php $__currentLoopData = $bandage->sports; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sport): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($sport->id); ?>"
                                    selected="selected"><?php echo e($sport->title); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    <?php endif; ?>
                </select>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <p class="input-label">
            * Pflichtfelder
        </p>
    </div>
</div>

<div class="club-content__form-action-wrapper">
    <a href="<?php echo e(route('bandage.index')); ?>"
       class="club-content__form-cancel-action-button button button--grey button--center button--full-width">
        Abbrechen
    </a>
    <?php echo Form::submit('Speichern',
        array(
            'class' => 'button--light club-content__form-save-action-button button button--edit button--center button--full-width'
        )); ?>

</div>
<?php echo Form::close(); ?>

