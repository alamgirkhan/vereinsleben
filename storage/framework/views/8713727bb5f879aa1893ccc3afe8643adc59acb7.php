<div class="row" id="emailupdate" style="display:none;">
    <div class="col-lg-12 col-md-12 col-sn-12 col-xs-12 konto">
        <?php if($errors->count() > 0): ?>
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        <?php endif; ?>
        <div class="row">
            <?php echo Form::model($user, ['route' => 'user.email.update', 'method' => 'POST']); ?>


            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php echo Form::label('updateEmail', 'E-Mail', ['class' => 'input-label']); ?>

                <div class="input-group">
                    <?php echo Form::text('new_email', null, ['required', 'class' => 'input', 'placeholder' => 'E-Mail']); ?>

                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="input-group">
                    <?php echo Form::submit('Speichern', array('class' => 'button button--light button--light-white button--center button--full-width')); ?>

                </div>
            </div>
            <?php echo Form::close(); ?>

        </div>
    </div>
</div>