<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('components.slider',
    ['condensed' => true,
     'content' => [
            'icon' =>'user-plus',
            'headline' => 'Registrieren',
            'caption' => 'Mitglied werden. Vereine entdecken.'
    ]], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <section class="auth">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <?php if(isset($success)): ?>
                        <h1>Hallo <?php echo e($username); ?>!</h1>
                        <h2>Willkommen bei vereinsleben.de</h2>
                    <?php else: ?>
                        <form action="<?php echo e(url('/register')); ?>" method="POST" id="form_register" class="mainrgstr">
                            <div class="row">

                                <div class="col-md-12">
                                    <label class="input-label" for="email">E-Mail *</label>
                                    <div class="input-group">
                                        <span class="fa fa-envelope input-icon"></span>
                                        <input placeholder="E-Mail" class="input" type="email" name="email" id="email"
                                               value="<?php echo e(old('email')); ?>" required="required">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label class="input-label" for="password">Passwort (min. 6 Zeichen) *</label>
                                    <div class="input-group">
                                        <span class="fa fa-lock input-icon"></span>
                                        <input placeholder="Passwort" class="input" type="password" name="password"
                                               id="password" required="required">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label class="input-label" for="password_confirmation">Passwort wiederholen *</label>
                                    <div class="input-group">
                                        <span class="fa fa-lock input-icon"></span>
                                        <input placeholder="Passwort wiederholen" class="input" type="password"
                                               name="password_confirmation"
                                               id="password_confirmation"
                                               required="required">
                                    </div>
                                </div>

                                <div class="col-md-12 rgs-slt-area">
                                    <div class="input-group">
                                        <input type="hidden" name="newsletter" value="0">
                                        <input class="input-checkbox" type="checkbox" name="newsletter" id="newsletter"
                                               value="1" <?php echo e(old('newsletter', 0) ? 'checked="checked"' : ''); ?>>
                                        <label class="input-checkbox__label" for="newsletter">Ich möchte den
                                            kostenlosen Newsletter erhalten
                                            und erkläre hierfür meine ausdrückliche Einwilligung. Weitere Informationen
                                            zu Protokollierung
                                            der Anmeldung, Versand des Newsletters und Abmeldemöglichkeit sind in den
                                            <a class="input-checkbox__label-link"
                                               href="<?php echo e(url('/content/datenschutz')); ?>">Datenschutzhinweisen</a> zu
                                            finden.</label>
                                    </div>
                                </div>

                                <div class="col-md-12 rgs-slt-area">
                                    <div class="input-group">
                                        <input class="input-checkbox" required type="checkbox" name="terms" id="terms" value="1"
                                               <?php if(old('terms', false) === '1'): ?> checked="checked" <?php endif; ?>>
                                        <label class="input-checkbox__label" for="terms" id="termserror">Ich habe die
                                            <a class="input-checkbox__label-link" href="<?php echo e(url('/content/agbs')); ?>">
                                                AGB gelesen</a> und bin mit ihnen einverstanden. *</label>
                                    </div>
                                </div>

                                <div class="col-md-12 rgs-slt-area">
                                    <div class="input-group">
                                        <input class="input-checkbox" required="required" type="checkbox" name="privacy"
                                               id="privacy" value="1"
                                               <?php if(old('privacy', false) === '1'): ?> checked="checked" <?php endif; ?>>
                                        <label class="input-checkbox__label" for="privacy" id="privacyerror">Ich
                                            erkläre meine Einwilligung darin,
                                            dass meine Daten entsprechend dem in den
                                            <a class="input-checkbox__label-link"
                                               href="<?php echo e(url('/content/datenschutz')); ?>">
                                                Datenschutzhinweisen</a> genannten Umfang zum Zweck
                                            der Erstellung eines persönlichen Benutzerkontos verarbeitet und gespeichert
                                            werden.
                                            Das Benutzerkonto und alle damit verbundenen personenbezogenen Daten können
                                            jederzeit von mir gelöscht werden.
                                            Weitere Informationen dazu sind in den
                                            <a class="input-checkbox__label-link"
                                               href="<?php echo e(url('/content/datenschutz')); ?>">Datenschutzhinweisen</a> zu
                                            finden. *
                                        </label>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="input-group">
                                        <?php echo Recaptcha::render(); ?>

                                    </div>
                                </div>

                                <?php echo e(csrf_field()); ?>


                                <div class="col-md-12">
                                    <p class="pull-right">
                                        * Pflichtfeld<br /><br />
                                    </p>
                                </div>

                                <div class="col-md-12">
                                    <input type="submit"
                                           class="button button--center button--full-width button--light button--light-white"
                                           value="Registrieren"
                                           id="register">
                                </div>
                            </div>
                        </form>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>