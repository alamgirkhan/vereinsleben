<div class="profile-statusposts">
    <div class="profile-statusposts__container" data-user-post-container>
        <?php
        $newPost = new \Vereinsleben\Post;
        if (Auth::check()){
            $newPost->user_id = $user->id;
        }
        ?>
        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('store', $newPost)): ?>
            <?php echo $__env->make('user.profile.partials.post.edit.form', ['user' => $user], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php endif; ?>

        <?php $__empty_1 = true; $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
            <?php echo $__env->make('user.profile.partials.post.single', ['user' => $user, 'post' => $post], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
            <?php echo $__env->make('user.profile.partials.post.empty', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php endif; ?>
    </div>
</div>