<?php $__env->startSection('content'); ?>
    
    <?php echo $__env->make('admin.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <section class="admin__overview" data-token="<?php echo e(csrf_token()); ?>"
             data-mapping-url="<?php echo e(app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('api.club.list')); ?>">
        <div class="container">
            <div class="row admin__section">
                <div class="col-xs-12">
                    <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">VERBÄNDE</h1>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <a href="<?php echo e(route('bandage.create')); ?>"
                           class="button button--icon button--full-width button--center button--light">
                            <span class="fa fa-plus"></span> Lege deinen Verband an
                        </a>
                    </div>
                </div>
            </div>
            <?php if($bandages): ?>
                <table id="admin-bandage" class="admin__table" data-token="<?php echo e(csrf_token()); ?>">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Sportart</th>
                        <th>Bereich</th>
                        <th>Optionen</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $bandages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bandage): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td onclick='window.open("<?php echo e(route('bandage.detail', ['bandage' => $bandage->slug])); ?>")'>
                                <?php echo e($bandage->id); ?>

                            </td>
                            <td><?php echo e($bandage->name); ?></td>
                            <td>
                                <?php if(isset($bandage->sports)): ?>
                                    <?php $__currentLoopData = $bandage->sports; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $sport): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <span>
                                            <?php echo e($sport->title); ?><?php echo e($key != (count($bandage->sports) -1) ? ', ' : ''); ?>

                                        </span>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                <?php endif; ?>
                            </td>
                            <td><?php echo e($bandage->region); ?></td>
                            <td>
                                <div class="admin__table-option">
                                    <a href="<?php echo e(route('bandage.admin.show', ['slug' => $bandage->slug])); ?>"
                                       target="_blank" data-toggle="tooltip" data-placement="top"
                                       title="Bearbeiten"><span class="fa fa-edit"></span></a>
                                    <?php if($bandage->published == 0): ?>
                                        <a href="<?php echo e(route('admin.bandage.deactivate', ['id' => $bandage->id])); ?>"
                                           data-toggle="tooltip" data-placement="top" title="Aktiv/Inaktiv">
                                            <span class="fa fa-globe" style="color:red"></span>
                                        </a>
                                    <?php endif; ?>
                                    <?php if($bandage->published == 1): ?>
                                        <a href="<?php echo e(route('admin.bandage.deactivate', ['id' => $bandage->id])); ?>"
                                           data-toggle="tooltip" data-placement="top" title="Aktiv/Inaktiv">
                                            <span class="fa fa-globe" style="color:green"></span>
                                        </a>
                                    <?php endif; ?>
                                    <?php if(is_null($bandage->deleted_at)): ?>
                                        <?php echo Form::model(
                                             $bandage,
                                             array(
                                                 'method' => 'delete',
                                                 'route' => ['bandage.delete', $bandage->slug, '#allgemeine-daten'],
                                                 'class' => 'club-edit__form',
                                                 'files' => 'false'
                                             )
                                         ); ?>


                                        <button onclick="return confirm('Möchten Sie diesen Verband wirklich löschen?')" type="submit" class="btn-del">
                                            <i class="fa fa-trash"></i>
                                        </button>

                                        <?php echo Form::close(); ?>

                                    <?php endif; ?>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            <?php endif; ?>
        </div>
    </section>
    <?php $__env->startPush('scripts'); ?>
        <script>
            $(document).ready(function () {
              $('#admin-bandage').DataTable({
                    dom         : 'Blfrtip',
                    "lengthMenu": [50, 100, 250],
                    buttons     : [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5'
                    ],

                    language: {
                        url: '/js/vendor/i18n/dataTables.german.json'
                    }
                }).on('page.dt', function() {
                    $('html, body').animate({
                        scrollTop: $(".dataTables_wrapper").offset().top
                    }, 'slow');

                    $('thead tr th:first-child').focus().blur();
                });
            })
        </script>


        <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
        <script src="http://legacy.datatables.net/release-datatables/extras/TableTools/media/js/TableTools.js"></script>

    <?php $__env->stopPush(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>