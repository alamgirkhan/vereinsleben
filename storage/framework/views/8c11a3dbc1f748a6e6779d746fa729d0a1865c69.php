<!DOCTYPE html>
<html>
<head>
    <title><?php echo $__env->yieldContent('title', 'vereinsleben.de - wir. leben. sport.'); ?></title>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta charset="utf-8">
    <meta name="description" content="<?php echo $__env->yieldContent('meta_description', ''); ?>">
    <meta name="keywords" content="<?php echo $__env->yieldContent('meta_keywords', ''); ?>">
    <meta name="author" content="<?php echo $__env->yieldContent('meta_author', ''); ?>">
    <meta name="language" content="<?php echo $__env->yieldContent('meta_language', ''); ?>">

    <meta property="og:title" content="<?php echo $__env->yieldContent('title', 'vereinsleben.de - wir. leben. sport.'); ?>"/>
    <meta property="og:description" content="<?php echo $__env->yieldContent('meta_description', ''); ?>">

    <link rel="shortcut icon" href="<?php echo e(asset('favicon.ico')); ?>" type="image/x-icon">
    <link rel="icon" href="<?php echo e(asset('favicon.ico')); ?>" type="image/x-icon">
    <link rel="apple-touch-icon" href="<?php echo e(asset('apple-touch-icon.png')); ?>">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo e(asset('apple-touch-icon-57x57.png')); ?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo e(asset('apple-touch-icon-72x72.png')); ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo e(asset('apple-touch-icon-76x76.png')); ?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo e(asset('apple-touch-icon-114x114.png')); ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo e(asset('apple-touch-icon-120x120.png')); ?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo e(asset('apple-touch-icon-144x144.png')); ?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo e(asset('apple-touch-icon-152x152.png')); ?>">


    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <meta property="fb:app_id"          content="<?php echo e(env('FACEBOOK_CLIENT_ID')); ?>" />
    <?php echo $__env->yieldContent('fb-meta'); ?>

    <script src="<?php echo e(asset('js/vendor-head.js')); ?>"></script>


    <script type="text/javascript">
        window.cookieconsent_options = {
            "message"  :"Cookies helfen uns bei der Bereitstellung unserer Dienste. Durch die Nutzung unserer Webseite erklärst du dich mit dem Einsatz von Cookies einverstanden.",
            "dismiss"  :"Bestätigen",
            "learnMore":"Weitere Informationen.",
            "link"     :"https://www.vereinsleben.de/content/datenschutz/",
            "theme"    : "light-bottom"
        };
    </script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
    <script type="text/javascript" src="<?php echo e(asset('js/jcookie.js')); ?>"></script>
    <script src="https://www.instagram.com/embed.js"></script>

    <link rel="stylesheet" href="<?php echo e(asset('js/OwlCarousel2/assets/owl.carousel.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('js/OwlCarousel2/assets/owl.theme.default.min.css')); ?>">
    <?php if(app()->environment() === 'production'): ?>
        <link rel="stylesheet" href="<?php echo e(asset(elixir('css/vendor.css'))); ?>">
        <link rel="stylesheet" href="<?php echo e(asset(elixir('css/app.css'))); ?>">

    <?php else: ?>
        <link rel="stylesheet" href="<?php echo e(asset('css/vendor.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('css/app.css')); ?>">

    <?php endif; ?>
</head>
<body id="body">
<?php echo $__env->make('layouts.components.to-top', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.components.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="<?php if(isset($federalState) && $federalState->name == 'Rheinland-Pfalz'): ?> page <?php else: ?> pagetop <?php endif; ?>">
    
    <?php $__currentLoopData = ['warning', 'error', 'success', 'info']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $level): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php if(Session::has('flash-'.$level)): ?>
            <div class="flash-message flash-message--<?php echo e($level); ?>">
                <?php if(is_array(Session::get('flash-'.$level))): ?>
                    <?php $__currentLoopData = Session::get('flash-'.$level); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li><?php echo e($error); ?></li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php else: ?>
                    <?php echo Session::get('flash-'.$level); ?>

                <?php endif; ?>
            </div>
        <?php endif; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    

    <?php if(isset($errors) && $errors->count() > 0): ?>
        <div class="flash-message flash-message--error">
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
    <?php endif; ?>

    <?php echo $__env->yieldContent('content'); ?>
    <?php echo $__env->make('layouts.components.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</div>

<?php if(app()->environment() === 'production'): ?>
    <?php echo $__env->make('layouts.utils.google.analytics', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('layouts.utils.google.tag-manager-noscript', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script src="<?php echo e(asset(elixir('js/vendor.js'))); ?>"></script>
    <?php echo $__env->yieldContent('js-additional-vendor'); ?>
    <script src="<?php echo e(asset(elixir('js/main.js'))); ?>"></script>
<?php else: ?>
    <script src="<?php echo e(asset(('js/vendor.js'))); ?>"></script>
    <?php echo $__env->yieldContent('js-additional-vendor'); ?>
    <script src="<?php echo e(asset(('js/main.js'))); ?>"></script>
<?php endif; ?>
<style>
    .slide {
        position: relative;
    }

    .slick-slide {
        width: 100%;
    }
</style>
<script>
    $(function () {
        $('#dl-menu').dlmenu();
        $("#userlinks").hide();
    });

    $(function () {
        $(".dropdown").hover(
            function () {
                $('.dropdown-menu', this).stop(true, true).fadeIn("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");
            },
            function () {
                $('.dropdown-menu', this).stop(true, true).fadeOut("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");
            });
    });

    function friendshipEvent(senderUser, recipientUser, type, id) {
        var url = '<?php echo e(url('/benutzer')); ?>';
        $.get(url + '/' + senderUser + '/friendship_create/' + recipientUser + '?relation=' + type + '&ajax=1', function (data) {
            if (type == 'accept') {
                $("#acceptuser_" + id).hide();
                $("#denyuser_" + id).hide();
                $("#removeuser_" + id).show();
                $("#bloackuser_" + id).show();
            } else {
                $("#user_" + id).hide();
            }

        });
    }

    function sendRequest(senderUser, recipientUser, type, id) {
        var url = '<?php echo e(url('/benutzer')); ?>';
        $.get(url + '/' + senderUser + '/friendship_create/' + recipientUser + '?relation=' + type + '&ajax=1', function (data) {
            $("#" + type + "_" + id).empty().html(data);
        });
    }

    function Unfriendship(senderUser, recipientUser, type, id) {
        var url = '<?php echo e(url('/benutzer')); ?>';
        $.get(url + '/' + senderUser + '/friendship_edit/' + recipientUser + '?relation=' + type + '&ajax=1', function (data) {
            $("#user_" + id).hide();
        });
    }

    function updateProfile(current, save) {
        $('#save').val(save);
        $('#current').val(current);
        /*var url = '<?php echo e(url('/user')); ?>';*/
        var url = '/user';

        var result = $.post( url + '/profile/wizard/update', $('#form_wizard').serialize());
         result.done(function( data ) {
                    $("#wizard_" + current).hide();
                    $("#wizard_" + data).show();
          });

    }

    function ajaxLoad(url, type) {
        $('#' + type + '_more_result').remove();
        $('#' + type + '_loader').show();
        $.get(url, function (data) {
            $('#' + type + '_loader').hide();
            $('#' + type + '_result').append(data['data']['rendered']);
        });
    }

    function goToByScroll(id) {
        id = id.replace("link", "");
        $('html,body').animate({
                scrollTop: $("#" + id).offset().top
            },
            'slow');
    }

    $('ul.tabs li').click(function () {
        var tab_id = $(this).attr('data-tab');

        $('ul.tabs li').removeClass('current');
        $('.tab-content').removeClass('current');

        $(this).addClass('current');
        $("#" + tab_id).addClass('current');
    })

    $("#checkAll").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
    });

    $("#checkAllSpitzensportStates").change(function () {
        $(".spitzensportStatesSelection input:checkbox").prop('checked', $(this).prop("checked"));
    });

    $(".edit-email").click(function () {
        $("#emailupdate").slideToggle("slow", function () {
        });
    });

    $(".edit-links").click(function () {
        $("#userlinks").slideToggle("slow", function () {
        });
    });


    $('.modal__close').click(function () {
        $("div").remove(".modal__container");
    })

    $("#register").click(function () {
        if($('#terms').is(':checked'))
            $('#termserror').css('color', '');
        else
            $('#termserror').css('color', 'red');
    });

    $("#register").click(function () {
        if ($('#privacy').is(':checked'))
            $('#privacyerror').css('color', '');
        else
            $('#privacyerror').css('color', 'red');
    });

    <?php if(Auth::check()): ?>
        $("#register-btn").hide();

    <?php endif; ?>

    function nextStep(current, save) {

        if (current == 'thankyou') {
            location.reload(true);
            return;
        }
        if (current == 'close') {
            Cookies.set("hideProfilVervollstaendigung","1",{ expires: 1 });
            location.reload(true);
            return;
        }

        if (save == '1' && current == 'state') {
            if ($('#wstate').val() == '') {
                $('#wstate').addClass('error');
                return false;
            }
        }

        if (save == '1' && current == 'anrede') {
            $('#wgender').removeClass('error');
            $('#wfirstname').removeClass('error');
            $('#wlastname').removeClass('error');

            var firstname = $('#wfirstname').val();
            var lastname = $('#wlastname').val();


            if ($('#wgender').val() == '') {
                $('#wgender').addClass('error');
                return false;
            }

            let re_name = new RegExp('^[a-zA-ZäöüÄÖÜß\\-. ]+$');

            if (!re_name.exec(firstname)) {
                $('#wfirstname').addClass('error');
                console.log("Error Vorname");
                return false;
            }
            if (!re_name.exec(lastname)) {
                $('#wlastname').addClass('error');
                console.log("Error Nachname");
                return false;
            }

        }

        if (save == '1' && current == 'dob') {
            $('#wbirthday-day').removeClass('error');
            $('#wbirthday-month').removeClass('error');
            $('#wbirthday-year').removeClass('error');

            if ($('#wbirthday-day').val() == '') {
                $('#wbirthday-day').addClass('error');
                return false;
            }
            if ($('#wbirthday-month').val() == '') {
                $('#wbirthday-month').addClass('error');
                return false;
            }
            if ($('#wbirthday-year').val() == '') {
                $('#wbirthday-year').addClass('error');
                return false;
            }
        }

        if (save == '1' && current == 'postleitzahl') {
            $('#wzip').removeClass('error');
            $('#wcity').removeClass('error');

            let re_zip = new RegExp('^([0-9]{4,5})$');
            let re_city = new RegExp('^[a-zA-ZäöüÄÖÜß\\-. ]+$');


            var zip = $('#wzip').val();
            var city = $('#wcity').val();

            if (!re_zip.exec(zip)) {
                $('#wzip').addClass('error');
                return false;
            }
            if (!re_city.exec(city)) {
                $('#wcity').addClass('error');
                return false;
            }
        }
        updateProfile(current, save);
    }

    function nextStepClub(current, next, save) {

        if (current == 'thankyou') {
            $("#form_clubwizard").submit();
        }
        if (current == 'close') {
            location.reload();
            return;
        }

        var name = $.trim($('#wcname').val());
        var sport = $.trim($('#wcsport').val());
        var timeline_begin_year = $.trim($('#wctimeline_begin_year').val());
        var timeline_end_year = $.trim($('#wctimeline_end_year').val());
        var timeline_begin_month = $.trim($('#wctimeline_begin_month').val());
        var timeline_end_month = $.trim($('#wctimeline_end_month').val());
        var career_type = $.trim($('#wccareer_type').val());
        var street = $.trim($('#wcstreet').val());
        var house_number = $.trim($('#wchouse_number').val());
        var zip = $.trim($('#wczip').val());
        var city = $.trim($('#wccity').val());
        var web = $.trim($('#wcweb').val());


        if (save == '1' && current == 'state') {

            $('#wcname').removeClass('error');
            $('#wcsport').removeClass('error');
            if ($('#wcname').val() == '') {
                $('#wcname').addClass('error');
                return false;
            }
            if ($('#wcsport').val() == '') {
                $('#wcsport').addClass('error');
                return false;
            }
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }

        if (save == '1' && current == 'dob') {

            $('#wctimeline_begin_month').removeClass('error');
            if ($('#wctimeline_begin_month').val() == '') {
                $('#wctimeline_begin_month').addClass('error');
                return false;
            }
            $('#wctimeline_begin_year').removeClass('error');
            if ($('#wctimeline_begin_year').val() == '') {
                $('#wctimeline_begin_year').addClass('error');
                return false;
            }
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();

        }
        else if (save == '0' && current == 'dob') {
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }

        if (save == '1' && current == 'street') {
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }
        else if (save == '0' && current == 'street') {
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }

        if (save == '1' && current == 'postleitzahl') {

            $('#wccity').removeClass('error');
            $('#wczip').removeClass('error');

            if (!$.isNumeric(zip) && zip != '') {
                $('#wczip').addClass('error');
                return false;
            }

            if (zip.length != 5) {
                $('#wczip').addClass('error');
                return false;
            }

            if (!city.match('^[A-Za-z\u00E4\u00F6\u00FC\u00C4\u00D6\u00DC\u00df -]{2,50}$')) {
                $('#wccity').addClass('error');
                return false;
            }
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();

        }
        else if (save == '0' && current == 'postleitzahl') {
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }

        if (save == '1' && current == 'email') {
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }
        else if (save == '0' && current == 'email') {
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }

        if (save == '1' && current == 'photo') {

            /* var url = '<?php echo e(url('/user')); ?>';
            $.post(url + '/club/wizard/store', { name: name, street:street, house_number:house_number,zip:zip,city:city }, function (data) {
                if (data) {
                    $("#wizard_" + current).hide();
                    $("#wizard_" + data).show();
                }
            });*/
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }
        else if (save == '0' && current == 'photo') {
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }

        if (save == '1' && current == 'thanks') {
            $("#form_clubwizard").submit();
        }
    }


    function nextStepClubContest(current, next, save) {

        if (current == 'thankyou') {
            $("#form_clubwizardcontest").submit();
        }
        if (current == 'close') {
            location.reload();
            return;
        }

        var name = $.trim($('#wcname').val());
        var sport = $.trim($('#wcsport').val());
        var street = $.trim($('#wcstreet').val());
        var house_number = $.trim($('#wchouse_number').val());
        var zip = $.trim($('#wczip').val());
        var city = $.trim($('#wccity').val());
        var web = $.trim($('#wcweb').val());


        if (save == '1' && current == 'state') {

            $('#wcname').removeClass('error');
            $('#wcsport').removeClass('error');
            if ($('#wcname').val() == '') {
                $('#wcname').addClass('error');
                return false;
            }
            if ($('#wcsport').val() == '') {
                $('#wcsport').addClass('error');
                return false;
            }
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }

        if (save == '1' && current == 'dob') {

            // $('#wctimeline_begin_month').removeClass('error');
            // if ($('#wctimeline_begin_month').val() == '') {
            //     $('#wctimeline_begin_month').addClass('error');
            //     return false;
            // }
            // $('#wctimeline_begin_year').removeClass('error');
            // if ($('#wctimeline_begin_year').val() == '') {
            //     $('#wctimeline_begin_year').addClass('error');
            //     return false;
            // }
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();

        }
        else if (save == '0' && current == 'dob') {
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }

        if (save == '1' && current == 'street') {
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }
        else if (save == '0' && current == 'street') {
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }

        if (save == '1' && current == 'postleitzahl') {

            $('#wccity').removeClass('error');
            $('#wczip').removeClass('error');

            if (!$.isNumeric(zip) && zip != '') {
                $('#wczip').addClass('error');
                return false;
            }

            if (zip.length != 5) {
                $('#wczip').addClass('error');
                return false;
            }

            if (!city.match('^[A-Za-z\u00E4\u00F6\u00FC\u00C4\u00D6\u00DC\u00df -]{2,50}$')) {
                $('#wccity').addClass('error');
                return false;
            }
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();

        }
        else if (save == '0' && current == 'postleitzahl') {
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }

        if (save == '1' && current == 'email') {
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }
        else if (save == '0' && current == 'email') {
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }

        if (save == '1' && current == 'photo') {

            /* var url = '<?php echo e(url('/user')); ?>';
            $.post(url + '/club/wizard/store', { name: name, street:street, house_number:house_number,zip:zip,city:city }, function (data) {
                if (data) {
                    $("#wizard_" + current).hide();
                    $("#wizard_" + data).show();
                }
            });*/
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }
        else if (save == '0' && current == 'photo') {
            $("#wizard_" + current).hide();
            $("#wizard_" + next).show();
        }

        if (save == '1' && current == 'thanks') {
            $("#form_clubwizardcontest").submit();
        }
    }



    $("#sortable").sortable();
    $("#sortable").disableSelection();

    $("input[name^='sportIdArr']").each(function () {
        $("#accordion_" + $(this).val()).accordion();
    });

    function getContact(id, dynamicId) {
        var url = '<?php echo e(url('/benutzer')); ?>';
        $.get(url + '/contact/' + id, function (data) {
            var result = data.split(':');
            $('#phone' + dynamicId).val(result[0]);
            $('#ap_email' + dynamicId).val(result[1]);
        });
    }

    $(document).ready(function () {
        setTimeout(function () {
            $('#messagewindow').animate({
                scrollTop: $('#messagewindow')[0].scrollHeight
            }, 1000)
        }, 2000);
    });

    var rootUrl = '<?php echo (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/'; ?>';

    $(function () {
        $('.keyword-item').click(function(){
          let sport = $(this).data('sport');
          let location = $(this).data('location');
          let url = rootUrl + sport + '/' + location;
            $("#keyword-url").html('');
          $("#keyword-url").append(
            '<a target="_blank" href="' + url + '">' + url + '</a>'
          )
        });
    });

</script>
<!-- Facebook Pixel Code -->
<script>
    $(".cc_banner cc_container cc_container--open").onclick =
    !function (f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function () {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '133008314040080');
    fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=133008314040080&ev=PageView&noscript=1"
    /></noscript>
<!-- End Facebook Pixel Code -->



<?php echo $__env->yieldContent('post-js'); ?>


<?php echo $__env->yieldPushContent('scripts'); ?>

</body>
</html>