<div class="container">
    <div class="pd-0-mg-200 mt-0 mb-0">
        <div class="row " data-vote="<?php echo e($vote->id); ?>">
            <?php $__currentLoopData = $vote->voteNominees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $nominee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php echo $__env->make('vote.nominee-teaser', [
                    'voteId' => $vote->id,
                    'club' => $nominee->voteable,
                    'nominee' => $nominee,
                    'index' => $index
                ], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>

    
    <?php echo $__env->make('vote.previous-winner', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php if(Auth::check()): ?>
        <?php echo $__env->make('user.profile.modal.profile-info-wizard', ['me' => $me, 'state' => $state, 'show' => 1], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>


</div>