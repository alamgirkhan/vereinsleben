<div class="col-xxs-12 col-xs-6 col-md-3">
    <figure class="news-overview__teaser <?php if($page == '' && ($key == 0 || $key == 1 || $key == 2 || $key == 3)): ?> featured-icon <?php endif; ?>">
        <div class="news-overview__image-wrapper">
            <a class="news-overview__link"
               href="<?php echo e(route('news.detail', [$news->category_id, $news->slug])); ?>">
                <div class="news-overview__image"
                     style="background-image: url('<?php echo e(asset($news->main_image->url('startpage'))); ?>');"></div>
            </a>
        </div>
        <figcaption class="news-overview__item-content news-overview__item-content--plain">
            <h3 class="news-overview__item-headline" data-insert-hyphen="true">
                <a class="news-overview__link" href="<?php echo e(route('news.detail', [$news->category_id, $news->slug])); ?>">
                    <?php echo e(str_limit($news->title, $limit = 46, $end = '...')); ?>

                </a>
            </h3>
        </figcaption>
    </figure>
</div>

<?php if(isset($loadMore) && $news->count() >= 12): ?>
    <div class="col-md-12">
        <div class="button__center-wrapper">
            <button class="button button--padded button--dark">Weitere News laden</button>
        </div>
    </div>
<?php endif; ?>