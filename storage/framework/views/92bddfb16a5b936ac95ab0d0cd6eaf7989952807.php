<?php $__env->startSection('title', 'vereinsleben.de - wir. leben. sport.'); ?>
<?php $__env->startSection('meta_description', 'Finde den passenden Sportverein und nutze die Bewertungsfunktion, findet neue Mitglieder und informiert über eurer Sportverein mit Neuigkeiten aus dem Breitensport.'); ?>
<?php $__env->startSection('meta_keywords', 'Sportvereine, bewerten, suchen, Mitglieder, finden, Breitensport'); ?>

<?php $__env->startSection('content'); ?>
    <?php if($sliderImages && $sliderImages->count()): ?>
        <?php echo $__env->make('components.startpage-slider', [
        'slider' => $sliderImages
        ], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>

    <?php echo $__env->make('components.highlight-ribbon', ['numbers' => $numbers], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    

    <?php if($regionalNews && $regionalNews->count() >= 4): ?>
        <?php echo $__env->make('news.partials.regional-teaser', [
        'news' => $regionalNews,
        ], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>

    <?php if($latestNews && $latestNews->count() >= 4): ?>
        <?php echo $__env->make('news.partials.full-teaser', [
        'news' => $latestNews,
        'featuredNews' => $featuredNews
        ], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>

    <?php echo $__env->make('components.feature-section',[
        'upcomingEvents'=>$upcomingEvents,
    ], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <?php echo $__env->make('components.partner-section', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>