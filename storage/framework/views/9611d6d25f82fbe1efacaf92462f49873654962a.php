<div class="spitzensport-slider">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="small-tag">
                    <div class="curve"></div>
                    <span>Spitzensport <br> in rheinland-pfalz</span>
                </div>
                <div class="owl-carousel spitzensport owl-theme">
                    <?php $__currentLoopData = $spitzensports; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $spitzensport): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <section class="deine-section" style="background-image: url('<?php echo e(asset($spitzensport->main_image->url('singleview'))); ?>'); background-position: center center; background-size: cover;">
                        <div class="deine-section-inner">
                            
                            <div class="headline">
                                <h2><?php echo e($spitzensport->full_name); ?></h2>
                                <span><?php echo e($spitzensport->profession); ?></span>
                            </div>
                        </div>
                        <div class="subline">
                            <div class=" alle-news">
                                <a class="btn btn-default registrieren-btn" href="<?php echo e(route('spitzensport.detail', [$spitzensport->category_id, $spitzensport->slug])); ?>">
                                    mehr lesen
                                </a>
                            </div>
                        </div>
                    </section>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->startPush('scripts'); ?>
<script>
    $(document).ready(function(){
        var carouselSpitzensport = $(".owl-carousel.spitzensport");
        carouselSpitzensport.owlCarousel({
            nav: true,
            items : 1,
            autoPlay: true,
            autoplayTimeout: 3000,
            itemsDesktop : false,
            itemsDesktopSmall : false,
            itemsTablet: false,
            itemsMobile: false,
            navText: [
                '<img onmouseout="this.src=`<?php echo e(asset('static_images/btn_left_normal.png')); ?>`" onmouseover="this.src=`<?php echo e(asset('static_images/btn_left_over.png')); ?>`" src="<?php echo e(asset('static_images/btn_left_normal.png')); ?>"/>',
                '<img onmouseout="this.src=`<?php echo e(asset('static_images/btn_right_normal.png')); ?>`" onmouseover="this.src=`<?php echo e(asset('static_images/btn_right_over.png')); ?>`" src="<?php echo e(asset('static_images/btn_right_normal.png')); ?>"/>'
            ],
            autoHeight: false
        });
    });
</script>
<?php $__env->stopPush(); ?>