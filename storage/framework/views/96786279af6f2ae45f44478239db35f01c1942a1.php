<div class="col-xs-12 col-sm-6 col-md-6">
    <div class="fan-box">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 club-result-thum">
                <div class="thum-bg">
                    <a href="<?php echo e(route('bandage.detail', $bandage->slug)); ?>"><img
                                src="<?php echo e($bandage->avatar->url('singleView')); ?>" alt="">
                    </a>
                </div>
            </div>
            <div class="col-lg-9 col-sm-6">
                <div class="fan-box-detail">
                    <h3><a href="<?php echo e(route('bandage.detail', $bandage->slug)); ?>" class="member-card__link">
                            <?php echo e($bandage->name); ?>

                        </a>
                    </h3>
                </div>
                <div class="fan-lctn"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo e($bandage->city); ?></div>
            </div>
        </div>
    </div>
</div>

