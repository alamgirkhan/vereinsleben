<div <?php echo e(isset($newPost) ? 'id=add-post' : ''); ?> class="tabbable-menu__page <?php echo e(isset($newPost) ? '' : 'tabbable-menu__page--active'); ?>">
    <?php if($bandage->id === null || isset($newPost)): ?>
        <?php echo Form::open(
            array(
                'method' => 'post',
                'route' => ['bandage.post.store', '#pinnwand'],
                'files' => 'true'
            )
        ); ?>

    <?php else: ?>
        <?php echo Form::model(
        $post,
        array(
            'method' => 'patch',
            'route' => ['bandage.post.update', $post->id, '#pinnwand'],
            'files' => 'true',
            'data-async-form'
        )
    ); ?>

    <?php endif; ?>
    <div class="club-detail__inline-form">
        <div class="row">
            <div class="col-xs-12">
                <?php echo Form::label('title', 'Titel', ['class' => 'input-label']); ?>

                <div class="input-group">
                    <?php echo Form::text('title', null, ['class' => 'input', 'placeholder' => 'Titel der Neuigkeit', 'required' => 'required']); ?>

                </div>
            </div>

            <div class="col-xs-12">
                <?php echo Form::label('content_raw', 'Text', ['class' => 'input-label']); ?>

                <div class="input-group">
                    <?php echo Form::textarea('content_raw', null, ['class' => 'input', 'rows' => 10, 'placeholder' => 'Inhalt der Neuigkeit', 'required' => 'required']); ?>

                </div>
            </div>

            <div class="col-xs-6">
                <?php echo Form::label('published_from', 'Veröffentlicht von', ['class' => 'input-label']); ?>

                <div class="input-group">
                    <?php echo Form::text('published_from', isset($post->published_from) ? date('d.m.Y H:i', strtotime($post->published_from)) : null, ['class' => 'input', 'placeholder' => 'DD.MM.YY HH:mm', 'data-datetimepicker-enabled']); ?>

                </div>
            </div>

            <div class="col-xs-6">
                <?php echo Form::label('published_until', 'Veröffentlicht bis', ['class' => 'input-label']); ?>

                <div class="input-group">
                    <?php echo Form::text('published_to', isset($post->published_to) ? date('d.m.Y H:i', strtotime($post->published_to)) : null, ['class' => 'input', 'placeholder' => 'DD.MM.YY HH:mm', 'data-datetimepicker-enabled']); ?>

                </div>
            </div>

            <?php echo Form::hidden('bandage_id', $bandage->id); ?>


            <div class="col-xs-12">
                <div id="<?php if(isset($post)): ?><?php echo e('upload-result-'. $post->id); ?><?php else: ?><?php echo e('upload-result'); ?><?php endif; ?> "
                     class="thumbnail__list">
                    <?php if(isset($post)): ?>
                        <?php $__currentLoopData = $post->images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <input type="checkbox" name="delete-image[]" value="<?php echo e($image->id); ?>"
                                   id="post_thumbnail-<?php echo e($image->id); ?>" class="thumbnail__switch">
                            <label for="post_thumbnail-<?php echo e($image->id); ?>"
                                   class="thumbnail__label thumbnail__container thumbnail__container--deleteable">
                                <img class="thumbnail__image" src="<?php echo e(asset($image->picture->url('singleView'))); ?>"/>
                            </label>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </div>
            </div>

            <div data-upload-result>
                <div class="col-xs-12 hidden" data-new-image-text>
                    <span class="input-label">Neu hinzugefügte Bilder</span>
                </div>
                <div class="col-xs-12">
                    <div id="<?php if(isset($post)): ?><?php echo e('upload-result-'. $post->id); ?><?php else: ?><?php echo e('upload-result'); ?><?php endif; ?> "
                         class="thumbnail__list">
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="club-detail__inline-form-submit-wrapper">
                        <label for="<?php if(isset($post)): ?><?php echo e('image-'. $post->id); ?><?php else: ?><?php echo e('image'); ?><?php endif; ?>"
                               class="button button--grey button--center button--icon button--full-width">
                            <span class="fa fa-upload"></span>Bilder hochladen</label>
                        <input multiple type="file" name="images[]"
                               id="<?php if(isset($post)): ?><?php echo e('image-'. $post->id); ?><?php else: ?><?php echo e('image'); ?><?php endif; ?>"
                               class="input-file">
                        <button class="button button--dark button--center button--icon button--full-width"
                                type="submit"><span class="fa fa-edit"></span>
                            <?php if($bandage->id === null || isset($newPost)): ?>
                                Posten
                            <?php else: ?>
                                Aktualisieren
                            <?php endif; ?>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo Form::close(); ?>

</div>
