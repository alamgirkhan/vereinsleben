<?php if(!Request::is('/')): ?>
    <section class="partner-logo">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 partner-logo-in">
                    <ul>
                        <?php if(isset($federalState) && $federalState->name == 'Rheinland-Pfalz'): ?>
                            <li><a target="_blank" href="<?php echo e(url("partner/lotto")); ?>">
                                    <img src="<?php echo e(url('/static_images/partner-lotto.png')); ?>" width="87%" height="87%"
                                         onmouseover="this.src='<?php echo e(url('/static_images/partner-lotto-c.png')); ?>'"
                                         onmouseout="this.src='<?php echo e(url('/static_images/partner-lotto.png')); ?>'"/>
                                </a></li>
                            <li class="sparda"><a target="_blank" href="<?php echo e(url("partner/sparda-bank-suedwest")); ?>">
                                    <img src="<?php echo e(url('/static_images/partner-sparda-bank.png')); ?>" width="87%"
                                         height="87%"
                                         onmouseover="this.src='<?php echo e(url('/static_images/partner-sparda-bank-c.png')); ?>'"
                                         onmouseout="this.src='<?php echo e(url('/static_images/partner-sparda-bank.png')); ?>'"/>
                                </a>
                            </li>

                            <li><a target="_blank" href="<?php echo e(url("partner/best-western")); ?>">
                                    <img src="<?php echo e(url('/static_images/bw-logo-small.png')); ?>" width="90%" height="auto" style="margin-top: 5px;"
                                         onmouseover="this.src='<?php echo e(url('/static_images/bw-logo-small-hover.png')); ?>'"
                                         onmouseout="this.src='<?php echo e(url('/static_images/bw-logo-small.png')); ?>'"/>
                                </a></li>
                                <?php if(1==2): ?>
                                <li><a target="_blank" href="<?php echo e(url("partner/abc-europe-werbeagentur")); ?>">
                                            <img src="<?php echo e(url('/static_images/partner-abc-c.jpg')); ?>" width="90%" height="90%"
                                                 onmouseover="this.src='<?php echo e(url('/static_images/partner-abc.jpg')); ?>'"
                                                 onmouseout="this.src='<?php echo e(url('/static_images/partner-abc-c.jpg')); ?>'"/>
                                        </a></li>
                                 <?php endif; ?>
                            <li><a target="_blank" href="<?php echo e(url("partner/rpr1")); ?>">
                                    <img src="<?php echo e(url('/static_images/partner-rpr1.png')); ?>" width="87%" height="87%"
                                         onmouseover="this.src='<?php echo e(url('/static_images/partner-rpr1-c.png')); ?>'"
                                         onmouseout="this.src='<?php echo e(url('/static_images/partner-rpr1.png')); ?>'"/>
                                </a></li>
                            <li><a target="_blank" href="<?php echo e(url("partner/bigfm")); ?>">
                                    <img src="<?php echo e(url('/static_images/partner-bigfm.png')); ?>" width="87%" height="87%"
                                         onmouseover="this.src='<?php echo e(url('/static_images/partner-bigfm-c.png')); ?>'"
                                         onmouseout="this.src='<?php echo e(url('/static_images/partner-bigfm.png')); ?>'"/>
                                </a></li>
                            <li><a target="_blank" href="<?php echo e(url("partner/landessportbund-rheinland-pfalz")); ?>">
                                <img src="<?php echo e(url('/static_images/partner_lsbrlp_sw.gif')); ?>"
                                     onmouseover="this.src='<?php echo e(url('/static_images/partner_lsbrlp_color.gif')); ?>'"
                                     onmouseout="this.src='<?php echo e(url('/static_images/partner_lsbrlp_sw.gif')); ?>'"/>
                            </a></li>
                            <li><a target="_blank" href="<?php echo e(url("partner/sportbund-rheinland")); ?>">
                                    <img src="<?php echo e(url('/static_images/partner-sportbund.png')); ?>" width="87%" height="87%"
                                         onmouseover="this.src='<?php echo e(url('/static_images/partner-sportbund-c.png')); ?>'"
                                         onmouseout="this.src='<?php echo e(url('/static_images/partner-sportbund.png')); ?>'"/>
                                </a></li>
                            <li><a target="_blank" href="<?php echo e(url("partner/sportbund-rheinhessen")); ?>">
                                    <img src="<?php echo e(url('/static_images/partner-rhenhessen.png')); ?>" width="87%" height="87%"
                                         onmouseover="this.src='<?php echo e(url('/static_images/partner-rhenhessen-c.png')); ?>'"
                                         onmouseout="this.src='<?php echo e(url('/static_images/partner-rhenhessen.png')); ?>'"/>
                                </a></li>
                            <li><a target="_blank" href="<?php echo e(url("partner/sportbund-pfalz")); ?>">
                                    <img src="<?php echo e(url('/static_images/partner-pfalz.png')); ?>" width="87%" height="87%"
                                         onmouseover="this.src='<?php echo e(url('/static_images/partner-pfalz-c.png')); ?>'"
                                         onmouseout="this.src='<?php echo e(url('/static_images/partner-pfalz.png')); ?>'"/>
                                </a></li>
                            <li><a target="_blank" href="<?php echo e(url("partner/deutscher-dart-verband")); ?>">
                                    <img src="<?php echo e(url('/static_images/ddv_logo_footer_sw.png')); ?>"
                                         onmouseover="this.src='<?php echo e(url('/static_images/ddv_logo_footer_color.png')); ?>'"
                                         onmouseout="this.src='<?php echo e(url('/static_images/ddv_logo_footer_sw.png')); ?>'"/>
                                </a></li>

                            <li class="sw"><a target="_blank" href="<?php echo e(url("partner/suedwestdeutscher-fussballverband")); ?>">
                                <img src="<?php echo e(url('/static_images/partner_swfv_klein_sw.gif')); ?>" width="70%" height="70%"
                                     onmouseover="this.src='<?php echo e(url('/static_images/partner_swfv_klein.gif')); ?>'"
                                     onmouseout="this.src='<?php echo e(url('/static_images/partner_swfv_klein_sw.gif')); ?>'"/>
                              </a></li>

                        <?php elseif(isset($federalState) && $federalState->name == 'Baden-Württemberg'): ?>
                            <li><a target="_blank" href="<?php echo e(url("partner/best-western")); ?>">
                                    <img src="<?php echo e(url('/static_images/bw-logo-small.png')); ?>" width="90%" height="auto" style="margin-top: 5px;"
                                         onmouseover="this.src='<?php echo e(url('/static_images/bw-logo-small-hover.png')); ?>'"
                                         onmouseout="this.src='<?php echo e(url('/static_images/bw-logo-small.png')); ?>'"/>
                                </a></li>
                            <li><a target="_blank" href="<?php echo e(url("partner/bigfm")); ?>">
                                    <img src="<?php echo e(url('/static_images/partner-bigfm.png')); ?>" width="87%" height="87%"
                                         onmouseover="this.src='<?php echo e(url('/static_images/partner-bigfm-c.png')); ?>'"
                                         onmouseout="this.src='<?php echo e(url('/static_images/partner-bigfm.png')); ?>'"/>
                                </a></li>
                            <li><a target="_blank" href="<?php echo e(url("partner/deutscher-dart-verband")); ?>">
                                    <img src="<?php echo e(url('/static_images/ddv_logo_footer_sw.png')); ?>"
                                         onmouseover="this.src='<?php echo e(url('/static_images/ddv_logo_footer_color.png')); ?>'"
                                         onmouseout="this.src='<?php echo e(url('/static_images/ddv_logo_footer_sw.png')); ?>'"/>
                                </a></li>
                        <?php elseif(isset($federalState) && $federalState->name == 'Saarland'): ?>

                            <li class="sparda">
                                <a target="_blank" href="<?php echo e(url("partner/sparda-bank-suedwest")); ?>">
                                    <img src="<?php echo e(url('/static_images/partner-sparda-bank.png')); ?>" width="87%"
                                         height="87%"
                                         onmouseover="this.src='<?php echo e(url('/static_images/partner-sparda-bank-c.png')); ?>'"
                                         onmouseout="this.src='<?php echo e(url('/static_images/partner-sparda-bank.png')); ?>'"/>
                                </a>
                            </li>
                            <li><a target="_blank" href="<?php echo e(url("partner/best-western")); ?>">
                                    <img src="<?php echo e(url('/static_images/bw-logo-small.png')); ?>" width="90%" height="auto" style="margin-top: 5px;"
                                         onmouseover="this.src='<?php echo e(url('/static_images/bw-logo-small-hover.png')); ?>'"
                                         onmouseout="this.src='<?php echo e(url('/static_images/bw-logo-small.png')); ?>'"/>
                                </a></li>
                            <li><a target="_blank" href="<?php echo e(url("partner/deutscher-dart-verband")); ?>">
                                    <img src="<?php echo e(url('/static_images/ddv_logo_footer_sw.png')); ?>"
                                         onmouseover="this.src='<?php echo e(url('/static_images/ddv_logo_footer_color.png')); ?>'"
                                         onmouseout="this.src='<?php echo e(url('/static_images/ddv_logo_footer_sw.png')); ?>'"/>
                                </a></li>
                        <?php else: ?>
                            <li><a target="_blank" href="<?php echo e(url("partner/best-western")); ?>">
                                    <img src="<?php echo e(url('/static_images/bw-logo-small.png')); ?>" width="90%" height="auto" style="margin-top: 5px;"
                                         onmouseover="this.src='<?php echo e(url('/static_images/bw-logo-small-hover.png')); ?>'"
                                         onmouseout="this.src='<?php echo e(url('/static_images/bw-logo-small.png')); ?>'"/>
                                </a></li>
                            <li><a target="_blank" href="<?php echo e(url("partner/deutscher-dart-verband")); ?>">
                                    <img src="<?php echo e(url('/static_images/ddv_logo_footer_sw.png')); ?>"
                                         onmouseover="this.src='<?php echo e(url('/static_images/ddv_logo_footer_color.png')); ?>'"
                                         onmouseout="this.src='<?php echo e(url('/static_images/ddv_logo_footer_sw.png')); ?>'"/>
                                </a></li>
                        <?php endif; ?>

                    </ul>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
<div class="clear"></div>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-6 social-media-section">
                <h2>wir.leben.sport.</h2>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 footer-links">
                <h3>Quick-Links</h3>
                <ul>
                    <li><a href="<?php echo e(url('verein-des-monats')); ?>">Verein des Monats</a></li>
                    <li><a href="<?php echo e(url('service/stellenboerse')); ?>">Trainer-Stellenbörse</a></li>
                    <li>
                        <a href="<?php echo e(url('neuigkeiten/?searchtext=Sport+mit+Verantwortung&federal_state=0&news_type=')); ?>">Sport
                            mit Verantwortung</a></li>
                    <li><a href="<?php echo e(url('event/all')); ?>">Eventkalender</a></li>
                    <li><a href="/podcast">Podcast</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 footer-links">
                <h3>News</h3>
                <ul>
                    <li><a href="<?php echo e(url('neuigkeiten/')); ?>">Allgemeine News</a></li>
                    <li><a href="/neuigkeiten?searchtext=&federal_state=0&news_type=4">Vereinsnews</a></li>
                    <li><a href="/neuigkeiten?searchtext=&federal_state=0&news_type=3">Verbandsnews</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-2 col-sm-6 footer-links">
                <h3>Über uns</h3>
                <ul>
                    <li><a href="<?php echo e(url('/kontakt')); ?>">Kontakt</a></li>
                    <li><a href="<?php echo e(url('/content/impressum')); ?>">Impressum</a></li>
                    <li><a href="<?php echo e(url('/content/datenschutz')); ?>">Datenschutz</a></li>
                    <li><a href="<?php echo e(url('/content/agbs')); ?>">AGB</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<section class="copyright">
    <div class="container">
        <div class="row">
            <div class="col-lg-12"> &copy; 2018 vereinsleben.de</div>
        </div>
    </div>
</section>