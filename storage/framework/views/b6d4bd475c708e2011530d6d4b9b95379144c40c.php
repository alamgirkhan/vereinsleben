<?php $__currentLoopData = $rankings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rank => $ranking): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <?php if($rank >= ($from - 1) && $rank < ($to - 1)): ?>
        <div class="vote-placement">
            <div class="vote-placement__content">
                <div class="vote-placement__num"><?php echo e($rank + 1); ?>.</div>
                <a href="<?php echo e(route('club.vote.nominee', $ranking->voteable->slug)); ?>">
                    <img src="<?php echo e(asset($ranking->voteable->avatar->url('startPage'))); ?>" class="vote-placement__img">
                </a>
                <a href="<?php echo e(route('club.vote.nominee', $ranking->voteable->slug)); ?>"
                class="vote-placement__club"><?php echo e($ranking->voteable->name); ?></a>
            </div>
            <vote-button label="Abstimmen" :id="<?php echo e($vote->id); ?>"
                         :authenticated="<?php echo e((int)Auth::check()); ?>"
                         nominee="<?php echo e($ranking->id); ?>"
                         voted="<?php echo e(Auth::check() && $hasVoted); ?>"></vote-button>
        </div>
    <?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>