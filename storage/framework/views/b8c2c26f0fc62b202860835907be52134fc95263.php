<?php if($bandage->id === null): ?>
    <?php echo Form::model(
        $bandage,
        array(
            'method' => 'post',
            'route' => 'bandage.store',
            'class' => 'club-edit__form',
            'files' => 'true'
        )
    ); ?>

<?php else: ?>
    <?php echo Form::model(
        $bandage,
        array(
            'method' => 'patch',
            'route' => ['bandage.update.public', $bandage->slug],
            'class' => 'club-edit__form',
            'files' => 'true'
        )
    ); ?>

<?php endif; ?>
<label class="club-content__edit-button club-content__edit-button--avatarimage pull-right"
       href="#" for="avatar">
    <span class="fa fa-pencil"></span>
</label>

<div class="club-content__edit-area club-content__edit-area--avatarimage club-content__box club-content__box--avatar-upload">
    <a href="#" class="club-content__icon-button" id="avatar-zoom-out">
        <span class="fa fa-search-minus"></span>
    </a>
    <a href="#" class="club-content__icon-button" id="avatar-zoom-in">
        <span class="fa fa-search-plus"></span>
    </a>

    <input type="file" name="avatar" id="avatar" class="club-content__hidden-input">
    <input type="hidden" name="crop-props">

    <div class="club-content__form-action-wrapper">
        <a href="#"
           class="club-content__form-cancel-action-button button button--grey button--center button--full-width">
            Abbrechen
        </a>
        <?php echo Form::submit('Speichern',
            array(
                'class' => 'button--save club-content__form-save-action-button button button--edit button--center button--full-width'
            )); ?>

    </div>
</div>
<?php echo Form::close(); ?>