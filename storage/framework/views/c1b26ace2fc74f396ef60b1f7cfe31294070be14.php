<p class="vote-detail__btn-wrapper">
    <a href="/verein-des-monats" class="btn btn--grey btn--inline btn--large btn--upper">
        Zurück zur Hauptseite
    </a>

    <a href="/verein-des-monats/teilnehmen" class="btn btn--default btn--inline btn--large btn--upper">
        Jetzt für "Verein des Monats" bewerben
    </a>
</p>