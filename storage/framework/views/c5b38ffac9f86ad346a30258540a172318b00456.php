<div class="number-of-week">
    <div class="owl-carousel owl-theme">
        <?php $__currentLoopData = $numbers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $number): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <section class="deine-section" style="background-image: url('<?php echo e(asset('uploaded/number-of-week/' . $number->image)); ?>')">
                <div class="number-opacity" style="opacity: <?php echo e($number->opacity / 100); ?>"></div>
                <div class="deine-section-inner">
                    <div class="container">
                        <div class="row">
                            <div class="headline col-lg-12">
                                <h2>DIE ZAHL DER WOCHE</h2>
                                <span><?php echo e(date('d.m.', strtotime($number->published_from))); ?> - <?php echo e(date('d.m.Y', strtotime($number->published_from . ' + ' . 6 . ' days' ))); ?></span>
                            </div>
                        </div>
                        <div class="deine-boxes">
                            <h1><?php echo e($number->number); ?></h1>
                            <p><?php echo e($number->short_description); ?></p>
                        </div>
                    </div>
                </div>
                <div class="subline">
                    <?php echo $number->sub_line; ?>

                </div>
            </section>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
</div>

<?php $__env->startPush('scripts'); ?>
<script src="<?php echo e(asset(('js/OwlCarousel2/owl.carousel.js'))); ?>"></script>
<script>
    var numberCount = '<?php echo count($numbers) ?>';
    $().ready(function(){
        var carousel = $(".owl-carousel");
        carousel.owlCarousel({
            nav:true,
            slideSpeed : 300,
            paginationSpeed : 400,
            items : 1,
            itemsDesktop : false,
            itemsDesktopSmall : false,
            itemsTablet: false,
            itemsMobile : false,
            navText: [
                '<img onmouseout="this.src=`<?php echo e(asset('static_images/btn_left_normal.png')); ?>`" onmouseover="this.src=`<?php echo e(asset('static_images/btn_left_over.png')); ?>`" src="<?php echo e(asset('static_images/btn_left_normal.png')); ?>"/>',
                '<img onmouseout="this.src=`<?php echo e(asset('static_images/btn_right_normal.png')); ?>`" onmouseover="this.src=`<?php echo e(asset('static_images/btn_right_over.png')); ?>`" src="<?php echo e(asset('static_images/btn_right_normal.png')); ?>"/>'
            ],
            startPosition: (numberCount - 1),
            autoHeight: false
        });
    });
</script>
<?php $__env->stopPush(); ?>