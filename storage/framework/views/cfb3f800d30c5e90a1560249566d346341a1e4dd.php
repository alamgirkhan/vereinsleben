<div class="club-post__item" data-edit-wrapper>
    <div class="row">
        <?php if(Auth::check() && Auth::user()->isBandageAdmin($bandage)): ?>
            <div class="col-xs-11 edit-del-btn">
                <div class="club-content__options">
                    <a href="#" class="club-content__options-button" title="Bearbeiten"
                       data-edit='{ "bandage_id": "<?php echo e($bandage->id); ?>",
                       "resource": { "id": "<?php echo e($event->id); ?>",
                       "type": "event",
                       "url": "<?php echo e(route('bandage.event.edit', $event->id)); ?>"
                       } }'>
                        <span class="fa fa-pencil club-content__options-button-edit"></span>
                    </a>
                    <a href="#" class="club-content__options-button club-content__options-button--hidden"
                       title="Bearbeiten abbrechen"
                       data-cancel='{ "bandage_id": "<?php echo e($bandage->id); ?>",
                       "resource": { "id": "<?php echo e($event->id); ?>",
                       "type": "event",
                       "url": "<?php echo e(route('bandage.event.single', [$bandage->slug, $event->id])); ?>"
                       } }'>
                        <span class="fa fa-times club-content__options-button-cancel"></span>
                    </a>
                    <a href="#" class="club-content__options-button" title="Löschen"
                       data-delete='{ "bandage_id": "<?php echo e($bandage->id); ?>",
                       "resource": { "id": "<?php echo e($event->id); ?>",
                       "type": "event",
                       "url": "<?php echo e(route('event.delete', $event->id)); ?>"
                       } }'>
                        <span class="fa fa-trash club-content__options-button-remove"></span>
                    </a>
                </div>
            </div>
        <?php endif; ?>
    </div>

    <div data-edit-content class="club-post__container">
        <div class="row">
            <div class="col-lg-1 club-post__date">
                <?php if($event->published_from >= \Carbon\Carbon::now()): ?>
                    <span class="club-post__date club-post__date--scheduled">
                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                        <?php echo e(date('d.m.Y / H:i', strtotime($event->published_from))); ?>


                        <?php if(isset($event->published_to) and $event->published_to !== null): ?>
                            - <?php echo e(date('d.m.Y / H:i', strtotime($event->published_to))); ?>

                        <?php endif; ?>
                </span>
                <?php else: ?>
                    <span class="date-no"><?php echo e(date('d', strtotime($event->published_at))); ?>.</span><br>
                    <span class="month-no"><?php echo e(strtoupper(date('M', strtotime($event->published_at)))); ?></span><br>
                    <span class="year-no"><?php echo e(date('Y', strtotime($event->published_at))); ?></span>
                <?php endif; ?>
            </div>
            <div class="col-lg-11 col-sm-11 event-post">
                <?php if($event->images()->count() > 0): ?>
                    <div class="event-post-thum">
                        <?php if($event->images()->count() == 1): ?>
                            <?php $__currentLoopData = $event->images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="club-post__image"
                                     style="background-image: url('<?php echo e(asset($image->picture->url('singleView'))); ?>')"></div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php else: ?>

                            <div class="row no-margin">
                                <div class="col-lg-8 col-sm-6 club-post__image"
                                     style="background:url('<?php echo e(asset($event->images[0]->picture->url('singleView'))); ?>') no-repeat;background-size:cover;"></div>
                                <div class="col-lg-4 col-sm-6 club-post_right-thums">
                                    <div class="row">
                                        <?php $__currentLoopData = $event->images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($key !=0): ?>
                                                <div class="col-lg-12 club-post__sml-image"
                                                     style="background:url('<?php echo e(asset($image->picture->url('singleView'))); ?>') no-repeat;background-size:cover;"></div>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="event-post-thum-arrow"><i class="fa fa-caret-up" aria-hidden="true"></i></div>
                        <?php if($event->images()->count() > 3): ?>
                            <div class="event-plus-icon"><a href="javascript:void(0);"><i class="fa fa-plus-circle"
                                                                                          aria-hidden="true"></i></a>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>

                <?php if(isset($event->content)): ?>
                    <div class="row post-head">
                        <div class="col-lg-2 col-sm-6 postlogo">
                            <img width="61px" src="<?php echo e(asset($bandage->avatar->url('singleView'))); ?>"/>
                        </div>
                        <div class="col-lg-10 col-sm-6 postdetail">
                            <h2><?php echo e($event->title); ?></h2>
                            <div class="row">
                                <div class="col-lg-4 col-sm-6 postlefttxt"><?php echo e($bandage->shorthand); ?></div>
                                <div class="col-lg-8 col-sm-6 postlocation">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo e($event->street); ?>

                                    <?php if(isset($event->house_number)): ?>
                                        <span>, <?php echo e($event->house_number); ?>, </span>
                                    <?php endif; ?>
                                    <?php echo e($event->zip); ?>, <?php echo e($event->city); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row post-head">
                        <div class="col-lg-4 col-sm-6 postlocation">
                            <i class="fa fa-calendar"
                               aria-hidden="true"></i> <?php echo e(date('d.m.Y H:i', strtotime($event->schedule_begin))); ?>

                        </div>
                        <div class="col-lg-6 col-sm-6 postlocation">
                            <i class="fa fa-clock-o"
                               aria-hidden="true"></i> <?php echo e(date('d.m.Y H:i', strtotime($event->schedule_end))); ?>

                        </div>
                    </div>
                    <?php if(isset($event->info)): ?>
                        <div class="row event-head">
                            <div class="col-lg-12 postlocation">
                                <i class="fa fa-eur" aria-hidden="true"></i> <?php echo e($event->info); ?>

                            </div>
                        </div>
                    <?php endif; ?>
                    <p><?php echo nl2br($event->content); ?></p>
                    <div class="row post-head">
                        <div class="col-lg-8 col-sm-6"><a class="btn btn-default interessiert-btn"
                                                          href="#">Interessiert</a>&nbsp;
                            <a href="#" class="kommentieren"><i class="fa fa-comment-o" aria-hidden="true"></i>
                                Kommentieren <sup>8</sup></a>&nbsp; <a href="#" class="kommentieren"><i
                                        class="fa fa-paper-plane-o" aria-hidden="true"></i> Teilen</a>
                        </div>
                        <div class="col-lg-4 col-sm-6 "><a href="#" class="allevents">Alle<br> Events ansehen</a></div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>


