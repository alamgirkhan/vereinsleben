<?php if($slider->count() > 0): ?>
<div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="roundimg-down"><a href="#"><i class="fa fa-angle-down" aria-hidden="true"></i></a></div>
            </div>
        </div>
    </div>
    <div class="startpage-slider">
        <?php $__currentLoopData = $slider; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $slide): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            
            
            
            
            <?php
            $slideCount = 0;
            for ($i = 1; $i < 7; $i++) {
                if ($slide["logo{$i}_file_name"]) {
                    $slideCount = $slideCount + 1;
                }
            }
            ?>
            <div class="carousel-item <?php echo e($key == 0 ? 'active' : ''); ?> <?php echo e($slideCount > 2 ? 'small small'.$slideCount : ''); ?>"
                 style="background-image: url(<?php echo e(asset($slide->slider_image->url('original'))); ?>)">

                <div class="carousel-caption d-none d-md-block">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-7 col-sm-7 slide2">

                                <?php if(isset($slide->h2)): ?>
                                    <h2><?php echo $slide->h2; ?></h2><br>
                                <?php endif; ?>

                                <?php if(isset($slide->h3)): ?>
                                    <h3><?php echo $slide->h3; ?></h3><br>
                                <?php endif; ?>

                                <span class="slider2-logos">
                                    <?php if(isset($slide->logo1_file_name)): ?>
                                        <img src="<?php echo e(asset($slide->logo1->url('original'))); ?>" alt=""
                                             style="display: inline-block; margin-right: 7px;"/>
                                    <?php endif; ?>
                                </span>
                                    <span class="slider2-logos">
                                    <?php if(isset($slide->logo2_file_name)): ?>
                                        <img src="<?php echo e(asset($slide->logo2->url('original'))); ?>" alt=""
                                             style="display: inline-block; margin-right: 7px;"/>
                                    <?php endif; ?>
                                </span>
                                    <span class="slider2-logos">
                                    <?php if(isset($slide->logo3_file_name)): ?>
                                        <img src="<?php echo e(asset($slide->logo3->url('original'))); ?>" alt=""
                                             style="display: inline-block; margin-right: 7px;"/>
                                    <?php endif; ?>
                                </span>
                                    <span class="slider2-logos">
                                    <?php if(isset($slide->logo4_file_name)): ?>
                                        <img src="<?php echo e(asset($slide->logo4->url('original'))); ?>" alt=""
                                             style="display: inline-block; margin-right: 7px;"/>
                                    <?php endif; ?>
                                </span>
                                    <span class="slider2-logos">
                                    <?php if(isset($slide->logo5_file_name)): ?>
                                        <img src="<?php echo e(asset($slide->logo5->url('original'))); ?>" alt=""
                                             style="display: inline-block; margin-right: 7px;"/>
                                    <?php endif; ?>
                                </span>
                                    <span class="slider2-logos">
                                    <?php if(isset($slide->logo6_file_name)): ?>
                                        <img src="<?php echo e(asset($slide->logo6->url('original'))); ?>" alt=""
                                             style="display: inline-block; margin-right: 7px;"/>
                                    <?php endif; ?>
                                </span>

                                <?php if(isset($slide->btn)): ?>
                                    <br>
                                    <div class="Jetzt-btn"><a href="<?php echo e($slide->link); ?>"><?php echo e($slide->btn); ?></a></div>
                                <?php endif; ?>

                            </div>
                        </div>
                    </div>
                </div>


            </div>

        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
</div>
<?php endif; ?>