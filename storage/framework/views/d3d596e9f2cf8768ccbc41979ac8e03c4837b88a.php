<div class="section section--vote-white pd-0-mg-200 mt-0 mb-0">
    <div class="container">
        <h2 class="hl hl--large hl--underlined hl--inverted hl--centered hl--upper">
            <?php echo e($vote->teaser_headline); ?>

        </h2>

        <div class="text-block text-block--compressed text-block--thin text-block--smaller-spacing">
            <?php echo $vote->teaser; ?>

        </div>

        <p class="text-block text-block--compressed text-block--align-center">
            <a href="/verein-des-monats/teilnehmen" class="btn btn--default btn--inline btn--large btn--upper">
                Jetzt als "Verein des Monats" bewerben
            </a>
        </p>
    </div>
    <?php echo $__env->make('vote.addthis', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</div>