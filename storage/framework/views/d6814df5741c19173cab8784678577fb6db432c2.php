<?php $__env->startSection('title'); ?>
    <?php echo e($partner->title); ?>


    <?php if(isset($partner->sub_title)): ?>
        - <?php echo e($partner->sub_title); ?>

    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('meta_description'); ?><?php if(isset($partner->meta->meta_description )): ?><?php echo e($partner->meta->meta_description); ?><?php endif; ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection('meta_language'); ?><?php if(isset($partner->meta->meta_description )): ?><?php echo e($partner->meta->meta_language); ?><?php endif; ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection('meta_author'); ?><?php if(isset($partner->meta->meta_description )): ?><?php echo e($partner->meta->meta_author); ?><?php endif; ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection('meta_keywords'); ?><?php if(isset($partner->meta->meta_description )): ?><?php echo e($partner->meta->meta_keywords); ?><?php endif; ?> <?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <section class="news-detail">
        <div class="container">
            <div class="row">
                
                <div class="col-lg-9 col-sm-6 news">
                    <h2><?php echo e($partner->title); ?></h2>
                    <?php if(isset($partner->sub_title)): ?>
                        <h3><?php echo e($partner->sub_title); ?></h3>
                    <?php endif; ?>


                    <p>
                        <?php echo $partner->content; ?>

                    </p>
                </div>

                

                <div class="col-lg-3 col-sm-6">
                    <div class="row">
                        <div class="col-lg-12 profile-cnt-details">
                            <div class="rhs-box partner-box">
                                <?php if($partner->logo->originalFilename() !== null): ?>
                                    <img class="news-detail__main-image"
                                         src="<?php echo e(asset($partner->logo->url('original'))); ?>"/>
                                <?php endif; ?>

                                    <?php if($partner->address !== null && trim($partner->address) !== ""): ?>
                                        <div class="row" style="margin-top: 15px;">
                                            <div class="col-sm-2"><span><i class="fa fa-map-marker" aria-hidden="true"></i></span></div>
                                            <div class="col-sm-10"><?php echo e($partner->address); ?></div>
                                        </div>
                                    <?php endif; ?>
                                    <?php if($partner->phone !== null && trim($partner->phone) !== ""): ?>
                                        <div class="row" style="margin-top: 15px;">
                                            <div  class="col-sm-2"><span><i class="fa fa-phone" aria-hidden="true"></i></span></div>
                                            <div  class="col-sm-10"><?php echo e($partner->phone); ?></div>
                                        </div>
                                    <?php endif; ?>

                                    <?php if($partner->email !== null && trim($partner->email) !== ""): ?>
                                        <div class="row" style="margin-top: 15px;">
                                            <div  class="col-sm-2"><span><i class="fa fa-envelope" aria-hidden="true"></i></span></div>
                                            <div class="col-sm-10"><a href="mailto:<?php echo e($partner->email); ?>"><?php echo e($partner->email); ?></a></div>
                                        </div>
                                    <?php endif; ?>

                                    <?php if($partner->website !== null && trim($partner->website) !== ""): ?>

                                        <div class="row" style="margin-top: 15px;" id="website">
                                            <div  class="col-sm-2"><span><i class="fa fa-globe" aria-hidden="true"></i></span></div>
                                            <div class="col-sm-10"><a href="<?php echo e($partner->website); ?>" target="_blank"><?php echo e($partner->websiteKurz); ?></a></div>
                                        </div>
                                        </a>
                                    <?php endif; ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>