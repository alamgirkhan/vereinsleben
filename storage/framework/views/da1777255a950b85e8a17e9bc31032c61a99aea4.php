<?php $__env->startSection('content'); ?>
    <?php if($bandage->unowned === 1): ?>
        <?php echo $__env->make('bandage.partials.profile.unowned', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php else: ?>
        <section data-token="<?php echo e(csrf_token()); ?>" <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $bandage)): ?> data-token="<?php echo e(csrf_token()); ?>"
                 data-club-editable="true" <?php endif; ?>>
            <input type="hidden" name="search_id" id="search_id" value="<?php echo e($bandage->id); ?>"/>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 profilseite-slide">
                        <div class="club-detail__main-image-wrapper" style="background-image: url('<?php echo e(asset($bandage->header->url('singleView'))); ?>')"
                             <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $bandage)): ?> data-section-editable="true" <?php endif; ?>>
                            <img class="club-detail__main-image" src="<?php echo e(asset($bandage->header->url('singleView'))); ?>">
                            <div class="club-detail__notification club-detail__notification--hidden"></div>
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $bandage)): ?>
                                <?php echo $__env->make('bandage.partials.edit.header-image', ['bandage' => $bandage], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>

            <section id="about-strip" <?php if($bandage->header_color): ?> style="box-shadow:2px 2px 6px #<?php echo e($bandage->header_color); ?>"
                     <?php endif; ?> class="about-strip">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-sm-4"></div>
                        <div class="col-md-9 col-sm-8 col-xs-8 proifle-top-links profile-desk">
                            <div class="row">
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-3 count-tabs">
                                            Besucher<br><strong><?php echo e($bandage->views); ?></strong></div>
                                        <div class="col-md-3 count-tabs">
                                            Abonnenten<br><strong> <?php echo e($bandage->subcribles()->count()); ?></strong></div>
                                        
                                            
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-10 fan-btns"><?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('updateSubscribe', $bandage)): ?>
                                                <a href="#" class="btn--member" id="bin_subcribe"
                                                   <?php if($bandage->title_color): ?> style="color:#<?php echo e($bandage->title_color); ?>"
                                                   <?php endif; ?>
                                                   data-bandage-subscribe-button
                                                   data-bandage-subscribe-url=" <?php echo e(route('bandage.update.subscribe', $bandage->slug)); ?> "
                                                   data-bandage-subscribe-status="<?php echo e(Auth::check() && Auth::user()->isBandageSubscriber($bandage) ? 'true' : 'false'); ?>">
                                                    Abonnieren</a>
                                                </a><?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-9 col-xs-8 proifle-top-links profile-mob">
                            <div class="row">
                                <div class="col-md-5 col-sm-5 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-5 col-xs-6 fan-btns"><?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('updateSubscribe', $bandage)): ?>
                                                <a href="#"
                                                   style="<?php echo e(!Auth::check() ? 'display: none;' : ''); ?>"
                                                   data-bandage-subscribe-button
                                                   data-bandage-subscribe-url=" <?php echo e(route('bandage.update.subscribe', $bandage->slug)); ?> "
                                                   data-bandage-subscribe-status="<?php echo e(Auth::check() && Auth::user()->isBandageSubscriber($bandage) ? 'true' : 'false'); ?>">
                                                    <?php if(Auth::user()->isBandageSubscriber($bandage)): ?>
                                                        Abmelden
                                                    <?php else: ?>
                                                        Abonnieren
                                                    <?php endif; ?>
                                                </a><?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-7 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-3 count-tabs">
                                            Besucher<br><strong><?php echo e($bandage->views); ?></strong></div>
                                        <div class="col-md-3 count-tabs">
                                            Abonnenten<br><strong> <?php echo e($bandage->subcribles()->count()); ?></strong></div>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <div class="row club-left proife-lft ">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-3 profile-pic"
                                         <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $bandage)): ?> data-section-editable="true" <?php endif; ?>>
                                        <img class=""
                                             src="<?php echo e(asset($bandage->avatar->url('singleView'))); ?>">
                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $bandage)): ?>
                                            <?php echo $__env->make('bandage.partials.edit.avatar-image', ['bandage' => $bandage], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="row pro-na-sec">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-3 profile-name bandage">
                                        <a href="#">
                                          <img id="profile-pic-icon" style="width: 60px" src="<?php echo e(asset('default_images/bandage/profile-pic-icon.png')); ?>"/><br>Verbandsprofil
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 profile-tabs">
                                        <?php echo $__env->make('bandage.partials.detail-menu', ['bandage' => $bandage, 'socialLinks' => $socialLinks, 'socialLinkTypes' => $socialLinkTypes], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <?php if($socialLinks->count() > 0  || (Auth::check() && Auth::user()->isBandageAdmin($bandage))): ?>
                                    <section class="club-detail__menu-list-item club-detail__menu-list-item--static"
                                             <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $bandage)): ?> data-section-editable="true" <?php endif; ?>>

                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $bandage)): ?>
                                            <span>
											<a class="club-content__edit-button club-content__edit-button--sociallinks pull-right"
                                               href="#">
												<span class="fa fa-pencil"></span>
											</a>
										</span>
                                        <?php endif; ?>

                                        <div class="club-content__view-area">
                                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $bandage)): ?>
                                                <?php if(count($socialLinks) === 0): ?>
                                                    <em>Noch keine Links angelegt.</em>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </div>

                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $bandage)): ?>
                                            <div class="club-content__edit-area">
                                                <?php echo $__env->make('bandage.partials.edit.social-links', ['bandage' => $bandage, 'socialLinks' => $socialLinks, 'socialLinkTypes' => $socialLinkTypes], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                            </div>
                                        <?php endif; ?>
                                    </section>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 profile-cnt-details">
                                <h2>Mehr vom Verband</h2>
                                <ul>
                                    <?php if($bandage->email !== null && trim($bandage->email) !== ""): ?>
                                        <li><a href="mailto:<?php echo e($bandage->email); ?>">
                                                <span><i class="fa fa-envelope"
                                                         aria-hidden="true"></i></span><?php echo e($bandage->email); ?></a>
                                        </li>
                                    <?php endif; ?>
                                    <?php if($bandage->phone !== null && trim($bandage->phone) !== ""): ?>
                                        <li><span><i class="fa fa-phone"
                                                     aria-hidden="true"></i></span> <?php echo e($bandage->phone); ?></li>
                                    <?php endif; ?>

                                    <?php $__currentLoopData = $socialLinks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $socialLink): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($socialLink->link_type == 'googleplus'): ?>
                                            <li>
                                                <a href="<?php echo e($socialLink->url); ?>"
                                                   target="_blank">
                                                    <span><i class="fa fa-google-plus" aria-hidden="true"></i></span>
                                                    <?php echo e(ucfirst($socialLink->link_type)); ?></a>
                                            </li>
                                        <?php elseif($socialLink->link_type == 'web'): ?>
                                            <li>
                                                <a href="<?php echo e($socialLink->url); ?>"
                                                   target="_blank">
                                                    <span><i class="fa fa-globe" aria-hidden="true"></i></span>
                                                    <?php echo e($socialLink->url); ?></a>
                                            </li>
                                        <?php else: ?>
                                            <li>
                                                <a href="<?php echo e($socialLink->url); ?>"
                                                   target="_blank">
                                                    <span><i class="fa fa-<?php echo e($socialLink->link_type); ?>"
                                                             aria-hidden="true"></i></span>
                                                    <?php echo e(ucfirst($socialLink->link_type)); ?></a>
                                            </li>
                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 profile-cnt-details2">
                                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $bandage)): ?>
                                    <?php if(isset($completionProgress)): ?>
                                        <?php echo $__env->make('bandage.partials.completion-progress', ['completionProgress' => $completionProgress], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 profile-cnt-details profile-cnt-details2">
                                <h2>Profil Teilen</h2>
                                <ul class="styled">
                                    <li>
                                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo e(urlencode(route('bandage.detail', $bandage->slug))); ?>"
                                           target="_blank">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://twitter.com/intent/tweet?url=<?php echo e(urlencode(route('bandage.detail', $bandage->slug))); ?>"
                                           target="_blank" class="">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>


                    </div>

                    <div class="col-md-9 col-sm-8 proifle-post-sec">
                        <h2 id="clubname" class="clubname"><?php echo e($bandage->name); ?></h2>
                        <div class="row">
                            <div class="col-lg-8 col-sm-8 col-md-8">
                                <div class="row post-location-links">

                                    <?php if(isset($bandage->shorthand) && trim($bandage->shorthand) !== ''): ?>
                                        <div class="col-lg-6 col-sm-6">
                                            <span class="icon-area"><i class="fa fa-at"
                                                                       aria-hidden="true"></i></span> <?php echo e($bandage->shorthand); ?>

                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-md-12">
                                <div <?php if($bandage->title_color): ?> style="color:#<?php echo e($bandage->title_color); ?>"
                                     <?php endif; ?> id="post-location-links" class="row post-location-links">
                                    <?php if($bandage->fullAddress() != ''): ?>
                                        <div class="col-lg-8 col-md-8 sm-8 xs-12">
                                            <span class="icon-area"><i class="fa fa-map-marker" aria-hidden="true"></i></span> <?php echo e($bandage->fullAddress()); ?>

                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 profile-post-tabs">
                                <ul>
                                    <li>Sportarten:</li>
                                    <?php if(count($bandage->sports()->get())): ?>
                                        <?php $__currentLoopData = $bandage->sports()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $b): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($key > 6): ?>
                                                <li class="sporthide" style="display: none">
                                                    <a class="club_sports" style="background-color:#<?php echo e($bandage->title_color); ?>"><?php echo e($b->title); ?></a>
                                                </li>
                                            <?php else: ?>
                                                <li>
                                                    <a class="club_sports" style="background-color:#<?php echo e($bandage->title_color); ?>"><?php echo e($b->title); ?></a>
                                                </li>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php if(count($bandage->sports()->get()) > 7): ?>
                                            <li>
                                                <a style="background-color: #01262e;" id="sportsmore" onclick="showHiddenSports()">Mehr anzeigen</a>
                                            </li>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-offset-0" style="padding-left:18px;">
                                <?php if(count($errors) > 0): ?>
                                    <div class="flash-message flash-message--error">
                                        <strong>Es ist ein Fehler aufgetreten:</strong>
                                        <ul>
                                            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li><?php echo e($error); ?></li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                                    </div>
                                <?php endif; ?>

                                <section id="pinnwand" class="pageable-menu__page pageable-menu__page--active">
                                    <h2 class="club-content__box-headline">VERBANDS-PINNWAND
                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $bandage)): ?>
                                            <a class="pull-right"
                                               onClick="$('#add-post').toggleClass('pageable-menu__page--active');"
                                               href="#add-post">
                                                <span><i class="fa fa-plus-circle" aria-hidden="true"></i></span>
                                            </a>
                                        <?php endif; ?>
                                    </h2>

                                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $bandage)): ?>
                                        <?php echo $__env->make('bandage.partials.edit.post', ['newPost' => true], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                    <?php endif; ?>

                                    <div class="club-post__wrapper">
                                        <?php if($posts->count() > 0): ?>
                                            <div data-load-more='{"url": "<?php echo e(route('bandage.post.latest', $bandage->slug)); ?>"}'>
                                                <div data-load-more-html></div>

                                                <div class="col-md-12">
                                                    <div data-loader
                                                         class="loader__wrapper loader__wrapper--centered">
                                                        <div class="loader loader--loading"></div>
                                                        <div class="loader loader--loading"></div>
                                                    </div>
                                                </div>
                                                <?php if($posts->count() > 9): ?>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="button__center-wrapper">
                                                                <a data-load-more-button
                                                                   class="btn btn-default interessiert-btn">
                                                                    MEHR ANZEIGEN
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        <?php else: ?>
                                            <div class="club-post__item">
                                                <span class="club-post__date noborder">vereinsleben.de</span>
                                                <div class="club-post__content">
                                                    Keine Neuigkeiten vorhanden
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>


                                </section>

                                <section id="informationen"
                                         class="pageable-menu__page pageable-menu__page--active club-content club-content--information"
                                         <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $bandage)): ?> data-section-editable="true" <?php endif; ?>>

                                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $bandage)): ?>
                                        <a class="club-content__edit-button club-content__edit-button--information button button--edit button--icon button--center button--condensed pull-right"
                                           href="#">
                                            <span class="fa fa-pencil"></span>
                                        </a>
                                    <?php endif; ?>

                                    <h2 class="club-content__box-headline">Informationen</h2>

                                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $bandage)): ?>
                                        <div class="row clr-theme">
                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-6 clr1 clr-arrow">
                                                <span class="lin">Linienfarbe wählen</span>
                                                <input type="text" name="header_color" id="header_color"
                                                       class="jscolor {valueElement:'header_color', onFineChange:'setHeaderColor(this)'} clr-code"
                                                       value="<?php if($bandage->header_color): ?> <?php echo e($bandage->header_color); ?> <?php else: ?> 29c7ab <?php endif; ?>">
                                                <a href="javascript:assignVal(1)"><span class="icon-area"><i
                                                          class="fa fa-arrow-right"
                                                          aria-hidden="true"></i></span></a>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-6 clr1 clr-arrow">
                                                <span class="sch">Schriftfarbe wählen</span>

                                                <input type="text" name="title_color" id="title_color"
                                                       class="jscolor {valueElement:'title_color', onFineChange:'setTextColor(this)'} clr-code"
                                                       value="<?php if($bandage->title_color): ?> <?php echo e($bandage->title_color); ?> <?php else: ?> 29c7ab <?php endif; ?>">
                                                <a href="javascript:assignVal(2)"><span class="icon-area"><i
                                                          class="fa fa-arrow-left"
                                                          aria-hidden="true"></i></span></a>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 clr3">
                                                <button <?php if($bandage->title_color): ?> style="background:#<?php echo e($bandage->title_color); ?>"
                                                        <?php endif; ?> id="color-publish" class="color-publish"
                                                        data-bandage-color-published
                                                        data-bandage-slug='<?php echo e($bandage->slug); ?>'>Speichern
                                                </button>
                                                <span class="undo"><a href="<?php echo e($bandage->slug); ?>/color/reset"><span
                                                          class="zu">Zurücksetzen</span> <i
                                                          class="fa fa-undo"></i></a></span>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $bandage)): ?>
                                        <div class="row">
                                            <div class="club-content__edit-area club-content__box">
                                                <?php echo $__env->make('bandage.partials.edit.base-data', ['bandage' => $bandage], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $bandage)): ?>
                                        <div class="club-content__edit-area">
                                            <?php echo $__env->make('bandage.partials.edit.about-history', ['bandage' => $bandage], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                        </div>
                                    <?php endif; ?>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="tabset">
                                                <input type="radio" name="tabset" id="tab1" aria-controls="satammdaten"
                                                       checked>
                                                <label for="tab1">Stammdaten</label>
                                                <input type="radio" name="tabset" id="tab2" aria-controls="verein">
                                                <label for="tab2">Über den Verband</label>

                                                <div class="tab-panels">
                                                    <section id="satammdaten" class="tab-panel">
                                                        <div class="row">
                                                            <div class="col-lg-6 col-sm-6">
                                                                <div class="shaddow-box">
                                                                    <h3>Eckdaten</h3>
                                                                    <ul>
                                                                        <li><span class="icon-area"><i
                                                                                        class="fa fa-flag-o"
                                                                                        aria-hidden="true"></i></span>
                                                                            <span class="txt-area"><?php echo e($bandage->founded); ?>

                                                                                gegründet</span>
                                                                        </li>
                                                                        <li><span class="icon-area"><i
                                                                                        class="fa fa-users"
                                                                                        aria-hidden="true"></i></span>
                                                                            <span class="txt-area">Abonnenten <?php echo e($bandage->subcribles()->count()); ?></span>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-6 col-sm-6">
                                                                <div class="shaddow-box">
                                                                    <h3>Kontaktinfos</h3>
                                                                    <ul>
                                                                        <?php if($bandage->email !== null && trim($bandage->email) !== ""): ?>
                                                                            <li><span class="icon-area"><i
                                                                                            class="fa fa-envelope"
                                                                                            aria-hidden="true"></i></span>
                                                                                <span class="txt-area"><a
                                                                                            href="mailto:<?php echo e($bandage->email); ?>"><?php echo e($bandage->email); ?></a></span>
                                                                            </li>
                                                                        <?php endif; ?>
                                                                        <?php if($bandage->phone !== null && trim($bandage->phone) !== ""): ?>
                                                                            <li><span class="icon-area"><i
                                                                                            class="fa fa-phone"
                                                                                            aria-hidden="true"></i></span>
                                                                                <span class="txt-area"><?php echo e($bandage->phone); ?></span>
                                                                            </li>
                                                                        <?php endif; ?>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <?php if(count($socialLinks) > 0): ?>
                                                                <div class="col-lg-6 col-sm-6">
                                                                    <div class="shaddow-box">
                                                                        <h3>Web & Social Media</h3>
                                                                        <ul>
                                                                            <?php $__currentLoopData = $socialLinks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $socialLink): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                <?php if($socialLink->link_type == 'googleplus'): ?>
                                                                                    <li>
                                                                                        <a href="<?php echo e($socialLink->url); ?>"
                                                                                           target="_blank">
                                                                                            <span><i class="fa fa-google-plus"
                                                                                                     aria-hidden="true"></i></span>
                                                                                            <?php echo e(ucfirst($socialLink->link_type)); ?>

                                                                                        </a>
                                                                                    </li>
                                                                                <?php elseif($socialLink->link_type == 'web'): ?>
                                                                                    <li>
                                                                                        <a href="<?php echo e($socialLink->url); ?>"
                                                                                           target="_blank">
                                                                                            <span><i class="fa fa-globe"
                                                                                                     aria-hidden="true"></i></span>
                                                                                            <?php echo e($socialLink->url); ?></a>
                                                                                    </li>
                                                                                <?php else: ?>
                                                                                    <li>
                                                                                        <a href="<?php echo e($socialLink->url); ?>"
                                                                                           target="_blank">
                                                                                            <span><i class="fa fa-<?php echo e($socialLink->link_type); ?>"
                                                                                                     aria-hidden="true"></i></span>
                                                                                            <?php echo e(ucfirst($socialLink->link_type)); ?>

                                                                                        </a>
                                                                                    </li>
                                                                                <?php endif; ?>
                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            <?php endif; ?>
                                                        </div>
                                                    </section>

                                                    <section id="verein" class="tab-panel">
                                                        <?php if(trim($bandage->about) !== '' || trim($bandage->description) !== ''): ?>
                                                            <?php if(isset($bandage->about)): ?>
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="shaddow-box">
                                                                            <h3>Beschreibung</h3>
                                                                            <p><?php echo nl2br($bandage->about); ?></p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php endif; ?>

                                                            <?php if(isset($bandage->description)): ?>
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="shaddow-box">
                                                                            <h3>Geschichte</h3>
                                                                            <p><?php echo nl2br($bandage->description); ?></p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php endif; ?>
                                                        <?php elseif(Auth::check() && Auth::user()->isBandageAdmin($bandage)): ?>
                                                            <div class="club-content__box">
                                                                Keine Informationen eingetragen
                                                            </div>
                                                        <?php endif; ?>
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $bandage)): ?>
                                        <div class="club-content__edit-area">
                                            
                                        </div>
                                    <?php endif; ?>
                                </section>

                                <section id="ansprechpartner" class="pageable-menu__page pageable-menu__page--active">






























































                                    <h2 class="club-content__box-headline">Ansprechpartner
                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $bandage)): ?>
                                            <a class="pull-right"
                                               onClick="$('#add-contact-person').toggleClass('pageable-menu__page--active');"
                                               href="#add-contact-person">
                                                <span><i class="fa fa-plus-circle" aria-hidden="true"></i></span>
                                            </a>
                                        <?php endif; ?>
                                    </h2>

                                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $bandage)): ?>
                                        <?php echo $__env->make('bandage.partials.edit.contact-person', ['newContact' => true], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                    <?php endif; ?>

                                    <?php if($contactPersons->count() > 0): ?>
                                        <div data-load-more='{"url": "<?php echo e(route('contact-person.list', $bandage->slug)); ?>"}'>
                                            <div data-load-more-html></div>

                                            <div class="col-md-12">
                                                <div data-loader
                                                     class="loader__wrapper loader__wrapper--centered">
                                                    <div class="loader loader--loading"></div>
                                                    <div class="loader loader--loading"></div>
                                                </div>
                                            </div>
                                            <?php if($contactPersons->count() > 9): ?>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="button__center-wrapper">
                                                            <a data-load-more-button
                                                               class="btn btn-default interessiert-btn">
                                                                MEHR ANZEIGEN
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    <?php else: ?>
                                        <div class="club-post__item">
                                            <span class="club-post__date noborder">Hier können Sie die Ansprechpartner des Verbands hinzufügen.</span>
                                        </div>
                                    <?php endif; ?>
                                </section>

                                <section id="events" class="pageable-menu__page pageable-menu__page--active">
                                    <h2 class="club-content__box-headline">KOMMENDE EVENTS
                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $bandage)): ?>
                                            <a class="pull-right"
                                               onClick="$('#add-event').toggleClass('pageable-menu__page--active');"
                                               href="#add-event">
                                                <span><i class="fa fa-plus-circle" aria-hidden="true"></i></span>
                                            </a>
                                        <?php endif; ?>
                                    </h2>

                                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $bandage)): ?>
                                        <?php echo $__env->make('bandage.partials.edit.event', ['newEvent' => true], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                    <?php endif; ?>

                                    <div class="club-post__wrapper">
                                        <?php if($events->count() > 0): ?>
                                            <div data-load-more='{"url": "<?php echo e(route('bandage.event.latest', $bandage->slug)); ?>"}'>
                                                <div data-load-more-html></div>

                                                <div class="col-md-12">
                                                    <div data-loader
                                                         class="loader__wrapper loader__wrapper--centered">
                                                        <div class="loader loader--loading"></div>
                                                        <div class="loader loader--loading"></div>
                                                    </div>
                                                </div>
                                                <?php if($events->count() > 9): ?>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="button__center-wrapper">
                                                                <a data-load-more-button
                                                                   class="btn btn-default interessiert-btn">
                                                                    MEHR ANZEIGEN
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        <?php else: ?>
                                            <div class="club-post__item">
                                                <span class="club-post__date noborder">vereinsleben.de</span>
                                                <div class="club-post__content">
                                                    Keine Events vorhanden
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </section>

                                <section id="netzwerk" class="pageable-menu__page pageable-menu__page--active">
                                    <h2 class="club-content__box-headline">EUER NETZWERK</h2>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="tabset">
                                                <input type="radio" name="user_type" value="b_subs" id="subs"
                                                       aria-controls="b_subs" checked="">
                                                <label for="subs">Abonnenten</label>

                                                <input type="radio" name="user_type" value="b_vereinen" id="vereinen"
                                                       aria-controls="b_vereinen">
                                                <label for="vereinen">Vereine</label>

                                                <input type="radio" name="user_type" value="verbände" id="verbände"
                                                       aria-controls="b_verbände">
                                                <label for="verbände" class="last">Verbände</label>

                                                <div id="network-search-area" class="row network-search-area">
                                                    <div class="col-lg-5 col-sm-6">
                                                        <div class="input-group">
															<span class="input-group-addon">
																<i class="fa fa-search" aria-hidden="true"></i>
															</span>
                                                            <input type="text" name="search" id="search"
                                                                   class="form-control" placeholder="Namen Suchen ...">
                                                        </div>
                                                    </div>
                                                    
                                                        
                                                                    
                                                                    
                                                    
                                                </div>

                                                <div class="tab-panels">

                                                    <section id="b_subs" class="tab-panel">
                                                        <?php echo $__env->make('bandage.partials.fan', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                                    </section>

                                                    <section id="b_vereinen" class="tab-panel">
                                                        <?php echo $__env->make('bandage.partials.vereinen', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                                    </section>

                                                    <section id="b_verbände" class="tab-panel">
                                                        <?php if($parentBandage != null): ?>
                                                            <?php echo $__env->make('bandage.partials.bandage-card', ['bandage' => $parentBandage], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                                        <?php endif; ?>
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>

                                
                                    
                                    
                                        
                                    
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>
    <script src="<?php echo e(asset(('js/jscolor.js'))); ?>"></script>
    <script>
        function showHiddenSports(){

            var sportarray = document.getElementsByClassName('sporthide');

            if(document.getElementById("sportsmore").text == "Weniger anzeigen"){

                console.log('Weniger anzeigen');

                Array.prototype.forEach.call(sportarray, function(el){
                    el.setAttribute("style", "display:none;");
                });

                document.getElementById("sportsmore").text = "Mehr anzeigen";
            }
            else{

                Array.prototype.forEach.call(sportarray, function(el){
                    el.removeAttribute("style", "display");
                });

                document.getElementById("sportsmore").text = "Weniger anzeigen";
            }

            // document.getElementById("sportsmore").style.display = "none";
        }

        function setHeaderColor(picker) {
            document.getElementById('about-strip').style["boxShadow"] = '2px 2px 6px #' + picker.toString();
            document.getElementById('profile-pic-icon').style.background = '#' + picker.toString();
        }

        function setTextColor(picker, colorcode = '') {
            var color = picker.toString();
            if (colorcode) {
                color = colorcode;
            }
            document.getElementById('clubname').style.color = '#' + color;
            document.getElementById('post-location-links').style.color = '#' + color;
            document.getElementById('color-publish').style.background = '#' + color;
            document.getElementById('bin_subcribe').style.color = '#' + color;

            if($('#bin_subcribe').hasClass('btn--active')) {
                $('#bin_subcribe').removeClass('btn--active');
            }

            var clubsports = document.getElementsByClassName("club_sports");
            for (i = 0; i < clubsports.length; i++) {
                clubsports[i].style.backgroundColor = '#' + color;
            }
            var plusicons = document.getElementsByClassName("fa-plus-circle");
            for (i = 0; i < plusicons.length; i++) {
                plusicons[i].style.color = '#' + color;
            }
            var menus = document.getElementsByClassName("club-detail__menu-list-link active");
            for (i = 0; i < menus.length; i++) {
                menus[i].style.color = '#' + color;
                menus[i].style.borderLeft = '3px solid #' + color;
            }
            var css = '.btn--fan:after {background:#' + color + ' url(/static_images/fan.png)}';
            var css = css + '.btn--member:after {background:#' + color + ' url(/static_images/fan.png)}';
            var css = css + '.btn--fan:hover:after {background:#' + color + ' url(/static_images/fan-active.png)}';
            var css = css + '.btn--member:hover:after {background:#' + color + ' url(/static_images/fan-active.png)}';
            var css = css + '.profile-tabs li a:hover{ color: #' + color + ';border-left:3px solid #' + color + '}';
            var style = document.createElement('style');
            if (style.styleSheet) {
                style.styleSheet.cssText = css;
            } else {
                style.appendChild(document.createTextNode(css));
            }
            document.getElementsByTagName('body')[0].appendChild(style);
        }

        function assignVal(val) {
            if (val == 1) {
                titlecolor = document.getElementById('header_color').value;
                document.getElementById('title_color').value = titlecolor;
                document.getElementById('title_color').style.backgroundColor = '#' + titlecolor;
                setTextColor('', titlecolor);
            } else {
                document.getElementById('header_color').value = document.getElementById('title_color').value;
                document.getElementById('header_color').style.backgroundColor = '#' + document.getElementById('title_color').value;
                document.getElementById('about-strip').style["boxShadow"] = '2px 2px 6px #' + document.getElementById('title_color').value;
                document.getElementById('profile-pic-icon').style.background = '#' + document.getElementById('title_color').value;
            }
        }
    </script>
    <?php if($bandage->header_color OR $bandage->title_color): ?>
        <style type="text/css" rel="stylesheet">
            <?php if($bandage->title_color): ?>
		    .btn--fan:after {
                background: <?php echo e('#'.$bandage->title_color); ?>  url('/static_images/fan.png');
            }

            .btn--fan:hover:after {
                background: <?php echo e('#'.$bandage->title_color); ?>  url('/static_images/fan-active.png')
            }

            .btn--member:after {
                background: <?php echo e('#'.$bandage->title_color); ?>  url('/static_images/fan.png');
            }

            .btn--member:hover:after {
                background: <?php echo e('#'.$bandage->title_color); ?>  url('/static_images/fan-active.png');
            }

            .btn--active.btn--fan:after {
                background: <?php echo e('#'.$bandage->title_color); ?>  url('/static_images/fan-active.png');
            }

            .btn--active.btn--member:after, .btn--active.btn--member:focus:after {
                background: <?php echo e('#'.$bandage->title_color); ?>  url('/static_images/fan-active.png');
            }

            .btn--active.btn--member:hover:after {
                background: <?php echo e('#'.$bandage->title_color); ?>  url('/static_images/fan-active.png');
            }

            .profile-tabs li a.active {
                color: #<?php echo e($bandage->title_color); ?>;
                border-left: 3px solid #<?php echo e($bandage->title_color); ?>;
            }

            .profile-tabs li a:hover {
                color: #<?php echo e($bandage->title_color); ?>;
                border-left: 3px solid #<?php echo e($bandage->title_color); ?>;
            }

            <?php endif; ?>
	        <?php if($bandage->title_color): ?>
		    .profile-name img {
                background: #<?php echo e($bandage->header_color); ?>;
            }
            <?php endif; ?>
        </style>
    <?php endif; ?>
    <?php $__env->startPush('scripts'); ?>
        <script>
            $(document).ready(function () {
                $('#button-show-form').click(function(){
                    $(this).addClass('hide');
                    $('.form').removeClass('hide');
                });

                $('.reply').click(function(){
                    $('.reply').removeClass('active');
                    $(this).addClass('active');

                    let review_id = $(this).data('review-id');
                    $('input#parent_id').val(review_id);

                    $('#answer-form .form').removeClass('hide');

                    $([document.documentElement, document.body]).animate({
                        scrollTop: $("#answer-form").offset().top - 500
                    }, 200);
                });

                $('#scroll-to-request-admin').click(function(){
                    $([document.documentElement, document.body]).animate({
                        scrollTop: $("#request-admin").offset().top - 500
                    }, 500);
                });
            })
        </script>
    <?php $__env->stopPush(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>