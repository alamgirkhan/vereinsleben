<div class="stoeer_adds">
    <div class="container" style="padding-top: 40px;">
        <div class="image-hor campaign-image" id="vlStroeerDivId" ></div>
    </div>
</div>
<br><br>
<?php $__env->startPush('scripts'); ?>
<script src="https://cdn.stroeerdigitalgroup.de/metatag/live/OMS_vereinsleben/metaTag.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        SDG.Publisher.setZone('homepage');
        SDG.Publisher.registerSlot('banner', 'vlStroeerDivId').load();
        SDG.Publisher.finalizeSlots();
    </script>
    <script>
        window.setTimeout(function() {
            SDG.Publisher.loadAllSlots(true)
        },60000);
    </script>
<?php $__env->stopPush(); ?>