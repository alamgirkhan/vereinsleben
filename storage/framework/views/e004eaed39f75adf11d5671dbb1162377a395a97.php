<?php $__env->startSection('content'); ?>
    <section data-token="<?php echo e(csrf_token()); ?>">
        <input type="hidden" name="search_id" id="search_id" value="<?php echo e($user->id); ?>"/>
        <div class="container-fluid">
            <div class="row">
                <?php echo $__env->make('user.profile.partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
        </div>
        <section class="about-strip userprofile-main">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4"></div>
                    <div class="col-md-9 col-sm-8 col-xs-8 proifle-top-links profile-desk ">
                        <div class="row">
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-3 count-tabs">Besucher<br><strong><?php echo e($user->views == null ? 0 : $user->views); ?></strong>
                                    </div>
                                    <div class="col-md-4 count-tabs no-border">
                                        Freunde<br><strong><?php echo e($user->getFriendsCount()); ?></strong></div>
                                </div>
                            </div>
                            <?php if($me && !$me->hasSentFriendRequestTo($user) && !$me->isFriendWith($user) && ($me->id != $user->id)): ?>
                                <div class="col-md-5">
                                    <div class="folgen-btn folgen-padding">
                                        <?php if(Auth::check() && !$me->isProfileCompleted()): ?>
                                            <a href="#"
                                               data-modal-url="<?php echo e(route('user.profile.wizard')); ?>"
                                               data-modal="team-member">FREUNDSCHAFTSANFRAGE SENDEN</a>

                                        <?php else: ?>
                                        <a href="javascript:sendRequest('<?php echo e($me->username); ?>', '<?php echo e($user->username); ?>', 'send-friend-request', '<?php echo e($user->id); ?>');">
                                            <i class="fa fa-plus-circle" aria-hidden="true"></i><span
                                                    id="send-friend-request_<?php echo e($user->id); ?>">FREUNDSCHAFTSANFRAGE SENDEN</span>
                                        </a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if($me && $me->hasSentFriendRequestTo($user)): ?>
                                <div class="col-md-5">
                                    <div class="friend-btn">Anfrage gesendet</div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-md-9 col-xs-8 proifle-top-links profile-mob">
                        <div class="row">

                            <?php if($me && !$me->hasSentFriendRequestTo($user) && !$me->isFriendWith($user) && ($me->id != $user->id)): ?>
                                <div class="col-md-5 col-xs-12">
                                    <div class="folgen-btn folgen-padding">
                                        <?php if(Auth::check() && !$me->isProfileCompleted()): ?>
                                            <a href="#"
                                               data-modal-url="<?php echo e(route('user.profile.wizard')); ?>"
                                               data-modal="team-member">FREUNDSCHAFTSANFRAGE SENDEN</a>

                                        <?php else: ?>
                                            <a href="javascript:sendRequest('<?php echo e($me->username); ?>', '<?php echo e($user->username); ?>', 'send-friend-request', '<?php echo e($user->id); ?>');">
                                            <i class="fa fa-plus-circle" aria-hidden="true"></i><span
                                                    id="send-friend-request_<?php echo e($user->id); ?>">FREUNDSCHAFTSANFRAGE SENDEN</span>
                                        </a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if($me && $me->hasSentFriendRequestTo($user)): ?>
                                <div class="col-md-5">
                                    <div class="friend-btn">Anfrage gesendet</div>
                                </div>
                            <?php endif; ?>
                            <div class="col-md-7 col-xs-12">
                                <div class="row">
                                    <div class="col-md-3 count-tabs">Besucher<br><strong><?php echo e($user->views == null ? 0 : $user->views); ?></strong>
                                    </div>
                                    <div class="col-md-4 count-tabs no-border">
                                        Freunde<br><strong><?php echo e($user->getFriendsCount()); ?></strong></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <div class="row proife-lft">
                        <div class="col-lg-12">
                            <div class="row">
                                <?php echo $__env->make('user.profile.partials.avatar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                            </div>
                            <?php if(Auth::user() && policy($me)->showProfile($me, $user)): ?>
                                <div class="row">
                                    <div class="col-lg-12 profile-tabs userprofile-nav">
                                        <aside>
                                            <nav>
                                                <ul class="club-detail__menu profile-tabs profile-menu-desk"
                                                    data-pageable-menu>
                                                    <li class="club-detail__menu-list-item active">
                                                        <a class="club-detail__menu-list-link active" href="#pinnwand">
                                                        <span><i class="fa fa-newspaper-o"
                                                                 aria-hidden="true"></i></span> Pinnwand
                                                        </a>
                                                    </li>
                                                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $user)): ?>
                                                        <li class="club-detail__menu-list-item">
                                                            <a class="club-detail__menu-list-link"
                                                               href="#informationen">
                                                                <span><i class="fa fa-info"
                                                                         aria-hidden="true"></i></span>
                                                                Informationen
                                                            </a>
                                                        </li>
                                                    <?php endif; ?>
                                                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $user)): ?>
                                                        <li class="club-detail__menu-list-item">
                                                            <a class="club-detail__menu-list-link" href="#passwort">
                                                            <span><i class="fa fa-unlock-alt"
                                                                     aria-hidden="true"></i></span>
                                                                Passwort ändern
                                                            </a>
                                                        </li>
                                                    <?php endif; ?>
                                                    <li class="club-detail__menu-list-item">
                                                        <a class="club-detail__menu-list-link" href="#sportvita">
                                                            <span class="sportvita-icon"></span>&nbsp;Sport-Vita
                                                        </a>
                                                    </li>

                                                    <li class="club-detail__menu-list-item">
                                                        <a class="club-detail__menu-list-link" href="#netzwerk">
                                                            <span><i class="fa fa-users" aria-hidden="true"></i></span>
                                                            Netzwerk
                                                            <?php if($me->id == $user->id): ?>
                                                                <?php if( count($user->getFriendRequests()) > 0 ): ?>
                                                                    &nbsp;
                                                                    <div class="friend-request-circle">
                                                                        <?php echo e(count($user->getFriendRequests())); ?>

                                                                    </div>
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </a>
                                                    </li>

                                                    <li class="club-detail__menu-list-item">
                                                        <a class="club-detail__menu-list-link" href="#fotos">
                                                            <span><i class="fa fa-camera" aria-hidden="true"></i></span>
                                                            Fotos
                                                        </a>
                                                    </li>
                                                    
                                                        
                                                            
                                                                
                                                                         
                                                                
                                                            
                                                        
                                                    
                                                </ul>

                                                <ul class="club-detail__menu profile-tabs profile-menu-mob"
                                                    data-pageable-menu>
                                                    <li class="club-detail__menu-list-item active">
                                                        <a class="club-detail__menu-list-link active" href="#pinnwand">
                                                        <span><i class="fa fa-newspaper-o"
                                                                 aria-hidden="true" href="#pinnwand"></i></span>
                                                        </a>
                                                    </li>
                                                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $user)): ?>
                                                        <li class="club-detail__menu-list-item">
                                                            <a class="club-detail__menu-list-link"
                                                               href="#informationen">
                                                        <span><i class="fa fa-info" aria-hidden="true"
                                                                 href="#informationen"></i></span>
                                                            </a>
                                                        </li>
                                                    <?php endif; ?>
                                                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $user)): ?>
                                                        <li class="club-detail__menu-list-item">
                                                            <a class="club-detail__menu-list-link" href="#passwort">
                                                            <span><i class="fa fa-unlock-alt"
                                                                     aria-hidden="true" href="#passwort"></i></span>
                                                            </a>
                                                        </li>
                                                    <?php endif; ?>
                                                    <li class="club-detail__menu-list-item">
                                                        <a class="club-detail__menu-list-link" href="#sportvita">
                                                            <!--<span><i class="fa fa-spotify" aria-hidden="true"
                                                                     href="#sportvita"></i></span>-->
                                                            <span class="sportvita-icon" href="#sportvita"></span>
                                                        </a>
                                                    </li>

                                                    <li class="club-detail__menu-list-item">
                                                        <a class="club-detail__menu-list-link" href="#netzwerk">
                                                        <span><i class="fa fa-users" aria-hidden="true"
                                                                 href="#netzwerk"></i></span>
                                                        </a>
                                                    </li>

                                                    <li class="club-detail__menu-list-item">
                                                        <a class="club-detail__menu-list-link" href="#fotos">
                                                        <span><i class="fa fa-camera" aria-hidden="true"
                                                                 href="#fotos"></i></span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </nav>
                                        </aside>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="row hidden-xs">
                        <div class="col-lg-12">
                            <?php if($socialLinks->count() > 0  || Auth::user() && policy($me)->showProfile($me, $user)): ?>
                                <section class="club-detail__menu-list-item club-detail__menu-list-item--static"
                                         <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $user)): ?> data-section-editable="true" <?php endif; ?>>

                                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $user)): ?>
                                        <span>
											<a class="club-content__edit-button club-content__edit-button--sociallinks pull-right edit-links"
                                               href="#">
												<span class="fa fa-pencil"></span>
											</a>
										</span>
                                    <?php endif; ?>

                                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $user)): ?>
                                        <div id="userlinks" class="club-content__edit-area">
                                            <?php echo $__env->make('user.profile.partials.edit.social-links', ['club' => $user, 'socialLinks' => $socialLinks, 'socialLinkTypes' => $socialLinkTypes], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                        </div>
                                    <?php endif; ?>
                                    <div class="club-content__view-area">
                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $user)): ?>
                                            <?php if(count($socialLinks) === 0): ?>
                                                <em>Noch keine Links angelegt.</em>
                                            <?php else: ?>
                                                <em>Links bearbeiten</em>
                                            <?php endif; ?>
                                        <?php endif; ?>

                                    </div>
                                </section>


                                <div class="row hidden-xs">
                                    <div class="col-lg-12 profile-cnt-details">
                                        <h2>Mehr vom User</h2>
                                        <ul>

                                            <?php $__currentLoopData = $socialLinks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $socialLink): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($socialLink->link_type == 'googleplus'): ?>
                                                    <li>
                                                        <a href="<?php echo e($socialLink->url); ?>"
                                                           target="_blank">
                                                            <span><i class="fa fa-google-plus"
                                                                     aria-hidden="true"></i></span>
                                                            <?php echo e(ucfirst($socialLink->link_type)); ?></a>
                                                    </li>
                                                <?php elseif($socialLink->link_type == 'web'): ?>
                                                    <li>
                                                        <a href="<?php echo e($socialLink->url); ?>"
                                                           target="_blank">
                                                            <span><i class="fa fa-globe" aria-hidden="true"></i></span>
                                                            <?php echo e(str_limit($socialLink->url, $limit = 22, $end = '...')); ?>

                                                        </a>
                                                    </li>
                                                <?php else: ?>
                                                    <li>
                                                        <a href="<?php echo e($socialLink->url); ?>"
                                                           target="_blank">
                                                    <span><i class="fa fa-<?php echo e($socialLink->link_type); ?>"
                                                             aria-hidden="true"></i></span>
                                                            <?php echo e(ucfirst($socialLink->link_type)); ?></a>
                                                    </li>
                                                <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                </div>

                <div class="col-md-9 col-sm-8 col-xs-12 proifle-post-sec">
                    <?php if($me && ($me->isFriendWith($user) || ($me->id == $user->id || $me->role_id == 2))): ?>
                        <div class="row">
                            <div class="col-md-offset-0 pfl-info">
                                <?php if(count($errors) > 0): ?>
                                    <div class="flash-message flash-message--error">
                                        <strong>Es ist ein Fehler aufgetreten:</strong>
                                        <ul>
                                            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li><?php echo e($error); ?></li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                                    </div>
                                <?php endif; ?>

                                <section id="pinnwand" class="pageable-menu__page pageable-menu__page--active">
                                    <h2 class="clubname"><?php echo e($user->fullName()); ?></h2>
                                    <h2 class="club-content__box-headline">PINNWAND</h2>
                                    <div class="club-post__wrapper">
                                        <?php echo $__env->make('user.profile.partials.post.list', ['user' => $user, 'posts' => $posts], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                    </div>
                                </section>

                                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $user)): ?>
                                    <section id="informationen" class="pageable-menu__page pageable-menu__page--active">
                                        <h2><?php echo e($user->fullName()); ?></h2>
                                        <h2 class="club-content__box-headline">Informationen</h2>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                    <div class="col-lg-6 col-md-6 col-sm-6  col-xs-12">
                                                        <div class="shaddow-box">
                                                            <ul>
                                                                <li><span class="icon-area"><i class="fa fa-envelope"
                                                                                               aria-hidden="true"></i></span>
                                                                    <span class="txt-area"><a
                                                                                href="mailto:<?php echo e($user->email); ?>"><?php echo e($user->email); ?></a></span>
                                                                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $user)): ?>
                                                                        <button class="btn btn--inline btn--icon animation pull-right edit-email">
                                                                            <i class="fa fa-pencil"
                                                                               aria-hidden="true"></i>
                                                                        </button><br>
                                                                        <!-- skhan: User hat die Möglichkeit sein Konto zu löschen -->
                                                                        <a href="#"
                                                                           class="btn btn--red"
                                                                           data-modal-url=" <?php echo e(route('account-delete-request', $user->username)); ?>"
                                                                           data-modal="team-member"
                                                                           style="color: white;">Konto
                                                                            löschen</a>
                                                                    <?php endif; ?>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $user)): ?>
                                            <?php echo $__env->make('user.edit-email', ['user' => $user] , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                        <?php endif; ?>
                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $user)): ?>
                                            <?php echo $__env->make('user.edit-profile', ['user' => $user] , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                        <?php endif; ?>
                                    </section>
                                <?php endif; ?>

                                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $user)): ?>
                                    <section id="passwort" class="pageable-menu__page pageable-menu__page--active">
                                        <h2><?php echo e($user->fullName()); ?></h2>
                                        <h2 class="club-content__box-headline">Passwort ändern</h2>
                                        <?php echo $__env->make('user.edit-password', ['user' => $user] , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                    </section>
                                <?php endif; ?>

                                <section id="sportvita" class="pageable-menu__page pageable-menu__page--active">
                                    <h2><?php echo e($user->fullName()); ?></h2>
                                    <h2 class="club-content__box-headline">Sport-Vita</h2>
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="tabset">
                                                <input type="radio" name="tabset" checked id="tab2"
                                                       aria-controls="erfolge">
                                                <label for="tab2">Vereine</label>

                                                <input type="radio" name="tabset" id="tab3" aria-controls="interessen">
                                                <label for="tab3">Interessen</label>

                                                <div class="tab-panels">
                                                    <section id="erfolge" class="tab-panel">
                                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $user)): ?>
                                                            <div class="row" id="suggestedclub" style="display:none;">
                                                                <div class="col-xs-12 input-hint pull-right">
                                                                    Nicht gefunden?
                                                                    <a href="#" class=""
                                                                       data-modal-url1="<?php echo e(route('user.club.wizard')); ?>"
                                                                       data-modal1="team-member" id="clubwizard">
                                                                        Verein vorschlagen
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><a
                                                                            class="pull-right profile-plus-icon"
                                                                            data-sport-career-add>
                                                                        <i class="fa fa-plus-circle"
                                                                           aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                        <?php if($sportCareers->count() > 0): ?>
                                                            <div class="row">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="well">
                                                                        <?php echo $__env->make('user.profile.partials.sport-career.list', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php else: ?>
                                                            <div class="row">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    
                                                                </div>
                                                            </div>
                                                            <?php endif; ?>
                                                    </section>

                                                    <section id="interessen" class="tab-panel">
                                                        <div class="row">
                                                            <div class="col-lg-12 col-md-12 col-sn-12 col-xs-12">
                                                                <div class="well">
                                                                    <div id="sport-interest" class="profile-interests"
                                                                         data-sport-interest-edit-url="<?php echo e(route('user.sport.edit', $user->id)); ?>"
                                                                         data-interest-url="<?php echo e(route('user.sport', $user->id)); ?>">

                                                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $user)): ?>
                                                                            <button data-edit-sport-interest
                                                                                    class="btn btn--inline btn--icon animation pull-right sport-edit">
                                                                                <i class="fa fa-pencil"
                                                                                   aria-hidden="true"></i>
                                                                            </button>
                                                                        <?php endif; ?>
                                                                        <div data-sport-interest-wrapper
                                                                             data-sport-interest-template-url="<?php echo e(route('user.sport.template', $user->id)); ?>"
                                                                             data-sport-interest-edit-url="<?php echo e(route('user.sport.edit', $user->id)); ?>">
                                                                            <?php echo $__env->make('user.profile.partials.sport-interest', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>

                                <section id="netzwerk" class="pageable-menu__page pageable-menu__page--active">
                                    <h2><?php echo e($user->fullName()); ?></h2>
                                    <h2 class="club-content__box-headline">Netzwerk</h2>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="tabset">
                                                <input type="radio" name="user_type" value="friend" id="tab4"
                                                       aria-controls="friend" checked>
                                                <label for="tab4">Freunde</label>
                                                <input type="radio" name="user_type" value="u_vereine" id="tab5"
                                                       aria-controls="vereine">
                                                <label for="tab5">Vereine</label>

                                                <div class="row network-search-area">
                                                    <div class="col-lg-5 col-sm-6">
                                                        <div class="input-group">
													<span class="input-group-addon">
													   <i class="fa fa-search" aria-hidden="true"></i>
													</span>
                                                            <input type="text" id="search" name="search"
                                                                   class="form-control"
                                                                   placeholder="in meinem Netzwerk finden">
                                                        </div>
                                                    </div>
                                                    
                                                    
                                                    
                                                    
                                                    
                                                </div>

                                                <div class="tab-panels">
                                                    <section id="friend" class="tab-panel">
                                                        <?php echo $__env->make('user.partials.friend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                                    </section>

                                                    <section id="u_vereine" class="tab-panel">
                                                        <?php echo $__env->make('user.partials.vereine', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>

                                <section id="fotos" class="pageable-menu__page pageable-menu__page--active">
                                    <h2><?php echo e($user->fullName()); ?></h2>
                                    <h2 class="club-content__box-headline">Fotos</h2>
                                    <?php echo $__env->make('user.profile.partials.post.image', ['user' => $user, 'posts' => $posts], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </section>
                            </div>
                        </div>
                    <?php else: ?>
                        <h2><?php echo e($user->fullName()); ?></h2>
                    <?php endif; ?>
                </div>
            </div>
        </div>

    </section>

    <?php if(Auth::check()): ?>
        <?php echo $__env->make('user.profile.modal.profile-info-wizard', ['me' => $me, 'state' => $state], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('post-js'); ?>
    <script src="<?php echo e(asset(('js/user-profile.js'))); ?>"></script>
    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>