<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('admin.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <section class="admin__overview">
        <div class="container">
            <div class="row admin__section">
                <div class="col-xs-12">
                    <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">Slider</h1>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <a href="<?php echo e(route('admin.slider.add')); ?>"
                           class="button button--icon button--full-width button--center button--light">
                            <span class="fa fa-plus"></span> Slider hinzufügen
                        </a>
                    </div>
                </div>
            </div>
            <table id="admin-table" class="admin__table" data-token="<?php echo e(csrf_token()); ?>">
                <thead>
                <tr>
                    <th>ID&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th>Titel&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th>Image</th>
                    <th>Pos&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                    <th>Veröffentlicht</th>
                    <th>Optionen</th>
                </tr>
                </thead>
                <tbody>
                <?php $__currentLoopData = $sliders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $sliderItem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($sliderItem->id); ?></td>
                        <td><?php echo e($sliderItem->title); ?></td>
                        <td><img width="100" src="<?php echo e(asset($sliderItem->slider_image->url('original'))); ?>" alt=""></td>
                        <td><?php echo e($sliderItem->position); ?></td>
                        <td>
                            <?php if($sliderItem->published == 1): ?>
                                <i class="fa fa-circle" style="color: green;"></i>
                            <?php else: ?>
                                <i class="fa fa-circle" style="color: red;"></i>
                            <?php endif; ?>
                        </td>
                        <td>
                            <div class="admin__table-option">
                                <a href="<?php echo e(route('admin.slider.edit', $sliderItem->id)); ?>" target="_blank"><span
                                            class="fa fa-edit"></span></a>
                                <a onclick="return confirm('Möchtest du diese Aufzeichnung wirklich löschen?')"
                                   href="<?php echo e(route('admin.slider.destroy', $sliderItem->id)); ?>">
                                    <span class="fa fa-trash"></span>
                                </a>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>