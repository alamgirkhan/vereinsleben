<div class="row">
    <div class="col-lg-12 col-md-12 col-sn-12 col-xs-12 konto">
        <h2 class="user-dashboard__subheadline">Konto einstellungen</h2>
        <?php if($errors->count() > 0): ?>
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li style="color:red;"><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        <?php endif; ?>
        <div class="row">
            <?php echo Form::model($user, ['route' => 'user.profile.update', 'method' => 'POST']); ?>



            <div class="col-md-12">
                <?php echo Form::label('gender', 'Anrede', ['class' => 'input-label']); ?>

                <div class="input-group">
                    <?php echo Form::select('gender', ['' => 'Bitte wählen', 'male' => 'Herr', 'female' => 'Frau'], null, ['required' => 'required', 'class' => 'select']); ?>

                </div>
            </div>

            <div class="col-md-12">
                <div class="row">
                    <div class="input-group">
                        <div class="col-md-4">
                            <?php echo Form::label('firstname', 'Vorname(n)', ['class' => 'input-label']); ?>

                            <?php echo Form::text('firstname', null, ['required' => 'required', 'class' => 'input', 'placeholder' => 'Vorname(n)', 'pattern' => '^[a-zA-ZäöüÄÖÜß\-. ]+$']); ?>

                        </div>
                        <div class="col-md-4">
                            <?php echo Form::label('lastname', 'Nachname', ['class' => 'input-label']); ?>

                            <?php echo Form::text('lastname', null, ['required' => 'required', 'class' => 'input', 'placeholder' => 'Nachname', 'pattern' => '^^[a-zA-ZäöüÄÖÜß\-. ]+$']); ?>

                            
                        </div>
                        <div class="col-md-4">
                            <?php echo Form::label('nickname', 'Spitzname', ['class' => 'input-label']); ?>

                            <?php echo Form::text('nickname', null, ['class' => 'input', 'placeholder' => 'Spitzname']); ?>

                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <?php echo Form::label('birthday', 'Geburtsdatum', ['class' => 'input-label']); ?>

                <div class="row">
                    <div class="input-group geburtsdatum">
                        <div class="col-md-4">
                            <?php echo Form::select('birthday-day',  (array('' => 'Tag') + array_combine(range(1, 31), range(1, 31))), isset($user->birthday->day) ? $user->birthday->day : null, ['required' => 'required', 'class' => 'select']); ?>

                        </div>
                        <div class="col-md-4">
                            <?php echo Form::select('birthday-month',  (array('' => 'Monat') + array_combine(range(1, 12), range(1, 12))), isset($user->birthday->month) ? $user->birthday->month : null, ['required' => 'required', 'class' => 'select']); ?>

                        </div>
                        <div class="col-md-4">
                            <?php echo Form::select('birthday-year',  (array('' => 'Jahr') + array_combine(range(1900, date('Y')), range(1900, date('Y')))), isset($user->birthday->year) ? $user->birthday->year : null, ['required' => 'required', 'class' => 'select']); ?>

                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                <?php echo Form::label('street', 'Straße', ['class' => 'input-label']); ?>

                <div class="input-group">
                    <?php echo Form::text('street', null, ['class' => 'input', 'placeholder' => 'Straße', 'pattern' => '^(([a-zA-ZäöüÄÖÜß]{2,})([-. ]+)?)+']); ?>

                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <?php echo Form::label('street', 'Hausnummer', ['class' => 'input-label']); ?>

                <div class="input-group">
                    <?php echo Form::text('house_number', null, ['class' => 'input', 'placeholder' => 'Hausnummer', 'id' => 'hnr', 'pattern' => '^[1-9][0-9]?[0-9]?[A-Za-z]{0,2}?']); ?>

                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="input-group">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <?php echo Form::label('zip', 'Postleitzahl', ['class' => 'input-label']); ?>

                            <?php echo Form::text('zip', null, ['class' => 'input', 'placeholder' => 'Postleitzahl', 'pattern' => '^([0-9]{4,5})$']); ?>

                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                            <?php echo Form::label('city', 'Ort', ['class' => 'input-label']); ?>

                            <?php echo Form::text('city', null, ['class' => 'input', 'placeholder' => 'Ort', 'pattern' => '^(([a-zA-ZäöüÄÖÜß]{2,})([-. ]+)?)+']); ?>

                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php echo Form::label('federal_state', 'Bundesland', ['class' => 'input-label']); ?>

                <div class="input-group">
                    <?php echo Form::select('federal_state', array('' => 'Bitte wählen')+$federal_states->pluck('name', 'name')->all(), null, ['class' => 'select', 'id' => 'state']); ?>

                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="input-group nwsltr">
                    <?php echo Form::checkbox('newsletter', '1', ($user->newsletter == '1'), ['id' => 'newsletter', 'class' => 'input-checkbox']); ?>

                    <?php echo Form::label('newsletter', 'Ich möchte kostenfrei den Newsletter erhalten', ['class' => 'input-checkbox__label']); ?>

                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="input-group msge">
                    <?php echo Form::checkbox('messages', '1', ($user->messages == '1'), ['id' => 'messages', 'class' => 'input-checkbox']); ?>

                    <?php echo Form::label('messages', 'Ich möchte Systembenachrichtigungen erhalten', ['class' => 'input-checkbox__label']); ?>

                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="input-group">
                    <?php echo Form::submit('Speichern', array('class' => 'button button--light button--light-white button--center button--full-width')); ?>

                </div>
            </div>

            <?php echo Form::close(); ?>

            
        </div>
    </div>
</div>


<style>

    /*input:invalid{*/
        /*border: 1px solid red!important;*/
    /*}*/

    input:invalid:not(:focus):not(:placeholder-shown) {

        border: 1px solid red!important;
    }

</style>