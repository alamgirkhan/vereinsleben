<?php
$classes = [];
$type = 'default';
if (isset($post) && $post->images()->count() > 0) {
    $classes[] = 'profile-statusposts__item--photo';
    $type = 'photo';
} elseif (isset($post) && $post->videos()->count() > 0) {
    $classes[] = 'profile-statusposts__item--video';
    $type = 'video';
}
?>
<div class="profile-statusposts__item <?php echo e(implode(' ', $classes)); ?>" data-user-post data-user-post-type="<?php echo e($type); ?>"
     <?php if(isset($post) && $post->id !== null): ?> data-user-post-href="<?php echo e(route('user.post.template', [$user->username, $post->id])); ?>" <?php endif; ?>>
    <?php if(isset($post) && $post->id !== null): ?>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 statusposts-lead">
                <div class="profile-statusposts__actions">
            <button class="btn btn--inline" data-user-post-cancel>
                <i class="fa fa-close" aria-hidden="true"></i> Bearbeiten abbrechen
            </button>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php if(!isset($post) || $post->id === null): ?>
        <?php
        $newPost = new \Vereinsleben\Post;
        if (Auth::check()) {
            $newPost->user_id = Auth::user()->id;
        }
        $newPost->published_at = date('Y-m-d H:i:s');
        $publishedDate = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $newPost->published_at);
        ?>
        <?php echo Form::model(
            $newPost,
            [
                'route' => ['user.post.store', $user->username],
                'enctype' => 'multipart/form-data',
                'files' => true,
                'method' => 'POST',
                'data-user-post-create-form' => '',
            ]
        ); ?>

    <?php else: ?>
        <?php echo Form::model(
            $post,
            [
                'route' => ['user.post.update', $user->username, $post->id],
                'enctype' => 'multipart/form-data',
                'files' => true,
                'method' => 'POST',
                'data-user-post-edit-form' => '',
            ]
        ); ?>

        <?php
        $publishedDate = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $post->published_at);
        ?>
    <?php endif; ?>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-3 col-xs-4">
            <div class="profile-statusposts__date">
        <div class="profile-statusposts__day">
            <?php echo e($publishedDate->day); ?>

        </div>
        <div class="profile-statusposts__month-and-year">
            <div class="profile-statusposts__month">
                <?php echo e($publishedDate->formatLocalized('%B')); ?>

            </div>
            <div class="profile-statusposts__year">
                <?php echo e($publishedDate->year); ?>

            </div>
        </div>
            </div>
        </div>
        <div class="col-lg-11 col-md-11 col-sm-9 col-xs-8 profile-statusposts-lead">
            <div class="profile-statusposts__form">
        <?php echo Form::textarea('content', null, ['class' => 'input profile-statusposts__form-input', 'rows' => 5,
            'placeholder' => 'Was machst du gerade?', 'required' => true, 'data-user-post-content-field' => true]); ?>


        <div class="profile-statusposts__advanced-options">
            <button class="profile-statusposts__advanced-options-button btn btn--inline btn--icon btn--icon-big btn--icon-switch" data-statuspost-media-button="photo">
                <i class="fa fa-camera" aria-hidden="true"></i>
            </button>
            <button class="profile-statusposts__advanced-options-button btn btn--inline btn--icon btn--icon-big btn--icon-switch" data-statuspost-media-button="video">
                <i class="fa fa-youtube-play " aria-hidden="true"></i>
            </button>
        </div>
                <div class="clear"></div>

            </div>

    <div class="profile-statusposts__edit-photo" data-statuspost-media-content="photo">
        <div id="<?php if(isset($post)): ?><?php echo e('upload-result-'. $post->id); ?><?php else: ?><?php echo e('upload-result'); ?><?php endif; ?> "
             class="thumbnail__list">
            <?php if(isset($post)): ?>
                <?php $__currentLoopData = $post->images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <input type="checkbox" name="delete-image[]" value="<?php echo e($image->id); ?>"
                           id="event_thumbnail-<?php echo e($image->id); ?>" class="thumbnail__switch">
                    <label for="event_thumbnail-<?php echo e($image->id); ?>"
                           class="thumbnail__label thumbnail__container thumbnail__container--deleteable">
                        <img class="thumbnail__image" src="<?php echo e(asset($image->picture->url('singleView'))); ?>"/>
                    </label>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
        </div>

        <div data-upload-result>
            <div class="hidden" data-new-image-text>
                <span class="input-label">Neu hinzugefügte Bilder</span>
            </div>
            <div>
                <div id="<?php if(isset($post)): ?><?php echo e('upload-result-'. $post->id); ?><?php else: ?><?php echo e('upload-result'); ?><?php endif; ?> "
                     class="thumbnail__list">
                </div>
            </div>
            <div>
                <div>
                    <label for="<?php if(isset($post)): ?><?php echo e('user_post_image-'. $post->id); ?><?php else: ?><?php echo e('user_post_image'); ?><?php endif; ?>"
                           class="btn btn--grey btn--full-width btn--large">
                        <span class="fa fa-upload"></span> Bilder hochladen</label>
                    <input multiple type="file" name="images[]"
                           id="<?php if(isset($post)): ?><?php echo e('user_post_image-'. $post->id); ?><?php else: ?><?php echo e('user_post_image'); ?><?php endif; ?>"
                           class="input-file">
                </div>
            </div>
        </div>
    </div>

    <div class="profile-statusposts__edit-video" data-statuspost-media-content="video">
        <div>
            <?php if(isset($post) && $post->videos()->count() > 0 && ($video = $post->videos->first()) !== null): ?>
                <?php echo Form::text('video_youtube', $video->url, ['class' => 'input profile-statusposts__form-input', 'placeholder' => 'YouTube/Vimeo Videolink']); ?>

            <?php else: ?>
                <?php echo Form::text('video_youtube', null, ['class' => 'input profile-statusposts__form-input', 'placeholder' => 'YouTube/Vimeo Videolink']); ?>

            <?php endif; ?>
        </div>
        <div data-video-preview></div>
    </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-3 col-xs-4"></div>
        <div class="col-lg-11 col-md-11 col-sm-9 col-xs-8">
            <button type="submit" class="profile-statusposts__form-btn btn btn--input btn--inline btn--dark-blue">
                Posten
            </button>
            <?php if(!isset($post) || $post->id === null): ?>
                <button class="profile-statusposts__form-btn--reset btn btn--input btn--inline" data-user-post-reset>
                    <i class="fa fa-close" aria-hidden="true"></i> Zurücksetzen
                </button>
            <?php endif; ?>
        </div>
    </div>

    <?php echo e(Form::close()); ?>

</div>
