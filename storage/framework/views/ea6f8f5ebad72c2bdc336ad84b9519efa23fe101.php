<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('admin.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <section class="club-new" data-club-editable="true">

        <div class="club-detail-content">
            <div class="container-flex">
                <h1 class="club-detail__headline">Verband bearbeiten</h1>
                <div class="row">
                    <div class="col-md-10 col-md-offset-2">
                        <div class="club-content__edit-area club-content__box">
                            <?php echo $__env->make('admin.bandage.edit.base-data', ['bandage' => $bandage, 'bandages' => $bandages], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <section class="admin__overview">
            <div class="container-flex">
                <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">Verein Admins</h1>
                <div class="row">

                    <?php echo Form::open(['route' => 'admin.bandage.admin.add', 'method' => 'GET', 'id' => 'search', 'class' => '']); ?>

                    <div class="col-md-3">
                        <div class="input-group">
                            <?php echo Form::text('user_id', isset($query['user_id']) ? $query['user_id'] : null, ['id' => 'user_id', 'autofocus', 'class' => 'input', 'placeholder' => 'Nutzer-ID']); ?>

                        </div>
                    </div>
                    <?php echo e(Form::hidden('bandage_id', $bandage->id, array('id' => $bandage->id))); ?>

                    <div class="col-md-3">
                        <div class="input-group">
                            <?php echo Form::submit('Admin hinzufügen', array('class' => 'button button--light button--light-white button--center button--full-width')); ?>

                        </div>
                    </div>
                    <?php echo Form::close(); ?>


                </div>
                <div class="row">
                    <?php if(count($admins) > 0): ?>
                        <table id="admin-bandage-admins" class="admin__table" data-token="<?php echo e(csrf_token()); ?>">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Admin-Name</th>
                                <th>Admin-Email</th>
                                <th>Optionen</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $admins; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $admin): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><a href="<?php echo e(route('user.detail', ['username' => $admin->username])); ?>"
                                           target="_blank"><?php echo e($admin->id); ?></a>
                                    </td>
                                    <td>
                                        <?php echo e($admin->firstname); ?>&nbsp;<?php echo e($admin->lastname); ?></td>
                                    <td><?php echo e($admin->email); ?></td>
                                    <td>
                                        <div class="admin__table-option">
                                            <a onclick="return confirm('Vereinsadmin wirklich löschen?')"
                                               href="<?php echo e(route('admin.bandage.admin.delete', ['bandage_id' => $bandage->id, 'id' => $admin->id])); ?>">
                                                <span class="fa fa-trash"></span>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    <?php endif; ?>
                </div>
            </div>
        </section>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
    <script>
        $().ready(function(){
            var editClubSelector = '[data-club-editable]';
            var sectionSelector = '[data-section-editable]';
            var editButtonSelector = '.club-content__edit-button';
            var cancelEditButtonSelector = '.club-content__form-cancel-action-button';
            var editAreaSelector = '.club-content__edit-area';
            var viewAreaSelector = '.club-content__view-area';

            var $editClubHandle = $(editClubSelector);
            if ($editClubHandle) {
                $editClubHandle.find(sectionSelector).each(function () {
                    $(this).find(editAreaSelector).hide();
                });

                $(editClubSelector).on('click', editButtonSelector, function (e) {
                    // e.preventDefault();
                    var $section = $(this).closest(sectionSelector);
                    $section.find(viewAreaSelector).hide();
                    $section.find(editButtonSelector).hide();
                    $section.find(editAreaSelector).show();
                });

                $(editClubSelector).on('click', cancelEditButtonSelector, function (e) {
                    e.preventDefault();
                    var $section = $(this).closest(sectionSelector);
                    $section.find(viewAreaSelector).show();
                    $section.find(editButtonSelector).show();
                    $section.find(editAreaSelector).hide();
                });
            }
        });

        $('#admin-bandage-admins').DataTable({
            dom         : 'Blfrtip',
            "lengthMenu": [50, 100, 250],
            buttons     : [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],

            language: {
                url: '/js/vendor/i18n/dataTables.german.json'
            }
        }).on('page.dt', function() {
            $('html, body').animate({
                scrollTop: $(".dataTables_wrapper").offset().top
            }, 'slow');

            $('thead tr th:first-child').focus().blur();
        });
    </script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script src="http://legacy.datatables.net/release-datatables/extras/TableTools/media/js/TableTools.js"></script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>