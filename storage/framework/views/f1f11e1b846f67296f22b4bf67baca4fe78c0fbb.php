<?php if(count($news) > 0): ?>
    <section class="sportnews-section sportnews-inner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Das könnte Dich auch interessieren</h2>
                </div>
            </div>
            <div class="row sportnews-start">
                <div class="col-lg-9 col-sm-6">
                    <div class="row">
                        <?php if(isset($news[0])): ?>
                            <div class="col-lg-4 col-sm-6 mobile-news-row1">
                                
                                <a href="<?php echo e(route('news.detail', [$news[0]->category_id, $news[0]->slug])); ?>">
                                    <div class="row m-new-l">
                                        <div class="col-lg-12 news-a space-right-remove"
                                             style="background:url('<?php echo e(($news[0]->main_image !== null) ? asset($news[0]->main_image->url('startpage')) : ''); ?>') top center no-repeat;background-size: cover;">
                                            <div class="news-arrow-top">
                                                <img src="<?php echo e(asset('static_images/arrow-top.png')); ?>" alt=""/>
                                            </div>
                                            <div class="news-arrow-left">
                                                <img src="<?php echo e(asset('static_images/arrow-left.png')); ?>" alt=""/>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <div class="row mob-mr">
                                    <div class="col-lg-12 col-xs-6 m-new-row">
                                        <div class="news-box">
                                            <div class="news-box-inner">
                                                <h4>
                                                    
                                                    <a href="<?php echo e(route('news.detail', [$news[0]->category_id, $news[0]->slug])); ?>"><?php echo e($news[0]->title); ?></a>
                                                </h4>
                                                <p> <?php echo e(str_limit($news[0]->content_teaser, 150, '...')); ?></p>
                                                <div class="mehr">
                                                    
                                                    <a href="<?php echo e(route('news.detail', [$news[0]->category_id, $news[0]->slug])); ?>">Mehr</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="col-lg-8 col-sm-6 mobile-news-row2">
                            <?php if(isset($news[1])): ?>
                                <div class="row news-row-space1">
                                    <div class="col-lg-6 col-sm-6 space-right-remove m-new-l">
                                        <div class="news-box news-box2">
                                            <div class="news-box-inner">
                                                <h4>
                                                    
                                                    <a href="<?php echo e(route('news.detail', [$news[1]->category_id, $news[1]->slug])); ?>"><?php echo e($news[1]->title); ?></a>
                                                </h4>
                                                <p> <?php echo e(str_limit($news[1]->content_teaser, 150, '...')); ?></p>
                                                <div class="mehr">
                                                    
                                                    <a href="<?php echo e(route('news.detail', [$news[1]->category_id, $news[1]->slug])); ?>">Mehr</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <a href="<?php echo e(route('news.detail', [$news[1]->category_id, $news[1]->slug])); ?>">
                                        <div class="col-lg-6 col-sm-6 news-b space-left-remove m-new-r"
                                             style="background:url('<?php echo e(($news[1]->main_image !== null) ? asset($news[1]->main_image->url('startpage')) : ''); ?>') top center no-repeat;background-size: cover;">
                                            <div class="news-arrow-right"><img
                                                        src="<?php echo e(asset('static_images/arrow-right.png')); ?>" alt=""/></div>
                                        </div>
                                    </a>
                                </div>
                            <?php endif; ?>
                            <?php if(isset($news[2])): ?>
                                <div class="row news-row-space1">
                                    
                                    <a href="<?php echo e(route('news.detail', [$news[2]->category_id, $news[2]->slug])); ?>">
                                        <div class="col-lg-6 col-sm-6 news-b space-right-remove m-new-l"
                                             style="background:url('<?php echo e(($news[2]->main_image !== null) ? asset($news[2]->main_image->url('startpage')) : ''); ?>') top center no-repeat;background-size: cover;">
                                        <div class="news-arrow-left"><img
                                                    src="<?php echo e(asset('static_images/arrow-left.png')); ?>" alt=""/></div>
                                    </div>
                                    </a>
                                    <div class="col-lg-6 col-sm-6 space-right-remove m-new-r">
                                        <div class="news-box news-box2">
                                            <div class="news-box-inner">
                                                <h4>
                                                    
                                                    <a href="<?php echo e(route('news.detail', [$news[2]->category_id, $news[2]->slug])); ?>"><?php echo e($news[2]->title); ?></a>
                                                </h4>
                                                <p> <?php echo e(str_limit($news[2]->content_teaser, 150, '...')); ?></p>
                                                <div class="mehr">
                                                    
                                                    <a href="<?php echo e(route('news.detail', [$news[2]->category_id, $news[2]->slug])); ?>">Mehr</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

                
                    
                        
                            
                                
                                
                                    
                                        
                                            
                                            
                                        
                                    
                                
                            
                        
                    
                
            </div>
            <div class="row alle-news">
                
                
                <div class="col-lg-12"><a class="btn btn-default registrieren-btn"
                                          href="<?php echo e(url('/neuigkeiten')); ?>">alle News</a></div>
            </div>
        </div>
    </section>
<?php endif; ?>