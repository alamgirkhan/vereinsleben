<section class="section section--white section--vote-bordered no-before no-after">

    <?php if($previous && ($previous->getWinner() !== false && !$previous->isCurrent()) && !($vote instanceof \Vereinsleben\Vote)): ?>

        <?php if(!($vote instanceof \Vereinsleben\Vote)): ?>
            <?php echo $__env->make('vote.intro', ['vote' => $previous], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php endif; ?>
    <?php elseif($previous && ($previous->getWinner() === false && !$previous->isCurrent()) && !($vote instanceof \Vereinsleben\Vote)): ?>
        
        <?php echo $__env->make('vote.break-between', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>

    
    
    <?php if(!$previous && !($vote instanceof \Vereinsleben\Vote)): ?>
        <?php echo $__env->make('vote.new-year-teaser', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>

    
    <?php if($vote instanceof \Vereinsleben\Vote): ?>
        <?php echo $__env->make('vote.intro', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>

    
    <?php if($vote && $vote->isCurrent() ): ?>
        <?php echo $__env->make('vote.nominee-list', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>

    
    <?php if($vote ): ?>
        <?php echo $__env->make('vote.partials.teaser-content', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>
</section>