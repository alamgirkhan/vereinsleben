<section class="profile-section profile-section--avatar">
    <div class="profile-avatar">
        <div class="profile-avatar__image-container">
            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $user)): ?>
                <div class="btn btn--icon profile__edit-button animation animation--scale" data-call-popover="avatar">
                    <i class="fa fa-upload" aria-hidden="true"></i>
                </div>
            <?php endif; ?>
            <div class="profile-avatar__image-wrapper col-lg-12 col-md-12 col-sm-12 col-xs-3 profile-pic">
                <img class="profile-avatar__image" src="<?php echo e(asset($user->avatar->url('singleView'))); ?>"
                     data-upload-image/>
            </div>
        </div>

        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $user)): ?>
            <?php echo $__env->make('user.profile.partials.edit.avatar', ['user' => $user], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php endif; ?>
    </div>
</section>