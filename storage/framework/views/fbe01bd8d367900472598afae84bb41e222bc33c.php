<?php $__env->startSection('content'); ?>
    <?php if(Auth::user()->isAdmin()): ?>
        <?php echo $__env->make('admin.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>
    <section class="admin__overview">
        <div class="container">
            <div class="row admin__section">
                <div class="col-xs-12">
                    <h1 class="hl hl--upper hl-alternate-ft hl--large-alternate">News</h1>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <a href="<?php echo e(route('admin.news.create')); ?>"
                           class="button button--icon button--full-width button--center button--light">
                            <span class="fa fa-plus"></span> News hinzufügen
                        </a>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="input-group">
                        <a href="<?php echo e(route('admin.news.pos')); ?>"
                           class="button button--icon button--full-width button--center button--light">
                            News Positionen
                        </a>
                    </div>
                </div>

            </div>
            <table id="admin-table" data-order="[[ 1, &quot;desc&quot; ]]" class="admin__table"
                   data-token="<?php echo e(csrf_token()); ?>">
                <thead>
                    <tr>
                        <th style="width:15%">Galerie&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                        <th style="width:5%">ID&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                        <th style="width:35%">Titel&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                        <th style="width:10%">Autor&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                        <th style="width:15%">Erstellt&nbsp;<i class="glyphicon glyphicon-sort"></i></th>
                        <th style="width:10%">Publiziert</th>
                        <th style="width:10%">Optionen</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $newsItem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php if($newsItem->has_gallery == 1): ?>
                                    <span><i class="fa fa-newspaper-o" aria-hidden="true"></i></span>
                                <?php endif; ?>
                            </td>
                            <td><?php echo e($newsItem->id); ?></td>
                            <td><?php echo e($newsItem->title); ?></td>
                            <td><?php echo e($newsItem->user['fullname'] != null || $newsItem->user['fullname'] != "" ? $newsItem->user['fullname'] : $newsItem->user['username']); ?></td>
                            <td><?php echo e(date('Y/m/d', strtotime($newsItem->created_at))); ?></td>
                            <td>
                                <div class="onoffswitch">
                                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox"
                                           id="news-published-<?php echo e($key); ?>" <?php echo e(($newsItem->state === News::STATE_PUBLISHED) ? 'checked' : ''); ?>>
                                    <label data-toggle-published data-news-id="<?php echo e($newsItem->id); ?>"
                                           class="onoffswitch-label" for="news-published-<?php echo e($key); ?>"></label>
                                </div>
                            </td>

                            <td>
                                <div class="admin__table-option">
                                    <a href="<?php echo e(route('admin.news.edit', $newsItem)); ?>"><span class="fa fa-edit"></span></a>
                                    <?php if(is_null($newsItem->deleted_at) && Auth::user()->isAdmin()): ?>
                                        <a data-toggle-delete data-record-id="<?php echo e($newsItem->id); ?>"
                                           data-delete-path="<?php echo e(route('admin.news.destroy')); ?>" href="#">
                                            <span class="fa fa-trash"></span>
                                        </a>
                                    <?php endif; ?>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>