<?php


use Vereinsleben\Services\GeocoderHelper;

class GeocoderHelperTest extends \TestCase
{


    public function test()
    {
        $geocoderHelper = new GeocoderHelper();
//        $geocoderHelper->setAddress('56564', 'Neuwied', 'Hermannstr', '1');
//        $geocoderHelper->setIpAddress('212.7.163.49');
//        $this->assertEquals('Rheinland-Pfalz', $geocoderHelper->getFederalStateInGerman());
//        $geocoderHelper->setIpAddress('108.171.129.166');
//        echo($geocoderHelper->getFederalStateInGerman());

        $geocoderHelper->setIpAddress('46.5.19.18');
        echo($geocoderHelper->getFederalStateInGerman());

//        $geocoderHelper->setIpAddress('47.31.14.113');
//        echo($geocoderHelper->getFederalStateInGerman());
    }

    public function testGetFederalstateInGerman()
    {
        $geocoderHelper = new GeocoderHelper('Germany');
        $geocoderHelper->setAddress('01067', 'Dresden');
        $this->assertEquals('Sachsen', $geocoderHelper->getFederalStateInGerman());
        $geocoderHelper->setAddress('70173', 'Stuttgart');
//        $this->assertEquals('Baden-Württemberg', $geocoderHelper->getFederalState('Stuttgart, Germany'));
        $geocoderHelper->setAddress('50667', 'köln');
        $this->assertEquals('Nordrhein-Wesfalen', $geocoderHelper->getFederalStateInGerman());
        $geocoderHelper->setAddress('30159', 'Hannover');
        $this->assertEquals('Niedersachsen', $geocoderHelper->getFederalStateInGerman());
        $geocoderHelper->setAddress('80331', 'München');
        $this->assertEquals('Bayern', $geocoderHelper->getFederalStateInGerman());
        $geocoderHelper->setAddress('56564', 'Neuwied');
        $this->assertEquals('Rheinland-Pfalz', $geocoderHelper->getFederalStateInGerman());
        $geocoderHelper->setAddress('55246', 'Wiesbaden');
        $this->assertEquals('Hessen', $geocoderHelper->getFederalStateInGerman());
        $geocoderHelper->setAddress('66111', 'Saarbrücken');
        $this->assertEquals('Saarland', $geocoderHelper->getFederalStateInGerman());
        $geocoderHelper->setAddress('10115', 'Berlin');
        $this->assertEquals('Berlin', $geocoderHelper->getFederalStateInGerman());
        $geocoderHelper->setAddress('14770', 'Brandenburg');
        $this->assertEquals('Brandenburg', $geocoderHelper->getFederalStateInGerman());
        $geocoderHelper->setAddress('24103', 'Kiel');
        $this->assertEquals('Schleswig-Holstein', $geocoderHelper->getFederalStateInGerman());
        $geocoderHelper->setAddress('19053', 'Schwerin');
        $this->assertEquals('Mecklenburg-Vorpommern', $geocoderHelper->getFederalStateInGerman());
        $geocoderHelper->setAddress('99084', 'Erfurt');
        $this->assertEquals('Thüringen', $geocoderHelper->getFederalStateInGerman());
        $geocoderHelper->setAddress('28195', 'Bremen');
        $this->assertEquals('Bremen', $geocoderHelper->getFederalState());
        $geocoderHelper->setAddress('20095', 'Hamburg');
        $this->assertEquals('Hamburg', $geocoderHelper->getFederalState('Hamburg, Germany'));
    }

}
